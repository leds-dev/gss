1) Determine the directory where you want to place the GSS software.  This
   directory will be referred to below as <GSS_DIR>.  For example, if you want
   the gss1.6 directory to be under your home directory /export/home/gtacs,
   then for purposes of these instructions read <GSS_DIR> as /export/home/gtacs.

2) Place the CD in the CDROM drive, and cd to the lowest-level CDROM directory
   containing the two delivered files.  This directory varies depending on the
   server.  Confirm the UNIX name, size, and modified-date of the two delivered
   files by executing the following command:

	ls -lrt

   Note that the filenames may not match those in section 3.2 exactly due to
   differences between the UNIX and Windows operating systems.  Specifically,
   the UNIX filenames may appear in lower case, and may be contracted.  However,
   the sizes and modified-dates should match exactly.  Let the UNIX version of
   the GSS filename be <GSS_FILE>.  Copy the GSS file from the CD by executing
   the following commands:

	cp <GSS_FILE> <GSS_DIR>/gss_16_tar.Z  

3) cd to <GSS_DIR>, and execute the following commands:
   
   	uncompress gss_16_tar.Z
   	tar xvf gss_16_tar
   	rm gss_16_tar
   
4) If you have installed the gss into a directory other than your home directory,
   you must edit the following line in the file <GSS_DIR>/gss1.6/gss/RunGSS

	setenv GSS_HOME ${HOME}

   and replace the string ${HOME} with <GSS_DIR>.
   
5) Verify that the /etc/hosts file does not have a commented out reference
   to your local node prior to the formal reference.  Example (of what to avoid):
   
   	#mynode		192.168.254.1
   	mynode		192.168.1.1
   	
6) To give all users the capability to browse to uncontrolled INI and SSC files,
   leave the following line uncommented in the file <GSS_DIR>/gss1.6/gss/RunGSS:

	setenv GSS_DB_BROWSE_ENABLE YES

   To control this capability on a per-user basis, comment out the above line      
   in the file <GSS_DIR>/gss1.6/gss/RunGSS, and instead add one of the following
   two lines to the particular user's .profile file, using the profile file located
   in the <GSS_DIR>/gss1.6/login_files directory as an example:

   	GSS_DB_BROWSE_ENABLE=NO; export GSS_DB_BROWSE_ENABLE
						- or -
   	GSS_DB_BROWSE_ENABLE=YES; export GSS_DB_BROWSE_ENABLE

   The first line will disable this capability; the second will enable it.
   Note that if GSS_DB_BROWSE_ENABLE is not defined anywhere, browsing will be enabled.

7) The RunGSS file specifies the HTML viewer to use when opening the User's Guide
   and Maintenance Manual.  By default, the command to open the HTML viewer is set as
   follows:

	setenv GSS_HTML_VIEWER /bin/netscape

   This line of the RunGSS file should be edited as appropriate to specifiy the command
   used to open an HTML viewer on the local node.

8) The RunGSS file specifies the printer to use when outputting SkyMap and Star Tracker
   screen snaps.  By default, the printer command is set as follows:

	setenv GSS_PRINT_CMD 'lpr -Pprinter1'

   This line of the RunGSS file should be edited as appropriate to specifiy the command
   used to print on the local node.  The screen snaps are in color, so a color printer
   should be specified if possible.

9) In the $EPOCH_OUTPUT directory, for each server (e.g. nemesis, as in the example below)
   and spacecraft (e.g. goes13) add the following directory, if it does not already exist:

	gss

   Example: /export/home/gtacsops/epoch/output/nemesis/goes13/gss
    
10) Using the dt_dtwmrc file located in the <GSS_DIR>/gss1.6/login_files directory as an
    example, copy the following line

   	"GSS"       f.exec "<GSS_DIR>/gss1.6/gss/RunGSS"

    from the "Goesactionitems" menu into your ${HOME}/.dt/dtwmrc file.
   
11) Log off the server and log back in again.

12) Confirm that EPOCH has been installed completely.  If this is the case, cd to
    <GSS_DIR>/gss1.6/gss, and execute the following two commands to completely rebuild the
    GSS against the current EPOCH libraries:

	MakeCleanGSS
	MakeGSS
	
    If EPOCH has not been installed completely, then it may not be possible to rebuld the GSS,
    in which case this step can be skipped and the delivered executable used instead.  The GSS
    has been observed to operate in a more stable fashion if rebuilt upon installation.

13) GSS can be run from either the pull down menu (under GSS, which may itself be under a submenu,
    depending on the contents of your ${HOME}/.dt/dtwmrc file) or by executing
    <GSS_DIR>/gss1.6/gss/RunGSS from a terminal window.
