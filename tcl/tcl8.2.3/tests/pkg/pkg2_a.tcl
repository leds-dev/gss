# pkg2_a.tcl --
#
#  Test package for pkg_mkIndex. This package is required by pkg1.
#  This package is split into two files, to test packages that are split
#  over multiple files.
#
# Copyright (c) 2998 by Scriptics Corporation.
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAIMER OF ALL WARRANTIES.
# 
# SCCS: @(#) pkg2_a.tcl 1.1 14/09/22 12:58:45

package provide pkg2 1.0

namespace eval pkg2 {
    namespace export p2-1
}

proc pkg2::p2-1 { num } {
    return [expr $num * 2]
}
