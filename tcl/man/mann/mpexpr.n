.TH mpexpr TCL "8 January 1998" "Tcl"
'\" Copyright (c) 1993 The Regents of the University of California.
'\" Copyright (c) 1994-1996 Sun Microsystems, Inc.
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" SCCS: @(#) mpexpr.n 1.19 96/10/09 08:29:27
'\" 
'\"  mpexpr - this man page largely copied from Tcl's expr.n
'\" Note:  do not modify the .SH NAME line immediately below!
.SH NAME
mpexpr \- Evaluate an expression with multiple precision math
.sp
.SH SYNOPSIS
.sp
\fBpackage require Mpexpr\fR
.br
\fBmpexpr \fIarg \fR?\fIarg arg ...\fR?
.br
\fBmpformat \fIformatString \fR?\fIarg arg ...\fR?
.br
\fBglobal mp_precision\fR
.sp
.SH DESCRIPTION
.PP
Mpexpr is based on Tcl's native \fIexpr\fR command, and shares many
similarities with \fIexpr\fR.  Mpexpr performs all of its calculations
using an arbitrary precision math package.
.PP
Mpexpr concatenates \fIarg\fR's (adding separator spaces between them),
evaluates the result as a Tcl expression, and returns the value.
The operators permitted in Tcl expressions are a subset of
the operators permitted in C expressions, and they have the
same meaning and precedence as the corresponding C operators.
Expressions almost always yield numeric results
(integer or floating-point values).
For example, the expression
.sp
\fBmpexpr 8.2 + 6\fR
.sp
evaluates to 14.2.
Tcl expressions differ from C expressions in the way that
operands are specified.  Also, Tcl expressions support
non-numeric operands and string comparisons.
.sp
.SH OPERANDS
.PP
A Tcl expression consists of a combination of operands, operators,
and parentheses.
White space may be used between the operands and operators and
parentheses; it is ignored by the expression processor.
Where possible, operands are interpreted as integer values.
Integer values may be specified in decimal (the normal case), in octal (if the
first character of the operand is \fB0\fR), or in hexadecimal (if the first
two characters of the operand are \fB0x\fR).
If an operand does not have one of the integer formats given
above, then it is treated as a floating-point number if that is
possible.  Floating-point numbers may be specified in any of the
ways accepted by an ANSI-compliant C compiler (except that the
``f'', ``F'', ``l'', and ``L'' suffixes will not be permitted in
most installations).  For example, all of the
following are valid floating-point numbers:  2.1, 3., 6e4, 7.91e+16.
If no numeric interpretation is possible, then an operand is left
as a string (and only a limited set of operators may be applied to
it).
.PP
Operands may be specified in any of the following ways:
.IP [1]
As an numeric value, either integer or floating-point.
.IP [2]
As a Tcl variable, using standard \fB$\fR notation.
The variable's value will be used as the operand.
.IP [3]
As a string enclosed in double-quotes.
The expression parser will perform backslash, variable, and
command substitutions on the information between the quotes,
and use the resulting value as the operand
.IP [4]
As a string enclosed in braces.
The characters between the open brace and matching close brace
will be used as the operand without any substitutions.
.IP [5]
As a Tcl command enclosed in brackets.
The command will be executed and its result will be used as
the operand.
.IP [6]
As a mathematical function whose arguments have any of the above
forms for operands, such as ``\fBsin($x)\fR''.  See below for a list of defined
functions.
.LP
Where substitutions occur above (e.g. inside quoted strings), they
are performed by the expression processor.
However, an additional layer of substitution may already have
been performed by the command parser before the expression
processor was called.
As discussed below, it is usually best to enclose expressions
in braces to prevent the command parser from performing substitutions
on the contents.
.PP
For some examples of simple expressions, suppose the variable
\fBa\fR has the value 3 and
the variable \fBb\fR has the value 6.
Then the command on the left side of each of the lines below
will produce the value on the following line:
.sp
.TP
\fBmpexpr 3.1 + $a\fR
6.1
.TP
\fBmpexpr 2 + "$a.$b"\fR
5.6
.TP
\fBmpexpr 4*[llength "6 2"]\fR
8
.TP
\fBmpexpr {{word one} < "word $a"}\fR
0
.br
.sp
.SH OPERATORS
.PP
The valid operators are listed below, grouped in decreasing order
of precedence:
.TP 20
\fB\-\0\0+\0\0~\0\0!\fR
Unary minus, unary plus, bit-wise NOT, logical NOT.  None of these operands
may be applied to string operands, and bit-wise NOT may be
applied only to integers.
.TP 20
\fB*\0\0/\0\0%\fR
Multiply, divide, remainder.  None of these operands may be
applied to string operands, and remainder may be applied only
to integers.
The remainder will always have the same sign as the divisor and
an absolute value smaller than the divisor.
.TP 20
\fB+\0\0\-\fR
Add and subtract.  Valid for any numeric operands.
.TP 20
\fB<<\0\0>>\fR
Left and right shift.  Valid for integer operands only.
Integers in mpexpr are not limited to a machine word and do not
use two's complement format.  Therefore shifting will not include a sign bit.
.TP 20
\fB<\0\0>\0\0<=\0\0>=\fR
Boolean less, greater, less than or equal, and greater than or equal.
Each operator produces 1 if the condition is true, 0 otherwise.
These operators may be applied to strings as well as numeric operands,
in which case string comparison is used.
.TP 20
\fB==\0\0!=\fR
Boolean equal and not equal.  Each operator produces a zero/one result.
Valid for all operand types.
.TP 20
\fB&\fR
Bit-wise AND.  Valid for integer operands only.
.TP 20
\fB^\fR
Bit-wise exclusive OR.  Valid for integer operands only.
.TP 20
\fB|\fR
Bit-wise OR.  Valid for integer operands only.
.TP 20
\fB&&\fR
Logical AND.  Produces a 1 result if both operands are non-zero, 0 otherwise.
Valid for numeric operands only (integers or floating-point).
.TP 20
\fB||\fR
Logical OR.  Produces a 0 result if both operands are zero, 1 otherwise.
Valid for numeric operands only (integers or floating-point).
.TP 20
\fIx\fB?\fIy\fB:\fIz\fR
If-then-else, as in C.  If \fIx\fR
evaluates to non-zero, then the result is the value of \fIy\fR.
Otherwise the result is the value of \fIz\fR.
The \fIx\fR operand must have a numeric value.
.LP
See the C manual for more details on the results
produced by each operator.
All of the binary operators group left-to-right within the same
precedence level.  For example, the command
.sp
\fBmpexpr 4*2 < 7\fR
.sp
returns 0.
.PP
The \fB&&\fR, \fB||\fR, and \fB?:\fR operators have ``lazy
evaluation'', just as in C, 
which means that operands are not evaluated if they are
not needed to determine the outcome.  For example, in the command
.sp
\fBmpexpr {$v ? [a] : [b]}\fR
.sp
only one of \fB[a]\fR or \fB[b]\fR will actually be evaluated,
depending on the value of \fB$v\fR.  Note, however, that this is
only true if the entire expression is enclosed in braces;  otherwise
the Tcl parser will evaluate both \fB[a]\fR and \fB[b]\fR before
invoking the \fBexpr\fR command.
.sp
.SH "MATH FUNCTIONS"
.PP
Mpexpr supports the following mathematical functions in expressions.
\fIx\fR and \fIy\fR are integer or floating point values;
\fIi\fR, \fIj\fR and \fIc\fR are integer values;
.PP
Math functions compatible with \fIexpr\fR:
.TP 15
\fBacos(\fIx\fB)\fR
Arc cosine of \fIx\fR.
.TP 15
\fBasin(\fIx\fB)\fR
Arc sine of \fIx\fR.
.TP 15
\fBatan(\fIx\fB)\fR
Arc tangent of \fIx\fR.
.TP 15
\fBatan2(\fIx,y\fB)\fR
Arc tangent of \fIx\fR / \fIy\fR.
.TP 15
\fBceil(\fIx\fB)\fR
Least integral value greater than or equal to \fIx\fR.
.TP 15
\fBcos(\fIx\fB)\fR
Cosine of \fIx\fR.
.TP 15
\fBcosh(\fIx\fB)\fR
Hyperbolic cosine of \fIx\fR.
.TP 15
\fBexp(\fIx\fB)\fR
Exponential function e ** \fIx\fR.
.TP 15
\fBfloor(\fIx\fB)\fR
Greatest integral value less than or equal to \fIx\fR.
.TP 15
\fBfmod(\fIx,y\fB)\fR
Remainder of \fIx\fR divided by \fIy\fR.
.TP 15
\fBhypot(\fIx,y\fB)\fR
Euclidean distance of sqrt( \fIx\fR * \fIx\fR + \fIy\fR * \fIy\fR).
.TP 15
\fBlog(\fIx\fB)\fR
Natural logarithm of \fIx\fR.
.TP 15
\fBlog10(\fIx\fB)\fR
Base-10 logarithm of \fIx\fR.
.TP 15
\fBpow(\fIx,y\fB)\fR
\fIx\fR raised to the \fIy\fR power.
.TP 15
\fBsin(\fIx\fB)\fR
Sine of \fIx\fR.
.TP 15
\fBsinh(\fIx\fB)\fR
Hyperbolic sine of \fIx\fR.
.TP 15
\fBsqrt(\fIx\fB)\fR
Square root of \fIx\fR.
.TP 15
\fBtan(\fIx\fB)\fR
Tangent of \fIx\fR.
.TP 15
\fBtanh(\fIx\fB)\fR
Hyperbolic tangent of \fIx\fR.
.TP 15
\fBabs(\fIx\fB)\fR
Returns the absolute value of \fIx\fR.  \fIx\fR may be either
integer or floating-point, and the result is returned in the same form.
.TP 15
\fBdouble(\fIx\fB)\fR
If \fIx\fR is a floating value, returns \fIx\fR, otherwise converts
\fIx\fR to floating and returns the converted value.
.TP 15
\fBint(\fIx\fB)\fR
If \fIx\fR is an integer value, returns \fIx\fR, otherwise converts
\fIx\fR to integer by truncation and returns the converted value.
.TP 15
\fBround(\fIx\fB)\fR
If \fIx\fR is an integer value, returns \fIx\fR, otherwise converts
\fIx\fR to integer by rounding and returns the converted value.
.PP
Additional \fImpexpr\fR functions:
.TP 15
\fBroot(\fIx,y\fB)\fR
The \fIyth\fR root of \fIx\fR.
.TP 15
\fBfrem(\fIx,y\fB)\fR
Remove all occurance of factor\fIy\fR from number \fIx\fR.
.TP 15
\fBminv(\fIx,y\fB)\fR
Inverse of \fIx\fR modulo \fIy\fR.
.TP 15
\fBgcd(\fIx,y\fB)\fR
Greatest common divisor of \fIx\fR and \fIy\fR.
.TP 15
\fBlcm(\fIx,y\fB)\fR
Least common multiple of \fIx\fR and \fIy\fR.
.TP 15
\fBmax(\fIx,y\fB)\fR
Maximum of \fIx\fR and \fIy\fR.
.TP 15
\fBmin(\fIx,y\fB)\fR
Minimum of \fIx\fR and \fIy\fR.
.TP 15
\fBpi()\fR
Value of pi.
.TP 15
\fBfib(\fIi\fB)\fR
Fibonacci number of integer \fIi\fR.
.TP 15
\fBfact(\fIi\fB)\fR
Factorial of integer \fIi\fR.
.TP 15
\fBpfact(\fIi\fB)\fR
Product of prime numbers up to integer \fIi\fR.
.TP 15
\fBlfactor(\fIi,c\fB)\fR
Lowest prime factor of integer \fIi\fR, trying count \fIc\fR times.
.TP 15
\fBiroot(\fIi,j\fB)\fR
Integer root \fIj\fR of integer \fIi\fR.
.TP 15
\fBgcdrem(\fIi,j\fB)\fR
Relatively prime of greatest common divisior of \fIi\fR divided by \fIj\fR.
.TP 15
\fBperm(\fIi,j\fB)\fR
Permutations of \fIi\fR taking \fIj\fR at a 
time: \fIi\fR ! / ( \fIi\fR - \fIj\fR ) !.
.TP 15
\fBcomb(\fIi,j\fB)\fR
Combinations of \fIi\fR taking \fIj\fR at a 
time: \fIi\fR ! / ( \fIj\fR ! * ( \fIi\fR - \fIj\fR ) ! ) .
.TP 15
\fBprime(\fIi,c\fB)\fR
Return 0 if \fIi\fR is not prime, return 1 if \fIi\fR  probably is prime.
Test for primality count \fIc\fR times.  
The chance of a non-prime passing this test is less than (1/4)^count.
For example, a count of 100 fails for only 1 in 10^60 numbers.
.TP 15
\fBrelprime(\fIi,j\fB)\fR
Return 1 if \fIi\fR and \fIj\fR are relatively prime to each other, 0
otherwise.
.sp
.SH "TYPES, OVERFLOW, AND PRECISION"
.PP
Computations are performed using arbitrary fixed and floating point
values.  Native machine values (\fIint, long, IEEE 754 floating point, 
etc. \fR ) 
and instructions are not used.
Conversion among internal representations for integer, floating-point,
and string operands is done automatically as needed.
For arithmetic computations, integers are used until some
floating-point number is introduced, after which floating-point is used.
For example,
.sp
\fBmpexpr 5 / 4\fR
.sp
returns 1, while
.sp
\fBmpexpr 5 / 4.0\fR
.br
\fBmpexpr 5 / ( [string length "abcd"] + 0.0 )\fR
.sp
both return 1.25.
Floating-point values are always returned with a ``.''
or an ``e'' so that they will not look like integer values.  For
example,
.sp
\fBmpexpr 20.0/5.0\fR
.sp
returns ``4.0'', not ``4''.  
.PP
The global variable \fBmp_precision\fR
determines the number of significant digits that are retained
during evaluation.    If \fBmp_precision\fR
is unset then 17 digits of precision are used.
The maximum value of \fBmp_precision\fR is 10000.
Note that larger values for \fBmp_precision\fR will require increasingly 
longer execution times.
Setting \fBmp_precision\fR to an illegal value will generate an error.
.sp
.SH "STRING OPERATIONS"
.PP
String values may be used as operands of the comparison operators,
although the expression evaluator tries to do comparisons as integer
or floating-point when it can.
If one of the operands of a comparison is a string and the other
has a numeric value, the numeric operand is converted back to
a string using the C \fIsprintf\fR format specifier
\fB%d\fR for integers and \fB%g\fR for floating-point values.
For example, the commands
.sp
\fBmpexpr {"0x03" > "2"}\fR
\fBmpexpr {"0y" < "0x12"}\fR
.sp
both return 1.  The first comparison is done using integer
comparison, and the second is done using string comparison after
the second operand is converted to the string ``18''.
Because of Tcl's tendency to treat values as numbers whenever
possible, it isn't generally a good idea to use operators like \fB==\fR
when you really want string comparison and the values of the
operands could be arbitrary;  it's better in these cases to use the
\fBstring compare\fR command instead.
.PP
\fBmpformat\fR formats a string in the style 
of Tcl's native \fIformat\fR command.
Mpformat will interpret numeric arguments as arbitrary precision numbers.
Mpformat performs limited % substitution on the output string.
The following may be specified:
.sp
\fB% [-] [width[.precision]] formatChar\fR
.PP
.TP
\fB-\fR
Specifies left justification; right justification is the default.
.TP
\fBwidth.precision\fR
Specifies optional width and precision.  Default precision is 8.  
Width and/or precision may be specified as \fB*\fR, in which the next argument
will be used for the width or precision value.
.PP
\fBFormat character and result\fR
.TP
\fBd\fR
Format next argument as integer, truncating after the decimal point.
.TP
\fBf\fR
Format next argument in decimal floating point.
.TP
\fBe\fR
Format next argument in scientific notation.
.TP
\fBr\fR, \fBR\fR
Format next argument as rational fraction x / y.
.TP
\fBN\fR
Format next argument as numerator only of rational fraction x / y.
.TP
\fBD\fR
Format next argument as denominator only of rational fraction x / y.
.TP
\fBo\fR
Format next argument in octal format, with leading '0'; floating point argument
formatted as octal rational fraction x / y.
.TP
\fBx\fR
Format next argument in hexadecimal format, with leading '0x'; floating 
point formatted argument as hexadecimal rational fraction x / y.
.TP
\fBb\fR
Format next argument in binary format, with leading '0b'; floating
point argument formatted as binary rational fraction x / y.
.TP
\fBs\fR
Format next argument as string.
.TP
\fBc\fR
Format next argument as single character value.
.TP
\fB%\fR
Format single literal %.
.PP
\fBOther characters in format string\fR
.TP
\fB\\n\fR
Format ASCII newline.
.TP
\fB\\r\fR
Format ASCII carriage return.
.TP
\fB\\t\fR
Format ASCII tab.
.TP
\fB\\f\fR
Format ASCII form feed.
.TP
\fB\\v\fR
Format ASCII vertical tab.
.TP
\fB\\b\fR
Format ASCII backspace.
.sp
.SH NOTES
.PP
Mpexpr is based on Tcl 7.6 'tclExpr.c' and David Bell's 'Calc' program.
This man page is largely borrowed from Tcl 7.6 as well, as is the 
mpexpr test suite.
.sp
See the files README and INSTALL for additional information.
.sp
Tcl 7.6 is Copyright (c) 1987-1994 The Regents of the University of 
California and Copyright (c) 1994 Sun Microsystems, Inc.
.sp
Calc is Copyright (c) 1994 David I. Bell.
.sp
.SH AUTHOR
Tom Poindexter, tpoindex@nyx.net, Talus Technologies, Inc., Highlands Ranch, CO.
http://www.nyx.net/~tpoindex
.sp
Version 1.0 released November, 1998.
.sp
Copyright 1998 Tom Poindexter.  See the file 'LICENSE.TERMS' for
additional copyright and licensing terms.

