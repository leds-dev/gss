/*
@(#) gss_gtacs.c 
*/

#    include <signal.h>
#    include <stdio.h>
#    include <sys/types.h>
#    include <sys/time.h>

#    include "strmutil.p"
#	 include "stars.h"
#    include "lrv_enum.h"

#    define DECLARE_STATUS_BITS
#    define NumLrvs NUM_LRVS

     static client_point	*lrv_pid[NumLrvs];
     static	client_connection	*connection_id;

     struct     timeval     client_timeout        = {120, 0};
     struct     timeval     max_interval          = {120, 0};
     static     struct      timeval zero_interval = {0,0};
     static     int         waiting               = FALSE;

     static     double             gss_time_offset=0.0;
     static     double             gss_time;
     static     int                lrv_rate[NumLrvs];
     static     int                lrv_start_mf[NumLrvs];
     typedef    double             GSS_DBL;
     typedef    int                GSS_INT;
     typedef    unsigned long      GSS_UL;
     typedef    char               GSS_STR[sizeof(double)];

/* Add byte array */
     typedef    char               GSS_BYTES[8];

     typedef union {
             GSS_DBL dval;
             GSS_UL uval;
             GSS_INT ival;
             GSS_STR sval;
             GSS_BYTES bval;
             } VALUE;

     typedef    VALUE              LRV_BUF[ NumLrvs ];
     static     LRV_BUF            tm_data;         /* values from GTACS */
     static     LRV_BUF            sim_tm;         /* values for main sim */
	
	 int mnf_cnt = 0;

     int i;

     SIM_DATA_TYPE sim_data; 

     static void gss_pseudo_nap(long ms); 
	
/*============================================================================ */
/* Get GSS time offset from TCL */

void Set_GSS_TimeOffset(double TimeOffset)
{
	gss_time_offset = TimeOffset;
}

/*============================================================================= */
/* Get lrv's rate from TCL (INI file) */
/* 02/21/01 - Ryan Telkamp, Added start_mf */

void Set_gtacs_lrv_rate(int lrv_indx, int start_mf, int rate)
{

        lrv_start_mf[lrv_indx] = start_mf;
        
	lrv_rate[lrv_indx] = rate;

}
/*============================================================================= */
/* Update GSS sim_data */


void Update_sim_data()
{
    int	str_tst;
    
    sim_data.st[0].vt[0].hv.h = *( GSS_DBL *)( &sim_tm[0] );
    sim_data.st[0].vt[0].hv.v = *( GSS_DBL *)( &sim_tm[1] );
    sim_data.st[0].vt[0].mag = *( GSS_DBL *)( &sim_tm[2] );
    sim_data.st[0].vt[0].tm_osc_id = *( GSS_INT *)( &sim_tm[3] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[4] ),"V", 1);
    sim_data.st[0].vt[0].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[0].vt[0].valid = 1;

    }
/*    sim_data.st[0].vt[0].valid = *( GSS_INT *)( &sim_tm[4] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[5] ),"T", 1);
    sim_data.st[0].vt[0].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[0].vt[0].tracked = 1;

    }
/*    sim_data.st[0].vt[0].tracked = *( GSS_INT *)( &sim_tm[5] );*/

    sim_data.st[0].vt[1].hv.h = *( GSS_DBL *)( &sim_tm[6] );
    sim_data.st[0].vt[1].hv.v = *( GSS_DBL *)( &sim_tm[7] );
    sim_data.st[0].vt[1].mag = *( GSS_DBL *)( &sim_tm[8] );
    sim_data.st[0].vt[1].tm_osc_id = *( GSS_INT *)( &sim_tm[9] );
    
    str_tst = strncmp(*( GSS_STR *)( &sim_tm[10] ),"V", 1);
    sim_data.st[0].vt[1].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[0].vt[1].valid = 1;

    }
/*    sim_data.st[0].vt[1].valid = *( GSS_INT *)( &sim_tm[10] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[11] ),"T", 1);
    sim_data.st[0].vt[1].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[0].vt[1].tracked = 1;

    }
/*    sim_data.st[0].vt[1].tracked = *( GSS_INT *)( &sim_tm[11] );*/

    sim_data.st[0].vt[2].hv.h = *( GSS_DBL *)( &sim_tm[12] );
    sim_data.st[0].vt[2].hv.v = *( GSS_DBL *)( &sim_tm[13] );
    sim_data.st[0].vt[2].mag = *( GSS_DBL *)( &sim_tm[14] );
    sim_data.st[0].vt[2].tm_osc_id = *( GSS_INT *)( &sim_tm[15] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[16] ),"V", 1);
    sim_data.st[0].vt[2].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[0].vt[2].valid = 1;

    }
/*    sim_data.st[0].vt[2].valid = *( GSS_INT *)( &sim_tm[16] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[17] ),"T", 1);
    sim_data.st[0].vt[2].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[0].vt[2].tracked = 1;

    }
/*    sim_data.st[0].vt[2].tracked = *( GSS_INT *)( &sim_tm[17] );*/

    sim_data.st[0].vt[3].hv.h = *( GSS_DBL *)( &sim_tm[18] );
    sim_data.st[0].vt[3].hv.v = *( GSS_DBL *)( &sim_tm[19] );
    sim_data.st[0].vt[3].mag = *( GSS_DBL *)( &sim_tm[20] );
    sim_data.st[0].vt[3].tm_osc_id = *( GSS_INT *)( &sim_tm[21] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[22] ),"V", 1);
    sim_data.st[0].vt[3].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[0].vt[3].valid = 1;

    }
/*    sim_data.st[0].vt[3].valid = *( GSS_INT *)( &sim_tm[22] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[23] ),"T", 1);
    sim_data.st[0].vt[3].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[0].vt[3].tracked = 1;

    }
/*    sim_data.st[0].vt[3].tracked = *( GSS_INT *)( &sim_tm[23] );*/

    sim_data.st[0].vt[4].hv.h = *( GSS_DBL *)( &sim_tm[24] );
    sim_data.st[0].vt[4].hv.v = *( GSS_DBL *)( &sim_tm[25] );
    sim_data.st[0].vt[4].mag = *( GSS_DBL *)( &sim_tm[26] );
    sim_data.st[0].vt[4].tm_osc_id = *( GSS_INT *)( &sim_tm[27] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[28] ),"V", 1);
    sim_data.st[0].vt[4].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[0].vt[4].valid = 1;

    }
/*    sim_data.st[0].vt[4].valid = *( GSS_INT *)( &sim_tm[28] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[29] ),"T", 1);
    sim_data.st[0].vt[4].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[0].vt[4].tracked = 1;

    }
/*    sim_data.st[0].vt[4].tracked = *( GSS_INT *)( &sim_tm[29] );*/

    sim_data.st[1].vt[0].hv.h = *( GSS_DBL *)( &sim_tm[30] );
    sim_data.st[1].vt[0].hv.v = *( GSS_DBL *)( &sim_tm[31] );
    sim_data.st[1].vt[0].mag = *( GSS_DBL *)( &sim_tm[32] );
    sim_data.st[1].vt[0].tm_osc_id = *( GSS_INT *)( &sim_tm[33] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[34] ),"V", 1);
    sim_data.st[1].vt[0].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[1].vt[0].valid = 1;

    }
/*    sim_data.st[1].vt[0].valid = *( GSS_INT *)( &sim_tm[34] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[35] ),"T", 1);
    sim_data.st[1].vt[0].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[1].vt[0].tracked = 1;

    }
/*    sim_data.st[1].vt[0].tracked = *( GSS_INT *)( &sim_tm[35] );*/

    sim_data.st[1].vt[1].hv.h = *( GSS_DBL *)( &sim_tm[36] );
    sim_data.st[1].vt[1].hv.v = *( GSS_DBL *)( &sim_tm[37] );
    sim_data.st[1].vt[1].mag = *( GSS_DBL *)( &sim_tm[38] );
    sim_data.st[1].vt[1].tm_osc_id = *( GSS_INT *)( &sim_tm[39] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[40] ),"V", 1);
    sim_data.st[1].vt[1].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[1].vt[1].valid = 1;

    }
/*    sim_data.st[1].vt[1].valid = *( GSS_INT *)( &sim_tm[40] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[41] ),"T", 1);
    sim_data.st[1].vt[1].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[1].vt[1].tracked = 1;

    }
/*    sim_data.st[1].vt[1].tracked = *( GSS_INT *)( &sim_tm[41] );*/

    sim_data.st[1].vt[2].hv.h = *( GSS_DBL *)( &sim_tm[42] );
    sim_data.st[1].vt[2].hv.v = *( GSS_DBL *)( &sim_tm[43] );
    sim_data.st[1].vt[2].mag = *( GSS_DBL *)( &sim_tm[44] );
    sim_data.st[1].vt[2].tm_osc_id = *( GSS_INT *)( &sim_tm[45] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[46] ),"V", 1);
    sim_data.st[1].vt[2].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[1].vt[2].valid = 1;

    }
/*    sim_data.st[1].vt[2].valid = *( GSS_INT *)( &sim_tm[46] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[47] ),"T", 1);
    sim_data.st[1].vt[2].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[1].vt[2].tracked = 1;

    }
/*    sim_data.st[1].vt[2].tracked = *( GSS_INT *)( &sim_tm[47] );*/

    sim_data.st[1].vt[3].hv.h = *( GSS_DBL *)( &sim_tm[48] );
    sim_data.st[1].vt[3].hv.v = *( GSS_DBL *)( &sim_tm[49] );
    sim_data.st[1].vt[3].mag = *( GSS_DBL *)( &sim_tm[50] );
    sim_data.st[1].vt[3].tm_osc_id = *( GSS_INT *)( &sim_tm[51] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[52] ),"V", 1);
    sim_data.st[1].vt[3].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[1].vt[3].valid = 1;

    }
/*    sim_data.st[1].vt[3].valid = *( GSS_INT *)( &sim_tm[52] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[53] ),"T", 1);
    sim_data.st[1].vt[3].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[1].vt[3].tracked = 1;

    }
/*    sim_data.st[1].vt[3].tracked = *( GSS_INT *)( &sim_tm[53] );*/

    sim_data.st[1].vt[4].hv.h = *( GSS_DBL *)( &sim_tm[54] );
    sim_data.st[1].vt[4].hv.v = *( GSS_DBL *)( &sim_tm[55] );
    sim_data.st[1].vt[4].mag = *( GSS_DBL *)( &sim_tm[56] );
    sim_data.st[1].vt[4].tm_osc_id = *( GSS_INT *)( &sim_tm[57] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[58] ),"V", 1);
    sim_data.st[1].vt[4].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[1].vt[4].valid = 1;

    }
/*    sim_data.st[1].vt[4].valid = *( GSS_INT *)( &sim_tm[58] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[59] ),"T", 1);
    sim_data.st[1].vt[4].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[1].vt[4].tracked = 1;

    }
/*    sim_data.st[1].vt[4].tracked = *( GSS_INT *)( &sim_tm[59] );*/

    sim_data.st[2].vt[0].hv.h = *( GSS_DBL *)( &sim_tm[60] );
    sim_data.st[2].vt[0].hv.v = *( GSS_DBL *)( &sim_tm[61] );
    sim_data.st[2].vt[0].mag = *( GSS_DBL *)( &sim_tm[62] );
    sim_data.st[2].vt[0].tm_osc_id = *( GSS_INT *)( &sim_tm[63] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[64] ),"V", 1);
    sim_data.st[2].vt[0].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[2].vt[0].valid = 1;

    }
/*    sim_data.st[2].vt[0].valid = *( GSS_INT *)( &sim_tm[64] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[65] ),"T", 1);
    sim_data.st[2].vt[0].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[2].vt[0].tracked = 1;

    }
/*    sim_data.st[2].vt[0].tracked = *( GSS_INT *)( &sim_tm[65] );*/

    sim_data.st[2].vt[1].hv.h = *( GSS_DBL *)( &sim_tm[66] );
    sim_data.st[2].vt[1].hv.v = *( GSS_DBL *)( &sim_tm[67] );
    sim_data.st[2].vt[1].mag = *( GSS_DBL *)( &sim_tm[68] );
    sim_data.st[2].vt[1].tm_osc_id = *( GSS_INT *)( &sim_tm[69] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[70] ),"V", 1);
    sim_data.st[2].vt[1].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[2].vt[1].valid = 1;

    }
/*    sim_data.st[2].vt[1].valid = *( GSS_INT *)( &sim_tm[70] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[71] ),"T", 1);
    sim_data.st[2].vt[1].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[2].vt[1].tracked = 1;

    }
/*    sim_data.st[2].vt[1].tracked = *( GSS_INT *)( &sim_tm[71] );*/

    sim_data.st[2].vt[2].hv.h = *( GSS_DBL *)( &sim_tm[72] );
    sim_data.st[2].vt[2].hv.v = *( GSS_DBL *)( &sim_tm[73] );
    sim_data.st[2].vt[2].mag = *( GSS_DBL *)( &sim_tm[74] );
    sim_data.st[2].vt[2].tm_osc_id = *( GSS_INT *)( &sim_tm[75] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[76] ),"V", 1);
    sim_data.st[2].vt[2].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[2].vt[2].valid = 1;

    }
/*    sim_data.st[2].vt[2].valid = *( GSS_INT *)( &sim_tm[76] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[77] ),"T", 1);
    sim_data.st[2].vt[2].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[2].vt[2].tracked = 1;

    }
/*    sim_data.st[2].vt[2].tracked = *( GSS_INT *)( &sim_tm[77] );*/

    sim_data.st[2].vt[3].hv.h = *( GSS_DBL *)( &sim_tm[78] );
    sim_data.st[2].vt[3].hv.v = *( GSS_DBL *)( &sim_tm[79] );
    sim_data.st[2].vt[3].mag = *( GSS_DBL *)( &sim_tm[80] );
    sim_data.st[2].vt[3].tm_osc_id = *( GSS_INT *)( &sim_tm[81] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[82] ),"V", 1);
    sim_data.st[2].vt[3].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[2].vt[3].valid = 1;

    }
/*    sim_data.st[2].vt[3].valid = *( GSS_INT *)( &sim_tm[82] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[83] ),"T", 1);
    sim_data.st[2].vt[3].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[2].vt[3].tracked = 1;

    }
/*    sim_data.st[2].vt[3].tracked = *( GSS_INT *)( &sim_tm[83] );*/

    sim_data.st[2].vt[4].hv.h = *( GSS_DBL *)( &sim_tm[84] );
    sim_data.st[2].vt[4].hv.v = *( GSS_DBL *)( &sim_tm[85] );
    sim_data.st[2].vt[4].mag = *( GSS_DBL *)( &sim_tm[86] );
    sim_data.st[2].vt[4].tm_osc_id = *( GSS_INT *)( &sim_tm[87] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[88] ),"V", 1);
    sim_data.st[2].vt[4].valid = 0;
    if ( str_tst == 0 ) {
        sim_data.st[2].vt[4].valid = 1;

    }
/*    sim_data.st[2].vt[4].valid = *( GSS_INT *)( &sim_tm[88] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[89] ),"T", 1);
    sim_data.st[2].vt[4].tracked = 0;
    if ( str_tst == 0 ) {
        sim_data.st[2].vt[4].tracked = 1;

    }
/*    sim_data.st[2].vt[4].tracked = *( GSS_INT *)( &sim_tm[89] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[90] ),"O", 1);
    if ( str_tst == 0 ) {
        sim_data.st[1].st_fov.power = 0;

    }

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[90] ),"C", 1);
    if ( str_tst == 0 ) {
        sim_data.st[1].st_fov.power = 1;

    }

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[90] ),"R", 1);
    if ( str_tst == 0 ) {
        sim_data.st[1].st_fov.power = 2;

    }

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[90] ),"U", 1);
    if ( str_tst == 0 ) {
        sim_data.st[1].st_fov.power = 3;

    }
/*    sim_data.st[1].st_fov.power = *( GSS_INT *)( &sim_tm[90] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[91] ),"F", 1);
    sim_data.st[1].st_fov.mode = 0;
    if ( str_tst == 0 ) {
        sim_data.st[1].st_fov.mode = 1;

    }
/*    sim_data.st[1].st_fov.mode = *( GSS_INT *)( &sim_tm[91] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[92] ),"O", 1);
    if ( str_tst == 0 ) {
        sim_data.st[2].st_fov.power = 0;

    }

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[92] ),"C", 1);
    if ( str_tst == 0 ) {
        sim_data.st[2].st_fov.power = 1;

    }

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[92] ),"R", 1);
    if ( str_tst == 0 ) {
        sim_data.st[2].st_fov.power = 2;

    }

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[92] ),"U", 1);
    if ( str_tst == 0 ) {
        sim_data.st[2].st_fov.power = 3;

    }
/*    sim_data.st[2].st_fov.power = *( GSS_INT *)( &sim_tm[92] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[93] ),"F", 1);
    sim_data.st[2].st_fov.mode = 0;
    if ( str_tst == 0 ) {
        sim_data.st[2].st_fov.mode = 1;

    }
/*    sim_data.st[2].st_fov.mode = *( GSS_INT *)( &sim_tm[93] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[94] ),"O", 1);
    if ( str_tst == 0 ) {
        sim_data.st[3].st_fov.power = 0;

    }

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[94] ),"C", 1);
    if ( str_tst == 0 ) {
        sim_data.st[3].st_fov.power = 1;

    }

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[94] ),"R", 1);
    if ( str_tst == 0 ) {
        sim_data.st[3].st_fov.power = 2;

    }

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[94] ),"U", 1);
    if ( str_tst == 0 ) {
        sim_data.st[3].st_fov.power = 3;

    }
/*    sim_data.st[3].st_fov.power = *( GSS_INT *)( &sim_tm[94] );*/

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[95] ),"F", 1);
    sim_data.st[3].st_fov.mode = 0;
    if ( str_tst == 0 ) {
        sim_data.st[3].st_fov.mode = 1;

    }
/*    sim_data.st[3].st_fov.mode = *( GSS_INT *)( &sim_tm[95] );*/

    sim_data.tm.eci2body.quat.x = *( GSS_DBL *)( &sim_tm[96] );
    sim_data.tm.eci2body.quat.y = *( GSS_DBL *)( &sim_tm[97] );
    sim_data.tm.eci2body.quat.z = *( GSS_DBL *)( &sim_tm[98] );
    sim_data.tm.eci2body.quat.a = *( GSS_DBL *)( &sim_tm[99] );

	/* update the direction cosine matrix and the inverse */
	rot_set_q (&sim_data.tm.eci2body, &sim_data.tm.eci2body.quat);

    sim_data.tm.ephem[0] = *( GSS_DBL *)( &sim_tm[100] );
    sim_data.tm.ephem[1] = *( GSS_DBL *)( &sim_tm[101] );
    sim_data.tm.ephem[2] = *( GSS_DBL *)( &sim_tm[102] );
    sim_data.tm.ephem[3] = *( GSS_DBL *)( &sim_tm[103] );
    sim_data.tm.ephem[4] = *( GSS_DBL *)( &sim_tm[104] );
    sim_data.tm.ephem[5] = *( GSS_DBL *)( &sim_tm[105] );
    sim_data.tm.ephem[6] = *( GSS_DBL *)( &sim_tm[106] );

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[107] ),"V", 1);
    sim_data.tm.ephem_valid = FALSE;
    if ( str_tst == 0 ) {
        sim_data.tm.ephem_valid = TRUE;

    }

    str_tst = strncmp(*( GSS_STR *)( &sim_tm[109] ),"U", 1);
    sim_data.tm.yaw_flip = TRUE;
    if ( str_tst == 0 ) {
        sim_data.tm.yaw_flip = FALSE;

    }


/*    sim_data.tm.ephem_valid = *( GSS_DBL *)( &sim_tm[107] ); */
/*    sim_data.tm.mjfttg.days = *( GSS_INT *)( &sim_tm[108] ); */
/*    sim_data.tm.mjfttg.frac_day = *( GSS_DBL *)( &sim_tm[109] ); */

    mnf_cnt = *( GSS_INT *)( &sim_tm[110] );
    
    
}

/*============================================================================= */


void  gss_gtacs_lrv_callback(client_point *cpid)
{

/*******************************************************************************

Purpose:

        This routine performs the task of transferring the updated LRV information
      from the GTACS communication buffer to the GSS's LRV holding buffer.

Origin:

        Written by R. Telkamp, 05/30/00

History:

        MM/DD/YY - REA, Reason for modification

*******************************************************************************/

     int     indx;

     if (cpid->status_bits &
          (point_status_bit_mask[STALE] | point_status_bit_mask [QUESTIONABLE]))
         {
         return;
         }

     indx = (int)cpid->user_data;
     switch (cpid->data_type) {
          case U_LONG:
               tm_data[indx].uval = *(int *)cpid->value;
               break;
          case DOUBLE:
               tm_data[indx].dval = *(double *)cpid->value;
               break;
          case STRING:
                strncpy (tm_data[indx].sval, cpid->value, 7);
/*               tm_data[indx].sval = cpid->value;*/
               break;
          case TIMEVAL: {
               struct timeval * tv = (struct timeval *) cpid->value;
               tm_data[indx].uval = tv->tv_sec;
               break;
          }
     }

}

/* =========================================================================== */
void  gss_update_gss_time_callback(client_point *cpid)
{

/*******************************************************************************

Purpose:

        This routine performs the task of updating the GSS time variable from
      the telemetry minor frame receipt time.

Origin:

        Written by R. Telkamp, 05/30/00

History:

        MM/DD/YY - REA, Reason for modification

*******************************************************************************/

     if (cpid->status_bits &
          (point_status_bit_mask[STALE] | point_status_bit_mask [QUESTIONABLE]))
     return;

     gss_time = gss_time_offset + (*(double *)cpid->value);
}

/*============================================================================= */

void  gss_update_gss_lrvs_callback(client_point *cpid)
{

/*******************************************************************************

Purpose:

        This routine performs the task of transferring the updated LRV information
      from the GSS's LRV holding buffer to the GSS SIM buffer at the appropriate
      minor frame.

Origin:

        Written by R. Telkamp, 05/30/00

History:

        MM/DD/YY - REA, Reason for modification

*******************************************************************************/

     int     indx;
     int     interval;

     if (cpid->status_bits &
          (point_status_bit_mask[STALE] | point_status_bit_mask [QUESTIONABLE]))
         {
         return;
         }

     indx = (int)cpid->user_data;
     tm_data[indx].ival = *(int *)cpid->value;

     for (i=0; i<NumLrvs; i++) 
     {
     
          interval = 32 / lrv_rate[i];
          
          if ( lrv_rate[i] == 32 ) 
	  {
              sim_tm[i] = tm_data[i];
          }
          
          if ( (tm_data[indx].ival == lrv_start_mf[i]) ||
               (tm_data[indx].ival == lrv_start_mf[i] + interval) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*2)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*3)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*4)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*5)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*6)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*7)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*8)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*9)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*10)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*11)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*12)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*13)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*14)) ||
               (tm_data[indx].ival == lrv_start_mf[i] + (interval*15))    )
               
          {
               sim_tm[i] = tm_data[i];
               
          }
               
/*
          if ( (tm_data[indx].ival) / (32 / lrv_rate[i]) * (32 / lrv_rate[i])
 ==(tm_data[indx].ival) ) 
		  {
              sim_tm[i] = tm_data[i];
          }
*/

     }

     Update_sim_data();
}

/*============================================================================= */

int  gss_gtacs_connect(char * stream)
{
/*******************************************************************************

Purpose:

	This routine establishes the connection to the GOES GTACS (Epoch 2000)
      telemetry system for the Ground SIAD Software program.

Origin:

	Written by R. Telkamp, 05/30/00

History:

	MM/DD/YY - REA, reason for modification

*******************************************************************************/

/*******************************************************************************

	Connect to stream.

*******************************************************************************/
     int err;
     
     err = connect_stream (stream, client_timeout, &connection_id);
     
     if (err != 0) {
          disconnect_streams ();
          return(err);
     }

     return(0);
}

/*============================================================================= */

int  gss_gtacs_register_lrv (char * lrvname, int lrv_indx)
{
/*******************************************************************************

Purpose:

	This routine registers interest in the data for an LRV with GTACS.

Origin:

	Written by R. Telkamp, 05/30/00

History:

	MM/DD/YY - REA, reason for modification

*******************************************************************************/

/*******************************************************************************

	Request SYNC_SERVICE service for ALL_SAMPLES.

*******************************************************************************/
     int err;

     err = request_point (
                        lrvname,
                        DEFAULT_CONVERSION,
                        ALL_SAMPLES,
                        SYNC_SERVICE,
                        1,
                        client_timeout,
                        gss_gtacs_lrv_callback,
                        (void *)lrv_indx,                        /* user_data if you need it */
                        &lrv_pid[lrv_indx]);
                        

     if (err != 0) {
         disconnect_streams ();
         return (err);
     }
	
	return(0);
}

/*============================================================================= */

int  gss_gtacs_register_mfc_lrv (char * lrvname, int lrv_indx)
{
/*******************************************************************************

Purpose:

	This routine registers interest in the data for an LRV with GTACS.

Origin:

	Written by R. Telkamp, 05/30/00

History:

	MM/DD/YY - REA, reason for modification

*******************************************************************************/

/*******************************************************************************

	Request SYNC_SERVICE service for ALL_SAMPLES.

*******************************************************************************/
     int err;

     err = request_point (
                        lrvname,
                        DEFAULT_CONVERSION,
                        ALL_SAMPLES,
                        SYNC_SERVICE,
                        0,
                        client_timeout,
                        gss_update_gss_lrvs_callback,
                        (void *)lrv_indx,     /* user_data if you need it */
                        &lrv_pid[lrv_indx]);

     if (err != 0) {
         disconnect_streams ();
         return (err);
     }
	
	return(0);
}

/*============================================================================= */

int  gss_gtacs_register_time_lrv (char * lrvname, int lrv_indx)
{
/*******************************************************************************

Purpose:

	This routine registers interest in the data for an LRV with GTACS.

Origin:

	Written by R. Telkamp, 05/30/00

History:

	MM/DD/YY - REA, reason for modification

*******************************************************************************/

/*******************************************************************************

	Request SYNC_SERVICE service for ALL_SAMPLES.

*******************************************************************************/
     int err;

     err = request_point (
                        lrvname,
                        DEFAULT_CONVERSION,
                        ALL_SAMPLES,
                        SYNC_SERVICE,
                        1,
                        client_timeout,
                        gss_update_gss_time_callback,
                        (void *)lrv_indx,                        /* user_data if you need it */
                        &lrv_pid[lrv_indx]);

     if (err != 0) {                
         disconnect_streams ();
         return (err);
     }
	
     return(0);
}

/*============================================================================= */

int  gss_gtacs_read_tm()
{
/*******************************************************************************

Purpose:

        This routine performs the GTACS read_streams() call to have the LRV values
      passed to the GSS program.

Origin:

        Written by R. Telkamp, 05/30/00

History:

        MM/DD/YY - REA, Reason for modification

*******************************************************************************/

/*******************************************************************************

        Process poll_point until interrupted.

*******************************************************************************/
     int err;
     err = read_streams (max_interval);

     if (err != 0) {
          disconnect_streams ();
          return(err);
     }
     return(0);
}

/* ========================================================================== */

void  gss_gtacs_shutdown()
{

/*******************************************************************************

Purpose:

        This routine performs the task of disconnecting GSS from the GTACS stream
      services.

Origin:

        Written by R. Telkamp, 05/30/00

History:

        MM/DD/YY - REA, Reason for modification

*******************************************************************************/

     for (i=0; i<NumLrvs; i++) {
          cancel_point (lrv_pid[i]);

     }

     disconnect_streams ();
}

/* =========================================================================== */

char *gss_get_lrv_value(char *data_type, int lrv_indx)
{
   char s[400];

	if (strcmp(data_type, "tm_data") == 0)
	{
	    sprintf(s, "%lf %lu %d %s", tm_data[lrv_indx].dval, 
                                        tm_data[lrv_indx].uval, 
		                        tm_data[lrv_indx].ival, 
                                        tm_data[lrv_indx].sval);
	    return(s);
	}
	if (strcmp(data_type, "sim_tm") == 0)
	{
             sprintf(s, "%lf %lu %d %s",  sim_tm[lrv_indx].dval, 
                                          sim_tm[lrv_indx].uval, 
                                          sim_tm[lrv_indx].ival, 
                                          sim_tm[lrv_indx].sval);
	     return(s);
	}
	else
  	     return("data_type does not exist");		
}

void gss_pseudo_set_response_cb (stol_completion_status status, char *response)
{
        waiting = FALSE;
}


int gss_pseudo_set_send(char *pseudo_name, char *type, double dvalue, int ivalue)
{
        char    dirbuf [256];
        execute_stol_args       esargs;
        execute_stol_results    *esres;
        int     wi;
        int	str_tst;
/*
                fprintf (stderr, "(set)lrv=%s, type=%s, dbl=%lf, int=%d\n", pseudo_name,type,dvalue,ivalue);
*/
        if (!connection_id)
                return  15;

        if (waiting) {
            /* Read following REQUEST_SUCCESS explaination */
            /* Is previous directive responsed yet? */
                connection_id->stol_response_cb = gss_pseudo_set_response_cb;
                for (waiting = TRUE, wi=0;  waiting && wi<1000; wi++, gss_pseudo_nap(10))
                     read_streams(zero_interval);
                if (waiting) {
/*                      fprintf (stderr,
                                "Server has not completed previous directive\n"); */
                        return  14;
                }
        }
        
        str_tst = strncmp(type,"F", 1);

        if (str_tst == 0) {
                sprintf(dirbuf, "PSEUDO SET %s %18.11E\n", pseudo_name, dvalue);

        }

        if (str_tst != 0) {
                sprintf(dirbuf, "PSEUDO SET %s %d\n", pseudo_name, ivalue);

        }

        esargs.directive = dirbuf;

        if ((esres = execute_stol_1 (&esargs, connection_id->client)) == NULL) {
                destroy_stream (connection_id);
                connection_id = (client_connection *)0;
                return  16; 
        }

        switch (esres->myerrno) {
                case REQUEST_SUCCESS: {
    /* You nedd to the following loop to make sure server finish your directive. */
    /* the following are looping 100 times, nap sleep 10 ms every time before */
    /* processing server input (the server will invoke pseudo_set_response_cb() */
    /* to clear waiting flag */
                        connection_id->stol_response_cb = gss_pseudo_set_response_cb;
                        for (waiting = TRUE, wi=0;  waiting && wi<100; wi++, gss_pseudo_nap(10))
                                read_streams(zero_interval);
    /* ..... */

                        return (0);
                }
                case STOL_BUSY: {
                      fprintf (stderr,
                                "Server has not completed previous directive.\n"); 
                        return (10);
                }
                case STOL_CONNECT_FAILED: {
                      fprintf (stderr,
                                "Server could not connect to client.\n");
                        return (11);
                }
                case STOL_WRITE_FAILED: {
                      fprintf (stderr,
                                "Server could not write back to client.\n");
                        return (12);
                }
        }
        return (13);

}

static void gss_pseudo_nap(long ms)
{
        struct timeval timeout;

        timeout.tv_sec = ms / 1000;
        timeout.tv_usec = (ms%1000)*1000L;

        select(0, NULL, NULL, NULL, &timeout);
}

/* =========================================================================== */

/* Test */

#ifdef GTACS_TEST

main (argc, argv)
        int argc;
        char *argv [];
{

#define NumLrvs 6

static char		*lrv_name[6] = {
	     "FRAME_CTR_1", "FRAME_CTR_2", "FRAME_CTR_3",
	     "NORMAL_DWELL_MODE", "MINOR_FRAME_COUNT", "FRAME_TIME" };

static  int     term_flag = FALSE;

lrv_rate[0] = 2;
lrv_rate[1] = 8;
lrv_rate[2] = 16;
lrv_rate[3] = 32;
lrv_rate[4] = 32;
lrv_rate[5] = 32;

/*******************************************************************************

        Connect to  stream.

*******************************************************************************/

     gss_gtacs_connect("smint");

     for (i=0; i<4; i++) {
         gss_gtacs_register_lrv(lrv_name[i], i);

     }
     i=4;
     gss_gtacs_register_mfc_lrv(lrv_name[i], i);

     i=5;
     gss_gtacs_register_time_lrv(lrv_name[i], i);

/*******************************************************************************

        Process until interrupted.

*******************************************************************************/

        while (!term_flag) 
	{
            gss_gtacs_read_tm();
            sleep (1);
            for (i=0; i<6; i++) 
	    {
			tm_data[i].dval, tm_data[i].uval, tm_data[i].ival, 
			tm_data[i].sval);
			sim_tm[i].dval, sim_tm[i].uval, 
			sim_tm[i].ival, sim_tm[i].sval);

            }
        }

/*******************************************************************************

        Clean up and exit.

*******************************************************************************/

        gss_gtacs_shutdown();

}

#endif /*GTACS_TEST */
