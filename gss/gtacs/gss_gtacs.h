/*******************************************************************************

    Purpose:

        Function prototypes for gss_gtacs.c.


*******************************************************************************/
#ifndef _GSS_GTACS_H
#define _GSS_GTACS_H

void Set_GSS_TimeOffset(double TimeOffset);
void Set_gtacs_lrv_rate(int lrv_indx, int start_mf, int rate);
int  gss_gtacs_connect(char * stream);
int  gss_gtacs_register_lrv (char * lrvname, int lrv_indx);
int  gss_gtacs_register_mfc_lrv (char * lrvname, int lrv_indx);
int  gss_gtacs_register_time_lrv (char * lrvname, int lrv_indx);
int  gss_gtacs_read_tm();
void  gss_gtacs_shutdown();
char *gss_get_lrv_value(char *data_type, int lrv_indx);
int gss_pseudo_set_send(char *pseudo_name, char *type, double dvalue, int ivalue);

#endif /* _GSS_GTACS_H */






