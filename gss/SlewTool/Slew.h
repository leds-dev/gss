
/*============================================================
	
	FILE NAME:
		Slew.h

	DESCRIPTION:
		This is header file of Slew.c

============================================================== */
#ifndef _SLEW_H
#define _SLEW_H

/* $Id: Slew.h,v 1.1 2000/03/22 00:00:49 jzhao Exp $ */

/* ------------------------------------------
	Macros
*/
/* -----------------------------------------
	Type define
*/
	enum SlewMode_Types
		{ SLEW_USER_DEFINED,
		  SLEW_ABOUT_SUNLINE, 
		  SLEW_SUN_SENSOR_TO_SUN,
		  SLEW_TO_EARTH_UPRIGHT,
		  SLEW_TO_EARTH_INVERTED,
		  NUM_SLEW_MODES };

	enum SlewSteer_Types
		{ SLEW_INERTIAL,
		  SLEW_SUN_FOLLOW, 
		  SLEW_EARTH_FOLLOW,
		  NUM_STEER_MODES };

/* Function Prototypes */

int Slew_AboutSunLine (DirCosMatrix_type DCM_SS2BDY,
						VECTOR_type SSBoresightSS,
                        double slew_angle,
                        VECTOR_type SteerVelECI,
                        double slew_rate,
                        QUATERNION_type Quat_Initial_Ref2BDY,
                        QUATERNION_type *Quat_Final_Ref2BDY,
                        QUATERNION_type *Quat_Initial2FinalPre,
                        QUATERNION_type *Quat_Initial2FinalPost);

int Slew_DeltaQuaternionStep (QUATERNION_type Quat_Initial2Final,
                  	       int N_steps,
                  	       QUATERNION_type *Quat_Initial2Final_Step);

int Slew_SunSensorToSun (VECTOR_type Sun_ECI,
						 DirCosMatrix_type DCM_SS2BDY,
						 VECTOR_type SSBoresightSS,
                  		 VECTOR_type SteerVelECI,
		   		  		 double slew_rate,
                         QUATERNION_type Quat_ECI2BDY,
                         QUATERNION_type *Quat_Final_ECI2BDY,
                  		 QUATERNION_type *Quat_Initial2FinalPre,
                  		 QUATERNION_type *Quat_Initial2FinalPost);

int Slew_ToEarth (VECTOR_type Earth_initial_ECI,
		   		  int orientation,
		   		  VECTOR_type NadirFacingAxis,
                  VECTOR_type SteerVelECI,
		   		  double slew_rate,
			      QUATERNION_type Quat_initial_ECI2BDY,
		   		  QUATERNION_type *Quat_Final_ECI2BDY,
                  QUATERNION_type *Quat_Initial2FinalPre,
                  QUATERNION_type *Quat_Initial2FinalPost);

int Slew_UserDefined (VECTOR_type axis,
                        double slew_angle,
                        VECTOR_type SteerVelECI,
                        double slew_rate,
                        QUATERNION_type Quat_Initial_Ref2BDY,
                        QUATERNION_type *Quat_Final_Ref2BDY,
                        QUATERNION_type *Quat_Initial2FinalPre,
                        QUATERNION_type *Quat_Initial2FinalPost);

int SlewEXE(  char *LogFileName,
			  int SlewMode,
			  int SlewSteer,
			  int NumSlewSteps,
			  double SlewRate,
			  double SlewAngle,
			  VECTOR_type SlewAxis,
			  VECTOR_type EarthPosECI,
			  VECTOR_type SunPosECI,
			  VECTOR_type EarthVelECI,
			  VECTOR_type SunVelECI,
			  VECTOR_type NadirFacingAxis,
			  QUATERNION_type Quat_Ref2BDY,
			  DirCosMatrix_type DCM_SS2BDY,
			  QUATERNION_type *Quat_Final,
			  QUATERNION_type *Quat_Initial2FinalBDY,
			  QUATERNION_type *Quat_Step_Post_Mult,
			  QUATERNION_type *Quat_Step_Pre_Mult);

/******************************************************************************
	Change Log:
	$Log: Slew.h,v $
	Revision 1.1  2000/03/22 00:00:49  jzhao
	Initial revision.
	
	
******************************************************************************/

#endif /* _SLEW_H */
