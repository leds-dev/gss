/* ========================================================================
	FILE NAME:
		SlewTest.c	

	DESCRIPTION:
		This is test Driver of the slew code (Slew and CIS_Slew)

	FUNCTION LIST:
						

 ==========================================================================

	REVISION HISTORY

 ========================================================================== */

 /* System Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Self Defined Headers */
#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <MessageIO.h>
#include <CISGlobal.h> 
#include <ephem_intf.h>
#include <StarTracker.h>
#include <StarCatalog.h>
#include <AcqParam.h>
#include <IDentifyStars.h>
#include <ChooseOSC.h>
#include <CIS_Slew.h>
#include <CISExeType.h>
#include <CISExe.h>
#include <Slew.h>

int	counter;

FILE *LogFp;
FILE *TestOutFp;
int TestCount=0;

char *strcpy (), *strncat ();

char    FileName_beginning[] = {"SlewResults_"};
char    FileName_ending[] = {".m"};
char    FileName[100];

char LogFileName[]={"SlewLog.Log"};

static char counter_string [] =
  {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
   'B', 'C', 'D', 'E', 'F'};

struct	Slew_mode_strings  {
	  char	entry[31];
	};

/* ======================================================= */
void main()
{
	int		TERMINATE=8;

	int		end_loop, i, orientation, N_steps, answer; 
	int		Slew_mode, prev_Slew_mode;
    int		Slew_steer;
	int		Tracker1_index, Tracker2_index;
    int		Intrusion[361][MAX_EPHEM_OBJS];

	static struct Slew_mode_strings	Slew_modes[5] = {
			  {"User-Defined Slew"},
			  {"Slew Sun Sensor to Sun"},
			  {"Slew about Sunline"},
			  {"Slew to Earth (Upright)"},
			  {"Slew to Earth (Inverted)"}};

	double		one_over_sqrt_2, slew_recommended_angle;
	double		AttitudeUncertainty, AttitudeUncertainty_deg;
	double		plus_slew_angle, minus_slew_angle;
	double		upright_slew_angle=0, inverted_slew_angle=0;
	double		slew_angle_deg, slew_angle;
	double		slew_rate, slew_rate_deg_per_s;
	double		mag, epsilon, epsilon_deg;

	VECTOR_type SSBoresightSS={1,0,0};
	VECTOR_type	SunPosECI, EarthPosECI, Moon_ECI;
	VECTOR_type	SunVelECI, EarthVelECI;
	VECTOR_type	SSBoresightBDY;
	VECTOR_type	STBoresightST;
	VECTOR_type	position_body, position_ECI[100], NadirFacingAxis;
	VECTOR_type	slew_axis;

	QUATERNION_type		Quat_Initial_BDY2ECI;
	QUATERNION_type		Quat_Initial_Ref2BDY;
	QUATERNION_type		Quat_Final;
	QUATERNION_type		Quat_Initial2Final;
	QUATERNION_type		Quat_Step_Post_Mult;
	QUATERNION_type		Quat_Step_Pre_Mult;

	DirCosMatrix_type	DCM_ST2BDY[NUM_ST], DCM_SS2BDY;
	DirCosMatrix_type	DCM_Initial_BDY2ECI;

	/* Constants */

	one_over_sqrt_2 = 1/sqrt(2);

	/* Star Tracker data */

	DCM_ST2BDY[0].DCM[0] =  -3.960641e-01;
	DCM_ST2BDY[0].DCM[1] =  -8.856487e-01;
	DCM_ST2BDY[0].DCM[2] =   2.424039e-01;
	DCM_ST2BDY[0].DCM[3] =  -4.055798e-01;
	DCM_ST2BDY[0].DCM[4] =   4.055798e-01;
	DCM_ST2BDY[0].DCM[5] =   8.191520e-01;
	DCM_ST2BDY[0].DCM[6] =  -8.237951e-01;
	DCM_ST2BDY[0].DCM[7] =   2.261226e-01;
	DCM_ST2BDY[0].DCM[8] =  -5.198368e-01;

	DCM_ST2BDY[1].DCM[0] =  -6.386522e-01;
	DCM_ST2BDY[1].DCM[1] =   5.153954e-01;
	DCM_ST2BDY[1].DCM[2] =   5.713938e-01;
	DCM_ST2BDY[1].DCM[3] =  -4.055798e-01;
	DCM_ST2BDY[1].DCM[4] =   4.055798e-01;
	DCM_ST2BDY[1].DCM[5] =  -8.191520e-01;
	DCM_ST2BDY[1].DCM[6] =  -6.539330e-01;
	DCM_ST2BDY[1].DCM[7] =  -7.548991e-01;
	DCM_ST2BDY[1].DCM[8] =  -4.999048e-02;

	DCM_ST2BDY[2].DCM[0] =   9.114595e-01;
	DCM_ST2BDY[2].DCM[1] =   2.469965e-01;
	DCM_ST2BDY[2].DCM[2] =   3.289899e-01;
	DCM_ST2BDY[2].DCM[3] =  -4.055798e-01;
	DCM_ST2BDY[2].DCM[4] =   4.055798e-01;
	DCM_ST2BDY[2].DCM[5] =   8.191520e-01;
	DCM_ST2BDY[2].DCM[6] =   6.889598e-02;
	DCM_ST2BDY[2].DCM[7] =  -8.800556e-01;
	DCM_ST2BDY[2].DCM[8] =   4.698463e-01;

	STBoresightST.Vec[0] = 0;
	STBoresightST.Vec[1] = 0;
	STBoresightST.Vec[2] = 1;

	/* Sun sensor data */

	DCM_SS2BDY.DCM[0] =  one_over_sqrt_2;
	DCM_SS2BDY.DCM[1] =  0;
	DCM_SS2BDY.DCM[2] =  one_over_sqrt_2;

	DCM_SS2BDY.DCM[3] =  0;
	DCM_SS2BDY.DCM[4] =  1;
	DCM_SS2BDY.DCM[5] =  0;

	DCM_SS2BDY.DCM[6] = -one_over_sqrt_2;
	DCM_SS2BDY.DCM[7] =  0;
	DCM_SS2BDY.DCM[8] =  one_over_sqrt_2;

	/* Other spacecraft data  */

	NadirFacingAxis.Vec[0] = 0;
	NadirFacingAxis.Vec[1] = 0;
	NadirFacingAxis.Vec[2] = 1;

        /* Calculate the sun sensor boresight orientation in the spacecraft
           body frame */
 
        MatrixMult(DCM_SS2BDY.DCM, SSBoresightSS.Vec, SSBoresightBDY.Vec,
                   3, 3, 1, DOUBLE_DATA);

	printf ("\nSunSensor Boresight in Body frame is: [%f %f %f]",
	    SSBoresightBDY.Vec[0],
	    SSBoresightBDY.Vec[1],
	    SSBoresightBDY.Vec[2]);

	/* Defaults */

	Quat_Initial_Ref2BDY.Quat[0] = 0;
	Quat_Initial_Ref2BDY.Quat[1] = 0;
	Quat_Initial_Ref2BDY.Quat[2] = 0;
	Quat_Initial_Ref2BDY.Quat[3] = 1;

	EarthPosECI.Vec[0] = 0;
	EarthPosECI.Vec[1] = 0;
	EarthPosECI.Vec[2] = 1;

	Moon_ECI.Vec[0] = -1;
	Moon_ECI.Vec[1] = 0;
	Moon_ECI.Vec[2] = 0;

	SunPosECI.Vec[0] = one_over_sqrt_2;
	SunPosECI.Vec[1] = 0;
	SunPosECI.Vec[2] = -one_over_sqrt_2;

	slew_rate = 0;

	/* --------------------------------------------------
		User Selection
	----------------------------------------------------- */
	
	end_loop = FALSE;

	counter = -1;
	printf("\nInput starting file index: ");
	scanf("%d", &counter);
	counter -= 1;

	while (!end_loop)  {

	  printf ("\n\nInput choice (Slew as user defines = %d;",
					SLEW_USER_DEFINED);
	  printf   ("\n              Slew sun sensor to sun = %d;",
					SLEW_SUN_SENSOR_TO_SUN);
	  printf   ("\n              Slew about SunLine = %d;",
					SLEW_ABOUT_SUNLINE);
	  printf   ("\n              Slew to Earth (Upright) = %d;",
					SLEW_TO_EARTH_UPRIGHT);
	  printf   ("\n              Slew to Earth (Inverted) = %d;",
					SLEW_TO_EARTH_INVERTED);
	  printf   ("\n              Terminate = %d: ", TERMINATE);
	  printf   ("\n");

	  scanf("%d", &Slew_mode);


	  if (Slew_mode == SLEW_USER_DEFINED)  {
	    printf("\nInput axis (body frame) of desired slew: ");
	    scanf("%lf %lf %lf", 
	          &slew_axis.Vec[0], 
		  &slew_axis.Vec[1], 
		  &slew_axis.Vec[2]);

	    printf("\nInput angle of desired slew (deg.): ");
	    scanf("%lf", &slew_angle_deg);

	    slew_angle = slew_angle_deg * DTR;

	    printf("\nInput initial Ref-to-body quaternion: ");
	    scanf("%lf %lf %lf %lf", 
	          &Quat_Initial_Ref2BDY.Quat[0], 
		  &Quat_Initial_Ref2BDY.Quat[1], 
		  &Quat_Initial_Ref2BDY.Quat[2], 
		  &Quat_Initial_Ref2BDY.Quat[3]);
	  }
	  else if (Slew_mode == SLEW_SUN_SENSOR_TO_SUN)  {

	    printf("\nInput Sun ECI: ");
	    scanf("%lf %lf %lf", 
	          &SunPosECI.Vec[0], &SunPosECI.Vec[1], &SunPosECI.Vec[2]);

	    printf("\nInput initial ECI-to-body quaternion: ");
	    scanf("%lf %lf %lf %lf", 
	          &Quat_Initial_Ref2BDY.Quat[0], 
		  &Quat_Initial_Ref2BDY.Quat[1], 
		  &Quat_Initial_Ref2BDY.Quat[2], 
		  &Quat_Initial_Ref2BDY.Quat[3]);
	  }

	  else if (Slew_mode == SLEW_ABOUT_SUNLINE)  {

	    printf("\nInput angle of desired slew (deg.): ");
	    scanf("%lf", &slew_angle_deg);

		printf("SlewAngle %f\n", slew_angle_deg);

	    slew_angle = slew_angle_deg * DTR;

	    printf("\nInput initial Ref-to-body quaternion: ");
	    scanf("%lf %lf %lf %lf", 
	          &Quat_Initial_Ref2BDY.Quat[0], 
		  &Quat_Initial_Ref2BDY.Quat[1], 
		  &Quat_Initial_Ref2BDY.Quat[2], 
		  &Quat_Initial_Ref2BDY.Quat[3]);
	  }

	  else if (Slew_mode == SLEW_TO_EARTH_UPRIGHT ||
		   Slew_mode == SLEW_TO_EARTH_INVERTED)  {

	    printf("\nInput initial ECI-to-body quaternion: ");
	    scanf("%lf %lf %lf %lf", 
	          &Quat_Initial_Ref2BDY.Quat[0], 
		  &Quat_Initial_Ref2BDY.Quat[1], 
		  &Quat_Initial_Ref2BDY.Quat[2], 
		  &Quat_Initial_Ref2BDY.Quat[3]);

	    printf("\nInput Earth ECI at start of slew: ");
	    scanf("%lf %lf %lf", 
	          &EarthPosECI.Vec[0], 
		  &EarthPosECI.Vec[1], 
		  &EarthPosECI.Vec[2]);

	    printf("\nInput Slew Rate (deg./s): ");
	    scanf("%lf", &slew_rate_deg_per_s);

	    slew_rate = slew_rate_deg_per_s * DTR;
	  }

	  else if (Slew_mode == TERMINATE)  {
	    end_loop = TRUE;
	    continue;
	  }
	  else  {
	    printf ("\nSorry - Choice not understood.  Please try again.");
	    continue;
	  }

	  printf("\nInput Number of Steps Desired: ");
	  scanf("%d", &N_steps);

	 SlewEXE(  LogFileName,
			   Slew_mode,
               Slew_steer,
			   N_steps,
		   	   slew_rate,
			   slew_angle,
		   	   slew_axis,
			   EarthPosECI,
		       SunPosECI,
			   EarthVelECI,
		       SunVelECI,
		       NadirFacingAxis,
	           Quat_Initial_Ref2BDY,
	  		   DCM_SS2BDY,
	    	   &Quat_Final,
	  		   &Quat_Initial2Final,
	  		   &Quat_Step_Post_Mult,
	   		   &Quat_Step_Pre_Mult);

	      for (i=0; i<4; i++)
	        Quat_Final.Quat[i] = 0;
	}
}

/************************************************************************
	Change Log:
	
*************************************************************************/
