/* $Id: Slew.c,v 1.2 2000/04/03 18:03:21 jzhao Exp $ */
/* ========================================================================
    FILE NAME:
        Slew.c

    DESCRIPTION:
		This file include Slew funtions used by GSS.

    FUNCTION LIST:

 ========================================================================== */

 /* System Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/* Self Defined Headers */
#include <MessageIO.h>
#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <CISGlobal.h>
#include <EPHGlobal.h>
#include <ephem_intf.h>
#include <StarTracker.h>
#include <StarCatalog.h>
#include <IDentifyStars.h>
#include <ChooseOSC.h>
#include <CISExeType.h>
#include <CISExe.h>
#include <Slew.h>
#include <TerminateFnc.h>

/* ======================================================= */

/* --------------------------------
Slew_AboutSunLine: 
		This function is used to design a slew of a given angle
		about the sun sensor line-of-sight (which is assumed to be
		pointing at the sun).  A positive angle will yield a 
		counter-clockwise slew, from the point of view of an
		observer sitting on the sun; a negative angle will yield
		a clockwise slew. The input and output attitudes are with
		respect to a "reference" frame, which may be ECI or
		orbit-reference.

Input:
	DCM_SS2BDY:	            DirCosMatrix_type. Sun sensor alignment DCM. Sun Sensor frame to Body.
	SSBoresightSS:          VECTOR_type.       Sun Sensor Boresight in Sun Sensor frame. 
   	slew_angle:	            double.            Angle of recommended slew (radians).
    SteerVelECI:            VECTOR_type.       Represents  angular velocity of steering frame, in ECI in radians/sec.
    slew_rate:              double.            Represents slew rate, in radians/sec.
	Quat_Initial_Ref2BDY:   QUATERNION_type.   Attitude.  Reference frame to Body at start of slew.

Output:
	Quat_Final_Ref2BDY:     QUATERNION_type.   Attitude.  Reference frame to Body at end of slew
	Quat_Initial2FinalPre:	QUATERNION_type.   Delta quaternion pre-multiplier (ECI rotation)
	Quat_Initial2FinalPost:	QUATERNION_type.   Delta quaternion post-multiplier (Body rotation)
*/

int Slew_AboutSunLine (DirCosMatrix_type DCM_SS2BDY,
						VECTOR_type SSBoresightSS,
   		          		double slew_angle,
						VECTOR_type SteerVelECI,
   		          		double slew_rate,
						QUATERNION_type Quat_Initial_Ref2BDY,
						QUATERNION_type *Quat_Final_Ref2BDY,
		          		QUATERNION_type *Quat_Initial2FinalPre,
		          		QUATERNION_type *Quat_Initial2FinalPost)
{
    int status=0;
    double slew_time;
    double steer_rate;
    double steer_angle;
    VECTOR_type Steer_Axis={1,0,0};
    QUATERNION_type Temp_Q;
    QUATERNION_type Norm_Q;
    VECTOR_type SSBoresightBDY;

	/* Calculate the steering rate and unit vector  */
    steer_rate = sqrt( SteerVelECI.Vec[0]* SteerVelECI.Vec[0] +
                       SteerVelECI.Vec[1]* SteerVelECI.Vec[1] +
                       SteerVelECI.Vec[2]* SteerVelECI.Vec[2] );
    if (steer_rate > 0.0 )
    {
        Steer_Axis.Vec[0] = SteerVelECI.Vec[0] / steer_rate;
        Steer_Axis.Vec[1] = SteerVelECI.Vec[1] / steer_rate;
        Steer_Axis.Vec[2] = SteerVelECI.Vec[2] / steer_rate;
    }

    /* Calculate the slew time.  */
    slew_time = fabs(slew_angle) / slew_rate;

    /* Calculate the pre-multiply ECI delta quaternion from the steering velocity and the slew time.  */
    steer_angle = slew_time * steer_rate;
    status = Quat_AxisAngle2Q(Steer_Axis, steer_angle, Quat_Initial2FinalPre);

    /* Calculate the sun sensor boresight orientation in the spacecraft body frame */
    MatrixMult(DCM_SS2BDY.DCM, SSBoresightSS.Vec, SSBoresightBDY.Vec, 3, 3, 1, DOUBLE_DATA);

    /* Calculate the post-multiply BODY delta quaternion from the axis and angle.  */
    status = Quat_AxisAngle2Q(SSBoresightBDY, slew_angle, Quat_Initial2FinalPost);

    /* Calculate final quaternion.  */
    status = QuaternionVerifyNormalize(Quat_Initial_Ref2BDY.Quat, Norm_Q.Quat, DOUBLE_DATA);
    QuaternionMult(Quat_Initial2FinalPre->Quat, Norm_Q.Quat, Temp_Q.Quat, DOUBLE_DATA);
    QuaternionMult(Temp_Q.Quat, Quat_Initial2FinalPost->Quat, Quat_Final_Ref2BDY->Quat, DOUBLE_DATA);

    return(status);
}

/* --------------------------------
Slew_DeltaQuaternionStep: 
		This function is used to derive a "sub-" delta quaternion,
		representing change in attitude for each step in a slew.
		The steps are assumed to be equally spaced.

Input:
	Quat_Initial2Final
			QUATERNION_type.  Change in attitude from start of 
			desired slew to end.
	N_steps:	scalar integer.  Number of points to be determined.

Output:
	Quat_Initial2Final_Step:	
			QUATERNION_type.  Change in attitude from i-th step
			of the slew to the (i+1)-th step.
*/

int Slew_DeltaQuaternionStep (QUATERNION_type Quat_Initial2Final,
		  					   int N_steps,
		  					   QUATERNION_type *Quat_Initial2Final_Step)
{
	int status=0;
    double angle, angle_fraction;
    VECTOR_type	axis;

	if (N_steps == 0)
	{
		memcpy(Quat_Initial2Final_Step, &Quat_Initial2Final, 
										sizeof(QUATERNION_type));
		return(status);
	}
    /* Calculate the axis and angle associated with this slew.  */

    status = Quat_Q2AxisAngle (Quat_Initial2Final, &axis, &angle);

    /* Calculate the delta quaternion for a slew through an angle 
       of [(1/N_steps) * angle].  */

    angle_fraction = angle/N_steps;
    status = Quat_AxisAngle2Q (axis, angle_fraction, Quat_Initial2Final_Step);

	return(status);
}

/* --------------------------------
Slew_SunSensorToSun: 
		This function is used to design a slew positioning the s/c
		so that the sun sensor is pointing to the sun.  The slew is
		constrained so that throughout the slew, the y-component of
		the sun position, in the s/c body frame is always in the
		range [SunBDY[1], SSBoresightBDY[1]].
		
		SunBDY[1] represents the y-component of the sun position,
		in the s/c body frame, at the start of the slew.

		SSBoresightBDY[1] represents the y-component of the sun 
		sensor line-of-sight, in the s/c body frame.

Input:
	Sun_ECI:	            VECTOR_type.       Sun vector in ECI.
	DCM_SS2BDY:	            DirCosMatrix_type. Sun sensor alignment DCM. Sun Sensor frame to body.
	SSBoresightSS:          VECTOR_type.       Sun Sensor Boresigth in Sun Sensor frame.	
    SteerVelECI:            VECTOR_type.       Represents  angular velocity of steering frame, in ECI in radians/sec.
    slew_rate:              double.            Represents slew rate, in radians/sec.
    Quat_Initial_ECI2BDY:   QUATERNION_type.   Attitude. ECI frame to Body.

Output:
	Quat_Final_ECI2BDY:	    QUATERNION_type.   Attitude.  ECI to Body at end of slew
	Quat_Initial2FinalPre:	QUATERNION_type.   Delta quaternion pre-multiplier (ECI rotation)
	Quat_Initial2FinalPost:	QUATERNION_type.   Delta quaternion post-multiplier (Body rotation)
*/

int Slew_SunSensorToSun (VECTOR_type Sun_ECI,
						 DirCosMatrix_type DCM_SS2BDY,
						 VECTOR_type SSBoresightSS,
		   				 VECTOR_type SteerVelECI,
		   				 double slew_rate,
		          		 QUATERNION_type Quat_Initial_ECI2BDY,
						 QUATERNION_type *Quat_Final_ECI2BDY,
		          		 QUATERNION_type *Quat_Initial2FinalPre,
		          		 QUATERNION_type *Quat_Initial2FinalPost)
{
	int status = 0;
    int i;
    double slew_time, slew_angle, steer_rate, steer_angle, scale_factor, tmp1, tmp2;
    double cos_slew_1_angle, cos_slew_2_angle;
    DirCosMatrix_type DCM_ECI2BDY;
	VECTOR_type Steer_Axis={1,0,0};
	VECTOR_type Slew_Axis;
    VECTOR_type slew_1_axis, slew_2_axis, Norm_V;
    VECTOR_type SSBoresightBDY, SunBDY, Sun_1_BDY;
    QUATERNION_type delta_quat_1, delta_quat_2, Quat_TempPost, Norm_Q, Temp_Q;
    QUATERNION_type *Quat_Initial2Final;

	/* Calculate the steering rate and unit vector  */
	steer_rate = sqrt( SteerVelECI.Vec[0]* SteerVelECI.Vec[0] +
					   SteerVelECI.Vec[1]* SteerVelECI.Vec[1] +
					   SteerVelECI.Vec[2]* SteerVelECI.Vec[2] );
    if (steer_rate > 0.0 )
	{
		Steer_Axis.Vec[0] = SteerVelECI.Vec[0] / steer_rate;
		Steer_Axis.Vec[1] = SteerVelECI.Vec[1] / steer_rate;
		Steer_Axis.Vec[2] = SteerVelECI.Vec[2] / steer_rate;
	}

	status = VectorVerifyNormalize (Sun_ECI.Vec, Norm_V.Vec, DOUBLE_DATA);
	status = QuaternionVerifyNormalize(Quat_Initial_ECI2BDY.Quat, Norm_Q.Quat, DOUBLE_DATA);

    /* Calculate the sun sensor boresight orientation in the spacecraft bodt frame */
    MatrixMult(DCM_SS2BDY.DCM, SSBoresightSS.Vec, SSBoresightBDY.Vec, 3, 3, 1, DOUBLE_DATA);

    /* Calculate the sun position in the spacecraft body frame */
    Quaternion2DirCosMatrix(Norm_Q.Quat, DCM_ECI2BDY.DCM, DOUBLE_DATA);
    MatrixMult(DCM_ECI2BDY.DCM, Norm_V.Vec, SunBDY.Vec, 3,3,1, DOUBLE_DATA);

    /*  The recommended slew shall be constructed using two sub-slews; 
	on a spherical map, the recommended slew will be the hypotenuse of a
	(spherical) right triangle in which the sub-slews are the legs.
	The first "leg" is a slew about the spacecraft y-axis, designed to 
	"line up" the sun and the sun sensor.  "Lining up" means that [0 1 0], 
	the sun sensor, and the sun position in the body frame, are all on 
	one geodesic.  In the (unlikely) event that this is the case prior 
	to slewing, this leg is not needed.  The vector Sun_1_BDY represents
	the sun position, in the body frame, after this leg.  */

    /* Calculate Sun_1_BDY  */
	tmp1 = 1 - SunBDY.Vec[1] * SunBDY.Vec[1];
	tmp2 = 1 - SSBoresightBDY.Vec[1] * SSBoresightBDY.Vec[1];
	if ( (tmp1 < 0.0) || (tmp2 < TINY) ) 
	{
		status = MATH_ERROR;  
		return(status);
	}
    scale_factor = sqrt( tmp1/tmp2 ); 
 
    Sun_1_BDY.Vec [0] = SSBoresightBDY.Vec[0] * scale_factor;
    Sun_1_BDY.Vec [1] = SunBDY.Vec [1];
    Sun_1_BDY.Vec [2] = SSBoresightBDY.Vec[2] * scale_factor;

    /* Using Sun_1_BDY, calculate the first leg of the recommended slew  */
    cos_slew_1_angle = DotProduct(SunBDY.Vec, Sun_1_BDY.Vec, DOUBLE_DATA);
 
    if (cos_slew_1_angle > 1 - TINY)
      QuaternionIdent(delta_quat_1.Quat, DOUBLE_DATA);
    else {
      slew_1_axis.Vec [0] = 0;
      slew_1_axis.Vec [1] = 1;
      slew_1_axis.Vec [2] = 0;
      QuatTRIAD (SunBDY.Vec, Sun_1_BDY.Vec, slew_1_axis.Vec, slew_1_axis.Vec, delta_quat_1.Quat);
    }
 
    /*  The second leg is the "optimal" sub-slew moving the sun position in
	the body frame from Sun_1_BDY to the position of the sun sensor.
	"Optimal" means that it is the shortest slew possible in which that
	can be done.  The slew axis, in the body frame, will be perpendicular
	to both the Sun_1_BDY vector, and the sun sensor position vector.  */

    cos_slew_2_angle = DotProduct(Sun_1_BDY.Vec, SSBoresightBDY.Vec, DOUBLE_DATA);
 
    if (cos_slew_2_angle > 1 - TINY)
      QuaternionIdent(delta_quat_2.Quat, DOUBLE_DATA);
    else {
      CrossProduct(Sun_1_BDY.Vec, SSBoresightBDY.Vec, slew_2_axis.Vec, DOUBLE_DATA);
      QuatTRIAD (Sun_1_BDY.Vec, SSBoresightBDY.Vec, slew_2_axis.Vec, slew_2_axis.Vec, delta_quat_2.Quat);
    }
 
    /* The recommended slew will be the combination of both legs calculated
       above.  It will be the hypotenuse of the (spherical) right triangle
       formed by its two composite slews.  */
    QuaternionMult(delta_quat_1.Quat, delta_quat_2.Quat, Quat_TempPost.Quat, DOUBLE_DATA);

    /* Convert the delta quaternion into canonical form (non-negative scalar element). */
    if (Quat_TempPost.Quat[3] < 0)
	for (i=0; i<4; i++) Quat_TempPost.Quat[i] *= -1;
    memcpy (Quat_Initial2FinalPost, &Quat_TempPost, sizeof(QUATERNION_type));
	
    /* Compute the slew time from the Post-Multiply quat */
	status = Quat_Q2AxisAngle(Quat_TempPost, &Slew_Axis, &slew_angle);
	slew_time = fabs (slew_angle / slew_rate);

	/* Compute the Pre-Multiply quat from the slew time. A first order correction only
       since the ACS does not implement a sun-steering frame */
	steer_angle = slew_time * steer_rate;
	status = Quat_AxisAngle2Q(Steer_Axis, steer_angle, Quat_Initial2FinalPre);

    /* Calculate the ECI-to-body quaternion at slew completion */
    QuaternionMult (Norm_Q.Quat, Quat_Initial2FinalPost->Quat, Temp_Q.Quat, DOUBLE_DATA);
    QuaternionMult (Quat_Initial2FinalPre->Quat, Temp_Q.Quat, Quat_Final_ECI2BDY->Quat, DOUBLE_DATA);

    return(status);
}

/* --------------------------------
Slew_ToEarth: 
		This function is used to design a slew from an arbitrary
		initial orientation to an orientation with Earth's
		center on the s/c axis, NadirFacingAxis, and the s/c y-axis
		perpendicular to the orbit plane.  (The calling
		function contains a parameter defining whether the
		y-axis points "above" or "below" that plane; i.e.,
		whether the s/c is in an UPRIGHT or INVERTED position.)
		This function takes the motion of Earth (in the s/c
		body frame) into account
		
Input:
	Earth_Initial_ECI:   	VECTOR_type.      Represents position of Earth in ECI frame at start of slew.
	orientation:            integer.          UPRIGHT indicates that the wing is South; INVERTED North.
	NadirFacingAxis:        VECTOR_type.      Represents s/c body fixed vector to be pointed to Earth's center.
    SteerVelECI:            VECTOR_type.      Represents  angular velocity of steering frame, in ECI in radians/sec.
    slew_rate:              double.           Represents slew rate relative to the steering frame, in radians/sec.
	Quat_Initial_ECI2BDY:   QUATERNION_type.  Attitude. ECI to Body at start of slew 

Output:
	Quat_Final_ECI2BDY:   	QUATERNION_type.  Attitude.  ECI to Body at end of slew
	Quat_Initial2FinalPre:	QUATERNION_type.  Delta quaternion pre-multiplier (ECI rotation)
	Quat_Initial2FinalPost:	QUATERNION_type.  Delta quaternion post-multiplier (Body rotation)
*/

int Slew_ToEarth (  VECTOR_type Earth_Initial_ECI,
		   			int orientation,
		   			VECTOR_type NadirFacingAxis,
		   			VECTOR_type SteerVelECI,
		   			double slew_rate,
					QUATERNION_type Quat_Initial_ECI2BDY,
		   			QUATERNION_type *Quat_Final_ECI2BDY,
		   			QUATERNION_type *Quat_Initial2FinalPre,
		   			QUATERNION_type *Quat_Initial2FinalPost)
{
	int status = 0;
    int i, j;
	double steer_rate, steer_angle, slew_angle, slew_time;
	VECTOR_type Steer_Axis={1,0,0};
	VECTOR_type Slew_Axis;
    VECTOR_type DesiredWingECI, Earth_Final_ECI, Earth_ECI;
    QUATERNION_type Quat_Initial_BDY2ECI, Quat_TempPost, Norm_Q, Temp_Q;
    VECTOR_type NorthECI = {0, 0, 1}, SouthECI = {0, 0, -1};
    VECTOR_type WingBDY = {0, -1, 0};

	if (slew_rate <= TINY) 
	{
		memcpy(Quat_Initial2FinalPost, &Quat_Initial_ECI2BDY, sizeof(QUATERNION_type));
		return(status);
	}

	/* Calculate the steering rate and unit vector  */
	steer_rate = sqrt( SteerVelECI.Vec[0]* SteerVelECI.Vec[0] +
					   SteerVelECI.Vec[1]* SteerVelECI.Vec[1] +
					   SteerVelECI.Vec[2]* SteerVelECI.Vec[2] );
    if (steer_rate > 0.0 )
	{
		Steer_Axis.Vec[0] = SteerVelECI.Vec[0] / steer_rate;
		Steer_Axis.Vec[1] = SteerVelECI.Vec[1] / steer_rate;
		Steer_Axis.Vec[2] = SteerVelECI.Vec[2] / steer_rate;
	}

    /* Initialize inverse initial quaternion  */
	status = QuaternionVerifyNormalize(Quat_Initial_ECI2BDY.Quat, Norm_Q.Quat, DOUBLE_DATA);
	QuaternionInvers(Norm_Q.Quat,Quat_Initial_BDY2ECI.Quat, DOUBLE_DATA);

    /* Compute desired ECI orientation of solar array axis  */
    if (orientation == UPRIGHT)
      memcpy (&DesiredWingECI, &SouthECI, sizeof(VECTOR_type));
    else
      memcpy (&DesiredWingECI, &NorthECI, sizeof(VECTOR_type));

	/* Initialize Nadir ECI location */
	status = VectorVerifyNormalize(Earth_Initial_ECI.Vec, Earth_ECI.Vec,
									DOUBLE_DATA);

	/* Initialize Final quaternion, assuming no steering */
    QuatTRIAD (Earth_ECI.Vec, NadirFacingAxis.Vec,
               DesiredWingECI.Vec, WingBDY.Vec, 
		   	   Quat_Final_ECI2BDY->Quat);

    /* Calculate the post-multiply delta quaternion for the slew, assuming no steering. */
    QuaternionMult (Quat_Initial_BDY2ECI.Quat, Quat_Final_ECI2BDY->Quat,
		      		Quat_TempPost.Quat, DOUBLE_DATA);

	/* Compute the slew time from the Post-Multiple quat */
	status = Quat_Q2AxisAngle(Quat_TempPost, &Slew_Axis, &slew_angle);
	slew_time = fabs (slew_angle / slew_rate);

	/* Compute the Pre-Multiply quat from the slew time. This is exact, since ACS implements orbit steering */
	steer_angle = slew_time * steer_rate;
	status = Quat_AxisAngle2Q(Steer_Axis, steer_angle, Quat_Initial2FinalPre);

	/* Initialize Final quaternion, assuming no steering */
    QuaternionMult (Quat_Initial2FinalPre->Quat, Quat_Final_ECI2BDY->Quat,
		      		Temp_Q.Quat, DOUBLE_DATA);

	/* Copy results to output */
    memcpy (Quat_Initial2FinalPost, &Quat_TempPost, sizeof(QUATERNION_type));
    memcpy (Quat_Final_ECI2BDY, &Temp_Q, sizeof(QUATERNION_type));

	return(status);

}

/* --------------------------------
Slew_UserDefined:
		This function is used to design a slew from an known
		initial orientation to a known final orientation.  The
		orientations are with respect to a "reference" frame, which
		may be ECI or orbit-reference.

Input:
	axis:                   VECTOR_type.      Represents s/c body fixed vector that serves as slew axis.
	slew_angle:             double.           Represents slew angle, in radians.  
	SteerVelECI:            VECTOR_type.      Represents  angular velocity of steering frame, in ECI in radians/sec.
	slew_rate:              double.           Represents slew rate, in radians/sec.  
	Quat_Initial_Ref2BDY:   QUATERNION_type.  Attitude. Reference frame to Body at start of slew. 
Output:
	Quat_Final_Ref2BDY:     QUATERNION_type.  Attitude.  Reference frame to Body at end of slew.
	Quat_Initial2FinalPre:	QUATERNION_type.  Delta quaternion pre-multiplier (ECI rotation)
	Quat_Initial2FinalPost:	QUATERNION_type.  Delta quaternion post-multiplier (Body rotation)
*/


int Slew_UserDefined (VECTOR_type axis,
					  double slew_angle,
					  VECTOR_type SteerVelECI,
					  double slew_rate,
					  QUATERNION_type Quat_Initial_Ref2BDY,
					  QUATERNION_type *Quat_Final_Ref2BDY,
					  QUATERNION_type *Quat_Initial2FinalPre,
					  QUATERNION_type *Quat_Initial2FinalPost)
{
	int status=0;
    double slew_time;
    double steer_rate;
    double steer_angle;
	VECTOR_type Steer_Axis={1,0,0};
	QUATERNION_type Temp_Q;
	QUATERNION_type Norm_Q;

	/* Calculate the steering rate and unit vector  */
	steer_rate = sqrt( SteerVelECI.Vec[0]* SteerVelECI.Vec[0] +
					   SteerVelECI.Vec[1]* SteerVelECI.Vec[1] +
					   SteerVelECI.Vec[2]* SteerVelECI.Vec[2] );
    if (steer_rate > 0.0 )
	{
		Steer_Axis.Vec[0] = SteerVelECI.Vec[0] / steer_rate;
		Steer_Axis.Vec[1] = SteerVelECI.Vec[1] / steer_rate;
		Steer_Axis.Vec[2] = SteerVelECI.Vec[2] / steer_rate;
	}

	/* Calculate the slew time.  */
	slew_time = fabs(slew_angle) / slew_rate;

	/* Calculate the pre-multiply ECI delta quaternion from the steering velocity and the slew time.  */
	steer_angle = slew_time * steer_rate;
	status = Quat_AxisAngle2Q(Steer_Axis, steer_angle, Quat_Initial2FinalPre);

	/* Calculate the post-multiply BODY delta quaternion from the axis and angle.  */
	status = Quat_AxisAngle2Q(axis, slew_angle, Quat_Initial2FinalPost);

	/* Calculate final quaternion.  */
	status = QuaternionVerifyNormalize(Quat_Initial_Ref2BDY.Quat, Norm_Q.Quat, DOUBLE_DATA);
	QuaternionMult(Quat_Initial2FinalPre->Quat, Norm_Q.Quat, Temp_Q.Quat, DOUBLE_DATA);
	QuaternionMult(Temp_Q.Quat, Quat_Initial2FinalPost->Quat, Quat_Final_Ref2BDY->Quat, DOUBLE_DATA);

	return(status);
}

/* -----------------------
SlewEXE:	Slew Tool main routine.

Input:
	LogFileName:	char*. (from GUI) 
	SlewMode:	int. Mode of slew planning.(enum SlewMode_Types)(User supply).
	SlewSteer:	int. Steering Frame for slew planning.(enum SlewSteer_Types)(User supply).
	NumSlewSteps:	int. Number of steps display in slew(User Supply).
	SlewRate:	double. in rad/sec(User Supply).
	SlewAngle:	double. Slew Angle(Magnitude) in radians(User Supply).	
	SlewAxis:	VECTOR_type. Slew Axis(User Supply).
	EarthPosECI:	VECTOR_type. Earth position unit vector in ECI frame(GSS Calculated). 
	SunPosECI:		VECTOR_type. Sun position unit vector in ECI frame(from SLALIB).
	EarthVelECI:	VECTOR_type. Earth angular velocity in ECI frame relative to S/C in rad/sec (GSS Calculated). 
	SunVelECI:		VECTOR_type. Sun angular velocity in ECI frame relative to S/C in rad/sec (from SLALIB).
	NadirFacingAxis:	VECTOR_type. Nadir Facing Axis(from .ini).
	Quat_Ref2BDY: QUATERNION_type. ECI to body quaternion(from TM or CIS).
	DCM_SS2BDY: DirCosMatrix_type. Sun Sensor alignment(from .ini or CIS)

Output:
	Quat_Final:          QUATERNION_type. New orientation(to procs)
	Quat_Initial2FinalBDY:  QUATERNION_type. Delta quaternion of original
						 orientation to new orientation ignoring the steering rate (to slew display).
	Quat_Step_Post_Mult: QUATERNION_type. Delta quaternion step in body frame.
				Orientation at n step of slew to orientation at n+1 step(to slew display).
	Quat_Step_Pre_Mult: QUATERNION_type. Delta quaternion step in ECI frame.
				Orientation at n step of slew to orientation at n+1 step(to slew display).

*/

int SlewEXE(  char *LogFileName, 
			  int SlewMode,
			  int SlewSteer,
			  int NumSlewSteps,
			  double SlewRate,
			  double SlewAngle,
			  VECTOR_type SlewAxis,
			  VECTOR_type EarthPosECI,
			  VECTOR_type SunPosECI,
			  VECTOR_type EarthVelECI,
			  VECTOR_type SunVelECI,
			  VECTOR_type NadirFacingAxis,
			  QUATERNION_type Quat_Ref2BDY,
			  DirCosMatrix_type DCM_SS2BDY,
			  QUATERNION_type *Quat_Final,
			  QUATERNION_type *Quat_Initial2FinalBDY,
			  QUATERNION_type *Quat_Step_Post_Mult,
			  QUATERNION_type *Quat_Step_Pre_Mult)
{
	int status=0;
	VECTOR_type SSBoresightSS={1, 0, 0};
	VECTOR_type SSBoresightBDY;
	VECTOR_type SteerVelECI, SteerVelECIDeg;
	QUATERNION_type Quat_Initial2FinalPre;
	QUATERNION_type Quat_Initial2FinalPost;
	time_t t;

	if ( (LogFp = fopen(LogFileName, "a")) == NULL )
	{
		status = FILE_ERROR;
		DisplayMessage(status, "in SLEW");
		goto Quit;
	}

	/*-------------------------
		Log SLEW Input/Output 
	--------------------------- */
	time(&t);
	fprintf(LogFp,
	"####################################################################\n");
	fprintf(LogFp,  "#          SLEW Calculations Start at %s", ctime(&t));
	fprintf(LogFp,
	"####################################################################\n\n");

	switch(SlewSteer)
	{
		case SLEW_INERTIAL:
			 SteerVelECI.Vec[0] = 0.0;
			 SteerVelECI.Vec[1] = 0.0;
			 SteerVelECI.Vec[2] = 0.0;
			 WriteString("SLEW_INERTIAL", "SLEW_STEER");				 
			 WriteString(" ", " ");				 
			 break;

		case SLEW_SUN_FOLLOW:
			 SteerVelECI.Vec[0] = SunVelECI.Vec[0];
			 SteerVelECI.Vec[1] = SunVelECI.Vec[1];
			 SteerVelECI.Vec[2] = SunVelECI.Vec[2];
			 WriteString("SLEW_SUN_FOLLOW", "SLEW_STEER");				 
			 WriteString(" ", " ");				 
			 break;

		case SLEW_EARTH_FOLLOW:
			 SteerVelECI.Vec[0] = EarthVelECI.Vec[0];
			 SteerVelECI.Vec[1] = EarthVelECI.Vec[1];
			 SteerVelECI.Vec[2] = EarthVelECI.Vec[2];
			 WriteString("SLEW_EARTH_FOLLOW", "SLEW_STEER");				 
			 WriteString(" ", " ");				 
			 break;

		default:
			status = MODE_ERROR;
			break;
	}

    SteerVelECIDeg.Vec[0] = RTD*SteerVelECI.Vec[0];
    SteerVelECIDeg.Vec[1] = RTD*SteerVelECI.Vec[1];
    SteerVelECIDeg.Vec[2] = RTD*SteerVelECI.Vec[2];
    WriteVector(SteerVelECIDeg.Vec, "STEER_RATE (d/s)");

	if (status < 0 )
	{
	 	DisplayMessage(status, "in SLEW");
		LogMessage(status, "in SLEW");
		goto Quit;
	}

	switch(SlewMode)
	{
		case SLEW_USER_DEFINED:
			 status =  Slew_UserDefined (SlewAxis, SlewAngle, SteerVelECI, SlewRate,
										 Quat_Ref2BDY, 
			 							 Quat_Final, &Quat_Initial2FinalPre, &Quat_Initial2FinalPost);

			 WriteString("SLEW_USER_DEFINED", "SLEW_MODE");				 
			 WriteString(" ", " ");				 
			 WriteVector(SlewAxis.Vec, "SLEW_AXIS");
			 WriteDouble(SlewAngle*RTD, "SLEW_ANGLE (d)       ");
			 WriteString(" ", " ");				 
			 WriteDouble(RTD*SlewRate, "SLEW_RATE (d/s)      ");
			 WriteString(" ", " ");				 
			 WriteQuaternion(&Quat_Ref2BDY, "INITIAL SPACECRAFT QUATERNION");
			 break;

		case SLEW_ABOUT_SUNLINE:
			 status = Slew_AboutSunLine (DCM_SS2BDY, SSBoresightSS, SlewAngle, SteerVelECI, SlewRate,
										 Quat_Ref2BDY,
									 	 Quat_Final, &Quat_Initial2FinalPre, &Quat_Initial2FinalPost);
             MatrixMult(DCM_SS2BDY.DCM, SSBoresightSS.Vec, SSBoresightBDY.Vec, 3, 3, 1, DOUBLE_DATA);
			 WriteString("SLEW_ABOUT_SUNLINE", "SLEW_MODE");
			 WriteString(" ", " ");				 
			 WriteVector(SSBoresightBDY.Vec, "SUN_SENSOR_BORESIGHT");
			 WriteDouble(SlewAngle*RTD, "SLEW_ANGLE (d)       ");
			 WriteString(" ", " ");				 
			 WriteDouble(RTD*SlewRate, "SLEW_RATE (d/s)      ");
			 WriteString(" ", " ");				 
			 WriteQuaternion(&Quat_Ref2BDY, "INITIAL SPACECRAFT QUATERNION");
			 break;

		case SLEW_SUN_SENSOR_TO_SUN:
			 status = Slew_SunSensorToSun (SunPosECI, DCM_SS2BDY, SSBoresightSS, SteerVelECI, SlewRate, 
										   Quat_Ref2BDY,
			 							   Quat_Final, &Quat_Initial2FinalPre, &Quat_Initial2FinalPost);
             MatrixMult(DCM_SS2BDY.DCM, SSBoresightSS.Vec, SSBoresightBDY.Vec, 3, 3, 1, DOUBLE_DATA);
			 WriteString("SLEW_SUN_SENSOR_TO_SUN", "SLEW_MODE");
			 WriteString(" ", " ");				 
			 WriteVector(SunPosECI.Vec, "SUN_ECI");
			 WriteVector(SSBoresightBDY.Vec, "SUN_SENSOR_BORESIGHT");
			 WriteDouble(RTD*SlewRate, "SLEW_RATE (d/s)      ");
			 WriteString(" ", " ");				 
			 WriteQuaternion(&Quat_Ref2BDY, "INITIAL SPACECRAFT QUATERNION");
			 break;

		case SLEW_TO_EARTH_UPRIGHT:
			 status = Slew_ToEarth (EarthPosECI, UPRIGHT, NadirFacingAxis, SteerVelECI, SlewRate,
									Quat_Ref2BDY,
			 						Quat_Final, &Quat_Initial2FinalPre, &Quat_Initial2FinalPost);
			 WriteString("SLEW_TO_EARTH", "SLEW_MODE");
			 WriteString(" ", " ");				 
			 WriteString("UPRIGHT", "WING_ORENTATION");
			 WriteString(" ", " ");				 
			 WriteVector(EarthPosECI.Vec, "EARTH_ECI");
			 WriteVector(NadirFacingAxis.Vec, "NADIR_FACING_AXIS");
			 WriteDouble(RTD*SlewRate, "SLEW_RATE (d/s)      ");
			 WriteString(" ", " ");				 
			 WriteQuaternion(&Quat_Ref2BDY, "INITIAL SPACECRAFT QUATERNION");
			 break;

		case SLEW_TO_EARTH_INVERTED:
			 status = Slew_ToEarth (EarthPosECI, INVERTED, NadirFacingAxis, SteerVelECI, SlewRate,
									Quat_Ref2BDY,
			 						Quat_Final, &Quat_Initial2FinalPre, &Quat_Initial2FinalPost);
			 WriteString("SLEW_TO_EARTH", "SLEW_MODE");
			 WriteString(" ", " ");				 
			 WriteString("INVERTED", "WING_ORENTATION");
			 WriteString(" ", " ");				 
			 WriteVector(EarthPosECI.Vec, "EARTH_ECI");
			 WriteVector(NadirFacingAxis.Vec, "NADIR_FACING_AXIS");
			 WriteDouble(RTD*SlewRate, "SLEW_RATE (d/s)      ");
			 WriteString(" ", " ");				 
			 WriteQuaternion(&Quat_Ref2BDY, "INITIAL SPACECRAFT QUATERNION");
			 break;

		default:
			status = MODE_ERROR;
			break;
	}

	if (status < 0 )
	{
	 	DisplayMessage(status, "in SLEW");
		LogMessage(status, "in SLEW");
		goto Quit;
	}

    /* Copy the total post-multiply delta quaternion to the output variable */
    memcpy(Quat_Initial2FinalBDY, &(Quat_Initial2FinalPost), sizeof(QUAT));

    /* Write total results to log file */
	WriteQuaternion(Quat_Final, "FINAL SPACCECRAFT QUATERNION");
	WriteQuaternion(&Quat_Initial2FinalPre, "STEERING FRAME TOTAL DELTA-QUATERNION PRE-MULTIPLIER");
	WriteQuaternion(&Quat_Initial2FinalPost, "BODY FRAME TOTAL DELTA-QUATERNION POST-MULTIPLIER");
	WriteInt(NumSlewSteps, "SLEW_STEPS         ");
    WriteString(" ", " ");				 

    /* Convert total slews to step slews */
	status = Slew_DeltaQuaternionStep (Quat_Initial2FinalPre, NumSlewSteps, Quat_Step_Pre_Mult);
	status = Slew_DeltaQuaternionStep (Quat_Initial2FinalPost, NumSlewSteps, Quat_Step_Post_Mult);
	if (status < 0)
	{
		DisplayMessage(status, "in SLEW");
		LogMessage(status, "in SLEW");
		goto Quit;
	}

    /* Write step results to log file */
	WriteQuaternion(Quat_Step_Pre_Mult, "STEERING FRAME STEP DELTA-QUATERNION PRE-MULTIPLIER");
	WriteQuaternion(Quat_Step_Post_Mult, "BODY FRAME STEP DELTA-QUATERNION POST-MULTIPLIER");

	time(&t);
	fprintf(LogFp,
	"####################################################################\n");
	fprintf(LogFp, "#   SLEW Calculations End at %s", ctime(&t));
	fprintf(LogFp,
	"####################################################################\n\n");

Quit:
	fclose(LogFp);
	return(status);
}

/******************************************************************************
	Change Log:
	$Log: Slew.c,v $
	Revision 1.2  2000/04/03 18:03:21  jzhao
	Modified do Integration.
	
	Revision 1.1  2000/03/22 00:00:49  jzhao
	Initial revision.
	
	
******************************************************************************/
