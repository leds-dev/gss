#include <math.h>
#include <string.h>

#include "MathDef.h"
#include "Boundary.h"
#include "WrapAround.h"
#include "PreparePlotData.h"

void PrintPairPoints (int size, char *s, RA_DEC pos_of_pts[])
{
	int i;
	char buf[2*DATA_FORMAT_SIZE];

	if (size <= 0)
	{
		s[0] = '\0';
		return;
	}

	sprintf (s, "%10.6f %10.6f", pos_of_pts[0].ra*RTD, pos_of_pts[0].dec*RTD);

	for ( i = 1; i < size; i++)
	{
		sprintf (buf, " %10.6f %10.6f",
				 pos_of_pts[i].ra*RTD, pos_of_pts[i].dec*RTD);
		strcat (s, buf);
	}
}

void PrintOneFovDataSet (char *buf, int pts_per_side,
						   RA_DEC obj_fov_data[])
{
	char tmp_buf[DATA_FORMAT_SIZE*TOT_POINTS_PER_FOV];

	strcpy(buf, "{");

	PrintPairPoints(pts_per_side, tmp_buf, obj_fov_data);

	strcat (buf, tmp_buf);
	
	strcat (buf, "} {");

	PrintPairPoints(pts_per_side, tmp_buf, &obj_fov_data[pts_per_side]);

	strcat (buf, tmp_buf);

	strcat (buf, "} {");

	PrintPairPoints(pts_per_side, tmp_buf, &obj_fov_data[2*pts_per_side]);

	strcat (buf, tmp_buf);

	strcat (buf, "} {");

	PrintPairPoints(pts_per_side, tmp_buf, &obj_fov_data[3*pts_per_side]);

	strcat (buf, tmp_buf);

	strcat (buf, "}");
}

int PrintCircularData (char *s, double radius, double obj_in_eci[3])
{
	char 		buf[DATA_FORMAT_SIZE*TOT_POINTS_PER_OBJ+8];
	char 		small_buf[DATA_FORMAT_SIZE*8];

	RA_DEC		circle_boundary_data[TOT_POINTS_PER_OBJ];

	RA_DEC		opposite_obj_center, opposite_circle_boundary_data[TOT_POINTS_PER_OBJ];
    RA_DEC		right_boundary_data[TOT_POINTS_PER_OBJ];
	RA_DEC      left_boundary_data[TOT_POINTS_PER_OBJ];
	RA_DEC	    reordered_circle_boundary_data[TOT_POINTS_PER_OBJ];

	wrap_type 	wrap_status;

	double 		declinations[2], pole_dec, crossing_dec[2], max_dec;
    double		opposite_obj_in_eci[3], opposite_radius;
	double		right_side_declination, left_side_declination;
	double		right_90, left_90;

	int 		j, j_wrap, k, endpoints[2], dir[2], status;
	int			start_right_side_index, start_left_side_index, max_dec_index;
	int			N_right_side_pts, N_left_side_pts;

    /* Deal with the case where the entire FOV is in the circle */

    if (radius >= PI)  {
		/* This first character is the left bracket */
		strcpy(s, "{{");

		sprintf (buf, " %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f",
                         0.0, -90.0, 
                         0.0,  90.0,
                       360.0,  90.0, 
                       360.0, -90.0, 
                         0.0, -90.0); 
		strcat(s, buf);

		strcat (s, "}}");
        return 0;
    }

    /* Calculate how many poles are within the circle (wrap_status) */
	wrap_status = CircleWrapAroundPole (radius, obj_in_eci);

	/* According the wrapping cases, construct data points for plotting */
	switch (wrap_status)
	{
		case None:

	        /* Generate the data set for plotting */
	        Update_Circle_Boundary (obj_in_eci, circle_boundary_data,
							        TOT_POINTS_PER_OBJ, radius);
	
			/* This first character is the left bracket */
			strcpy(s, "{{");

			PrintPairPoints(TOT_POINTS_PER_OBJ, buf,
							circle_boundary_data);

			strcat(s, buf);

			sprintf (buf, " %10.6f %10.6f",
						  circle_boundary_data[0].ra*RTD,
						  circle_boundary_data[0].dec*RTD);
			strcat(s, buf);

			strcat(s, "}}");

			break;

		case Wrap_S_Pole: case Wrap_N_Pole:

	        /* Generate the data set for plotting */
	        Update_Circle_Boundary (obj_in_eci, circle_boundary_data,
							        TOT_POINTS_PER_OBJ, radius);
	
			/* Figure out which pole is wrapped */
			if (wrap_status == Wrap_S_Pole)
				pole_dec = -90.0;
			else
				pole_dec =  90.0;

			/* This first character is the left bracket */
			strcpy(s, "{{");

			/* Get the declination of the point with zero right ascension. */
			declinations[0] = DeclOfZeroRAPointOnWrappingCircle
							  (radius, obj_in_eci);

			if (declinations[0] == PI)
			{
				sprintf (s, 
                  "Error: anomaly occurred in computing the declination of the point with right ascension zero.\n");
				return 1;
			}
			/* Get the endpoint index just acrossing right ascension zero and
			   the direction in which the acrossing goes */
			status = EndPointIndex (TOT_POINTS_PER_OBJ,
									circle_boundary_data,
									endpoints, dir, crossing_dec);

			/* Status should be 1 --- Only one point with zero RA */
			if (status == ONE_ACROSSING)
			{

				if (endpoints[0] == TOT_POINTS_PER_OBJ-1)
				{

					PrintPairPoints(TOT_POINTS_PER_OBJ, buf,
									circle_boundary_data);
					strcat (s, buf);

					sprintf (small_buf, " %10.6f %10.6f}}",
							 circle_boundary_data[0].ra*RTD,
							 circle_boundary_data[0].dec*RTD);
				}
				else
				{

					PrintPairPoints (TOT_POINTS_PER_OBJ-endpoints[0]-1, buf,
								 	 &circle_boundary_data[endpoints[0]+1]);

					strcat (s, buf);

					strcat (s, " ");

					PrintPairPoints (endpoints[0]+1, buf, circle_boundary_data);

					strcat (s, buf);

					sprintf (small_buf, " %10.6f %10.6f}}",
							 circle_boundary_data[endpoints[0]+1].ra*RTD,
							 circle_boundary_data[endpoints[0]+1].dec*RTD);
				}

				if (dir[0] == 1)
					sprintf (buf,
							 " %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f",
						 	  360.0, declinations[0]*RTD, 360.0, pole_dec,
						      0.0, pole_dec, 0.0, declinations[0]*RTD);
				else if (dir[0] == -1)
					sprintf (buf,
							 " %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f",
						 	  0.0, declinations[0]*RTD, 0.0, pole_dec,
						      360.0, pole_dec, 360.0, declinations[0]*RTD);

				strcat(s, buf);

				strcat(s, small_buf);

			}
			else
			{
				/* Display an error message */
				sprintf (s, "Error: expecting one acrossing while status is %d\n", status);
				return 1;
				/* Not a consistent situation */
			}

			break;
		
		case Wrap_2_Pole:
		
	        /* In order for both poles to be within the circle, the radius must
               exceed PI/2.  The strategy for this case is to calculate the
               "opposite" circle - the circle forming the complement of the enclosed
			   region, and working with that.  The "opposite" circle will be centered
			   about -obj_in_eci, and have a radius of PI-radius. */

            for (j=0; j<3; j++)
              opposite_obj_in_eci[j] = -obj_in_eci[j];
			opposite_radius = PI - radius;

	        Update_Circle_Boundary (opposite_obj_in_eci, opposite_circle_boundary_data,
							        TOT_POINTS_PER_OBJ, opposite_radius);
	
			/* Determine if the opposite circle crosses the 0-right ascension
			   line.  */

			status = EndPointIndex (TOT_POINTS_PER_OBJ,
									opposite_circle_boundary_data,
									endpoints, dir, crossing_dec);

            /* If the opposite circle crosses the 0-RA line, status will
			   be 2.
			   
			   If it is tangent to 0-RA, status will be 1.

			   If neither of the above is the case, status will be 0.  */

            if (status == 2)  {

			  /* The 0-RA line will be crossed.  The circle plot will 
				 look as follows:

                 dec =  90 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                           ^                                        v
                           ^                                        v
                   pt. A   ^                                        v
                            ^                                      v
                             ^                                    v
                              ^                                  v
                              ^                                  v
                             ^                                    v
                            ^                                      v
                   pt. B   ^                                        v
                           ^                                        v
                 dec = -90 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                           |                                        |
                           RA=360                                RA=0

              */

              /* Divide the circular boundary into a curve on the
                 right side of the plot (RA just above 0), and
				 the left side of the plot (RA just below 360). */

    		  for (j=0; j<2; j++)  {
				if (dir[j] == 1)  {

                  /* Determine the index in opposite_circle_boundary_data
					 where the right-side curve begins.  */

				  start_right_side_index = endpoints[j]+1;
				  if (start_right_side_index > TOT_POINTS_PER_OBJ-1)
				    start_right_side_index = 0;
				  right_side_declination = crossing_dec[j] * 180/PI;
				}
				else  {

                  /* Determine the index in opposite_circle_boundary_data
					 where the left-side curve begins.  */

				  start_left_side_index = endpoints[j]+1;
				  if (start_left_side_index > TOT_POINTS_PER_OBJ-1)
					start_left_side_index = 0;
				  left_side_declination = crossing_dec[j] * 180/PI;
			    }
			  }

              /* Determine the number of points in the
				 right and left-side curves.  */

              N_right_side_pts = start_left_side_index - start_right_side_index;
			  if (N_right_side_pts < 0)
				N_right_side_pts += TOT_POINTS_PER_OBJ;
			  N_left_side_pts = TOT_POINTS_PER_OBJ - N_right_side_pts;

              /* Form the curves - right_boundary_data and
				 left_boundary_data.  */

              k = 0;
			  for (j=start_right_side_index; 
				    j<start_right_side_index + N_right_side_pts; j++)  {

				if (j >= TOT_POINTS_PER_OBJ)
				  j_wrap = j - TOT_POINTS_PER_OBJ;
				else
				  j_wrap = j;

				right_boundary_data[k].ra = opposite_circle_boundary_data[j_wrap].ra;
				right_boundary_data[k].dec = opposite_circle_boundary_data[j_wrap].dec;

			    k += 1;
			  }

              k = 0;

			  for (j=start_left_side_index; 
           		   j<start_left_side_index + N_left_side_pts; j++)  {

			    if (j >= TOT_POINTS_PER_OBJ)
			  	  j_wrap = j - TOT_POINTS_PER_OBJ;
			    else
			  	  j_wrap = j;

			    left_boundary_data[k].ra = opposite_circle_boundary_data[j_wrap].ra;
			    left_boundary_data[k].dec = opposite_circle_boundary_data[j_wrap].dec;

			    k += 1;
			  }

             /* The curves will be plotted differently if right_boundary_data
				increases in declination with index, or decreases in declination
				with index.  Clearly, left_boundary_data will do the opposite.
				(The figure above assumes right_boundary_data will decrease in
				declination with index.)  Since right_side_declination will
				be the first declination in right_boundary_data, if it is
				less than left_side_declination, right_boundary_data will 
                increase in declination with index. And vice-versa.  */

		  	 if (right_side_declination > left_side_declination)  {
			    right_90 = 90.0;
			    left_90 = -90.0;
			  }  
			  else  {
				right_90 = -90.0;
				left_90 = 90.0;
			  }

		      /* This first character is the left bracket */
		      strcpy(s, "{{");

              /* Begin setting up the curve.  Start at pt. A in
				 the figure above.  (Note: The figure above
				 assumes right_boundary_data decreases in
				 declination with index, so right_side_declination
				 exceeds left_side_declination, and 
                 right_90 = +90 deg.) */

		      sprintf (buf, 
                 " %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f",
					360.0,  right_side_declination,
					360.0,  right_90,
                      0.0,  right_90, 
                      0.0,  right_side_declination);
		      strcat(s, buf);

			  strcat (s, " ");

              /* Set up the right-side curve ... */
			  PrintPairPoints (N_right_side_pts, buf,
							   right_boundary_data);

			  strcat (s, buf);

			  strcat (s, " ");

              /* Set up the connection from the final pt.
				 of the right-side curve to pt. B on the
				 figure.  */

		      sprintf (buf, 
                 " %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f",
					   0.0,  left_side_declination,
                       0.0,  left_90, 
                     360.0,  left_90,
					 360.0,  left_side_declination);

		      strcat(s, buf);

			  strcat (s, " ");

              /* Set up the left-side curve ... */
			  PrintPairPoints (N_left_side_pts, buf,
							   left_boundary_data);

			  strcat (s, buf);

		      strcat (s, "}}");
              return 0;
            }

            else  {  /* Boundary does not include, or 
						is tangent to, 0-RA crossing */

			  /* The circle plot will look as follows:

										 pt. A
                                          |

                 dec =  90 >>>>>>>>>>>>>>>v >>>>>>>>>>>>>>>>>>>>>>>>>
                           ^              v ^                       v
                           ^              v ^                       v
                           ^              v ^                       v
                           ^              v ^                       v
                           ^              v ^                       v
                           ^            v     ^                     v
                           ^          v         ^                   v
                           ^         v           ^                  v
                           ^        v             ^                 v
                           ^         v           ^                  v
                           ^          v         ^                   v
                           ^            v     ^                     v
                           ^              > >                       v
                           ^                                        v
                           ^                                        v
                 dec = -90 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                           |                                        |
                           RA=360                                RA=0

              */

              /* Create "reordered_circle_boundary_data" - a boundary
				 data set containing the same points as
				 opposite_circle_boundary_data, but starting at the point of
				 maximum declination.
				 
				 Begin by finding the maximum declination (and the
				 corresponding index) in opposite_circle_boundary_data.  */

              max_dec = -PI/2;
              for (j=0; j<TOT_POINTS_PER_OBJ; j++)  {
				if (opposite_circle_boundary_data[j].dec >= max_dec)  {
				  max_dec_index = j;
				  max_dec = opposite_circle_boundary_data[j].dec;
				}
			  }

			  max_dec *= 180/PI;

              /* Create reordered_circle_boundary_data... */

              k = 0;

			  for (j=max_dec_index; 
           		   j<max_dec_index + TOT_POINTS_PER_OBJ; j++)  {

			    if (j >= TOT_POINTS_PER_OBJ)
			  	  j_wrap = j - TOT_POINTS_PER_OBJ;
			    else
			  	  j_wrap = j;

			    reordered_circle_boundary_data[k].ra = opposite_circle_boundary_data[j_wrap].ra;
			    reordered_circle_boundary_data[k].dec = opposite_circle_boundary_data[j_wrap].dec;

			    k += 1;
			  }

		      /* This first character is the left bracket */
		      strcpy(s, "{{");

              /* Set up boundary plot from (RA=360, Dec=+90) to
				 pt. A on figure above, then to top (max_dec) of
				 reordered_circle_boundary.  */

		      sprintf (buf, 
                 " %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f",
					360.0,  90.0,
					reordered_circle_boundary_data[0].ra * 180/PI, 90.0,
					reordered_circle_boundary_data[0].ra * 180/PI, 
					   reordered_circle_boundary_data[0].dec * 180/PI);
		      strcat(s, buf);

			  strcat (s, " ");

              /* Set up plot for reordered_circle_boundary... */

			  PrintPairPoints (TOT_POINTS_PER_OBJ, buf,
							   reordered_circle_boundary_data);

			  strcat (s, buf);

			  strcat (s, " ");

              /* Set up plot for top of reordered_circle_boundary,
				 back to pt. A, then about the outer boundary of the
				 field-of-view.  */

		      sprintf (buf, 
                 " %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f",
					reordered_circle_boundary_data[0].ra * 180/PI, 
					   reordered_circle_boundary_data[0].dec * 180/PI,
					reordered_circle_boundary_data[0].ra * 180/PI, 90.0,
					   0.0,  90.0,
                       0.0, -90.0,
                     360.0, -90.0, 
					 360.0,  90.0);

		      strcat(s, buf);

		      strcat (s, "}}");
              return 0;
			}
		
		    break;


		case Across_RA_Zero: 
			
	        /* Generate the data set for plotting */
	        Update_Circle_Boundary (obj_in_eci, circle_boundary_data,
							        TOT_POINTS_PER_OBJ, radius);
	
			strcpy (s, "{{");

			status = DeclsOfTwoZeroRAPointsOnCircle
					 (radius, obj_in_eci, declinations);
			
			if (!status)
			{
				status = EndPointIndex (TOT_POINTS_PER_OBJ,
										circle_boundary_data,
										endpoints, dir, crossing_dec);


				if (status == NO_ACROSSING) /* Discrete points may not wrap even though continuous would have */
				{
				
					PrintPairPoints(TOT_POINTS_PER_OBJ, buf,
							circle_boundary_data);

					strcat(s, buf);

					sprintf (buf, " %10.6f %10.6f",
						  circle_boundary_data[0].ra*RTD,
						  circle_boundary_data[0].dec*RTD);
					strcat(s, buf);

					strcat(s, "}}");

				}
				else if (status != TWO_ACROSSING)
				{
					/* generating error message */
					sprintf (s,
                             "Error: Inconsistent case: should across twice, while status is %d\n", status);
					return 1; /* Return abnormal status */
				}
				else
				{
					/* generate first set of data */

					PrintPairPoints (endpoints[1] - endpoints[0], buf,
								 	 &circle_boundary_data[endpoints[0]+1]);

					strcat(s, buf);

					if (fabs(circle_boundary_data[endpoints[1]].dec
							    - declinations[0]) <
					    fabs(circle_boundary_data[endpoints[1]].dec
							    - declinations[1]))


						/* { */ sprintf (buf,
							 " %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f} {%10.6f %10.6f %10.6f %10.6f %10.6f %10.6f ",
									 (double)(1+dir[1])*180.0,
									 declinations[0]*RTD,
									 (double)(1+dir[1])*180.0,
									 declinations[1]*RTD,
									 circle_boundary_data[endpoints[0]+1].ra*
									 RTD,
									 circle_boundary_data[endpoints[0]+1].dec*
									 RTD,
									 circle_boundary_data[endpoints[0]].ra*RTD,
									 circle_boundary_data[endpoints[0]].dec*RTD,
									 (double)(1-dir[1])*180.0,
									 declinations[1]*RTD,
									 (double)(1-dir[1])*180.0,
									 declinations[0]*RTD); /* } */
						else

							/* { */ sprintf (buf,
									 " %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f} {%10.6f %10.6f %10.6f %10.6f %10.6f %10.6f ",
									 (double)(1+dir[1])*180.0,
									 declinations[1]*RTD,
									 (double)(1+dir[1])*180.0,
									 declinations[0]*RTD,
									 circle_boundary_data[endpoints[0]+1].ra*
									 RTD,
									 circle_boundary_data[endpoints[0]+1].dec*
									 RTD,
									 circle_boundary_data[endpoints[0]].ra*RTD,
									 circle_boundary_data[endpoints[0]].dec*RTD,
									 (double)(1-dir[1])*180.0,
									 declinations[0]*RTD,
									 (double)(1-dir[1])*180.0,
									 declinations[1]*RTD); /* } */

					strcat (s, buf);

					/* generate second set of data */

					PrintPairPoints (TOT_POINTS_PER_OBJ-endpoints[1]-1, buf,
								 	 &circle_boundary_data[endpoints[1]+1]);

					strcat (s, buf);

					strcat (s, " ");

					PrintPairPoints (endpoints[0]+1, buf, circle_boundary_data);

					strcat (s, buf);

					strcat (s, "}}");

				}
			}
			else
			{
				sprintf (s, "Error: Anomaly occured in computing the two declinations.");
				return 1; /* Something is wrong */
			}

			break;

	}

	return 0;
}

int PrintRectangleData (char *s, double fov_h, double fov_v,
						double obj_to_eci[3][3])
{
	char buf[DATA_FORMAT_SIZE*(2*TOT_POINTS_PER_FOV+8)];
	RA_DEC	obj_fov_data[TOT_POINTS_PER_FOV+4];
	RA_DEC	left_and_right[2][TOT_POINTS_PER_FOV+4];
	wrap_type 	obj_fov_wrap;

	int pts_per_side = TOT_POINTS_PER_FOV/4;

	double declinations[2], corners_in_eci[4][3];
	double pole_dec = 90.0;

	/* Get the wrapping situation */
	obj_fov_wrap = RectangleWrapAroundPole (fov_h, fov_v, obj_to_eci);


	Update_Rectangle_Boundary (obj_to_eci, corners_in_eci, obj_fov_data,
							   pts_per_side, fov_h, fov_v);

	if (obj_fov_wrap == None &&
		RectangleAcrossRightAscentZero(obj_fov_data[0].ra,
		   obj_fov_data[pts_per_side+1].ra,
			   obj_fov_data[2*(pts_per_side+1)].ra,
				   obj_fov_data[3*(pts_per_side+1)].ra))

		obj_fov_wrap = Across_RA_Zero;

	switch (obj_fov_wrap)
	{
		case None:

			sprintf (s, "{{");

			PrintOneFovDataSet (buf, pts_per_side+1,  obj_fov_data);

			strcat (s, buf);

			strcat (s, "}}");
			
			break;

		case Wrap_N_Pole: case Wrap_S_Pole:

			/* Figure out which pole the FOV wraps */
			if ( obj_fov_wrap == Wrap_S_Pole )
				pole_dec = - pole_dec;

			declinations[0] = DeclOfZeroRAPointOnWrappingRectangle
							  (corners_in_eci);

			if (declinations[0] == PI)
			{
				sprintf (s, "Error: anomaly occurred in computing the declination of the point with 0 right ascension.\n");
				return 1;
			}

			ExtendLeftandRight (4*pts_per_side+4, obj_fov_data,
								 left_and_right[0], left_and_right[1]);

			strcpy (s, "{{");

			PrintOneFovDataSet (buf, pts_per_side+1, left_and_right[0]);

			strcat (s, buf);

			strcpy (buf, "} {");

			strcat (s, buf);

			PrintOneFovDataSet (buf, pts_per_side+1, left_and_right[1]);

			strcat (s, buf);

			sprintf (buf,
		     " {%10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f}}}\0",
			   0.0, declinations[0]*RTD, 0.0, pole_dec,
				 360.0, pole_dec, 360.0, declinations[0]*RTD);

			strcat (s, buf);

			break;

		case Across_RA_Zero:

			MappingLeftandRight (4*pts_per_side+4, obj_fov_data,
								 left_and_right[0], left_and_right[1]);

			strcpy (s, "{{");

			PrintOneFovDataSet (buf, pts_per_side+1, left_and_right[0]);

			strcat (s, buf);

			strcpy (buf, "} {");

			strcat (s, buf);

			PrintOneFovDataSet (buf, pts_per_side+1, left_and_right[1]);

			strcat (s, buf);

			sprintf (buf, "}}\0");

			strcat (s, buf);

			break;
	}

	return 0;
}
