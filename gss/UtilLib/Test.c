/*===================================================================

	FILE NAME: 
		Test.c	

	DESCRIPTION:
		*This is a test driver of util functions. 
		
 ====================================================================

	REVISION HISTORY:


 ==================================================================== */

/* Systerm Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Self Defined Headers */
#include <MessageIO.h>
#include <MathFnc.h>
#include <MathAppliFnc.h>

FILE *LogFp;

/* ----------------------------------------------
	Test case for MathFnc.c 
*/

void main() 
{
	int st = 0;
	float vect1[] = {1.0, 2.0, 3.0};
	float vect2[] = {5.0, 6.0, 7.0};
	float vect11[3], vect22[3];
	float vect3[3];
	double dvect[3];
	double mag;
	double Dot;
	double angle;
	double RA, Dec;

	int m=3, n=4, i, j;
	float mat1[]={1, 2, 3, 4, 5, 6, 7, 8, 9};
	float mat2[]={1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
	float mat3[12];

	float Quat1[]={0.70710678, 0.70710678, 0, 0};
	float Quat2[]={0.57735027, 0.57735027, 0.57735027, 0};
	float Quat3[4], Quat4[4], Quat5[4];
	float DirCosMat1[9], DirCosMat2[9];
	double Angle1=180*DTR, Angle2=180*DTR, Angle3=180*DTR; 

	double U_A[]={1, 0, 0};
	double V_A[]={0, 1, 0};
	double U_B[]={1, 0, 0};
	double V_B[]={0, 1, 0};
	double UM1[]={1, 0, 0,
				 0, 1, 0,
				 0, 0, 1};
	double UR1[]={1, 0, 0,
				 0, 1, 0,
				 0, 0, 1};
	double UR2[]={1, 0, 0,
				  0, 0, 1,
				  0, -1, 0};
	double UR3[]={0, 0, 1,
				  0, 1, 0,
				  -1, 0, 0};
	double UR4[]={0.9999999, 0.0003, -0.0002,
				  -0.0003, 1.0, 0.0001,
				  0.0002, -0.0001, 1.0};

	double Quat[4];

/* Test Vector founction */

	printf("Input Vectors: vect1=(%10.8f, %10.8f, %10.8f)\n", vect1[0], vect1[1], vect1[2]);
	printf("               vect2=(%10.8f, %10.8f, %10.8f)\n", vect2[0], vect2[1], vect2[2]);

	AngleBetVectors(vect1, vect2, &angle, FLOAT_DATA);
	printf("AngleBetVectors is %10.8f  \n", angle);

	Dot = DotProduct(vect1, vect2, FLOAT_DATA);
	printf("DotProduct is %10.8f  \n", Dot);

	CrossProduct(vect1, vect2, vect3, FLOAT_DATA);
	printf("CrossProduct is (%10.8f, %10.8f, %10.8f) \n", vect3[0], vect3[1], vect3[2]);

	st = VectorNormalize(vect1, vect11, FLOAT_DATA);
	st = VectorNormalize(vect2, vect22, FLOAT_DATA);
	AngleBetNormalVectors(vect11, vect22, &angle, FLOAT_DATA);
	printf("AngelBetNormalVectors is %10.8f \n", angle);

	st = VectorVerifyNormalize(vect1, vect11, FLOAT_DATA);
	st = VectorVerifyNormalize(vect2, vect22, FLOAT_DATA);
	DisplayMessage(st, "Test.c: vect2");
	AngleBetNormalVectors(vect11, vect22, &angle, FLOAT_DATA);
	printf("AngelBetNormalVectors is %10.8f \n", angle);

/* Test Matrix Functions */
		
	MatrixZero(mat1, m, m, FLOAT_DATA);
	MatrixIdent(mat1, m, FLOAT_DATA);
	MatrixAdd(mat1, mat1, mat1, m, m, FLOAT_DATA);
	MatrixMult(mat1, mat2, mat3, m, m, n, FLOAT_DATA);
	MatrixTranspose(mat3, mat3, m, n, FLOAT_DATA);
	for (i = 0; i < n; i++)
	{
		printf("\n");
		for (j = 0; j < m; j++)
			printf("%7.3f", mat3[i*m+j]); 
	}
	printf("\n\n");

/* Test Quaternion Functions */

	st = QuaternionVerifyNormalize(Quat1, Quat3, FLOAT_DATA);
	DisplayMessage(st, "in Test.c: Quat1");
	printf("\nQNorm= \n");
	for (i=0; i<n; i++)
		printf("%12.8f", Quat3[i]);
	printf("\n");

	st = QuaternionVerifyNormalize(Quat2, Quat3, FLOAT_DATA);
	DisplayMessage(st, "in Test.c: Quat2");
	printf("\nQNorm= \n");
	for (i=0; i<n; i++)
		printf("%12.8f", Quat3[i]);

	QuaternionMult(Quat1, Quat2, Quat3, FLOAT_DATA);
	Quaternion2DirCosMatrix(Quat1, DirCosMat1, FLOAT_DATA);
	Quaternion2DirCosMatrix(Quat2, DirCosMat2, FLOAT_DATA);
	DirCosMatrix2Quaternion(DirCosMat1, Quat1, FLOAT_DATA);
	DirCosMatrix2Quaternion(DirCosMat2, Quat2, FLOAT_DATA);
	QuaternionRotateEuler123(Quat1, Quat4, Angle1, Angle2, Angle3, FLOAT_DATA);
	QuaternionRotateEuler213(Quat1, Quat5, Angle1, Angle2, Angle3, FLOAT_DATA);

	printf("\nQMult= \n");
	for (i=0; i<n; i++)
		printf("%12.8f", Quat3[i]);

	printf("\n\nQ1_2DCM= ");
	for (i=0; i<m; i++)
	{
		printf("\n");
		for (j=0; j<m; j++)
			printf("%12.8f", DirCosMat1[i*m+j]);
	}

	printf("\n\nQ2_2DCM= ");
	for (i=0; i<m; i++)
	{
		printf("\n");
		for (j=0; j<m; j++)
			printf("%12.8f", DirCosMat2[i*m+j]);
	}

	printf("\n\nDCM1_2Quat1= \n");
	for (i=0; i<n; i++)
		printf("%12.8f", Quat1[i]);

	printf("\n\nDCM2_2Quat2= \n");
	for (i=0; i<n; i++)
		printf("%12.8f", Quat2[i]);

	printf("\n\nQ1Rot123= \n");
	for (i=0; i<n; i++)
		printf("%12.8f", Quat4[i]);

	printf("\n\nQ1Rot213= \n");
	for (i=0; i<n; i++)
		printf("%12.8f", Quat5[i]);

/* Test MathAppliFnc.c functions */

	RAscenDeclina2ECI(74.09281*DTR, 13.514466*DTR, dvect, DOUBLE_DATA);
	printf("\n\nRAscenDeclina2ECI = \n");
	for (i=0; i<3; i++)
		printf("%12.8f ", dvect[i]);

	ECI2RAscenDeclina(dvect, &RA, &Dec, DOUBLE_DATA);
	printf("\n\nECI2RAscenDeclina = \n");
	printf("%12.7f %12.7f\n", RA*RTD, Dec*RTD);

	RAscenDeclina2ECI(74.343086*DTR, 17.153677*DTR, dvect, DOUBLE_DATA);
	printf("\n\nRAscenDeclina2ECI = \n");
	for (i=0; i<3; i++)
		printf("%12.8f ", dvect[i]);

	RAscenDeclina2ECI(72.843593*DTR, 18.839861*DTR, dvect, DOUBLE_DATA);
	printf("\n\nRAscenDeclina2ECI = \n");
	for (i=0; i<3; i++)
		printf("%12.8f ", dvect[i]);
	RAscenDeclina2ECI(71.57012*DTR, 18.734693*DTR, dvect, DOUBLE_DATA);
	printf("\n\nRAscenDeclina2ECI = \n");
	for (i=0; i<3; i++)
		printf("%12.8f ", dvect[i]);

	ECI2RAscenDeclina(vect3, &RA, &Dec, FLOAT_DATA);
	printf("\n\nECI2RAscenDeclina = \n");
	printf("%12.7f %12.7f\n", RA, Dec);

	QuatTRIAD(U_A, U_B, V_A, V_B, Quat); 
	printf("\n\nQuatTRIAD = \n");
	for (i=0; i<4; i++)
		printf("%12.8f ", Quat[i]);

	MatrixTranspose(UM1, UM1, 3, 3, DOUBLE_DATA);
	MatrixTranspose(UR1, UR1, 3, 3, DOUBLE_DATA);
	QUEST(3, UM1, UR1, Quat);
	printf("\n\nQUEST = \n");
	for (i=0; i<4; i++)
		printf("%12.8f ", Quat[i]);

	MatrixTranspose(UR2, UR2, 3, 3, DOUBLE_DATA);
	QUEST(3, UM1, UR2, Quat);
	printf("\n\nQUEST = \n");
	for (i=0; i<4; i++)
		printf("%12.8f ", Quat[i]);

	MatrixTranspose(UR3, UR3, 3, 3, DOUBLE_DATA);
	QUEST(3, UM1, UR3, Quat);
	printf("\n\nQUEST = \n");
	for (i=0; i<4; i++)
		printf("%12.8f ", Quat[i]);

	MatrixTranspose(UR4, UR4, 3, 3, DOUBLE_DATA);
	QUEST(3, UM1, UR4, Quat);
	printf("\n\nQUEST = \n");
	for (i=0; i<4; i++)
		printf("%12.8f ", Quat[i]);

	printf("\n\n");


}
