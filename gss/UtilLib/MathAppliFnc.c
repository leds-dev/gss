/* $Id: MathAppliFnc.c,v 1.4 2000/03/30 00:17:36 jzhao Exp $ */
/*==========================================================================
	FILE NAME:
		MathAppliFnc.c

	DESCRIPESCRIPTION:
		This file include Applicational Mathmatical functions.
		All calculations done in double precision.

	Naming conventions:
		* [m x n] = m rows by n cols (in row order)

	Notes:
		* input vectors/matrices preserved.
		* input matrix may = output matrix.
		* Scalar part of Quaternion is in 4th element

	FUNCTION LIST:
		int RAscenDeclina2ECI(double RAscen, double Declina, 
											void *ECI, int type);
		int ECI2RAscenDeclina(void *ECI, double *RAscen, double *Declina,
											int type);
        double QuaternionSepAngle(double Quat1[], double Quat2[]);

		int QuatTRIAD(double U_A[], double U_B[], double V_A[], double V_B[], 
			  								double Quat[]);
		int QUEST(int N, double UM[], double UR[], double Quat[]); 

 ====================================================================

	REVISION HISTORY:

 ==================================================================== */
/* Systerm Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/* Self Defined Headers */

#include <MessageIO.h>
#include <MathAppliFnc.h>

#define ERROR_TEXT "in MathAppliFnc"

/* ================================================================== */

/*----------------------------------
RAscenDeclina2ECI:	Converts the input Right Ascension and Declination(Radians)
					to a unit ECI vector.

Input:	
	RAscen, Declina: Right Ascension and Declination angles expressed in Radians 
	type:	FLOAT_DATA or DOUBLE_DATA(default = double)		

Ouput:	
	ECI:	3x1 unit vector in ECI frame.

*/

int RAscenDeclina2ECI(double RAscen, double Declina, void *ECI, int type)
{
	int st=0, i;
	float *fECI;
	double *dECI;
	double tmp[3];	

	fECI = ECI;
	dECI = ECI;
	
	tmp[0] = cos(RAscen)*cos(Declina);
	tmp[1] = sin(RAscen)*cos(Declina);
	tmp[2] = sin(Declina);

	for (i=0; i<3; i++)
		if (type == FLOAT_DATA)	fECI[i] = (float)tmp[i];
		else					dECI[i] = tmp[i];

	return(st);
}

/*----------------------------------
ECI2RAscenDeclina:	Converts the input ECI to Right Ascension and 
					Declination(Radians))

Input:	
	ECI:	3x1 unit vector in ECI frame.
	type:	FLOAT_DATA or DOUBLE_DATA(default = double)		

Ouput:	
	RAscen, Declina: Right Ascension and Declination angles expressed in Radians. 

*/
int ECI2RAscenDeclina(void *ECI, double *RAscen, double *Declina, int type)
{
	int st=0;
	float *fECI;
	double mag;
	double *dECI;

	fECI = ECI;
	dECI = ECI;
	
	if(type == FLOAT_DATA)
	{
		if ( (fECI[1]==0.0) && (fECI[0]==0.0) )
		{
			*RAscen = 0.0;
			*Declina = fECI[2] > 0.0 ? 0.5*PI:-0.5*PI;
		}
		else
		{
			mag = sqrt((double)fECI[0]*(double)fECI[0] +
					   (double)fECI[1]*(double)fECI[1] +
					   (double)fECI[2]*(double)fECI[2]);

			*RAscen = atan2((double)fECI[1], (double)fECI[0]);
			*Declina = asin((double)fECI[2]/mag);
		}
	}
	else
	{
		if ( (dECI[1]==0.0) && (dECI[0]==0.0) )
		{
			*RAscen = 0.0;
			*Declina = dECI[2] > 0.0 ? 0.5*PI:-0.5*PI;
		}
		else
		{
			mag = sqrt(dECI[0]*dECI[0] + dECI[1]*dECI[1] + dECI[2]*dECI[2]);

			*RAscen = atan2(dECI[1], dECI[0]);
			*Declina = asin(dECI[2]/mag);
		}
	}

	if (*RAscen < 0.0) *RAscen += 2*PI;		/* Convert to 0-2PI range */

	return(st);
}

/*-------------------------------
QuaternionSepAngle: Calculates Separation angle of two quaternions.

Input:
 * Quat1:   array[4] representing Quaternion.
 * Quat2:   array[4] representing Quaternion.

Output:
 * return:    angle of separation between quaternions (radians)
*/

double QuaternionSepAngle(double Quat1[], double Quat2[])
{
    double Quat2_inv[4], Quat_delta[4];
    double sep;

     /* Calculate [Quat1 * Quat2^(-1)] */
     QuaternionInvers(Quat2, Quat2_inv, DOUBLE_DATA);
     QuaternionMult(Quat1, Quat2_inv, Quat_delta, DOUBLE_DATA);

     /* Calculate angular separation between the quaternions */
     if (fabs(Quat_delta[3]) < sqrt(1 - TINY))
     	sep = 2 * acos(Quat_delta[3]);
     else
        sep = 2 * sqrt(Quat_delta[0]*Quat_delta[0] +
                       Quat_delta[1]*Quat_delta[1] +
                       Quat_delta[2]*Quat_delta[2]);

    /* For separations between 180 and 360 degrees, replace
       separation with 360 degrees minus separation.  */

    if (sep > PI)
      sep = 2 * PI - sep;

    return(sep);
}

/*-------------------------------------------
QuatTRIAD:	Generate Quaternion repersenting rotation of body B in (fixed)
			frame A. (Direct Quaternion Method).

Input:
		U_A, U_B:	3 element vectors. Where U_A represents a vector expressed
					with respect to frame A. U_A corresponds to vector U_B,
					representing a vector expressed with respect to frame B.
		V_A, V_B:	3 element vectors. Where V_A represents a vector expressed
					with respect to frame A. V_A corresponds to vector V_B,
					representing a vector expressed with respect to frame B.

Output:
		Quat:		4 element vector, representing the Quaternion.

*/

int QuatTRIAD(double U_A[], double U_B[], double V_A[], double V_B[], 
			  double Quat[])
{
	int st = 0, i;
	double U_A_Norm[3], U_B_Norm[3], V_A_Norm[3], V_B_Norm[3];
	double W_A[3], W_B[3], W_A_Norm[3], W_B_Norm[3];
	double U_AB[3], V_AB[3], W_AB[3], MagU_AB, MagV_AB, MagW_AB;
	double U_ABxV_AB[3], V_ABxW_AB[3], W_ABxU_AB[3];
	double MagU_ABxV_AB, MagV_ABxW_AB, MagW_ABxU_AB;
	double UAngle, VAngle, WAngle, Angle, Parity, tmp[3];
	double Axis[3], AxAxis[3], BxAxis[3], AxAxis_Norm[3], BxAxis_Norm[3];

/* Normalize input U_A, U_B, V_A, V_B vectors. */

	st =  VectorNormalize(U_A, U_A_Norm, DOUBLE_DATA);	
	st =  VectorNormalize(U_B, U_B_Norm, DOUBLE_DATA);	
	st =  VectorNormalize(V_A, V_A_Norm, DOUBLE_DATA);	
	st =  VectorNormalize(V_B, V_B_Norm, DOUBLE_DATA);	

/* Creat W_A & W_B from Cross products */

	CrossProduct(U_A_Norm, V_A_Norm, W_A, DOUBLE_DATA);
	CrossProduct(U_B_Norm, V_B_Norm, W_B, DOUBLE_DATA);

/* Normalize W_A & W_B */

	st =  VectorNormalize(W_A, W_A_Norm, DOUBLE_DATA);	
	st =  VectorNormalize(W_B, W_B_Norm, DOUBLE_DATA);	
	 
/* Calculate difference vectors U_AB, V_AB, W_AB */	

	VectorDiff(U_A_Norm, U_B_Norm, U_AB, DOUBLE_DATA);
	VectorDiff(V_A_Norm, V_B_Norm, V_AB, DOUBLE_DATA);
	VectorDiff(W_A_Norm, W_B_Norm, W_AB, DOUBLE_DATA);

/* Calculate Magnitude of U_AB, V_AB, W_AB */

	MagU_AB = VectorMagnitude(U_AB, DOUBLE_DATA);
	MagV_AB = VectorMagnitude(V_AB, DOUBLE_DATA);
	MagW_AB = VectorMagnitude(W_AB, DOUBLE_DATA);

/* Allow for the case that U_A_Norm/U_B_Norm, or V_A_Norm/V_B_Norm, or 
   W_A_Norm/W_B_Norm is the axis of rotation */

	if (MagU_AB < TINY)  	/* TINY=1.0e-10 */
		for (i=0; i<3; i++) Axis[i] = U_A_Norm[i];
	else if (MagV_AB < TINY)  
		for (i=0; i<3; i++) Axis[i] = V_A_Norm[i];
	else if (MagW_AB < TINY)
		for (i=0; i<3; i++) Axis[i] = W_A_Norm[i];
	else
	{
	
/* None of the unit vectors are the rotation axis.  This rotation axis 
   must be the vector "Axis" which satiafies:

   Axis <dot> U_A_Norm = Axis <dot> U_B_Norm; and
   Axis <dot> V_A_Norm = Axis <dot> V_B_Norm; and
   Axis <dot> W_A_Norm = Axis <dot> W_B_Norm;

   In general, there will be two unit-vector solutions to the above; 
   one will be the negative of the other. It doesn't matter which one 
   we use; the sign of the angle of rotation will depend on the choice,
   but the quaternion resulting will be the same.

   If we can assume that U_AB is NOT parallel to V_AB or W_AB, and V_AB is 
   NOT parallel to W_AB, then the solution for "Axis" will be:

   U_AB <cross> V_AB = V_AB <cross> W_AB = W_AB <cross> U_AB

   or the negative. A solution will exsit, however, even when the above 
   assumption is violated, as long as one of the cross products above is non 0.
*/
		CrossProduct(U_AB, V_AB, U_ABxV_AB, DOUBLE_DATA);
		CrossProduct(V_AB, W_AB, V_ABxW_AB, DOUBLE_DATA);
		CrossProduct(W_AB, U_AB, W_ABxU_AB, DOUBLE_DATA);

		MagU_ABxV_AB = VectorMagnitude(U_ABxV_AB, DOUBLE_DATA);
		MagV_ABxW_AB = VectorMagnitude(V_ABxW_AB, DOUBLE_DATA);
		MagW_ABxU_AB = VectorMagnitude(W_ABxU_AB, DOUBLE_DATA);

/* To define the axis, choose the cross-product with greatest magnitude */

		if ((MagU_ABxV_AB >= MagV_ABxW_AB) && 
			(MagU_ABxV_AB >= MagW_ABxU_AB))	
			VectorDivScalar(U_ABxV_AB, Axis, MagU_ABxV_AB, DOUBLE_DATA);

		else if ((MagV_ABxW_AB >= MagU_ABxV_AB) &&
				 (MagV_ABxW_AB >= MagW_ABxU_AB))
				 VectorDivScalar(V_ABxW_AB, Axis, MagV_ABxW_AB, 
															DOUBLE_DATA);
		else 
			VectorDivScalar(W_ABxU_AB, Axis, MagW_ABxU_AB, DOUBLE_DATA);

	} /* end of if MagU_AB<TINY , Axis is selected*/ 

/* The angle of rotation will correspond to the angle subtended by the 
   rotation of a vector perpendicular to the axis. */

	AngleBetVectors(Axis, U_A_Norm, &UAngle, DOUBLE_DATA);
	AngleBetVectors(Axis, V_A_Norm, &VAngle, DOUBLE_DATA);
	AngleBetVectors(Axis, W_A_Norm, &WAngle, DOUBLE_DATA);

	if (fabs(UAngle) >= 1.0e-5)
	{
		CrossProduct(U_A_Norm, Axis, AxAxis, DOUBLE_DATA);
		CrossProduct(U_B_Norm, Axis, BxAxis, DOUBLE_DATA);
		VectorDivScalar(AxAxis, AxAxis_Norm, sin(UAngle), DOUBLE_DATA);
		VectorDivScalar(BxAxis, BxAxis_Norm, sin(UAngle), DOUBLE_DATA);
	}
	else if((fabs(VAngle) >= fabs(UAngle)) && 
			(fabs(VAngle) >= fabs(WAngle))) 
	{
		CrossProduct(V_A_Norm, Axis, AxAxis, DOUBLE_DATA);
		CrossProduct(V_B_Norm, Axis, BxAxis, DOUBLE_DATA);
		VectorDivScalar(AxAxis, AxAxis_Norm, sin(VAngle), DOUBLE_DATA);
		VectorDivScalar(BxAxis, BxAxis_Norm, sin(VAngle), DOUBLE_DATA);
	}
	else
	{
		CrossProduct(W_A_Norm, Axis, AxAxis, DOUBLE_DATA);
		CrossProduct(W_B_Norm, Axis, BxAxis, DOUBLE_DATA);
		VectorDivScalar(AxAxis, AxAxis_Norm, sin(WAngle), DOUBLE_DATA);
		VectorDivScalar(BxAxis, BxAxis_Norm, sin(WAngle), DOUBLE_DATA);
	}

/* The angle between AxAxis and BxAxis will correspond to the angle of 
   rotation. However, we need to determine the direction of rotation; 
   i.e., the sign of the angle.
*/
	CrossProduct(BxAxis_Norm, AxAxis_Norm, tmp, DOUBLE_DATA);
	Parity = DotProduct(Axis, tmp, DOUBLE_DATA);
	
	AngleBetVectors(AxAxis_Norm, BxAxis_Norm, &Angle, DOUBLE_DATA);
	if (Parity < 0) Angle *= -1;

	for (i=0; i<3; i++) Quat[i] = Axis[i]*sin(Angle/2.0);
						Quat[3] = cos(Angle/2.0);
	
	return(st);
}

/*----------------------
QUEST:	This routine will compute the quaternion representing the optimal 
		three axis attitude estimate for 2 or more star in FOV.
Input:
	N:	int. Number of vectors(stars).
	UM:	Array[3xN] of double. N Measured unit vectors.
	UR:	Array[3xN] of double. N Reference unit vectors. 
Output:
	Quat:	Array[4] of double. Optimal Quaternion.	
*/

int QUEST(int N, double UM[], double UR[], double Quat[])
{
	int status = 0;
	int i, CASESR;
	double NW, MAX_VL, AC, BC, ZSZ, CC, ZSSZ, DC, X, DX, NORM;
	double *V, *W, *VT, *VSR, *VSRT;
	double B1[9], B2[9], B3[9], B4[9], B1T[9], B2T[9], B3T[9], B4T[9];
	double S1[9], S2[9], S3[9], S4[9], B[9], S[9], BS[9], SS[9], XT[9];
	double cofactor[3], Z[3], SZ[3], SSZ[3], XV[3]; 
	double SIGMASR[4], DELSR[4], KCSR[4], RHOSR[4], QR[4];
	double SIGMA, DEL, KC, LAMBDA, ALPHA, BETA, RHO;

	V = calloc(3*N, sizeof(double));
	W = calloc(3*N, sizeof(double));
	VT = calloc(3*N, sizeof(double));
	VSR = calloc(3*N, sizeof(double));
	VSRT = calloc(3*N, sizeof(double));
	if ( VSRT == NULL )
	{
		status = MEM_ERROR;
		DisplayMessage( status, ERROR_TEXT );
		goto quit;
	}

	/* Normalize input vectors */
	MatrixTranspose(UM, UM, 3, N, DOUBLE_DATA);
	for ( i=0; i<N; i++)
		VectorNormalize(UM+i*3, UM+i*3, DOUBLE_DATA);
	MatrixTranspose(UM, UM, N, 3, DOUBLE_DATA);

	MatrixTranspose(UR, UR, 3, N, DOUBLE_DATA);
	for ( i=0; i<N; i++)
		VectorNormalize(UR+i*3, UR+i*3, DOUBLE_DATA);
	MatrixTranspose(UR, UR, N, 3, DOUBLE_DATA);

	/* Build weighted matrices: V = (1/SQRT(N))*CSTAR
								W = (1/SQRT(N))*MSTAR
		each measurement assigned equal weight				*/

	NW = 1.0/sqrt((double)N);
	MatrixMultScalar(UR, V, NW, 3, N, DOUBLE_DATA);
	MatrixMultScalar(UM, W, NW, 3, N, DOUBLE_DATA);

	/* Apply successive rotation to avoid singularity

	   If q4 = 0, which occurs for 180 degree rotations,
	   a singularity occurs to avoid this, we can prerotate
	   by 180 degrees about the x, y, or z axes. */ 

	/*==========================
	COMPUTE B MATRIX FOR EACH CASE: B = W*transpose(V)

	CASE 1: No Prerotation:		
	uv = transpose(x, y, z)      */

	MatrixTranspose(V, VT, 3, N, DOUBLE_DATA);
	MatrixMult(W, VT, B1, 3, N, 3, DOUBLE_DATA);

	/*
	CASE 2: 180 degrees about x-axis  
	uv = transpose(x, -y, -z)	*/

	for (i=0; i<N; i++)
	{
		VSR[i]   	= V[i];
		VSR[i+N] 	= -V[i+N]; 
		VSR[i+2*N] 	= -V[i+2*N]; 
	}
	MatrixTranspose(VSR, VSRT, 3, N, DOUBLE_DATA);
	MatrixMult(W, VSRT, B2, 3, N, 3, DOUBLE_DATA);
	
	/*
	CASE 3: 180 degrees about y-axis
	uv = transpose(-x, y, -z) 	*/

	for (i=0; i<N; i++)
	{
		VSR[i]		= -V[i];
		VSR[i+N]	=  V[i+N];
		VSR[i+2*N]	= -V[i+2*N]; 
	}
	MatrixTranspose(VSR, VSRT, 3, N, DOUBLE_DATA);
	MatrixMult(W, VSRT, B3, 3, N, 3, DOUBLE_DATA);

	/*
	CASE 4: 180 degrees about z-axis
	uv = transpose(-x, -y, z)  	*/  

	for (i=0; i<N; i++)
	{
		VSR[i]		= -V[i];
		VSR[i+N]	= -V[i+N];
		VSR[i+2*N]	=  V[i+2*N]; 
	}
	MatrixTranspose(VSR, VSRT, 3, N, DOUBLE_DATA);
	MatrixMult(W, VSRT, B4, 3, N, 3, DOUBLE_DATA);

	/*=========================
	COMPUTE SIGMA FOR EACH CASE: SIGMA = trace(B)  */

	SIGMASR[0] = B1[0] + B1[4] + B1[8];
	SIGMASR[1] = B2[0] + B2[4] + B2[8];
	SIGMASR[2] = B3[0] + B3[4] + B3[8];
	SIGMASR[3] = B4[0] + B4[4] + B4[8];

	/*========================
	COMPUTE S MATRIX FOR EACH CASE: S = B + transpose(B) */

	MatrixTranspose(B1, B1T, 3, 3, DOUBLE_DATA); 
	MatrixAdd(B1, B1T, S1, 3, 3, DOUBLE_DATA);

	MatrixTranspose(B2, B2T, 3, 3, DOUBLE_DATA); 
	MatrixAdd(B2, B2T, S2, 3, 3, DOUBLE_DATA);

	MatrixTranspose(B3, B3T, 3, 3, DOUBLE_DATA); 
	MatrixAdd(B3, B3T, S3, 3, 3, DOUBLE_DATA);

	MatrixTranspose(B4, B4T, 3, 3, DOUBLE_DATA); 
	MatrixAdd(B4, B4T, S4, 3, 3, DOUBLE_DATA);

	/*=======================
	COMPUTE DEL FOR EACH CASE: DEL = determinant(S) */

	DELSR[0] = MatrixDeterminant_3x3(S1);
	DELSR[1] = MatrixDeterminant_3x3(S2);
	DELSR[2] = MatrixDeterminant_3x3(S3);
	DELSR[3] = MatrixDeterminant_3x3(S4);

	/*=======================
	COMPUTE KC FOR EACH CASE: KC = trace(adj(S)) */ 

	cofactor[0] = S1[4] * S1[8] - S1[5] * S1[7];
	cofactor[1] = S1[0] * S1[8] - S1[2] * S1[6];
	cofactor[2] = S1[0] * S1[4] - S1[1] * S1[3];
	KCSR[0] = cofactor[0] + cofactor[1] + cofactor[2];

	cofactor[0] = S2[4] * S2[8] - S2[5] * S2[7];
	cofactor[1] = S2[0] * S2[8] - S2[2] * S2[6];
	cofactor[2] = S2[0] * S2[4] - S2[1] * S2[3];
	KCSR[1] = cofactor[0] + cofactor[1] + cofactor[2];

	cofactor[0] = S3[4] * S3[8] - S3[5] * S3[7];
	cofactor[1] = S3[0] * S3[8] - S3[2] * S3[6];
	cofactor[2] = S3[0] * S3[4] - S3[1] * S3[3];
	KCSR[2] = cofactor[0] + cofactor[1] + cofactor[2];

	cofactor[0] = S4[4] * S4[8] - S4[5] * S4[7];
	cofactor[1] = S4[0] * S4[8] - S4[2] * S4[6];
	cofactor[2] = S4[0] * S4[4] - S4[1] * S4[3];
	KCSR[3] = cofactor[0] + cofactor[1] + cofactor[2];

	/* ==========================
	COMPUTER RHO ESTIMATE FOR EACH CASE:
	RHO = q4*sqrt(q1**2+q2**2+q3**2+q4**2)
	(Assume maximum eigenvalue = 1)				

	FIND MAXIMUM VALUE OF RHO
	even if RHO is finite, large cancellation occur for small RHO
	values which degrade the accuracy of the estimste, therefor 
	we want to choose the largest value       */

	for (i=0; i<4; i++)
	{
		RHOSR[i] = ( 1 + SIGMASR[i] )*( 1 - SIGMASR[i]*SIGMASR[i] + KCSR[i] ) - 
																  	DELSR[i]; 
		if (i==0)
		{
			MAX_VL = fabs(RHOSR[0]); 
			CASESR = 1;
		}
		else 
		{
			if (fabs(RHOSR[i]) > MAX_VL)
			{
				MAX_VL = fabs(RHOSR[i]);
				CASESR = i+1;
			}
		}
	}

	/* =========================
	APPLY PREROTATION COORESPONDING TO MAXIMUM VALUE OF RHO */
	
	switch (CASESR)
	{
		/*
		CASE 1: NO PREROTATION
		uv = transpose(x, y, z)		*/

		case 1: 
			KC = KCSR[0];
			SIGMA = SIGMASR[0];
			DEL = DELSR[0];
			
			memcpy(B, B1, 9*sizeof(double));	
			memcpy(S, S1, 9*sizeof(double));
			break;
		/*
		CASE 2:	180 rotation about x-axis
		uv = transpose(x, -y, -z)	*/

		case 2:
			KC = KCSR[1];
			SIGMA = SIGMASR[1];
			DEL = DELSR[1];

			memcpy(B, B2, 9*sizeof(double));
			memcpy(S, S2, 9*sizeof(double));
			break;  
		/*
		CASE 3:	180 rotation about y-axis
		uv = transpose(-x, y, -z)	*/

		case 3:
			KC = KCSR[2];
			SIGMA = SIGMASR[2];
			DEL = DELSR[2];

			memcpy(B, B3, 9*sizeof(double));
			memcpy(S, S3, 9*sizeof(double));
			break;  
		/*
		CASE 4:	180 rotation about z-axis
		uv = transpose(-x, -y, z)	*/

		case 4:
			KC = KCSR[3];
			SIGMA = SIGMASR[3];
			DEL = DELSR[3];

			memcpy(B, B4, 9*sizeof(double));
			memcpy(S, S4, 9*sizeof(double));
			break;  

		default:
			break;
	}
	/*===============================
	CONSTRUCT CHARACTERISTIC EQUATION:
	x**4 -(a+b)*x**2 -c*x +(a*b +c*sigm -d) = 0 
	where:
	Z = trace(WxV)
	a = sigma**2 - k
	b = sigma**2 + |Z|**2
	c = del + transpose(Z)*S*Z
	d = transpose(Z)*S*S*Z						*/
	
	Z[0] = B[5] - B[7];
	Z[1] = B[6] - B[2];
	Z[2] = B[1] - B[3];

	AC = SIGMA*SIGMA - KC;
	BC = SIGMA*SIGMA + Z[0]*Z[0] + Z[1]*Z[1] + Z[2]*Z[2];
	MatrixMult(S, Z, SZ, 3, 3, 1, DOUBLE_DATA);
	MatrixMult(Z, SZ, &ZSZ, 1, 3, 1, DOUBLE_DATA);
	CC = DEL + ZSZ;
	MatrixMult(S, SZ, SSZ, 3, 3, 1, DOUBLE_DATA);
	MatrixMult(Z, SSZ, &ZSSZ, 1, 3, 1, DOUBLE_DATA);
	DC = ZSSZ;

	/*=================================
	SOLVE FOR MAXIMUM EIGENVALUE USING NEWTON RAPHSON METHOD
	note: fewer than 4 iterations may be sufficient.

	initial quess value		*/

	X = 1;
	for (i=0; i<4; i++)
	{
		DX = pow(X,4) - (AC+BC)*X*X - CC*X +(AC*BC + CC*SIGMA - DC); 
		DX /= (4*pow(X,3) - 2*(AC+BC)*X - CC);
		X -= DX;
	}

	/* Maximum eigenvalue */
	LAMBDA = X;

	/*=================================
	CONSTRUCT OPTIMAL QUATERNION
	The eigenvector corresponding to the maximum eigenvalue is the 
	optimal quaternion

	X = (alpha*I +beat*S + S*S)*Z
	norm = 1/sqrt(rho**2 +|X|**2)
	q1 = X1*norm
	q2 = X2*norm
	q3 = X3*norm
	q4 = rho*norm

	where:
	alpha = lambda**2 - sigma**2 + k
	beta = lambda - sigma
	rho = (lambda+sigma)*alpha -del			*/ 

	ALPHA = LAMBDA*LAMBDA - SIGMA*SIGMA + KC;
	BETA = LAMBDA - SIGMA;
	RHO = (LAMBDA + SIGMA)*ALPHA - DEL;
	MatrixMultScalar(S, BS, BETA, 3, 3, DOUBLE_DATA);
	MatrixMult(S, S, SS, 3, 3, 3, DOUBLE_DATA);
	MatrixAdd(BS, SS, XT, 3, 3, DOUBLE_DATA);

	for (i=0; i<3; i++)
		XT[i*3+i] += ALPHA;

	MatrixMult(XT, Z, XV, 3, 3, 1, DOUBLE_DATA);
	NORM = sqrt(RHO*RHO + XV[0]*XV[0] + XV[1]*XV[1] + XV[2]*XV[2]);
	
	QR[0] = XV[0]/NORM;
	QR[1] = XV[1]/NORM; 
	QR[2] = XV[2]/NORM; 
	QR[3] = RHO/NORM; 

	/*=============================
	CORRECT FOR PREROTATION			*/

	switch(CASESR)
	{
		/* CASE 1: NO ROTATIO */
		case 1:
			memcpy(Quat, QR, 4*sizeof(double));
			break;

		/* CASE 2: 180 ROTATION ABOUT X-AXIS 
			q = transpose(q4, -q3, q2, -q1) */
		case 2:
			Quat[0] = QR[3];
			Quat[1] = -QR[2];
			Quat[2] = QR[1];
			Quat[3] = -QR[0];
			break;

		/* CASE 3: 180 ROTATION ABOUT y-AXIS
			q = transpose(q3, q4, -q1, -q2)	*/
		case 3:
			Quat[0] = QR[2];
			Quat[1] = QR[3];
			Quat[2] = -QR[0];
			Quat[3] = -QR[1];
			break;

		/* CASE 4: 180 ROTATION ABOUT Z-AXIS
			q= transpoase(-q2, q1, q4, -q3)	*/
		case 4:
			Quat[0] = -QR[1];
			Quat[1] = QR[0];
			Quat[2] = QR[3];
			Quat[3] = -QR[2];
			break;

		default:
			break;
	}

quit:
	free(V);
	free(W);
	free(VT);
	free(VSR);
	free(VSRT);

	return(status);	
}

/*******************************************************************************
	Change Log:
	$Log: MathAppliFnc.c,v $
	Revision 1.4  2000/03/30 00:17:36  jzhao
	Fixed typo.
	
	Revision 1.3  2000/02/24 01:06:46  jzhao
	Modified for port to NT
	
	Revision 1.2  2000/02/23 21:14:08  jzhao
	Add missing return values.
	
	Revision 1.1  1999/12/13 22:12:41  jzhao
	Initial Release.
	
********************************************************************************/
