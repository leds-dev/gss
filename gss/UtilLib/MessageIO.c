/* $Id: MessageIO.c,v 1.3 2000/03/21 23:38:45 jzhao Exp $ */
/*===================================================================

    FILE NAME:
        MessageIO.c

    DESCRIPTION:
		This File Contains the routines to handle the error massage 
		and processing status display and log.

 ====================================================================

    REVISION HISTORY:

 ==================================================================== */

 /* System Headers */

#include <stdio.h>

 /* self defined headers */
#include <MessageIO.h>

 /* Global Variables */

 static char *ErrorMsg[] = 
 {
	"No Error",
	"Out Of Memoery",
	"File Open Error", 
	"Math Error",
	"Non-normalized Quat/Vec",
	"Unknown Mode",
 };

 static char *StatusMsg[] =
 {
	" ",
	"Failure: No Enough Tracked Stars for Acquisition",
	"Failure: No Reference Tracker for Acquisition",
	"Failure: Contradictory Acquisition",
	"Failure: No Enough Identifiable Stars for Acquisition",
	"Partial Successful Acquision(Single Tracker Acquision)",
	"Successful Acquisition",
	"CIS Complete",
 };

/*-----------------
GetMessage
*/

char *GetMessage(int stat, char *s)
{
	static char Msg[100];

	if ( (stat <= 0) && (stat > LAST_ERROR) )
		sprintf(Msg, "%s %s", ErrorMsg[-stat], s);

	else if (stat < LAST_STATUS)
		sprintf(Msg, "%s %s", StatusMsg[stat], s);

	return(Msg);
}
/* -----------------
DisplayMessage:
*/
void DisplayMessage(int stat, char *s)
{
	if ( ( (stat <= 0) && (stat > LAST_ERROR) ) || 
		 ( (stat > 0) && (stat < LAST_STATUS) ) )
	{
		printf("=======================================\n");
		printf("	%s\n", GetMessage(stat, s));
		printf("=======================================\n");
	}
	LogMessage(stat, s);
} 

/* -----------------
LogMessage:
*/
void LogMessage(int stat, char *s)
{
	if ( ( (stat <= 0) && (stat > LAST_ERROR) ) || 
		 ( (stat > 0) && (stat < LAST_STATUS) ) )
	{
		fprintf(LogFp, "====================================\n");
		fprintf(LogFp, "	%s\n", GetMessage(stat, s));
		fprintf(LogFp, "====================================\n");
	}
}

/******************************************************************************
	Change Log:
	$Log: MessageIO.c,v $
	Revision 1.3  2000/03/21 23:38:45  jzhao
	Fixed uninitialized variables.
	
	Revision 1.2  2000/02/09 23:50:29  jzhao
	Added DirCosMatrix2Quaternion function.
	
	Revision 1.1  1999/12/13 22:12:42  jzhao
	Initial Release.
	
*******************************************************************************/
