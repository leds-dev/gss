/* $Id: MathFnc.c,v 1.7 2000/03/21 23:38:45 jzhao Exp $ */
/*===================================================================

	FILE NAME: 
		MathFnc.c

	DESCRIPTION:
		*This file include general mathmatic functions(Matrices, Vectors,
		 Quaternion... calculations). 
		*All calculations done in double precision.
		*The scalar element of All Quatrnions is in 4th element.
		
	Naming conventions:
 		* [m x n] = m rows by n cols (in row order)

	Notes:
	 	* input vectors/matrices preserved.  
		* input matrix/vector may = output matrix/vector.
		* Scalar part of Quaternion is in 4th element.

	FUNCTION LIST:
		
		int RemoveZero(double *pt_mag);
		double VectorMagnitude(void *vect, int type);
		int VectorNormalize(void *vect1, void *vect2, int type);
		int VectorVerifyNormalize(void *vect1, void *vect2, int type);
		double DotProduct(void *vect1, void *vect2, int type);
		void CrossProduct(void *vect1, void *vect2, void *vect, int type);

		int AngleBetVectors(void *vect1, void *vect2, double *angle, int type);
		int AngleBetNormalVectors(void *vect1, void *vect2, 
								  double *angle, int type);
		int VectorDiff(void *vect1, void *vect2, void *vect3, int type);
		int VectorDivScalar(void *vect1, void *vect2, double scalar, int type); 	
		int MatrixZero(void *mat1, int m, int n, int type);
		int MatrixIdent(void *mat1, int m, int type);
		int MatrixAdd(void *mat1, void *mat2, void *mat3, 
					  int m, int n, int type);
		int MatrixMult(void *mat1, void *mat2, void *mat3,
					   int m, int n, int p, int type);
		int MatrixTranspose(void *mat1, void *mat2, 
							int m, int n, int type);

		int QuaternionMult(void *Quat1, void *Quat2, void *Quat3, int type)
		int Quaternion2DirCosMatrix(void *Quat1, void *DirCosMat, int type)
		int QuaternionRotateEuler123(void *Quat1, void *Quat2, double Angle1, 
									 double Angle2, double Angle3, int type)
		int QuaternionRotateEuler213(void *Quat1, void *Quat2, double Angle1, 
									 double Angle2, double Angle3, int type)
		int QuaternionInvers(void *Quat1, void *Quat2, int type)
		int QuaternionIdent(void *Quat, int type); 
		int QuaternionNormalize(void *Quat1, void *Quat2, int type)
		int QuaternionVerifyNormalize(void *Quat1, void *Quat2, int type)
		int DirCosMatrix2Quaternion(void *DirCosMat, void Quat, int type)

 ==================================================================== */

/* Systerm Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Self Defined Headers */

#include <MessageIO.h>
#include <MathFnc.h> 

/* macros */
#define ERROR_TEXT "in MATHFNC"

/*=====================

 Vector functions

 ====================== */

/*---------------------
RemoveZero: Force magnitude of a vector > minimum value, preserving sign
Description:
 * used to avoid divide by zero
 * return error status, but dont display message (caller can ignore)

*/
int RemoveZero(double *pt_mag)
{
  int    st=0;

  if (fabs(*pt_mag) < TINY) 
  {
    st = MATH_ERROR;
    if (*pt_mag >= 0) *pt_mag =  TINY;
    if (*pt_mag <  0) *pt_mag = -TINY;
  }
  return(st);			
}

/*-------------------------------
VectorMagnitude: Calculates magnitude of the given vector

Input: 
 * vect:   array[X,Y,Z] representing vector
 * type:	FLOAT_DATA or DOUBLE_DATA(default = double)	

Output:
 * return: magnitude of vect

*/
double VectorMagnitude(void *vect, int type)
{
  double mag;
  float	*fvect;
  double *dvect;

  fvect = vect;
  dvect = vect;

  if (type == FLOAT_DATA)
  {
	mag = (double)fvect[0]*fvect[0] + 
		  (double)fvect[1]*fvect[1] + 
          (double)fvect[2]*fvect[2];
  }
  else
  {
	mag =  dvect[0]*dvect[0] + 
		   dvect[1]*dvect[1] + 
           dvect[2]*dvect[2];
  }

  return( sqrt(mag) );
}

/*-------------------------------
VectorVerifyNormalize: Verify normalization of input vector and
					   normlize it if needed.

Input: 
 * vect1:   array[X,Y,Z] representing vector
 * type:	FLOAT_DATA or DOUBLE_DATA 

Output:
 * vect2:	normalized vector(can be vect1)

*/
int VectorVerifyNormalize(void *vect1, void *vect2, int type)
{
  int    st=0;
  int	 n;
  float *fvect1, *fvect2;
  double *dvect1, *dvect2;
  double mag;

  fvect1 = vect1; fvect2 = vect2;
  dvect1 = vect1; dvect2 = vect2;

  if (type == FLOAT_DATA)
  {
	mag = (double)fvect1[0]*fvect1[0] + 
		  (double)fvect1[1]*fvect1[1] + 
          (double)fvect1[2]*fvect1[2];
	
	if (fabs(1.0-mag) > NORM_TOL) 
	{
		st = NON_NORMALIZE;
		mag = sqrt(mag);
		RemoveZero(&mag);
		for (n=0; n<3; n++)	fvect2[n] = (float)(fvect1[n]/mag);
	}
	else for (n=0; n<3; n++) fvect2[n] = fvect1[n];
  }		
  else
  {
	mag = dvect1[0]*dvect1[0] + 
		  dvect1[1]*dvect1[1] + 
          dvect1[2]*dvect1[2];
	
	if (fabs(1.0-mag) > NORM_TOL)
	{
		st = NON_NORMALIZE;
		mag = sqrt(mag);
		RemoveZero(&mag);
		for (n=0; n<3; n++)	dvect2[n] = dvect1[n]/mag;
	}
	else for (n=0; n<3; n++) dvect2[n] = dvect1[n];
  }
  return(st);
}

/*-------------------------------
VectorNormalize: 

Input: 
 * vect1:   array[X,Y,Z] representing vector
 * type:	FLOAT_DATA or DOUBLE_DATA 

Output:
 * vect2:	normalized vector(can be vect1)

*/
int VectorNormalize(void *vect1, void *vect2, int type)
{
  int    st=0;
  int	n;
  float *fvect1, *fvect2;
  double *dvect1, *dvect2;
  double mag;

  fvect1 = vect1; fvect2 = vect2;
  dvect1 = vect1; dvect2 = vect2;

  if (type == FLOAT_DATA)
  	mag = VectorMagnitude(fvect1, FLOAT_DATA);
  else
  	mag = VectorMagnitude(dvect1, DOUBLE_DATA);
  st  = RemoveZero(&mag);

  for (n=0; n<3; n++) 
	if (type == FLOAT_DATA) fvect2[n] = (float)(fvect1[n]/mag);
	else					dvect2[n] = dvect1[n]/mag;
  return(st);
}

/*---------------------------------
DotProduct: Calculatess dot product of two vectors
Description:
 * dot_product = v1 <dot> v2 = v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2]
							 = |v1| * |v2| * cos(theta)

Input: 
 * vect1, vect2:  vector arrays
 * type:		  FLOAT_DATA or DOUBLE_DATA

Output:
 * return:        dot product of vect1 and vect2

*/
double DotProduct(void *vect1, void *vect2, int type)
{
  float *fvect1, *fvect2;
  double *dvect1, *dvect2;
  double dot;

  fvect1 = vect1; fvect2 = vect2;
  dvect1 = vect1; dvect2 = vect2;

  if (type == FLOAT_DATA)
	dot = (double)fvect1[0]*fvect2[0] + (double)fvect1[1]*fvect2[1] + 
          (double)fvect1[2]*fvect2[2];
  else
	dot = dvect1[0]*dvect2[0] + dvect1[1]*dvect2[1] + 
          dvect1[2]*dvect2[2];

  return(dot);
}

/*---------------------------------
CrossProduct: Calculates cross product of two vectors
Description:
 *  v  = v1 x v2 
   |v| = |v1| * |v2| * sin(theta)

Input:
 * vect1, vect2:	vector arrays

Output:
 * vect3:	cross product of vect1 and vect2 (can be vect1 or vect2)

*/
void CrossProduct(void *vect1, void *vect2, void *vect3, int type)
{
  int i;
  float *fvect1, *fvect2, *fvect3;
  double *dvect1, *dvect2, *dvect3;
  double tmp[3];

  fvect1 = vect1; fvect2 = vect2; fvect3 = vect3;
  dvect1 = vect1; dvect2 = vect2; dvect3 = vect3;

  if ( type == FLOAT_DATA )
  {
	tmp[0] = (double)fvect1[1]*fvect2[2] - (double)fvect1[2]*fvect2[1];
	tmp[1] = (double)fvect1[2]*fvect2[0] - (double)fvect1[0]*fvect2[2];
	tmp[2] = (double)fvect1[0]*fvect2[1] - (double)fvect1[1]*fvect2[0]; 
  }
  else
  {
	tmp[0] = dvect1[1]*dvect2[2] - dvect1[2]*dvect2[1];
	tmp[1] = dvect1[2]*dvect2[0] - dvect1[0]*dvect2[2];
	tmp[2] = dvect1[0]*dvect2[1] - dvect1[1]*dvect2[0]; 
  }

  for ( i=0; i<3; i++)
  	if (type == FLOAT_DATA) fvect3[i] = (float)tmp[i];
	else					dvect3[i] = tmp[i];
}

/*-----------------------------------------------
AngleBetVectros: calculates angle between two vectors

Input: 
 * vect1, vect2:  vector arrays
 * type:		  FLOAT_DATA or DOUBLE_DATA

Output:
 * angle:  angle between vect1, vect2 (in radians)

*/
int AngleBetVectors(void *vect1, void *vect2, double *angle, int type)
{
  int st = 0;
  float *fvect1, *fvect2;
  double *dvect1, *dvect2;
  double mag, theta;

  fvect1 = vect1;	fvect2 = vect2;
  dvect1 = vect1;	dvect2 = vect2;

  if (type == FLOAT_DATA)
  {
  	mag  = VectorMagnitude(fvect1, FLOAT_DATA) * 
		   VectorMagnitude(fvect2, FLOAT_DATA);
  	st = RemoveZero(&mag);
  	theta = DotProduct(fvect1, fvect2, FLOAT_DATA) / mag;
  }
  else
  {
  	mag  = VectorMagnitude(dvect1, DOUBLE_DATA) * 
		   VectorMagnitude(dvect2, DOUBLE_DATA);
  	st = RemoveZero(&mag);
  	theta = DotProduct(dvect1, dvect2, DOUBLE_DATA) / mag;
  }

  if ( (fabs(theta) > 1)&&(fabs(theta) <= 1+TINY) )  
  {
	if ( theta > 0 ) theta = 1;
	else			 theta = -1;
  }

  if (fabs(theta) <= 1) *angle = acos(theta);
  else                 
  {
	st = MATH_ERROR;	
	DisplayMessage( st, ERROR_TEXT ); 
  }
  return(st);
}

/*-----------------------------------------------
AngleBetNormalVectors: calculates angle between two normalized vectors

Input: 
 * vect1, vect2:  vector arrays

Output:
 * angel:  angle between vect1, vect2 (in radians)

*/
int AngleBetNormalVectors(void *vect1, void *vect2, double *angle, int type)
{
  int st = 0;
  float *fvect1, *fvect2;
  double *dvect1, *dvect2;
  double theta;

  fvect1 = vect1;	fvect2 = vect2;
  dvect1 = vect1;	dvect2 = vect2;

  if(type == FLOAT_DATA)
  	theta = DotProduct(fvect1, fvect2, FLOAT_DATA);
  else
	theta = DotProduct(dvect1, dvect2, DOUBLE_DATA);

  if (theta > 1) theta = 1.0; 
  if (theta < -1) theta = -1.0;

  *angle = acos(theta);

  return(st);
}

/*-------------------------------
VectorDiff: Calculates difference vector of two given vectors

Input: 
 * vect1, vect2:   array[X,Y,Z] representing vector

Output:
 * vect3: difference vector(can be vect1 or vect2) 

*/
int VectorDiff(void *vect1, void *vect2, void *vect3, int type)
{
	int st = 0;
	float *fvect1, *fvect2, *fvect3;
	double *dvect1, *dvect2, *dvect3;

	fvect1 = vect1;	fvect2 = vect2;	fvect3 = vect3;
	dvect1 = vect1;	dvect2 = vect2;	dvect3 = vect3;

	if (type == FLOAT_DATA)
	{
		fvect3[0] = fvect1[0] - fvect2[0];
		fvect3[1] = fvect1[1] - fvect2[1];
		fvect3[2] = fvect1[2] - fvect2[2];
	}
	else
	{
		dvect3[0] = dvect1[0] - dvect2[0];
		dvect3[1] = dvect1[1] - dvect2[1];
		dvect3[2] = dvect1[2] - dvect2[2];
	}

	return(st);
}

/*-------------------------------
VectorDivScalar: Calculates a vector(each element) divided by a scalar   

Input: 
 * vect1 :   array[X,Y,Z] representing vector
 * scalar:	 a scalar  
Output:
 * vect2:	(can be vect1) 

*/
int VectorDivScalar(void *vect1, void *vect2, double scalar, int type)
{
	int st = 0;
	float *fvect1, *fvect2;
	double *dvect1, *dvect2;

	fvect1 = vect1;	fvect2 = vect2;
	dvect1 = vect1; dvect2 = vect2;

	if ( fabs(scalar) < TINY) 
	{
		if (scalar >= 0) scalar = TINY;
		else			 scalar = -TINY;
	}

	if (type == FLOAT_DATA)
	{
		fvect2[0] = (float)(fvect1[0]/scalar);
		fvect2[1] = (float)(fvect1[1]/scalar);
		fvect2[2] = (float)(fvect1[2]/scalar);
	}
	else
	{
		dvect2[0] = dvect1[0]/scalar;
		dvect2[1] = dvect1[1]/scalar;
		dvect2[2] = dvect1[2]/scalar;
	}

	return(st);
}
/*==================

 Matrix Math

 =================== */

/*------------------
MatrixZero:  Creat zero matrix(all elements in the matrix are zero) 

Input:
 * type: FLOAT_DATA or DOUBLE_DATA (default = double)

Output:
 * mat1:  zero matrix

*/
int MatrixZero(void *mat1, int m, int n, int type)
{
  int    st=0;
  int    i, total;
  float  *fmat1;
  double *dmat1;

  fmat1 = mat1;
  dmat1 = mat1;  
  total = m*n;

  for (i = 0; i < total; i++)
    if (type == FLOAT_DATA) fmat1[i] = 0;
    else               		dmat1[i] = 0;

  return(st);
}

/*------------------
MatrixIdent:  Creat identity matrix (must be square)

Input:
 * type: FLOAT_DATA or DOUBLE_DATA (default = double)

Output:
 * mat1:  identity matrix

*/
int MatrixIdent(void *mat1, int m, int type)
{
  int    st=0;
  int    i, total;
  float  *fmat1;
  double *dmat1;

  MatrixZero(mat1, m , m, type);

  fmat1 = mat1;
  dmat1 = mat1;  
  total = m*m;

  for (i = 0; i < total; i+=m+1)
    if (type == FLOAT_DATA) fmat1[i] = 1;
    else               		dmat1[i] = 1;

  return(st);
}

/*------------------
MatrixAdd:  Add two matrices

Input:
 * mat1, mat2:  [m x n] matrices
 * type: FLOAT_DATA or DOUBLE_DATA (default = double)

Output:
 * mat3:  sum of the matrices (can be same as mat1 or mat2)

*/
int MatrixAdd(void *mat1, void *mat2, void *mat3, int m, int n, int type)
{
  int    st=0;
  int    i, total;
  float  *fmat1, *fmat2, *fmat3;
  double *dmat1, *dmat2, *dmat3;

  fmat1 = mat1;  fmat2 = mat2;  fmat3 = mat3;
  dmat1 = mat1;  dmat2 = mat2;  dmat3 = mat3;
  total = m*n;

  for (i = 0; i < total; i++)
    if (type == FLOAT_DATA) fmat3[i] = fmat1[i] + fmat2[i];
    else               		dmat3[i] = dmat1[i] + dmat2[i];

  return(st);
}

/*------------------
MatrixMultScalar: Matrix mult. by scalar. 

Input:
 * mat1:  [m x n] matrices
 * scalar: double; 
 * type: FLOAT_DATA or DOUBLE_DATA (default = double)

Output:
 * mat2: scalar times the mat1 (can be same as mat1)

*/
int MatrixMultScalar(void *mat1, void *mat2, double scalar, int m, int n, 
																int type)
{
  int    st=0;
  int    i, total;
  float  *fmat1, *fmat2;
  double *dmat1, *dmat2;

  fmat1 = mat1;  fmat2 = mat2;  
  dmat1 = mat1;  dmat2 = mat2; 
  total = m*n;

  for (i = 0; i < total; i++)
    if (type == FLOAT_DATA) fmat2[i] =(float)(scalar * fmat1[i]);
    else               		dmat2[i] = scalar * dmat1[i];

  return(st);
}

/*-----------------------
MatrixMult: Multiply two matrices

Inputs:
 * mat1:     [m x n] matrix
 * mat2:     [n x p] matrix
 * m, n, p:  size of the matrices
 * type:     FLOAT_DATA or DOUBLE (default = double)

Output:
 * mat3:     [m x p] matrix (can be same as mat1 or mat2)

*/
int MatrixMult(void *mat1, void *mat2, void *mat3,
             int m, int n, int p, int type)
{
  int    st=0;
  int    i, j, k;
  float  *fmat1, *fmat2, *fmat3;
  double *dmat1, *dmat2, *dmat3;
  double *tmp=NULL;

  fmat1 = mat1;  fmat2 = mat2;  fmat3 = mat3;
  dmat1 = mat1;  dmat2 = mat2;  dmat3 = mat3;

  tmp = calloc(m*p, sizeof(double));
  if (tmp == NULL)
  {
    st = MEM_ERROR;
	DisplayMessage( st, ERROR_TEXT ); 
    goto quit;
  }
  for (i = 0; i < m; i++)
    for (j = 0; j < p; j++)
    {
      tmp[i*p+j] = 0;

      for (k = 0; k < n; k++)
        if (type == FLOAT_DATA) tmp[i*p+j] += (double)fmat1[i*n+k]*fmat2[k*p+j];
        else               		tmp[i*p+j] += dmat1[i*n+k]*dmat2[k*p+j];
    }

  for (i = 0; i < m*p; i++)
    if (type == FLOAT_DATA) fmat3[i] = (float)tmp[i];
    else               		dmat3[i] = tmp[i];

quit:
  free(tmp);
  return(st);
}


/*-------------------
MatrixTranspose: Calculates the transpose of a matrix

Input:
 * mat1:  [m x n] matrix

Output:
 * mat2:  [n x m] matrix (can be same as mat1)

*/
int MatrixTranspose(void *mat1, void *mat2, int m, int n, int type)
{
  int    st=0;
  int    i, j;
  float  *fmat1, *fmat2;
  double *dmat1, *dmat2;
  double *tmp=NULL;

  fmat1 = mat1;  fmat2 = mat2;
  dmat1 = mat1;  dmat2 = mat2;

  tmp = calloc(m*n, sizeof(double));
  if (tmp == NULL)
  {
    st = MEM_ERROR;
	DisplayMessage( st, ERROR_TEXT );
    goto quit;
  }

  for(i = 0; i < n; i++)
    for (j = 0; j < m; j++)
    {
      if (type == FLOAT_DATA) tmp[i*m+j] = fmat1[j*n+i];
      else                    tmp[i*m+j] = dmat1[j*n+i];
    }

  for (i = 0; i < m*n; i++)
    if (type == FLOAT_DATA) fmat2[i] = (float)tmp[i];
    else                    dmat2[i] = tmp[i];

quit:
  free(tmp);
  return(st);
}

/* ----------------------------
MatrixDeterminant_3x3: Calculate determinant for 3x3 matrix. 

Input:
	*mat1 [3x3] matrix
Output/return:
	det determinant.

*/

double MatrixDeterminant_3x3(double *mat)
{
	double det;

	det =  mat[0] * mat[4] * mat[8] +
		   mat[1] * mat[5] * mat[6] +
		   mat[2] * mat[3] * mat[7] -
		   mat[0] * mat[7] * mat[5] -
		   mat[1] * mat[3] * mat[8] -
		   mat[2] * mat[4] * mat[6];

	return(det);
}

/* ============================

   Quaternion Math

  ============================= */

/* --------------------------------
QuaternionIdent: Create a Identity Quaternion. 

Input
	type:	int DOUBLE_DATA/FLOAT_DATA.
Output:
	Quat:	[4x1] Quaternoin vector.
*/

void QuaternionIdent(void *Quat, int type)
{
	int i;
	double *dQuat;
	float  *fQuat;

	dQuat = Quat;
	fQuat = Quat;

	for (i=0; i<3; i++) 
		if (type == FLOAT_DATA) fQuat[i] = 0.0;
		else				 	dQuat[i] = 0.0;	
	
	if (type == FLOAT_DATA) fQuat[3] = 1.0;
	else					dQuat[3] = 1.0;
}

/*-------------------------------
QuaternionNormalize: 

Input: 
 * Quat1:   array[4] representing Quaternion. 
 * type:	FLOAT_DATA or DOUBLE_DATA 

Output:
 * Quat2:	normalized Quaternion(can be Quat1)
*/

int QuaternionNormalize(void *Quat1, void *Quat2, int type)
{
	int    st=0; 
	int		n;
	float *fQuat1, *fQuat2;
	double *dQuat1, *dQuat2;
	double mag;

	fQuat1 = Quat1; fQuat2 = Quat2;
	dQuat1 = Quat1; dQuat2 = Quat2;

	if (type == FLOAT_DATA)
	{
		mag = (double)fQuat1[0]*fQuat1[0] + 
			  (double)fQuat1[1]*fQuat1[1] +
		  	  (double)fQuat1[2]*fQuat1[2] +
		  	  (double)fQuat1[3]*fQuat1[3]; 
		mag = sqrt(mag);
		st = RemoveZero(&mag);
		for (n=0; n<4; n++)	fQuat2[n] = (float)(fQuat1[n]/mag);
  	}
  	else
	{
  		mag = dQuat1[0]*dQuat1[0] + 
		  	  dQuat1[1]*dQuat1[1] +
		  	  dQuat1[2]*dQuat1[2] +
		   	  dQuat1[3]*dQuat1[3]; 
		mag = sqrt(mag);
		st = RemoveZero(&mag);
		for (n=0; n<4; n++)	dQuat2[n] = dQuat1[n]/mag;
	}
  return(st);
}

/* --------------------------------
QuaternionVerifyNormalize: Verify normlization of input quaternion and 
							normalize it if needed. 
Input: 
 * Quat1:   array[4] representing Quaternion. 
 * type:	FLOAT_DATA or DOUBLE_DATA 

Output:
 * Quat2:	normalized Quaternion(can be Quat1)
*/

int QuaternionVerifyNormalize(void *Quat1, void *Quat2, int type)
{
	int    st=0;
	int		n;
	float *fQuat1, *fQuat2;
	double *dQuat1, *dQuat2;
	double mag;

	fQuat1 = Quat1; fQuat2 = Quat2;
	dQuat1 = Quat1; dQuat2 = Quat2;

	if (type == FLOAT_DATA)
	{
		mag = (double)fQuat1[0]*fQuat1[0] + 
			  (double)fQuat1[1]*fQuat1[1] +
		  	  (double)fQuat1[2]*fQuat1[2] +
		  	  (double)fQuat1[3]*fQuat1[3]; 

		if(fabs(1.0-mag) > NORM_TOL)
		{
			st = NON_NORMALIZE;
			mag = sqrt(mag);
			RemoveZero(&mag);
			for (n=0; n<4; n++)	fQuat2[n] = (float)(fQuat1[n]/mag);
		}
		else for (n=0; n<4; n++) fQuat2[n] = fQuat1[n];
  	}
  	else
	{
  		mag = dQuat1[0]*dQuat1[0] + 
		  	  dQuat1[1]*dQuat1[1] +
		  	  dQuat1[2]*dQuat1[2] +
		   	  dQuat1[3]*dQuat1[3]; 

		if(fabs(1.0-mag) > NORM_TOL)
		{
			st = NON_NORMALIZE;
			mag = sqrt(mag);
			RemoveZero(&mag);
			for (n=0; n<4; n++)	dQuat2[n] = dQuat1[n]/mag;
		}
		else for (n=0; n<4; n++) dQuat2[n] = dQuat1[n];
	}
  return(st);
}

/* --------------------------------

QuaternionMult:	Calculate multiplication of two quaternions.

Description:
	Q = Q1 X Q2

	|Q[1]| 		|Q1[4]	-Q1[3]	Q1[2]	Q1[1]|		|Q2[1]|
	|Q[2]|	=	|Q1[3]	 Q1[4] -Q1[1]	Q1[2]|	*	|Q2[2]|
	|Q[3]|		|-Q1[2]  Q1[1]  Q1[4]	Q1[3]|		|Q2[3]|
	|Q[4]|		|-Q1[1]	-Q1[2] -Q1[3]	Q1[4]|		|Q2[4]|

Input:
	Quat1, Quat2:	[4x1] Quaternion Vectors
	type		:	can be FLOAT_DATA or DOUBLE_DATA (default = double)

Output:
	Quat3:			[4x1] Quaternion Vector, can be Quat1 or Quat2

*/

int QuaternionMult(void *Quat1, void *Quat2, void *Quat3, int type)
{
	int 	i, st = 0;
	float 	*fQuat1, *fQuat2, *fQuat3;
	double	*dQuat1, *dQuat2, *dQuat3;
	double	*tmp=NULL;

	fQuat1 = Quat1; fQuat2 = Quat2; fQuat3 = Quat3;
	dQuat1 = Quat1; dQuat2 = Quat2; dQuat3 = Quat3;

	tmp = calloc(4, sizeof(double));
	if(tmp == NULL)
	{
		st = MEM_ERROR;
		DisplayMessage( st, ERROR_TEXT ); 
		goto quit;
	}

	if(type == FLOAT_DATA)
	{
		tmp[0] = (double)fQuat1[3]*fQuat2[0] - (double)fQuat1[2]*fQuat2[1] + 
				 (double)fQuat1[1]*fQuat2[2] + (double)fQuat1[0]*fQuat2[3];

		tmp[1] = (double)fQuat1[3]*fQuat2[1] + (double)fQuat1[2]*fQuat2[0] + 
				 (double)fQuat1[1]*fQuat2[3] - (double)fQuat1[0]*fQuat2[2];

		tmp[2] = (double)fQuat1[3]*fQuat2[2] + (double)fQuat1[2]*fQuat2[3] - 
				 (double)fQuat1[1]*fQuat2[0] + (double)fQuat1[0]*fQuat2[1];

		tmp[3] = (double)fQuat1[3]*fQuat2[3] - (double)fQuat1[2]*fQuat2[2] - 
				 (double)fQuat1[1]*fQuat2[1] - (double)fQuat1[0]*fQuat2[0];
	}
	else
	{
		tmp[0] = dQuat1[3]*dQuat2[0] - dQuat1[2]*dQuat2[1] + 
				 dQuat1[1]*dQuat2[2] + dQuat1[0]*dQuat2[3];

		tmp[1] = dQuat1[3]*dQuat2[1] + dQuat1[2]*dQuat2[0] + 
				 dQuat1[1]*dQuat2[3] - dQuat1[0]*dQuat2[2];

		tmp[2] = dQuat1[3]*dQuat2[2] + dQuat1[2]*dQuat2[3] - 
				 dQuat1[1]*dQuat2[0] + dQuat1[0]*dQuat2[1];

		tmp[3] = dQuat1[3]*dQuat2[3] - dQuat1[2]*dQuat2[2] - 
				 dQuat1[1]*dQuat2[1] - dQuat1[0]*dQuat2[0];
	}

	for(i=0; i<4; i++)
	{
		if(type == FLOAT_DATA)	fQuat3[i] = (float)tmp[i];
		else					dQuat3[i] = tmp[i];
	}

quit:
	free(tmp);
	return(st);
}

/*----------------------------------------------------------------------
QuaternionInvers: Creat Invers of the Quatrnion.

Input:
	Quat1:	[4x1] vector
	type:	can be FLOAT_DATA or DOUBLE_DATA 
Output:
	Quat2;	[4x1] vector. Invers Quaternion(Can be same as Qaut1)
*/

int QuaternionInvers(void *Quat1, void *Quat2, int type)
{
	int st = 0;
	float *fQuat1, *fQuat2;
	double *dQuat1, *dQuat2;

	fQuat1 = Quat1;	fQuat2 = Quat2;
	dQuat1 = Quat1; dQuat2 = Quat2;

	if(type == FLOAT_DATA)
	{
		fQuat2[0] = -fQuat1[0];
		fQuat2[1] = -fQuat1[1];
		fQuat2[2] = -fQuat1[2];
		fQuat2[3] = fQuat1[3];
	}
	else
	{
		dQuat2[0] = -dQuat1[0];
		dQuat2[1] = -dQuat1[1];
		dQuat2[2] = -dQuat1[2];
		dQuat2[3] = dQuat1[3];
	}	
	return(st);
}
/* -----------------------------------------------------------------------
Quaternion2DirCosMatrix: Calculate Direction Cosin Matrix(DCM)(3x3) repersented
						 by Quaternion Vector(4x1).

Input:	Quat:	[4x1] vector
		type:	can be FLOAT_DATA or DOUBLE_DATA (default = double)
Ouput:	DirCosMat: [3x3] matrix

--*/

int Quaternion2DirCosMatrix(void *Quat, void *DirCosMat, int type)
{
	int 	st = 0;
	float 	*fQuat, *fDirCosMat;
	double	*dQuat, *dDirCosMat;

	fQuat = Quat; fDirCosMat = DirCosMat;
	dQuat = Quat; dDirCosMat = DirCosMat;

	if (type == FLOAT_DATA)
	{
		fDirCosMat[0] = (float)(1.0 - 2.0*((double)fQuat[1]*fQuat[1] + 
								   (double)fQuat[2]*fQuat[2]));
		fDirCosMat[1] =	(float)(2.0*((double)fQuat[0]*fQuat[1] +
								   (double)fQuat[2]*fQuat[3]));
		fDirCosMat[2] =	(float)(2.0*((double)fQuat[0]*fQuat[2] -
								   (double)fQuat[1]*fQuat[3]));

		fDirCosMat[3] = (float)(2.0*((double)fQuat[0]*fQuat[1] - 
								   (double)fQuat[2]*fQuat[3]));
		fDirCosMat[4] = (float)(1.0 - 2.0*((double)fQuat[0]*fQuat[0] +
								   (double)fQuat[2]*fQuat[2]));
		fDirCosMat[5] = (float)(2.0*((double)fQuat[1]*fQuat[2] +
								   (double)fQuat[0]*fQuat[3]));

		fDirCosMat[6] = (float)(2.0*((double)fQuat[0]*fQuat[2] +
								   (double)fQuat[1]*fQuat[3]));
		fDirCosMat[7] =	(float)(2.0*((double)fQuat[1]*fQuat[2] -
								   (double)fQuat[0]*fQuat[3]));
		fDirCosMat[8] = (float)(1.0 - 2.0*((double)fQuat[0]*fQuat[0] +
								   (double)fQuat[1]*fQuat[1]));
	}
	else
	{
		dDirCosMat[0] = 1.0 - 2.0*(dQuat[1]*dQuat[1] + dQuat[2]*dQuat[2]);
		dDirCosMat[1] = 	  2.0*(dQuat[0]*dQuat[1] + dQuat[2]*dQuat[3]);
		dDirCosMat[2] =		  2.0*(dQuat[0]*dQuat[2] - dQuat[1]*dQuat[3]);

		dDirCosMat[3] =		  2.0*(dQuat[0]*dQuat[1] - dQuat[2]*dQuat[3]);
		dDirCosMat[4] = 1.0 - 2.0*(dQuat[0]*dQuat[0] + dQuat[2]*dQuat[2]);
		dDirCosMat[5] = 	  2.0*(dQuat[1]*dQuat[2] + dQuat[0]*dQuat[3]);

		dDirCosMat[6] =		  2.0*(dQuat[0]*dQuat[2] + dQuat[1]*dQuat[3]);
		dDirCosMat[7] =		  2.0*(dQuat[1]*dQuat[2] - dQuat[0]*dQuat[3]);
		dDirCosMat[8] = 1.0 - 2.0*(dQuat[0]*dQuat[0] + dQuat[1]*dQuat[1]);
	}
	return(st);
}

/* -----------------------------------------------------------------------
DirCosMatrix2Quaternion: Calculate the Quaternion(4x1) representation of the
						 input Direction Cosine Matrix(3x3).
Input:	DirCosMat:	[3x3] matrix.
		type:		can be FLOAT_DATA or DOUBLE_DATA (default = double).
Output: Quat:		[4x1] vector.
*/

int DirCosMatrix2Quaternion(void *DirCosMat, void *Quat, int type)
{
	int     st = 0;
	float   *fQuat, *fDirCosMat;
	double  *dQuat, *dDirCosMat;
	double  C11, C12, C13, C21, C22, C23, C31, C32, C33;
	double	T, Z1, Z2, M, Q_N, F;

	fQuat = Quat; fDirCosMat = DirCosMat;
	dQuat = Quat; dDirCosMat = DirCosMat;

	if (type == FLOAT_DATA)
	{
		C11 = (double)fDirCosMat[0];			
		C12 = (double)fDirCosMat[1];			
		C13 = (double)fDirCosMat[2];			
		C21 = (double)fDirCosMat[3];			
		C22 = (double)fDirCosMat[4];			
		C23 = (double)fDirCosMat[5];			
		C31 = (double)fDirCosMat[6];			
		C32 = (double)fDirCosMat[7];			
		C33 = (double)fDirCosMat[8];			

		/* Select combination of diagonal direction cosine elements that
			produce the largest denominator.	*/
		T = C11 + C22 +C33;
		Z1 = Max(C11, C22);
		Z2 = Max(C33, T);
		M = Max(Z1, Z2);

		Q_N = 0.5 * sqrt(1.0 + M + M - T);
		F = 0.25 / Q_N;

		/* Do Computations */

		if (M == C11)
		{
			fQuat[0] = (float)Q_N; 
			fQuat[1] = (float)((C12 + C21) * F);
			fQuat[2] = (float)((C13 + C31) * F);
			fQuat[3] = (float)((C23 - C32) * F);
		}
		else if (M == C22)
		{
			fQuat[0] = (float)((C12 + C21) * F);
			fQuat[1] = (float)Q_N; 
			fQuat[2] = (float)((C23 + C32) * F);
			fQuat[3] = (float)((C31 - C13) * F);
		}
		else if (M == C33)
		{
			fQuat[0] = (float)((C13 + C31) * F);
			fQuat[1] = (float)((C23 + C32) * F);
			fQuat[2] = (float)Q_N; 
			fQuat[3] = (float)((C12 - C21) * F);
		}
		else
		{
			fQuat[0] = (float)((C23 - C32) * F);
			fQuat[1] = (float)((C31 - C13) * F);
			fQuat[2] = (float)((C12 - C21) * F);
			fQuat[3] = (float)Q_N; 
		}

		if ( fQuat[3] < 0.0 )
		{
			fQuat[0] = -fQuat[0];
			fQuat[1] = -fQuat[1];
			fQuat[2] = -fQuat[2];
			fQuat[3] = -fQuat[3];
		}
	}
	else
	{
		C11 = dDirCosMat[0];			
		C12 = dDirCosMat[1];			
		C13 = dDirCosMat[2];			
		C21 = dDirCosMat[3];			
		C22 = dDirCosMat[4];			
		C23 = dDirCosMat[5];			
		C31 = dDirCosMat[6];			
		C32 = dDirCosMat[7];			
		C33 = dDirCosMat[8];			

		/* Select combination of diagonal direction cosine elements that
			produce the largest denominator.	*/
		T = C11 + C22 +C33;
		Z1 = Max(C11, C22);
		Z2 = Max(C33, T);
		M = Max(Z1, Z2);

		Q_N = 0.5 * sqrt(1.0 + M + M - T);
		F = 0.25 / Q_N;

		/* Do Computations */

		if (M == C11)
		{
			dQuat[0] = Q_N; 
			dQuat[1] = (C12 + C21) * F;
			dQuat[2] = (C13 + C31) * F;
			dQuat[3] = (C23 - C32) * F;
		}
		else if (M == C22)
		{
			dQuat[0] = (C12 + C21) * F;
			dQuat[1] = Q_N; 
			dQuat[2] = (C23 + C32) * F;
			dQuat[3] = (C31 - C13) * F;
		}
		else if (M == C33)
		{
			dQuat[0] = (C13 + C31) * F;
			dQuat[1] = (C23 + C32) * F;
			dQuat[2] = Q_N; 
			dQuat[3] = (C12 - C21) * F;
		}
		else
		{
			dQuat[0] = (C23 - C32) * F;
			dQuat[1] = (C31 - C13) * F;
			dQuat[2] = (C12 - C21) * F;
			dQuat[3] = Q_N; 
		}

		if ( dQuat[3] < 0.0 )
		{
			dQuat[0] = -dQuat[0];
			dQuat[1] = -dQuat[1];
			dQuat[2] = -dQuat[2];
			dQuat[3] = -dQuat[3];
		}
	}

	return(st);
}
/* -----------------------------------------------------------------------
QuaternionRotateEuler123: Calcula8e 1-2-3 Euler angle rotations on a given 
						  Quaternion vector. 

Input:	Quat1:	[4x1] vector
		Angle1, Angle2, Angle3: 1-2-3 Euler angle(to be defined in radians) 
		type:	can be FLOAT_DATA or DOUBLE_DATA (default = double)
Ouput:  Quat2: [4x1] Quaernion vector(can be Quat1).	

--*/

int QuaternionRotateEuler123(void *Quat1, void *Quat2, double Angle1, 
							 double Angle2, double Angle3, int type)
{
	int 	i, st = 0;
	float 	*fQuat1, *fQuat2;
	double	*dQuat1, *dQuat2;
	double  *dtmp1 = NULL, *dtmp2 = NULL;

	fQuat1 = Quat1; fQuat2 = Quat2;
	dQuat1 = Quat1; dQuat2 = Quat2;

	dtmp1 = calloc(4, sizeof(double));
	dtmp2 = calloc(4, sizeof(double)); 
	if ( dtmp2 == NULL)
	{
		st = MEM_ERROR;
		DisplayMessage( st, ERROR_TEXT );
		goto quit;
	}

	for (i=0; i<4; i++)
	{
		if (type == FLOAT_DATA) dtmp1[i] = fQuat1[i];
		else					dtmp1[i] = dQuat1[i];
	}

	dtmp2[0] = sin(0.5*Angle1);
	dtmp2[1] = 0.0;
	dtmp2[2] = 0.0;
	dtmp2[3] = cos(0.5*Angle1);

	QuaternionMult(dtmp1, dtmp2, dtmp1, DOUBLE_DATA);

	dtmp2[0] = 0.0;
	dtmp2[1] = sin(0.5*Angle2);
	dtmp2[2] = 0.0;
	dtmp2[3] = cos(0.5*Angle2);

	QuaternionMult(dtmp1, dtmp2, dtmp1, DOUBLE_DATA);

	dtmp2[0] = 0.0;
	dtmp2[1] = 0.0;
	dtmp2[2] = sin(0.5*Angle3);
	dtmp2[3] = cos(0.5*Angle3);

	QuaternionMult(dtmp1, dtmp2, dtmp1, DOUBLE_DATA);

	for (i=0; i<4; i++)
	{
		if (type == FLOAT_DATA) fQuat2[i] = (float)dtmp1[i];
		else					dQuat2[i] = dtmp1[i];
	}
		
quit:
	free(dtmp1);
	free(dtmp2);
	return(st);
}

/* -----------------------------------------------------------------------
QuaternionRotateEuler213: Calculate 2-1-3 Euler angle rotations on a given 
						  Quaternion vector. 

Input:	Quat1:	[4x1] vector
		Angle1, Angle2, Angle3: 2-1-3 Euler angle(to be defined in radians) 
		type:	can be FLOAT_DATA or DOUBLE_DATA (default = double)
Ouput:  Quat2: [4x1] Quaernion vector(can be Quat1).	

--*/

int QuaternionRotateEuler213(void *Quat1, void *Quat2, double Angle1, 
							 double Angle2, double Angle3, int type)
{
	int 	i, st = 0;
	float 	*fQuat1, *fQuat2;
	double	*dQuat1, *dQuat2;
	double  *dtmp1 = NULL, *dtmp2 = NULL;

	fQuat1 = Quat1; fQuat2 = Quat2;
	dQuat1 = Quat1; dQuat2 = Quat2;

	dtmp1 = calloc(4, sizeof(double));
	dtmp2 = calloc(4, sizeof(double)); 
	if ( dtmp2 == NULL)
	{
		st = MEM_ERROR;
		DisplayMessage( st, ERROR_TEXT );
		goto quit;
	}

	for (i=0; i<4; i++)
	{
		if (type == FLOAT_DATA) dtmp1[i] = fQuat1[i];
		else					dtmp1[i] = dQuat1[i];
	}

	dtmp2[0] = 0.0;
	dtmp2[1] = sin(0.5*Angle1);
	dtmp2[2] = 0.0;
	dtmp2[3] = cos(0.5*Angle1);

	QuaternionMult(dtmp1, dtmp2, dtmp1, DOUBLE_DATA);

	dtmp2[0] = sin(0.5*Angle2);
	dtmp2[1] = 0.0;
	dtmp2[2] = 0.0;
	dtmp2[3] = cos(0.5*Angle2);

	QuaternionMult(dtmp1, dtmp2, dtmp1, DOUBLE_DATA);

	dtmp2[0] = 0.0;
	dtmp2[1] = 0.0;
	dtmp2[2] = sin(0.5*Angle3);
	dtmp2[3] = cos(0.5*Angle3);

	QuaternionMult(dtmp1, dtmp2, dtmp1, DOUBLE_DATA);

	for (i=0; i<4; i++)
	{
		if (type == FLOAT_DATA) fQuat2[i] = (float)dtmp1[i];
		else					dQuat2[i] = dtmp1[i];
	}
		
quit:
	free(dtmp1);
	free(dtmp2);
	return(st);
}

/****************************************************************************
	Change Log:
	$Log: MathFnc.c,v $
	Revision 1.7  2000/03/21 23:38:45  jzhao
	Fixed uninitialized variables.
	
	Revision 1.6  2000/02/24 01:06:47  jzhao
	Modified for port to NT
	
	Revision 1.5  2000/02/24 00:39:38  jzhao
	Modified for port to NT
	
	Revision 1.4  2000/02/23 21:14:08  jzhao
	Add missing return values.
	
	Revision 1.3  2000/02/23 17:33:08  jzhao
	Added QuaternionNormalize function.
	
	Revision 1.2  2000/02/09 23:50:29  jzhao
	Added DirCosMatrix2Quaternion function.
	
	Revision 1.1  1999/12/13 22:12:41  jzhao
	Initial Release.
	
*****************************************************************************/
