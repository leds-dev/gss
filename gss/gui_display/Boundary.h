/****************************************************************************

  Filename: Boundary.h

  Description:
	Header file for generating data point set for plotting purpose.

 ****************************************************************************/

#ifndef _BOUNDARY_H
#define _BOUNDARY_H

#include "mathlib.h"

void Update_Rectangle_Boundary ( double obj_to_eci[3][3],
								 double corners_in_eci[4][3],
								 RA_DEC boundary_pt_pos[],
								 int num_of_points,
								 double FOV_H,
								 double FOV_V);

void Update_Circle_Boundary (double obj_in_eci[3],
							 RA_DEC boundary_pt_pos[],
							 int num_of_points,
							 double ang_radius);

#endif  /* _BOUNDARY_H */
