/**************************************************************************
  This file defines the interface functions for Tcl to get the relevant
  data to draw the skymap
 **************************************************************************/

#ifndef TCL_DISPLAY_INTER_H
#define TCL_DISPLAY_INTER_H

#include "EPHGlobal.h"

char *ST_Stars (int st_id);				/* Stars in ST FOV */

char *ST_Object (int st_id);			/* Celestial objects in ST FOV */

char *ST_North (int st_id);				/* North Pole direction */

char *ST_Y (int st_id);					/* Body Y direction */

char *ST_Z (int st_id);					/* Body Z direction */

char *Sky_ST (int st_id);				/* Star tracker FOV in Skymap */

char *Sky_Object (EPHEM_OBJ_type obj_id, double radius);

char *Sky_Annulus (int st_id);

char *Sky_RoughBoresight (int st_id, double rough_radius);

char *Sky_Body_Axis (int axis_id);

char *Sky_T_And_C_Stayout (int tc_obj_id);

char *Sky_InstOptStayout ();

char *Sky_InstCoolStayout ();

#endif /* TCL_DISPLAY_INTER_H */
