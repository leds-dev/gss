#include <stdio.h>
#include <math.h>
#include <MathDef.h>
#include <MathFnc.h>
#include <Boundary.h>
#include <PreparePlotData.h>
#include <Test.h>

#define BUF_SIZE	5000
#define ST_FOV_SIZE 8*DTR  /* Star tracker field of view */
#define SUN_STAY_OUT_ZONE 30*DTR  /* Sun Stay out Zone radius */
#define SQRT2		1.414213562

double theta[16] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				  -30.0*DTR, 30.0*DTR, -89.99*DTR, 89.99*DTR, 75*DTR,
				  -80.0*DTR, 80.0*DTR, -80.0*DTR, -60*DTR, 60*DTR};
				  
double phi[16] = {0.0, 15.0*DTR, 30.0*DTR, 90.0*DTR, 180.0*DTR, -25*DTR,
 				  0.0, 15.0*DTR, 0.0, 0.0, 60.0*DTR, 180.0*DTR, 180.0*DTR,
				  -60*DTR, 0.0*DTR, 180*DTR};

double obj_sizes[16] = {90.0*DTR, 30*DTR, 0.25*DTR, 80.0*DTR, 100.0*DTR,
                       10.0*DTR, 80*DTR, 1.0*DTR, 4.0*DTR, 4.0*DTR, 5.0*DTR,
                       10.0*DTR, 10*DTR, 100*DTR, 40.0*DTR, 50.0*DTR};

double eph_obj_in_eci[16][3];

double psi1[12] = {0.0, 0.0*DTR, 0.0*DTR, 4.0*DTR, 176.0*DTR,
				  4.0*SQRT2*DTR, (180.0 - 4.0*SQRT2)*DTR,
				  30*DTR, 90*DTR, 120*DTR, 4.0*DTR, 4.0*DTR};

double psi2[12] = {0.0, 4.0*DTR, -4.0*DTR, 0.0*DTR, 0.0*DTR,
				  45.0*DTR, 225*DTR,
				  270*DTR, 300*DTR, 315*DTR, 90.0*DTR, 270.0*DTR};

double psi3[12] = {0.0, 0.0*DTR, 90*DTR, 180*DTR, 270*DTR,
				  45.0*DTR, 225*DTR,
				  0.0*DTR, 90.0*DTR, 180.0*DTR, 0.0*DTR, 0.0*DTR};

double STs_fov_h[10] = {20.0*DTR, 8.0*DTR, 8.0*DTR, 8.0*DTR, 8.0*DTR,
						8.0*DTR, 8.0*DTR, 8.0*DTR, 8.0*DTR, 8.0*DTR};

double STs_fov_v[10] = {10.0*DTR, 8.0*DTR, 8.0*DTR, 8.0*DTR, 8.0*DTR,
						8.0*DTR, 8.0*DTR, 8.0*DTR, 8.0*DTR, 8.0*DTR};

double STs_to_eci_data[3][3][3] ={{{ 1.0, 0.0, 0.0 },
					     	 { 0.0, 0.0, 1.0 },
					     	 { 0.0,-1.0, 0.0 } },
					     	{{ 0.0, 0.0, 1.0 },
					     	 { 0.0, 1.0, 0.0 },
							 {-1.0, 0.0, 0.0 } },
					     	{{ 0.9999999, 0.0000, 0.0000001 },
					     	 { 0.0, 0.99999999, 0.0 },
							 { -0.0000001, 0.00, 0.99999999}}};

double STs_to_eci[3][3][3];

FILE *LogFp, *file_index;

int main ()
{
	FILE *fp;
	char *s;
	int i, j;

	char choice;

	double euler1[3][3], euler2[3][3], euler3[3][3], rot_mat[3][3];

	double boresight[3] = {0.0, 0.0, 1.0};

	file_index = fopen ("temp_1.m", "w");

	printf ("Enter your choice:(o)for eph object,(t)for star tracker FOV\n");

	scanf ("%c", &choice);

	s = (char *)calloc (BUF_SIZE, sizeof(char));

	LogFp = fopen ("test.log", "w");

	fp = fopen ("test.dat", "w");

	switch (choice)
	{
		case 'o':

			for (i = 0; i < 16; i++)
			{
				eph_obj_in_eci[i][0] = cos(theta[i])*cos(phi[i]);
				eph_obj_in_eci[i][1] = cos(theta[i])*sin(phi[i]);
				eph_obj_in_eci[i][2] = sin(theta[i]);

				fprintf (fp, "\n\n##These are the data for object %d Stayout Zone: \n\n", i);

				strcpy (s, GetCircleAroundEphObjectData(i, obj_sizes[i]));

				fprintf (fp, "%s", s);

			}

			break;

		case 't':

			for (i = 0; i < 12; i++)
			{

				euler1[0][0] = cos(psi1[i]);
				euler1[0][1] = 0.0;
				euler1[0][2] = sin(psi1[i]);
				euler1[1][0] = 0.0;
				euler1[1][1] = 1.0;
				euler1[1][2] = 0.0;
				euler1[2][0] = -euler1[0][2];
				euler1[2][1] = 0.0;
				euler1[2][2] = euler1[0][0];

				euler2[0][0] = cos(psi2[i]);
				euler2[0][1] = -sin(psi2[i]);
				euler2[0][2] = 0.0;
				euler2[1][0] = -euler2[0][1];
				euler2[1][1] = euler2[0][0];
				euler2[1][2] = 0.0;
				euler2[2][0] = 0.0;
				euler2[2][1] = 0.0;
				euler2[2][2] = 1.0;

				euler3[0][0] = cos(psi3[i]);
				euler3[0][1] = -sin(psi3[i]);
				euler3[0][2] = 0.0;
				euler3[1][0] = -euler3[0][1];
				euler3[1][1] = euler3[0][0];
				euler3[1][2] = 0.0;
				euler3[2][0] = 0.0;
				euler3[2][1] = 0.0;
				euler3[2][2] = 1.0;

				MatrixMult (euler1, euler3, rot_mat, 3, 3, 3, DOUBLE_DATA);
				MatrixMult (euler2, rot_mat, rot_mat, 3, 3, 3, DOUBLE_DATA);

				for (j = 0; j < 3; j++)
				{
					boresight[0] = 0.0;
					boresight[1] = 0.0;
					boresight[2] = 1.0;

					memcpy (STs_to_eci[j], STs_to_eci_data[j], 3*3*sizeof(double));

					MatrixMult (rot_mat, STs_to_eci[j], STs_to_eci[j],
								3, 3, 3, DOUBLE_DATA);

					MatrixMult (rot_mat, boresight, boresight,
								3, 3, 1, DOUBLE_DATA);

					printf ("ST %d boresight vector is %f %f %f.\n",
					j+1, boresight[0], boresight[1], boresight[2]);
				}
				fprintf (fp, "\n\n##These are number %d data set for ST1 FOV: \n\n",i);

				strcpy (s, GetStarTrackerFovData(0));

				fprintf (fp, "%s", s);

				fprintf (fp, "\n\n##These are number %d data set for ST2 FOV: \n\n",i);

				strcpy (s, GetStarTrackerFovData(1));

				fprintf (fp, "%s", s);

				fprintf (fp, "\n\n##These are number %d data set for ST3 FOV: \n\n",i);

				strcpy (s, GetStarTrackerFovData(2));

				fprintf (fp, "%s", s);

			}

			break;

		default:

			break;

	}

	fprintf (fp, "\n");

	fclose (fp);

	free (s);

	fclose (LogFp);

	fclose (file_index);

	return 0;
}

double * ephem_get_eci_pos (int id)
{
	return(eph_obj_in_eci[id]);
}

double GetObjectSize (int id)
{
	return obj_sizes[id];
}

void GetST2ECI (int id, double st_to_eci[3][3])
{
	st_to_eci[0][0] = STs_to_eci[id][0][0];
	st_to_eci[0][1] = STs_to_eci[id][0][1];
	st_to_eci[0][2] = STs_to_eci[id][0][2];
	st_to_eci[1][0] = STs_to_eci[id][1][0];
	st_to_eci[1][1] = STs_to_eci[id][1][1];
	st_to_eci[1][2] = STs_to_eci[id][1][2];
	st_to_eci[2][0] = STs_to_eci[id][2][0];
	st_to_eci[2][1] = STs_to_eci[id][2][1];
	st_to_eci[2][2] = STs_to_eci[id][2][2];
}

void GetSTFovSize (int id, double *h, double *v)
{
	*h = STs_fov_h[id];
	*v = STs_fov_v[id];
}
