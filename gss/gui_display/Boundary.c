/***************************************************************************

 Filename: Boundary.c

 Description: This file is responsible to generate a set of points
			  to form a boundary for a retangle and a circle.

 ***************************************************************************/

#include <math.h>

#include "MathAppliFnc.h"
#include "Boundary.h"

#define EPSILON 1e-6

/*****************************************************************************
  This function generates data points for drawing the boundary of a rectangle.
 *****************************************************************************/
void Update_Rectangle_Boundary ( double obj_to_eci[3][3],
								 double corners_in_eci[4][3],
								 RA_DEC boundary_pt_pos[],
								 int num_of_points,
								 double FOV_H,
								 double FOV_V)
{
	double half_length = tan (0.5* FOV_H);
	double half_width = tan (0.5* FOV_V);

    /*
	   Here Dimension 4 represents four corners, the convention goes as this:
	   0 --- Left Bottom
	   1 --- Right Bottom
	   2 --- Right Top
	   3 --- Left Top
							  ^ y
                              |
	 3 +------------+ 2       +---> x
	   |            ^          
	   |            |
	   |            | V
	   |            |
	   |            |
	 0 +<-----------+ 1
			 H
     */

    double corners_in_obj[4][3];
	double single_pt_pos[3];
	RA_DEC boundary_ra_dec;

	int i, j, k;

    /* Initialize four corner coordinate */

	corners_in_obj[0][0] = -half_length;
	corners_in_obj[0][1] = -half_width;
	corners_in_obj[0][2] =  1.0;

	corners_in_obj[1][0] =  half_length;
	corners_in_obj[1][1] = -half_width;
	corners_in_obj[1][2] =  1.0;

	corners_in_obj[2][0] =  half_length;
	corners_in_obj[2][1] =  half_width;
	corners_in_obj[2][2] =  1.0;

	corners_in_obj[3][0] = -half_length;
	corners_in_obj[3][1] =  half_width;
	corners_in_obj[3][2] =  1.0;

    for ( i = 0; i < 4; i++ )
		MatrixMult ( obj_to_eci, corners_in_obj[i],
					 corners_in_eci[i], 3, 3, 1, DOUBLE_DATA); 

    for ( i = 0; i < 3; i++ )
	{
		for ( j= 0; j <= num_of_points; j++ )
		{
			for ( k = 0; k < 3; k++ )
				single_pt_pos[k] = corners_in_eci[i][k] +
					(double)j/(double)num_of_points*
						(corners_in_eci[i+1][k] - corners_in_eci[i][k]);

			VectorNormalize (single_pt_pos, single_pt_pos, DOUBLE_DATA);

			if (fabs(single_pt_pos[0]) + fabs(single_pt_pos[1]) < EPSILON)
			{
				for ( k = 0; k < 3; k++ )
					single_pt_pos[k] = corners_in_eci[i][k] +
						((double)j/(double)num_of_points + EPSILON)*
							(corners_in_eci[i+1][k] - corners_in_eci[i][k]);

				VectorNormalize (single_pt_pos, single_pt_pos, DOUBLE_DATA);
			}

			ECI2RAscenDeclina(single_pt_pos, &boundary_ra_dec.ra,
							  &boundary_ra_dec.dec, DOUBLE_DATA);

			boundary_pt_pos[j+i*(num_of_points+1)] = boundary_ra_dec;
		}
	}

	for ( j = 0; j <= num_of_points; j++ )
	{
		for ( k = 0; k < 3; k++ )
			single_pt_pos[k] = corners_in_eci[3][k] +
				(double)j/(double)num_of_points*
					(corners_in_eci[0][k] - corners_in_eci[3][k]);

		VectorNormalize (single_pt_pos, single_pt_pos, DOUBLE_DATA);

		if (fabs(single_pt_pos[0]) + fabs(single_pt_pos[1]) < EPSILON)
		{
			for ( k = 0; k < 3; k++ )
				single_pt_pos[k] = corners_in_eci[3][k] +
					((double)j/(double)num_of_points + EPSILON)*
						(corners_in_eci[0][k] - corners_in_eci[3][k]);

			VectorNormalize (single_pt_pos, single_pt_pos, DOUBLE_DATA);
		}

		ECI2RAscenDeclina(single_pt_pos, &boundary_ra_dec.ra,
						  &boundary_ra_dec.dec, DOUBLE_DATA);

		boundary_pt_pos[j+3*(num_of_points+1)] = boundary_ra_dec;
	}
}

/*****************************************************************************
  This function generates data points for drawing the boundary of a circle.
 *****************************************************************************/
void Update_Circle_Boundary (double obj_in_eci[3],
							 RA_DEC boundary_pt_pos[],
							 int num_of_points,
							 double ang_radius)
{
	double single_pt_pos_eci[3];

	/* Compute the small angle for rotation */
	double half_del_ang = PI/(double) num_of_points;
	double cos_half_del_ang = cos (half_del_ang);
	double sin_half_del_ang = sin (half_del_ang);

	double delta_q[4], tiny_delta_q[4];
	double rotation_matrix[3][3], tiny_rotation_matrix[3][3];
	double modified_single_pt_pos_eci[3];
	RA_DEC obj_center, point_on_stayout_circle;
	int i;

	/* Form a quaternion rotating by the small
	   angle about vector obj_in_eci */
	delta_q[0] = sin_half_del_ang*obj_in_eci[0];
	delta_q[1] = sin_half_del_ang*obj_in_eci[1];
	delta_q[2] = sin_half_del_ang*obj_in_eci[2];
	delta_q[3] = cos_half_del_ang;

	/* This is a tiny rotation to avoid a singular point */
	tiny_delta_q[0] = EPSILON*obj_in_eci[0];
	tiny_delta_q[1] = EPSILON*obj_in_eci[1];
	tiny_delta_q[2] = EPSILON*obj_in_eci[2];
	tiny_delta_q[3] = 1.0 - 0.5*EPSILON*EPSILON;

	/* Get the right ascension and declination of the center */
	ECI2RAscenDeclina(obj_in_eci, &obj_center.ra,
					  &obj_center.dec, DOUBLE_DATA);


	/* Pick one point in the stayout zone with the same right ascension */

	point_on_stayout_circle.ra =
		obj_center.ra;

	if (obj_center.dec > 0.0)
		point_on_stayout_circle.dec = obj_center.dec - ang_radius;
	else
		point_on_stayout_circle.dec = obj_center.dec + ang_radius;

	boundary_pt_pos[0] = point_on_stayout_circle;

	/* Convert the dec and ra to eci vector */
	RAscenDeclina2ECI (point_on_stayout_circle.ra,
					   point_on_stayout_circle.dec,
					   single_pt_pos_eci,
					   DOUBLE_DATA);

	/* Generate the rotation matrix */
	Quaternion2DirCosMatrix (delta_q, rotation_matrix, DOUBLE_DATA);


	/* Generate the tiny rotation matrix */
	Quaternion2DirCosMatrix (tiny_delta_q, tiny_rotation_matrix, DOUBLE_DATA);

	for ( i = 1; i < num_of_points; i++ )
	{
		/* Rotate until go a full circle about the center */
		MatrixMult (rotation_matrix, single_pt_pos_eci, single_pt_pos_eci,
					3, 3, 1, DOUBLE_DATA);

		if (fabs(single_pt_pos_eci[0]) + fabs(single_pt_pos_eci[1]) < EPSILON)
		{
			/* Perform an extra rotation */
			MatrixMult (tiny_rotation_matrix,
						single_pt_pos_eci,
						modified_single_pt_pos_eci,
						3, 3, 1, DOUBLE_DATA);

			/* Convert the vector in eci to right ascension and declination */
			ECI2RAscenDeclina(modified_single_pt_pos_eci,
							  &boundary_pt_pos[i].ra,
							  &boundary_pt_pos[i].dec,
							  DOUBLE_DATA);
		}
		else
		{
			/* Convert the vector in eci to right ascension and declination */
			ECI2RAscenDeclina(single_pt_pos_eci,
							  &boundary_pt_pos[i].ra,
							  &boundary_pt_pos[i].dec,
							  DOUBLE_DATA);
		}
	}
}
