#ifndef _PREPARE_PLOT_DATA_H
#define _PREPARE_PLOT_DATA_H

#define	DATA_FORMAT_SIZE	40		/* This is the number of characters
									   used by each data             */

#define	TOT_POINTS_PER_OBJ	80	/* Total number of points used for drawing */
								/* stayout zone and the objs               */

#define	TOT_POINTS_PER_FOV	80	/* Total number of points used for drawing */
								/* field of view                           */

int PrintCircularData (char *s, double radius, double obj_in_eci[3]);

int PrintRectangleData (char *s, double fov_h, double fov_v,
						double obj_to_eci[3][3]);

#endif  /* _PREPARE_PLOT_DATA_H */
