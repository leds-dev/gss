/********************************************************************

  Filename: WrapAround.c

  Description:

	This file deals with the wrap around for Rectangular or Circular
  shape.

  *********************************************************************/

#include <math.h>
#include "MathFnc.h"
#include "WrapAround.h"

#define EPSILON  1e-6

/************************************************************************
  This function determines whether a rectangle includes the North or the
  South Poles.
 ************************************************************************/
wrap_type RectangleWrapAroundPole (double fov_h, double fov_v,
								 double obj_to_eci[3][3])
{
	double pole_vec [3] = {0.0, 0.0, 1.0};
	double eci_to_obj[3][3];
	double alpha, beta;
	int i,j;

	for ( i = 0; i < 3; i++)
		for ( j = 0; j < 3; j++)
			eci_to_obj [i][j] = obj_to_eci [j][i]; /* Transpose for Inverse */

	MatrixMult (eci_to_obj, pole_vec, pole_vec, 3, 3, 1, DOUBLE_DATA);

	if (pole_vec[2] > EPSILON) /* possible to wrap north pole */
	{
		alpha = atan2 (pole_vec[0], pole_vec[2]);
		beta  = atan2 (pole_vec[1], pole_vec[2]);

		if ((fabs(alpha) < 0.5*fov_h) && (fabs(beta) < 0.5*fov_v))
			return (Wrap_N_Pole);
		else
			return (None);
	}
	else if (pole_vec[2] < - EPSILON) /* possible to wrap south pole */
	{
		alpha = atan2 (pole_vec[0], -pole_vec[2]);
		beta  = atan2 (pole_vec[1], -pole_vec[2]);

		if ((fabs(alpha) < 0.5*fov_h) && (fabs(beta) < 0.5*fov_v))
			return (Wrap_S_Pole);
		else
			return (None);
	}
	else
		return (None);

}

/************************************************************************
  This function determines whether the rectangle acrosses the right
  ascension zero line.
 ************************************************************************/
int RectangleAcrossRightAscentZero (double ra_1, double ra_2,
								    double ra_3, double ra_4)
{
	return ((fabs(ra_1 - ra_3) > PI) || (fabs (ra_2 - ra_4) > PI));
}

/************************************************************************
  This function determines whether a circle includes the North or the
  South Poles as well as acrosses right ascension zero line.
 ************************************************************************/
wrap_type CircleWrapAroundPole (double ang_radius, double center_in_eci[3])
{
	double cos_ang = cos(ang_radius);

	if (center_in_eci[2] > cos_ang)
		return Wrap_N_Pole;
	else if (center_in_eci[2] < - cos_ang)
		return Wrap_S_Pole;
	else if ((center_in_eci[0] > 0.0) &&
			 (fabs(center_in_eci[1]) < sin(ang_radius)))
		return Across_RA_Zero;
	else
		return None;
}

/************************************************************************
  This function computes the declination of the point with zero right
  ascension on a circle including either north pole or the south pole.
 ************************************************************************/
double DeclOfZeroRAPointOnWrappingCircle (double ang_radius,
										  double center_in_eci[3])
{
	double phase, cos_value;

	if (center_in_eci[2] > EPSILON)			/* Wrapping North Pole */
	{

		phase = atan2 (center_in_eci[0], center_in_eci[2]);
											/* phase is in -pi/2,+pi/2 */

		cos_value = cos (ang_radius) /
					sqrt (center_in_eci[0]*center_in_eci[0] +
						  center_in_eci[2]*center_in_eci[2] );

		if (fabs(cos_value) <= 1.0)
			return (0.5*PI - acos (cos_value) - phase);
		else
			return PI;	/* inconsistent case occurred */
	}
	else if (center_in_eci[2] < -EPSILON)	/* Wrapping South Pole */
	{

		phase = atan2 (-center_in_eci[0], -center_in_eci[2]);
											/* phase is in -pi/2,+pi/2 */

		cos_value = cos (ang_radius) /
					sqrt (center_in_eci[0]*center_in_eci[0] +
						  center_in_eci[2]*center_in_eci[2] );

		if (fabs(cos_value) <= 1.0)
			return (-0.5*PI + acos (cos_value) - phase);
		else
			return PI;

	}
	else		/* Should not trip here normally */

		return PI;

}


/***************************************************************************
  This function computes the declinations of the two points on the circle
  acrossing right ascension zero line.
 ***************************************************************************/
int DeclsOfTwoZeroRAPointsOnCircle (double ang_radius,
									double center_in_eci[3],
									double declinations[2])
{
	double phase, cos_value;
	int status = 0;

	if (center_in_eci[0] > 0.0)
	{

		phase = atan2 (center_in_eci[2], center_in_eci[0]);
											/* phase is in -pi/2,+pi/2 */

		cos_value = cos (ang_radius) /
					sqrt (center_in_eci[0]*center_in_eci[0] +
						  center_in_eci[2]*center_in_eci[2] );

		if (fabs(cos_value) <= 1.0)
		{
			declinations[0] = phase - acos (cos_value);
			declinations[1] = phase + acos (cos_value);
		}
		else	/* inconsistent situation */
		{
			status = 1;

			declinations[0] = 0.0;
			declinations[1] = 0.0;
		}
	}
	else	/* Should not trip here normally, facing the wrong direction */
	{
		status = 1;

		declinations[0] = 0.0;
		declinations[1] = 0.0;
	}

	return (status);
}

/**************************************************************************
  This function does a linear interpolation to find the position of the
  point with right ascension zero on the segment between two known
  positions.
 **************************************************************************/
int interpolate (double vec1[3], double vec2[3], double vec3[3])
{
	double lambda;

	if (vec1[1] == 0.0)
	{
		vec3[0] = vec1[0];
		vec3[1] = vec1[1];
		vec3[2] = vec1[2];
	}
	else if (vec2[2] == 0.0)
	{
		vec3[0] = vec2[0];
		vec3[1] = vec2[1];
		vec3[2] = vec2[2];
	}
	else
	{
		lambda = - vec1[1]/(vec2[1] - vec1[1]);

		vec3[1] = 0.0;
		vec3[0] = vec1[0]*(1.0-lambda) + vec2[0]*lambda;
		vec3[2] = vec1[2]*(1.0-lambda) + vec2[2]*lambda;
	}

	return (int)(vec3[0] >= 0.0);
}


/**************************************************************************
  This function computes the declination of the point with right ascension
  zero on the a rectanglar boundary.
 **************************************************************************/
double DeclOfZeroRAPointOnWrappingRectangle (double corner_in_eci[4][3])
{
	double zero_ra_pos[3] = {0.0, 0.0, 1.0};

	if ((corner_in_eci[0][1]*corner_in_eci[1][1] <= 0) &&
		interpolate(corner_in_eci[0], corner_in_eci[1], zero_ra_pos))

		return atan2(zero_ra_pos[2], zero_ra_pos[0]);

	else if ((corner_in_eci[0][1]*corner_in_eci[3][1] <= 0) &&
		interpolate(corner_in_eci[0], corner_in_eci[3], zero_ra_pos))

		return atan2(zero_ra_pos[2], zero_ra_pos[0]);

	else if ((corner_in_eci[2][1]*corner_in_eci[1][1] <= 0) &&
		interpolate(corner_in_eci[2], corner_in_eci[1], zero_ra_pos))

		return atan2(zero_ra_pos[2], zero_ra_pos[0]);

	else if ((corner_in_eci[2][1]*corner_in_eci[3][1] <= 0) &&
		interpolate(corner_in_eci[2], corner_in_eci[3], zero_ra_pos))

		return atan2(zero_ra_pos[2], zero_ra_pos[0]);

	else		/* There is no point on the rectangle with RA=0 */
		/* This is a wrong situation. Does not wrap pole */
		return PI;

}

/***************************************************************************
  This function goes through the data points to find out the indices of the
  points just before acrossing the right ascension zero. The direction of
  the acrossing is also recorded.
 ***************************************************************************/
int EndPointIndex (int size, RA_DEC pos_of_points[],
				   int end_points[2], int dir[2])
{
	int i;
	int status = NO_ACROSSING;

	end_points[0] = 0;
	end_points[1] = 0;

	for ( i = 0; i < size-1; i++)
	{
		if ((pos_of_points[i+1].ra - pos_of_points[i].ra) < - PI )
		{
			end_points[status] = i;
			dir[status] = 1;		/* from 2Pi to 0 */
			status ++;

			if (status > 2)
				return WRONG_ACROSSING;		/* Anomalous case */
		}
		else if ((pos_of_points[i+1].ra - pos_of_points[i].ra) > PI )
		{
			end_points[status] = i;
			dir[status] = -1;		/* from 0 to 2Pi */
			status ++;

			if (status > 2)
				return WRONG_ACROSSING;		/* Anomalous case */
		}
	}

	if ((pos_of_points[0].ra - pos_of_points[size-1].ra) < - PI )
	{
		end_points[status] = size-1;
		dir[status] = 1;			/* from 2Pi to 0 */
		status ++;

		if (status > 2)
			return WRONG_ACROSSING;			/* Anomalous case */
	}
	else if ((pos_of_points[0].ra - pos_of_points[size-1].ra) > PI )
	{

		end_points[status] = size-1;
		dir[status] = -1;			/* from 0 to 2Pi */
		status ++;

		if (status > 2)
			return WRONG_ACROSSING;		/* Anomalous case */
	}

	return status;
}

/*************************************************************************
   This routine generates two sets of data out of one set, the two sets
   generated represent the same point with different domain constrain for
   the right ascension. The right ascension of the left set is between
   -0.9*Pi and 1.1*Pi, while the right ascension for the right set is
   between 0.9*Pi and 2.9*Pi.
 **************************************************************************/

void MappingLeftandRight ( int size, RA_DEC points_in_mid[],
						   RA_DEC left_points[],
						   RA_DEC right_points[])
{
	int index;

	for (index = 0; index < size; index ++)
	{
		left_points[index] = points_in_mid[index];
		right_points[index] = points_in_mid[index];

		if (left_points[index].ra >= 1.1*PI)
			left_points[index].ra -= 2.0*PI;

		if (right_points[index].ra <= 0.9*PI)
			right_points[index].ra += 2.0*PI;
	}
}


/***************************************************************************
  This function extends the data set to left and right so that the right
  ascension changes continuously from the first point to the last one.
 ***************************************************************************/

void ExtendLeftandRight ( int size, RA_DEC points_in_mid[],
						   RA_DEC left_points[],
						   RA_DEC right_points[])
{
	int index;

	left_points[0] = points_in_mid[0];
	right_points[size-1] = points_in_mid[size-1];

	for (index = 1; index < size; index ++)
	{
		left_points[index] = points_in_mid[index];

		if (left_points[index].ra - left_points[index-1].ra > PI)

			left_points[index].ra -= 2.0*PI;

		else if (left_points[index].ra - left_points[index-1].ra < -PI)

			left_points[index].ra += 2.0*PI;

		right_points[size-index-1] = points_in_mid[size-index-1];

		if (right_points[size-index-1].ra - right_points[size-index].ra > PI)

			right_points[size-index-1].ra -= 2.0*PI;

		else if (right_points[size-index-1].ra -
		    right_points[size-index].ra < -PI)

			right_points[size-index-1].ra += 2.0*PI;
	}
}
