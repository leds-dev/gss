#include <math.h>
#include <string.h>

#include "stars.h"
#include "draw_objs.h"
#include "MathDef.h"
#include "CISInterface.h"
#include "MathFnc.h"
#include "WrapAround.h"
#include "PreparePlotData.h"
#include "TclDisplayInter.h"

#define ST_FOV_H			8.0*DTR /* 8 deg */
#define ST_FOV_V			8.0*DTR /* 8 deg */

#define St2Bdy(i)	&sim_data.st[i].st2body.dcm
#define Bdy2St(i)	&sim_data.st[i].st2body.dcm_r
#define Eci2Bdy		&sim_data.eci2body.dcm
#define Bdy2Eci		&sim_data.eci2body.dcm_r

/* This routine generates the Hs and Vs of relevant stars in FOV */
char *ST_Stars (int st_id)
{
	char fov_star_data[MAXNUM_STARS_FOV*DATA_FORMAT_SIZE];
	char buf[10+DATA_FORMAT_SIZE];
	TrackerFOV_HV_type	star_info;
	double eci2st[3][3];
	int i;

	/* Construct the dir cos matrix from ECI to ST */
	MatrixMult(Bdy2St(st_id), Eci2Bdy, eci2st, 3, 3, 3, DOUBLE_DATA);

	StarsInTrackerFOV (st_id, (DirCosMatrix_type *)eci2st, &star_info);

	strcpy (fov_star_data, "");

	for ( i = 0; i < star_info.NumStars; i++)
	{
		sprintf (buf, " %d %10.6f %10.6f",
				 star_info.Stars[i].ASC_ID,
				 star_info.Stars[i].HPos,
				 star_info.Stars[i].VPos);

		strcat (fov_star_data, buf);
	}

	return (fov_star_data);
}

/***************************************************************************
  This function provides the H and Vs for ephemeris object in the specified
  star tracker reference frame
 ***************************************************************************/
char *ST_Object (int st_id)
{
	static char eph_obj_data[2*MAXNUM_STARS_FOV*DATA_FORMAT_SIZE];
	double eci_to_st[3][3], obj_in_eci[3], obj_in_st[3];
	double epsilon = 1e-6;
	double h, v;
	int obj_indx;

	/* Generate eci_to_st */
	MatrixMult(Bdy2St(st_id), Eci2Bdy, eci_to_st, 3, 3, 3, DOUBLE_DATA);

	/* Initialize the string */
	strcpy (eph_obj_data, "");

	for ( obj_indx = 0; obj_indx < MAXNUM_STARS_FOV; obj_indx ++)
	{
		/* Get eph obj vector in ECI */
		memcpy(obj_in_eci, (double *)ephem_get_eci_pos(obj_indx),
                            3*sizeof(double));

		MatrixMult(eci_to_st, obj_in_eci, obj_in_st, 3, 3, 1, DOUBLE_DATA);

		if (obj_in_st[2] > epsilon)
		{
			h = obj_in_st[0]/obj_in_st[2];
			v = obj_in_st[1]/obj_in_st[2];

			sprintf(eph_obj_data, "%d %10.6f 10.6%",obj_indx, h, v);
		}
	}

	return (eph_obj_data);
}

/***************************************************************************
 This function provides H,V data for the North/South Pole if it is in star
 tracker field of view otherwise it provides a direction angle of north
 relative to the H axis of the FOV
 ***************************************************************************/
char *ST_North (int st_id)
{
	static char pole_data[2*DATA_FORMAT_SIZE];
	double st_to_eci[3][3];
	double epsilon = 1e-6;
	double h, v;

	/* Get st_to_eci */
	MatrixMult(Bdy2Eci, St2Bdy(st_id), st_to_eci, 3, 3, 3, DOUBLE_DATA);

	/* North Pole unit vector in star tracker frame is
	   (st_to_eci[2][0], st_to_eci[2][1], st_to_eci[2][2]) */

	/* Check whether the star tracker views the poles */
	if (st_to_eci[2][2] > epsilon) /* View North */
	{
		h = st_to_eci[2][0]/st_to_eci[2][2];
		v = st_to_eci[2][1]/st_to_eci[2][2];

		if ((fabs(h) < tan(0.5*ST_FOV_H)) && (fabs(v) < tan(0.5*ST_FOV_V)))
			sprintf(pole_data, "N %10.6f 10.6%", h, v);
		else
			sprintf(pole_data, "%10.6f",
					atan2(st_to_eci[2][1], st_to_eci[2][0]));
	}
	else if (st_to_eci[2][2] < - epsilon) /* View South */
	{
		h = st_to_eci[2][0]/st_to_eci[2][2];
		v = st_to_eci[2][1]/st_to_eci[2][2];

		/* South pole in field of view ? */
		if ((fabs(h) < tan(0.5*ST_FOV_H)) && (fabs(v) < tan(0.5*ST_FOV_V)))
			sprintf(pole_data, "S %10.6f 10.6%", h, v);
		else
			sprintf(pole_data, "%10.6f",
					atan2(st_to_eci[2][1], st_to_eci[2][0]));
	}
	else
		sprintf(pole_data, "%10.6f", atan2(st_to_eci[2][1], st_to_eci[2][0]));

	return (pole_data);
}

/***************************************************************************
 This function provides data for the direction of Y axis in star
 tracker frame
 ***************************************************************************/
char *ST_Y (int st_id)
{
	static char axis_data[2*DATA_FORMAT_SIZE];
	double y_dir[3]={0.0, 1.0, 0.0};

	/* Get y_axis direction in ST frame */
	MatrixMult(Bdy2St(st_id), y_dir, y_dir, 3, 1, 1, DOUBLE_DATA);

	sprintf(axis_data, "%10.6f", atan2(y_dir[1], y_dir[0]));

	return (axis_data);
}

/***************************************************************************
 This function provides data for the direction of Z axis in star
 tracker frame
 ***************************************************************************/
char *ST_Z (int st_id)
{
	static char axis_data[2*DATA_FORMAT_SIZE];
	double z_dir[3]={0.0, 0.0, 1.0};

	/* Get z_axis direction in ST frame */
	MatrixMult(Bdy2St(st_id), z_dir, z_dir, 3, 1, 1, DOUBLE_DATA);

	sprintf(axis_data, "%10.6f", atan2(z_dir[1], z_dir[0]));

	return (axis_data);
}

/****************************************************************************
  This routine provides data points for star tracker field of view  via a
  string in format:
  {..{x1 y1 x2 y2 ... xn yn} { ...} ...}
 ****************************************************************************/
char *Sky_ST (int st_id)
{
	static char plot_data[DATA_FORMAT_SIZE*(2*TOT_POINTS_PER_FOV+8)];

	double st_to_eci[3][3];

	double st_fov_h, st_fov_v;

	/* Get st_to_eci */
	MatrixMult(&sim_data.eci2body.dcm_r, St2Bdy(st_id), st_to_eci,
			   3, 3, 3, DOUBLE_DATA);

	/* Set st_fov */
	st_fov_h = ST_FOV_H;
	st_fov_v = ST_FOV_V;

	PrintRectangleData (plot_data, st_fov_h, st_fov_v, st_to_eci);

	return plot_data;
}

/****************************************************************************
  This routine provides data points for drawing stayout zone via a string in
  format:
  {..{x1 y1 x2 y2 ... xn yn} { ...} ...}
 ****************************************************************************/
char *Sky_Object (EPHEM_OBJ_type obj_id, double radius)
{
	static char plot_data[DATA_FORMAT_SIZE*TOT_POINTS_PER_OBJ+8];
	double obj_in_eci[3] = {-1.0, 0.0, 0.0};

	/* Get obj_in_eci from ephemeris */
	memcpy(obj_in_eci, (double *) ephem_get_eci_pos(obj_id), 3*sizeof(double));

	/* Check if the radius is greater than 90 degree */
	if (radius > 0.5*PI)
	{
		radius = PI - radius;
		obj_in_eci[0] = -obj_in_eci[0];
		obj_in_eci[1] = -obj_in_eci[1];
		obj_in_eci[2] = -obj_in_eci[2];
	}

	PrintCircularData(plot_data, radius, obj_in_eci);

	return plot_data;
}

/****************************************************************************
  This routine provides data points for drawing the annulus for the specified
  star tracker :
  {..{x1 y1 x2 y2 ... xn yn} { ...} ...}
 ****************************************************************************/
char *Sky_Annulus (int st_id)
{
	static char annulus_data[2*DATA_FORMAT_SIZE*TOT_POINTS_PER_OBJ+8];
	double inner_radius, outer_radius;

	GetInnerAndOuterRadius (st_id, &inner_radius, &outer_radius);

	strcpy (annulus_data, "{");

	strcat (annulus_data, Sky_Object(GSS_SUN, outer_radius));

	strcat (annulus_data, " ");

	strcat (annulus_data, Sky_Object(GSS_SUN, inner_radius));

	strcat (annulus_data, "}");

	return (annulus_data);
}

/****************************************************************************
  This function provides data for drawing a circle representing the rough
  region that the specified star tracker views.
 ****************************************************************************/
char *Sky_RoughBoresight (int st_id, double rough_radius)
{
	static char plot_data[DATA_FORMAT_SIZE*TOT_POINTS_PER_OBJ+8];
	double eci_to_st[3][3];
 
 	/* Get st_to_eci */
	MatrixMult(Bdy2St(st_id), &sim_data.eci2body.dcm, eci_to_st,
				3, 3, 3, DOUBLE_DATA);
 
	PrintCircularData (plot_data, rough_radius, (double*)eci_to_st[2]);
	return plot_data;
}

/****************************************************************************
  This function provides the right ascension and declination for the
  specified body axis: 0 -- x, 1 -- y, 2 -- z
 ****************************************************************************/
char *Sky_Body_Axis (int axis_id)
{
	static char plot_data[DATA_FORMAT_SIZE];
	RA_DEC x_pos;
	
	ECI2RAscenDeclina((double *)&sim_data.eci2body.dcm[axis_id],
					   &x_pos.ra, &x_pos.dec, DOUBLE_DATA);

	sprintf (plot_data, "%10.6f 10.6f", x_pos.ra, x_pos.dec);
	return plot_data;
}

/****************************************************************************
  This function provides data to draw the specified T&C object coverage region
 ****************************************************************************/
char *Sky_T_And_C_Stayout (int tc_obj_id)
{
	static char plot_data[DATA_FORMAT_SIZE*TOT_POINTS_PER_OBJ+8];
	double boresight[3] = {0.0, 0.0, 1.0};
	double radius;

	switch (tc_obj_id)
	{
		case 0:
			radius = body_objs.CMD_FWD.height;
			MatrixMult (&body_objs.CMD_FWD.xx2body.dcm, boresight,
						 boresight, 3, 1, 1, DOUBLE_DATA);
			break;
		case 1:
			radius = body_objs.CMD_AFT.height;
			MatrixMult (&body_objs.CMD_AFT.xx2body.dcm, boresight,
						 boresight, 3, 1, 1, DOUBLE_DATA);
			break;
		case 2:
			radius = body_objs.DSN_TM_1K_FWD.height;
			MatrixMult (&body_objs.DSN_TM_1K_FWD.xx2body.dcm, boresight,
						 boresight, 3, 1, 1, DOUBLE_DATA);
			break;
		case 3:
			radius = body_objs.DSN_TM_4K_FWD.height;
			MatrixMult (&body_objs.DSN_TM_4K_FWD.xx2body.dcm, boresight,
						 boresight, 3, 1, 1, DOUBLE_DATA);
			break;
		case 4:
			radius = body_objs.DSN_TM_1K_AFT.height;
			MatrixMult (&body_objs.DSN_TM_1K_AFT.xx2body.dcm, boresight,
						 boresight, 3, 1, 1, DOUBLE_DATA);
			break;
		case 5:
			radius = body_objs.DSN_TM_4K_AFT.height;
			MatrixMult (&body_objs.DSN_TM_4K_AFT.xx2body.dcm, boresight,
						 boresight, 3, 1, 1, DOUBLE_DATA);
			break;
		case 6:
			radius = body_objs.CDA_TM_1K_FWD.height;
			MatrixMult (&body_objs.CDA_TM_1K_FWD.xx2body.dcm, boresight,
						 boresight, 3, 1, 1, DOUBLE_DATA);
			break;
		case 7:
			radius = body_objs.CDA_TM_4K_FWD.height;
			MatrixMult (&body_objs.CDA_TM_4K_FWD.xx2body.dcm, boresight,
						 boresight, 3, 1, 1, DOUBLE_DATA);
			break;
		case 8:
			radius = body_objs.CDA_TM_1K_AFT.height;
			MatrixMult (&body_objs.CDA_TM_1K_AFT.xx2body.dcm, boresight,
						 boresight, 3, 1, 1, DOUBLE_DATA);
			break;
		case 9:
			radius = body_objs.CDA_TM_4K_AFT.height;
			MatrixMult (&body_objs.CDA_TM_4K_AFT.xx2body.dcm, boresight,
						 boresight, 3, 1, 1, DOUBLE_DATA);
		default:
			strcpy (plot_data, "");
			return plot_data;
	}

	MatrixMult (Bdy2Eci, boresight, boresight, 3, 1, 1, DOUBLE_DATA);

	PrintCircularData(plot_data, radius, boresight);
	return plot_data;
}

/****************************************************************************
  This function provides data for drawing the Instrument Optical stayout zone
 ****************************************************************************/
char *Sky_InstOptStayout ()
{
	static char plot_data[DATA_FORMAT_SIZE*(2*TOT_POINTS_PER_FOV+8)];
	double fov_h, fov_v;
	double inst_to_eci[3][3];

	fov_h = body_objs.INSTRUMENT_OPTICAL.width;
	fov_v = body_objs.INSTRUMENT_OPTICAL.height;

	MatrixMult (Bdy2Eci, &body_objs.INSTRUMENT_OPTICAL.xx2body.dcm, 
				inst_to_eci, 3, 3, 3, DOUBLE_DATA);
	
	PrintRectangleData (plot_data, fov_h, fov_v, inst_to_eci);
	return plot_data;
}
 
/****************************************************************************
  This function provides data for drawing the instrument cool stayout zone
 ****************************************************************************/
char *Sky_InstCoolStayout ()
{
	static char plot_data[DATA_FORMAT_SIZE*TOT_POINTS_PER_OBJ+8];
	double boresight[3] = {0.0, 0.0, 1.0};
	double radius;

	radius = body_objs.INSTRUMENT_COOLER.height;

	MatrixMult (&body_objs.INSTRUMENT_COOLER.xx2body.dcm, boresight,
				 boresight, 3, 1, 1, DOUBLE_DATA);

	MatrixMult (Bdy2Eci, boresight, boresight, 3, 1, 1, DOUBLE_DATA);

	PrintCircularData(plot_data, radius, boresight);
	return plot_data;
}
