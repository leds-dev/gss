#include <math.h>
#include <string.h>

#include "MathDef.h"
#include "Boundary.h"
#include "WrapAround.h"
#include "PreparePlotData.h"

void PrintPairPoints (int size, char *s, RA_DEC pos_of_pts[])
{
	int i;
	char buf[2*DATA_FORMAT_SIZE];

	sprintf (s, "%10.6f %10.6f", pos_of_pts[0].ra*RTD, pos_of_pts[0].dec*RTD);

	for ( i = 1; i < size; i++)
	{
		sprintf (buf, " %10.6f %10.6f",
				 pos_of_pts[i].ra*RTD, pos_of_pts[i].dec*RTD);
		strcat (s, buf);
	}
}

void PrintOneFovDataSet (char *buf, int pts_per_side,
						   RA_DEC obj_fov_data[])
{
	char tmp_buf[DATA_FORMAT_SIZE*TOT_POINTS_PER_FOV];

	strcpy(buf, "{");

	PrintPairPoints(pts_per_side, tmp_buf, obj_fov_data);

	strcat (buf, tmp_buf);
	
	strcat (buf, "} {");

	PrintPairPoints(pts_per_side, tmp_buf, &obj_fov_data[pts_per_side]);

	strcat (buf, tmp_buf);

	strcat (buf, "} {");

	PrintPairPoints(pts_per_side, tmp_buf, &obj_fov_data[2*pts_per_side]);

	strcat (buf, tmp_buf);

	strcat (buf, "} {");

	PrintPairPoints(pts_per_side, tmp_buf, &obj_fov_data[3*pts_per_side]);

	strcat (buf, tmp_buf);

	strcat (buf, "}");
}

int PrintCircularData (char *s, double radius, double obj_in_eci[3])
{
	char buf[DATA_FORMAT_SIZE*TOT_POINTS_PER_OBJ+8];
	RA_DEC	circle_boundary_data[TOT_POINTS_PER_OBJ];
	wrap_type wrap_status;
	double declinations[2], pole_dec;
	int endpoints[2], dir[2], status;

	wrap_status = CircleWrapAroundPole (radius, obj_in_eci);

	/* Generate the data set for plotting */
	Update_Circle_Boundary (obj_in_eci, circle_boundary_data,
							TOT_POINTS_PER_OBJ, radius);
	
	/* According the wrapping cases, construct data points for plotting */
	switch (wrap_status)
	{
		case None:

			/* This first character is the left bracket */
			strcpy(s, "{{");

			PrintPairPoints(TOT_POINTS_PER_OBJ, buf,
							circle_boundary_data);

			strcat(s, buf);

			strcat(s, "}}");

			break;

		case Wrap_S_Pole: case Wrap_N_Pole:

			/* Figure out which pole is wrapped */
			if (wrap_status == Wrap_S_Pole)
				pole_dec = -90.0;
			else
				pole_dec =  90.0;

			/* This first character is the left bracket */
			strcpy(s, "{{");

			/* Get the declination of the point with zero right ascension. */
			declinations[0] = DeclOfZeroRAPointOnWrappingCircle
							  (radius, obj_in_eci);

			if (declinations[0] == PI)
			{
				sprintf (s, "Error: anomaly occurred in computing the declination of the point with right ascension zero.\n");
				return 1;
			}
			/* Get the endpoint index just acrossing right ascension zero and
			   the direction in which the acrossing goes */
			status = EndPointIndex (TOT_POINTS_PER_OBJ,
									circle_boundary_data,
									endpoints, dir);

			/* Status should be 1 --- Only one point with zero RA */
			if (status == ONE_ACROSSING)
			{

				if (endpoints[0] == TOT_POINTS_PER_OBJ-1)
				{

					PrintPairPoints(TOT_POINTS_PER_OBJ, buf,
									circle_boundary_data);
					strcat (s, buf);
				}
				else
				{

					PrintPairPoints (TOT_POINTS_PER_OBJ-endpoints[0]-1, buf,
								 	 &circle_boundary_data[endpoints[0]+1]);

					strcat (s, buf);

					strcat (s, " ");

					PrintPairPoints (endpoints[0]+1, buf, circle_boundary_data);

					strcat (s, buf);

				}

				if (dir[0] == 1)
					sprintf (buf,
							 " %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f}}",
						 	  360.0, declinations[0]*RTD, 360.0, pole_dec,
						      0.0, pole_dec, 0.0, declinations[0]*RTD);
				else if (dir[0] == -1)
					sprintf (buf,
							 " %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f}}",
						 	  0.0, declinations[0]*RTD, 0.0, pole_dec,
						      360.0, pole_dec, 360.0, declinations[0]*RTD);

				strcat(s, buf);

			}
			else
			{
				/* Display an error message */
				sprintf (s, "Error: expecting one acrossing while status is %d\n", status);
				return 1;
				/* Not a consistent situation */
			}

			break;

		case Across_RA_Zero: 
			
			strcpy (s, "{{");

			status = DeclsOfTwoZeroRAPointsOnCircle
					 (radius, obj_in_eci, declinations);
			
			if (!status)
			{
				status = EndPointIndex (TOT_POINTS_PER_OBJ,
										circle_boundary_data,
										endpoints, dir);


				if (status == NO_ACROSSING) /* Discrete points may not wrap even though continuous would have */
				{

					PrintPairPoints(TOT_POINTS_PER_OBJ, buf,
							circle_boundary_data);

					strcat(s, buf);

					strcat(s, "}}");

				}
				else if (status != TWO_ACROSSING)
				{
					/* generating error message */
					sprintf (s, "Error: Inconsistent case: should across twice, while status is %d\n", status);
					return 1; /* Return abnormal status */
				}
				else
				{
					/* generate first set of data */

					PrintPairPoints (endpoints[1] - endpoints[0], buf,
								 	 &circle_boundary_data[endpoints[0]+1]);

					strcat(s, buf);

					if (fabs(circle_boundary_data[endpoints[1]].dec
						    - declinations[0]) <
					    fabs(circle_boundary_data[endpoints[1]].dec
						    - declinations[1]))


						sprintf (buf,
								 " %10.6f %10.6f %10.6f %10.6f} {%10.6f %10.6f %10.6f %10.6f ",
								 (double)(1+dir[1])*180.0, declinations[0]*RTD,
								 (double)(1+dir[1])*180.0, declinations[1]*RTD,
								 (double)(1-dir[1])*180.0, declinations[1]*RTD,
								 (double)(1-dir[1])*180.0, declinations[0]*RTD);
					else

						sprintf (buf,
								 " %10.6f %10.6f %10.6f %10.6f} {%10.6f %10.6f %10.6f %10.6f ",
								 (double)(1+dir[1])*180.0, declinations[1]*RTD,
								 (double)(1+dir[1])*180.0, declinations[0]*RTD,
								 (double)(1-dir[1])*180.0, declinations[0]*RTD,
								 (double)(1-dir[1])*180.0, declinations[1]*RTD);

						
					strcat (s, buf);

					PrintPairPoints (TOT_POINTS_PER_OBJ-endpoints[1]-1, buf,
								 	 &circle_boundary_data[endpoints[1]+1]);

					strcat (s, buf);

					strcat (s, " ");

					PrintPairPoints (endpoints[0]+1, buf, circle_boundary_data);

					strcat (s, buf);

					strcat (s, "}}");

				}
			}
			else
			{
				sprintf (s, "Error: Anomaly occured in computing the two declinations.");
				return 1; /* Something is wrong */
			}

			break;

	}

	return 0;
}

int PrintRectangleData (char *s, double fov_h, double fov_v,
						double obj_to_eci[3][3])
{
	char buf[DATA_FORMAT_SIZE*(2*TOT_POINTS_PER_FOV+8)];
	RA_DEC	obj_fov_data[TOT_POINTS_PER_FOV+4];
	RA_DEC	left_and_right[2][TOT_POINTS_PER_FOV+4];
	wrap_type 	obj_fov_wrap;

	int pts_per_side = TOT_POINTS_PER_FOV/4;

	double declinations[2], corners_in_eci[4][3];
	double pole_dec = 90.0;

	/* Get the wrapping situation */
	obj_fov_wrap = RectangleWrapAroundPole (fov_h, fov_v, obj_to_eci);


	Update_Rectangle_Boundary (obj_to_eci, corners_in_eci, obj_fov_data,
							   pts_per_side, fov_h, fov_v);

	if (obj_fov_wrap == None &&
		RectangleAcrossRightAscentZero(obj_fov_data[0].ra,
		   obj_fov_data[pts_per_side+1].ra,
			   obj_fov_data[2*(pts_per_side+1)].ra,
				   obj_fov_data[3*(pts_per_side+1)].ra))

		obj_fov_wrap = Across_RA_Zero;

	switch (obj_fov_wrap)
	{
		case None:

			sprintf (s, "{{");

			PrintOneFovDataSet (buf, pts_per_side+1,  obj_fov_data);

			strcat (s, buf);

			strcat (s, "}}");
			
			break;

		case Wrap_N_Pole: case Wrap_S_Pole:

			/* Figure out which pole the FOV wraps */
			if ( obj_fov_wrap == Wrap_S_Pole )
				pole_dec = - pole_dec;

			declinations[0] = DeclOfZeroRAPointOnWrappingRectangle
							  (corners_in_eci);

			if (declinations[0] == PI)
			{
				sprintf (s, "Error: anomaly occurred in computing the declination of the point with 0 right ascension.\n");
				return 1;
			}

			ExtendLeftandRight (4*pts_per_side+4, obj_fov_data,
								 left_and_right[0], left_and_right[1]);

			strcpy (s, "{{");

			PrintOneFovDataSet (buf, pts_per_side+1, left_and_right[0]);

			strcat (s, buf);

			strcpy (buf, "} {");

			strcat (s, buf);

			PrintOneFovDataSet (buf, pts_per_side+1, left_and_right[1]);

			strcat (s, buf);

			sprintf (buf,
		     " {%10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10.6f}}}\0",
			   0.0, declinations[0]*RTD, 0.0, pole_dec,
				 360.0, pole_dec, 360.0, declinations[0]*RTD);

			strcat (s, buf);

			break;

		case Across_RA_Zero:

			MappingLeftandRight (4*pts_per_side+4, obj_fov_data,
								 left_and_right[0], left_and_right[1]);

			strcpy (s, "{{");

			PrintOneFovDataSet (buf, pts_per_side+1, left_and_right[0]);

			strcat (s, buf);

			strcpy (buf, "} {");

			strcat (s, buf);

			PrintOneFovDataSet (buf, pts_per_side+1, left_and_right[1]);

			strcat (s, buf);

			sprintf (buf, "}}\0");

			strcat (s, buf);

			break;
	}

	return 0;
}
