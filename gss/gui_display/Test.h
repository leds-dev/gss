#ifndef _TEST_H
#define _TEST_H

double *ephem_get_eci_pos (int id);
double GetObjectSize (int id);

void GetST2ECI (int id, double st_to_eci[3][3]);

void GetSTFovSize (int id, double *h, double *v);

#endif /* _TEST_H */
