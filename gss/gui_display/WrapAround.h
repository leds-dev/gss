/********************************************************************
  Filename: WrapAround.h

  Description:

	This file deals with the wrap around for Rectangular or Circular
  shape.

  *********************************************************************/

#ifndef _WRAPAROUND_H
#define _WRAPAROUND_H

#include "mathlib.h"

#define	WRONG_ACROSSING	-1
#define	NO_ACROSSING	0
#define	ONE_ACROSSING	1
#define	TWO_ACROSSING	2

typedef enum {None, Wrap_N_Pole, Wrap_S_Pole, Across_RA_Zero} wrap_type;

wrap_type RectangleWrapAroundPole (double fov_h, double fov_v,
								 double obj_to_eci[3][3]);

int RectangleAcrossRightAscentZero (double ra_1, double ra_2,
								    double ra_3, double ra_4);

wrap_type CircleWrapAroundPole (double ang_radius, double center_in_eci[3]);

int CircleAcrossRightAscentZero (double ang_radius, double center_in_eci[3]);

double DeclOfZeroRAPointOnWrappingCircle (double ang_radius,
										  double center_in_eci[3]);

int DeclsOfTwoZeroRAPointsOnCircle (double ang_radius,
								    double center_in_eci[3],
									double declinations[2]);

double DeclOfZeroRAPointOnWrappingRectangle (double corner_in_eci[4][3]);

int EndPointIndex (int size, RA_DEC pos_of_points[],
				   int end_points[2], int dir[2]);

void MappingLeftandRight ( int size, RA_DEC points_in_mid[],
						   RA_DEC left_points[],
						   RA_DEC right_points[]);

void ExtendLeftandRight ( int size, RA_DEC points_in_mid[],
						   RA_DEC left_points[],
						   RA_DEC right_points[]);

#endif  /* _WRAPAROUND_H */
