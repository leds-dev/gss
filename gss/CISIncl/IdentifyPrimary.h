/* ========================================================================
	FILE NAME:
		IdentifyPrimary.h
	DESCRIPTION:
				This is a header file of IdentifyPrimary.c 

 ====================================================================

	REVISION HISTORY

 ==================================================================== */

#ifndef _IDENTIFYPRIMARY_H
#define _IDENTIFYPRIMARY_H

/* $Id: IdentifyPrimary.h,v 1.1 1999/12/13 17:12:40 jzhao Exp $ */ 

/* -----------------------
	include files
*/

/* ------------------------
	Macros
*/

/* ------------------------
	Type define and Global(external) variables 
*/
/* -----------------------
	Function Prototypes
*/	
int IDentifyPrimaryCandidate(int CIS_MODE,
							 ST_type *PrimaryStar,
						  	 StarSubSC_type *PrimaryCandidate,
							 VECTOR_type *SunECI,
							 DirCosMatrix_type *PrimaryST_DCM_ECI2ST,
							 double SunPrimaryStarAngle,
							 VECTOR_type *FixedPtST,
							 VECTOR_type *FixedPtECI,
							 double *MaxRotationError); 

void IDentifyPrimaryStar(SubSC_type *PrimarySubCatalog,
						 ST_type  *PrimaryStar,
						 DirCosMatrix_type DCM_PrimaryST2ST[],
						 VECTOR_type *SunSTPrimary,
						 double MaxMagResPrimary,
						 double MaxAngCornerPrimaryStar[],
						 double *SunPrimaryAngle,
						 int *Min_Indx, int *Max_Indx);

void IDentifyPrimaryStarTracker(int PrimaryST_ID,
								TrackerOutputST_type TrackedStars[],
								DirCosMatrix_type DCM_ST2BDY[],
								TrackerOutputST_type StarMatchList[],
								double SepTolerance[],
								DirCosMatrix_type DCM_PrimaryST2ST[]);

void ElimnateCurPrimaryStar( TrackerOutputST_type *StarMatchListPrimaryST);

/*****************************************************************************
	Change Log:
	$Log: IdentifyPrimary.h,v $
	Revision 1.1  1999/12/13 17:12:40  jzhao
	Initial Release.
	
******************************************************************************/

#endif /* _IDENTIFYPRIMARY_H */
