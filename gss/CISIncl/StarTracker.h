/* ========================================================================
	FILE NAME:
		StartTracker.h

	DESCRIPTION:
		This is a header file of StarTracker.c.

 ====================================================================

	REVISION HISTORY

 ==================================================================== */

#ifndef _STARTRACKER_H
#define _STARTRACKER_H

/* $Id: StarTracker.h,v 1.1 1999/12/13 17:12:41 jzhao Exp $ */

/* ------------------------
	Headers 
*/
#include <CISGlobal.h> 

/* ------------------------
	Macros
*/
/* ------------------------
	Type define and Global(external) variables 
*/
	typedef struct
	{
		int VT_ID;
		double HPos;
		double VPos;
		double Magnitude;
	}VT_type;
	
	typedef struct
	{
		int	NumVTs;
		int ST_ID;
		VT_type VTs[NUM_VT];
	}TrackerOutputVT_type;

	typedef struct
	{
		int	   VT_ID;
		double STFrame[3];
		double Magnitude;
	}ST_type;
	
	typedef struct
	{
		int NumStars;
		int ST_ID;
		ST_type Stars[NUM_VT];
	}TrackerOutputST_type;

	typedef struct
	{
		double Mean_H;
		double Mean_V;
		double StdDev_H;
		double StdDev_V;
		double Spread;
	}StatisticsVT_type;

	typedef struct
	{
		int NumST;
		int ST_ID[NUM_ST];
	}STList_type;

/*	Function Prototypes */

void STFOV_Bounds(double *HMax, double *HMin, double *VMax, double *VMin);

void STFOV_CornerPos_STFrame(VECTOR_type CornerPos[]);

int STFOV_StarPosStatistics(TrackerOutputVT_type *TrackedVTs,
							StatisticsVT_type   *PosStatistics);

void StarTrackerAlignment(int Ref1ST_ID, int Ref2ST_ID, int ValidST[], 
				  		VECTOR_type *STBoresight,
						VECTOR_type STBoresightBDY[],
					  	DirCosMatrix_type DCM_ECI2ST[],
						QUATERNION_type Quat_ST2BDY[],
					  	QUATERNION_type Quat_BDY2ST[],
						DirCosMatrix_type DCM_ST2BDY[],
					  	DirCosMatrix_type DCM_BDY2ST[]);

void AlignmentEstimateChange (DirCosMatrix_type  Original_DCM_Sensor2BDY,
                              DirCosMatrix_type  Refined_DCM_Sensor2BDY,
                              VECTOR_type SensorBoresight_Sensor,
                              double *SeparationBoresight,
                              double *RotationAboutBoresight);

void ST_HVPos2STFrame(TrackerOutputVT_type *StarsHVPos,
					  TrackerOutputST_type *StarsSTFrame);

void ST_STFrame2HVPos(TrackerOutputST_type *StarsSTFrame,
					  TrackerOutputVT_type *StarsHVPos);

void STVector2HVPos(double STVector[], double *HPos, double *VPos);

void GenAvailableSTList(TrackerOutputVT_type RawVTs[],
                        TrackerOutputST_type TrackedStarST[],
                        STList_type *AvailableSTList,
                        STList_type *PossibleRefSTList);

int TotalSTStars(TrackerOutputST_type TrackedStarST[]); 

/***************************************************************************
	Change Log:
	$Log: StarTracker.h,v $
	Revision 1.1  1999/12/13 17:12:41  jzhao
	Initial Release.
	
****************************************************************************/

#endif /* _STARTRACKER_H */
