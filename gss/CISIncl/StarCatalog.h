/* =======================================================================

	FILE NAME:
		StarCatalog.h

	DESCRIPTION:
		This is a header file of StarCatalog.c.

==========================================================================

	REVISION HISTORY

========================================================================== */

#ifndef _STARCATALOG_H
#define _STARCATALOG_H

/* $Id: StarCatalog.h,v 1.4 2000/03/15 21:22:21 jzhao Exp $ */

/* --------------------------
	Include Headers
*/

#include <CISGlobal.h>

/* ---------------------------
	Macros
*/
#define ASC_TYPE 0		 
#define SUBSC_TYPE 1

/* ----------------------------
	Type define and Global(external) variables
*/
	
	typedef struct
	{
		unsigned long int	Sky2k_ID;
		int					OSC_ID;
		double 				RAscen;
		double 				Declina;
		double				ECI[3];
		double 				Magnitude;
		double 				MagnitudeST[NUM_ST];
		double 				MagResidual;
		int 				ASC_ID;
		int					BSS_ID;
	}StarASC_type;

	typedef struct
	{
		int 			ASC_ID;
		int		 		OSC_ID;
		int				BSS_ID;
		double 			ECI[3];
		double 			Magnitude;
		double 			MagResidual;
	}StarSubSC_type;

	typedef struct
	{
		int				NumStars;
		StarASC_type	*Stars[MAXNUM_ASC_STARS];
	}AcqSC_type;

	typedef struct
	{
		int				NumStars;
		int 			ST_ID;
		StarSubSC_type	*Stars[MAXNUM_SUBSC_STARS];
	}SubSC_type;

	typedef struct
	{
		double	MaxAng;
		double	MinAng;
	}MaxMinAng_type;
		
/*	Function Prototypes */

int Read_StarCatalog(char *FileName, void *StarCatalog, int CatalogType);
int Write_StarCatalog(char *FileName, void *StarCatalog, int CatalogType);
int Write_RaDec(char *FileName, SubSC_type *StarCatalog);

void CalRadiusAnnulusSTFOV(VECTOR_type SunST[],
							VECTOR_type FOVCornerST[],
							double Tolerance,
							double InnerRadius[],
							double OuterRadius[]);

void CalRadiuSpotSTFOV(double Tolerance, double *MaxRadius);

void GenSunPointingSubCatalog(AcqSC_type    *AcqStarCatalog,
                              VECTOR_type   *SunECI,
                              VECTOR_type   SunST[],
                              double        Tolerance,
                              SubSC_type    SubStarCatalog[]);

void GenRoughKnowledgeSubCatalog(AcqSC_type *AcqStarCatalog,
								 DirCosMatrix_type  DCM_ECI2ST[],
							  	 VECTOR_type *STBoresight,
								 double       Tolerance,
								 SubSC_type   SubStarCatalog[]);

/**********************************************************************************
    Change Log:
    $Log: StarCatalog.h,v $
	
***********************************************************************************/

#endif /* _STARCATALOG_H */
