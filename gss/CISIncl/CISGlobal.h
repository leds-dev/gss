/* =======================================================================

	FILE NAME:
			CISGlobal.h

	DESCRIPTION:
			This is a header file of CIS global definitions. 

==========================================================================

	REVISION HISTORY

========================================================================== */

#ifndef _CISGLOBAL_H
#define _CISGLOBAL_H

/* $Id: CISGlobal.h,v 1.9 2000/03/28 22:50:54 jzhao Exp $ */

/* ---------------------------
	Headers
*/
#include <CISGlobalType.h>

/* ---------------------------
	Macros
*/

#define MAXNUM_ASC_STARS    15000 			  /* max number of stars in */
								 			  /* Aquisition Star Catalog */
#define MAXNUM_SUBSC_STARS MAXNUM_ASC_STARS   /* Max number of stars in */
											  /* Sub_Star Catalog */
#define MAXNUM_STARS_FOV 100	/* Max. number of ASC/OSC stars covered by */
				/* ST FOV */ 
				/* Changed from 50 to 100; R. Telkamp 02/23/01*/
					
#define PIXEL_DEG	0.01617763176	/* Number of degrees in 1 pixel */

#define H_MAX_ANG  (4.0*DTR)  /* ST FOV radians */
#define H_MIN_ANG (-4.0*DTR)  /* ST FOV radians */
#define V_MAX_ANG  (4.0*DTR)  /* ST FOV radians */
#define V_MIN_ANG (-4.0*DTR)  /* ST FOV radians */

#define MAX_STFOV_RADIUS    (Max(H_MAX_ANG, V_MAX_ANG)*SQRT_2)
						/* Max Star Tracker FOV Radius from ST boresight(radians) */

#define MAX_SUN_POINTING_ERROR (2.0*DTR) 		/*Was 1.0*DTR radians */

#define PRIMARY_ST_LOOSE_THRESHOLD 2 	/* direct loose match threshold */
#define TOTAL_ST_LOOSE_THRESHOLD   3
#define PRIMARY_ST_TIGHT_THRESHOLD_GOOD_ALIGN 3  /* direct match tight threshold */
#define PRIMARY_ST_TIGHT_THRESHOLD_BAD_ALIGN  4
#define TOTAL_ST_TIGHT_THRESHOLD   4
#define PRIMARY_ST_XTIGHT_THRESHOLD 4 	/* direct extratight match threshold */

#define TRUE 1
#define FALSE 0

#define SUN_HOLD_MODE 1
#define ROUGH_KNOWLEDGE_MODE 0

#define OSCInFOV 1
#define ASCInFOV 0

/* -----------------------------
	extern variables
*/
 
extern FILE *LogFp;

extern int PRIMARY_ST_TIGHT_THRESHOLD;		/* 3 Good Align. otherwise 4 */
extern double MAX_ROUGH_KNOWLEDGE_ERROR;	/* (5.0*DTR) radians */

extern double MAX_ST_MISALIGNMENT_ERROR;	/* (0.15*DTR) radians */
extern double MAX_SEPARAT_PRIMARY_TOL;		/* (0.02*DTR) radians */
extern double PARALLEL_ERROR_THRESHOLD;		/* (0.30*DTR) radians */
extern double MAG_SATURATION;			/* 0.81 saturation of magnitude */
extern double MAGNITUDE_BIAS;			/* Magnitude Bias from VT reading */
extern double SMS_FOV;				/* (3.89*DTR) radians */
extern double ATTITUDE_UNCERTAINTY;		/* (5.0*DTR) radians */
extern double OVERLAP_PIXELS;			/* 4.0 pixels */
extern double SPREAD_LIMIT;			/* (3.75*DTR) radians */

extern double MAX_SEPARAT_NONPRIMARY_TOL; 	/* radians */   
extern double MAX_SUN_PRIMARY_ERROR;		/* radians */  
extern double MAX_SUN_NONPRIMARY_ERROR;		/* radians */ 
extern double MAX_ROUGH_PRIMARY_ERROR;		/* radians */ 

#ifdef CIS_TEST
extern FILE *TestOutFp;
extern int TestCount;
#endif

/**********************************************************************************
	Change Log:
	$Log: CISGlobal.h,v $
	Revision 1.9  2000/03/28 22:50:54  jzhao
	Changed format of CISOutput and Recommended track.
	Added function to write CISOutput to Log File.
	
	Revision 1.8  2000/03/15 21:22:20  jzhao
	Rearranged SubCatalog memory allocation.
	
	Revision 1.7  2000/03/01 19:17:04  jzhao
	undefineed CIS_TEST.
	
	Revision 1.6  2000/02/24 00:18:29  jzhao
	Modified for port to NT
	
	Revision 1.5  2000/02/19 01:18:01  jzhao
	Undefined CIS_TEST
	
	Revision 1.4  2000/02/14 21:27:23  jzhao
	Add functions of GetRoughKnowledgeRadius and GetSunHoldAnnulus. They can be
	called indipendently of CISEXE.
	Move MAX_ROUGH_KNOWLEDGE_ERROR from macro define to IniVariables.
	
	Revision 1.3  2000/02/09 23:53:34  jzhao
	Modified for intergrating to GUI.
	
	Revision 1.2  1999/12/23 21:12:21  jzhao
	Added FOVSize_type and Modified TrackerFOV calling argument list.
	
	Revision 1.1  1999/12/13 17:12:39  jzhao
	Initial Release.
	
***********************************************************************************/

#endif /* _CISGLOBAL_H */

