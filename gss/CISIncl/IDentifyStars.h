/* ========================================================================
	FILE NAME:
		StarIDentification.h
	DESCRIPTION:
				This is a header file of StarIDentification.c 

 ====================================================================

	REVISION HISTORY

 ==================================================================== */

#ifndef _IDENTIFYSTARS_H
#define _IDENTIFYSTARS_H

/* $Id: IDentifyStars.h,v 1.5 2000/02/24 00:18:29 jzhao Exp $ */

/* -----------------------
	include files
*/

#include <CISGlobal.h>
#include "StarTracker.h"

/* ------------------------
	Macros
*/

/* ------------------------
	Type define and Global(external) variables 
*/
	typedef struct
	{
		int				ASC_ID;
		int				OSC_ID;
		int				BSS_ID;
		int				SubSC_ID; 
		int				VT_ID;
		double			MagnitudeCat;
		double			MagnitudeTM;
		double			STFrame[3];
	}IDStar_type;
	
	typedef struct
	{
		int			NumStars;
		int			ST_ID;
		IDStar_type Stars[NUM_VT];
	}IDStarOutput_type;

/*	Function Prototypes */

double GetMaxMagnitudeResidue(SubSC_type *SubStarCatalog);

void GetMaxAngBetSTCornerPrimaryStar(double PrimaryStarST[],
                                    DirCosMatrix_type DCM_PrimaryST2ST[],
                                    double MaxAngle[]); 

void GetSeparationTolerance(int PrimaryST_ID, double SepTolerance[]);

void GetDirCosMatrixPrimaryST2ST(DirCosMatrix_type DCM_ST2BDY[],
								 int PrimaryST_ID,
							  	 DirCosMatrix_type DCM_PrimaryST2ST[]); 

void GetPositionToleranceLoose(double MaxAngleSTCornerPrimaryStar[],
                                double SepTolerance[],
                                double MaxRotTolerance,
                                double PosTolerance[]); 

void SearchMinMaxMagIndx(SubSC_type *SubCatalog,
                         double MaxMagRes,
                         double CoreMagnitude,
                         int *Min_Indx, int *Max_Indx); 

void IDentifyStars(SubSC_type *SubCatalog,
	   				TrackerOutputST_type *TrackedStars,
				  	DirCosMatrix_type *DCM_ST2ECI,
					VECTOR_type *RefSTFrame,
					double RefECI[],
					double MaxMagRes,
				   	double PosTolerance,
				  	double SepTolerance,
				 	IDStarOutput_type *IDStars);

int TotalIDStars(IDStarOutput_type IDStarsST[]);

int IDStarPosStatistics(IDStarOutput_type *IDStars,
                        StatisticsVT_type   *PosStatistics);

void CalNumIDStarsOSC(IDStarOutput_type IDStars[], int NumIDStarsOSC[], 
													int *TotalIDStarsOSC);
void GetNumIDStars(IDStarOutput_type IDStars[], int NumIDStars[]);
int MagnitudeCheck(double SCStarMag, double STStarMag, double MagRes);

/*******************************************************************************
    Change Log:
    $Log:  $
	
********************************************************************************/

#endif /* _IDENTIFYSTARS_H */
