/* ========================================================================
	FILE NAME:
		EstimateAttitude.h
	DESCRIPTION:
				This is a header file of EstimateAttitude.c 

 ====================================================================

	REVISION HISTORY

 ==================================================================== */

#ifndef _ESTIMATEATTITUDE_H
#define _ESTIMATEATTITUDE_H

/* $Id& */

/* -----------------------
	include files
*/

/* ------------------------
	Macros
*/

/* ------------------------
	Type define and Global(external) variables 
*/

/*	Function Prototypes */

void PrimaryAttitudeEstimate(VECTOR_type *RefPrimaryECI,
                             VECTOR_type *RefPrimaryST,
                             double PrimaryCandidateECI[],
                             double PrimaryStarST[],
                             QUATERNION_type *PrimaryEstimateQuat_ECI2ST,
                             DirCosMatrix_type *PrimaryEstimateDCM_ECI2ST);

int RefineAttitudeEstimate(IDStarOutput_type *IDStars,
                            SubSC_type  *SubStarCatalog,
                            QUATERNION_type *RefineQuat_ECI2ST,
                            DirCosMatrix_type *RefineDCM_ECI2ST);

void DiffBetEstimatedAttitude(QUATERNION_type *EstimatedQuat_ECI2ST1,
                              QUATERNION_type *EstimatedQuat_ECI2ST2,
                              QUATERNION_type *DeltaQuat_ECI2ST,
                              VECTOR_type *AcqError,
                              double *AcqErrorTotal);

void CalParamDirectMatch( DirCosMatrix_type *PrimaryEstimateDCM_ECI2ST,
                          DirCosMatrix_type *DCM_PrimaryST2CurrST,
                          double RefPrimaryST[],
                          DirCosMatrix_type *CurrDCM_ST2ECI,
                          VECTOR_type *RefCurrST ); 

void CalSTBoresightECI(VECTOR_type *STBoresight,
                       DirCosMatrix_type *DCM_ECI2ST,
                       VECTOR_type *STBoresightECI); 

double  CalParallelError(double StarSpread);

double CalPerpendicularError(SubSC_type *SubStarCatalog,
                             IDStarOutput_type *IDStars,
                             DirCosMatrix_type *DCM_ECI2ST);

void CalTransversErrorSunHold(VECTOR_type *SunECI,
                                VECTOR_type *PrimaryCandidateStarECI,
                                double      SunPrimaryStarAngle,
                                double  *SunPrimaryCandidateAngle,
                                double  *TransversError); 

void CalTransversErrorRoughKnowledge(
                            DirCosMatrix_type *PrimaryST_DCM_ECI2ST,
                            VECTOR_type *PrimaryCandidateStarECI,
                            VECTOR_type *PrimaryStarST,
                            VECTOR_type *PrimaryCandidateST,
                            double *TransversError); 

void CalMaxRotationErrorSunHold(double TransversError,
                                double SunPrimaryCandidateAngle,
                                double *longitudinalError,
                                double *MaxRotationError);

void CalFixPtMaxRotationErrorRoughKnowledge(
                            DirCosMatrix_type *PrimaryST_DCM_ECI2ST,
                            VECTOR_type *PrimaryStarST,
                            VECTOR_type *PrimaryCandidateST,
                            VECTOR_type *FixedPtST,
                            VECTOR_type *FixedPtECI,
                            double *MaxRotationError); 

int IdentifyContradictory( QUATERNION_type *PrevQuat_ECI2ST,
                           QUATERNION_type *CurrQuat_ECI2ST );

/******************************************************************************
	Change Log:
	$Log: EstimateAttitude.h,v $
	Revision 1.1  1999/12/13 17:12:39  jzhao
	Initial Release.
	
*******************************************************************************/

#endif /* _ESTIMATEATTITUDE_H */
