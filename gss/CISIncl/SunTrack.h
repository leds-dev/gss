/* ========================================================================
	FILE NAME:
		SunTrack.h

	DESCRIPTION:
		This is a header file of SunTrack.c.

 ==================================================================== */

#ifndef _SUNTRACK_H
#define _SUNTRACK_H

/* $Id:$ */

/* ------------------------
	Headers 
*/
/* ------------------------
	Type define
*/
	typedef struct
	{
		VECTOR_type ECI;
		VECTOR_type Velocity; /* (m/sec) */
		double DistFromSC;     /* (m) */
		double StayoutRadius; /* (radius) */
	}SunStruc_type;
	
/*	Function Prototypes */

void SunTrack ( VECTOR_type SSBoresightSS,
		DirCosMatrix_type DCM_SS2BDY,
               QUATERNION_type Quat_ECI2BDY,
               SunStruc_type Sun_structure,
               VECTOR_type *Spacecraft_Angular_VelocityBDY);

/***************************************************************************
	Change Log:
	$Log:$
	
****************************************************************************/

#endif /* _SUNTRACK_H */
