/* ========================================================================
    FILE NAME:
		CISExe.h

    DESCRIPTION:
        This is a header file of CISExe.c.

 ====================================================================

    REVISION HISTORY

 ==================================================================== */

#ifndef _CISEXE_H
#define _CISEXE_H

/* $Id: CISExe.h,v 1.9 2000/04/03 16:42:46 jzhao Exp $ */

/* ------------------------
    Headers
*/

#include <CISGlobal.h>
#include <ephem_intf.h>
#include <SunTrack.h>
#include <CIS_Slew.h>

/* ------------------------
    Macros
*/

/* ------------------------
    Type define and Global(external) variables
*/

	enum {PWR12 = 1, PWR23, PWR13, PWR123};

	enum TrackActionTypes
	{NEW_POSN,
	 OLD_POSN,
	 NO_POSN};

    typedef struct
    {
		QUATERNION_type Quat_ECI2BDY;
		QUATERNION_type Quat_SS2BDY;
		QUATERNION_type Quat_ST2BDY[NUM_ST];
		DirCosMatrix_type DCM_ECI2BDY;	
		DirCosMatrix_type DCM_SS2BDY;	
		DirCosMatrix_type DCM_ST2BDY[NUM_ST];	
		STAYOUT_type	  StayOutZones[MAX_EPHEM_OBJS];	/* degree */
		SunStruc_type	  SunStruc;			/* degree */
		double MAX_ROUGH_KNOWLEDGE_ERROR;   		/* degree */
		double MAX_ST_MISALIGNMENT_ERROR;   		/* degree */
		double MAX_SEPARAT_PRIMARY_TOL;     		/* degree */
		double PARALLEL_ERROR_THRESHOLD;    		/* degree */
		double MAG_SATURATION;              
		double MAGNITUDE_BIAS;             
		double SMS_FOV;					/* degree */
		double ATTITUDE_UNCERTAINTY;			/* degree */
		double OVERLAP_PIXELS;				/* pixels */
		double SPREAD_LIMIT;				/* degree */
        int PWR_Config;
        int GoodAlign;
    }IniVariable_type;

/*  Function Prototypes */

	void GetRoughKnowledgeRadius(IniVariable_type *IniVars, double *Radius);
	void GetSunHoldAnnulus( IniVariable_type *IniVars,
							double InnerRadius[], double OuterRadius[]);

	int CISEXE(AcqSC_type *AcqStarCatalog,
		   char *LogFileName,
           IniVariable_type *IniVariables,
           VTBuffer_type VTBuffer[][NUM_VT],
           int CIS_MODE,
           int STCalibON,
           int SSCalibON,
		   CISOutput_type *CISOutput); 

/****************************************************************************
	Change Log:
	$Log: CISExe.h,v $
	Revision 1.9  2000/04/03 16:42:46  jzhao
	Modified for integrate to TCL
	
	Revision 1.8  2000/03/28 22:50:54  jzhao
	Changed format of CISOutput and Recommended track.
	Added function to write CISOutput to Log File.
	
	Revision 1.7  2000/03/21 23:36:50  jzhao
	Added CIS_Slew functions.
	
	Revision 1.6  2000/02/25 21:38:30  jzhao
	Get Log file name from GUI.
	
	Revision 1.5  2000/02/23 17:35:47  jzhao
	Move *LogFp back to CIS code.
	
	Revision 1.4  2000/02/22 18:21:40  jzhao
	Set FILE *LogFp to extern variable(get from GUI)
	
	Revision 1.3  2000/02/14 21:27:22  jzhao
	Add functions of GetRoughKnowledgeRadius and GetSunHoldAnnulus. They can be
	called indipendently of CISEXE.
	Move MAX_ROUGH_KNOWLEDGE_ERROR from macro define to IniVariables.
	
	Revision 1.2  2000/02/09 23:53:33  jzhao
	Modified for intergrating to GUI.
	
	Revision 1.1  1999/12/13 17:12:38  jzhao
	Initial Release.
	
*****************************************************************************/

#endif /* _CISEXE_H */
