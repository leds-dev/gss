/*============================================================
	
	FILE NAME:
		CIS_Slew.h

	DESCRIPTION:
		This is header file of CIS_Slew.c

 ============================================================= */

#ifndef _CIS_SLEW_H
#define _CIS_SLEW_H

/* $Id: CIS_Slew.h,v 1.1 2000/03/21 23:36:51 jzhao Exp $ */


/* -----------------------------------------
	Includes
*/
#include <ephem_intf.h>

/* -----------------------------------------
    Type define
*/
    typedef struct
    {
		/* Radius of the relevant celestial bodie(in radians) */
        double Radius;

        /* Center of the relevant celestial bodie */
        VECTOR_type ECI;

    }STAYOUT_type;

/* Function Prototypes */

int Slew_EnoughStars (DirCosMatrix_type DCM_ECI2ST,
                      VECTOR_type axisST,
                      double angle,
                      VECTOR_type STBoresightST,
                      AcqSC_type *AcqStarCatalog,
                      int tracker_index,
                      TrackerFOV_HV_type *OSCStarsInFOV);

void Slew_ToPositionWing (  VECTOR_type SSBoresightSS,
							DirCosMatrix_type DCM_SS2BDY,
                       		QUATERNION_type Quat_Initial_ECI2BDY,
                       		double *upright_slew_angle,
                       		double *inverted_slew_angle);

void Slew_ToStarrierSkies (int Tracker1_index, int Tracker2_index,
                           DirCosMatrix_type DCM_ST2BDY[NUM_ST],
                           VECTOR_type SSBoresightSS,
                           VECTOR_type STBoresightST,
                           DirCosMatrix_type DCM_SS2BDY,
                           STAYOUT_type StayOuts[MAX_EPHEM_OBJS],
                           QUATERNION_type Quat_ECI2BDY,
                           double AttitudeUncertainty,
			   			   AcqSC_type *AcqStarCatalog,
                           int Intrusion[][MAX_EPHEM_OBJS],
                           double *plus_slew_angle,
                           double *minus_slew_angle);

void SlewIntersection (QUATERNION_type Quat_ECI2BDY,
                       VECTOR_type BodyFixedPtBDY,
                       VECTOR_type SlewAxisBDY,
                       VECTOR_type ECIFixedPtECI,
                       double Separation,
                       double *theta_plus, double *theta_minus,
                       int *theta_plus_valid, int *theta_minus_valid);

int Quat_AxisAngle2Q(VECTOR_type axis, double angle,
                      QUATERNION_type *Q);

int Quat_Q2AxisAngle(QUATERNION_type inputQ, VECTOR_type *axis,
                     double *angle);

/******************************************************************************
	Change Log:
	$Log: CIS_Slew.h,v $
	Revision 1.1  2000/03/21 23:36:51  jzhao
	Added CIS_Slew functions.
	
******************************************************************************/

#endif /* _CIS_SLEW_H */
