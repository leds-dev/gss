/* =======================================================================

	FILE NAME:
			CISGlobalType.h

	DESCRIPTION:
			This is a header file of CIS global type definitions. 

==========================================================================

	REVISION HISTORY

========================================================================== */

#ifndef _CISGLOBALTYPE_H
#define _CISGLOBALTYPE_H

/* $Id:$ */

/* ---------------------------
	Macros
*/
#define NUM_ST  3  /* Number of Star trackers on board */
#define NUM_VT  5  /* Number of Vritual Trackers for each star tracker */

/* -------------------------------------
	Type define 
*/
  typedef struct
  {
  	double Vec[3];
  }VECTOR_type;

  typedef struct
  {
  	double Quat[4];
  }QUATERNION_type;

  typedef struct
  {
  	double DCM[9];
  }DirCosMatrix_type;

/**********************************************************************************
	Change Log:
	$Log: $
	
***********************************************************************************/

#endif /* _CISGLOBALTYPE_H */

