/* ========================================================================
    FILE NAME:
        	TerminateFnc.h
    DESCRIPTION:
                This is a header file of TerminateFnc.c

 ====================================================================

    REVISION HISTORY

 ==================================================================== */

#ifndef _TERMINATEFNC_H
#define _TERMINATEFNC_H

/* $Id: TerminateFnc.h,v 1.3 2000/03/28 22:50:55 jzhao Exp $ */

/* -----------------------
    include files
*/

#include <CISGlobal.h>

/* ------------------------
    Macros
*/

/* ------------------------
    Type define and Global(external) variables
*/

void SunSensorAlignment(VECTOR_type *SUN_ECI,
                        VECTOR_type *SSBoresight,
                        DirCosMatrix_type *DCM_ECI2BDY,
                        QUATERNION_type *Quat_SS2BDY,
                        QUATERNION_type *Quat_BDY2SS,
                        DirCosMatrix_type *DCM_SS2BDY,
                        DirCosMatrix_type *DCM_BDY2SS); 

void SetReferenceTrackers(int NumStarID[], 
						  int ValidRefST[],
						  double ParallelErr[], 
                          int DefaultRefST1_ID, int DefaultRefST2_ID, 
						  int *RefST1_ID, int *RefST2_ID); 

void CalECI2BDY(QUATERNION_type *Quat_ECI2ST,
                QUATERNION_type *Quat_ST2BDY,
                QUATERNION_type *Quat_ECI2BDY, 
                QUATERNION_type *Quat_BDY2ECI,
                DirCosMatrix_type *DCM_ECI2BDY,
                DirCosMatrix_type *DCM_BDY2ECI);

int AcqStatus(int IDContradictory[], int IDRef);

void WriteString(char *String, char *s);
void WriteInt(int a, char *s);
void WriteDouble(double a, char *s);
void WriteVector(double Vec[], char *s);
void WriteMatrix(DirCosMatrix_type *Matrix, char *s);
void WriteQuaternion(QUATERNION_type *Quat, char *s);
void WriteIntNUM_ST(int d[], char *s);
void WriteDoubleNUM_ST(double d[], char *s);
void WriteMatrixNUM_ST(DirCosMatrix_type Matrix[], char *s);
void WriteQuaternionNUM_ST(QUATERNION_type Quat[], char *s);
void WriteRawVTs(TrackerOutputVT_type RawVTs[], char *s);
void WriteIDStars(IDStarOutput_type IDStars[], char *s);
void WriteRecommendTrack(RecommendVT_type RecommendTrack[], char *s);
void WriteSTList(STList_type *STList, char *s);
void WritePosStatistics(StatisticsVT_type PosStatistics[], char *s);
void WriteCISOutput(CISOutput_type *CISOutput, char *s);

void TestOutString(char *s);
void TestOutCount(char *s);
void TestOutInt(int a, char *s);
void TestOutDouble(double a, char *s);
void TestOutIntNUM_ST(int a[], char *s);
void TestOutDoubleNUM_ST(double a[], char *s);
void TestOutQuaternion(QUATERNION_type *Quat, char *s);
void TestOutQuaternionNUM_ST(QUATERNION_type Quat[], char *s);
void TestOutMatrix(DirCosMatrix_type *Matrix, char *s);
void TestOutIDStars(IDStarOutput_type IDStars[],
					RecommendVT_type RecommendTrack[], char *s1, char *s2);

/****************************************************************************
	Change Log:
	$Log: TerminateFnc.h,v $
	Revision 1.3  2000/03/28 22:50:55  jzhao
	Changed format of CISOutput and Recommended track.
	Added function to write CISOutput to Log File.
	
	Revision 1.2  2000/03/21 23:36:51  jzhao
	Added CIS_Slew functions.
	
	Revision 1.1  1999/12/13 17:12:42  jzhao
	Initial Release.
	
*****************************************************************************/

#endif /* _TERMINATEFNC_H */
