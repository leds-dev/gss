/* ========================================================================
    FILE NAME:
        	ChooseOSC.h
    DESCRIPTION:
                This is a header file of ChooseOSC.c

 ====================================================================

    REVISION HISTORY

 ==================================================================== */

#ifndef _CHOOSEOSC_H
#define _CHOOSEOSC_H

/* $Id: ChooseOSC.h,v 1.5 2000/03/28 22:50:54 jzhao Exp $ */

/* -----------------------
    include files
*/

#include "CISGlobal.h"
#include "StarCatalog.h"
#include "IDentifyStars.h"

/* ------------------------
    Macros
*/

/* ------------------------
    Type define and Global(external) variables
*/
    typedef struct
    {
        double HPos;
        double VPos;
        double Magnitude;
        int    Tracked;
        int    Valid;
        int    UserSel;
    }VTBuffer_type;

	typedef struct
	{
		int		VT_ID;
		double	BTrackPriority;
	}BT_type; 

	typedef struct
	{
		int		NumVTs;
		int		ST_ID;
		BT_type VT[NUM_VT];
	}BTList_type;

	typedef struct
	{
		double HMax;
		double HMin;
		double VMax;
		double VMin;
	}FOVSize_type; 			/* radians!! */

	typedef struct
	{
		double			HPos;
		double			VPos;
		double			Magnitude;
		int				VT_ID;
		int				OSC_ID;
		int				ASC_ID; 	
		int				BSS_ID;
	}Star_HV_type;

	typedef struct
	{
		int				NumStars;
		int				ST_ID;
		Star_HV_type	Stars[MAXNUM_STARS_FOV];
	}TrackerFOV_HV_type;

	typedef struct
	{
		int				NumVTs;
		int				ST_ID;
		Star_HV_type	Stars[NUM_VT]; 
	}RecommendVT_type;
/* -----------------------
	Function Prototypes
*/

void TrackerFOV(AcqSC_type *AcqStarCatalog,
                VECTOR_type *STBoresight,
                DirCosMatrix_type *DCM_ECI2ST,
				FOVSize_type	*FOVSize,
                TrackerFOV_HV_type *CatalogStarsInFOV,
                int TrackerID,
                int FOVStarType);


void BreakTrackSet(VTBuffer_type (*VTBuff)[NUM_VT],
				   IDStarOutput_type *IDStars,
                   StatisticsVT_type *Centroid,
                   BTList_type *BreakTrack,
				   TrackerFOV_HV_type *TrackedOSCStars); 

void ChooseOSCStarForVT(VTBuffer_type VTBuff[][NUM_VT],
					  VECTOR_type *STBoresight,
                      IDStarOutput_type IDStars[],
                      StatisticsVT_type CentroidIDStar[],
                      AcqSC_type *AcqStarCatalog,
                      DirCosMatrix_type *DCM_ECI2BDY,
					  DirCosMatrix_type DCM_BDY2ST[],
                      RecommendVT_type RecommendTrack[]); 

/******************************************************************************
    Change Log:
    $Log:  $
	
*******************************************************************************/

#endif /* _CHOOSEOSC_H */
