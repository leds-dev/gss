/*============================================================
	
	FILE NAME:
		AcqParam.h

	DESCRIPTION:
		This is header file of AcqParam.c

 ============================================================= 

	REVISION HISTORY

 ============================================================= */

#ifndef _ACQPARAM_H
#define _ACQPARAM_H

/* $Id: AcqParam.h,v 1.1 1999/12/13 17:12:38 jzhao Exp $ */

/* Macros */
#define ROTATE123 0
#define ROTATE213 1   

/* Function Prototypes */

void GetSunSensorLOSInSTFrame(VECTOR_type *SunSensorLOSnorm,
							  DirCosMatrix_type DCM_ST2BDY[],
							  VECTOR_type SunST[]);

void GetTransformECI2ST(QUATERNION_type *Rough_Quat_ECI2BDY,
						QUATERNION_type Quat_BDY2ST[],
						QUATERNION_type Rough_Quat_ST2ECI[],
						QUATERNION_type Rough_Quat_ECI2ST[],
						DirCosMatrix_type Rough_DCM_ST2ECI[],
						DirCosMatrix_type Rough_DCM_ECI2ST[]);

void GetSTOrientation(VECTOR_type RotAngle[], int RotOrder,
					QUATERNION_type Quat_BDY2ST[],
					QUATERNION_type Quat_ST2BDY[],
					DirCosMatrix_type DCM_BDY2ST[],
					DirCosMatrix_type DCM_ST2BDY[]); 

void GetSTLosBDY(VECTOR_type STLos[],
				 DirCosMatrix_type DCM_ST2BDY[],
				 VECTOR_type STLosBDY[]); 

/*******************************************************************************
	Change Log:
	$Log: AcqParam.h,v $
	Revision 1.1  1999/12/13 17:12:38  jzhao
	Initial Release.
	
********************************************************************************/

#endif /* _ACQPARAM_H */
