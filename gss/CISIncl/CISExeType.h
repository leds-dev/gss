/* ========================================================================
    FILE NAME:
		CISExeType.h

    DESCRIPTION:
        This is a header file of CISExe.c.

 ====================================================================

    REVISION HISTORY

 ==================================================================== */

#ifndef _CISEXETYPE_H
#define _CISEXETYPE_H

/* $Id:$ */

/* ------------------------
    Headers
*/

#include <CISGlobalType.h>

/* ------------------------
    Type define and Global(external) variables
*/

    typedef struct
    {
        int BSS_ID;
        int OSC_ID;
    }ID_type;

    typedef struct
    {
        ID_type Stars[NUM_VT];
    }IDOutput_type;

    typedef struct
    {
        double HPos;
        double VPos;
        double Magnitude;
        int    TrackAction;
    }RecommendTrack_type;

    typedef struct
    {
        RecommendTrack_type VTs[NUM_VT];
    }RecommendOutput_type;

    typedef struct
    {
        QUATERNION_type     Quat_ECI2BDY;
        DirCosMatrix_type   DCM_ST2BDY[NUM_ST];
        DirCosMatrix_type   DCM_SS2BDY;
        RecommendOutput_type    RecommendTrack[NUM_ST];
        int                 ValidRefST[NUM_ST];
        int                 GoodAlign;
        IDOutput_type       IdentifiedStars[NUM_ST];
        double              AttErrorMetric[NUM_ST];
        double              AlignBoresightErrST[NUM_ST];
        double              AlignRotateErrST[NUM_ST];
        double              AlignBoresightErrSS;
        double   			Spread[NUM_ST];
        double              UpRightSlewAngle;
        double              InvertedSlewAngle;
        double              PlusSlewAngle;
        double              MinusSlewAngle;
		VECTOR_type			SC_Angular_VelocityBDY;
        int                 Status;
    }CISOutput_type;

/****************************************************************************
	Change Log:
	$Log: $
	
*****************************************************************************/

#endif /* _CISEXETYPE_H */
