/* $Id: draw_objs.c,v 1.4 2000/03/03 17:59:44 sue Exp $ */

#include <assert.h>

#include "draw_objs.h"
#include "pixels.h"
#include "annulus.h"

/****************************************************************/
/* TCL/SWIG will modify the initial values, which are bogus. */

DRAW_OBJ sun_sensor;
BODY_OBJS body_objs;

/****************************************************************/
/* define the three unit axis vectors */

/* unit dcm matrix = 1's on diagonal */
static DIM33 DCM_I = { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };

static VEC3 *X_axis = &DCM_I[0];
static VEC3 *Y_axis = &DCM_I[1];
static VEC3 *Z_axis = &DCM_I[2];

/****************************************************************/
/* fake array typedef for init() routine */

#define NUM_BOBJ ( sizeof(BODY_OBJS) / sizeof(DRAW_OBJ) )
typedef DRAW_OBJ BARRAY[NUM_BOBJ];

/****************************************************************/
/* convert from body to RA_DEC */

static void body2ra( RA_DEC *ra, VEC3 *body )
{
VEC3 eci;
vec_mult( &eci, &sim_data.eci2body.dcm_r, body );
eci2ra( ra, &eci );
}

/****************************************************************/
/* project a circle */

static void p_circle( DRAW_OBJ *o )
{
GPOINT pt;
RA_DEC center;
VEC3 body;

vec_mult( &body, &o->xx2body.dcm, &o->axis );
body2ra( &center, &body );
ra_to_px( &pt, &center );

calc_circle( &o->p, &pt, o->width );
}

/****************************************************************/
/* initialize a DRAW_OBJ */

static void ctor( DRAW_OBJ *o )
{
memcpy( &o->axis, Z_axis, sizeof(VEC3) );
rot_set_dcm( &o->xx2body, &DCM_I );
o->draw = p_circle;
o->width = 1.0;
o->height = 1.0;
}

/****************************************************************/
/* initialize the drawable objects */

void draw_init()
{
int i;

/* array-based initialization of body_objs structure. */
/* SWIG hates arrays */

BARRAY *b = (BARRAY *) &body_objs;
assert( sizeof( BARRAY ) == sizeof( BODY_OBJS ) );
for (i = 0; i < NUM_BOBJ; ++i )
	ctor( b[i] );	/* initialize body objects */

ctor( &sun_sensor );		/* initialize sun sensor */
}

/****************************************************************
$Log: draw_objs.c,v $
Revision 1.4  2000/03/03 17:59:44  sue
MSC predefines type POINT; changed to GPOINT.

Revision 1.3  2000/02/14 18:54:27  sue
Support for stx2sta INI file output.

Revision 1.2  2000/02/14 17:33:44  sue
Change xx_2_body to xx2body name to regularize more names.

Revision 1.1  2000/02/09 22:17:40  sue
Initial release.  Moving TCL dev layer to C.

****************************************************************/
