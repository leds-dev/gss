#ifndef DRAW_OBJS_H
#define DRAW_OBJS_H

/* $Id: draw_objs.h,v 1.3 2000/03/16 22:36:07 sue Exp $ */

#include "stars.h"

/*****************************************************************/
/* for circles, set width and height to radius. */
/* otherwise, it's a rectangle or square. */

typedef struct {
	ROTATION xx2body;			/* rotation */
	VEC3 axis;					/* X/Y/Z axis in body coordinates */
	double width;				/* visual dimensions */
	double height;
	PT_LIST p;					/* point list */
	VFP draw;					/* drawing routine */
	} DRAW_OBJ;

/*****************************************************************/

typedef struct {				/* SWIG hates arrays */
	DRAW_OBJ	CMD_FWD;	
	DRAW_OBJ	CMD_AFT;	
	DRAW_OBJ	DSN_TM_1K_FWD;	
	DRAW_OBJ	DSN_TM_4K_FWD;	
	DRAW_OBJ	DSN_TM_1K_AFT;	
	DRAW_OBJ	DSN_TM_4K_AFT;	
	DRAW_OBJ	CDA_TM_1K_FWD;	
	DRAW_OBJ	CDA_TM_4K_FWD;	
	DRAW_OBJ	CDA_TM_1K_AFT;	
	DRAW_OBJ	CDA_TM_4K_AFT;	
	DRAW_OBJ	INSTRUMENT_OPTICAL;	
	DRAW_OBJ	INSTRUMENT_COOLER;	
	} BODY_OBJS;

/*****************************************************************/
/* TCL/SWIG will update these once they've been initialized. */

extern DRAW_OBJ sun_sensor;			/* sun sensor */
extern BODY_OBJS body_objs;

void draw_init();					/* initialize everything */

/*****************************************************************
$Log: draw_objs.h,v $
Revision 1.3  2000/03/16 22:36:07  sue
Only two instruments.

Revision 1.2  2000/02/14 17:33:44  sue
Change xx_2_body to xx2body name to regularize more names.

Revision 1.1  2000/02/09 22:17:40  sue
Initial release.  Moving TCL dev layer to C.

*****************************************************************/

#endif
