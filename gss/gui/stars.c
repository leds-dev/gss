/* $Id: stars.c,v 1.33 2000/03/03 17:59:45 sue Exp $ */
#include <math.h>

#include "stars.h"
#include "catalog.h"
#include "draw_objs.h"
#include "st.h"

/*********************************************************************/
/* create the GUI data setup */
/* the GTACS buffer and CIS snapshot are PRIVATE. */

SIM_DATA_TYPE sim_data;			/* for entire gui - every TBD secs */

/*********************************************************************/
/* FOV BOUNDING BOX */

typedef struct {
	double hmin;
	double hmax;
	double vmin;
	double vmax;
	} BBOX;

static BBOX bbox;

/*********************************************************************/
/* fov edges - reference pixels.c TBR */
/* THE CORNER ORDER MUST MATCH THE TCL FOV DRAW ROUTINE!!! */

static HV st_hv_fov[NUM_CORNERS] = {
	{ -TRACKER_FOV_H, -TRACKER_FOV_V },		/* lower left corner */
	{  TRACKER_FOV_H, -TRACKER_FOV_V },		/* lower right corner */
	{  TRACKER_FOV_H,  TRACKER_FOV_V },		/* upper right corner */
	{ -TRACKER_FOV_H,  TRACKER_FOV_V }		/* upper left corner */
	};

PT_LIST st_px_corners;

/*********************************************************************/
/* conversion data */

static DIM33 *eci2body = &sim_data.eci2body.dcm;
static DIM33 *body2eci = &sim_data.eci2body.dcm_r;

static HV meridian = { 0.0, 0.0 };

/*********************************************************************/
/*********************************************************************/
/* SWIG helper for coordinate transforms */

ST *get_ST ( int i ) { return &sim_data.st[i-1]; }

/*********************************************************************/

static void bbox_init()			/* call every time ST is drawn */
{
bbox.hmin =  500.0;
bbox.hmax = -500.0;
bbox.vmin =  500.0;
bbox.vmax = -500.0;
}

static void minmax( HV *hv )
{
if (hv->h < bbox.hmin) bbox.hmin = hv->h;
if (hv->h > bbox.hmax) bbox.hmax = hv->h;
if (hv->v < bbox.vmin) bbox.vmin = hv->v;
if (hv->v < bbox.vmax) bbox.vmax = hv->v;
}

/*********************************************************************/

void stfov_init()
{
int i;

/* ST pointer is self-identifying */
for (i = 0; i < NUM_STS; ++i )
	{ sim_data.st[i].num = i + 1; }

/* pre-calc ST FOV pixel positions */
for (i = 0; i < NUM_CORNERS; ++i)
	{ hv_to_px( &st_px_corners.points[i], &st_hv_fov[i] ); }

st_px_corners.len = NUM_CORNERS;
}

/*********************************************************************/
/*********************************************************************/

static void meridian_calc( HV *hv, ST *st )
{
RA_DEC ra2 = st->los.ra;	/* structure copy */
ra2.dec += 3.5;
st_RAtoHV( hv, &ra2, st );
}

/*********************************************************************/
/* project ST FOV into skymap coordinates */
/* returns hmax and vmin for the selected ST */

static void calc_skyfov( ST *st )
{
int i;

/* initialize bounding box array */
bbox_init();

/* project each of the four FOV corners */
/* all that really changes is the sign of the corner element */

for (i = 0; i < NUM_CORNERS; ++i )
	{
	FOV_CORNER *sky = &st->skyfov[i];
	st_HVtoRA( &sky->ra, &st_hv_fov[i], st );
	ra_to_px( &sky->sky_px, &sky->ra );
	minmax( &st_hv_fov[i] );
	}
}

/*********************************************************************/

void update_st( ST *st )
{
int valid_vts;
int nstars;
int i;

/* update the matrix in case it's changed. */
mat_xpose( &st->st2body.dcm, &st->st2body.dcm_r );

/* generate skymap projection for this ST */
calc_skyfov( st );

/* calculate ASC stars in ST FOV */
nstars = st_fov_stars( &st->fov_stars, &st->los, st->num );

/* First output all stars */
for (i = 0; i < nstars; i++)
	{
	FOV_STAR *fov = &st->fov_stars[i];

	/* plot the star */

	/* This was dummy function, commended out by JZ 06/15/2000  
	plot_fov_star( fov );
	*/

/*	truth_star( fov );		calculate "truth" */
	}

/* Next output all VTs */
valid_vts = 0;

for (i = 0; i < NUM_VTS; i++)
	{
	VT *vt = &st->vt[i];

	if (vt->valid)
		{
		/* Convert HV to ST pixels */
		/* This was dummy function, commended out by JZ /06/15/2000
		plot_vt( vt );
		*/

		++valid_vts;
		}

	/* Check if there are any reduced FOV windows. */
#ifdef RFOV_EXISTS
	if (vt->rfov.h > -500)
		{
		st_rfov( &vt->rfov );
		}
#endif
	}

/* Update meridian position - TBR */
meridian_calc( &meridian, st );
}

/*********************************************************************/

void st_HVtoRA ( RA_DEC *ra2, HV *hv, ST *st )
{
VEC3 rIMU, rECI, rST;

hv2rst( &rST, hv );
vec_mult( &rIMU, &st->st2body.dcm, &rST );	/* ST to body */
unitize( &rIMU );					/* unitized ST to body */
vec_mult( &rECI, body2eci, &rIMU );	/* body to eci */
eci2ra( ra2, &rECI );				/* eci to radec */
}

/*********************************************************************/

void st_RAtoHV ( HV *hv, RA_DEC *ra2, ST *st )
{
VEC3 rIMU, rECI, rST;

ra2eci( &rECI, ra2 );
vec_mult( &rIMU, eci2body, &rECI );
unitize( &rIMU );
vec_mult( &rST, &st->st2body.dcm_r, &rIMU );
rst2hv( hv, &rST );
}

/*******************************************************************/

#ifdef VT_TO_PX
/* old code from Chris Rice.  No idea if I need it. TBR */

void vt_to_px( GPOINT *p, VT *vt );	/* vt (for ST or skymap???) */

void vt_to_px ( GPOINT *p, VT *vt )
{
/* a magic constant I inherited; no idea what it really is. TBR */
#define G_ST				412529.616		/* G = 1.573676 * 2**18 */

HV hv;
hv.h = vt->hv.h / G_ST;
hv.v = vt->hv.v / G_ST;
hv_to_px( p, &hv);
}
#endif

/**********************************************************************/

void update_st2body()
{
int i;
for ( i = 0; i < NUM_STS; ++i )
	{
	ST *st = &sim_data.st[i];
	mat_xpose( &st->st2body.dcm, &st->st2body.dcm_r );
	}
}

/**********************************************************************/

void update_los()
{
int i;
for ( i = 0; i < NUM_STS; ++i )
	{
	ST *st = &sim_data.st[i];
	eci2ra( &st->los.ra, &st->los.eci );
	}
}

void SetQuatSelAndValid (int q_sel, int q_valid)
{
	sim_data.quat_sel = q_sel;
	sim_data.q_valid = q_valid;

	UpdateAttitude ();
}
 
/* For Tcl to pass static quat value */
char *SetStaticQuatValue (double q1, double q2, double q3, double q4)
{
	static char buf[500];
	QUAT	temp_q;
	double  delta;

	/* Check the normalization */
	delta = fabs(q1*q1 + q2*q2 + q3*q3 + q4*q4 - 1.0);
	
/*	if (fabs(q1*q1 + q2*q2 + q3*q3 + q4*q4 - 1.0) > 1e-8)*/

	if (delta > 1e-5)
	{
		sprintf (buf,
			"Error: q = (%9.7f, %9.7f, %9.7f, %9.7f) is not a normalized quaternion!  Delta = %9.7f\n",
                	q1, q2, q3, q4, delta);

		return buf;
	}
	else
		strcpy (buf, "");

	temp_q.x = q1;
	temp_q.y = q2;
	temp_q.z = q3;
	temp_q.a = q4;

	/* Set the static quaternion as well as the dir cosine matrices */
	rot_set_q (&sim_data.static_eci2body, &temp_q);

	return buf;
}

/* Get the initial value for st2body direction cosine matrix */
char *GetInitSt2BodyDCM (int st_id)
{
	static char buf[9*40];

	st_id --;	/* Map 1, 2 ,3 to C index 0, 1, 2 */

	sprintf (buf,
             "%15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f",
			 sim_data.st[st_id].st2body_i.dcm[0][0],
			 sim_data.st[st_id].st2body_i.dcm[0][1],
			 sim_data.st[st_id].st2body_i.dcm[0][2],
			 sim_data.st[st_id].st2body_i.dcm[1][0],
			 sim_data.st[st_id].st2body_i.dcm[1][1],
			 sim_data.st[st_id].st2body_i.dcm[1][2],
			 sim_data.st[st_id].st2body_i.dcm[2][0],
			 sim_data.st[st_id].st2body_i.dcm[2][1],
			 sim_data.st[st_id].st2body_i.dcm[2][2]);

	return buf;
}

/**********************************************************************
Change Log:
$Log: stars.c,v $
Revision 1.33  2000/03/03 17:59:45  sue
MSC predefines type POINT; changed to GPOINT.

Revision 1.32  2000/02/14 17:31:06  sue
mat_mult() to vec_mult() change in mathlib.[ch].

Revision 1.31  2000/02/09 17:56:54  sue
Code cleanup based on -pedantic -Wall -ansi.

Revision 1.30  2000/02/09 15:20:09  sue
Changed from body2st to st2body per convention.

Revision 1.29  2000/02/04 17:39:05  sue
CIS Integration.
SSC has time in true julian; will be switched to J2000 soon.

Revision 1.28  2000/01/31 19:38:44  sue
Added more system .h includes to satisfy VC5.

Revision 1.27  2000/01/19 15:26:22  sue
Annulus gui computation.

Revision 1.26  2000/01/12 14:56:59  sue
Fix off-by-one error in get_ST().

Revision 1.25  1999/12/07 21:23:08  sue
SWIG will take mathlib.h, except for the function decls.
Still working on ST interface.

Revision 1.24  1999/12/07 14:46:58  sue
Moved vt_to_px from pixels to stars.c.  No idea if I need it. (TBR).
Has magic G_ST constant.

Revision 1.23  1999/12/06 22:58:41  sue
Fixed dependency between FOV_STARS and SIM_DATA.
Almost ready to throw in J. Liu's read_ssc stuff (ugh).

Revision 1.22  1999/12/03 23:06:54  sue
Moderate reorg to get ready for fov_stars.

Revision 1.21  1999/12/03 14:47:14  sue
Cleaning up calc_fov.  Now just needs ST ptr.
I suspect the bounding box should be in RA_DEC, but
Dave N. wasn't sure.

Revision 1.20  1999/12/02 16:52:56  sue
Rfov will be present for all STs, not just ST1.  Chris only had one
real ST available when he wrote the code.

Revision 1.19  1999/12/02 16:50:21  sue
Added vmax calculation to stfov bounding box.

Revision 1.18  1999/12/01 22:11:04  sue
Mathlib works with new fake_tm() from Dave Needelman's sun test case.

Revision 1.17  1999/11/30 15:51:48  sue
Forgot to update pos.ra after TM input.
Working on matrix math macros which SWIG will take.

Revision 1.16  1999/11/29 18:02:38  sue
Changed draw_vt() reference to plot_vt().

Revision 1.15  1999/11/29 17:46:18  sue
Changed plot_star() reference to plot_fov_star().

Revision 1.14  1999/11/29 17:40:54  sue
Took caps out of hv2rst/rst2hv names.
Changed st_init() to stfov_init to avoid name clash with plot.c.

Revision 1.13  1999/11/29 17:34:42  sue
Changed fov_stars() loop counter from asc_index to nstars.
Working on duplicate st_init() routines in stars.c and plot.c.
There is more duplicate code that I couldn't separate out at home,
due to the lack of a printer.

Revision 1.12  1999/11/29 17:26:08  sue
Reordered objects for cleaner gss main compile.
Cleaning up st_fov skymap projection.

Revision 1.11  1999/11/29 16:43:06  sue
Some FOV data can be calculated once.
Created hv2rst and rst2hv in mathlib.
calc_fov() is hosed; waiting for Dave Needelman.

Revision 1.10  1999/11/29 15:48:53  sue
Major update from work at home over Thanksgiving.
pixels.[ch] applies logical/engr. scaling to canvas.
Overhauled the "plot" interface; gss app now builds against better lib.
Couldn't finish plot due to lack of printer for code review.

Revision 1.10  1999/11/28 09:02:26  sue
Draw and plot ST are mixed up.

Revision 1.9  1999/11/28 08:21:30  sue
Fixing coordinate routine name typos.

Revision 1.8  1999/11/28 07:35:50  sue
Got plot to compile.

Revision 1.7  1999/11/28 01:53:22  sue
Added CVS tags.

Revision 1.3  1999/11/27 21:44:13  sue
Major overhaul.
Added direct pixel and canvas support.
Stars.[ch] should really be named gss.[ch] (TBR).
Jeff Rice's "ETK--" is now totally separate from the library.

Revision 1.2  1999/11/25 03:37:42  sue
Restored missing bodies.

Revision 1.9  1999/11/24 16:55:12  sue
Splitting libgss into separate .so files.
Bigger but more testable.

Revision 1.8  1999/11/23 16:59:00  sue
Malloc'ed ASC array is no longer public; use asc_idx(asc_id) instead.

Revision 1.7  1999/11/08 21:10:16  sue
Variable sim_data moved from gss_app to stars and made public.
Tests now done in separate directory.

Revision 1.6  1999/11/01 23:52:06  sue
ASC now generated by C code.  Debugging core dump in parser.
SCxxxx files no longer needed; using ../tcl/ASC.txt from ryan's demo.
Need to modify tcl to compute ASC.txt line count before calling C.

Revision 1.5  1999/10/28 20:15:37  sue
GSS app isn't calling right functions.

Revision 1.4  1999/10/28 20:04:41  sue
Accidentally cut FOV draw cmd when splitting out wrap code.

Revision 1.3  1999/10/28 19:58:27  sue
Renamed menu.tcl to gss.tcl.
Took out wrap code from stars.c.

Revision 1.2  1999/10/28 18:29:41  sue
Added osc_id to catalog.h.
Stars makes better use of the math library.
Color values need to be on TCL side (not yet complete).

Revision 1.1  1999/10/26 20:21:53  sue
Star display, coordinate xform, etc.  The workhorse package.
All display elements should be moved to plot.c eventually.

**********************************************************************/
