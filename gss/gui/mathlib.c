/* $Id: mathlib.c,v 1.25 2000/05/22 22:40:56 jzhao Exp $ */

/* trig functions, etc */
#include <math.h>

/* memcpy() */
#include <string.h>

#include "mathlib.h"

/**************************************************************/
/* integration with some of the CIS Library routines. */

#ifndef CSU_TEST
#include "MathFnc.h"

/* force all CIS math library calls to run in double precision. */
static int CIS_MATH = DOUBLE_DATA;

#endif

/**************************************************************/
/* initial value for quaternion (invalid?) */

QUAT Q_I = mQ_I;

/* scratch area for quaternion multiply */
static QUAT qxm[4];

/********************************************************************/
/* convert +/- 180 to 0-360 */

double modulus_360 ( double in )
{
if (in < 0.0)
	in += 360.0;
return in;
}

/********************************************************************/

#define VEC3_ARRAY

#ifdef VEC3_ARRAY
#define VEC30(v) (*v)[0]
#define VEC31(v) (*v)[1]
#define VEC32(v) (*v)[2]
#else
#define VEC30(v) v->x
#define VEC31(v) v->y
#define VEC32(v) v->z
#endif

/********************************************************************/

void eci2ra( RA_DEC *rdp, VEC3 *eci )
{
double ra;

if ( ( VEC30(eci) == 0.0 ) && ( VEC31(eci) == 0.0) )
	{ ra = 0.0; }
else {
	ra = modulus_360( RTD * atan2 ( VEC31(eci), VEC30(eci) ));
	}

rdp->ra = ra;
rdp->dec = RTD * asin( VEC32(eci) );
}

/********************************************************************/

void ra2eci( VEC3 *eci, RA_DEC *ra2 )
{
double star_dec = ra2->dec * DTR;
double star_ra = ra2->ra * DTR;

VEC32(eci)	= sin( star_dec ); 
star_dec	= cos( star_dec );

VEC31(eci) = star_dec * sin(star_ra); 
VEC30(eci) = star_dec * cos(star_ra); 
}

/**********************************************************************/

void rst2hv( HV *hv, VEC3 *rST )
{
unitize( rST );
hv->h = RTD *  atan2( VEC30(rST), VEC32(rST) );
hv->v = RTD *  atan2( VEC31(rST), VEC32(rST) );
}

/**********************************************************************/

void hv2rst( VEC3 *rST, HV *hv )
{
VEC30(rST) = tan( hv->h * DTR );
VEC31(rST) = tan( hv->v * DTR );
VEC32(rST) = 1.0;
unitize( rST );
}

/********************************************************************/
/* dereference the pointers */

#define OUT (*out)
#define IN (*in)
#define DCM (*dcm)

/********************************************************************/

void vec_mult ( VEC3 *out, DIM33 *dcm, VEC3 *in )
{
OUT[0] = DCM[0][0] * IN[0] + DCM[0][1] * IN[1] + DCM[0][2] * IN[2];
OUT[1] = DCM[1][0] * IN[0] + DCM[1][1] * IN[1] + DCM[1][2] * IN[2];
OUT[2] = DCM[2][0] * IN[0] + DCM[2][1] * IN[1] + DCM[2][2] * IN[2];
}

/********************************************************************/
/* transpose 3x3 matrix */

void mat_xpose ( DIM33 *out, DIM33 *in )
{
OUT[0][0] = IN[0][0];
OUT[0][1] = IN[1][0];
OUT[0][2] = IN[2][0];

OUT[1][0] = IN[0][1];
OUT[1][1] = IN[1][1];
OUT[1][2] = IN[2][1];

OUT[2][0] = IN[0][2];
OUT[2][1] = IN[1][2];
OUT[2][2] = IN[2][2];
}

/********************************************************************/
/* multiply two 3x3 matrices */

void mat_mult ( DIM33 *out, DIM33 *left, DIM33 *right )
{
int i;
DIM33 rt, rt_out;
mat_xpose( &rt, right );
for (i = 0; i < DIM3_LEN; ++i )
	vec_mult( &rt_out[i], left, &rt[i] );
mat_xpose( out, &rt_out );
}

/********************************************************************/
/* Normalize the vector */

void unitize ( VEC3 *in )
{
double vector_mag = sqrt( IN[0]*IN[0] + IN[1]*IN[1] + IN[2]*IN[2] );
IN[0] /= vector_mag;
IN[1] /= vector_mag;
IN[2] /= vector_mag;
}

/**************************************************************/
/* matrices are assumed to be orthogonal, so inv == xpose. */

void mat_inv( DIM33 *out, DIM33 *in )
{ mat_xpose( out, in ); }

/**************************************************************/

static void q_mult ( QUAT *quat );

static void q_mult ( QUAT *quat )
{
qxm[0].x = quat->x * quat->x;
qxm[0].y = quat->x * quat->y;
qxm[0].z = quat->x * quat->z;
qxm[0].a = quat->x * quat->a;

qxm[1].x = quat->y * quat->x;
qxm[1].y = quat->y * quat->y;
qxm[1].z = quat->y * quat->z;
qxm[1].a = quat->y * quat->a;

qxm[2].x = quat->z * quat->x;
qxm[2].y = quat->z * quat->y;
qxm[2].z = quat->z * quat->z;
qxm[2].a = quat->z * quat->a;

qxm[3].x = quat->a * quat->x;
qxm[3].y = quat->a * quat->y;
qxm[3].z = quat->a * quat->z;
qxm[3].a = quat->a * quat->a;
}

/********************************************************************/

static void q_to_dcm ( DIM33 *dcm, QUAT *quat )
{
static double TWO = 2.0;

q_mult( quat );
 
DCM[0][0] = qxm[0].x - qxm[1].y - qxm[2].z + qxm[3].a;
DCM[1][1] = -qxm[0].x + qxm[1].y - qxm[2].z + qxm[3].a;
DCM[2][2] = -qxm[0].x - qxm[1].y + qxm[2].z + qxm[3].a;

DCM[0][1] = TWO * (qxm[0].y + qxm[2].a);
DCM[0][2] = TWO * (qxm[0].z - qxm[1].a);

DCM[1][0] = TWO * (qxm[0].y - qxm[2].a);
DCM[1][2] = TWO * (qxm[1].z + qxm[0].a);

DCM[2][0] = TWO * (qxm[0].z + qxm[1].a);
DCM[2][1] = TWO * (qxm[1].z - qxm[0].a);
}

/********************************************************************/
/* Convert from dcm to quaternion */
/* Uses the flight software algorithm. */

#define C11 DCM[0][0]
#define C12 DCM[0][1]
#define C13 DCM[0][2]
#define C21 DCM[1][0]
#define C22 DCM[1][1]
#define C23 DCM[1][2]
#define C31 DCM[2][0]
#define C32 DCM[2][1]
#define C33 DCM[2][2]

typedef double QA[QUAT_LEN];
#define QI (* (QA *) q)

void dcm_to_quat( QUAT *q, DIM33 *dcm )
{
#ifndef CSU_TEST
int unused = DirCosMatrix2Quaternion( dcm, q, CIS_MATH );
#else

/* Select combination of diagonal DCM elements that
	produce the largest denominator. */

double T = C11 + C22 + C33;		/* trace(dcm) */
double Z1 = max(C11, C22);
double Z2 = max(C33, T);
double M = max(Z1, Z2);

double Q_N = 0.5 * sqrt( 1.0 + M + M - T);
double F = 0.25 / Q_N;

/* compute the quaternion */
if (M == C11)
	{
	QI[0] = Q_N; 
	QI[1] = (C12 + C21) * F;
	QI[2] = (C13 + C31) * F;
	QI[3] = (C23 - C32) * F;
	}
else if (M == C22)
	{
	QI[0] = (C12 + C21) * F;
	QI[1] = Q_N; 
	QI[2] = (C23 + C32) * F;
	QI[3] = (C31 - C13) * F;
	}
else if (M == C33)
	{
	QI[0] = (C13 + C31) * F;
	QI[1] = (C23 + C32) * F;
	QI[2] = Q_N; 
	QI[3] = (C12 - C21) * F;
	}
else {
	QI[0] = (C23 - C32) * F;
	QI[1] = (C31 - C13) * F;
	QI[2] = (C12 - C21) * F;
	QI[3] = Q_N; 
	}

/* correct the sign */
if ( QI[3] < 0.0 )
	{
	QI[0] = -QI[0];
	QI[1] = -QI[1];
	QI[2] = -QI[2];
	QI[3] = -QI[3];
	}

#endif
}

/* erase the defines; avoid any collisions. */
#undef QI
#undef C11
#undef C12
#undef C13
#undef C21
#undef C22
#undef C23
#undef C31
#undef C32
#undef C33

/********************************************************************/

void rot_set_q ( ROTATION *r, QUAT *q )
{
#ifdef CSU_TEST
memcpy( &r->quat, q, sizeof(QUAT) );
#else
QuaternionNormalize( q, &r->quat, CIS_MATH );
#endif

q_to_dcm( &r->dcm, &r->quat );
mat_xpose( &r->dcm_r, &r->dcm );
}

/********************************************************************/

void rot_set_dcm ( ROTATION *r, DIM33 *dcm )
{
memcpy( &r->dcm, dcm, sizeof(DIM33) );
mat_xpose( &r->dcm_r, &r->dcm );
dcm_to_quat( &r->quat, &r->dcm );
}

/********************************************************************/

void rot_xfm( VEC3 *out, VEC3 *in, ROTATION *r )
{
puts( "rot_xfm() stub" );
OUT[0] = OUT[1] = OUT[2] = 0.0;
}

/**********************************************************************
Change Log:
$Log: mathlib.c,v $
Revision 1.25  2000/05/22 22:40:56  jzhao
Fixed mat_mult function.

Revision 1.24  2000/05/16 21:43:50  klafter
Split math definitions out into MathDef.h

Revision 1.23  2000/03/02 22:10:55  sue
Had the args to QuatNormalize reversed.
Took plot_stub dependency out of makefile.

Revision 1.22  2000/03/01 23:31:00  sue
Trying to call QuaternionNormalize for tm_test.

Revision 1.21  2000/03/01 16:11:14  sue
Tweaking mathlib to find DN precision problem.
Force CIS calls to be done as doubles.

Revision 1.20  2000/02/29 19:11:20  sue
Moved all the old rts_sim files to their own sibling subdirectory per
Jane's request.

Revision 1.19  2000/02/23 18:57:26  sue
DN found another math error.  Jane hasn't broken these out from STARS.

Revision 1.18  2000/02/17 23:45:22  sue
Added dcm2q code.
CISLib reference induced loader problems with TCL.

Revision 1.17  2000/02/14 18:54:27  sue
Support for stx2sta INI file output.

Revision 1.16  2000/02/14 17:30:21  sue
Renamed mat_mult() to vec_mult() and added real mat_mult() routine.

Revision 1.15  2000/02/09 18:13:47  sue
Moved DCM_I from mathlib to draw_objs to keep SWIG from complaining.

Revision 1.14  2000/02/09 17:56:54  sue
Code cleanup based on -pedantic -Wall -ansi.

Revision 1.13  2000/02/09 15:09:47  sue
Makefile now builds lib before app.
Mathlib includes common.h
More mods for Jane's integration.

Revision 1.12  2000/01/31 19:38:43  sue
Added more system .h includes to satisfy VC5.

Revision 1.11  1999/12/03 23:06:53  sue
Moderate reorg to get ready for fov_stars.

Revision 1.10  1999/12/03 16:31:22  sue
Moved constants RTD and DTR out of .h to .c

Revision 1.9  1999/12/02 14:55:23  sue
SWIG wants (TYPE *x) rather than TYPE_PTR x.

Revision 1.8  1999/12/01 22:11:03  sue
Mathlib works with new fake_tm() from Dave Needelman's sun test case.

Revision 1.7  1999/11/30 18:00:12  sue
First cut at fake tm.
Mathlib is back to using arrays.  Investigate using ifdef SWIG.

Revision 1.6  1999/11/30 15:51:47  sue
Forgot to update pos.ra after TM input.
Working on matrix math macros which SWIG will take.

Revision 1.5  1999/11/29 17:40:54  sue
Took caps out of hv2rst/rst2hv names.
Changed st_init() to stfov_init to avoid name clash with plot.c.

Revision 1.4  1999/11/29 16:43:06  sue
Some FOV data can be calculated once.
Created hv2rst and rst2hv in mathlib.
calc_fov() is hosed; waiting for Dave Needelman.

Revision 1.3  1999/11/29 15:48:50  sue
Major update from work at home over Thanksgiving.
pixels.[ch] applies logical/engr. scaling to canvas.
Overhauled the "plot" interface; gss app now builds against better lib.
Couldn't finish plot due to lack of printer for code review.

Revision 1.4  1999/11/27 21:44:13  sue
Major overhaul.
Added direct pixel and canvas support.
Stars.[ch] should really be named gss.[ch] (TBR).
Jeff Rice's "ETK--" is now totally separate from the library.

Revision 1.3  1999/11/25 19:21:26  sue
More work on quaterions and different transforms.

Revision 1.2  1999/11/25 16:42:48  sue
Better comments and formatting.

Revision 1.1.1.1  1999/11/25 03:00:47  sue
First Laptop

Revision 1.1  1999/10/26 20:19:29  sue
Math library for quaternions, matrices, etc.
NOT integrated with CIS.

**********************************************************************/
