/********************************************************/
/* fast circle and annulus for TK */
/* $Id: annulus.c,v 1.4 2000/03/03 17:59:43 sue Exp $ */

#include <math.h>
#include <string.h>

#include "annulus.h"
#include "fov_stars.h"

/* either H or V works because it's a square. */
#define HALF_ST_FOV TRACKER_FOV_H

/********************************************************/

static double circle[NUM_PTS][2] = { {0.0} };

/********************************************************/
/* precalculate cos,sin pairs for a circle. */

void circle_init()
{
int i;
double k = 2.0 * PI / NUM_PTS;
double ang;

for (i = 0; i < NUM_PTS; ++i)
	{
	ang = k * i;
	circle[i][0] = cos( ang );
	circle[i][1] = sin( ang );
	}
}

/********************************************************/

static void circle_scale ( PT_LIST *p, double r )
{
int i;

int *ip1 = &p->points[0].x;
double *ip0 = &circle[0][0];

for (i = 0; i < (NUM_PTS * 2); ++i)
	{ *ip1++ = floor( *ip0++ * r ); }
}

/********************************************************/

static void circle_offset( PT_LIST *p, GPOINT *c )
{
int i;
for (i = 0; i < NUM_PTS; ++i)
	{
	p->points[i].x += c->x;
	p->points[i].y += c->y;
	}
}

/********************************************************/

void calc_circle( PT_LIST *p, GPOINT *c, double r )
{
p->len = NUM_PTS;
circle_scale( p, r );
circle_offset( p, c );
}

/********************************************************/

static struct {
	PT_LIST outer;
	PT_LIST inner_rev;
	} annulus;

/********************************************************/

void calc_annulus( RA_DEC *center, double center_radius )
{
PT_LIST inner;		/* temporary */
GPOINT pt;

int i;

ra_to_px( &pt, center );

calc_circle( &annulus.outer, &pt, center_radius + HALF_ST_FOV );
calc_circle( &inner, &pt, center_radius - HALF_ST_FOV );

/* reverse inner circle */
annulus.inner_rev.len = NUM_PTS;
for (i = 0; i < NUM_PTS; ++i )
	{ annulus.inner_rev.points[i] = inner.points[(NUM_PTS-1) - i]; }
}

/********************************************************/
/* draw annulus */
/* nested circles don't work. */
/* calculate two extra points which close the circle. */
/* it isn't perfect because the -smooth option in TCL rounds the */
/* endpoints just a bit. without -smooth, it's a ten-gon.  */

char *draw_annulus( RA_DEC *center, double radius )
{
static char buf0[100];
static char buf1[20];
static char buf2[100];

GPOINT *op = &annulus.outer.points[0];
GPOINT *ip = &annulus.inner_rev.points[NUM_PTS-1];

calc_annulus( center, radius );

strcpy( buf0, list_2_tcl( &annulus.outer ));
/* printf( "outer = %s\n", buf0 ); */

sprintf( buf1, " %d %d %d %d", op->x, op->y, ip->x, ip->y );
/* printf( "new pts = %s\n", buf1 ); */
strcat( buf0, buf1 );

strcpy( buf2, list_2_tcl( &annulus.inner_rev ));
/* printf( "inner = %s\n", &buf2[0] ); */
strcat( buf0, buf2 );

/* puts( "HERE\n\n" ); */
return &buf0[0];
}

/********************************************************/

#ifdef STAND_ALONE

#include <stdio.h>

int main()
{
RA_DEC test = { 45.0, -45.0 };
double center_radius = 20.5;

c_init( 300, 300 );		/* initialize canvas logic */
circle_init();			/* create circle cos/sin matrix */

puts( draw_annulus( &test, center_radius ));

return 0;
}
#endif

/********************************************************
$Log: annulus.c,v $
Revision 1.4  2000/03/03 17:59:43  sue
MSC predefines type POINT; changed to GPOINT.

Revision 1.3  2000/02/09 17:56:52  sue
Code cleanup based on -pedantic -Wall -ansi.

Revision 1.2  2000/01/31 19:38:41  sue
Added more system .h includes to satisfy VC5.

Revision 1.1  2000/01/19 16:26:06  sue
Added annulus code.

********************************************************/
