/* $Id: catalog.c,v 1.31 2000/04/20 20:29:25 sue Exp $ */

/********************************************************************/
/* AARGH */

#include <stdlib.h>
#include <math.h>

#ifdef SunOS
#define __EXTENSIONS__
char os_type[] = "sun";
#else
#define __USE_XOPEN
char os_type[] = "linux";
#endif

#include <time.h>
#include <assert.h>

#include "EPHGlobal.h"
#include "catalog.h"
#include "load_ssc_core.h"

/* turn on simple ASCII output of OSC.  STOL proc is nasty. */
#define REAL_OSC_FORMAT

/********************************************************************/
/* PUBLIC DATA */

static TIME_type	time_epoch;

MAG_VAR_ADJ mag_var_adj = { 0.0 };	/* from INI file */

double gss_start = 0.0;				/* from CFG dialog, in MJD? */

int asc_len = 0;					/* ASC length */
static STAR_PTR asc = 0;			/* ASC catalog */

static STAR_PTR *osc = 0;			/* OSC list */
int osc_len = 0;					/* OSC length */

static STAR_PTR *asc_decl = 0;		/* sort ASC by declination */

void catalog_init( void );			/* generate truth catalog */

/********************************************************************/
/*	standard EPOCH format */

static char fmt_epoch[] = "%Y/%m/%d %T";

/********************************************************************/

static void cat_exit( STR fmt, ... )
{
fprintf( stderr, fmt );
fputc( '\n', stderr );

exit(99);
}

/********************************************************************/
/* compute star epoch */

#define MJD0   2415020.0			/* from julian to MJD */
#define J2000 (2451545.0 - MJD0)

static void adj_epoch( STAR *sp )
{
#ifdef COMMENTS
1.  Gregorian 2000-01-01 12:00:00 corresponds to JDN 2451545.0.
2.  Gregorian 1858-11-17 00:00:00.00 corresponds to JDN 2400000.5; MJD 0.0.
3.  Julian -4712-01-01 12:00:00.00 corresponds to JDN 0.0.
4.  Gregorian -4713-11-24 12:00:00.00 corresponds to JDN 0.0.

SSC has epoch in true julian format (ie, days since date 3).
constants are taken from libastro.
#endif

/* convert star epoch from true julian to J2000 */
sp->epoch -= MJD0;
sp->epoch += J2000;
}

/********************************************************************/
/* magnitude residual adjustment - from Jane 10/27/99  */
/* adjustment values come from INI file */

static void adj_mag_resid( STAR_PTR sp, char class )
{
double dm;

switch( class )
	{
	case 'O': 
		dm = mag_var_adj.O;	
		break;
	case 'B':
		dm = mag_var_adj.B;	
		break;
	case 'A':  
		dm = mag_var_adj.A;	
		break;
	case 'F':  
		dm = mag_var_adj.F;	
		break;
	case 'G':  
		dm = mag_var_adj.G;	
		break;
	case 'K':  
		dm = mag_var_adj.K;	
		break;
	case 'M':  
		dm = mag_var_adj.M;	
		break;
	case 'R':  
		dm = mag_var_adj.R;	
		break;
	case 'N':  
		dm = mag_var_adj.N;	
		break;
	default:
		dm = mag_var_adj.DEFAULT;
		break;
	}
sp->resid += dm;			/* adjust magnitude residual */
}

/********************************************************************/
/* update ssc to asc. */

void ssc_2_asc( STAR_PTR sp, char class )
{

	/* ST magnitude corrections */
	{
		int i;
		for ( i = 0; i < NUM_STS; ++i)
			{ sp->mag[i+1] += sp->mag[0]; }
	}

	/* magnitude residual adjustment - from Jane 10/27/99  */
	adj_mag_resid( sp, class );
}

/********************************************************************/
/* BEGIN CATALOG SORTING SUPPORT */
/********************************************************************/

static int sign( double val )
{
if ( fabs(val ) < 1e-10) return 0;
else if (val > 0.0) return 1;
else return -1;
}

/********************************************************************/

static int cmp_decl_ra( STAR **a, STAR **b )
{
static int count = 0;

STAR_PTR x = *a;
STAR_PTR y = *b;

/*
if (0 == (count%100))
	printf( "%08x, %d, %d\n", ++count, x->asc_index, y->asc_index);
*/

if (x->pos.ra.dec - y->pos.ra.dec != 0)
	return sign( x->pos.ra.dec - y->pos.ra.dec );
else
	return sign( x->pos.ra.ra  - y->pos.ra.ra  );
}

/********************************************************************/

static void sorts()
{
typedef int (*QFP) (const void *, const void *); /* qsort fn ptr */
/* printf( "%d, %d\n", asc_len, osc_len ); */

/* sort pointers, not the catalogs themselves. */
qsort( asc_decl, asc_len, sizeof(STAR *), (QFP) cmp_decl_ra );
qsort(	&osc[0], osc_len, sizeof(STAR *), (QFP) cmp_decl_ra );
}

/********************************************************************/
/* load ssc file and convert to asc catalog. */
/* ASC catalog exists in RAM ONLY. */
/* catalog size is internally limited to 15000 stars. */

void load_ssc( int n2, STR ssc_file, long int asc_epoch )
{
int i = 0;					/* i-th item of SSC data */
int o = 0;					/* o-th OSC entry */
FP fp;						/* SSC file pointer */
STAR_PTR sp;

#ifdef SWIG_DEBUG
puts( ssc_file );
#endif

int n = min( n2 , 15000 );

fp = fopen( ssc_file, "r" );
if (!fp) cat_exit( "Can't open SSC File" );

asc = (STAR_PTR) calloc( n, sizeof( STAR ));
if (!asc) cat_exit( "Can't create ASC array." );

osc = (STAR_PTR *) calloc( n, sizeof( STAR_PTR ));
if (!osc) cat_exit( "Can't create OSC array." );

asc_decl = (STAR_PTR *) calloc( n, sizeof( STAR_PTR ));
if (!asc_decl) cat_exit( "Can't create ASC DECL array." );

/*fprintf( stderr, "reading %s\n", ssc_file );*/
/*fflush( stderr );*/

/* Update time */
Time_FillInStructure ((GSS_TIME_type) asc_epoch, &time_epoch);

/* build asc , not sorted according to declination yet */
/* i = load_ssc_jliu(time_epoch.julian_epoch,  n, fp, asc); */
i = load_ssc_core(&time_epoch,  n, fp, asc);

/* loop through asc to generate osc */
/* puts( "Ready to generate OSC table." ); */
for (n = 0; n < i; ++n)
	{
	sp = &asc[n];
	sp->asc_index = n;
	asc_decl[n] = sp;			/* save pointer for sort */
	if (sp->osc_id ) osc[o++] = sp;
	}

asc_len = i;
osc_len = o;

/* very simple sanity check on data using last star */
/* the STAR format string assumes everything is a double. */
assert( sizeof(sp->pos.ra.ra ) == sizeof(double) );
assert( sizeof(sp->pos.ra.dec) == sizeof(double) );

/* sort according to the declinations */
sorts();
}

/********************************************************************/
/* index ASC and OSC for TCL */

/* STAR * asc_idx( int i ) { return &asc[i]; } */
STAR * asc_idx( int i ) { return asc_decl[i]; }
STAR * osc_idx( int i ) { return osc[i]; }

/********************************************************************/
/* dump a star */

static void dump_star( STAR_PTR sp, FP fp )
{
static char fmt_star[] = "%8.8d\t%d\t%d\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n";
char epoch_buf[80];
struct tm *time_ptr;
int len;

/* strftime() len return unused for now */
time_ptr = gmtime( &sp->epoch );
len = strftime( epoch_buf, sizeof(epoch_buf), fmt_epoch, time_ptr );

fprintf( fp, fmt_star,
	sp->sky2000,					/* J2000 id */
	sp->bss_id,						/* BSS star identifier */
	sp->osc_id,						/* OSC identifier */
	sp->pos.eci[0],					/* reals from here on */
	sp->pos.eci[1],
	sp->pos.eci[2],
	sp->pos.ra.ra,
	sp->pos.ra.dec,
	sp->mag[0],
	sp->mag[1],
	sp->mag[2],
	sp->mag[3],
	sp->resid
	);
}

/********************************************************************/
/* dump simple ASC for user */

void dump_asc( char * asc_name, char * ssc_name, char * gss_vers, char *asc_epoch)
{
int i;
time_t t;

FP fp = fopen( asc_name, "w" );

time(&t);

fprintf( fp, "# ASC Text File\n" );
fprintf( fp, "# from SSC File: %s\n", ssc_name );
fprintf( fp, "# Using ASC Epoch %s GMT\n", asc_epoch);
fprintf( fp, "# and GSS Version %s\n", gss_vers);
fprintf( fp, "# On GMT %s#\n", ctime(&t));
fprintf( fp, "#\n");
fprintf( fp, "\nSky2000\tBSS\tOSC\teci(1)\teci(2)\teci(3)\tRA (deg)\tDEC (deg)\tMagnitude\tST 1 MAG\tST 2 MAG\tST 3 MAG\tMag Resid\n" );

for (i = 0; i < asc_len; ++i)
	{ dump_star( &asc[i], fp ); }

fclose(fp);
}

/********************************************************************/
/* dump OSC for user (sigh) */

void dump_osc( char *osc_name, char *ssc_name, char *asc_epoch, char *gss_vers,
	double min, double max, double width )
{
int i;
time_t t;

FP fp = fopen( osc_name, "w" );

time(&t);

fprintf( fp, "# OSC Text File\n" );
fprintf( fp, "# from SSC File: %s\n", ssc_name );
fprintf( fp, "# Using ASC Epoch %s GMT\n", asc_epoch);
fprintf( fp, "# and GSS Version %s\n", gss_vers);
fprintf( fp, "# On GMT %s#\n", ctime(&t));
fprintf( fp, "# For %g to %g degrees, %g deg strip width\n#\n", min, max, width);
fprintf( fp, "\nSky2000\tBSS\tOSC\teci(1)\teci(2)\teci(3)\tRA (deg)\tDEC (deg)\tMagnitude\tST 1 MAG\tST 2 MAG\tST 3 MAG\tMag Resid\n" );

for (i = 0; i < osc_len; ++i)
{
     if ( (osc[i]->pos.ra.dec <= max) && (osc[i]->pos.ra.dec >= min) )
     {
         dump_star( osc[i], fp ); 	/* get the osc star */
         
     }
     
}

fclose(fp);
}

/********************************************************************/
/* internal initialization for this package. */

void catalog_init( void )
{
/* dummy for chris' process code */
}

/********************************************************************/

int binary_search ( double val )
{
int top, bottom, middle, index;
 
top = ( val >= asc[asc_len - 1].pos.ra.dec );	/* test */

if (top)
	{
	bottom = asc_len;
 
	for (index = 1; (index < asc_len) && (top < bottom); ++index)
		{
		middle = (top + bottom)/2;
		if ( val > asc[middle].pos.ra.dec )
			top = middle + 1;	
		else
			bottom = middle;
		} 
	}
return top;
}

/********************************************************************/

#ifdef STAND_ALONE
int main()
{
load_ssc( 10667, "../tcl/SSC.dat" );
dump_osc( "../tcl/OSC.txt", "../tcl/SSC.dat", "13/39/9999", 24.5, 25.0, 1 );

return(0);
}
#endif

/**********************************************************************
Change Log:
$Log: catalog.c,v $

**********************************************************************/
