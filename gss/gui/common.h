#ifndef COMMON_H
#define COMMON_H

/* $Id: common.h,v 1.3 2000/03/02 17:49:25 sue Exp $ */

#define NUM_VTS				5
#define NUM_STS				3

/* MVC has min and max predefined */
#define min(a,b) ( ((a) < (b)) ? (a) : (b) )
#define max(a,b) ( ((a) > (b)) ? (a) : (b) )

/*
$Log: common.h,v $
Revision 1.3  2000/03/02 17:49:25  sue
min and max macros are predefined in MVC5.

Revision 1.2  2000/02/04 17:39:03  sue
CIS Integration.
SSC has time in true julian; will be switched to J2000 soon.

Revision 1.1  2000/02/01 23:48:02  sue
Adding common.h to simpify later integration with CIS and GTACS.

*/

#endif
