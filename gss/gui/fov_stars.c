/*********************************************************************/
/* $Id: fov_stars.c,v 1.12 2000/02/09 17:56:53 sue Exp $ */

#include "fov_stars.h"
#include "catalog.h"

#include <math.h>

/********************************************************************/
/* Physical tracker is +/- 4.0 degrees in HV frame. */
/* HV does NOT map directly to skymap; it's weird stuff. */
/* ST display is larger than physical to show stars just out of view. */
/* All constants are defined for one half the field of view width/height. */
/* HV constants are independent, although FOV is currently square. */

#define ST_CANVAS_MARGIN	0.25

#define DEC_FOV				(TRACKER_FOV_H + ST_CANVAS_MARGIN)
#define RA_FOV				(TRACKER_FOV_V + ST_CANVAS_MARGIN)

/* Stars below this magnitude will never be displayed in an ST. */
#define MIN_MAG				6.5

/*********************************************************************/

static double STAR_SCENE_FOV = 0.0;
static VEC3 rST_base;

static double DEAllow = 0.0;

static double HMax = 0.0;
static double HMin = 0.0;
static double VMax = 0.0;
static double VMin = 0.0;

/*********************************************************************/

void fov_init()
{
rST_base[0] = tan(TRACKER_FOV_H * DTR);
rST_base[1] = tan(TRACKER_FOV_V * DTR);
rST_base[2] = 1.0;

/* since FOV is square, either H or V will do. */
STAR_SCENE_FOV = TRACKER_FOV_H / rST_base[0];

DEAllow = DEC_FOV * sqrt(2.0);

HMax =  tan( DEC_FOV * DTR );
HMin = -tan( DEC_FOV * DTR );
VMax =  tan(  RA_FOV * DTR );
VMin = -tan(  RA_FOV * DTR );
}

/*********************************************************************/

int st_fov_stars ( FOV_STARS *fov_stars, POS *los, int st_no )
{
ST *st = get_ST(st_no);

int nstars;

double dec, MaxDE, MinDE;
double min1, min2, RAAllow, min_cosDE;

double DelRA;
int count, min_SkyMapCount;

RA_DEC ra2 = los->ra;

dec = RTD * asin( los->eci[2] );	/* Z element */

MaxDE = dec + DEAllow;
MinDE = dec - DEAllow;

if (MaxDE > 90) 
	MaxDE = 90;
if (MinDE < -90) 
	MinDE = -90;

min_SkyMapCount = binary_search (MinDE);

if (min_SkyMapCount <= 0) return(-1);

min1 = cos(MinDE * DTR);
min2 = cos(MaxDE * DTR);
min_cosDE = min( min1, min2 );

if (min_cosDE > (DEAllow * DTR))
	RAAllow = DEAllow/min_cosDE;
else
	RAAllow = 360.0;

nstars = 0;
for (count = min_SkyMapCount; nstars < STARS_PER_FOV; ++count)
	{
	STAR *star = asc_idx(count);
	HV hv;

	if (star->pos.ra.dec >= MaxDE) break;

	DelRA = fabs( star->pos.ra.ra - ra2.ra ); 

	if (((DelRA < RAAllow) || ((360.0-DelRA) < RAAllow)) &&
			(star->mag[0] < MIN_MAG))
		{
		st_RAtoHV( &hv, &star->pos.ra, st );

		if ( ((HMin <= hv.h) && (hv.h <= HMax))
			&& ((VMin <= hv.v) && (hv.v <= VMax)) )
			{
			FOV_STAR *fov = &(*fov_stars)[nstars];
			fov->hv.h = hv.h;
			fov->hv.v = hv.v;
			fov->mag  = star->mag[0];
			fov->asc_id = count;
			++nstars;
			}
		}
	}

return nstars;
}

/*********************************************************************/
/* return info on one FOV star */

char *fov_info( int stnum, int fov_star )
{
static char buf[50];
FOV_STAR *fov = &get_ST(stnum)->fov_stars[fov_star];
STAR_PTR asc = asc_idx( fov->asc_id );

sprintf( buf, "%4.1f\n%4.1f\n%4.1f\n% 5d\n%4.1f\n%4.1f\n% 5d\n",
	asc->mag[0], asc->pos.ra.ra, asc->pos.ra.dec, asc->osc_id,
	fov->hv.h, fov->hv.v, asc->sky2000 );
return buf;
}

/*********************************************************************
$Log: fov_stars.c,v $
Revision 1.12  2000/02/09 17:56:53  sue
Code cleanup based on -pedantic -Wall -ansi.

Revision 1.11  2000/02/04 17:39:03  sue
CIS Integration.
SSC has time in true julian; will be switched to J2000 soon.

Revision 1.10  2000/01/31 20:39:35  sue
More cross-platform cleanup.

Revision 1.9  2000/01/19 15:26:21  sue
Annulus gui computation.

Revision 1.8  1999/12/15 20:56:24  sue
Added simple helper function for ST popup.

Revision 1.7  1999/12/06 23:07:56  sue
Changed separate RADEC and ECI in catalog to POS.

Revision 1.6  1999/12/06 22:58:40  sue
Fixed dependency between FOV_STARS and SIM_DATA.
Almost ready to throw in J. Liu's read_ssc stuff (ugh).

Revision 1.5  1999/11/29 18:38:49  sue
Changed arg name stars_fov to fov_stars.

Revision 1.4  1999/11/29 17:34:42  sue
Changed fov_stars() loop counter from asc_index to nstars.
Working on duplicate st_init() routines in stars.c and plot.c.
There is more duplicate code that I couldn't separate out at home,
due to the lack of a printer.

Revision 1.3  1999/11/29 17:26:08  sue
Reordered objects for cleaner gss main compile.
Cleaning up st_fov skymap projection.

Revision 1.2  1999/11/29 16:43:05  sue
Some FOV data can be calculated once.
Created hv2rst and rst2hv in mathlib.
calc_fov() is hosed; waiting for Dave Needelman.

Revision 1.1  1999/11/29 15:48:49  sue
Major update from work at home over Thanksgiving.
pixels.[ch] applies logical/engr. scaling to canvas.
Overhauled the "plot" interface; gss app now builds against better lib.
Couldn't finish plot due to lack of printer for code review.

Revision 1.3  1999/11/28 08:21:30  sue
Fixing coordinate routine name typos.

Revision 1.2  1999/11/28 03:37:31  sue
Trying to get plot.c to compile.

*********************************************************************/
