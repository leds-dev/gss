#ifndef CATALOG_H
#define CATALOG_H

/* $Id: catalog.h,v 1.23 2000/02/21 23:19:45 sue Exp $ */

#include "stars.h"
#include <time.h>

/********************************************************************/
/* MAIN CATALOG TYPE DEFINITION */
/* SWIG define cuts wrapper size by 50%. */
/* magnitudes are per-display. */
/* order through resid matches Jane's StarASC_type */
/* done to avoid her having to rewrite lots of code. */

typedef struct {
	int sky2000;			/* sky2000 identifier */
	int osc_id;				/* OSC identifier - 0 => ASC Only */
	POS pos;				/* pos in RA_DEC and ECI */
	double mag[4];			/* magnitude 0=skymap, 1-3=ST1-3 */
#ifndef SWIG
	double resid;			/* magnitude variation residual */
	int asc_index;			/* &asc[asc_index] = self */
	int bss_id;				/* BSS identifier 1-65535 */
	time_t epoch;			/* star epoch */
	RA_DEC pm;				/* proper motion - rates */
	char class;
#endif
	} STAR;
typedef STAR *STAR_PTR;

/********************************************************************/
/* FOR TCL - DO NOT TOUCH */
/* these are computed by load_ssc() */

extern int asc_len;
extern STAR *asc_idx( int i );			/* i-th ASC star */

extern int osc_len;
extern STAR *osc_idx( int i );		/* i-th OSC star */

/********************************************************************/
/* data for magnitude adjustments from GSS.INI - do not touch */

typedef struct {
	double O;
	double B;
	double A;
	double F;
	double G;
	double K;
	double M;
	double R;
	double N;
	double DEFAULT;
	} MAG_VAR_ADJ;

extern MAG_VAR_ADJ mag_var_adj;

/********************************************************************/

void load_ssc( int n , char * ssc_file, long int asc_epoch );
void dump_asc( char * asc_file, char * ssc_file, char *Epoch, char *asc_epoch );
void dump_osc(
	char *osc_name, char *ssc_name, char *Epoch, char *asc_epoch,
	double min, double max, double width );

void ssc_2_asc( STAR_PTR sp, char class );	/* convert SSC entry to ASC */

int binary_search( double val );	/* search tm for value - TBR */

void catalog_init( void );			/* placeholder for gss app */

/**********************************************************************
    Change Log:
    $Log: $
**********************************************************************/

#endif
