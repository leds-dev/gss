#ifndef GSS_SLEW_H
#define GSS_SLEW_H

/* $Id: gss_slew.h,v 1.1 2000/03/01 23:32:28 sue Exp $ */

#include "mathlib.h"

typedef enum {
	SLEW_USER,
	SLEW_SUNLINE,
	SLEW_TO_SUN,
	SLEW_UPRIGHT,
	SLEW_INVERTED
	} SLEW_MODE;

typedef enum {
	INIT_SLEW,
	PERFORM_SLEW,
	CANCEL_SLEW,
	COMMIT_SLEW,
	ACCEPT_SLEW
	} SLEW_ACTION;

/* slew vector is normalized AFTER input */
void gss_slew_gui(
	SLEW_MODE slew_mode,
	VEC3 *slew_vector,
	double magnitude,
	int steps,
	SLEW_ACTION slew_action
	);

/*
$Log: gss_slew.h,v $
Revision 1.1  2000/03/01 23:32:28  sue
Interface for SLEW GUI.

*/

#endif
