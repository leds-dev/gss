#ifndef TYPES_H
#define TYPES_H

/* $Id: types.h,v 1.10 2000/03/03 22:24:56 sue Exp $ */

#include <stdio.h>
/*
typedef enum{ FALSE, TRUE }BOOL;
*/

typedef int BOOL;

typedef unsigned short US;
typedef unsigned long UL;

typedef FILE *FP;				/* file pointer */

typedef char *STR;				/* character pointer */
typedef void *VP;				/* void data pointer */

/* special item for SWIG */
#define TCL_STR char *

typedef void (*VFP)();			/* void function pointer */
typedef int (*IFP)();			/* int function pointer */

/**********************************************************************
Change Log:
$Log: types.h,v $
Revision 1.10  2000/03/03 22:24:56  sue
Evil workaround for SWIG DIM33 problem; declared sta2body as a
single-element array of type DIM33.  Seems to work.

Revision 1.9  2000/03/03 20:00:22  sue
Boolean problem only occurs on Windows in SWIG wrapper.

Revision 1.8  2000/03/03 19:01:34  sue
MSC doesn't define bool; changed to vanilla int.  Forget type safety.

Revision 1.7  2000/03/03 17:55:49  sue
Workaround to avoid predefined MS values.

Revision 1.6  1999/12/02 15:04:05  sue
Type TM_DATA only needed by rts_io.

Revision 1.5  1999/11/29 15:48:55  sue
Major update from work at home over Thanksgiving.
pixels.[ch] applies logical/engr. scaling to canvas.
Overhauled the "plot" interface; gss app now builds against better lib.
Couldn't finish plot due to lack of printer for code review.

Revision 1.2  1999/11/25 03:37:42  sue
Restored missing bodies.

Revision 1.4  1999/11/08 20:37:29  sue
Added ECI to ASC stars.  Still having trouble with strptime().

Revision 1.3  1999/11/02 22:55:02  sue
Split gss_app into main and library to support SWIG.
SWIG does not like STR typedef; investigate typemaps.

Revision 1.2  1999/11/01 23:52:06  sue
ASC now generated by C code.  Debugging core dump in parser.
SCxxxx files no longer needed; using ../tcl/ASC.txt from ryan's demo.
Need to modify tcl to compute ASC.txt line count before calling C.

Revision 1.1  1999/10/26 20:06:43  sue
General type definitions.

**********************************************************************/

#endif
