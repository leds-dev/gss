/* $Id: user_quat.c,v 1.6 2000/02/03 18:31:15 sue Exp $ */
/* for now, ignore generating the dcm and the transposes */
/* that's in either mathlib or Jane's code */

#include "user_quat.h"

/* memcpy() */
#include <string.h>

/**************************************************************/
/* USER SELECTION OF QUATERNIONS */
/**************************************************************/

static Q_SEL q_sel = INVALID;

static QUAT all_quats[ Q_ITEMS ] = {
	mQ_I,		/* Invalid */
	mQ_I,		/* TM */
	mQ_I,		/* CIS */
	mQ_I		/* User */
	};

static QUAT *eci2body = &all_quats[ INVALID ];

static void quat_recalc();

/**************************************************************/
/**************************************************************/

/* quaterion index test: 1/0 = good/bad */

static BOOL sel_ok ( Q_SEL sel )
{ return ((sel >= INVALID) && (sel <= USER)); }

/**************************************************************/
/* save values */

void quat_save( QUAT *qp, Q_SEL sel )
{
if ( sel_ok(sel) && (sel != INVALID) )
	memcpy( &all_quats[sel], &qp, sizeof( QUAT ) );
}

/**************************************************************/
/* return current value for GUI */

QUAT *gui_ival() { return eci2body; }

/**************************************************************/
/* activate selected quaternion */

void quat_select( Q_SEL sel )
{

QUAT *temp;
BOOL recalc;

if ( !sel_ok( sel )) return;

q_sel = sel;
temp = &all_quats[sel];
recalc = (temp != eci2body);
eci2body = temp;
if (recalc) quat_recalc();
}

static void quat_recalc()
{
printf( "Quat recalc\n" );
/* got me */
/* maybe it evens out in the wash */
/* let the individual thingies resync themselves... */
}

/**************************************************************/

#ifdef DEBUG_MAIN

static char *q_tag[Q_ITEMS] = { "Invalid", "TM", "CIS", "USER" };

static void q_print( QUAT *qp )
{
typedef double QA[4];

QA *qa = (QA *) qp;
int i;

printf( q_tag[ qp-all_quats ] );

for ( i = 0; i < 4; ++i)
	{ printf( "\t%lf", (*qa)[i] ); }
putchar('\n');
}

static void q_dump()
{
Q_SEL i;

printf( "QSEL = %s\nECI2BODY\n", q_tag[q_sel] );
q_print( eci2body );
putchar('\n');
for ( i = INVALID; i < Q_ITEMS; ++i )
	q_print( &all_quats[i] );
}

int main()
{
static QUAT test[6] = {
	{ 0.3, -5.1, 4.2, 26.7 },
	{ -5.1, 4.2, 26.7, 0.3 },
	{ 4.2, 26.7, 0.3, -5.1 },
	{ -0.3, 5.1, -4.2, -26.7 },
	{ 5.1, -4.2, -26.7, -0.3 },
	{ -4.2, -26.7, -0.3, 5.1 },
	};

Q_SEL i;

puts( "VIRGIN MATRIX" );
q_dump();

puts( "TEST INPUT" );
for ( i = INVALID; i < Q_ITEMS; ++i )
	{
	printf("\nQuaternion set/input %s\n", q_tag[i] );
	save( test[i], i );
	q_dump();
	}

puts( "TEST SELECTION" );
for ( i = INVALID; i < Q_ITEMS; ++i )
	{
	printf("\nQuaternion select %s\n", q_tag[i] );
	select( i );
	q_dump();
	}

return(0);
}
#endif

/**************************************************************
$Log: user_quat.c,v $
Revision 1.6  2000/02/03 18:31:15  sue
Junk at EOF.

Revision 1.5  2000/01/31 20:39:36  sue
More cross-platform cleanup.

Revision 1.4  2000/01/31 19:38:44  sue
Added more system .h includes to satisfy VC5.

Revision 1.3  1999/11/29 19:32:28  sue
Renamed save() to quat_save().  SWIG finds a LOT of name errors.

Revision 1.2  1999/11/29 15:48:55  sue
Major update from work at home over Thanksgiving.
pixels.[ch] applies logical/engr. scaling to canvas.
Overhauled the "plot" interface; gss app now builds against better lib.
Couldn't finish plot due to lack of printer for code review.

Revision 1.3  1999/11/25 19:21:26  sue
More work on quaterions and different transforms.

Revision 1.2  1999/11/25 16:31:49  sue
Changed routine name select to quat_select.

Revision 1.1  1999/11/25 16:29:45  sue
Initial release.

**************************************************************/
