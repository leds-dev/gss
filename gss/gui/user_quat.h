#ifndef USER_QUAT
#define USER_QUAT

/* $Id: user_quat.h,v 1.2 1999/11/29 15:48:55 sue Exp $ */
/* for now, ignore generating the dcm and the transposes */
/* that's in either mathlib or Jane's code */

#include "mathlib.h"

/**************************************************************/
/* user selections */

typedef enum { INVALID, TM, CIS, USER, Q_ITEMS } Q_SEL;

/**************************************************************/
/* save values */

void quat_save( QUAT *qp, Q_SEL sel );

/**************************************************************/
/* return current eci2body for GUI */

QUAT *gui_ival();

/**************************************************************/
/* activate selected quaternion */

void quat_select( Q_SEL sel );

/**************************************************************
$Log: user_quat.h,v $
Revision 1.2  1999/11/29 15:48:55  sue
Major update from work at home over Thanksgiving.
pixels.[ch] applies logical/engr. scaling to canvas.
Overhauled the "plot" interface; gss app now builds against better lib.
Couldn't finish plot due to lack of printer for code review.

Revision 1.3  1999/11/25 19:21:26  sue
More work on quaterions and different transforms.

Revision 1.2  1999/11/25 16:31:49  sue
Changed routine name select to quat_select.

Revision 1.1  1999/11/25 16:29:45  sue
Initial release.

**************************************************************/

#endif
