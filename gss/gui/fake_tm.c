/* $Id: fake_tm.c,v 1.29 2000/04/03 20:45:25 sue Exp $ */

#include <stdio.h>
#include <math.h>

/* memcpy() */
#include <string.h>

#include "stars.h"
#include "cis_intf.h"
#include "draw_objs.h"

/********************************************************************/
/* CIS will not compile if fake_tm.[ch] is not present. */
/* Done s.t. the value will NOT be lost per DN request. */

double ROUGH_KNOWLEDGE_ERROR = 5.0;

/********************************************************************/
/* if stand-alone, update ST los. */

#ifdef STAND_ALONE
#define FAKE_LOS 1
#endif

/********************************************************************/
#define FALSE 0
#define TRUE 1

static RA_DEC sun_data;
static RA_DEC bore_data;
/* static RA_DEC corner_data; */

static int sim_cis_user[NUM_STS][NUM_VTS];
static SIM_DATA_TYPE fake_sim;

static double UNUSED = 0.0;

/********************************************************************/
/* function pointer for running with compiled data vs file. */

static void load_sun1();
static VFP test_fp = load_sun1;

/********************************************************************/

typedef double QUAT_ARRAY[QUAT_LEN];

/********************************************************************/

#define ST(i) fake_sim.st[i-1]
#define VT(i,j) ST(i).vt[j-1]
#define QA(x) (*(QUAT_ARRAY *) &(x.quat) )

/* inputs */
#define stfov_corner_st(a,b)	ST(b).skyfov.rST[a-1]
#define saa_mqsttimu_0(a,b)		QA( ST(a).st2body )[b-1]
#define saa_mcsttimu_0(a,b)		UNUSED

#define saa_st_h_pos(a,b)		VT(a,b).hv.h
#define saa_st_v_pos(a,b)		VT(a,b).hv.v
#define saa_st_mag(a,b) 		VT(a,b).mag

#define mqecitimu(a)			QA(fake_sim.eci2body)[a-1]
#define stlosimu(a,b)			UNUSED
#define stloseci(a,b)			ST(a).los.eci[b-1]

/* outputs */
#define acss_mqsttimu_0(a,b)	UNUSED
#define saa_mqsttimu(a,b)		UNUSED
#define mqsttimu(a,b)			UNUSED
#define mcsttimu(a,b)			UNUSED
#define mcecitimu(a,b)			UNUSED

#define saa_ref1_st				UNUSED
#define saa_ref2_st				UNUSED

/* other data */
#define sun_ra					sun_data.ra
#define sun_dec					sun_data.dec

#define bore_ra					bore_data.ra
#define bore_dec				bore_data.dec

#define corner_ra				corner_data.ra
#define corner_dec				corner_data.dec

/********************************************************************/

static void load_sun1()
{

/* % ----------------------------- */
/* % ThreeTrackerSun Input */
/* % ----------------------------- */
 
sun_ra =   5.639183e+01;
sun_dec =  -1.469029e+01;
 
/* % Orientations */
 
/* % Corners */
#ifdef FOO 

stfov_corner_st (1,1) =  -6.958737e-02;
stfov_corner_st (1,2) =   6.958737e-02;
stfov_corner_st (1,3) =   9.951458e-01;
stfov_corner_st (2,1) =   6.958737e-02;
stfov_corner_st (2,2) =   6.958737e-02;
stfov_corner_st (2,3) =   9.951458e-01;
stfov_corner_st (3,1) =  -6.958737e-02;
stfov_corner_st (3,2) =  -6.958737e-02;
stfov_corner_st (3,3) =   9.951458e-01;
stfov_corner_st (4,1) =   6.958737e-02;
stfov_corner_st (4,2) =  -6.958737e-02;
stfov_corner_st (4,3) =   9.951458e-01;

/* % Sun Sensor */
acss_mcsstimu_0 (1,1) =   7.071068e-01;
acss_mcsstimu_0 (1,2) =   0.000000e+00;
acss_mcsstimu_0 (1,3) =   7.071068e-01;
acss_mcsstimu_0 (2,1) =   0.000000e+00;
acss_mcsstimu_0 (2,2) =   1.000000e+00;
acss_mcsstimu_0 (2,3) =   0.000000e+00;
acss_mcsstimu_0 (3,1) =  -7.071068e-01;
acss_mcsstimu_0 (3,2) =   0.000000e+00;
acss_mcsstimu_0 (3,3) =   7.071068e-01;

#endif
/* % Tracker 1; */

saa_mqsttimu_0 (1,1) =   4.237313e-01;
saa_mqsttimu_0 (1,2) =  -7.618203e-01;
saa_mqsttimu_0 (1,3) =  -3.430188e-01;
saa_mqsttimu_0 (1,4) =   3.498853e-01;

saa_mcsttimu_0 (1,1) =  -3.960641e-01;
saa_mcsttimu_0 (1,2) =  -8.856487e-01;
saa_mcsttimu_0 (1,3) =   2.424039e-01;
saa_mcsttimu_0 (2,1) =  -4.055798e-01;
saa_mcsttimu_0 (2,2) =   4.055798e-01;
saa_mcsttimu_0 (2,3) =   8.191520e-01;
saa_mcsttimu_0 (3,1) =  -8.237951e-01;
saa_mcsttimu_0 (3,2) =   2.261226e-01;
saa_mcsttimu_0 (3,3) =  -5.198368e-01;

/* % Tracker 2; */

saa_mqsttimu_0 (2,1) =  -3.794222e-02;
saa_mqsttimu_0 (2,2) =  -7.235714e-01;
saa_mqsttimu_0 (2,3) =   5.438479e-01;
saa_mqsttimu_0 (2,4) =   4.233607e-01;

saa_mcsttimu_0 (4,1) =  -6.386522e-01;
saa_mcsttimu_0 (4,2) =   5.153954e-01;
saa_mcsttimu_0 (4,3) =   5.713938e-01;
saa_mcsttimu_0 (5,1) =  -4.055798e-01;
saa_mcsttimu_0 (5,2) =   4.055798e-01;
saa_mcsttimu_0 (5,3) =  -8.191520e-01;
saa_mcsttimu_0 (6,1) =  -6.539330e-01;
saa_mcsttimu_0 (6,2) =  -7.548991e-01;
saa_mcsttimu_0 (6,3) =  -4.999048e-02;

/* % Tracker 3; */

saa_mqsttimu_0 (3,1) =   5.089286e-01;
saa_mqsttimu_0 (3,2) =  -7.790058e-02;
saa_mqsttimu_0 (3,3) =   1.954527e-01;
saa_mqsttimu_0 (3,4) =   8.346984e-01;

saa_mcsttimu_0 (7,1) =   9.114595e-01;
saa_mcsttimu_0 (7,2) =   2.469965e-01;
saa_mcsttimu_0 (7,3) =   3.289899e-01;
saa_mcsttimu_0 (8,1) =  -4.055798e-01;
saa_mcsttimu_0 (8,2) =   4.055798e-01;
saa_mcsttimu_0 (8,3) =   8.191520e-01;
saa_mcsttimu_0 (9,1) =   6.889598e-02;
saa_mcsttimu_0 (9,2) =  -8.800556e-01;
saa_mcsttimu_0 (9,3) =   4.698463e-01;

/* % Star Data (contrived); */

/* % Tracker 1:; */

saa_st_h_pos (1,1) =   2.607428e+00;
saa_st_h_pos (1,2) =  -2.119215e+00;
saa_st_h_pos (1,3) =   8.593749e-01;
saa_st_h_pos (1,4) =   1.976480e+00;
saa_st_h_pos (1,5) =   3.335055e+00;

saa_st_v_pos (1,1) =  -1.558559e+00;
saa_st_v_pos (1,2) =   3.663950e+00;
saa_st_v_pos (1,3) =   3.732660e+00;
saa_st_v_pos (1,4) =   2.807667e+00;
saa_st_v_pos (1,5) =   1.923084e+00;

saa_st_mag (1,1) =   4.447496e+00;
saa_st_mag (1,2) =   4.369231e+00;
saa_st_mag (1,3) =   5.800610e+00;
saa_st_mag (1,4) =   5.408975e+00;
saa_st_mag (1,5) =   5.712805e+00;

/* % Tracker 2:; */

saa_st_h_pos (2,1) =  -1.481197e+00;
saa_st_h_pos (2,2) =   1.088164e+00;
saa_st_h_pos (2,3) =  -2.464549e+00;
saa_st_h_pos (2,4) =  -1.772880e+00;
saa_st_h_pos (2,5) =  -3.506885e+00;

saa_st_v_pos (2,1) =   2.072219e+00;
saa_st_v_pos (2,2) =   2.916139e+00;
saa_st_v_pos (2,3) =   3.006895e+00;
saa_st_v_pos (2,4) =   2.079663e+00;
saa_st_v_pos (2,5) =   3.827189e+00;

saa_st_mag (2,1) =   2.930243e+00;
saa_st_mag (2,2) =   4.566532e+00;
saa_st_mag (2,3) =   4.686308e+00;
saa_st_mag (2,4) =   5.193578e+00;
saa_st_mag (2,5) =   5.754899e+00;

/* % Tracker 3:; */

saa_st_h_pos (3,1) =   7.335725e-01;
saa_st_h_pos (3,2) =   9.877697e-01;
saa_st_h_pos (3,3) =  -3.929598e+00;
saa_st_h_pos (3,4) =  -2.018300e+00;
saa_st_h_pos (3,5) =   2.777778e-14;

saa_st_v_pos (3,1) =  -1.963580e+00;
saa_st_v_pos (3,2) =   2.788031e-02;
saa_st_v_pos (3,3) =   3.527292e+00;
saa_st_v_pos (3,4) =  -3.531911e+00;
saa_st_v_pos (3,5) =   2.777778e-14;

saa_st_mag (3,1) =   3.623763e+00;
saa_st_mag (3,2) =   4.063947e+00;
saa_st_mag (3,3) =   4.482660e+00;
saa_st_mag (3,4) =   5.015479e+00;
saa_st_mag (3,5) =   1.000000e-10;

/* % Other data:; */
#ifdef FOO
saa_max_sun_pointing_error =   2.000000e+00;
saa_st_mag_bias_offset =   0.000000e+00;

saa_default_st (1) =   1.000000e+00;
saa_default_st (2) =   3.000000e+00;
#endif

/* % -----------------------------; */
/* % ThreeTrackerSun Results; */
/* % -----------------------------; */

saa_ref1_st =   1.000000e+00;
saa_ref2_st =   3.000000e+00;

mqecitimu (1) =   6.742967e-01;
mqecitimu (2) =   1.913022e-01;
mqecitimu (3) =  -8.774820e-02;
mqecitimu (4) =   7.078331e-01;

mcecitimu (1,1) =   9.114075e-01;
mcecitimu (1,2) =   1.337667e-01;
mcecitimu (1,3) =  -3.891566e-01;
mcecitimu (2,1) =   3.822110e-01;
mcecitimu (2,2) =   7.524846e-02;
mcecitimu (2,3) =   9.210062e-01;
mcecitimu (3,1) =   1.524834e-01;
mcecitimu (3,2) =  -9.881519e-01;
mcecitimu (3,3) =   1.745492e-02;


/* % Miscellaneous */

stlosimu (1,1) =   2.434627e-01;
stlosimu (1,2) =   8.184507e-01;
stlosimu (1,3) =  -5.204464e-01;
stlosimu (2,1) =   5.723287e-01;
stlosimu (2,2) =  -8.184507e-01;
stlosimu (2,3) =  -5.077694e-02;
stlosimu (3,1) =   3.301385e-01;
stlosimu (3,2) =   8.184507e-01;
stlosimu (3,3) =   4.702628e-01;

stloseci (1,1) =   4.547498e-01;
stloseci (1,2) =   6.077451e-01;
stloseci (1,3) =   6.510365e-01;
stloseci (2,1) =   2.023804e-01;
stloseci (2,2) =   6.581164e-02;
stloseci (2,3) =  -9.770931e-01;
stloseci (3,1) =   6.844305e-01;
stloseci (3,2) =  -3.597151e-01;
stloseci (3,3) =   6.341608e-01;

/* % Sun Sensor:; */
#ifdef FOO
acss_mcsstimu (1,1) =   6.944351e-01;
acss_mcsstimu (1,2) =  -2.222294e-02;
acss_mcsstimu (1,3) =   7.192121e-01;
acss_mcsstimu (2,1) =   3.170179e-02;
acss_mcsstimu (2,2) =   9.994973e-01;
acss_mcsstimu (2,3) =   2.738135e-04;
acss_mcsstimu (3,1) =  -7.188567e-01;
acss_mcsstimu (3,2) =   2.261017e-02;
acss_mcsstimu (3,3) =   6.947905e-01;

/* % Tracker 1:; */

saa_best_idstars (1,1) =  -6.386090e-02;
saa_best_idstars (1,2) =   2.717050e-02;
saa_best_idstars (1,3) =  -4.895435e-02;
saa_best_idstars (1,4) =  -3.350108e-02;
saa_best_idstars (1,5) =  -6.509385e-02;
saa_best_idstars (2,1) =  -3.690336e-02;
saa_best_idstars (2,2) =   4.547570e-02;
saa_best_idstars (2,3) =   3.444789e-02;
saa_best_idstars (2,4) =   5.814218e-02;
saa_best_idstars (2,5) =   1.496655e-02;
saa_best_idstars (3,1) =   9.972763e-01;
saa_best_idstars (3,2) =   9.985959e-01;
saa_best_idstars (3,3) =   9.982068e-01;
saa_best_idstars (3,4) =   9.977460e-01;
saa_best_idstars (3,5) =   9.977669e-01;
saa_best_idstars (4,1) =   1.110000e+02;
saa_best_idstars (4,2) =   6.900000e+01;
saa_best_idstars (4,3) =   3.490000e+02;
saa_best_idstars (4,4) =   4.980000e+02;
saa_best_idstars (4,5) =   5.640000e+02;
saa_best_idstars (5,1) =   3.184000e+03;
saa_best_idstars (5,2) =   3.256000e+03;
saa_best_idstars (5,3) =   3.275000e+03;
saa_best_idstars (5,4) =   0.000000e+00;
saa_best_idstars (5,5) =   3.253000e+03;
saa_best_idstars (6,1) =   8.713000e+03;
saa_best_idstars (6,2) =   8.919000e+03;
saa_best_idstars (6,3) =   8.969000e+03;
saa_best_idstars (6,4) =   9.022000e+03;
saa_best_idstars (6,5) =   8.914000e+03;
saa_best_idstars (7,1) =   2.000000e+00;
saa_best_idstars (7,2) =   1.000000e+00;
saa_best_idstars (7,3) =   4.000000e+00;
saa_best_idstars (7,4) =   5.000000e+00;
saa_best_idstars (7,5) =   3.000000e+00;
#endif

mqsttimu (1,1) =   4.238917e-01;
mqsttimu (1,2) =  -7.617311e-01;
mqsttimu (1,3) =  -3.429451e-01;
mqsttimu (1,4) =   3.499575e-01;

mcsttimu (1,1) =  -3.956911e-01;
mcsttimu (1,2) =  -8.858154e-01;
mcsttimu (1,3) =   2.424039e-01;
mcsttimu (2,1) =  -4.057505e-01;
mcsttimu (2,2) =   4.054090e-01;
mcsttimu (2,3) =   8.191520e-01;
mcsttimu (3,1) =  -8.238902e-01;
mcsttimu (3,2) =   2.257757e-01;
mcsttimu (3,3) =  -5.198368e-01;

#ifdef FOO
corner_imu(1) = {0.2071, 0.1521, 0.3304, 0.2753};
corner_imu(2) = {0.8716, 0.8152, 0.8152, 0.7587};
corner_imu(3) = {-0.4443, -0.5589, -0.4757, -0.5904};

corner_eci(1) = {0.4541, 0.3650, 0.5402, 0.4509};
corner_eci(2) = {0.5323, 0.6340, 0.5756, 0.6773};
corner_eci(3) = {0.7144, 0.6819, 0.6139, 0.5813};

corner_ra = {49.5317, 60.0706, 46.8188, 56.3498};
corner_dec = {45.5946, 42.9875, 37.8736, 35.5442};
#endif

bore_ra = 53.1941;
bore_dec = 40.6198;

#ifdef FOO
saa_best_candidate_mqecitst (1,1) =   6.837727e-02;
saa_best_candidate_mqecitst (1,2) =   4.120751e-01;
saa_best_candidate_mqecitst (1,3) =   8.067639e-01;
saa_best_candidate_mqecitst (1,4) =   4.179123e-01;

saa_best_candidate_mcecitst (1,1) =  -6.413477e-01;
saa_best_candidate_mcecitst (1,2) =   7.306663e-01;
saa_best_candidate_mcecitst (1,3) =  -2.340939e-01;
saa_best_candidate_mcecitst (2,1) =  -6.179600e-01;
saa_best_candidate_mcecitst (2,2) =  -3.110868e-01;
saa_best_candidate_mcecitst (2,3) =   7.220460e-01;
saa_best_candidate_mcecitst (3,1) =   4.547511e-01;
saa_best_candidate_mcecitst (3,2) =   6.077432e-01;
saa_best_candidate_mcecitst (3,3) =   6.510373e-01;

saa_recommendtrack (1,1) =  -3.195500e+00;
saa_recommendtrack (1,2) =   0.000000e+00;
saa_recommendtrack (1,3) =   0.000000e+00;
saa_recommendtrack (1,4) =   0.000000e+00;
saa_recommendtrack (1,5) =   0.000000e+00;
saa_recommendtrack (2,1) =  -3.523363e+00;
saa_recommendtrack (2,2) =   0.000000e+00;
saa_recommendtrack (2,3) =   0.000000e+00;
saa_recommendtrack (2,4) =   0.000000e+00;
saa_recommendtrack (2,5) =   0.000000e+00;
saa_recommendtrack (3,1) =   5.955000e+00;
saa_recommendtrack (3,2) =   0.000000e+00;
saa_recommendtrack (3,3) =   0.000000e+00;
saa_recommendtrack (3,4) =   0.000000e+00;
saa_recommendtrack (3,5) =   0.000000e+00;
saa_recommendtrack (4,1) =   3.092000e+03;
saa_recommendtrack (4,2) =   0.000000e+00;
saa_recommendtrack (4,3) =   0.000000e+00;
saa_recommendtrack (4,4) =   0.000000e+00;
saa_recommendtrack (4,5) =   0.000000e+00;
saa_recommendtrack (5,1) =   8.448000e+03;
saa_recommendtrack (5,2) =   0.000000e+00;
saa_recommendtrack (5,3) =   0.000000e+00;
saa_recommendtrack (5,4) =   0.000000e+00;
saa_recommendtrack (5,5) =   0.000000e+00;
saa_recommendtrack (6,1) =   5.000000e+00;
saa_recommendtrack (6,2) =   0.000000e+00;
saa_recommendtrack (6,3) =   0.000000e+00;
saa_recommendtrack (6,4) =   0.000000e+00;
saa_recommendtrack (6,5) =   0.000000e+00;

saa_best_err_perp (1) =   3.197792e+00;
saa_best_err_par (1) =   6.762210e+02;

/* % Tracker 2:; */

saa_best_idstars (8,1) =  -3.614710e-02;
saa_best_idstars (8,2) =  -5.086510e-02;
saa_best_idstars (8,3) =  -5.240774e-02;
saa_best_idstars (8,4) =  -3.627165e-02;
saa_best_idstars (8,5) =  -6.662296e-02;
saa_best_idstars (9,1) =  -2.583199e-02;
saa_best_idstars (9,2) =   1.896633e-02;
saa_best_idstars (9,3) =  -4.294214e-02;
saa_best_idstars (9,4) =  -3.091729e-02;
saa_best_idstars (9,5) =  -6.103259e-02;
saa_best_idstars (10,1) =   9.990126e-01;
saa_best_idstars (10,2) =   9.985254e-01;
saa_best_idstars (10,3) =   9.977021e-01;
saa_best_idstars (10,4) =   9.988636e-01;
saa_best_idstars (10,5) =   9.959098e-01;
saa_best_idstars (11,1) =   1.600000e+01;
saa_best_idstars (11,2) =   1.280000e+02;
saa_best_idstars (11,3) =   1.120000e+02;
saa_best_idstars (11,4) =   3.390000e+02;
saa_best_idstars (11,5) =   5.360000e+02;
saa_best_idstars (12,1) =   0.000000e+00;
saa_best_idstars (12,2) =   6.900000e+01;
saa_best_idstars (12,3) =   5.300000e+01;
saa_best_idstars (12,4) =   4.900000e+01;
saa_best_idstars (12,5) =   0.000000e+00;
saa_best_idstars (13,1) =   1.280000e+02;
saa_best_idstars (13,2) =   1.730000e+02;
saa_best_idstars (13,3) =   1.320000e+02;
saa_best_idstars (13,4) =   1.230000e+02;
saa_best_idstars (13,5) =   1.350000e+02;
saa_best_idstars (14,1) =   1.000000e+00;
saa_best_idstars (14,2) =   2.000000e+00;
saa_best_idstars (14,3) =   3.000000e+00;
saa_best_idstars (14,4) =   4.000000e+00;
saa_best_idstars (14,5) =   5.000000e+00;
#endif

mqsttimu (2,1) =  -3.735844e-02;
mqsttimu (2,2) =  -7.240322e-01;
mqsttimu (2,3) =   5.427373e-01;
mqsttimu (2,4) =   4.240495e-01;

mcsttimu (4,1) =  -6.375727e-01;
mcsttimu (4,2) =   5.143924e-01;
mcsttimu (4,3) =   5.734993e-01;
mcsttimu (5,1) =  -4.061975e-01;
mcsttimu (5,2) =   4.080812e-01;
mcsttimu (5,3) =  -8.176022e-01;
mcsttimu (6,1) =  -6.546026e-01;
mcsttimu (6,2) =  -7.542349e-01;
mcsttimu (6,3) =  -5.123653e-02;

#ifdef FOO
saa_best_candidate_mqecitst (2,1) =   2.720844e-01;
saa_best_candidate_mqecitst (2,2) =   9.563034e-01;
saa_best_candidate_mqecitst (2,3) =   5.968876e-02;
saa_best_candidate_mqecitst (2,4) =   8.883246e-02;

saa_best_candidate_mcecitst (4,1) =  -8.361577e-01;
saa_best_candidate_mcecitst (4,2) =   5.309951e-01;
saa_best_candidate_mcecitst (4,3) =  -1.374208e-01;
saa_best_candidate_mcecitst (5,1) =   5.097859e-01;
saa_best_candidate_mcecitst (5,2) =   8.448146e-01;
saa_best_candidate_mcecitst (5,3) =   1.625010e-01;
saa_best_candidate_mcecitst (6,1) =   2.023823e-01;
saa_best_candidate_mcecitst (6,2) =   6.582126e-02;
saa_best_candidate_mcecitst (6,3) =  -9.770921e-01;

saa_recommendtrack (7,1) =   5.076735e-02;
saa_recommendtrack (7,2) =  -2.740891e-01;
saa_recommendtrack (7,3) =   0.000000e+00;
saa_recommendtrack (7,4) =   0.000000e+00;
saa_recommendtrack (7,5) =   0.000000e+00;
saa_recommendtrack (8,1) =  -1.363702e+00;
saa_recommendtrack (8,2) =  -2.032830e+00;
saa_recommendtrack (8,3) =   0.000000e+00;
saa_recommendtrack (8,4) =   0.000000e+00;
saa_recommendtrack (8,5) =   0.000000e+00;
saa_recommendtrack (9,1) =   5.913100e+00;
saa_recommendtrack (9,2) =   6.164000e+00;
saa_recommendtrack (9,3) =   0.000000e+00;
saa_recommendtrack (9,4) =   0.000000e+00;
saa_recommendtrack (9,5) =   0.000000e+00;
saa_recommendtrack (10,1) =   4.100000e+01;
saa_recommendtrack (10,2) =   3.400000e+01;
saa_recommendtrack (10,3) =   0.000000e+00;
saa_recommendtrack (10,4) =   0.000000e+00;
saa_recommendtrack (10,5) =   0.000000e+00;
saa_recommendtrack (11,1) =   1.030000e+02;
saa_recommendtrack (11,2) =   8.300000e+01;
saa_recommendtrack (11,3) =   0.000000e+00;
saa_recommendtrack (11,4) =   0.000000e+00;
saa_recommendtrack (11,5) =   0.000000e+00;
saa_recommendtrack (12,1) =   1.000000e+00;
saa_recommendtrack (12,2) =   5.000000e+00;
saa_recommendtrack (12,3) =   0.000000e+00;
saa_recommendtrack (12,4) =   0.000000e+00;
saa_recommendtrack (12,5) =   0.000000e+00;

saa_best_err_perp (2) =   2.238946e+00;
saa_best_err_par (2) =   1.110164e+03;

/* % Tracker 3:; */

saa_best_idstars (15,1) =   3.426143e-02;
saa_best_idstars (15,2) =  -4.865310e-04;
saa_best_idstars (15,3) =  -6.137989e-02;
saa_best_idstars (15,4) =   6.156637e-02;
saa_best_idstars (15,5) =   0.000000e+00;
saa_best_idstars (16,1) =   1.279539e-02;
saa_best_idstars (16,2) =   1.723898e-02;
saa_best_idstars (16,3) =  -6.840145e-02;
saa_best_idstars (16,4) =  -3.515188e-02;
saa_best_idstars (16,5) =   0.000000e+00;
saa_best_idstars (17,1) =   9.993310e-01;
saa_best_idstars (17,2) =   9.998513e-01;
saa_best_idstars (17,3) =   9.957679e-01;
saa_best_idstars (17,4) =   9.974838e-01;
saa_best_idstars (17,5) =   0.000000e+00;
saa_best_idstars (18,1) =   4.700000e+01;
saa_best_idstars (18,2) =   8.000000e+01;
saa_best_idstars (18,3) =   1.470000e+02;
saa_best_idstars (18,4) =   4.090000e+02;
saa_best_idstars (18,5) =   0.000000e+00;
saa_best_idstars (19,1) =   3.132000e+03;
saa_best_idstars (19,2) =   3.189000e+03;
saa_best_idstars (19,3) =   3.226000e+03;
saa_best_idstars (19,4) =   0.000000e+00;
saa_best_idstars (19,5) =   0.000000e+00;
saa_best_idstars (20,1) =   8.571000e+03;
saa_best_idstars (20,2) =   8.729000e+03;
saa_best_idstars (20,3) =   8.836000e+03;
saa_best_idstars (20,4) =   8.368000e+03;
saa_best_idstars (20,5) =   0.000000e+00;
saa_best_idstars (21,1) =   1.000000e+00;
saa_best_idstars (21,2) =   2.000000e+00;
saa_best_idstars (21,3) =   3.000000e+00;
saa_best_idstars (21,4) =   4.000000e+00;
saa_best_idstars (21,5) =   0.000000e+00;
#endif

mqsttimu (3,1) =   5.083986e-01;
mqsttimu (3,2) =  -7.802538e-02;
mqsttimu (3,3) =   1.952957e-01;
mqsttimu (3,4) =   8.350464e-01;

mcsttimu (7,1) =   9.115433e-01;
mcsttimu (7,2) =   2.468259e-01;
mcsttimu (7,3) =   3.288857e-01;
mcsttimu (8,1) =  -4.054979e-01;
mcsttimu (8,2) =   4.067810e-01;
mcsttimu (8,3) =   8.185968e-01;
mcsttimu (9,1) =   6.826647e-02;
mcsttimu (9,2) =  -8.795489e-01;
mcsttimu (9,3) =   4.708858e-01;

#ifdef FOO
saa_best_candidate_mqecitst (2,1) =   2.720844e-01;
saa_best_candidate_mqecitst (2,2) =   9.563034e-01;
saa_best_candidate_mqecitst (2,3) =   5.968876e-02;
saa_best_candidate_mqecitst (2,4) =   8.883246e-02;

saa_best_candidate_mcecitst (7,1) =   6.862111e-01;
saa_best_candidate_mcecitst (7,2) =   2.396336e-02;
saa_best_candidate_mcecitst (7,3) =  -7.270076e-01;
saa_best_candidate_mcecitst (8,1) =   2.463186e-01;
saa_best_candidate_mcecitst (8,2) =   9.327546e-01;
saa_best_candidate_mcecitst (8,3) =   2.632414e-01;
saa_best_candidate_mcecitst (9,1) =   6.844279e-01;
saa_best_candidate_mcecitst (9,2) =  -3.597146e-01;
saa_best_candidate_mcecitst (9,3) =   6.341639e-01;

saa_recommendtrack (13,1) =   2.738781e+00;
saa_recommendtrack (13,2) =   2.200014e+00;
saa_recommendtrack (13,3) =   0.000000e+00;
saa_recommendtrack (13,4) =   0.000000e+00;
saa_recommendtrack (13,5) =   0.000000e+00;
saa_recommendtrack (14,1) =   4.462370e-01;
saa_recommendtrack (14,2) =   3.038800e+00;
saa_recommendtrack (14,3) =   0.000000e+00;
saa_recommendtrack (14,4) =   0.000000e+00;
saa_recommendtrack (14,5) =   0.000000e+00;
saa_recommendtrack (15,1) =   6.071800e+00;
saa_recommendtrack (15,2) =   6.115000e+00;
saa_recommendtrack (15,3) =   0.000000e+00;
saa_recommendtrack (15,4) =   0.000000e+00;
saa_recommendtrack (15,5) =   0.000000e+00;
saa_recommendtrack (16,1) =   3.212000e+03;
saa_recommendtrack (16,2) =   3.263000e+03;
saa_recommendtrack (16,3) =   0.000000e+00;
saa_recommendtrack (16,4) =   0.000000e+00;
saa_recommendtrack (16,5) =   0.000000e+00;
saa_recommendtrack (17,1) =   8.786000e+03;
saa_recommendtrack (17,2) =   8.936000e+03;
saa_recommendtrack (17,3) =   0.000000e+00;
saa_recommendtrack (17,4) =   0.000000e+00;
saa_recommendtrack (17,5) =   0.000000e+00;
saa_recommendtrack (18,1) =   5.000000e+00;
saa_recommendtrack (18,2) =   4.000000e+00;
saa_recommendtrack (18,3) =   0.000000e+00;
saa_recommendtrack (18,4) =   0.000000e+00;
saa_recommendtrack (18,5) =   0.000000e+00;

saa_best_err_perp (3) =   1.365807e+00;
saa_best_err_par (3) =   6.191420e+02;

#endif

/* all VTs valid except the last */
{
int i,j;
for (i = 0; i < NUM_STS; ++i )
	for (j = 0; j < NUM_VTS; ++j )
		sim_cis_user[i][j] = ((i != 2) || (j != 4));
}
}
/* end load_sun1() */

/********************************************************************/
/* send fake_sim data to real sim_data buffer. */
/* sim_cis_user is only output once because otherwise the real */
/* cis_user array will always be overwritten, which is wrong. */

static void sim_updates()
{
static int cis_iters = 1;

int i;
int j;
int v;

/* set tm and ephemeris valid */
sim_data.tm.ephem_valid = TRUE;
sim_data.tm.q_valid = TRUE;

/* set eci2body */
rot_set_q( &sim_data.eci2body, &fake_sim.eci2body.quat );

for (i = 0; i < NUM_STS; ++i )
	{
	ST *st = &sim_data.st[i];
	/* tm updates */

	/* st2body quaternion.  */
	rot_set_q( &st->st2body, &fake_sim.st[i].st2body.quat );

#ifdef FAKE_LOS
	/* get the vector - workaround for mathlib */
	memcpy( &st->los.eci, &ST(i+1).los.eci, sizeof(st->los.eci) );
	eci2ra( &st->los.ra, &st->los.eci );
#endif

	for (j = 0; j < NUM_VTS; ++j )
		{
		VT *vt = &sim_data.st[i].vt[j];
		memcpy( vt, &fake_sim.st[i].vt[j], sizeof(VT) );
		v = i * NUM_VTS + j + 1;
		if (cis_iters)
			cis_vt_sel( v, vt->valid = vt->tracked = sim_cis_user[i][j] );
		}
	}
if (cis_iters > 0) --cis_iters;
}

/********************************************************************/

void test_updates()
{
sim_updates();

/* sun position should come from ephemeris. */
/* it needs to be overridden for testing. */
memcpy( &sim_sunpos, &sun_data, sizeof(RA_DEC) );
}

/**********************************************************/
/* LOAD DATA FROM MATLAB SIM FILE */
/**********************************************************/

FILE *SueTestInFp = 0;
int SueTestCount = 0;
FILE *SueTestOutFp = 0;

static int NumIter = 0;

/**********************************************************/

static void field4e( double *dp )
{
static int ct = 0;
static char fmt_field4e[] = "%*s %*s %*s %le;\n";
fscanf( SueTestInFp, fmt_field4e, dp );
printf( "sim read value %d = %10.8e\n", ++ct, *dp );
}

static void mcase()
{ fscanf(SueTestInFp, "%% Case %*s;\n"); }

static void skip()
{ fscanf(SueTestInFp, " \n"); }

/**********************************************************/
/* test data is read directly into sim_data structure */

static void ReadSimInput()
{
int i, j, k;
double tmp;

ST *st;

mcase();

skip();
for ( i = 0; i < QUAT_LEN; ++i )		/* eci2body */
	field4e( &QA( fake_sim.eci2body )[i] );

skip();
for ( i = 0; i < NUM_STS; ++i )			/* st2body */
	{
	k = i + 1;
	skip();
	for ( j = 0; j < QUAT_LEN; ++j )
		field4e( &QA( ST(k).st2body )[j] );

	/* sim file has body2st quat. */
	ST(k).st2body.quat.a = - ST(k).st2body.quat.a;
	}

skip(); skip();
field4e( &sun_data.ra );				/* sun position */
field4e( &sun_data.dec );

for ( i = 0; i < NUM_STS; ++i)			/* vt tm data */
	{
	skip();

	st = &fake_sim.st[i];

	for( j = 0; j < NUM_VTS; ++j)
		field4e( &st->vt[j].hv.h );
	for( j = 0; j < NUM_VTS; ++j)
		field4e( &st->vt[j].hv.v );
	for( j = 0; j < NUM_VTS; ++j)
		field4e( &st->vt[j].mag );

	for( j = 0; j < NUM_VTS; ++j)		/* CIS VT selections */
		{
		field4e( &tmp );
		sim_cis_user[i][j] = (int) tmp;
		}
	}

skip(); skip();
}

/***************************************************************/

static void seq_sim()
{
if (SueTestCount < NumIter )
	{
	++SueTestCount;			/* increment test count */
	ReadSimInput();			/* Read Next Sim Data Set */
	}
else fclose(SueTestInFp);
}

/***************************************************************/
/* Open Input Test Files */
/* just read the first case */
/* return 1 if there was a problem with the file. */
/* doesn't actually load until fake_tm() is called. */

int load_sim( char *in )		 /* was also int NumIter */
{
int err_num = 0;

test_fp = load_sun1;			/* default to internal compiled data */

if ((!in) || (!*in)) return( 0 );

if( NULL == (SueTestInFp = fopen(in, "r")) )
	{
	fprintf(stderr, "Can't open %s\n", in);
	err_num = 1;
	}
else {
	NumIter = 1;
	SueTestCount = 0;
	test_fp = seq_sim;
	}
return( err_num );
}

/********************************************************************/
/*					SIMULATION DATA									*/

void fake_tm()
{
(*test_fp)();		/* load appropriate test data */
test_updates();		/* read into main memory */
}

/********************************************************************/

#ifdef STAND_ALONE

static ST *st = 0;

static void test_stfov( double h, double v )
{
HV hv;
RA_DEC ra;

hv.h = h;
hv.v = v;

puts( "\nconvert ST corners from HV to RA" );
st_HVtoRA( &ra, &hv, st );
printf( "hv    = %10.8lf, %10.8lf\n", hv.h, hv.v );
printf( "radec = %10.8lf, %10.8lf\n", ra.ra, ra.dec );

puts( "now go back to HV" );
st_RAtoHV( &hv, &ra, st );
printf( "radec = %10.8lf, %10.8lf\n", ra.ra, ra.dec );
printf( "hv    = %10.8lf, %10.8lf\n", hv.h, hv.v );
}

/********************************************************************/

static void fovs()
{
test_stfov(  0.0,  0.0 );
test_stfov(  4.0,  4.0 );
test_stfov( -4.0,  4.0 );
test_stfov(  4.0, -4.0 );
test_stfov( -4.0, -4.0 );
}

/********************************************************************/

static void tests()
{
VT *vt = &st->vt[0];

HV hv;
RA_DEC ra;

fovs();

puts( "\nTrying some test case data:\n" );

puts( "\nst1 ST1 vt1 hv to radec" );
st_HVtoRA( &ra, &vt->hv, st );
printf( "hv    = %10.8lf, %10.8lf\n", vt->hv.h, vt->hv.v );
printf( "radec = %10.8lf, %10.8lf\n", ra.ra, ra.dec );

puts( "\nConverting fake boresight from radec to hv" );
st_RAtoHV( &hv, &bore_data, st );
printf( "radec = %10.8lf, %10.8lf\n", bore_data.ra, bore_data.dec );
printf( "hv    = %10.8lf, %10.8lf\n", hv.h, hv.v );
}

/********************************************************************/

static void quat_print( QUAT *q )
{
printf( "\tQUAT (x,y,z,a) = %10.8lf, %10.8lf, %10.8lf, %10.8lf\n",
	q->x, q->y, q->z, q->a );
}

static void v3_print( VEC3 *v )
{ printf( "\t%10.8f %10.8f %10.8f\n", (*v)[0], (*v)[1], (*v)[2] ); }

static void dim33_print( DIM33 *d )
{
int i;
for (i = 0; i < 3; ++i)
	v3_print( &(*d)[i] );
}

static void rot_print( STR s, ROTATION *r )
{
puts( s );
quat_print( &r->quat );
puts( "\txx2body dcm" );
dim33_print( &r->dcm );
puts( "\tbody2xx dcm" );
dim33_print( &r->dcm_r );
}

static void rot_dump()
{
static char tag[] = "ST x";
int i;

rot_print( "ECI2BODY", &sim_data.eci2body );
for (i = 0; i < NUM_STS; ++i )
	{
	tag[3] = '1' + i;
	rot_print( tag, &sim_data.st[i].st2body );
	}
}

/********************************************************************/

int main( int argc, char *argv[] )
{
st = &sim_data.st[0];
load_sim( argc > 1 ? argv[1] : 0 );
fake_tm();
fake_tm();		/* 2nd call to make sure cis_user is cut out. */
rot_dump();
tests();
return(0);
}
#endif

/********************************************************************
$Log: fake_tm.c,v $
Revision 1.29  2000/04/03 20:45:25  sue
Better comments on simulation setup.
sim_updates() should be a static function.

Revision 1.28  2000/04/03 20:39:40  sue
Added second call to fake_tm() in test() to show user_sel cutout works.

Revision 1.27  2000/04/03 18:36:31  sue
Cleaning up comments.

Revision 1.26  2000/03/28 18:20:55  sue
Cleaned up tm interface.

Revision 1.25  2000/03/13 21:32:57  sue
Allow sim to override EPHEM sun position.
Found typo in test reader.

Revision 1.24  2000/03/13 19:26:03  sue
Simulation needs to override EPHEM sun pos.

Revision 1.23  2000/03/01 23:30:59  sue
Trying to call QuaternionNormalize for tm_test.

Revision 1.22  2000/02/23 22:03:55  sue
Fixed off by one error in index.  More printout in STAND_ALONE.

Revision 1.21  2000/02/22 16:40:46  sue
Accidentally inverted the arguments for ST FOV corners for
the old compiled sun1 test case, which now works.
Jane told me DN test cases input BODY2ST quaternions, not ST2BODY ECI
as the RTS and old sim1 did.  Fix not tested yet.

Revision 1.20  2000/02/22 15:33:04  sue
Took "MAX_" from ROUGH_KNOWLEDGE_ERROR at Jane's request (name clash).

Revision 1.19  2000/02/21 23:35:09  sue
Fake value for GUI input MAX_ROUGH_KNOWLEDGE_ERROR.

Revision 1.18  2000/02/17 23:47:16  sue
Logic allowed CIS user picks to be written out more than once.

Revision 1.17  2000/02/17 01:26:17  sue
CIS simulation also wants to input CIS user selections.
Had to turn off reading TM once a second to keep TM from
overwriting GUI picks.  Added a one-shot flag to the writes.

Revision 1.16  2000/02/16 17:58:13  sue
Found pointer error.
Sim file forces VT valid, tracking, and CIS selected.

Revision 1.15  2000/02/16 17:07:47  sue
Deleted unused CIS_MODE variable.
tm_test target now debuggable.

Revision 1.14  2000/02/15 23:40:01  sue
Fake TM reader matches new matlab simfile format.
Added makefile target for debugging.

Revision 1.13  2000/02/15 20:56:53  sue
Jane's Test.c doesn't work with /home/davidn/MC_in_0.m.
Data loads correctly but ST still shows numerical problems.

Revision 1.12  2000/02/15 17:28:46  sue
Inverted track/valid logic during merge.

Revision 1.11  2000/02/14 17:32:48  sue
Added simulation reader code from Jane's Test.c.

Revision 1.10  2000/02/09 17:56:53  sue
Code cleanup based on -pedantic -Wall -ansi.

Revision 1.9  2000/01/31 19:38:42  sue
Added more system .h includes to satisfy VC5.

Revision 1.8  2000/01/19 17:54:36  sue
Forgot to define and set ephem_valid in fake_tm.

Revision 1.7  2000/01/14 18:21:53  sue
Fake TM quaterion is valid.

Revision 1.6  1999/12/13 14:52:17  sue
Added QUAT * cast to calls for eci2body and body2st.

Revision 1.5  1999/12/01 23:41:30  sue
Changed floats to doubles everywhere in code.

Revision 1.4  1999/12/01 22:11:02  sue
Mathlib works with new fake_tm() from Dave Needelman's sun test case.

Revision 1.3  1999/11/30 18:07:37  sue
Compiles.

Revision 1.2  1999/11/30 18:00:11  sue
First cut at fake tm.
Mathlib is back to using arrays.  Investigate using ifdef SWIG.

********************************************************************/
