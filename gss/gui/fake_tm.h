/* $Id: fake_tm.h,v 1.4 2000/02/22 15:33:04 sue Exp $ */

/********************************************************************/
/* eventually from GUI; here per DN s.t. it won't get lost. */
/* CIS will not compile if fake_tm.[ch] is not present. */

extern double ROUGH_KNOWLEDGE_ERROR;

/********************************************************************/
/* writes to global sim_data buffer in stars.h */

void fake_tm();

/********************************************************************/
/* load sim data from matlab file. */
/* pass 0 or empty string for internal sun mode data set */

int load_sim( char *in );		 /* was also int NumIter */

/********************************************************************
$Log: fake_tm.h,v $
Revision 1.4  2000/02/22 15:33:04  sue
Took "MAX_" from ROUGH_KNOWLEDGE_ERROR at Jane's request (name clash).

Revision 1.3  2000/02/21 23:35:09  sue
Fake value for GUI input MAX_ROUGH_KNOWLEDGE_ERROR.

Revision 1.2  2000/02/14 17:32:49  sue
Added simulation reader code from Jane's Test.c.

Revision 1.1  1999/11/30 15:55:03  sue
Initial release of fake_tm setup.  No one has reviewed the data values.

********************************************************************/
