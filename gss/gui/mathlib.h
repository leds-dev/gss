#ifndef MATHLIB_H
#define MATHLIB_H

/* $Id: mathlib.h,v 1.21 2000/05/16 21:43:50 klafter Exp $ */

#include "types.h"
#include "common.h"
#include "MathDef.h"

/******************************************************************/

typedef struct {
	double ra;
	double dec;
	} RA_DEC;
typedef RA_DEC *RD_PTR;

/******************************************************************/

typedef struct {
	double h;
	double v;
	} HV;
typedef HV *HV_PTR;

/******************************************************************/
/* SWIG isn't happy with arrays */

#define VEC3_LEN 3
typedef double VEC3[ VEC3_LEN ];

typedef VEC3 *VEC3_PTR;
typedef VEC3 *ECI;

/******************************************************************/

#define DIM3_LEN 3
typedef VEC3 DIM33[ DIM3_LEN ];

typedef DIM33 *DP;

/**************************************************************/

typedef struct {
	double x;	/* is Q VEC3 + rotation? - not really. */
	double y;
	double z;
	double a;

#ifdef SWIG
	QUAT();
	~QUAT();
#endif
	} QUAT;

typedef QUAT *QUAT_PTR;
#define QUAT_LEN (sizeof(QUAT)/sizeof(double))

#define mQ_I { 0.0, 0.0, 0.0, 1.0 }
QUAT Q_I;			/* const invalid/initial quaternion */

/******************************************************************/
/* position is calculated in ECI and RADEC */

typedef struct {
	RA_DEC ra;
	VEC3 eci;
	} POS;

/******************************************************************/
/* rotation holds quat, dcm and dcm transpose */
/* update only when new values are input */
/* sense of quaternion and dcm matches the data name, */
/* ie, eci2body.quat or dcm, but body2eci is eci2body.dcm_r */
/* Inverse quaternions only change the sign of a, so this can */
/* be done on the fly if necessary. */

typedef struct {
	QUAT quat;
	DIM33 dcm;
	DIM33 dcm_r;
	} ROTATION;
typedef ROTATION *ROT_PTR;

/******************************************************************/
/* external functions */
/* output in the first parameter, followed by inputs. */
/* this looks more algebraic, although it's less OOP. */
/* note that pointers are passed, rather than copied structures. */

#ifndef SWIG

/******************************************************************/
/* convert +/- 180 to 0-360 */

double modulus_360 ( double in );		/* mod360 */

void eci2ra( RA_DEC *ra2, VEC3 *eci );	/* from eci to RA */
void ra2eci( VEC3 *eci, RA_DEC *ra2 );	/* from RA to ECI */

void hv2rst( VEC3 *rST, HV* hv );		/* from HV to rST */
void rst2hv( HV *hv, VEC3 *rST );		/* from rST to HV */

void unitize ( VEC3 *in );
void vec_mult ( VEC3 *out, DIM33 *dcm, VEC3 *in );

void mat_xpose( DIM33 *in, DIM33 *out );	/* transpose */
void mat_inv( DIM33 *in, DIM33 *out );	/* orthogonal, so just call xpose() */
void mat_mult ( DIM33 *out, DIM33 *left, DIM33 *right );
#endif

void rot_set_q( ROTATION *r, QUAT *q );
void rot_set_dcm( ROTATION *r, DIM33 *dcm );

#ifndef SWIG
void rot_xfm( VEC3 *out, VEC3 *in, ROTATION *r );
#endif

/**********************************************************************
Change Log:
$Log: mathlib.h,v $
Revision 1.21  2000/05/16 21:43:50  klafter
Split math definitions out into MathDef.h

Revision 1.20  2000/03/03 21:22:37  sue
Symbol table overflow.  Took out unneeded data types and changed
compiler PIC code option to bigger tables.

Revision 1.19  2000/02/14 23:14:39  sue
Added st.o to makefile.
Took rot_xfm out of SWIG.

Revision 1.18  2000/02/14 18:54:27  sue
Support for stx2sta INI file output.

Revision 1.17  2000/02/14 17:30:21  sue
Renamed mat_mult() to vec_mult() and added real mat_mult() routine.

Revision 1.16  2000/02/09 18:13:47  sue
Moved DCM_I from mathlib to draw_objs to keep SWIG from complaining.

Revision 1.15  2000/02/09 15:09:47  sue
Makefile now builds lib before app.
Mathlib includes common.h
More mods for Jane's integration.

Revision 1.14  2000/01/31 19:38:43  sue
Added more system .h includes to satisfy VC5.

Revision 1.13  1999/12/07 21:23:08  sue
SWIG will take mathlib.h, except for the function decls.
Still working on ST interface.

Revision 1.12  1999/12/03 23:06:53  sue
Moderate reorg to get ready for fov_stars.

Revision 1.11  1999/12/03 16:31:22  sue
Moved constants RTD and DTR out of .h to .c

Revision 1.10  1999/12/02 15:24:22  sue
Added ifdef to make sure SWIG only sees RA_DEC and HV.

Revision 1.9  1999/12/02 14:55:23  sue
SWIG wants (TYPE *x) rather than TYPE_PTR x.

Revision 1.8  1999/12/01 23:41:31  sue
Changed floats to doubles everywhere in code.

Revision 1.7  1999/12/01 22:11:03  sue
Mathlib works with new fake_tm() from Dave Needelman's sun test case.

Revision 1.6  1999/11/30 18:00:12  sue
First cut at fake tm.
Mathlib is back to using arrays.  Investigate using ifdef SWIG.

Revision 1.5  1999/11/30 15:51:48  sue
Forgot to update pos.ra after TM input.
Working on matrix math macros which SWIG will take.

Revision 1.4  1999/11/29 16:43:06  sue
Some FOV data can be calculated once.
Created hv2rst and rst2hv in mathlib.
calc_fov() is hosed; waiting for Dave Needelman.

Revision 1.3  1999/11/29 15:48:50  sue
Major update from work at home over Thanksgiving.
pixels.[ch] applies logical/engr. scaling to canvas.
Overhauled the "plot" interface; gss app now builds against better lib.
Couldn't finish plot due to lack of printer for code review.

Revision 1.4  1999/11/27 21:44:13  sue
Major overhaul.
Added direct pixel and canvas support.
Stars.[ch] should really be named gss.[ch] (TBR).
Jeff Rice's "ETK--" is now totally separate from the library.

Revision 1.3  1999/11/25 19:21:26  sue
More work on quaterions and different transforms.

Revision 1.2  1999/11/25 16:42:48  sue
Better comments and formatting.

Revision 1.1.1.1  1999/11/25 03:00:47  sue
First Laptop

Revision 1.1  1999/10/26 20:03:49  sue
Interface to math library.  Includes quaternions, matrix ops, etc.
It is NOT yet integrated with CIS.

**********************************************************************/

#endif
