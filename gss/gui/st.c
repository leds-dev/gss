#include "st.h"
#include "stars.h"

/* $Id: st.c,v 1.5 2000/03/03 22:24:56 sue Exp $ */

DIM33 sta2body[1] = {{{1.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,1.0}}};

DIM33 stx2sta[NUM_STS] = {
	{{1.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,1.0}},
	{{1.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,1.0}},
	{{1.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,1.0}}
	};

#define STX(i) &sim_data.st[i].st2body.dcm
#define STXI(i) &sim_data.st[i].st2body_i.dcm

/*************************************************************/

static void addr( DIM33 *p ) { printf( "%08lx\n", (UL) p ); }

/*************************************************************/

static void dim3pr( DIM33 *d )
{
int i,j;

addr(d);

for ( i = 0; i < 3; ++i)
	{
	for ( j = 0; j < 3; ++j)
		{ printf("\t%f", (*d)[i][j] ); }
	putchar('\n');
	}

putchar('\n');
}

/*************************************************************/

void calc_stx()
{
	int i;

	puts("STA2BODY");
	dim3pr( &sta2body[0] );

	for (i = 0; i < NUM_STS; ++i )
	{
		mat_mult( STXI(i), &sta2body[0], &stx2sta[i] );

		printf( "STX2STA %d\n", i );
		dim3pr( &stx2sta[i] );

		printf( "st2body %d\n", i);
		dim3pr( STXI(i) );

		rot_set_dcm (&sim_data.st[i].st2body_i, STXI(i) );
		printf ( "Initial body2st %d\n", i);
		dim3pr( &sim_data.st[i].st2body_i.dcm_r );

		/* Initial the st2body */
		sim_data.st[i].st2body = sim_data.st[i].st2body_i;
	}
}

/*************************************************************/

void stx_inv()
{
int i;
DIM33 sta_inv;
mat_inv( &sta_inv, &sta2body[0] );
for (i = 0; i < NUM_STS; ++i )
	mat_mult( &stx2sta[i], &sta_inv, STXI(i) );
}

/*************************************************************/
/* helper functions for SWIG */

DIM33 *dbl2dcm( double *dp ) { return (DIM33 *) dp; }
double *dcm2dbl( DIM33 *dp ) { return (double *) dp; }
double *get_stx2sta( int st ) { return (double *) &stx2sta[st-1]; }

/*****************************************************************
$Log: st.c,v $
Revision 1.5  2000/03/03 22:24:56  sue
Evil workaround for SWIG DIM33 problem; declared sta2body as a
single-element array of type DIM33.  Seems to work.

Revision 1.4  2000/02/16 23:01:38  sue
ST and DCM input works.
Fixing CIS button logic.

Revision 1.3  2000/02/16 19:25:14  sue
Prettier.

Revision 1.2  2000/02/14 23:13:57  sue
Added DIM33 DCM helper functions.

Revision 1.1  2000/02/14 18:54:28  sue
Support for stx2sta INI file output.

*****************************************************************/
