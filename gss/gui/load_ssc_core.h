/* $Id: jliu_ssc.h,v 1.2 1999/12/07 15:43:16 sue Exp $ */

#include "catalog.h"

int load_ssc_core(TIME_type *time_epoch_pt, int max_lines, FILE *SSC, STAR *asc);

/*
$Log: jliu_ssc.h,v $
Revision 1.2  1999/12/07 15:43:16  sue
Added stand-alone target for testing.
Added support for blank lines and comments in SSC file.
Needed to allow for CVS version ID in SSC file.

Revision 1.1  1999/12/06 23:34:27  sue
Added J. Liu's SSC/ASC code.  I can't parse his SSC file or figure out
his code, so I'm just putting a wrapper around it.

*/
