/********************************************************************/

#ifndef FOVSTARS_H
#define FOVSTARS_H

/* $Id: fov_stars.h,v 1.6 2000/01/19 15:26:22 sue Exp $ */

#include "mathlib.h"

/* Physical tracker */
#define TRACKER_FOV_H		4.0
#define TRACKER_FOV_V		4.0

typedef struct {
	HV hv;
	double mag;
	int asc_id;
	} FOV_STAR;

#define STARS_PER_FOV 50
typedef FOV_STAR FOV_STARS[ STARS_PER_FOV ];

/********************************************************************/
/* calculate stars in ST FOV */
/* int st_no is a hack - TBR */

int st_fov_stars ( FOV_STARS *fov_stars, POS *los, int st_no );

/********************************************************************/
/* report fov star info for TCL */

char *fov_info( int stnum, int fov_star );

/********************************************************************
$Log: fov_stars.h,v $
Revision 1.6  2000/01/19 15:26:22  sue
Annulus gui computation.

Revision 1.5  1999/12/15 20:56:24  sue
Added simple helper function for ST popup.

Revision 1.4  1999/12/06 22:58:40  sue
Fixed dependency between FOV_STARS and SIM_DATA.
Almost ready to throw in J. Liu's read_ssc stuff (ugh).

Revision 1.3  1999/12/01 23:41:31  sue
Changed floats to doubles everywhere in code.

Revision 1.2  1999/11/29 16:43:06  sue
Some FOV data can be calculated once.
Created hv2rst and rst2hv in mathlib.
calc_fov() is hosed; waiting for Dave Needelman.

Revision 1.1  1999/11/29 15:48:49  sue
Major update from work at home over Thanksgiving.
pixels.[ch] applies logical/engr. scaling to canvas.
Overhauled the "plot" interface; gss app now builds against better lib.
Couldn't finish plot due to lack of printer for code review.

Revision 1.2  1999/11/28 03:37:31  sue
Trying to get plot.c to compile.

********************************************************************/
#endif
