/********************************************************/
/* fast circle and annulus for TK */
/* $Id: annulus.h,v 1.2 2000/03/03 17:59:44 sue Exp $ */

#include "pixels.h"

/********************************************************/
/* precalculate cos,sin pairs for a circle. */

void circle_init();

/********************************************************/
/* calculate points on a circle */

void calc_circle( PT_LIST *p, GPOINT *c, double r );

/********************************************************/
/* calculate an annulus (two nested circles ) */
/* the delta between them is fixed at ST_FOV_WIDTH / 2 */

void calc_annulus( RA_DEC *center, double center_radius );

/********************************************************/
/* draw annulus */
/* nested circles don't work. */
/* calculate two extra points which close the circle. */
/* it isn't perfect because the -smooth option in TCL rounds the */
/* endpoints just a bit. without -smooth, it's a ten-gon.  */

char *draw_annulus( RA_DEC *center, double radius );

/********************************************************
$Log: annulus.h,v $
Revision 1.2  2000/03/03 17:59:44  sue
MSC predefines type POINT; changed to GPOINT.

Revision 1.1  2000/01/19 16:26:06  sue
Added annulus code.

********************************************************/
