#ifndef CIS_INTF_H
#define CIS_INTF_H

/* $Id: cis_intf.h,v 1.21 2000/05/22 23:54:36 jzhao Exp $ */

#include "common.h"
#include "stars.h"

/**************************************************/
/* INI variables passed through to CIS. */

typedef struct {
	double MAX_ST_MISALIGNMENT_ERROR;
	double MAX_SEPARAT_PRIMARY_TOL;
	double PARALLEL_ERROR_THRESHOLD;
	double MAG_SATURATION;
	double MAGNITUDE_BIAS;
	double SMS_FOV;
	double ATTITUDE_UNCERTAINTY;
	double OVERLAP_PIXELS;
	double SPREAD_LIMIT;
	} CIS_INI_VARS;

extern CIS_INI_VARS cis_ini;

/**************************************************/
/* fake sun position for integration test */

extern RA_DEC sim_sunpos;

/**************************************************/
/* THE FOLLOWING DATA IS GLOBAL BUT HIDDEN FROM SWIG/TCL. */

#ifndef SWIG

/* ---------------------------------------------- */
/* snapshot buffer for CIS Library. */

extern SIM_DATA_TYPE snapshot;

/* ---------------------------------------------- */
/* user's selection of VT's for CIS input */

typedef int CIS_USER[NUM_STS][NUM_VTS];
extern CIS_USER cis_user;

/* ---------------------------------------------- */
/* dummy CIS output area - GUI displays TBD. */

typedef struct {
	char msg[1000];
	double tbd[20];
	} CIS_OUT;

extern CIS_OUT cis_data;

#endif

/**************************************************/
/* CIS_SNAPSHOT() MUST BE SYNCHRONIZED by GTACS TM update. */
/* DO NOT INVOKE CIS_SNAPSHOT() DIRECTLY. */
/* actual snapshot buffer should be private. */

void cis_snapshot();	/* take snapshot */

/**************************************************/

char *cis_result();		/* report cis result */

/**************************************************/
/* return formatted VT info string for CIS dialog */
/* vt = 1-15 */

char *cis_vtinfo( int vt );

/**************************************************/
/* return VT visible status = tracking and valid. */
/* vt = 1-15 */

int cis_vt_vis( int vt );

/**************************************************/
/* set user CIS VT selection. */
/* button will trigger it only if vt is visible. */
/* vt = 1-15 */

void cis_vt_sel( int vt, int val );

/***************************************************
$Log: cis_intf.h,v $
Revision 1.21  2000/05/22 23:54:36  jzhao
Removed cis_sunhold, cis_rough and cis_commit functions.

Revision 1.20  2000/05/17 00:22:12  jzhao
Remove passing variables from cis_sunhold and cis_rough to match CIS functions

Revision 1.19  2000/04/03 14:02:42  sue
Cleaned up all the CIS test code.

Revision 1.18  2000/03/27 23:15:35  sue
Added CIS Attitude Uncertainty per Jane.

Revision 1.17  2000/03/13 21:32:57  sue
Allow sim to override EPHEM sun position.
Found typo in test reader.

Revision 1.16  2000/03/13 19:26:03  sue
Simulation needs to override EPHEM sun pos.

Revision 1.15  2000/02/22 21:55:11  sue
Added reference to Jane's CIS logfile.  Turns out it
can't be shared with TCL just yet; TCL type not compatible.

Revision 1.14  2000/02/17 23:46:09  sue
Added real CIS calls; TCL loader problems.

Revision 1.13  2000/02/17 01:26:17  sue
CIS simulation also wants to input CIS user selections.
Had to turn off reading TM once a second to keep TM from
overwriting GUI picks.  Added a one-shot flag to the writes.

Revision 1.12  2000/02/16 23:01:38  sue
ST and DCM input works.
Fixing CIS button logic.

Revision 1.11  2000/02/15 19:03:18  sue
ifndef was missing trailing _H.

Revision 1.10  2000/02/15 18:49:38  sue
Hooking GUI up to real CIS data.

Revision 1.9  2000/02/09 22:17:40  sue
Initial release.  Moving TCL dev layer to C.

Revision 1.8  2000/02/07 22:56:57  sue
Code updates for header changes from Jane last week.
cis buffer now global.
ASC does not need class or subclass.
Renamed asc_id to asc_index for clarity.

Revision 1.7  2000/02/04 17:39:03  sue
CIS Integration.
SSC has time in true julian; will be switched to J2000 soon.

Revision 1.6  2000/02/01 22:01:04  sue
Added cis_commit() and cis_result() per GUI updates.
Added cis_id to SIM_DATA to support CIS recommended IDs.

Revision 1.5  2000/01/31 19:38:42  sue
Added more system .h includes to satisfy VC5.

Revision 1.4  1999/12/17 19:13:48  sue
CIS updates user_sel at snapshot and before calling Jane's CIS.

Revision 1.3  1999/11/29 15:48:48  sue
Major update from work at home over Thanksgiving.
pixels.[ch] applies logical/engr. scaling to canvas.
Overhauled the "plot" interface; gss app now builds against better lib.
Couldn't finish plot due to lack of printer for code review.

Revision 1.1.1.1  1999/11/25 03:00:47  sue
First Laptop

Revision 1.2  1999/11/08 20:03:13  sue
Added array cis_user for VT selections.
Added snapshot stub.

Revision 1.1  1999/11/04 20:00:33  sue
Added CIS interface for TCL/SWIG DLL.

***************************************************/

#endif
