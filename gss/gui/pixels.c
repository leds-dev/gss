/*******************************************************************/
/* $Id: pixels.c,v 1.16 2000/03/03 17:59:44 sue Exp $ */

#include "pixels.h"
#include "mathlib.h"
#include <string.h>

#ifdef STAND_ALONE
#define DEBUG
#endif

#ifdef DEBUG
#define OUTPUT printf
#else
/* eat arguments */
static void OUTPUT( char *fmt, ... ) {}
#endif

/*******************************************************************/

typedef struct {
	double x0;		/* logical origin */
	double x;		/* logical width */
	double M;		/* k/x */
	int k0;			/* physical origin */
	int k;			/* physical width */
	} CANVAS;

/*******************************************************************/

static CANVAS sky_x = { 360.0, -360.0 };	/* right ascension */
static CANVAS sky_y = {  90.0, -180.0 };	/* declination */

static CANVAS st_x = {  4.5, -9.0 };		/* H (+/- 4.0) */
static CANVAS st_y = {  4.5, -9.0 };		/* V (+/- 4.0) */

/* MARGIN defines the margin about the edge of each canvas. */
/* Used for the axis grids and other decoration. */
static int MARGIN = 20;						/* pixels */

/*******************************************************************/

static int to_pix( double x, CANVAS *c )
{ return (int)(( x - c->x0 ) * c->M ) + c->k0; }

/*******************************************************************/

static void set_c ( CANVAS *c, int size, int offset )
{
c->k = size;
c->k0 = offset + MARGIN;
c->M = ((double) (size - 2 * MARGIN)) / c->x;
}

/*******************************************************************/
/* for debugging */

static struct {
	int width;
	int height;
	double grid;
	} screen;

/*******************************************************************/

void c_init( int w, int h )
{
static double APP_LIMIT_RATIO = 0.9;

int a,b,c,d,e;

/* save values */
screen.width = w;
screen.height = h;
OUTPUT( "screen width, height = (%d, %d)\n", w, h);

/* limit total size to .8 of screen */
/* if < 800x600, go full size */

a = w * APP_LIMIT_RATIO;
b = h * APP_LIMIT_RATIO;

if (a < 800) a = w;
if (b < 600) b = h;

OUTPUT( "max width, height = (%d, %d)\n", a, b);

c = a/3;			/* three cols */
d = b/2;			/* two rows */
OUTPUT( "grid width, height = (%d, %d)\n", c, d);

/* grid must be square */
e = min( c, d );
screen.grid = e;
OUTPUT( "grid size = %d\n", e );

/* skymap in row 0, cols 0-1 */
/* legend in row 0, col 2 is irrelevant for now */
set_c( &sky_x, 2 * e, 0 );
set_c( &sky_y, e, 0 );

/* star trackers in row 1, cols 0-2 */
set_c( &st_x, e, 0 );
set_c( &st_y, e, 0 );
}

/*******************************************************************/

double get_grid() { return screen.grid; }

/*******************************************************************/

void ra_to_px( GPOINT *p, RA_DEC *ra2 )
{
OUTPUT( "ra_to_px point = %08lx\n", (UL) p );
OUTPUT( "\tp = %d, %d\n", p->x, p->y );
p->x = to_pix( ra2->ra , &sky_x );
p->y = to_pix( ra2->dec, &sky_y );
OUTPUT( "\tp = %d, %d\n", p->x, p->y );
}

/*******************************************************************/

void hv_to_px( GPOINT *p, HV *hv )
{
p->x = to_pix( hv->h , &st_x );
p->y = to_pix( hv->v, &st_y );
}

/*********************************************************************/

char *px2str( GPOINT *p )
{
static char buf[20];
sprintf( &buf[0], " %d %d", p->x, p->y );
return &buf[0];
}

/*********************************************************************/

char *list_2_tcl( PT_LIST *l )
{
static char buf[200];
GPOINT *p;
int i;

/* puts( "C list_2_tcl" ); */

buf[0] = '\0';				/* erase any old data */

for (i = 0; i < l->len; ++i )
	{
	char buf2[20];
	p = &l->points[i];
/*	printf( "pt %d = %d, %d\n", i, p->x, p->y ); */
	sprintf( buf2, " %d %d", p->x, p->y );
	strcat( buf, buf2 );
	}
return &buf[0];
}

/*********************************************************************/

#ifdef STAND_ALONE
static void print_c ( CANVAS* c )
{
printf ("x0=%lf, x=%lf, M=%lf,k0=%d,k=%d\n",
	c->x0,		/* logical origin */
	c->x,		/* logical width */
	c->M,		/* k/x */
	c->k0,		/* physical origin */
	c->k		/* physical width */
	);
}

static void test_c( CANVAS *cp, STR tag, int a, int b, int c, double d )
{
int i;
print_c( cp );

for (i = a; i <= b; i += c)
	printf( "%d: %s = %d\n", i, tag, to_pix( i/d, cp ));
}

int main()
{
c_init( 1024, 768 );

/* skymap */
test_c( &sky_x, "sky_x", 0, 360, 90, 1.0 );
test_c( &sky_y, "sky_y", -90, 90, 45, 1.0 );

test_c( &st_x, "st_x", -45, 45, 15, 10.0 );
test_c( &st_y, "st_y", -45, 45, 15, 10.0 );

return(0);
}
#endif

/*******************************************************************
$Log: pixels.c,v $
Revision 1.16  2000/03/03 17:59:44  sue
MSC predefines type POINT; changed to GPOINT.

Revision 1.15  2000/02/09 17:56:54  sue
Code cleanup based on -pedantic -Wall -ansi.

Revision 1.14  2000/02/04 17:39:04  sue
CIS Integration.
SSC has time in true julian; will be switched to J2000 soon.

Revision 1.13  2000/01/31 19:38:43  sue
Added more system .h includes to satisfy VC5.

Revision 1.12  2000/01/18 23:01:25  sue
Static char buffer has to be forced empty before strcat();

Revision 1.11  1999/12/13 15:08:54  sue
Changed sense of H axis; now increasing to left, like skymap.

Revision 1.10  1999/12/13 15:06:53  sue
Changed grid ratio to 0.9 from 0.8; this was Ryan's real intent.
He wanted 0.8 total, which is .9*.9.  0.8 * 0.8 = 0.64, far too low.

Revision 1.9  1999/12/09 17:53:04  sue
Moved pt_list to string into main library.

Revision 1.8  1999/12/08 19:21:17  sue
Added stupid pixel to string routine for TCL.

Revision 1.7  1999/12/08 16:09:45  sue
Grid limits sb min of 800x600 if possible, not max.

Revision 1.6  1999/12/06 15:01:12  sue
Reorganized GPOINT code; defn in stars.h was misplaced.

Revision 1.5  1999/12/03 23:06:53  sue
Moderate reorg to get ready for fov_stars.

Revision 1.4  1999/12/02 20:09:17  sue
Pixels works; ifdef'ed out custom debug interface.

Revision 1.3  1999/11/30 15:51:48  sue
Forgot to update pos.ra after TM input.
Working on matrix math macros which SWIG will take.

Revision 1.2  1999/11/29 21:53:39  sue
First attempt at extra 20-pixel margins on canvas.
This may be better as a logical value, but its pixels for now.  (TBR)

Revision 1.1  1999/11/29 15:48:51  sue
Major update from work at home over Thanksgiving.
pixels.[ch] applies logical/engr. scaling to canvas.
Overhauled the "plot" interface; gss app now builds against better lib.
Couldn't finish plot due to lack of printer for code review.

Revision 1.2  1999/11/28 01:53:21  sue
Added CVS tags.

*******************************************************************/
