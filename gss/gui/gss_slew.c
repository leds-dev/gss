/* $Id: gss_slew.c,v 1.1 2000/03/01 23:32:28 sue Exp $ */

#include "gss_slew.h"

/* slew vector is normalized AFTER input */
void gss_slew_gui(
	SLEW_MODE slew_mode,
	VEC3 *slew_vector,
	double magnitude,
	int steps,
	SLEW_ACTION slew_action
	) {}

/*
$Log: gss_slew.c,v $
Revision 1.1  2000/03/01 23:32:28  sue
Interface for SLEW GUI.

*/
