#ifndef STARS_H
#define STARS_H

/* $Id: stars.h,v 1.26 2000/04/05 22:06:31 sue Exp $ */

#include <sys/time.h>

#include "common.h"
#include "mathlib.h"
#include "fov_stars.h"
#include "pixels.h"
#include "user_quat.h"

/*********************************************************************/
/* SET UP FOV PROJECTIONS.  THESE NEED EVERYTHING FROM EVERYONE. */
/* data in sky_fov is overwritten when calc_fov() is called. */

#define NUM_CORNERS 4

/* one set works for all ST's, which is why it's not in ST */
extern PT_LIST st_px_corners;		/* ST pixels - precalc for gui */

typedef struct {
	RA_DEC ra;						/* skymap in RA */
	GPOINT sky_px;					/* skymap in pixels */
	} FOV_CORNER;

/* skymap projection */
typedef FOV_CORNER SKY_FOV[NUM_CORNERS];

/**********************************************************************/

/* HV and RADEC are in mathlib. */

typedef struct {
	HV hv;					/* H/V centroid for tracked star */
	double mag;	 			/* Observed magnitude for tracked star */
	int tracked;			/* Bit indicating whether star is tracked */
	int valid;				/* Valid bit indication for tracked star */
	int tm_osc_id;			/* OSC catalog number from TM */
	HV rfov;				/* H/V centroid for RFOV window
								(-1000 = No window) */
	int cis_bss_id;			/* CIS recognized BSS index */
	int cis_osc_flag;		/* CIS recognized OSC flag */
	} VT;
typedef VT *VT_PTR;

typedef struct {
	int mode;				/* from TM */
	int power;				/* from TM */
	int status;				/* computed */
	} ST_FOV;

typedef struct st_tag {
	char num;				/* st self-identifier; range 1-3 */
	VT vt[ NUM_VTS ];		/* VT */
	POS los;				/* ST line of sight - eci and radec */
	ROTATION st2body;		/* CSTtIMU */
	ROTATION st2body_i;		/* Initial value */

	SKY_FOV skyfov;			/* ST FOV projection onto skymap */
	FOV_STARS fov_stars;	/* ASC stars in ST FOV */

	ST_FOV st_fov;			/* TM ST FOV status */
	} ST;
typedef ST *ST_PTR;

typedef double EPHEM[7];	/* SC ephemeris */

typedef struct {
	struct timeval raw;		/* UL sec, UL usec - from EPOCH */
	double engr;			/* j2000-based? */
	} GSS_TIME;

typedef struct {
	ROTATION eci2body;		/* eci to body quaternion and matrices */
	BOOL q_valid;			/* for Fake TM  */
	BOOL yaw_flip;			/* yaw flip flag, 0=upright,1=flip */
	BOOL ephem_valid;		/* TM ephemeris validity */
	EPHEM ephem;			/* TM ephemeris */
	GSS_TIME gss_time;		/* current GSS time */
	} TM_DATA;

typedef struct {
	BOOL q_valid;			/* Attitude valid flag */
	Q_SEL quat_sel;			/* Specifying quaternion selection */
	ROTATION static_eci2body; /* static eci to body quaternion and matrices */
	ST st[ NUM_STS ];		/* ST */
	ROTATION eci2body;		/* eci to body quaternion and matrices */
	TM_DATA tm;				/* non-VT tm data */
	} SIM_DATA_TYPE;
typedef SIM_DATA_TYPE *SIM_PTR;

/********************************************************************/
/*					SIMULATION DATA									*/

extern SIM_DATA_TYPE sim_data;					/* define sim_data */

/********************************************************************/
/*					INITIALIZE INTERNAL DATA						*/

void stfov_init();					/* initialize internal sim data */

/*********************************************************************/
/* helper function for TCL */

extern ST *get_ST( int i );			/* const ST ptr for SWIG px xfms */

/*********************************************************************/
/* CONVERSIONS BETWEEN SKYMAP AND ST'S */
/* see pixels.h for the canvas mappings */

void st_HVtoRA ( RA_DEC *ra2, HV *hv, ST *st );
void st_RAtoHV ( HV *hv, RA_DEC *ra2, ST *st );

/*********************************************************************/
/* update sim after tm input */

void update_st2body();				/* TM update */
void update_los();					/* TM update */

void update_st( ST *st );			/* project FOV to skymap */

void SetQuatSelAndValid (int q_sel, int q_valid);	/* For Tcl control */

/* For Tcl to pass static quat value */
char *SetStaticQuatValue (double q1, double q2, double q3, double q4);

/* Update the body2eci */
void UpdateAttitude ();

/* Get the initial value for st2body direction cosine matrix */
char *GetInitSt2BodyDCM (int st_id);

/**********************************************************************
    Change Log:
    $Log:  $

**********************************************************************/

#endif
