#ifndef ST_H
#define ST_H

/* $Id: st.h,v 1.6 2000/03/03 22:24:56 sue Exp $ */

#include "mathlib.h"

extern DIM33 sta2body[1];
extern DIM33 stx2sta[NUM_STS];

void calc_stx();
void stx_inv();

/* helper functions for SWIG */
DIM33 *dbl2dcm( double *dp );
double *dcm2dbl( DIM33 *dp );
double *get_stx2sta( int st );

/*
$Log: st.h,v $
Revision 1.6  2000/03/03 22:24:56  sue
Evil workaround for SWIG DIM33 problem; declared sta2body as a
single-element array of type DIM33.  Seems to work.

Revision 1.5  2000/03/02 17:42:40  sue
Plot.h no longer needed.
Jane found two SWIG problems; trying a fix.

Revision 1.4  2000/03/01 23:31:02  sue
Trying to call QuaternionNormalize for tm_test.

Revision 1.3  2000/02/16 23:01:38  sue
ST and DCM input works.
Fixing CIS button logic.

Revision 1.2  2000/02/14 23:13:57  sue
Added DIM33 DCM helper functions.

Revision 1.1  2000/02/14 18:54:28  sue
Support for stx2sta INI file output.

*/

#endif
