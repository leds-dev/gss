#ifndef PIXELS_H
#define PIXELS_H

/* $Id: pixels.h,v 1.14 2000/03/03 17:59:44 sue Exp $ */

#include "mathlib.h"

/* max points per projection or object */
#define NUM_PTS 20

/**********************************************************************/

typedef struct {
	int x;
	int y;
	} GPOINT;

/**********************************************************************/

typedef struct {
	GPOINT a;
	GPOINT b;
	} LINE;

typedef struct {
	GPOINT a;
	GPOINT b;
	GPOINT c;
	GPOINT d;
	} BOX;				/* not a square or rectangle */

typedef struct {
	int len;
	GPOINT points[ NUM_PTS ];
	} PT_LIST;

/**********************************************************************/
/* initialize the canvas conversions */

void c_init( int screen_width, int screen_height );

double get_grid();	/* grid size for st and skymap */

void ra_to_px( GPOINT *p, RA_DEC *ra2 );	/* skymap */
void hv_to_px( GPOINT *p, HV *hv );	/* star tracker */

char *px2str( GPOINT *p );		/* spit out string for TCL */
char *list_2_tcl( PT_LIST *l );	/* ditto */

/**********************************************************************
$Log: pixels.h,v $
Revision 1.14  2000/03/03 17:59:44  sue
MSC predefines type POINT; changed to GPOINT.

Revision 1.13  2000/03/02 17:42:39  sue
Plot.h no longer needed.
Jane found two SWIG problems; trying a fix.

Revision 1.12  2000/02/08 17:41:10  sue
Doubled number of points per list.

Revision 1.11  1999/12/09 17:53:04  sue
Moved pt_list to string into main library.

Revision 1.10  1999/12/08 19:21:18  sue
Added stupid pixel to string routine for TCL.

Revision 1.9  1999/12/07 21:23:08  sue
SWIG will take mathlib.h, except for the function decls.
Still working on ST interface.

Revision 1.7  1999/12/07 17:00:06  sue
Turned SWIG GPOINT ctor back on.

Revision 1.6  1999/12/06 15:01:13  sue
Reorganized GPOINT code; defn in stars.h was misplaced.

Revision 1.5  1999/12/03 23:06:54  sue
Moderate reorg to get ready for fov_stars.

Revision 1.4  1999/12/02 17:39:35  sue
Added GPOINT constructor indicator for SWIG.

Revision 1.3  1999/12/02 16:50:21  sue
Added vmax calculation to stfov bounding box.

Revision 1.2  1999/11/30 15:51:48  sue
Forgot to update pos.ra after TM input.
Working on matrix math macros which SWIG will take.

Revision 1.1  1999/11/29 15:48:51  sue
Major update from work at home over Thanksgiving.
pixels.[ch] applies logical/engr. scaling to canvas.
Overhauled the "plot" interface; gss app now builds against better lib.
Couldn't finish plot due to lack of printer for code review.

Revision 1.2  1999/11/28 01:53:22  sue
Added CVS tags.

**********************************************************************/

#endif
