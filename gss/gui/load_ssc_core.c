/* $Id: jliu_ssc.c,v 1.9 2000/02/09 17:56:53 sue Exp $ */
/*
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Star Catalog Processing Algorithm: ASC
John Y. Liu
09-20-99
Updated 01/31/2000.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/

#include <math.h>
#include <stdio.h>
#include <string.h>

#include "EPHGlobal.h"
#include "catalog.h"

#define ByteNum 540

/******************************************************************/

int load_ssc_core(TIME_type *time_epoch_pt, int max_lines, FILE *SSC, STAR *asc)
{
static int count = 0;


	int kBSSid     [2];
	int kASCid     [2];
	int kSkyMapNo  [2];
	int kOSCid     [2];

	int kSSCEpoch  [2];
	int kRAhour    [2];
	int kRAmin     [2];
	int kRAsec     [2];
	int kDEdegSign [2];
	int kDEdeg     [2];
	int kDEarcmin  [2];
	int kDEarcsec  [2];
	int kPropMoRA  [2];
	int kPropMoDEs [2];
	int kPropMoDE  [2];
	int kClassCode [2];
	int kSubClass  [2];
	int kParallax  [2];
	int kRadialVs  [2];
	int kRadialV   [2];
	int kInstMag   [2];
	int kVarMag    [2];
	int kIMcorST1  [2];
	int kIMcorST2  [2];
	int kIMcorST3  [2];

	int    SkyMapNo        ;
	int    RAhour          ;
	int    RAmin           ;
	double RAsec           ;
	int    DEdeg           ;
	int    DEarcmin        ;
	double DEarcsec        ;
	double PropMoRA        ;
	double PropMoDE        ;
	double VarMag          ;
	char   ClassCode       ;
	char   SubClass        ;
	double Parallax        ;
	double RadialV         ;

	int    BSSid, ASCid, OSCid, SSCEpoch;
	double RA, DE, adjusted_RA_radians, adjusted_DE_radians;
	double StarUnit[3], InstMag, IMcorST1, IMcorST2, IMcorST3;
	char   InS[ByteNum], *c, StrCp[ByteNum];
	char   DEdegSign, PropMoDEs, RadialVs;
	char   BLANK[] = "                    ";

    /* The information used for each entry (star) in the star catalog will 
       be stored as a line in the catalog; this code reads each line as a
       character string.  The string may be broken down into sub-strings
       each one containing some piece of relevant information.  The "k" 
       vectors give the starting position and the length of the associated
       sub-string.  E.g., kClassCode [0] gives the starting position of the
       star class in the string, while kClassCode [1] gives the length of
       character string representing star class.  */

	kSkyMapNo  [0] = 1                                ; kSkyMapNo  [1] = 8;

 	kBSSid     [0] = kSkyMapNo  [0] + kSkyMapNo  [1]+1; kBSSid     [1] = 6;
 	kASCid     [0] = kBSSid     [0] + kBSSid     [1]+1; kASCid     [1] = 6;
    kOSCid     [0] = kASCid     [0] + kASCid     [1]+1; kOSCid     [1] = 6;

	kSSCEpoch  [0] = kOSCid     [0] + kOSCid     [1]+1; kSSCEpoch  [1] = 7;
	kRAhour    [0] = kSSCEpoch  [0] + kSSCEpoch  [1]+1; kRAhour    [1] = 2;
	kRAmin     [0] = kRAhour    [0] + kRAhour    [1]+1; kRAmin     [1] = 2;
	kRAsec     [0] = kRAmin     [0] + kRAmin     [1]+1; kRAsec     [1] = 7;
	kDEdegSign [0] = kRAsec     [0] + kRAsec     [1]+1; kDEdegSign [1] = 1;
	kDEdeg     [0] = kDEdegSign [0] + kDEdegSign [1]+0; kDEdeg     [1] = 2;
	kDEarcmin  [0] = kDEdeg     [0] + kDEdeg     [1]+1; kDEarcmin  [1] = 2;
	kDEarcsec  [0] = kDEarcmin  [0] + kDEarcmin  [1]+1; kDEarcsec  [1] = 6;
	kPropMoRA  [0] = kDEarcsec  [0] + kDEarcsec  [1]+1; kPropMoRA  [1] = 8;
	kPropMoDEs [0] = kPropMoRA  [0] + kPropMoRA  [1]+1; kPropMoDEs [1] = 1;
	kPropMoDE  [0] = kPropMoDEs [0] + kPropMoDEs [1]+0; kPropMoDE  [1] = 7;
	kClassCode [0] = kPropMoDE  [0] + kPropMoDE  [1]+1; kClassCode [1] = 1;
	kSubClass  [0] = kClassCode [0] + kClassCode [1]+1; kSubClass  [1] = 1;
	kParallax  [0] = kSubClass  [0] + kSubClass  [1]+1; kParallax  [1] = 8;
	kRadialVs  [0] = kParallax  [0] + kParallax  [1]+1; kRadialVs  [1] = 1;
	kRadialV   [0] = kRadialVs  [0] + kRadialVs  [1]+0; kRadialV   [1] = 5;
	kInstMag   [0] = kRadialV   [0] + kRadialV   [1]+1; kInstMag   [1] = 7;
	kVarMag    [0] = kInstMag   [0] + kInstMag   [1]+1; kVarMag    [1] = 5;
	kIMcorST1  [0] = kVarMag    [0] + kVarMag    [1]+1; kIMcorST1  [1] = 7;
	kIMcorST2  [0] = kIMcorST1  [0] + kIMcorST1  [1]+1; kIMcorST2  [1] = 7;
	kIMcorST3  [0] = kIMcorST2  [0] + kIMcorST2  [1]+1; kIMcorST3  [1] = 7;

    /* continue reading from file while there are more lines */
	while(((c = fgets(InS, ByteNum, SSC)) != NULL))
	{
		/* skip blanks and comments */
		if (( *c == '\n' ) || (*c == '#')) continue;

		if (count >= max_lines) {
			puts( "ASC limit exceeded." );
			exit(2000);
			}

	    /* convert the sub-strings representing information about the current
           entry into numerical data */

 		sscanf(InS-1+kASCid     [0],"%6c",StrCp); StrCp[kASCid     [1]] = '\0';
 		if (strncmp(BLANK,StrCp,kASCid       [1]) != 0)
 			sscanf(StrCp,"%d" ,&ASCid       );
 		else
 			ASCid = 0;

 		if (ASCid == 0) continue;		/* NOT IN ASC */

		sscanf(InS-1+kBSSid     [0],"%6c",StrCp); StrCp[kBSSid     [1]] = '\0';
 		if (strncmp(BLANK,StrCp,kBSSid       [1]) != 0)
 			sscanf(StrCp,"%d" ,&BSSid       );
 		else
 			BSSid     = 0;
 
		sscanf(InS-1+kSkyMapNo  [0],"%8c",StrCp); StrCp[kSkyMapNo  [1]] = '\0';
		if (strncmp(BLANK,StrCp,kSkyMapNo    [1]) != 0)
			sscanf(StrCp,"%d" ,&SkyMapNo    );
		else
			SkyMapNo  = 0;

		sscanf(InS-1+kOSCid     [0],"%6c",StrCp); StrCp[kOSCid     [1]] = '\0';
		if (strncmp(BLANK,StrCp,kOSCid       [1]) != 0)
			sscanf(StrCp,"%d" ,&OSCid       );
		else
			OSCid     = 0;

		sscanf(InS-1+kSSCEpoch  [0],"%7c",StrCp); StrCp[kSSCEpoch  [1]] = '\0';
		if (strncmp(BLANK,StrCp,kSSCEpoch    [1]) != 0)
			sscanf(StrCp,"%d" ,&SSCEpoch    );
		else
			SSCEpoch  = 0;

		sscanf(InS-1+kRAhour    [0],"%2c",StrCp); StrCp[kRAhour    [1]] = '\0';
		if (strncmp(BLANK,StrCp,kRAhour      [1]) != 0)
			sscanf(StrCp,"%d" ,&RAhour      );
		else
			RAhour    = 0;

		sscanf(InS-1+kRAmin     [0],"%2c",StrCp); StrCp[kRAmin     [1]] = '\0';
		if (strncmp(BLANK, StrCp, kRAmin     [1]) != 0)
			sscanf(StrCp,"%d" ,&RAmin       );
        else
			RAmin     = 0;

		sscanf(InS-1+kRAsec     [0],"%7c",StrCp); StrCp[kRAsec     [1]] = '\0';
		if (strncmp(BLANK, StrCp, kRAsec     [1]) != 0)
			sscanf(StrCp,"%lf",&RAsec       );
		else
			RAsec     = 0.0;

		sscanf(InS-1+kDEdegSign [0],"%1c",StrCp); StrCp[kDEdegSign [1]] = '\0';
		if (strncmp(BLANK, StrCp, kDEdegSign [1]) != 0)
			sscanf(StrCp,"%c" ,&DEdegSign   );
		else
			DEdegSign = '+';

		sscanf(InS-1+kDEdeg     [0],"%2c",StrCp); StrCp[kDEdeg     [1]] = '\0';
		if (strncmp(BLANK, StrCp, kDEdeg     [1]) != 0)
			sscanf(StrCp,"%d" ,&DEdeg       );
		else
			DEdeg     = 0;

		sscanf(InS-1+kDEarcmin  [0],"%2c",StrCp); StrCp[kDEarcmin  [1]] = '\0';
		if (strncmp(BLANK, StrCp, kDEarcmin  [1]) != 0)
			sscanf(StrCp,"%d" ,&DEarcmin    );
		else
			DEarcmin  = 0;

		sscanf(InS-1+kDEarcsec  [0],"%6c",StrCp); StrCp[kDEarcsec  [1]] = '\0';
		if (strncmp(BLANK, StrCp, kDEarcsec  [1]) != 0)
			sscanf(StrCp,"%lf",&DEarcsec    );
		else
			DEarcsec  = 0.0;

		sscanf(InS-1+kPropMoRA  [0],"%8c",StrCp); StrCp[kPropMoRA  [1]] = '\0';
		if (strncmp(BLANK, StrCp, kPropMoRA  [1]) != 0)
			sscanf(StrCp,"%lf",&PropMoRA    );
		else
			PropMoRA  = 0.0;

		sscanf(InS-1+kPropMoDEs [0],"%1c",StrCp); StrCp[kPropMoDEs [1]] = '\0';
		if (strncmp(BLANK, StrCp, kPropMoDEs [1]) != 0)
			sscanf(StrCp,"%c" ,&PropMoDEs   );
		else
			PropMoDEs = '+';

		sscanf(InS-1+kPropMoDE  [0],"%7c",StrCp); StrCp[kPropMoDE  [1]] = '\0';
		if (strncmp(BLANK, StrCp, kPropMoDE  [1]) != 0)
			sscanf(StrCp,"%lf",&PropMoDE    );
		else
			PropMoDE  = 0.0;

		sscanf(InS-1+kClassCode [0],"%1c",StrCp); StrCp[kClassCode [1]] = '\0';
		if (strncmp(BLANK, StrCp, kClassCode [1]) != 0)
			sscanf(StrCp,"%c" ,&ClassCode   );
		else
			ClassCode = ' ';

		sscanf(InS-1+kSubClass  [0],"%1c",StrCp); StrCp[kSubClass  [1]] = '\0';
		if (strncmp(BLANK, StrCp, kSubClass  [1]) != 0)
			sscanf(StrCp,"%c" ,&SubClass    );
		else
			SubClass  = ' ';

		sscanf(InS-1+kParallax  [0],"%8c",StrCp); StrCp[kParallax  [1]] = '\0';
		if (strncmp(BLANK, StrCp, kParallax  [1]) != 0)
			sscanf(StrCp,"%lf",&Parallax    );
		else
			Parallax  = 0.0;

		sscanf(InS-1+kRadialVs  [0],"%1c",StrCp); StrCp[kRadialVs  [1]] = '\0';
		if (strncmp(BLANK, StrCp, kRadialVs  [1]) != 0)
			sscanf(StrCp,"%c" ,&RadialVs    );
		else
			RadialVs  = '+';

		sscanf(InS-1+kRadialV   [0],"%5c",StrCp); StrCp[kRadialV   [1]] = '\0';
		if (strncmp(BLANK, StrCp, kRadialV   [1]) != 0)
			sscanf(StrCp,"%lf",&RadialV     );
		else
			RadialV   = 0.0;

		sscanf(InS-1+kInstMag   [0],"%7c",StrCp); StrCp[kInstMag   [1]] = '\0';
		if (strncmp(BLANK, StrCp, kInstMag   [1]) != 0)
			sscanf(StrCp,"%lf",&InstMag     );
		else
			InstMag   = 0.0;

		sscanf(InS-1+kVarMag    [0],"%5c",StrCp); StrCp[kVarMag    [1]] = '\0';
		if (strncmp(BLANK, StrCp, kVarMag    [1]) != 0)
			sscanf(StrCp,"%lf",&VarMag      );
		else
			VarMag    = 0.0;

		sscanf(InS-1+kIMcorST1  [0],"%7c",StrCp); StrCp[kIMcorST1  [1]] = '\0';
		if (strncmp(BLANK, StrCp, kIMcorST1  [1]) != 0)
			sscanf(StrCp,"%lf",&IMcorST1    );
		else
			IMcorST1  = 0.0;

		sscanf(InS-1+kIMcorST2  [0],"%7c",StrCp); StrCp[kIMcorST2  [1]] = '\0';
		if (strncmp(BLANK, StrCp, kIMcorST2  [1]) != 0)
			sscanf(StrCp,"%lf",&IMcorST2    );
		else
			IMcorST2  = 0.0;

		sscanf(InS-1+kIMcorST3  [0],"%7c",StrCp); StrCp[kIMcorST3  [1]] = '\0';
		if (strncmp(BLANK, StrCp, kIMcorST3  [1]) != 0)
			sscanf(StrCp,"%lf",&IMcorST3    );
		else
			IMcorST3  = 0.0;

		/* PostProcessing*/

		RA = 15.0 * (RAhour + RAmin / 60.0 + RAsec / 3600.0);
		DE =		DEdeg + DEarcmin / 60.0 + DEarcsec / 3600.0;
		if (DEdegSign == '-')
			DE = -DE;
		if (PropMoDEs == '-')
			PropMoDE = -PropMoDE;
		if (RadialVs  == '-')
			RadialV  = -RadialV;

		/* make proper motion, parallax, and radial_v corrections 
		   to RA/DE, to adjust for the difference between SSC and ASC
           epoch. */

		slaPm (RA * DTR, /* Right ascension from SSC (radians) */
		       DE * DTR, /* Declination from SSC (radians) */
		       PropMoRA/240 * DTR,	/* Change in right ascension, 
						   			   per unit time,
						   			   (radians/year) */
		       PropMoDE/3600 * DTR,	/* Change in declination, 
						   			   per unit time,
						   			   (radians/year) */
		       Parallax, /* Parallax (arc-seconds) */
		       RadialV,	 /* Radial velocity (km/s, '+' if receding) */
		       SSCEpoch + 2000.0,	/* Time at which SSC data applies 
							   		   (years after J2000) */
		       time_epoch_pt->julian_epoch,		/* Time at which
							   			       ASC data applies 
							   			       (years after J2000) */
		       &adjusted_RA_radians,	/* Right ascension suitable
						   				   for ASC (radians) */
		       &adjusted_DE_radians);	/* Declination suitable for 
						   				   ASC (radians) */

		/* update ASC entry */
		asc->sky2000      = SkyMapNo    	;
		asc->asc_index    = ASCid       	;
		asc->bss_id       = BSSid       	;
		asc->osc_id       = OSCid       	;
/*		asc->epoch        = SSCEpoch    	; */
		asc->epoch        = time_epoch_pt->gss  ;
	asc->pos.ra.ra    = adjusted_RA_radians * RTD   ;
        asc->pos.ra.dec   = adjusted_DE_radians * RTD	;
		ra2eci (&asc->pos.eci, &asc->pos.ra)	;
		asc->mag[0]       = InstMag     	;
		asc->mag[1]       = IMcorST1    	;
		asc->mag[2]       = IMcorST2    	;
		asc->mag[3]       = IMcorST3    	;
/* The star magnitude variability read from SSC is a pk-pk value, whereas the 
      star class magnitude variability in the INI file is a 0-pk value. To be
      consistent, only half of the star magnitude variability read from SSC 
      should be used. */
		asc->resid        = 0.5*VarMag      	;

		ssc_2_asc ( asc, ClassCode );

		++asc;
		++count;
/*		if ((count %100) ==0) printf( "ASC Entry %d\n", count ); */
	}

/* printf( "SSC Entries %d\n", count ); */
return( count );
}

/*******************************************************************/

#ifdef STAND_ALONE
int main()
{
static char ssc_file[] = "../tcl/SSC.dat";
int stars = 15000;
STAR *asc = (STAR *) calloc( stars, sizeof( STAR ));
FILE *fp = fopen( ssc_file, "r" );
int lines;

if (!fp) { printf( "Can't find file %s\n", ssc_file ); exit(99); }

lines = load_ssc_jliu( stars, fp, asc );
printf( "jliu read %d stars from %s\n", lines, ssc_file );
return(0);
}

/*******************************************************************/
/*
$Log: jliu_ssc.c,v $
Revision 1.10 2003/01/12 09:46:00 Toan Nguyen
Modified code to read star magnitude variability from SSC as a 0-pk value.

Revision 1.9  2000/02/09 17:56:53  sue
Code cleanup based on -pedantic -Wall -ansi.

Revision 1.8  2000/02/07 22:56:57  sue
Code updates for header changes from Jane last week.
cis buffer now global.
ASC does not need class or subclass.
Renamed asc_id to asc_index for clarity.

Revision 1.7  2000/02/04 17:39:04  sue
CIS Integration.
SSC has time in true julian; will be switched to J2000 soon.

Revision 1.6  2000/02/01 23:48:02  sue
Adding common.h to simpify later integration with CIS and GTACS.

Revision 1.5  2000/01/31 19:38:43  sue
Added more system .h includes to satisfy VC5.

Revision 1.4  1999/12/07 21:43:03  sue
Commented out JLiu debug printing, since it seems to load.
Skymap looks weird...

Revision 1.3  1999/12/07 16:51:31  sue
Catalog doesn't core dump in TCL.  No idea if data is right.

Revision 1.2  1999/12/07 15:43:16  sue
Added stand-alone target for testing.
Added support for blank lines and comments in SSC file.
Needed to allow for CVS version ID in SSC file.

Revision 1.1  1999/12/06 23:34:26  sue
Added J. Liu's SSC/ASC code.  I can't parse his SSC file or figure out
his code, so I'm just putting a wrapper around it.

*/

#endif
