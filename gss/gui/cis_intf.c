/* $Id: cis_intf.c,v 1.35 2000/05/22 23:54:36 jzhao Exp $ */

/* memcpy() */
#include <string.h>

#include "cis_intf.h"
#include "stars.h"

/********************************************************************/
/********************************************************************/
/* integrating the CIS Library */
/* SOME OF THE CIS INTERFACE DATA IS HIDDEN FROM SWIG/TCL. */

#include "TclInterface.h"

/********************************************************************/
/********************************************************************/
/* CIS SNAPSHOT BUFFER - treat as readonly. */
/* written to by cis_snapshot(), but MUST BE SYNCHRONIZED */
/* by GTACS TM update.  DO NOT INVOKE CIS_SNAPSHOT() DIRECTLY. */

SIM_DATA_TYPE snapshot;

RA_DEC sim_sunpos = { 0.0, 0.0 };	/* fake sun pos */

/**************************************************************/
/* pass CIS-unique data in from TCL ini file reader. */

CIS_INI_VARS cis_ini = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

int cis_ret = 0;		/* CIS sun/rough return value */

CIS_USER cis_user = { {0} };
CIS_OUT cis_data = { "", {0.0} };

/**************************************************************/
/**************************************************************/
/* get VT pointer in snapshot buffer */
/* range i = 1-15, NOT 0-14. */

static VT *vt_ptr( int i )
{
int vtnum = (i-1) % NUM_VTS;
int stnum = (i-vtnum) / NUM_VTS;

VT *vt = &snapshot.st[stnum].vt[vtnum];
return( vt );
}

/**************************************************************/
/* return VT visible = tracking and enabled. */
/* n runs from 1-15, not 0-14. */

int cis_vt_vis( int n )
{
VT_PTR vt = vt_ptr( n );
return( vt->tracked & vt->valid );
}

/**************************************************************/
/* set user VT selection */
/* i runs from 1-15, not 0-14. */

void cis_vt_sel( int i, int val )
{
int vtnum = (i-1) % NUM_VTS;
int stnum = (i-vtnum) / NUM_VTS;

cis_user[stnum][vtnum] = val;
}

/**************************************************************/
/* make sure user selections are logically valid. */

static void update_user()
{
int i,j,v;
for (i = 0; i < NUM_STS; ++i )
	{
	for (j = 0; j < NUM_VTS; ++j )
		{
		v = cis_vt_vis( (i * NUM_VTS) + j + 1 );
		cis_user[i][j] &= v;
		}
	}
}

/**************************************************************/
/* save tm to snapshot buffer.  relatively slow memcpy. */

extern void cis_snapshot()
{
static char msg[] = "\n\nCIS Snapshot\n";
strcpy( &cis_data.msg[0], &msg[0] );
memcpy( &snapshot, &sim_data, sizeof( SIM_DATA_TYPE ) );
update_user();
}

/**************************************************************/

char *cis_result() { return( cis_data.msg ); }

/**************************************************************/
/* return VT info for GUI display. */
/* i range = 1-15 */

char *cis_vtinfo( int i )
{
/*
static char fmt[] = "H: %4.2lf V: %4.2lf Mag: %4.2lf Valid: %d Track: %d\
TM_ID: %05d CIS_ID: %05d In_OSC: %d";
*/
static char fmt[] = "%4.2lf\t%4.2lf\t%4.2lf\t%3d\t%3d\t%05d\t%05d\t%d";

static char buf[50];

VT *vt = vt_ptr( i );
sprintf( buf, fmt,
	vt->hv.h, vt->hv.v, vt->mag, vt->valid, vt->tracked,
	vt->tm_osc_id, vt->cis_bss_id, vt->cis_osc_flag
	);
return &buf[0];
}

/***************************************************
$Log: $
***************************************************/
