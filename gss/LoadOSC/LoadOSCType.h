/* ========================================================================
	FILE NAME:
	LoadOSCType.h

	DESCRIPTION:
	This is a header file of LoadOSC.c.

 ========================================================================== */

#ifndef _LOADOSCTYPE_H
#define _LOADOSCTYPE_H

/* $Id: $ */

/* ------------------------
	Headers
*/
/* -----------------------
	Macros
*/

/* Global Variables */

/* Type define */

	typedef struct
	{
		int MAX_EDIT_AREA_STARS;
		int MAX_OSC_STARS;
		int StarAreaStartAddrState;
		int StarAreaStartAddr;
		int CatAreaStartAddrState;
		int	CatAreaStartAddr;
		int EditAreaStartAddrState;
		int EditAreaStartAddr;
		int DecStepStartAddrState;
		int DecStepStartAddr;
	}IniUpLoad_type;

/*  Function Prototypes */

/*************************************************************************************

	Change Log:
	$Log: $

**************************************************************************************/

#endif /*_LOADOSCTYPE_H */
