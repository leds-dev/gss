/* $Id: dbp_f2h.c,v 1.2 2000/02/24 18:38:46 jzhao Exp $ */
/***********************************************************************

File : dbp_f2h.c

Created by: Lisa Houle      original program (f2h.c)
            Mary Bruskotter "main" converted to routine and names
                                localized for dbprep interface

Summary : This program will accept a floating point number,
          input from the command line, 
      and output a 1750a hex format for that number.

    06-Aug-1999 - modified to use algorithm taken from Linh Ly's
                  hexit.c derived from Steve Wong's tool

************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <math.h>


/*************************************************************************
*
*   convert_single_float_to_hex
*
*       Convert a string representing a single precision floating point
*       value to the 1750a hex format for that number.
*/

void
convert_single_float_to_hex ( float_string, hex_string )

char *float_string, *hex_string;
{
  double flt_num, mantissa, hex_man;
  int    exponent;

        sscanf (float_string, "%lf", &flt_num);
        mantissa = frexp (flt_num, &exponent);
        /* a 24-bit float x will be represented as an integer (int)(x*(2**23))*/
        /* For example: 0.75 is represented as 600000(hex)                    */
        /*             -0.75 is represented as A00000(hex)                    */

        /* frexp splits a floating-point number flt_num into mantissa(double) */
        /* and exponent(integer) so that flt_num = x*(2**n), where the        */
        /* mantissa x is in the  range 0.5 <= |x| < 1.0                       */
        /* and since all floating point are assumed normalized meaning the    */
        /* sign bit of the mantissa and its next bit have to have opposite    */
        /* value, so the first 2 bits of the mantissa have to be 01 or 10     */
        /* Any value of x (0.5 <= |x| < 1.0) is okay, except when x = -0.5    */
        /* 0.5 is represented in 24 bits as 0100 0000 0000 0000 0000 0000,and */
        /*-0.5 is represented in 24 bits as 1100 0000 0000 0000 0000 0000,    */
        /* which is not normalized. For any other value of x(x != -0.5), |x|  */
        /* is always represented by a 24-bit string that starts with 01 and at*/
        /* least one more bit 1 within the last 22 bits of that 24-bit string.*/
        /* Therefore, if x is positive, it is normalized, and if x is         */
        /* negative, after we flip all the bits and add 1, the resulting      */
        /* string that represents x now is still normalized.                  */
        /* Since the only value of x (0.5 <= x < 1) of -0.5 can not be        */
        /* represented by a normallized 24-bit string, we need to change      */
        /* flt_num=-0.5*(2**n) to -1.0*(2**(n-1))                             */
        /* note: a double is 64 bits and an integer is 32 bits                */

        if (mantissa == -0.5) {
           hex_man = 0x800000; 
           exponent -= 1;
        }
        else if (mantissa >= 0.0) {
           hex_man = floor (mantissa / pow(2.0, -23.0) + 0.5);
           if ((int)hex_man == 0x800000) {
              hex_man /= 2.0;
              exponent += 1;
           }
        }
        else
            hex_man = ceil  (mantissa / pow(2.0, -23.0) - 0.5);

        /* the first 16 bits of hex_man */
        /* the last 8 bits of hex_man and 8 bits of exponent */

        sprintf ( hex_string, "%04x%02x%02x",
        (int)hex_man >> 8 & 0x0000FFFF,
        (int)hex_man & 0x000000FF, exponent & 0x000000FF);

/*
        printf ("\nHex representation of %30s is ", flt_str);
*/
      /* the first 16 bits of hex_man */
/*
        printf ("%04X ", (int)hex_man >> 8 & 0x0000FFFF);
*/
      /* the last 8 bits of hex_man and 8 bits of exponent */
/*
        printf ("%02X%02X\n", (int)hex_man & 0x000000FF, exponent & 0x000000FF);
*/
}     /* end convert_single_float_to_hex */

/*************************************************************************************

	Change Log:
	$Log: dbp_f2h.c,v $
	Revision 1.2  2000/02/24 18:38:46  jzhao
	Modified for port to NT
	
	Revision 1.1  2000/02/10 21:52:29  jzhao
	Initial Revision.
	

**************************************************************************************/
