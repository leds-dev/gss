/* $Id: Test.c,v 1.5 2000/03/28 22:54:28 jzhao Exp $ */
/* ========================================================================
	FILE NAME:
		Test.c

	DESCRIPTION:
		This is test Driver of CISLib.

	FUNCTION LIST:


 ========================================================================== */

/* System Headers */

#include <stdio.h>
#include <stdlib.h>

/* Self Defined Header */

#include <StarCatalog.h>
#include <LoadOSCType.h>
#include <LoadOSC.h>

FILE *LogFp;
/* ========================================================================== */


void main() 
{
	char UpLoadFileName[] = {"testDec96"};
	char ASCFileName[] = {"../Test/ASC.dat"};
	char Epoch[] = {"xxxxxxxxx"};

	AcqSC_type AcqCat;

	StarASC_type *ASC;
	int NumASC;

	IniUpLoad_type IniVaris;
	double DecStepSize;
	int UpLoadType;
	int NumSpares[360];
	float LowDec = 0.0;
    int TM_FOR_MST_FLAG = TRUE;

	int i;
	int err_state=0;
/*
	UpLoadType = LOAD_FULL_OSC;
*/
	UpLoadType = LOAD_DEC_STRIP;
	LowDec = 5.7;
	
	IniVaris.MAX_EDIT_AREA_STARS = 115;
	IniVaris.MAX_OSC_STARS = 6553;
	IniVaris.DecStepStartAddrState = 10;
	IniVaris.DecStepStartAddr = 10;
	IniVaris.StarAreaStartAddrState = 0;
	IniVaris.StarAreaStartAddr = 0;
	IniVaris.CatAreaStartAddrState = 1;            
	IniVaris.CatAreaStartAddr = 1;
	IniVaris.EditAreaStartAddrState = 2;
	IniVaris.EditAreaStartAddr = 0xbf63 ;


	DecStepSize = 1.0; /* Get from GUI? */

	for (i=0; i<360; i++)
	{
		if ( (i % 2) == 0 ) NumSpares[i] = 2;
		else 				NumSpares[i] = 1;
	}

	ASC = (StarASC_type *)calloc(MAXNUM_ASC_STARS, sizeof(StarASC_type) );
	{
		if ( ASC == NULL )
		{
			printf("Memory Error");
			goto Quit;
		}
	}

	/* ----------------------------------------------
		Read in ASC
	------------------------------------------------- */
	NumASC = Read_StarCatalog(ASCFileName, ASC, ASC_TYPE);

    /* ---------------------------
	  Create CIS ASC pointer list
  	------------------------------ */
  	AcqCat.NumStars = NumASC;

	for (i=0; i<NumASC; i++)
		AcqCat.Stars[i] = &ASC[i];

	/* --------------------------------------------
		Create UpLoad File
	----------------------------------------------- */
	err_state = CreateUpLoadFile(UpLoadFileName, ASCFileName, Epoch, 
								 &AcqCat, &IniVaris, NumSpares, UpLoadType, 
								 DecStepSize, LowDec, TM_FOR_MST_FLAG);
	if (err_state < 0)
		fprintf(stderr, "LoadOSC: LoadFile Error\n");

Quit:
	free(ASC);
}

/*************************************************************************************

	Change Log:
	$Log: Test.c,v $
	Revision 1.5  2000/03/28 22:54:28  jzhao
	Changed format of IniUpLoad_type via Sue's request.
	
	Revision 1.4  2000/02/23 17:31:35  jzhao
	Modified PROC format.
	
	Revision 1.3  2000/02/19 01:19:26  jzhao
	Modified upload Procs format.
	
	Revision 1.2  2000/02/11 22:07:43  jzhao
	Modified to match TM State lables.
	Removed StartAddr Variables from IniUpLoad_type.
	
	Revision 1.1  2000/02/10 21:52:28  jzhao
	Initial Revision.
	

**************************************************************************************/
