/* $Id: LoadOSC.c,v 1.8 2000/05/19 20:37:47 jzhao Exp $ */

/* ========================================================================
	FILE NAME:
		LoadOSC.c

	DESCRIPTION:
		Creates On Board Star Catalog(OSC) from Acquisition Star Catalog(ASC),
		generates a PROC for OSC uploading, and generates associated memory
		dump comparison files.

	FUNCTION LIST:

============================================================================ */

/* System Headers */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

/* Self Defined Headers */

#include <StarCatalog.h>
#include <LoadOSCType.h>
#include <LoadOSC.h>

#define NUM_WORDS_IN_STAR_RECORD  10
#define NUM_WORDS_IN_CATALOG_RECORD  3
#define MAXNUM_OSC_STARS	6553

/* Type define and Global variables */

	typedef struct
	{
		unsigned 			BSS_ID;
		double				Magnitude;
		int 				FixedPtMagnitude;
		int					IntDeclination;
		double				RightAscension;
		double				ECI[3];
	}StarOSC_type;

	typedef struct
	{
		int 				NumStars;
		StarOSC_type		Stars[MAXNUM_OSC_STARS];	
	}OSC_type;

	typedef struct
	{
		int		OSCIndx;
		int		StarListIndx;
		int		NumStars;
		int 	ListSize;
	}CatEntry_type;

	char LoadStarFileName[]={"LoadStar.tmp"};
	char LoadCatFileName[]={"LoadCat.tmp"};
    char LoadAdaStarFileName[] = {"LoadAdaStar.tmp"};
	char LoadAdaCatFileName[]={"LoadAdaCat.tmp"};
	FILE *FpLoadStar;
	FILE *FpLoadAdaStar;
	FILE *FpLoadCatalog;
    FILE *FpLoadAdaCatalog;
	FILE *FpStarMemDump;
	FILE *FpCatStrucMemDump;

	int TM_FOR_MST = FALSE; 
	int NumOSCSpares;
    static int oneDecOnly=0;
    int stripNum;
    double lowEnd;
    double upperEnd;

/* ========================================================================== */

/* --------------------------------------------
DecCompareFncOSC: Compare declination(OSC) for qsort.
*/

int DecCompareFncOSC(const void *p1, const void *p2)
{
	StarOSC_type *a, *b;
	a = (StarOSC_type *)p1;
	b = (StarOSC_type *)p2;

	if (a->IntDeclination < b->IntDeclination) return(-1);
	if (a->IntDeclination > b->IntDeclination) return(1);
	else return(0);
}

/* --------------------------------------------
RAscenCompareFncOSC: Compare Right Ascension(OSC) for qsort.
*/

int RAscenCompareFncOSC(const void *p1, const void *p2)
{
	StarOSC_type *a, *b;
	a = (StarOSC_type *)p1;
	b = (StarOSC_type *)p2;

	if (a->RightAscension < b->RightAscension) return(-1);
	if (a->RightAscension > b->RightAscension) return(1);
	else return(0);
}

/* --------------------------------------------
UpdatedCheckSum:
*/

unsigned long UpdateCheckSum(char *HexString, unsigned long InCheckSum)
{
	unsigned long NextValue;

	sscanf( HexString, "%x", &NextValue);
	return( InCheckSum+NextValue );
}

/* -------------------------------------------
GetSpareStarRecord
*/
void GetSpareStarRecord(StarOSC_type *SpareStar)
{
	SpareStar->BSS_ID = 0;
	SpareStar->Magnitude = 0.0;
	SpareStar->FixedPtMagnitude = 0;
	SpareStar->IntDeclination = 0;
	SpareStar->RightAscension = 0.0;
	SpareStar->ECI[0] = 0.0;
	SpareStar->ECI[1] = 0.0;
	SpareStar->ECI[2] = 0.0;
}

/* -------------------------------------------
OpenFile:
*/

FILE *OpenFile( char *FileName, char *Mode )
{
	FILE *fp;

	if ( ( fp = fopen( FileName, Mode ) ) == NULL )
	{
		fprintf( stderr, "LoadOSC: Can't open file ::%s:: Mode %s/n",
					FileName, Mode );
		return(NULL);
	}
	else
		return( fp );
}

/* ---------------------------------------
AppendFile: Append 2nd file to 1st file. 
*/

void AppendFile(FILE *Fp1, FILE *Fp2)
{
	char buf[400];

	while ( fgets( buf, 400, Fp2 ) != NULL )
			fputs( buf, Fp1); 
}

/* -------------------------------------------

CreateOSC: Converts Magnitude to integer(Fixed point 0.0625); 
		   Converts Declination to integer Declination index;
		   Sort OSC by Declination.
*/

int CreateOSC(AcqSC_type *ASC, OSC_type *OSC, IniUpLoad_type *IniVars, 
			  double DecStepSize) 
{
	int err_stat=0;
	int ASC_indx;
	int OSC_indx=0;
	FILE *fp;

	for ( ASC_indx=0; ASC_indx<ASC->NumStars; ASC_indx++)
	{
		if (ASC->Stars[ASC_indx]->OSC_ID != 0)
		{
			OSC->Stars[OSC_indx].BSS_ID = ASC->Stars[ASC_indx]->BSS_ID;
			OSC->Stars[OSC_indx].Magnitude = ASC->Stars[ASC_indx]->Magnitude;
			OSC->Stars[OSC_indx].FixedPtMagnitude = 
				 (int)( (ASC->Stars[ASC_indx]->Magnitude / 0.0625)+0.5 );	
			OSC->Stars[OSC_indx].IntDeclination =
				 (int)( (ASC->Stars[ASC_indx]->Declina+90.0) / DecStepSize );	
			OSC->Stars[OSC_indx].RightAscension = ASC->Stars[ASC_indx]->RAscen;
			OSC->Stars[OSC_indx].ECI[0] = ASC->Stars[ASC_indx]->ECI[0];
			OSC->Stars[OSC_indx].ECI[1] = ASC->Stars[ASC_indx]->ECI[1];
			OSC->Stars[OSC_indx].ECI[2] = ASC->Stars[ASC_indx]->ECI[2];

			OSC_indx++;
		}
	}
	OSC->NumStars = OSC_indx;

	/* Sort by Declination */
	qsort(OSC->Stars, OSC->NumStars, sizeof(StarOSC_type), DecCompareFncOSC);

	/* Write out OSC */
	if ((fp = OpenFile("OSC.dat", "w")) == NULL)
	{
		err_stat = -1;
		return(err_stat);
	}
	for (OSC_indx=0; OSC_indx<OSC->NumStars; OSC_indx++)
	{
		fprintf(fp, "%10d %12.6f %10d %10d %12.6f %12.6f %12.6f %12.6f\n",
				OSC->Stars[OSC_indx].BSS_ID,
				OSC->Stars[OSC_indx].Magnitude,
				OSC->Stars[OSC_indx].FixedPtMagnitude,
				OSC->Stars[OSC_indx].IntDeclination,
				OSC->Stars[OSC_indx].RightAscension,
				OSC->Stars[OSC_indx].ECI[0],
				OSC->Stars[OSC_indx].ECI[1],
				OSC->Stars[OSC_indx].ECI[2]); 
	}
	fclose(fp);

	return(err_stat);
}

/* ----------------------------------
CreateDecStripEntry: 	The index of the Entry will be the Declination strip 
						index, and each entry will contain starting index of
						the strip in OSC, starting index of the strip in star
						load list(include spare), number of stars in the
						strip, size of the star list(number of stars+spares).
*/

int CreateDecStripEntry(OSC_type 		*OSC, 
						 IniUpLoad_type *IniVaris, 
						 int			AllocStripSize[],
						 double			DecStepSize,
						 CatEntry_type  DecStripEntry[])
{
	int err_stat = 0;
	int Star_indx;
	int MaxNumDecStrip;
	int StarCount=0;
	int	DecStrip_indx=0; 
	int StripStart_indx_OSC=0;
	int StripStart_indx_Cat=1;

	FILE *fp;

	NumOSCSpares = 0;
	MaxNumDecStrip = (int)(180.0 / DecStepSize);	

	for (Star_indx=0; Star_indx<OSC->NumStars; Star_indx++)
	{
		while (DecStrip_indx < OSC->Stars[Star_indx].IntDeclination)
		{
			DecStripEntry[DecStrip_indx].OSCIndx = StripStart_indx_OSC;
			DecStripEntry[DecStrip_indx].NumStars = StarCount;
			DecStripEntry[DecStrip_indx].StarListIndx = StripStart_indx_Cat;
			DecStripEntry[DecStrip_indx].ListSize = AllocStripSize[DecStrip_indx];

			if( (DecStripEntry[DecStrip_indx].ListSize > 
					IniVaris->MAX_EDIT_AREA_STARS) ||
				(DecStripEntry[DecStrip_indx].NumStars >
					IniVaris->MAX_EDIT_AREA_STARS) || 
				(DecStripEntry[DecStrip_indx].NumStars >
					DecStripEntry[DecStrip_indx].ListSize) )
			{
				fprintf(stderr, 
					    "Too Many Stars in DecStrip %d\n", DecStrip_indx-1);
				fprintf(stderr, "NumStars = %d, ListSize = %d, Max Edit Area = %d\n",
						DecStripEntry[DecStrip_indx].NumStars,
						DecStripEntry[DecStrip_indx].ListSize,
						IniVaris->MAX_EDIT_AREA_STARS);
				err_stat = -1;
			}
				
			StripStart_indx_OSC = Star_indx;
			StripStart_indx_Cat += DecStripEntry[DecStrip_indx].ListSize;
			DecStrip_indx++;
			StarCount = 0;
		}
		StarCount++;
	}

	while (DecStrip_indx < MaxNumDecStrip)
	{
		DecStripEntry[DecStrip_indx].OSCIndx = StripStart_indx_OSC;
		DecStripEntry[DecStrip_indx].NumStars = StarCount;
		DecStripEntry[DecStrip_indx].StarListIndx = StripStart_indx_Cat;
		DecStripEntry[DecStrip_indx].ListSize = AllocStripSize[DecStrip_indx];

		if( (DecStripEntry[DecStrip_indx].ListSize > 
				IniVaris->MAX_EDIT_AREA_STARS) ||
			(DecStripEntry[DecStrip_indx].NumStars >
				IniVaris->MAX_EDIT_AREA_STARS) ||
			(DecStripEntry[DecStrip_indx].NumStars >
				DecStripEntry[DecStrip_indx].ListSize) )
		{
			fprintf(stderr, 
				    "Too Many Stars in DecStrip %d\n", DecStrip_indx-1);
			fprintf(stderr, "NumStars = %d, ListSize = %d, Max Edit Area = %d\n",
					DecStripEntry[DecStrip_indx].NumStars,
					DecStripEntry[DecStrip_indx].ListSize,
					IniVaris->MAX_EDIT_AREA_STARS);
			err_stat = -1;
		}

		StripStart_indx_Cat += DecStripEntry[DecStrip_indx].ListSize;
		DecStrip_indx++;
		StarCount = 0;
	}

	/* Write Out CatEntry */
	if ( (fp = OpenFile("CatEntry.dat", "w")) != NULL )
	{
		for (DecStrip_indx=0; DecStrip_indx<MaxNumDecStrip; DecStrip_indx++)
			fprintf(fp, "%10d %10d %10d %10d\n",
					DecStripEntry[DecStrip_indx].OSCIndx,	
					DecStripEntry[DecStrip_indx].StarListIndx,	
					DecStripEntry[DecStrip_indx].NumStars,	
					DecStripEntry[DecStrip_indx].ListSize);	
	}
	fclose(fp);

	/* Count total OSC spares */
	for (DecStrip_indx=0; DecStrip_indx<MaxNumDecStrip; DecStrip_indx++)
		NumOSCSpares += (DecStripEntry[DecStrip_indx].ListSize - DecStripEntry[DecStrip_indx].NumStars);

	return(err_stat);
}

/* --------------------------------------
LoadStar: Load one star, and update CheckSum. Write memory comparison file.
*/

void LoadStar(StarOSC_type *Star, 
			  unsigned long *StarListCheckSum) 
{
	char HexString[80];
	int tmpHex, HexString1, HexString2;
	char Buf[80];
	unsigned long CheckSum;
	int i, Value;
	char CMD[]={"ACE_GENERAL_DATALOAD_TYPE3"};

	CheckSum = *StarListCheckSum;

	/* BSS_ID & Magnitude */
	sprintf(HexString, "%04.4x%04.4x", 
					Star->BSS_ID, Star->FixedPtMagnitude);
	CheckSum = UpdateCheckSum(HexString, CheckSum);
	fprintf(FpLoadStar, "CMD $CTCU_SEL %s 0x%s RT $ACE_SEL # ID=%-u Mag=%-d(%4.2f)\n", 
			CMD, HexString, Star->BSS_ID, Star->FixedPtMagnitude, Star->Magnitude); 
    sscanf( HexString, "%x", &tmpHex);
	HexString1 = (tmpHex & 0xffff0000) >> 16;
	HexString2 = (tmpHex & 0x0000ffff);  
	fprintf(FpLoadAdaStar, "DATA %04x\n", HexString1);
	fprintf(FpLoadAdaStar, "DATA %04x # ID=%-u Mag=%-d(%4.2f)\n", 
		HexString2, Star->BSS_ID, Star->FixedPtMagnitude, Star->Magnitude); 
	
	/* write to Memory dump comparison file */
	sscanf(HexString, "%x", &Value); 
	fwrite(&Value, sizeof(int), 1, FpStarMemDump);

	/* Right Ascension */
	sprintf(Buf, "%+12.7E", Star->RightAscension); 
	convert_single_float_to_hex(Buf, HexString);
	CheckSum = UpdateCheckSum(HexString, CheckSum);
	fprintf(FpLoadStar, "CMD $CTCU_SEL %s 0x%s RT $ACE_SEL # RA=%-12.7E\n",
		    CMD, HexString, Star->RightAscension);
    sscanf( HexString, "%x", &tmpHex);
	HexString1 = (tmpHex & 0xffff0000) >> 16;
	HexString2 = (tmpHex & 0x0000ffff); 
	fprintf(FpLoadAdaStar, "DATA %04x\n", HexString1);
	fprintf(FpLoadAdaStar, "DATA %04x # RA=%-12.7E\n",
		    HexString2, Star->RightAscension);

	/* write to Memory dump comparison file */
	sscanf(HexString, "%x", &Value); 
	fwrite(&Value, sizeof(int), 1, FpStarMemDump);

	/* ECI Vector */
	for (i=0; i<3; i++)
	{
		sprintf(Buf, "%+12.7E", Star->ECI[i]);
		convert_single_float_to_hex(Buf, HexString);
		CheckSum = UpdateCheckSum(HexString, CheckSum);
		fprintf(FpLoadStar, "CMD $CTCU_SEL %s 0x%s RT $ACE_SEL # ECI[%1d]=%-12.7E\n",
				CMD, HexString, i+1, Star->ECI[i]);
        sscanf( HexString, "%x", &tmpHex);
		HexString1 = (tmpHex & 0xffff0000) >> 16;
		HexString2 = (tmpHex & 0x0000ffff); 
		fprintf(FpLoadAdaStar, "DATA %04x\n", HexString1);
		fprintf(FpLoadAdaStar, "DATA %04x # ECI[%1d]=%-12.7E\n",
				HexString2, i+1, Star->ECI[i]);

		/* write to Memory dump comparison file */
		sscanf(HexString, "%x", &Value); 
		fwrite(&Value, sizeof(int), 1, FpStarMemDump);
	}

	*StarListCheckSum = CheckSum;
}






/* -----------------------------------------
LoadSpareStar: Load one Spare Star.
*/
void LoadSpareStar(FILE *Fp, FILE *Fp1)
{
	int i;
	int Value[5] = {0, 0, 0, 0, 0};

	char CMD[]={"ACE_GENERAL_DATALOAD_TYPE3"};

	/* BSS_ID & Magnitude */
	fprintf(Fp, "		CMD $CTCU_SEL %s 0x0 RT $ACE_SEL	# ID=N/A  Mag=N/A\n", 
			CMD); 
	fprintf(Fp1, "		DATA 0x0000\n");
	fprintf(Fp1, "		DATA 0x0000 	# ID=N/A  Mag=N/A\n");
	
	/* Right Ascension */
	fprintf(Fp, "		CMD $CTCU_SEL %s 0x0 RT $ACE_SEL	# RA=N/A\n", CMD);
	fprintf(Fp1, "		DATA 0x0000\n");
	fprintf(Fp1, "		DATA 0x0000 	# RA=N/A\n");

	/* ECI Vector */
	for (i=0; i<3; i++)
    {
		fprintf(Fp, "		CMD $CTCU_SEL %s 0x0 RT $ACE_SEL	# ECI[%1d]=N/A\n", CMD, i+1);
	    fprintf(Fp1, "		DATA 0x0000\n");
		fprintf(Fp1, "		DATA 0x0000 	# ECI[%1d]=N/A\n", i+1);
    }
}

/* -----------------------------------------
 LoadCatalogEntry:	Load two adjacent Catalog entry at a time(each entry with  
					3 16bit words).  Update CheckSum. Write to memory comparison 
					file.
*/
void LoadCatalogEntry(CatEntry_type *Entry1, 
					  CatEntry_type *Entry2,
					  unsigned long *CatalogEntryCheckSum)
{
	char HexString[80];
	int tmpHex, HexString1, HexString2;
	unsigned long CheckSum;
	char CMD[]={"ACE_GENERAL_DATALOAD_TYPE3"};
	int Value;

	CheckSum = *CatalogEntryCheckSum;

	/* StarListIndx and NumStars for 1st Entry */
	sprintf(HexString, "%04.4x%04.4x", Entry1->StarListIndx,
									   Entry1->NumStars);
	CheckSum = UpdateCheckSum(HexString, CheckSum);
    sscanf( HexString, "%x", &tmpHex);
	fprintf(FpLoadCatalog, 
			"CMD $CTCU_SEL %s 0x%s RT $ACE_SEL# Indx=%-d NStars=%-d\n",
			 CMD, HexString, Entry1->StarListIndx, Entry1->NumStars); 
	HexString1 = (tmpHex & 0xffff0000) >> 16;
	HexString2 = (tmpHex & 0x0000ffff); 
	fprintf(FpLoadAdaCatalog, "DATA %04x\n", HexString1);
	fprintf(FpLoadAdaCatalog, 
			"DATA %04x # Indx=%-d NStars=%-d\n",
			 HexString2, Entry1->StarListIndx, Entry1->NumStars); 

	/* write to Memory dump comparison file */
	sscanf(HexString, "%x", &Value); 
	fwrite(&Value, sizeof(int), 1, FpCatStrucMemDump);

	/* ListSize for 1st Entry and StarListIndx for 2nd Entry */
	sprintf(HexString, "%04.4x%04.4x", Entry1->ListSize,
									   Entry2->StarListIndx);
	CheckSum = UpdateCheckSum(HexString, CheckSum);
    sscanf( HexString, "%x", &tmpHex);
	fprintf(FpLoadCatalog, 
			"CMD $CTCU_SEL %s 0x%s RT $ACE_SEL# Length=%-d Indx=%-d\n",
			 CMD, HexString, Entry1->ListSize, Entry2->StarListIndx); 
	HexString1 = (tmpHex & 0xffff0000) >> 16;
	HexString2 = (tmpHex & 0x0000ffff); 
	fprintf(FpLoadAdaCatalog, "DATA %04x\n", HexString1);
	fprintf(FpLoadAdaCatalog, 
			"DATA %04x # Length=%-d Indx=%-d\n",
			 HexString2, Entry1->ListSize, Entry2->StarListIndx); 

	/* write to Memory dump comparison file */
	sscanf(HexString, "%x", &Value); 
	fwrite(&Value, sizeof(int), 1, FpCatStrucMemDump);

	/* NumStars and ListSize for 2nd Entry */
	sprintf(HexString, "%04.4x%04.4x", Entry2->NumStars,
									   Entry2->ListSize);
	CheckSum = UpdateCheckSum(HexString, CheckSum);
    sscanf( HexString, "%x", &tmpHex);
	fprintf(FpLoadCatalog, 
			"CMD $CTCU_SEL %s 0x%s RT $ACE_SEL# NStars=%-d Length=%-d\n",
			 CMD, HexString, Entry2->NumStars, Entry2->ListSize); 
	HexString1 = (tmpHex & 0xffff0000) >> 16;
	HexString2 = (tmpHex & 0x0000ffff); 
	fprintf(FpLoadAdaCatalog, "DATA %04x\n", HexString1);
	fprintf(FpLoadAdaCatalog, 
			"DATA %04x # NStars=%-d Length=%-d\n",
			 HexString2, Entry2->NumStars, Entry2->ListSize); 

	/* write to Memory dump comparison file */
	sscanf(HexString, "%x", &Value); 
	fwrite(&Value, sizeof(int), 1, FpCatStrucMemDump);

	*CatalogEntryCheckSum = CheckSum;
}


/* -----------------------------
LoadDecStripStars: Load stars in one declination strip to Star List Area.
			  		Sort Stars within the strip by Right Ascension.
*/
void LoadDecStripStars(	OSC_type *OSC, 
			CatEntry_type *CatEntry,
			unsigned long *StarListCheckSum)
{
	int indx;
	int StarListIndx;
	int OSCIndx;
	StarOSC_type Spare;

	StarListIndx = CatEntry->StarListIndx;
	OSCIndx = CatEntry->OSCIndx;
 

	/* Sort Stars by Right Ascension */
	if (CatEntry->NumStars > 1)
		qsort(&(OSC->Stars[OSCIndx]), CatEntry->NumStars, 
			sizeof(StarOSC_type), RAscenCompareFncOSC);

	GetSpareStarRecord(&Spare);

	for (indx=0; indx<CatEntry->ListSize; indx++)
	{
		fprintf(FpLoadStar, "#	StarListIndx = %-d\n", StarListIndx); 
		fprintf(FpLoadAdaStar, "C	StarListIndx = %-d\n", StarListIndx); 
		StarListIndx++;

		if (indx < CatEntry->NumStars)
		   /* Load OSC Stars */
		   LoadStar(&(OSC->Stars[OSCIndx+indx]), StarListCheckSum);
		else
  		   /* Load Spare Star records */
		   LoadStar(&Spare, StarListCheckSum);
	}
}


/* ---------------------------------------
CreateFileHeader:
*/

void CreateFileHeader(FILE *FpUpLoad,
	              char *TpltFileName, 
	              char *UpLoadFileName,
	              char *SSCFileName,
	              char *INIFileName,
				  FILE *FpUpLoadAda,
	              char *Epoch,
	              char *VERSION)
{
	time_t t;

	time(&t);
	fprintf(FpUpLoad, 
	"# History: This file was generated by GSS Version %s on GMT %s", VERSION, ctime(&t));
	fprintf(FpUpLoad, 
	"#           From SIAD Star Catalog file %s\n", SSCFileName);
	fprintf(FpUpLoad, 
	"#           From GSS Initialization file %s\n", INIFileName);
	fprintf(FpUpLoad, 
	"#           From PROC Template file %s\n", TpltFileName);
	fprintf(FpUpLoad, 
	"#           Using ASC Epoch %s GMT\n#",Epoch);

	fprintf(FpUpLoadAda, 
	"C History: This file was generated by GSS Version %s on GMT %s", VERSION, ctime(&t));
	fprintf(FpUpLoadAda, 
	"C           From SIAD Star Catalog file %s\n", SSCFileName);
	fprintf(FpUpLoadAda, 
	"C           From GSS Initialization file %s\n", INIFileName);
	fprintf(FpUpLoadAda, 
	"C           Using ASC Epoch %s GMT\nC\n",Epoch);
    if (oneDecOnly == 0)
    {
       fprintf(FpUpLoadAda,
          "C Objective: This proceduce Uploads The Complete OSC.\n");
    }
    else
    {
       fprintf(FpUpLoadAda,
          "C Objective: This proceduce Updates OSC Stars Which Are Within:\n");
       fprintf(FpUpLoadAda,
          "C             Declination Strip %d: %3.1f to %3.1f Degree.\n", 
			stripNum, lowEnd, upperEnd);
    }
    fprintf(FpUpLoadAda,
    "C\n");
}


/* ---------------------------------------
UpLoadFullOSC
*/

int UpLoadFullOSC(char *TpltFileName,
	          char *RootFileName,
	          char *UpLoadFileName,
	          char *CatStrucFileName,
	          char *SSCFileName,
	          char *INIFileName,
	          char *Epoch,
	          char *VERSION,
	          IniUpLoad_type *IniVars,
	          OSC_type *OSC,
	          double DecStepSize,
	          CatEntry_type CatEntry[])
{
	char PrcFilename[200];
    char UpLoadAdaFilename[200];
	char StarsFilename[200];
	char CatStrucFilename[200];
	int err_state = 0;
	int indx, MaxNumDecStrip;
	FILE *FpUpLoad;
	FILE *FpUpLoadAda;
	FILE *FpTplt;
	unsigned long StarListCheckSum = 0;
	unsigned long CatalogEntryCheckSum = 0;
	int TotalCatalogStars;
	int TotalUpLoadStars;
	int NumWordStarList32B;  
	int NumWordCatEntry32B;
	int DecStep;
	int Total16bStars;
	int Total16bCatStruc;
	unsigned int StopStarsAddr;
	unsigned int StopCatStrucAddr;
	int Value, starAddr, catAddr;
	char String[40];
	float LowDec = -90.0;
	
	char file_line[121];
	char out_line[121];
	char blanks[121];
	char buf[20];
	char buf2[121];

    char tmpStr[5];
    int ii;

	int i, j, k, l, n, o, offset;

	for (i=0; i<=120; i++) {
	    blanks[i] = 0;
	    
	}

	/* -------------------------------------
	 Create Star List File and Catalog File(include mem dump files)
	 Mem dump file format: binary file.  
	 eg. 000d 5dc0 000d 5dc3 6755 df78 1bbc 2cca

	 where     000d = start address state
			   5dc0 = start address
			   000d = stop address state
			   5dc3 = stop address 
			   6755 = first data dump for address 5dc0
			   df78 = data dump for address 5dc1
			   1bbc = data dump for address 5dc2
			   2cca = data dump for address 5dc3
	 --------------------------------------- */
	sprintf(PrcFilename, "%s.prc", UpLoadFileName); 
    sprintf(UpLoadAdaFilename, "%s.dat", UpLoadFileName);
	sprintf(StarsFilename, "%s.mem", UpLoadFileName);
	sprintf(CatStrucFilename, "%s.mem", CatStrucFileName);

	if ( ( (FpLoadStar = OpenFile(LoadStarFileName, "w") ) == NULL ) || 
	     ( (FpLoadAdaStar = OpenFile(LoadAdaStarFileName, "w") ) == NULL ) || 
		 ( (FpLoadCatalog = OpenFile(LoadCatFileName, "w") ) == NULL ) ||
		 ( (FpLoadAdaCatalog = OpenFile(LoadAdaCatFileName , "w") ) == NULL) ||
		 ( (FpStarMemDump = OpenFile(StarsFilename, "wb") ) == NULL ) ||
		 ( (FpCatStrucMemDump = OpenFile(CatStrucFilename, "wb" ) ) == NULL ) ) 

		return(-1);

	MaxNumDecStrip = (int)(180.0 / DecStepSize);
	
	/* Write start and stop address to stars and CatStruc Mem dum files */
	Total16bStars = (OSC->NumStars + NumOSCSpares) * NUM_WORDS_IN_STAR_RECORD;
	Total16bCatStruc = MaxNumDecStrip*NUM_WORDS_IN_CATALOG_RECORD;
	StopStarsAddr = (unsigned int)IniVars->StarAreaStartAddr + Total16bStars - 1;
	StopCatStrucAddr = (unsigned int)IniVars->CatAreaStartAddr + 
															Total16bCatStruc - 1;
    sprintf(String, "%04.4x%04.4x", IniVars->StarAreaStartAddrState,
			IniVars->StarAreaStartAddr);
    sscanf(String, "%x", &Value);
	fwrite(&Value, sizeof(int), 1, FpStarMemDump);
	sprintf(String, "%04.4x%04.4x", IniVars->StarAreaStartAddrState,
			StopStarsAddr);
    sscanf(String, "%x", &Value);
	fwrite(&Value, sizeof(int), 1, FpStarMemDump);

    sprintf(String, "%04.4x%04.4x", IniVars->CatAreaStartAddrState,
			IniVars->CatAreaStartAddr);
    sscanf(String, "%x", &Value);
	fwrite(&Value, sizeof(int), 1, FpCatStrucMemDump); 
    sprintf(String, "%04.4x%04.4x", IniVars->CatAreaStartAddrState,
            StopCatStrucAddr);
    sscanf(String, "%x", &Value);
	fwrite(&Value, sizeof(int), 1, FpCatStrucMemDump);

    sprintf(tmpStr, "%1x%04x", IniVars->StarAreaStartAddrState,
		IniVars->StarAreaStartAddr);
	sscanf(tmpStr, "%x", &starAddr); 
    sprintf(tmpStr, "%1x%04x", IniVars->CatAreaStartAddrState,
		IniVars->CatAreaStartAddr);
    sscanf(tmpStr, "%x", &catAddr);

	for (indx=0; indx<MaxNumDecStrip; indx++)
	{
	    if (indx == 0)
		{
		   fprintf(FpLoadAdaStar,
			"\n\nTYPE	RAM_AF\n\n\n");
		   fprintf(FpLoadAdaStar,
			   "C\nC =========================\n");
		   fprintf(FpLoadAdaStar,
			   "C Load Star Data\n");
		   fprintf(FpLoadAdaStar,
			   "C =========================\n\n");
		   fprintf(FpLoadAdaStar,
			"BLOCK_NUM	1\n");
		   fprintf(FpLoadAdaStar,
			"ADDRESS  %x\n", starAddr);
		   fprintf(FpLoadAdaStar,
			"LENGTH %d\n\n", Total16bStars);
		   fprintf(FpLoadAdaCatalog,
			   "C\nC =========================\n");
		   fprintf(FpLoadAdaCatalog,
			   "C Load Catalog \n");
		   fprintf(FpLoadAdaCatalog,
			   "C =========================\n\n");
		   fprintf(FpLoadAdaCatalog,
			   "BLOCK_NUM 2\n");
		   fprintf(FpLoadAdaCatalog,
			   "ADDRESS   %x\n", catAddr);
		   fprintf(FpLoadAdaCatalog,
			"LENGTH %d\n\n", Total16bCatStruc);
	 	}

		/* Star Area List */
		fprintf(FpLoadStar, "# --\nC	DecStripIndx = %-d(%-5.1f~%-5.1f)\n", 
									indx+1, LowDec, LowDec+DecStepSize);
		fprintf(FpLoadStar, "#	Number of Stars in Strip = %-d\n", 
										CatEntry[indx].NumStars);
		fprintf(FpLoadStar, "# 	Strip Length = %-d\n# --\n", 
										CatEntry[indx].ListSize);
		fprintf(FpLoadAdaStar, "C --\nC	DecStripIndx = %-d(%-5.1f~%-5.1f)\n", 
									indx+1, LowDec, LowDec+DecStepSize);
		fprintf(FpLoadAdaStar, "C	Number of Stars in Strip = %-d\n", 
										CatEntry[indx].NumStars);
		fprintf(FpLoadAdaStar, "C 	Strip Length = %-d\nC --\n", 
										CatEntry[indx].ListSize);

		LoadDecStripStars(OSC, &(CatEntry[indx]), &StarListCheckSum); 

		/* Catalog Area Entry */
		if ( ( (indx+1) % 2 ) == 0 )
		{
			fprintf(FpLoadCatalog, 
			"# --\n#	DecStripIndx = %-d(%-5.1f~%-5.1f)\n", 
								indx, LowDec-DecStepSize, LowDec);
			fprintf(FpLoadCatalog, 
			"#	DecStripIndx = %-d(%-5.1f~%-5.1f)\n# --\n", 
								indx+1, LowDec, LowDec+DecStepSize);
			fprintf(FpLoadAdaCatalog, 
			"C --\nC	DecStripIndx = %-d(%-5.1f~%-5.1f)\n", 
								indx, LowDec-DecStepSize, LowDec);
			fprintf(FpLoadAdaCatalog, 
			"C	DecStripIndx = %-d(%-5.1f~%-5.1f)\nC --\n", 
								indx+1, LowDec, LowDec+DecStepSize);

			LoadCatalogEntry(&(CatEntry[indx-1]), &(CatEntry[indx]), 
												&CatalogEntryCheckSum);
		   if (indx == (MaxNumDecStrip-1) )
		   {
		      fprintf(FpLoadAdaStar,
			     "\n\nCHKSUM  %08x\n\n\n", StarListCheckSum);
		      fprintf(FpLoadAdaCatalog,
			     "\n\nCHKSUM  %08x\n\n\n", CatalogEntryCheckSum);
		   }
		}
		LowDec += DecStepSize;
	}
	fclose(FpLoadCatalog);
	fclose(FpLoadAdaStar);
	fclose(FpLoadAdaCatalog); 
	fclose(FpLoadStar);
	fclose(FpStarMemDump);
	fclose(FpCatStrucMemDump);

	/* Parameters for uploading */
	TotalCatalogStars = CatEntry[MaxNumDecStrip-1].OSCIndx + 
						CatEntry[MaxNumDecStrip-1].NumStars;
	TotalUpLoadStars = CatEntry[MaxNumDecStrip-1].StarListIndx + 
					   CatEntry[MaxNumDecStrip-1].ListSize - 1;
	NumWordStarList32B =  TotalUpLoadStars*NUM_WORDS_IN_STAR_RECORD / 2; 
	NumWordCatEntry32B = MaxNumDecStrip*NUM_WORDS_IN_CATALOG_RECORD / 2;

	if (NumWordStarList32B*2 != Total16bStars )
	{
		fprintf(stderr, "Error Number of upload stars");
		return(-1);
	}

 	if (DecStepSize == 0.5)
	{
		sprintf(String, "0.5_DEGREE");
		DecStep = 1;
	}
 	if (DecStepSize == 1.0)
	{
		sprintf(String, "1.0_DEGREE");
		DecStep = 2;
	}
 	if (DecStepSize == 2.0) 
	{
		sprintf(String, "2.0_DEGREE");
		DecStep = 3;
	}
	/*-------------------------------
		Creat UpLoad File 
	-------------------------------*/
	if ( ( (FpLoadStar = OpenFile(LoadStarFileName, "r") ) == NULL ) || 
	     ( (FpLoadAdaStar = OpenFile(LoadAdaStarFileName, "r") ) == NULL) ||
	     ( (FpLoadCatalog = OpenFile(LoadCatFileName, "r") ) == NULL ) ||
		 ( (FpLoadAdaCatalog = OpenFile(LoadAdaCatFileName, "r") ) == NULL ) ||
	     ( (FpUpLoad = OpenFile(PrcFilename, "w") ) == NULL )  ||
	     ( (FpUpLoadAda = OpenFile(UpLoadAdaFilename, "w") ) == NULL ) ||
	     ( (FpTplt = OpenFile(TpltFileName, "r") ) == NULL ) )
		 return(-1);

/*****************************************************************************************/

	while (fgets (file_line, 120, FpTplt)) {
            i = 0;
	    while (strlen(OSC_key[i]) != 0) {

	        if (strstr(file_line, OSC_key[i]) != 0) {
/*	            fprintf(stderr, "%s", file_line);*/
	            strncpy(out_line, blanks, 120);
	            sprintf(buf, "                    ");
	            
	            for (j=0; file_line[j] != '\0'; j++) {
	                for (k=j, l=0; OSC_key[i][l] != '\0' && file_line[k]==OSC_key[i][l]; k++, l++)
	                    ;
	                if (OSC_key[i][l] == '\0') {
	                    offset = strlen(OSC_key[i]);
	                    
	                    strncpy(out_line, file_line, j);
	                    switch(i) {
	                        case 0: 
				   			   CreateFileHeader(FpUpLoad, TpltFileName, 
				      			  PrcFilename, SSCFileName, INIFileName, FpUpLoadAda,
				      			  Epoch, VERSION);
	                            break;
	                        case 1: fprintf(FpUpLoad, "#\n#\n# -----------\n# End of File  \n# -----------\n#\n#\n");
	                        	fprintf(FpUpLoadAda, "C\nC\nC -----------\nC End of File  \nC -----------\nC\nC\n");
	                            break;
	                        case 2: sprintf(buf, "%d", TotalCatalogStars);
	                            break;
	                        case 3: sprintf(buf, "%d", TotalUpLoadStars);
	                            break;
	                        case 4: sprintf(buf, "0x%4.4x", IniVars->DecStepStartAddrState);
	                            break;
	                        case 5: sprintf(buf, "0x%4.4x", IniVars->DecStepStartAddr);
	                            break;
	                        case 6: sprintf(buf, "0x%4.4x", DecStep);
	                            break;
	                        case 7: sprintf(buf, "%-2.1f", DecStepSize);
	                            break;
	                        case 8: sprintf(buf, "0x%4.4x", IniVars->StarAreaStartAddrState);
	                            break;
	                        case 9: sprintf(buf, "0x%4.4x", IniVars->StarAreaStartAddr);
	                            break;
	                        case 10: sprintf(buf, "%-d", NumWordStarList32B);
	                            break;
	                        case 11: sprintf(buf, "%-lu", StarListCheckSum);
	                            break;
	                        case 12: AppendFile(FpUpLoadAda, FpLoadAdaStar);
	                            break;
	                        case 13: sprintf(buf, "0x%4.4x", IniVars->CatAreaStartAddrState);
	                            break;
	                        case 14: sprintf(buf, "0x%4.4x", IniVars->CatAreaStartAddr);
	                            break;
	                        case 15: sprintf(buf, "%-d", NumWordCatEntry32B);
	                            break;
	                        case 16: sprintf(buf, "%-lu", CatalogEntryCheckSum);
	                            break;
	                        case 17: AppendFile(FpUpLoadAda, FpLoadAdaCatalog);
	                            break;
	                        case 18: 
				   			   CreateFileHeader(FpUpLoad, TpltFileName, 
				      			  PrcFilename, SSCFileName, INIFileName, FpUpLoadAda,
				      			  Epoch, VERSION);
	                            break;
	                        case 19: fprintf(FpUpLoad, "#\n#\n# -----------\n# End of File \n# -----------\n#\n#\n");
	                        	fprintf(FpUpLoadAda, "C\nC\nC -----------\nC End of File \nC -----------\nC\nC\n");
	                            break;
	                        case 20: sprintf(buf, "%-2.1f", LowDec);
	                            break;
	                        case 21: sprintf(buf, "%-2.1f", LowDec+DecStepSize);
	                            break;
	                        case 23: sprintf(buf, "0x%4.4x", IniVars->EditAreaStartAddrState);
	                            break;
	                        case 24: sprintf(buf, "0x%4.4x", IniVars->EditAreaStartAddr);
	                            break;
	                        case 25: sprintf(buf, "%-lu", StarListCheckSum);
	                            break;
	                        case 26: sprintf(buf, "%-d", CatEntry->NumStars);
	                            break;
	                        case 27: AppendFile(FpUpLoadAda, FpLoadAdaStar);
				    break;
				case 28: sprintf(buf, "%s", RootFileName);
	                    }
	                    strcat(out_line, buf);

	                    n=0;
	                    for (o=j+offset; o <= strlen(file_line); o++) {
	                        buf2[n] = file_line[o];
	                        n++;
	                    }
	                   
	                    strcat(out_line, buf2);
	                    strcpy(file_line, out_line);
	                    
	                }
	                
	            }
	        
	        }
	        
	        
	        i++;
	        
	    }
	    fprintf(FpUpLoad, "%s", file_line);
	}


/****************************************************************************************************/

	fclose(FpLoadStar);
	fclose(FpLoadCatalog);
	fclose(FpUpLoad);
	fclose(FpTplt);
	fclose(FpLoadAdaStar);
	fclose(FpUpLoadAda); 

	return(err_state);
}

/* ---------------------------------------
UpLoadOneDecStrip:
*/

int UpLoadOneDecStrip(char *TpltFileName,
	              char *RootFileName,
	              char *UpLoadFileName,
	              char *SSCFileName,
	              char *INIFileName,
	              char *Epoch,
	              char *VERSION,
	              IniUpLoad_type *IniVars,
	              OSC_type *OSC,
	              CatEntry_type *CatEntry,
	              int DecStripIndx,
	              double DecStepSize,
	              double LowDec)
{
	char PrcFilename[200];
	char StarsFilename[200];
    char UpLoadAdaFilename[200];
	char HexString[20];
	int err_state = 0;
	int Total16bStars;
	unsigned int StopStarsAddr;
	int Value, ValueArray[5]={0, 0, 0, 0, 0};
	FILE *FpUpLoad;
	FILE *FpUpLoadAda;
	FILE *FpTplt;
	unsigned long StarListCheckSum = 0;
	char ReqEditAction[10];
	char String[60];
	
	char file_line[121];
	char out_line[121];
	char blanks[121];
	char buf[20];
	char buf2[121];
    char tmpStr[5];
    int starAddr, stripLen;

	int i, j, k, l, n, o, offset;

	for (i=0; i<=120; i++) {
	    blanks[i] = 0;
	    
	}

    stripNum = DecStripIndx+1;
    lowEnd = LowDec;
    upperEnd = LowDec + DecStepSize;

	/* -----------------------------------------------------
		Create Star List File(include emme dump file)
        Mem dump file format: binary file.
	    eg. 000d 5dc0 000d 5dc3 6755 df78 1bbc 2cca

		where     000d = start address state
			      5dc0 = start address
				  000d = stop address state
				  5dc3 = stop address
				  6755 = first data dump for address 5dc0
				  df78 = data dump for address 5dc1
				  1bbc = data dump for address 5dc2
				  2cca = data dump for address 5dc3
	--------------------------------------------------------*/
	sprintf(PrcFilename, "%s.prc", UpLoadFileName);
    sprintf(UpLoadAdaFilename, "%s.dat", UpLoadFileName);
	sprintf(StarsFilename, "%s.mem", UpLoadFileName);

	if ( ( (FpLoadStar = OpenFile(LoadStarFileName, "w") ) == NULL) ||
	     ( (FpLoadAdaStar = OpenFile(LoadAdaStarFileName, "w") ) == NULL) ||
	     ( (FpStarMemDump = OpenFile(StarsFilename, "wb") ) == NULL) )

		return(-1);

	if (CatEntry->NumStars > IniVars->MAX_EDIT_AREA_STARS)
	{
		fprintf(stderr, "Too Many Stars in the Strip. NumStars = %d\n",
					CatEntry->NumStars);
		return(-1);
	}

	/* Write start and stop address to stars Mem dum Comparison file */
	Total16bStars = IniVars->MAX_EDIT_AREA_STARS * NUM_WORDS_IN_STAR_RECORD;
	StopStarsAddr = (unsigned int)IniVars->EditAreaStartAddr + Total16bStars - 1; 
	sprintf(HexString, "%04.4x%04.4x", IniVars->EditAreaStartAddrState, 
			IniVars->EditAreaStartAddr);
	sscanf(HexString, "%x", &Value);
    fwrite(&Value, sizeof(int), 1, FpStarMemDump);
	sprintf(HexString, "%04.4x%04.4x", IniVars->EditAreaStartAddrState,
			StopStarsAddr);
    sscanf(HexString, "%x", &Value);
    fwrite(&Value, sizeof(int), 1, FpStarMemDump);

    stripLen = CatEntry->ListSize;

	/* Set ListSize to CatEntry->NumStars */
	CatEntry->ListSize = CatEntry->NumStars;

	CatEntry->StarListIndx = 1;

	LoadDecStripStars(OSC, CatEntry, &StarListCheckSum); 
	fclose(FpLoadStar);
	fclose(FpLoadAdaStar);
	
	/* Write reset of edit area to Zeros for mem dump file */ 
	for ( i=CatEntry->NumStars; i<IniVars->MAX_EDIT_AREA_STARS; i++ )
		fwrite(&ValueArray, sizeof(int), 5, FpStarMemDump);
    fclose(FpStarMemDump);

 	if (DecStepSize == 0.5) sprintf(String, "0.5_DEGREE");
 	if (DecStepSize == 1.0) sprintf(String, "1.0_DEGREE");
 	if (DecStepSize == 2.0) sprintf(String, "2.0_DEGREE");
	/* -------------------------
		Create UpLaod File
	---------------------------- */

	if ( ( (FpLoadStar = OpenFile(LoadStarFileName, "r") ) == NULL ) || 
	     ( (FpLoadAdaStar = OpenFile(LoadAdaStarFileName, "r") ) == NULL) ||
	     ( (FpUpLoad = OpenFile(PrcFilename, "w") ) == NULL ) ||
	     ( (FpUpLoadAda = OpenFile(UpLoadAdaFilename, "w") ) == NULL ) ||
	     ( (FpTplt = OpenFile(TpltFileName, "r") ) == NULL ) )
	     
		 return(-1);
		 
/********************************************************************************************/

    sprintf(tmpStr, "%1x%04x", IniVars->EditAreaStartAddrState,
		IniVars->EditAreaStartAddr);
	sscanf(tmpStr, "%x", &starAddr); 

	while (fgets (file_line, 120, FpTplt)) {
            i = 0;
	    while (strlen(OSC_key[i]) != 0) {

	        if (strstr(file_line, OSC_key[i]) != 0) {
	            strncpy(out_line, blanks, 120);
	            sprintf(buf, "                    ");
	            
	            for (j=0; file_line[j] != '\0'; j++) {
	                for (k=j, l=0; OSC_key[i][l] != '\0' && file_line[k]==OSC_key[i][l]; k++, l++)
	                    ;
	                if (OSC_key[i][l] == '\0') {
	                    offset = strlen(OSC_key[i]);
	                    
	                    strncpy(out_line, file_line, j);
	                    switch(i) {
	                        case 0: 
				   			   CreateFileHeader(FpUpLoad, TpltFileName, 
				   				  PrcFilename, SSCFileName, INIFileName, FpUpLoadAda,
				   				  Epoch, VERSION);
	                            break;
	                        case 1: fprintf(FpUpLoad, "#\n#\n# -----------\n# End of File \n# -----------\n#\n#\n");
	                        	fprintf(FpUpLoadAda, "C\n#\n# -----------\n# End of File \nC -----------\nC\nC\n");
	                            break;
	                        case 4: sprintf(buf, "0x%4.4x", IniVars->DecStepStartAddrState);
	                            break;
	                        case 5: sprintf(buf, "0x%4.4x", IniVars->DecStepStartAddr);
	                            break;
	                        case 7: sprintf(buf, "%-2.1f", DecStepSize);
	                            break;
	                        case 8: sprintf(buf, "0x%4.4x", IniVars->StarAreaStartAddrState);
	                            break;
	                        case 9: sprintf(buf, "0x%4.4x", IniVars->StarAreaStartAddr);
	                            break;
	                        case 11: sprintf(buf, "%-lu", StarListCheckSum);
	                            break;
	                        case 12: AppendFile(FpUpLoadAda, FpLoadAdaStar);
	                            break;
	                        case 13: sprintf(buf, "0x%4.4x", IniVars->CatAreaStartAddrState);
	                            break;
	                        case 14: sprintf(buf, "0x%4.4x", IniVars->CatAreaStartAddr);
	                            break;
	                        case 17: AppendFile(FpUpLoadAda, FpLoadAdaCatalog);
	                            break;
	                        case 18: 
						    {
				   			    CreateFileHeader(FpUpLoad, TpltFileName, 
				      			    PrcFilename, SSCFileName, INIFileName, FpUpLoadAda,
				      			    Epoch, VERSION);
		   						fprintf(FpUpLoadAda,
								    "\n\nTYPE	RAM_AF\n\n\n");
		   						fprintf(FpUpLoadAda,
			   					    "C\nC =========================\n");
		   						fprintf(FpUpLoadAda,
			   					    "C Load Star Data\n");
		   					  	fprintf(FpUpLoadAda,
			   					    "C =========================\n\n");
		   						fprintf(FpUpLoadAda,
								    "BLOCK_NUM	1\n");
		   						fprintf(FpUpLoadAda,
								    "ADDRESS  %x\n", starAddr);
		   						fprintf(FpUpLoadAda, 
								    "LENGTH %d\n\n", 
								    CatEntry->ListSize*NUM_WORDS_IN_STAR_RECORD);
								fprintf(FpUpLoadAda, "C --\nC	DecStripIndx = %-d(%-5.1f~%-5.1f)\n", stripNum, lowEnd, upperEnd);
								fprintf(FpUpLoadAda, "C	Number of Stars in Strip = %-d\n", CatEntry->ListSize);
								fprintf(FpUpLoadAda, "C 	Strip Length = %-d\nC --\n", stripLen);
	                            break;
						    }
	                        case 19: 
							{
	   						    fprintf(FpUpLoadAda,
			     				    "\n\nCHKSUM  %08x\n\n\n", StarListCheckSum);
							    fprintf(FpUpLoad, "#\n#\n# -----------\n# End of File\n# -----------\n#\n#\n");
	                        	fprintf(FpUpLoadAda, "C\nC\nC -----------\nC End of File\nC -----------\nC\nC\n");
	                            break;
							}
	                        case 20: sprintf(buf, "%-2.1f", LowDec);
	                            break;
	                        case 21: sprintf(buf, "%-2.1f", LowDec+DecStepSize);
	                            break;
	                        case 22: sprintf(buf, "%-d", DecStripIndx+1);
	                            break;
	                        case 23: sprintf(buf, "0x%4.4x", IniVars->EditAreaStartAddrState);
	                            break;
	                        case 24: sprintf(buf, "0x%4.4x", IniVars->EditAreaStartAddr);
	                            break;
	                        case 25: sprintf(buf, "%-lu", StarListCheckSum);
	                            break;
	                        case 26: sprintf(buf, "%-d", CatEntry->NumStars);
	                            break;
	                        case 27: AppendFile(FpUpLoadAda, FpLoadAdaStar);
	                            break;
	                        case 28: sprintf(buf, "%s", RootFileName);
	                    }

	                    if (strlen(buf) > 0) {
	                        strcat(out_line, buf);

	                    }
	                    
	                    n=0;
	                    for (o=j+offset; o <= strlen(file_line); o++) {
	                        buf2[n] = file_line[o];
	                        n++;
	                    }
	                   
	                    strcat(out_line, buf2);
	                    strcpy(file_line, out_line);
	                    
	                }
	                
	            }
	        
	        }
	        
	        
	        i++;
	        
	    }
	    fprintf(FpUpLoad, "%s", file_line);
	}

	fclose(FpLoadStar);
	fclose(FpUpLoad);
	fclose(FpTplt);
	fclose(FpLoadAdaStar);
	fclose(FpUpLoadAda); 

	return(err_state);
}

/* ----------------------------------------
CreateUpLoadFile:
*/

int CreateUpLoadFile(char *TpltFileName,
	             char *RootFileName, 
	             char *UpLoadFileName, 
	             char *CatStrucFileName, 
	             char *SSCFileName,
	             char *INIFileName,
	             char *Epoch,
	             char *VERSION,
	             AcqSC_type *ASC, 
	             IniUpLoad_type *IniVaris,
	             int AllocSizeStrips[],
	             int SelUpLoadType,
	             double DecStepSize,
	             double LowDec,
	             int TM_FOR_MST_FLAG)
{
	int err_state=0;
	
	OSC_type OSC;
	CatEntry_type CatEntry[360];
	int DecStripIndx;
	double DecStrip;

	TM_FOR_MST = TM_FOR_MST_FLAG;

	/* Create OSC from ASC */
	err_state = CreateOSC(ASC, &OSC, IniVaris, DecStepSize);
	if (err_state < 0) return(err_state);

	/* Creat Catalog Entry */
	err_state = CreateDecStripEntry(&OSC, IniVaris, AllocSizeStrips,
									DecStepSize, CatEntry);
	if (err_state < 0) return(err_state);

	/* Create UpLoad file */
	switch ( SelUpLoadType )
	{
		case LOAD_FULL_OSC:
			err_state = UpLoadFullOSC(TpltFileName, 
			                          RootFileName,
			                          UpLoadFileName, 
			                          CatStrucFileName, 
			                          SSCFileName, 
			                          INIFileName, 
			                          Epoch, 
			                          VERSION,
			                          IniVaris,
			                          &OSC, 
			                          DecStepSize, 
			                          CatEntry);
			break;

		case LOAD_DEC_STRIP:
			DecStripIndx = (int)((LowDec+90.0) / DecStepSize);		
			DecStrip = DecStripIndx * DecStepSize - 90.0;
		    oneDecOnly = 1;
			err_state = UpLoadOneDecStrip(TpltFileName,
						      RootFileName,  
			                              UpLoadFileName,
			                              SSCFileName, 
			                              INIFileName, 
			                              Epoch, 
			                              VERSION,
			                              IniVaris,
			                              &OSC, 
			                              &(CatEntry[DecStripIndx]),
			                              DecStripIndx, 
			                              DecStepSize, 
			                              DecStrip); 
		    oneDecOnly = 0;
			break;

		default:
			fprintf(stderr, "LoadOSC: UpLoad Type Error. UpLoadType = %d\n",
				SelUpLoadType);
			err_state = -1;
			break;
	}

	remove(LoadStarFileName);
	remove(LoadAdaStarFileName); 
	remove(LoadCatFileName);
	remove(LoadAdaCatFileName);
	remove("CatEntry.dat");
	remove("OSC.dat");
	return(err_state);
}

/*************************************************************************************

	Change Log:
	$Log:  $

	
**************************************************************************************/
