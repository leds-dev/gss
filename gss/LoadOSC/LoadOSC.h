/* ========================================================================
	FILE NAME:
	LoadOSC.h

	DESCRIPTION:
	This is a header file of LoadOSC.c.

 ========================================================================== */

#ifndef _LOADOSC_H
#define _LOADOSC_H

/* $Id: LoadOSC.h,v 1.7 2000/04/03 16:47:10 jzhao Exp $ */

/* ------------------------
	Headers
*/
/* -----------------------
	Macros
*/

/* Global Variables */

char OSC_key[30][40];


/* ---------------------
	Type Define
*/
	typedef enum {LOAD_FULL_OSC=0, LOAD_DEC_STRIP=1} OSC_UpLoadTypes; 

/*  Function Prototypes */

int CreateUpLoadFile(char *TpltFileName,
	             char *RootFileName,
	             char *UpLoadFileName,
	             char *CatStrucFileName,
	             char *SSCFileName,
	             char *INIFileName,
	             char *Epoch,
	             char *VERSION,
	             AcqSC_type *ASC,
	             IniUpLoad_type *IniVaris,
	             int AllocSizeStrips[],
	             int SelUpLoadType,
	             double DecStepSize,
	             double LowDec,
	             int TM_FOR_MST_FLAG); 

void convert_single_float_to_hex ( char *float_string, char *hex_string ); 

/*************************************************************************************

	Change Log:
	$Log: LoadOSC.h,v $
	Revision 1.7  2000/04/03 16:47:10  jzhao
	Modified for integrating to TCL
	
	Revision 1.6  2000/03/28 22:54:27  jzhao
	Changed format of IniUpLoad_type via Sue's request.
	
	Revision 1.5  2000/02/24 18:38:46  jzhao
	Modified for port to NT
	
	Revision 1.4  2000/02/23 17:31:35  jzhao
	Modified PROC format.
	
	Revision 1.3  2000/02/19 01:19:26  jzhao
	Modified upload Procs format.
	
	Revision 1.2  2000/02/11 22:07:42  jzhao
	Modified to match TM State lables.
	Removed StartAddr Variables from IniUpLoad_type.
	
	Revision 1.1  2000/02/10 21:52:28  jzhao
	Initial Revision.
	

**************************************************************************************/

#endif /*_LOADOSC_H */
