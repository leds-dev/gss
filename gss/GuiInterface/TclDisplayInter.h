/**************************************************************************
  This file defines the interface functions for Tcl to get the relevant
  data to draw the skymap
 **************************************************************************/

#ifndef TCL_DISPLAY_INTER_H
#define TCL_DISPLAY_INTER_H

#include "CISGlobalType.h"

enum {ST1, ST2, ST3, SS, NUM_SENSORS};

/* This function enables to set the direction cosine matrix for the
   specified T and C object */
extern void SetCircularZoneDCM (int zone_id,
				 				double xx_to_body11,
				 				double xx_to_body12,
				 				double xx_to_body13,
				 				double xx_to_body21,
				 				double xx_to_body22,
				 				double xx_to_body23,
				 				double xx_to_body31,
				 				double xx_to_body32,
				 				double xx_to_body33);

/* This routine enables Tcl/TK to set the dir cos matrix between STA
   body frames  */
void SetSTA2Body (double sta2body11,
                  double sta2body12,
                  double sta2body13,
                  double sta2body21,
                  double sta2body22,
                  double sta2body23,
                  double sta2body31,
                  double sta2body32,
                  double sta2body33);

/* This routine enables Tcl/TK to set the dir cos matrix between Sensor
   frame and STA */
void SetSX2STA ( int sensor_id,
                 double sx2sta11,
                 double sx2sta12,
                 double sx2sta13,
                 double sx2sta21,
                 double sx2sta22,
                 double sx2sta23,
                 double sx2sta31,
                 double sx2sta32,
                 double sx2sta33);

/* This routine enables Tcl/TK to set the dir cos matrix between Sensor
   frame and Body */
void SetSX2Body (int sensor_id,
                 double sx2body11,
                 double sx2body12,
                 double sx2body13,
                 double sx2body21,
                 double sx2body22,
                 double sx2body23,
                 double sx2body31,
                 double sx2body32,
                 double sx2body33);

/* This routine returns direction cosine matrix between sensor frame
   and body frame */
DirCosMatrix_type GetSX2BodyDCM (int sensor_id);

/* This routine returns quaternion sun sensor frame and body frame */
QUATERNION_type GetSX2BodyQuat (int sensor_id);

/* This function sets the size of the coverage zone */
extern void SetCircularCoverage (int zone_id, double radius);

/* This function sets the direction cosine matrix for the instrument FOV */
extern void SetInstOptDCM (double inst_opt_to_body11,
				 		   double inst_opt_to_body12,
				 		   double inst_opt_to_body13,
				 		   double inst_opt_to_body21,
				 		   double inst_opt_to_body22,
				 		   double inst_opt_to_body23,
				 		   double inst_opt_to_body31,
				 		   double inst_opt_to_body32,
				 		   double inst_opt_to_body33);

/* This function sets the size of the Instrument Optical FOV */
extern void SetInstOptFOV (double fov_h, double fov_v);

/* Get the direction cosine matrix between sensor frame and body frame */
char *GetSX2BodyDCM_Data (int sensor_id);

/* Get the direction cosine matrix between sensor frame and STA */
char *GetSX2STA_DCM_Data (int sensor_id);

/* Get the attitude information */
char *GetTM_ECI2BodyQuat ();

/* Get the orbit state vector */
char *GetOrbitStateVector ();

char *ST_Stars (int st_id);				/* Stars in ST FOV */

char *ST_Object (int st_id);			/* Celestial objects in ST FOV */

char *ST_NorthAndSouth (int st_id, double arrow_length); /* North South Dir */

char *ST_X (int st_id);					/* Body X direction */

char *ST_Y (int st_id);					/* Body Y direction */

char *ST_Z (int st_id);					/* Body Z direction */

char *ST_Boresight (int st_id);			/* Star tracker boresight RA&DEC */

char *Sky_ST (int st_id);				/* Star tracker FOV in Skymap */

char *Sky_Object (int obj_id, double radius);	/* Celestial object in skymap */

char *Sky_Annulus (int st_id);

char *Sky_RoughBoresight (int st_id, double rough_radius);

char *Sky_Body_Axis (int axis_id);

char *Sky_Circular_Zone (int obj_id);

char *Sky_InstOptFOV ();

#endif /* TCL_DISPLAY_INTER_H */
