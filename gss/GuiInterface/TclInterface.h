/* ========================================================================
	FILE NAME:
		TclInterface.h

	DESCRIPTION:
		This is a header file of GuiInterface.c.
		All Functions in this file will be called by TCL.

 ========================================================================== */

#ifndef _TCLINTERFACE_H
#define _TCLINTERFACE_H

/* $Id: TclInterface.h,v 1.1 2000/04/26 17:11:10 jzhao Exp $ */

/* ---------------------------------------------
	Headers
*/

/* --------------------------------------------
	Function Prototye
*/
void CISInitial();
void SetPwrOn(int power_config);
void ChooseAlignment(int align_flag);
int  GetAlignSel();
void SetCIS_SensorAlignment();
void GetLogFileName( char *FileName );
void GetRoughError(double RoughError);
void GetOSC_Strips(int index, int value);
void GetUploadIni(int MaxEditAreaStars,
                  int MaxOSCStars,
                  int StarAddrState,
                  int StarAddr,
                  int CatAddrState,
                  int CatAddr,
                  int EditAddrState,
                  int EditAddr,
                  int DecStepAddrState, 
                  int DecStepAddr);
void GetUploadKeys(int index, char *key_str);
void SetCISCommitBuff();
void SetSlewCommitBuff();

int CIS_SunHold();
int CIS_RoughKnowledge();
int UpLoadOSC(char *TpltFileName,
	      char *OSCBaseName,
	      char *OSCFileName,
	      char *CatStrucFileName,
	      char *SSCFileName,
	      char *INIFileName,
	      char *Epoch,
	      char *VERSION,
	      double DecMin,
	      double DecMax,
	      double DecStripWidth,
	      int TM_ForMST_Flag); 
char *SlewPlanning(int SlewMode, int SlewSteer, int NumSlewSteps, double SlewAngle,
				 double SlewAxis_x, double SlewAxis_y, double SlewAxis_z);

/* For Tcl to look up for the star information */

int GetASC_Size();
char* LookupStarInfo(int asc_id);

/* For Tcl/TK to get the direction cosine matrix from CIS */
char *GetCIS_ST2BodyDCM (int st_id);

/* For Tcl/TK to get the committed ST direction cosine matrix from CIS */
char *GetCommitST2BodyDCM (int st_id);

/* For Tcl/TK to get the committed Sun Sensor dir cos matrix from CIS */
char *GetCommitSS2BodyDCM ();

/* For Tcl/TK to get the Attitude info from CIS */
char *GetCommitECI2BodyQuat ();

/* For Tcl/TK to get the intermediate ECI2Body quaternion during slew */
char *GetNextStepQuat(int dir);

/* For Tcl/TK to get the Recommented Track */
char *GetCommitRecommendTrack (int st_id, int vt_index);

/* For Tcl/TK to get the valid flag for each Star Trackers */
char *GetCommitValidRefST(int st_id);

/* For Tcl/TK to get the Angular Rate */
char *GetCommitSC_AngVelBody();

/* For Tcl/TK to set the slew initial variables */
void GetSlewIniVars(double Slew_Rate,
					double NadirAxis_x,
					double NadirAxis_y,
                    double NadirAxis_z);


/***************************************************************************
	Change Log:
	$Log: TclInterface.h,v $
	Revision 1.1  2000/04/26 17:11:10  jzhao
	Initial revision.
	

****************************************************************************/

#endif /* _TCLINTERFACE_H */
