/* $Id: GuiInterface.c,v 1.11 2000/05/22 22:36:32 jzhao Exp $ */

/* ================================================================================

	FILE NAME:
		GuiInterface.c

	DESCRIPTION:
		This is interface of CIS, LoadOSC and SlewTool to GUI.

	FUNCTION LIST:

 ================================================================================== */
			
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "stars.h"
#include "catalog.h" 
#include "cis_intf.h"
#include "draw_objs.h" 
#include "ephem_intf.h"
#include "fake_tm.h"

#include <MathFnc.h>
#include <MessageIO.h>
#include <CISGlobal.h>
#include <StarCatalog.h>
#include <StarTracker.h>
#include <IDentifyStars.h>
#include <ChooseOSC.h>
#include <CISExeType.h>
#include <CISExe.h>
#include <LoadOSCType.h>
#include <LoadOSC.h>
#include <Slew.h>
#include <TclInterface.h>
#include <CISInterface.h>
#include <TclDisplayInter.h>

#define	EXTRA_FOV	0.5*DTR;

/* Local global */

static AcqSC_type AcqStarCatalog;
static char *LogFileName;

static IniVariable_type IniVars;
static CISOutput_type CISOutput;
static CISOutput_type CISCommitBuff;
static double InnerRadius[NUM_ST], OuterRadius[NUM_ST];
static double RoughFOVRadius;

static int	SizeOSC_Strips[360];
static IniUpLoad_type IniUploadVars; 
static double SlewRate;
static VECTOR_type NadirFacingAxis;
static SlewOutput_type SlewOutput;
static QUATERNION_type IntermediateQuat;
static SlewOutput_type SlewCommitBuff;

enum{ NON_CISData, CISData };

/* ===============================
	CIS Functions
   =============================== */

/* -------------------------------
CISInitial:			Get Not TM related and "go get" CIS INI Variables 
					from GUI.(GSS.INI)
					Setup Acquisition Star Catalog(ASC) pointer list. 
					Set up SayOut zone radius.
					(Should be called by TCL after GSS initialization 
					and before starting GSS aplications)

Output:	IniVariable_type IniVars(local global).

cis_ini, sun_sensor, sim_data, acs_len: Global variables and set by GUI. 
asc_idx(): GUI c function.
ephem_get_stayout(): ephem_intf c function.
*/

void CISInitial()
{
	int i;

	memcpy(&(IniVars.MAX_ST_MISALIGNMENT_ERROR), &cis_ini, 9*sizeof(double));  

	/* Initialize the sensor alignment */
	SetCIS_SensorAlignment ();

	/* --------------------------------------------------
		Get StayOutZone radius(in degree) for each stayout object 
	---------------------------------------------------- */

	for (i=0; i<MAX_EPHEM_OBJS; i++)
		IniVars.StayOutZones[i].Radius = ephem_get_stayout(i);

	/* Get StayOut radius for SunStruc */
	IniVars.SunStruc.StayoutRadius = IniVars.StayOutZones[GSS_SUN].Radius;

	/* ---------------------------
		Create ASC pointer list
	------------------------------ */
	AcqStarCatalog.NumStars = asc_len;
	for (i=0; i<asc_len; i++)
		AcqStarCatalog.Stars[i] = (StarASC_type *)asc_idx( i );
}

/***************************************************************
  Set the Power On configuration
   1 --- ST 1,2 on
   2 --- ST 2,3 on
   3 --- ST 1,3 on
   4 --- ST 1,2,3 on

 **************************************************************/
void SetPwrOn(int power_config)
{
	IniVars.PWR_Config = power_config;
}

/* Select the alignment: align_flag 0 --- Factory, 1 --- CIS */
void ChooseAlignment(int align_flag)
{
	IniVars.GoodAlign = align_flag;
}

/* return the alignment selection flag 0 --- Factory, 1 --- CIS */
int GetAlignSel()
{
	return (CISOutput.GoodAlign);
}

/* Set the sensor alignment. It is called by CISInitial and Tcl/TK */
void SetCIS_SensorAlignment()
{
	int ST_indx;

	for (ST_indx=ST1; ST_indx<=ST3; ST_indx++)
	{
		IniVars.DCM_ST2BDY[ST_indx] = GetSX2BodyDCM (ST_indx);

		DirCosMatrix2Quaternion(IniVars.DCM_ST2BDY[ST_indx].DCM, 
								IniVars.Quat_ST2BDY[ST_indx].Quat, DOUBLE_DATA);
 	} 

	IniVars.DCM_SS2BDY = GetSX2BodyDCM (SS);
	IniVars.Quat_SS2BDY = GetSX2BodyQuat (SS);
}

/* -------------------------------
GetTMInfor:	Get VT and S/C Attitude information from TM Snap Shot buffer Via GUI. 

Output: VTBuffer_type VTBuff[][NUM_VT], IniVariable_type *IniVars

snapshot:	Global variable and set by GUI.
*/

void GetTMInfor(VTBuffer_type VTBuff[][NUM_VT], IniVariable_type *CISIni)
{
	int ST_indx, VT_indx;

	for (ST_indx = 0; ST_indx < NUM_ST; ST_indx++ )
	{
		for (VT_indx = 0; VT_indx < NUM_VT; VT_indx++ )
		{
			VTBuff[ST_indx][VT_indx].HPos = 
									snapshot.st[ST_indx].vt[VT_indx].hv.h;
			VTBuff[ST_indx][VT_indx].VPos = 
									snapshot.st[ST_indx].vt[VT_indx].hv.v;
			VTBuff[ST_indx][VT_indx].Magnitude = 
									snapshot.st[ST_indx].vt[VT_indx].mag;
			VTBuff[ST_indx][VT_indx].Valid = 
									snapshot.st[ST_indx].vt[VT_indx].valid;
			VTBuff[ST_indx][VT_indx].Tracked = 
									snapshot.st[ST_indx].vt[VT_indx].tracked;
			VTBuff[ST_indx][VT_indx].UserSel = cis_user[ST_indx][VT_indx];
		}
	}
	memcpy(&(CISIni->Quat_ECI2BDY), &snapshot.eci2body.quat, sizeof(QUAT));
	memcpy(&(CISIni->DCM_ECI2BDY), &snapshot.eci2body.dcm, sizeof(DIM33));
}

/* ---------------------------------
GetRoughError:	Get RoughKnowledge Error form GUI.
				Called by TCL.
*/
void GetRoughError(double RoughError)
{
	IniVars.MAX_ROUGH_KNOWLEDGE_ERROR = RoughError; 
}

/* ---------------------------------
	Get Log file name from GUI(Called by TCL).

Input:	char *FileName.
*/
void GetLogFileName(char *FileName) 
{ 
	LogFileName = FileName;		
}

/* ----------------------------------
PutCISOutput:	Put CIS output data to GUI interface.

Input:	CISOutput_type *CISOut.
		int mode, a filter for the CIS screen.  Need to print out to the CIS
			screen only in one case but not the other.  Mode is 1 for 
			Rough Attitude 2 for Sun Hold.

snapshot, cis_data.msg:	Global variables and set by GUI.
*/

void PutCISOutput(CISOutput_type *CISOut, int mode)
{
	int ST_indx, VT_indx;
	int SpreadOK = TRUE;
	char s[200];
	
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		if (CISOut->ValidRefST[ST_indx] == 1)
		   sprintf(s, "\n\tST %1d used to generate CIS Results", ST_indx+1);
		else
		   sprintf(s, "\n\tST %1d not used to generate CIS Results", ST_indx+1);
		strcat(cis_data.msg, s);	
	}

	/* List of CIS Identified Stars(GUI display only) */
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		for (VT_indx=0; VT_indx<NUM_VT; VT_indx++)
		{
			snapshot.st[ST_indx].vt[VT_indx].cis_bss_id = CISOut->IdentifiedStars[ST_indx].Stars[VT_indx].BSS_ID;
			snapshot.st[ST_indx].vt[VT_indx].cis_osc_flag = CISOut->IdentifiedStars[ST_indx].Stars[VT_indx].OSC_ID;
		}

	/* Display metrics if partial or total success */
	if ((CISOut->Status == PARTIAL_SUCCESS) || (CISOut->Status == TOTAL_SUCCESS))
	{
		
	/* CIS Calculated Attitude Error Metric(dump to message string only) */
		strcat(cis_data.msg, "\nStar Tracker Boresight Attitude Uncertainty");
		for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		{
			if (CISOut->ValidRefST[ST_indx] == 1)
			{
			   sprintf(s, "\n\tST %1d Uncertainty = %9.3f (ArcSec)", ST_indx+1, CISOut->AttErrorMetric[ST_indx]);   
			   strcat(cis_data.msg, s);	
			}
		}

	/* CIS Calculated Star Spread Metric */
		strcat(cis_data.msg, "\nStar Spread");
		for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		{
			if (CISOut->ValidRefST[ST_indx] == 1)
			{
			   sprintf(s, "\n\tST %1d Spread = %9.3f (Deg)", ST_indx+1, CISOut->Spread[ST_indx]*RTD);
			   strcat(cis_data.msg, s);
			   if (CISOut->Spread[ST_indx] < SPREAD_LIMIT)
			      SpreadOK = FALSE;
			}
		}
	
		if (CISOut->Status == TOTAL_SUCCESS)
		{
			if (SpreadOK == TRUE)
			{
				sprintf(s, "\n\tStar Tracker Spreads all exceed threshold of %9.3f Degrees", SPREAD_LIMIT*RTD);
				strcat(cis_data.msg, s);
				sprintf(s, "\n\tCIS results adequate for upload");
				strcat(cis_data.msg, s);
			}
			else
			{
				sprintf(s, "\n\tStar Tracker Spreads do NOT all exceed threshold of %9.3f Degrees", SPREAD_LIMIT*RTD);
				strcat(cis_data.msg, s);
				sprintf(s, "\n\tCIS results NOT adequate for upload");
				strcat(cis_data.msg, s);
			}
		}
	
	/* Sensor alignment shifts computed by CIS */
		strcat(cis_data.msg, "\nSensor Alignment Shift");

  	/* Boresight alignment shift for sun sensor computed by CIS */
		if (mode == 2)
		{
			sprintf(s, "\n\tSun Sensor Boresight Transverse Shift = %9.3f (ArcSec)", CISOut->AlignBoresightErrSS);   
			strcat(cis_data.msg, s);	
		}

   	/* Boresight alignment shift for star trackers computed by CIS */
		for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		{
			if (CISOut->ValidRefST[ST_indx] == 1)
			{
			   sprintf(s, "\n\tST %1d Boresight Transverse Shift = %9.3f (ArcSec)", ST_indx+1, CISOut->AlignBoresightErrST[ST_indx]);   
			   strcat(cis_data.msg, s);	
			}
		}
	
	/* Boresight rotation shift for star trackers computed by CIS */
		for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		{
			if (CISOut->ValidRefST[ST_indx] == 1)
			{
			   sprintf(s, "\n\tST %1d Boresight Rotational Shift = %9.3f (ArcSec)", ST_indx+1, CISOut->AlignRotateErrST[ST_indx]);   
			   strcat(cis_data.msg, s);	
			}
		}
	
	/* CIS recommended Slew (dump to message string only) */
		if (mode == 2)
		{
			strcat(cis_data.msg, "\nRecommended Sunline Slews");
			if (CISOut->Status == TOTAL_SUCCESS)
			{
				sprintf(s, "\n\tUpright Slew Angle = %9.3f (Deg)", CISOut->UpRightSlewAngle);
				strcat(cis_data.msg, s);
				sprintf(s, "\n\tInverted Slew Angle = %9.3f (Deg)", CISOut->InvertedSlewAngle);
				strcat(cis_data.msg, s);
			}
			if (CISOut->Status == PARTIAL_SUCCESS)
			{
				sprintf(s, "\n\tPositive Slew Angle = %9.3f (Deg)", CISOut->PlusSlewAngle);
				strcat(cis_data.msg, s);
				sprintf(s, "\n\tNegative Slew Angle = %9.3f (Deg)", CISOut->MinusSlewAngle);
				strcat(cis_data.msg, s);
			}
		}
		
	}
	
	/* CIS processing Status(dump to message string only) */
	strcat(cis_data.msg, "\n");
	strcat(cis_data.msg, GetMessage(CISOut->Status, "in CISEXE")); 
	strcat(cis_data.msg, "\n");
}

/* -------------------------------
CIS_SunHold: To active CIS Sun Hold Mode from GUI
*/

int CIS_SunHold()
{
	int stat = 0;
	int i;

	int 	STCalibON = TRUE;
	int		SSCalibON = TRUE;
	int		CIS_MODE = SUN_HOLD_MODE;
	VTBuffer_type VTBuffer[NUM_ST][NUM_VT];

	/* Get from GUI */
	GetTMInfor(VTBuffer, &IniVars);

	/* Get center ECI for each stayout objects */
	for (i=0; i<MAX_EPHEM_OBJS; i++)
	{
		memcpy(&(IniVars.StayOutZones[i].ECI), ephem_get_eci_pos(i), 
													sizeof(VECTOR_type));
	}
	/* Get Dist. from SC & Velocity & ECI for SunStruc */
	IniVars.SunStruc.DistFromSC = ephem_get_distSC(GSS_SUN);
	memcpy(&(IniVars.SunStruc.ECI), &(IniVars.StayOutZones[GSS_SUN].ECI), 
													sizeof(VECTOR_type));
	memcpy(&(IniVars.SunStruc.Velocity), ephem_get_eci_vel(GSS_SUN),
													sizeof(VECTOR_type));
	/* Run CIS */
	stat = CISEXE(&AcqStarCatalog,   
					LogFileName,
		   			&IniVars,
		   			VTBuffer,
		   			CIS_MODE,
		   			STCalibON,
		   			SSCalibON,
					&CISOutput);

	/* Put to GUI */
	strcpy(cis_data.msg, "\n\nCIS_SunHold");
	PutCISOutput(&CISOutput, 2);
	
	return(stat);
}

/* -------------------------------
CIS_RoughKnowledge: To active CIS RoughKnowledge Mode from GUI
*/

int CIS_RoughKnowledge()
{
	int stat = 0;
	int i;

	int 	STCalibON = TRUE;
	int		SSCalibON = FALSE; 
	int		CIS_MODE = ROUGH_KNOWLEDGE_MODE; 
	VTBuffer_type VTBuffer[NUM_ST][NUM_VT];

	/* Get from GUI */
	GetTMInfor(VTBuffer, &IniVars);

	/* Get center ECI for each stayout objects */
	for (i=0; i<MAX_EPHEM_OBJS; i++)
	{
		memcpy(&(IniVars.StayOutZones[i].ECI), ephem_get_eci_pos(i), 
													sizeof(VECTOR_type));
	}
	/* Get Dist. from SC & Velocity & ECI for SunStruc */
	IniVars.SunStruc.DistFromSC = ephem_get_distSC(GSS_SUN);
	memcpy(&(IniVars.SunStruc.ECI), &(IniVars.StayOutZones[GSS_SUN].ECI), 
													sizeof(VECTOR_type));
	memcpy(&(IniVars.SunStruc.Velocity), ephem_get_eci_vel(GSS_SUN),
													sizeof(VECTOR_type));
	/* Run CIS */
	stat = CISEXE(&AcqStarCatalog,   
					LogFileName,
		   			&IniVars,
		   			VTBuffer,
		   			CIS_MODE,
		   			STCalibON,
		   			SSCalibON,
					&CISOutput);

	/* Put to GUI */
	strcpy(cis_data.msg, "\n\nCIS_RoughKnowledge");
	PutCISOutput(&CISOutput, 1);

	return(stat);
}

/* ---------------------------------
SetCISCommit: Set CIS commit buffer. Called by GUI.
			  Put committed results from CISOuput to CISCommitBuff.
*/

void SetCISCommitBuff()
{
	memcpy(&CISCommitBuff, &CISOutput, sizeof(CISOutput_type));
}

/* ---------------------------------
GetCISCommitBuff:	Get CIS commit buff. called by GUI.
Output:	CISOuput_type CISResults.
*/

void GetCISCommitBuff(CISOutput_type *CISResults)
{
	CISResults = &CISCommitBuff;
}

/* ---------------------------------
CIS_SunHoldAnnulus: Calculates Radius of annulus for each ST in SunHold mode.
Output: (Leave at local for now)
	InnerRadius:	Array[NUM_ST] of double.
	OuterRadius:	Array[NUM_ST} of double.
*/

void CIS_SunHoldAnnulus()
{
	GetSunHoldAnnulus(&IniVars, InnerRadius, OuterRadius);
}

/* ---------------------------------
CIS_RoughKnowledgeRadius: Calculates Radius of RoughFOV for ST 
							in RoughKnowledge mode.
Output: (leave at local for now)
	RoughFOVRadius:	double.(All STs have same RoughFOV Radius)
*/

void CIS_RoughKnowledgeRadius()
{
	GetRoughKnowledgeRadius(&IniVars, &RoughFOVRadius);
}

/* ============================== 
	OSC Upload 
   ============================== */

/* ----------------------------------
GetOSC_Strips: Get allocated width for each Dec Strip(Called by TCL).
*/

void GetOSC_Strips( int DecStripIndex, int Alloc_Stars )
{
	SizeOSC_Strips[DecStripIndex] = Alloc_Stars;
}


/* ----------------------------------
GetUploadKeys: Get the Upload Template keys used for search and replace (Called by TCL).
*/

void GetUploadKeys( int index, char *key_str )
{
	if (strlen(key_str) > 0) {
	    strcpy(OSC_key[index], key_str);
	    
	}
}

/* ----------------------------------
GetUploadIni:	Get Upload ini Variables from GUI(Called by GUI).
*/

void GetUploadIni(int MaxEditAreaStars,
				  int MaxOSCStars,
				  int StarAddrState,
				  int StarAddr,
				  int CatAddrState,
				  int CatAddr,
				  int EditAddrState,
				  int EditAddr,
				  int DecStepAddrState,
				  int DecStepAddr)
{
        IniUploadVars.MAX_EDIT_AREA_STARS = MaxEditAreaStars;
		IniUploadVars.MAX_OSC_STARS = MaxOSCStars;
		IniUploadVars.StarAreaStartAddrState = StarAddrState;
		IniUploadVars.StarAreaStartAddr = StarAddr;
		IniUploadVars.CatAreaStartAddrState = CatAddrState;
		IniUploadVars.CatAreaStartAddr = CatAddr;
		IniUploadVars.EditAreaStartAddrState = EditAddrState;
		IniUploadVars.EditAreaStartAddr = EditAddr;
		IniUploadVars.DecStepStartAddrState = DecStepAddrState;
		IniUploadVars.DecStepStartAddr = DecStepAddr;
}

/* ----------------------------------
UpLoadOSC:	Create upload/update OSC proc.
			TCL need to call GetOSC_Strips and GetIniUpload before call UpLoadOSC
*/

int UpLoadOSC(char *TpltFileName,
	      char *OSCBaseName, 
	      char *OSCFileName, 
	      char *CatStrucFileName, 
	      char *SSCFileName, 
	      char *INIFileName, 
	      char *Epoch, 
	      char *VERSION,
	      double DecMin, 
	      double DecMax,
	      double DecStripWidth,
	      int TM_ForMST_Flag) 
{
	int stat = 0;
	int j;
	int UserSelUpLoadType;

	/* Determin UpLoad Type */
	if ((DecMax - DecMin) == 180.0)	UserSelUpLoadType = LOAD_FULL_OSC;	
	else							UserSelUpLoadType = LOAD_DEC_STRIP;

	/* ---------------------------
		Create upload file
	------------------------------ */
	stat = CreateUpLoadFile(TpltFileName,
	                        OSCBaseName,
	                        OSCFileName,
	                        CatStrucFileName,
	                        SSCFileName,
	                        INIFileName,
	                        Epoch,
	                        VERSION, 
	                        &AcqStarCatalog,
	                        &IniUploadVars,
	                        SizeOSC_Strips,
	                        UserSelUpLoadType,
	                        DecStripWidth,
	                        DecMin,
	                        TM_ForMST_Flag);
	if (stat < 0)
		fprintf(stderr, "GuiInterface: UpLoad Error\n");

	return(stat);
}

/* ============================= 
	Slew Planning
   ============================= */

/* ----------------------------
GetSlewIniVars:	Get slew ini variables from GUI.
				Called by TCL.
*/
void GetSlewIniVars(double Slew_Rate, 
					double NadirAxis_x,
					double NadirAxis_y,
					double NadirAxis_z)
{
	SlewRate = Slew_Rate;
	NadirFacingAxis.Vec[0] = NadirAxis_x;
	NadirFacingAxis.Vec[1] = NadirAxis_y;
	NadirFacingAxis.Vec[2] = NadirAxis_z;
}

/* ----------------------------
SlewPlanning: Implement Slew Planning functions. Called by TCL.
Input:
		SlewMode: int
		SlewSteer: int
		NumSlewSteps:	int
		SlewAngle:	double
		SlewAxis_x:	double
		SlewAxis_y:	double
		SlewAxis_z:	double

Output: SlewOutput:	SlewOutput_type
		character string for SlewAxis and Slew angle
*/

char *SlewPlanning(int SlewMode,
				 int SlewSteer,
				 int NumSlewSteps,
				 double SlewAngle,
				 double SlewAxis_x,
				 double SlewAxis_y,
				 double SlewAxis_z)
{
	char buf[4*40];
	VECTOR_type SlewAxis;
	VECTOR_type EarthPosECI;
	VECTOR_type SunPosECI;
	VECTOR_type EarthVelECI;
	VECTOR_type SunVelECI;
	VECTOR_type EarthLinVelECI;
	VECTOR_type SunLinVelECI;
	double EarthDist;
	double SunDist;
    QUATERNION_type Quat_Ref2BDY;
    QUATERNION_type Quat_Initial2FinalBDY;
    DirCosMatrix_type DCM_SS2BDY;
	int status;
	int i;

	SlewAxis.Vec[0] = SlewAxis_x;
	SlewAxis.Vec[1] = SlewAxis_y;
	SlewAxis.Vec[2] = SlewAxis_z;

	/* Initialize the initial quaternion and its inverse */
	memcpy(&Quat_Ref2BDY, &(sim_data.eci2body.quat), sizeof(QUAT));

	/* Initialize the intermediate quaternion in between */
	memcpy(&IntermediateQuat, &(sim_data.eci2body.quat), sizeof(QUAT));

	DCM_SS2BDY = GetSX2BodyDCM(SS);

	/* Get the Earth unit vector and angular rate in ECI */
	EarthDist = ephem_get_distSC(GSS_EARTH);
	memcpy(&EarthPosECI, ephem_get_eci_pos(GSS_EARTH), sizeof(VECTOR_type));
	memcpy(&EarthLinVelECI, ephem_get_eci_vel(GSS_EARTH), sizeof(VECTOR_type));
    CrossProduct (EarthPosECI.Vec, EarthLinVelECI.Vec,
          EarthVelECI.Vec, DOUBLE_DATA);
    for (i=0; i<3; i++)
      EarthVelECI.Vec[i] /= EarthDist;

	/* Get the Sun unit vector and angular rate in ECI */
	SunDist = ephem_get_distSC(GSS_SUN);
	memcpy(&SunPosECI, ephem_get_eci_pos(GSS_SUN), sizeof(VECTOR_type));
	memcpy(&SunLinVelECI, ephem_get_eci_vel(GSS_SUN), sizeof(VECTOR_type));
    CrossProduct (SunPosECI.Vec, SunLinVelECI.Vec,
          SunVelECI.Vec, DOUBLE_DATA);
    for (i=0; i<3; i++)
      SunVelECI.Vec[i] /= SunDist;

	if ( ! SlewEXE( LogFileName,
            		SlewMode,
            		SlewSteer,
              		NumSlewSteps,
              		SlewRate*DTR,
              		SlewAngle*DTR,
              		SlewAxis,
					EarthPosECI,
					SunPosECI,
					EarthVelECI,
					SunVelECI,
              		NadirFacingAxis,
              		Quat_Ref2BDY,
              		DCM_SS2BDY,
              		&(SlewOutput.Quat_Final),
              		&(Quat_Initial2FinalBDY),
              		&(SlewOutput.Quat_Step_Post_Mult),
              		&(SlewOutput.Quat_Step_Pre_Mult) ))
	{

		Quat_Q2AxisAngle (Quat_Initial2FinalBDY,
							  &SlewAxis,
							  &SlewAngle);
		SlewAngle *= RTD;

		sprintf (buf, "%15.12f %15.12f %15.12f %15.10f",
				 SlewAxis.Vec[0],
				 SlewAxis.Vec[1],
				 SlewAxis.Vec[2],
				 SlewAngle);
				 
	}
	else
		sprintf (buf, "Error: Slew Planing failed.\n");

	return buf;
}

/* --------------------------------------
SetSlewCommitBuff:	Set Slew commit Buffer. Called by TCL.

*/

void SetSlewCommitBuff()
{
	memcpy(&SlewCommitBuff, &SlewOutput, sizeof(SlewOutput_type));
}

/* --------------------------------------
GetSlewCommitBuff: pass out the pointer of SlewCommitBuffer.
*/

void GetSlewCommitBuf(SlewOutput_type *SlewResults)
{
	SlewResults = &SlewCommitBuff;
}

/*************************************************************************
 This function provides the information about the stars in field of view
 of the specified star tracker
 *************************************************************************/
void StarsInTrackerFOV(int st_id, DirCosMatrix_type *dcm_eci2st,
					   TrackerFOV_HV_type *star_info)
{
	FOVSize_type	FOVSize;
	double			st_boresight[3] = {0.0, 0.0, 1.0};

	FOVSize.HMax = H_MAX_ANG + EXTRA_FOV;
	FOVSize.HMin = H_MIN_ANG - EXTRA_FOV;
	FOVSize.VMax = V_MAX_ANG + EXTRA_FOV;
	FOVSize.VMin = V_MIN_ANG - EXTRA_FOV;

	TrackerFOV (&AcqStarCatalog, (VECTOR_type *) st_boresight, dcm_eci2st,
                &FOVSize, star_info, st_id+1, ASCInFOV);
}

/****************************************************************************
 This function provides the inner and outer radius for displaying the annulus
 while holding the sun for the specified star tracker
 ****************************************************************************/

void GetInnerAndOuterRadius (int st_id, double *inner_r, double *outer_r)
{
	double inner_radius[NUM_ST], outer_radius[NUM_ST];

	GetSunHoldAnnulus (&IniVars, inner_radius, outer_radius);

	*inner_r = inner_radius[st_id];
	*outer_r = outer_radius[st_id];
}

char *GetCIS_ST2BodyDCM (int st_id)
{
	static char buf[9*40];

	st_id --; /* Map ST id  1, 2, 3  to C index 0, 1, 2 */
	sprintf (buf,
             "%15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f",
			 CISOutput.DCM_ST2BDY[st_id].DCM[0],
			 CISOutput.DCM_ST2BDY[st_id].DCM[1],
			 CISOutput.DCM_ST2BDY[st_id].DCM[2],
			 CISOutput.DCM_ST2BDY[st_id].DCM[3],
			 CISOutput.DCM_ST2BDY[st_id].DCM[4],
			 CISOutput.DCM_ST2BDY[st_id].DCM[5],
			 CISOutput.DCM_ST2BDY[st_id].DCM[6],
			 CISOutput.DCM_ST2BDY[st_id].DCM[7],
			 CISOutput.DCM_ST2BDY[st_id].DCM[8]);

	return buf;
}

char *GetCommitST2BodyDCM (int st_id)
{
	static char buf[9*40];

	st_id --; /* Map ST id  1, 2, 3  to C index 0, 1, 2 */
	sprintf (buf,
             "%15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f",
			 CISCommitBuff.DCM_ST2BDY[st_id].DCM[0],
			 CISCommitBuff.DCM_ST2BDY[st_id].DCM[1],
			 CISCommitBuff.DCM_ST2BDY[st_id].DCM[2],
			 CISCommitBuff.DCM_ST2BDY[st_id].DCM[3],
			 CISCommitBuff.DCM_ST2BDY[st_id].DCM[4],
			 CISCommitBuff.DCM_ST2BDY[st_id].DCM[5],
			 CISCommitBuff.DCM_ST2BDY[st_id].DCM[6],
			 CISCommitBuff.DCM_ST2BDY[st_id].DCM[7],
			 CISCommitBuff.DCM_ST2BDY[st_id].DCM[8]);

	return buf;
}

char *GetCommitSS2BodyDCM ()
{
	static char buf[9*40];

	sprintf (buf,
             "%15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f",
			 CISCommitBuff.DCM_SS2BDY.DCM[0],
			 CISCommitBuff.DCM_SS2BDY.DCM[1],
			 CISCommitBuff.DCM_SS2BDY.DCM[2],
			 CISCommitBuff.DCM_SS2BDY.DCM[3],
			 CISCommitBuff.DCM_SS2BDY.DCM[4],
			 CISCommitBuff.DCM_SS2BDY.DCM[5],
			 CISCommitBuff.DCM_SS2BDY.DCM[6],
			 CISCommitBuff.DCM_SS2BDY.DCM[7],
			 CISCommitBuff.DCM_SS2BDY.DCM[8]);

	return buf;
}

char *GetCommitECI2BodyQuat ()
{
	static char buf[4*40];

	sprintf (buf,
             "%15.12f %15.12f %15.12f %15.12f",
			 CISCommitBuff.Quat_ECI2BDY.Quat[0],
			 CISCommitBuff.Quat_ECI2BDY.Quat[1],
			 CISCommitBuff.Quat_ECI2BDY.Quat[2],
			 CISCommitBuff.Quat_ECI2BDY.Quat[3]);

	return buf;
}

char *GetCommitRecommendTrack (int st_id, int vt_index)
{
	static char buf[4*40];

	st_id --; /* map the star tracker id 1, 2, 3 to C index 0, 1, 2 */

	vt_index --; /* map the VT index from 1,2, ... 5 to 0, 1, ... 4 */

	sprintf (buf,
             "%15.12f %15.12f %15.12f %10d",
			 CISCommitBuff.RecommendTrack[st_id].VTs[vt_index].HPos,
			 CISCommitBuff.RecommendTrack[st_id].VTs[vt_index].VPos,
			 CISCommitBuff.RecommendTrack[st_id].VTs[vt_index].Magnitude,
			 CISCommitBuff.RecommendTrack[st_id].VTs[vt_index].TrackAction);

	return buf;
}

char *GetCommitValidRefST(int st_id)
{
	static char buf[40];

	st_id --; /* map the star tracker id 1, 2, 3 to C index 0, 1, 2 */

	sprintf (buf, "%10d", CISCommitBuff.ValidRefST[st_id]);

	return buf;
}

char *GetCommitSC_AngVelBody()
{
	static char buf[3*40];

	sprintf (buf, "%15.12f %15.12f %15.12f",
			 CISCommitBuff.SC_Angular_VelocityBDY.Vec[0],
			 CISCommitBuff.SC_Angular_VelocityBDY.Vec[1],
			 CISCommitBuff.SC_Angular_VelocityBDY.Vec[2]);

	return buf;
}

/* This function provides the intermediate quaternion for Tcl/TK to display
   a slew, dir = +1 for slew forward, dir = -1 for slew backward */
char *GetNextStepQuat(int dir)
{
	static char buf[4*40];
	QUATERNION_type	delta_q_post;
	QUATERNION_type	delta_q_pre;

	if (dir == 1) /* Slew forward */
	{
		delta_q_post = SlewOutput.Quat_Step_Post_Mult;
		delta_q_pre  = SlewOutput.Quat_Step_Pre_Mult;
	}
	else if (dir == -1) /* Slew backward */
	{
		delta_q_post.Quat[0] = - SlewOutput.Quat_Step_Post_Mult.Quat[0];
		delta_q_post.Quat[1] = - SlewOutput.Quat_Step_Post_Mult.Quat[1];
		delta_q_post.Quat[2] = - SlewOutput.Quat_Step_Post_Mult.Quat[2];
		delta_q_post.Quat[3] =   SlewOutput.Quat_Step_Post_Mult.Quat[3];
		delta_q_pre.Quat[0]  = - SlewOutput.Quat_Step_Pre_Mult.Quat[0];
		delta_q_pre.Quat[1]  = - SlewOutput.Quat_Step_Pre_Mult.Quat[1];
		delta_q_pre.Quat[2]  = - SlewOutput.Quat_Step_Pre_Mult.Quat[2];
		delta_q_pre.Quat[3]  =   SlewOutput.Quat_Step_Pre_Mult.Quat[3];
	}
	else
	{
		sprintf (buf, "Error: Unknown direction for slew\n");
		return buf;
	}

	QuaternionMult (delta_q_pre.Quat, IntermediateQuat.Quat,
					IntermediateQuat.Quat, DOUBLE_DATA);

	QuaternionMult (IntermediateQuat.Quat, delta_q_post.Quat,
					IntermediateQuat.Quat, DOUBLE_DATA);

	sprintf (buf, "%15.12f %15.12f %15.12f %15.12f",
			  IntermediateQuat.Quat[0],
			  IntermediateQuat.Quat[1],
			  IntermediateQuat.Quat[2],
			  IntermediateQuat.Quat[3]);

	return buf;
	
}

/***********************************************************************************

	Change Log:
	$Log: GuiInterface.c,v $
	Revision 1.11  2000/05/22 22:36:32  jzhao
	Calculate InVars.UQAT_ST2BDY[].Quat from DCM. Don't depend on GUI.
	
	Revision 1.10  2000/05/22 15:34:34  jzhao
	Modified for integration.
	
	Revision 1.9  2000/04/05 16:59:13  jzhao
	Added functions CIS_SunHoldAnnulus and CIS_RoughKnowledgeRadius.
	
	Revision 1.8  2000/04/03 16:45:47  jzhao
	Modified for integrating to TCL.
	
	Revision 1.7  2000/03/30 00:16:10  jzhao
	Modified GetLogFileName function for TCL to use.
	
	Revision 1.6  2000/03/29 17:57:52  jzhao
	Set External Variables for TCL to use.
	
	Revision 1.5  2000/03/28 22:58:03  jzhao
	Changed format of CISOutput.
	Modified for integrating to GUI.
	
	Revision 1.4  2000/02/25 21:39:25  jzhao
	Get Log file name from GUI.
	
	Revision 1.3  2000/02/24 19:50:15  jzhao
	Modified for port to NT
	
	Revision 1.2  2000/02/24 17:58:44  jzhao
	Modified for port to NT
	
	Revision 1.1  2000/02/23 19:10:50  jzhao
	Initial revision.
	

************************************************************************************/
