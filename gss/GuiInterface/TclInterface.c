#include <string.h>
#include <TclInterface.h>
#include <catalog.h>

/* This function returns the total number of stars in the ASC */
int GetASC_Size()
{
	return (asc_len);
}

/*************************************************************************
   This function provides star information for the id (ACS index) passed.
   The information is listed in the order as:

   Right Ascention
   Declination
   Magnitude for Skymap
   Magnitude for ST1
   Magnitude for ST2
   Magnitude for ST3
   Sky 2000 ID
   Boolean Flag specifying whether it is an OSC
   BSS ID
 *************************************************************************/
 
char* LookupStarInfo(int asc_id)
{
	static char star_info[150];
	STAR *star_pt = asc_idx(asc_id);

	sprintf (star_info,
             "%10.6f %10.6f %10.6f %10.6f %10.6f %10.6f %10d %10d %10d",
			 star_pt->pos.ra.ra,		/* Right Ascension of the Star */
			 star_pt->pos.ra.dec,		/* Declination of the Star */
			 star_pt->mag[0],			/* Magnitude for Skymap */
			 star_pt->mag[1],			/* Magnitude for ST 1	*/
			 star_pt->mag[2],			/* Magnitude for ST 2	*/
			 star_pt->mag[3],			/* Magnitude for ST 3	*/
			 star_pt->sky2000,			/* SKY2000 Id for the Star	*/
			 star_pt->osc_id,			/* Flag 0-ASC only; 1-OSC	*/
			 star_pt->bss_id);			/* BSS Id 1-65535	*/

	return (star_info);
}
