#include <math.h>
#include <string.h>

#include "stars.h"
#include "MathDef.h"
#include "CISInterface.h"
#include "EPHGlobal.h"
#include "MathFnc.h"
#include "WrapAround.h"
#include "PreparePlotData.h"
#include "TclDisplayInter.h"

#define ST_FOV_H			8.0*DTR /* 8 deg */
#define ST_FOV_V			8.0*DTR /* 8 deg */

#define Eci2Bdy		&sim_data.eci2body.dcm
#define Bdy2Eci		&sim_data.eci2body.dcm_r

enum {NOT_FLIPPED, FLIPPED};
enum {
	CDA_TM_AT_CDA_AFT,
	CMD_AT_CDA_AFT,
	CMD_AT_DSN_AFT,
	DSN_TM_AT_CDA_AFT,
	DSN_TM_AT_DSN_AFT,
	DSN_TM_AT_CDA_FWD,
	DSN_TM_AT_DSN_FWD,
	CDA_TM_AT_CDA_FWD,
	CMD_AT_CDA_FWD,
	CMD_AT_DSN_FWD,
	INSTRUMENT_COOLER,
	SUN_SENSOR,
	NUM_OF_CIRCULAR_ZONE};

typedef struct {
	ROTATION xx2body;           /* rotation */
	double radius;              /* coverage dimensions */
} CircularZone;

static CircularZone CircularZones[NUM_OF_CIRCULAR_ZONE];
static ROTATION STA2Body;
static ROTATION SX2STA[NUM_SENSORS];
static ROTATION SX2Body[NUM_SENSORS];
static ROTATION SX2ECI[NUM_SENSORS];
static ROTATION InstOptFOV;
static double InstFov_H;
static double InstFov_V;

/* This routine enables Tcl/TK to set the direction cosine matrix for the
   specified object */
void SetCircularZoneDCM (int zone_id,
                         double xx_to_body11,
                         double xx_to_body12,
                         double xx_to_body13,
                         double xx_to_body21,
                         double xx_to_body22,
                         double xx_to_body23,
                         double xx_to_body31,
                         double xx_to_body32,
                         double xx_to_body33)
{
	CircularZones[zone_id].xx2body.dcm[0][0] = xx_to_body11;
	CircularZones[zone_id].xx2body.dcm[0][1] = xx_to_body12;
	CircularZones[zone_id].xx2body.dcm[0][2] = xx_to_body13;
	CircularZones[zone_id].xx2body.dcm[1][0] = xx_to_body21;
	CircularZones[zone_id].xx2body.dcm[1][1] = xx_to_body22;
	CircularZones[zone_id].xx2body.dcm[1][2] = xx_to_body23;
	CircularZones[zone_id].xx2body.dcm[2][0] = xx_to_body31;
	CircularZones[zone_id].xx2body.dcm[2][1] = xx_to_body32;
	CircularZones[zone_id].xx2body.dcm[2][2] = xx_to_body33;

	/* Syncronize the quaternion and the inverse */
	rot_set_dcm (&(CircularZones[zone_id].xx2body),
				 &(CircularZones[zone_id].xx2body.dcm));
}

/* This routine enables Tcl/TK to set the dir cos matrix between STA
   body frames  */
void SetSTA2Body (double sta2body11,
                  double sta2body12,
                  double sta2body13,
                  double sta2body21,
                  double sta2body22,
                  double sta2body23,
                  double sta2body31,
                  double sta2body32,
                  double sta2body33)
{
	STA2Body.dcm[0][0] = sta2body11;
	STA2Body.dcm[0][1] = sta2body12;
	STA2Body.dcm[0][2] = sta2body13;
	STA2Body.dcm[1][0] = sta2body21;
	STA2Body.dcm[1][1] = sta2body22;
	STA2Body.dcm[1][2] = sta2body23;
	STA2Body.dcm[2][0] = sta2body31;
	STA2Body.dcm[2][1] = sta2body32;
	STA2Body.dcm[2][2] = sta2body33;

	/* Synchronize the quaternion and the inverse */
	rot_set_dcm (&STA2Body, &(STA2Body.dcm));
}

/* This routine enables Tcl/TK to set the dir cos matrix between Sensor
   frame and STA */
void SetSX2STA ( int sensor_id,
                 double sx2sta11,
                 double sx2sta12,
                 double sx2sta13,
                 double sx2sta21,
                 double sx2sta22,
                 double sx2sta23,
                 double sx2sta31,
                 double sx2sta32,
                 double sx2sta33)
{
	SX2STA[sensor_id].dcm[0][0] = sx2sta11;
	SX2STA[sensor_id].dcm[0][1] = sx2sta12;
	SX2STA[sensor_id].dcm[0][2] = sx2sta13;
	SX2STA[sensor_id].dcm[1][0] = sx2sta21;
	SX2STA[sensor_id].dcm[1][1] = sx2sta22;
	SX2STA[sensor_id].dcm[1][2] = sx2sta23;
	SX2STA[sensor_id].dcm[2][0] = sx2sta31;
	SX2STA[sensor_id].dcm[2][1] = sx2sta32;
	SX2STA[sensor_id].dcm[2][2] = sx2sta33;

	/* Synchronize the quaternion and the inverse */
	rot_set_dcm (&SX2STA[sensor_id], &(SX2STA[sensor_id].dcm));

	/* Update the SX2Body */
	MatrixMult (&STA2Body.dcm, &SX2STA[sensor_id].dcm,
                &SX2Body[sensor_id].dcm, 3, 3, 3, DOUBLE_DATA);

	/* Synchronize the quaternion and the inverse */
	rot_set_dcm (&SX2Body[sensor_id], &(SX2Body[sensor_id].dcm));
}

/* This routine enables Tcl/TK to set the dir cos matrix between Sensor
   frame and Body */
void SetSX2Body (int sensor_id,
                 double sx2body11,
                 double sx2body12,
                 double sx2body13,
                 double sx2body21,
                 double sx2body22,
                 double sx2body23,
                 double sx2body31,
                 double sx2body32,
                 double sx2body33)
{
	SX2Body[sensor_id].dcm[0][0] = sx2body11;
	SX2Body[sensor_id].dcm[0][1] = sx2body12;
	SX2Body[sensor_id].dcm[0][2] = sx2body13;
	SX2Body[sensor_id].dcm[1][0] = sx2body21;
	SX2Body[sensor_id].dcm[1][1] = sx2body22;
	SX2Body[sensor_id].dcm[1][2] = sx2body23;
	SX2Body[sensor_id].dcm[2][0] = sx2body31;
	SX2Body[sensor_id].dcm[2][1] = sx2body32;
	SX2Body[sensor_id].dcm[2][2] = sx2body33;

	/* Synchronize the quaternion and the inverse */
	rot_set_dcm (&SX2Body[sensor_id], &(SX2Body[sensor_id].dcm));

	/* Update the SX2STA */
	MatrixMult (&STA2Body.dcm_r, &SX2Body[sensor_id].dcm,
                &SX2STA[sensor_id].dcm, 3, 3, 3, DOUBLE_DATA);

	/* Synchronize the quaternion and the inverse */
	rot_set_dcm (&SX2STA[sensor_id], &(SX2STA[sensor_id].dcm));
}

/* This routine updates the attitude information according to current Mode */
void UpdateAttitude ()
{
	int sensor_index;

	if (sim_data.quat_sel == TM)
		sim_data.eci2body = sim_data.tm.eci2body;
	else
		sim_data.eci2body = sim_data.static_eci2body;

	for (sensor_index = ST1; sensor_index <= SS; sensor_index++)
	{
		MatrixMult(&sim_data.eci2body.dcm_r,
				   &SX2Body[sensor_index].dcm,
				   &SX2ECI[sensor_index].dcm,
				   3, 3, 3, DOUBLE_DATA);
					 
		rot_set_dcm(&SX2ECI[sensor_index], &SX2ECI[sensor_index].dcm);
	}
}

/* This routine returns direction cosine matrix between sensor frame
   and body frame */
DirCosMatrix_type GetSX2BodyDCM (int sensor_id)
{
	return *(DirCosMatrix_type *) &SX2Body[sensor_id].dcm;
}

/* This routine returns quaternion sun sensor frame and body frame */
QUATERNION_type GetSX2BodyQuat (int sensor_id)
{
	return *(QUATERNION_type *)&SX2Body[sensor_id].quat;
}

/* This routine is used by Tcl/TK to set the zone radius */
void SetCircularCoverage (int zone_id, double radius)
{
	CircularZones[zone_id].radius = radius*DTR;
}

/* Set the orientation of the Instrument Optical FOV */
void SetInstOptDCM (double inst_opt_to_body11,
                    double inst_opt_to_body12,
                    double inst_opt_to_body13,
                    double inst_opt_to_body21,
                    double inst_opt_to_body22,
                    double inst_opt_to_body23,
                    double inst_opt_to_body31,
                    double inst_opt_to_body32,
                    double inst_opt_to_body33)
{
	InstOptFOV.dcm[0][0] = inst_opt_to_body11;
	InstOptFOV.dcm[0][1] = inst_opt_to_body12;
	InstOptFOV.dcm[0][2] = inst_opt_to_body13;
	InstOptFOV.dcm[1][0] = inst_opt_to_body21;
	InstOptFOV.dcm[1][1] = inst_opt_to_body22;
	InstOptFOV.dcm[1][2] = inst_opt_to_body23;
	InstOptFOV.dcm[2][0] = inst_opt_to_body31;
	InstOptFOV.dcm[2][1] = inst_opt_to_body32;
	InstOptFOV.dcm[2][2] = inst_opt_to_body33;

	/* Syncronize the quaternion and the inverse */
	rot_set_dcm (&InstOptFOV, &(InstOptFOV.dcm));
}

/* This routine is used by Tcl/TK to set the size of the instrument
   optical field of view */
void SetInstOptFOV (double fov_h, double fov_v)
{
	InstFov_H = fov_h*DTR;
	InstFov_V = fov_v*DTR;
}

/* Get the direction cosine matrix between sensor frame and body frame */
char *GetSX2BodyDCM_Data (int sensor_id)
{
	static char buf[9*DATA_FORMAT_SIZE];

	sprintf (buf,
             "%15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f",
			 SX2Body[sensor_id].dcm[0][0],
			 SX2Body[sensor_id].dcm[0][1],
			 SX2Body[sensor_id].dcm[0][2],
			 SX2Body[sensor_id].dcm[1][0],
			 SX2Body[sensor_id].dcm[1][1],
			 SX2Body[sensor_id].dcm[1][2],
			 SX2Body[sensor_id].dcm[2][0],
			 SX2Body[sensor_id].dcm[2][1],
			 SX2Body[sensor_id].dcm[2][2]);

	return buf;
}

/* Get the direction cosine matrix between sensor frame and STA */
char *GetSX2STA_DCM_Data (int sensor_id)
{
	static char buf[9*DATA_FORMAT_SIZE];

	sprintf (buf,
             "%15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f %15.12f",
			 SX2STA[sensor_id].dcm[0][0],
			 SX2STA[sensor_id].dcm[0][1],
			 SX2STA[sensor_id].dcm[0][2],
			 SX2STA[sensor_id].dcm[1][0],
			 SX2STA[sensor_id].dcm[1][1],
			 SX2STA[sensor_id].dcm[1][2],
			 SX2STA[sensor_id].dcm[2][0],
			 SX2STA[sensor_id].dcm[2][1],
			 SX2STA[sensor_id].dcm[2][2]);

	return buf;
}

/* This routine provides the TM attitude information */
char *GetTM_ECI2BodyQuat ()
{
	static char buf[4*DATA_FORMAT_SIZE];

	sprintf (buf, "%15.12f %15.12f %15.12f %15.12f",
			 sim_data.tm.eci2body.quat.x,
			 sim_data.tm.eci2body.quat.y,
			 sim_data.tm.eci2body.quat.z,
			 sim_data.tm.eci2body.quat.a);

	return buf;

}

/* This routine provides the spacecraft ephemeris */
char *GetOrbitStateVector ()
{
	static char buf[7*DATA_FORMAT_SIZE];

	sprintf (buf, "%15.12f %15.12f %15.12f %15.12f %15.6f %15.8f %15.12f",
			 sim_data.tm.ephem[0],
			 sim_data.tm.ephem[1],
			 sim_data.tm.ephem[2],
			 sim_data.tm.ephem[3],
			 sim_data.tm.ephem[4],
			 sim_data.tm.ephem[5],
			 sim_data.tm.ephem[6]);

	return buf;

}
/* This routine generates the Hs and Vs of relevant stars in FOV */
char *ST_Stars (int st_id)
{
	static char fov_star_data[MAXNUM_STARS_FOV*DATA_FORMAT_SIZE];
	char buf[11+DATA_FORMAT_SIZE];
	TrackerFOV_HV_type	star_info;
	int i;

	st_id --; /* Map 1 2 3 index into C array index 0 1 2 */

	StarsInTrackerFOV (st_id, (DirCosMatrix_type *)&(SX2ECI[st_id].dcm_r),
					   &star_info);

	strcpy (fov_star_data, "{ ");

	for ( i = 0; i < star_info.NumStars; i++)
	{
		sprintf (buf, "{%10d %10.6f %10.6f} ",
				 star_info.Stars[i].ASC_ID - 1,
				 star_info.Stars[i].HPos*RTD,
				 star_info.Stars[i].VPos*RTD);

		strcat (fov_star_data, buf);
	}

	strcat (fov_star_data, "}");

	return (fov_star_data);
}

/***************************************************************************
  This function provides the H and Vs for ephemeris object in the specified
  star tracker reference frame
 ***************************************************************************/
char *ST_Object (int st_id)
{
	static char eph_obj_data[2*MAX_EPHEM_OBJS*(DATA_FORMAT_SIZE+11)];
	char buf[2*(DATA_FORMAT_SIZE+11)];
	double obj_in_eci[3], obj_in_st[3];
	double epsilon = 1e-6;
	double app_rad, true_rad;
	double h, v;
	int obj_indx;

	st_id --; /* Map st id 1 2 3 into C array index 0 1 2 * /

	/* Initialize the string */
	strcpy (eph_obj_data, "{ ");

	for ( obj_indx = 0; obj_indx < MAX_EPHEM_OBJS; obj_indx ++)
	{
		/* Get eph obj vector in ECI */
		memcpy(obj_in_eci, (double *)ephem_get_eci_pos(obj_indx),
                            3*sizeof(double));

		MatrixMult(SX2ECI[st_id].dcm_r, obj_in_eci,
                   obj_in_st, 3, 3, 1, DOUBLE_DATA);

		if (fabs(obj_in_st[2]) > epsilon)
		{
			h = atan2(obj_in_st[0], obj_in_st[2]);
			v = atan2(obj_in_st[1], obj_in_st[2]);

			/* Rescale so that ST center to object center angular distance is correct */
			app_rad = sqrt(h*h + v*v);
            if (app_rad > epsilon)
			{
				true_rad = acos(obj_in_st[2]);
				h = h * (true_rad/app_rad);
				v = v * (true_rad/app_rad);
			}

			h = h*RTD;
			v = v*RTD;

			sprintf(buf, "{%10d %10.6f %10.6f} ",obj_indx, h, v);

			strcat (eph_obj_data, buf);
		}
	}

	strcat (eph_obj_data, "}");

	return (eph_obj_data);
}

void NS_Direction (char *s, double st_to_eci[3][3], double arrow_length)
{
	char buf [2*DATA_FORMAT_SIZE];
	RA_DEC boresight, pole[2];
	double eci_to_st[3][3], pole_vec[2][3];
	int i;

	/* Take the transpose of the st_to_eci to form eci_to_st */
	MatrixTranspose (st_to_eci, eci_to_st, 3, 3, DOUBLE_DATA);

	/* Convert the boresight into right ascension and declination */
	ECI2RAscenDeclina ((double *)eci_to_st[2],
					   &boresight.ra, &boresight.dec, DOUBLE_DATA);

	/* Initialize string s */

	strcpy (s, "");
	
	for (i = 0; i < 2; i++)
	{
		/* move toward north by arrow_length rad */
		pole[i].ra = boresight.ra;
		pole[i].dec = boresight.dec + ( i ? - arrow_length : arrow_length);

		/* Convert it into vector in Eci frame  */
		RAscenDeclina2ECI (pole[i].ra, pole[i].dec, pole_vec[i], DOUBLE_DATA);

		/* Transform to ST Reference frame */
		MatrixMult (eci_to_st, pole_vec[i], pole_vec[i], 3, 3, 1, DOUBLE_DATA);

		/* Print H and V */
		sprintf (buf, "%10.6f %10.6f ",
				 atan2(pole_vec[i][0], pole_vec[i][2])*RTD,
				 atan2(pole_vec[i][1], pole_vec[i][2])*RTD);

		/* Append to string s */
		strcat (s, buf);
	}
}

/***************************************************************************
 This function provides H,V data for the North/South Pole if it is in star
 tracker field of view otherwise it provides two segments showing the
 north and south direction.
 ***************************************************************************/
char *ST_NorthAndSouth (int st_id, double arrow_length)
{
	static char pole_data[2*DATA_FORMAT_SIZE];
	double st_to_eci[3][3];
	double rough_threshold = cos(ST_FOV_H + ST_FOV_V);
	double h, v;

	st_id --; /* Map ST id 1,2,3 to C index 0,1,2 */

	/* Get st_to_eci */
	memcpy(st_to_eci, &SX2ECI[st_id].dcm, sizeof(DirCosMatrix_type));

	/* North Pole unit vector in star tracker frame is
	   (st_to_eci[0][2], st_to_eci[1][2], st_to_eci[2][2]) */

	/* Check whether the star tracker views the poles */
	if (st_to_eci[2][2] > rough_threshold) /* Possibly View North Pole */
	{
		h = st_to_eci[0][2]/st_to_eci[2][2];
		v = st_to_eci[1][2]/st_to_eci[2][2];

		if ((fabs(h) < tan(0.5*ST_FOV_H)) && (fabs(v) < tan(0.5*ST_FOV_V)))
			sprintf(pole_data, "N %10.6f %10.6f", h*RTD, v*RTD);
		else
			NS_Direction(pole_data, st_to_eci, arrow_length*DTR);
	}
	else if (st_to_eci[2][2] < - rough_threshold) /* Possibly View South Pole */
	{
		h = st_to_eci[2][0]/st_to_eci[2][2];
		v = st_to_eci[2][1]/st_to_eci[2][2];

		/* South pole in field of view ? */
		if ((fabs(h) < tan(0.5*ST_FOV_H)) && (fabs(v) < tan(0.5*ST_FOV_V)))
			sprintf(pole_data, "S %10.6f %10.6f", h*RTD, v*RTD);
		else
			NS_Direction(pole_data, st_to_eci, arrow_length*DTR);
	}
	else
		NS_Direction(pole_data, st_to_eci, arrow_length*DTR);

	return (pole_data);
}

/***************************************************************************
 This function provides data for the direction of X axis in star
 tracker frame
 ***************************************************************************/
char *ST_X (int st_id)
{
	static char axis_data[2*DATA_FORMAT_SIZE];
	double x_dir[3]={1.0, 0.0, 0.0};

	st_id --; /* Map ST id 1,2,3 to C index 0,1,2 */

	/* Get x_axis direction in ST frame */
	MatrixMult(&(SX2Body[st_id].dcm_r), x_dir, x_dir, 3, 3, 1, DOUBLE_DATA);

	sprintf(axis_data, "%10.6f %10.6f", x_dir[0], x_dir[1]);

	return (axis_data);
}

/***************************************************************************
 This function provides data for the direction of Y axis in star
 tracker frame
 ***************************************************************************/
char *ST_Y (int st_id)
{
	static char axis_data[2*DATA_FORMAT_SIZE];
	double y_dir[3]={0.0, 1.0, 0.0};

	st_id --; /* Map ST id 1,2,3 to C index 0,1,2 */

	/* Get y_axis direction in ST frame */
	MatrixMult(&(SX2Body[st_id].dcm_r), y_dir, y_dir, 3, 3, 1, DOUBLE_DATA);

	sprintf(axis_data, "%10.6f %10.6f", y_dir[0], y_dir[1]);

	return (axis_data);
}

/***************************************************************************
 This function provides data for the direction of Z axis in star
 tracker frame
 ***************************************************************************/
char *ST_Z (int st_id)
{
	static char axis_data[2*DATA_FORMAT_SIZE];
	double z_dir[3]={0.0, 0.0, 1.0};

	st_id --; /* Map ST id 1,2,3 to C index 0,1,2 */

	/* Get z_axis direction in ST frame */
	MatrixMult(&(SX2Body[st_id].dcm_r), z_dir, z_dir, 3, 3, 1, DOUBLE_DATA);

	sprintf(axis_data, "%10.6f %10.6f", z_dir[0], z_dir[1]);

	return (axis_data);
}

/****************************************************************************
  This function provides right ascension and declination of the boresight
  of the specified star tracker.
 ****************************************************************************/
char *ST_Boresight (int st_id)
{
	static char plot_data[2*DATA_FORMAT_SIZE];
	double ra, dec;	/* Boresight right ascension and declination */
 
	st_id --;	/* Star tracker id 1,2,3 ---> 0,1,2 for c code index */

	/* SX2ECI.dcm_r[2] ---> the boresight vector */
	ECI2RAscenDeclina((double*)SX2ECI[st_id].dcm_r[2], &ra, &dec, DOUBLE_DATA);

	sprintf (plot_data, "%10.6f %10.6f", ra*RTD, dec*RTD);

	return plot_data;
}

/****************************************************************************
  This routine provides data points for star tracker field of view  via a
  string in format:
  {..{x1 y1 x2 y2 ... xn yn} { ...} ...}
 ****************************************************************************/
char *Sky_ST (int st_id)
{
	static char plot_data[DATA_FORMAT_SIZE*(2*TOT_POINTS_PER_FOV+8)];

	double st_fov_h, st_fov_v;

	st_id --;	/* Star tracker id 1,2,3 ---> 0,1,2 for c code index */

	/* Set st_fov */
	st_fov_h = ST_FOV_H;
	st_fov_v = ST_FOV_V;

	PrintRectangleData (plot_data, st_fov_h, st_fov_v, SX2ECI[st_id].dcm);

	return plot_data;
}

/****************************************************************************
  This routine provides data points for drawing stayout zone via a string in
  format:
  {..{x1 y1 x2 y2 ... xn yn} { ...} ...}
 ****************************************************************************/
char *Sky_Object (int obj_id, double radius)
{
	static char plot_data[DATA_FORMAT_SIZE*TOT_POINTS_PER_OBJ+8];
	double obj_in_eci[3] = {-1.0, 0.0, 0.0};

	/* Get obj_in_eci from ephemeris */
	memcpy(obj_in_eci, (double *) ephem_get_eci_pos(obj_id), 3*sizeof(double));

	/* Check if the radius is greater than 90 degree */
	if (radius > 90.0)
	{
		radius = 180.0 - radius;
		obj_in_eci[0] = -obj_in_eci[0];
		obj_in_eci[1] = -obj_in_eci[1];
		obj_in_eci[2] = -obj_in_eci[2];
	}

	PrintCircularData(plot_data, radius*DTR, obj_in_eci);

	return plot_data;
}

/****************************************************************************
  This routine provides data points for drawing the annulus for the specified
  star tracker :
  {..{x1 y1 x2 y2 ... xn yn} { ...} ...}
 ****************************************************************************/
char *Sky_Annulus (int st_id)
{
	static char annulus_data[2*DATA_FORMAT_SIZE*TOT_POINTS_PER_OBJ+20];
	double inner_radius, outer_radius;

	GetInnerAndOuterRadius (st_id-1, &inner_radius, &outer_radius);

	strcpy (annulus_data, "{");

	strcat (annulus_data, Sky_Object(GSS_SUN, outer_radius*RTD));

	strcat (annulus_data, " ");

	strcat (annulus_data, Sky_Object(GSS_SUN, inner_radius*RTD));

	strcat (annulus_data, "}");

	return (annulus_data);
}

/****************************************************************************
  This function provides data for drawing a circle representing the rough
  region that the specified star tracker views.
 ****************************************************************************/
char *Sky_RoughBoresight (int st_id, double rough_radius)
{
	static char plot_data[DATA_FORMAT_SIZE*TOT_POINTS_PER_OBJ+8];
 
	st_id --;	/* Star tracker id 1,2,3 ---> 0,1,2 for c code index */

	PrintCircularData (plot_data, rough_radius*DTR,
                       (double*)SX2ECI[st_id].dcm_r[2]);
	return plot_data;
}

/****************************************************************************
  This function provides the right ascension and declination for the
  specified body axis: 0 -- x, 1 -- y, 2 -- z
 ****************************************************************************/
char *Sky_Body_Axis (int axis_id)
{
	static char plot_data[DATA_FORMAT_SIZE];
	RA_DEC x_pos;
	
	ECI2RAscenDeclina((double *)&sim_data.eci2body.dcm[axis_id],
					   &x_pos.ra, &x_pos.dec, DOUBLE_DATA);

	sprintf (plot_data, "%10.6f %10.6f", x_pos.ra*RTD, x_pos.dec*RTD);
	return plot_data;
}

/****************************************************************************
  This function provides data to draw the specified T&C object coverage region
 ****************************************************************************/
char *Sky_Circular_Zone (int zone_id)
{
	static char plot_data[DATA_FORMAT_SIZE*TOT_POINTS_PER_OBJ+8];
	double boresight[3] = {0.0, 0.0, 1.0};	/* Z axis */
	double radius = CircularZones[zone_id].radius;

	if (zone_id == SUN_SENSOR)	/* Sun sensor frame, X is boresight */
	{
		boresight[0] = 1.0;
		boresight[1] = 0.0;
		boresight[2] = 0.0;
		CircularZones[zone_id].xx2body = SX2Body[SS];
	}

	MatrixMult (&CircularZones[zone_id].xx2body.dcm, boresight,
						 boresight, 3, 3, 1, DOUBLE_DATA);

	MatrixMult (Bdy2Eci, boresight, boresight, 3, 3, 1, DOUBLE_DATA);

	PrintCircularData(plot_data, radius, boresight);

	return plot_data;
}

/****************************************************************************
  This function provides data for drawing the Instrument Optical FOV
 ****************************************************************************/
char *Sky_InstOptFOV ()
{
	static char plot_data[DATA_FORMAT_SIZE*(2*TOT_POINTS_PER_FOV+8)];
	double inst_to_eci[3][3];

	MatrixMult (Bdy2Eci, &(InstOptFOV.dcm), 
				inst_to_eci, 3, 3, 3, DOUBLE_DATA);
	
	PrintRectangleData (plot_data, InstFov_H, InstFov_V, inst_to_eci);
	return plot_data;
}
