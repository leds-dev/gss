/* ========================================================================
	FILE NAME:
		CISInterface.h

	DESCRIPTION:
		This is a header file of GuiInterface.c.
		All functions in this file will be called by GUI c code. 

 ========================================================================== */

#ifndef _CISINTERFACE_H
#define _CISINTERFACE_H

/* $Id: CISInterface.h,v 1.1 2000/04/26 17:23:54 jzhao Exp $ */

/* ---------------------------------------------
	Headers
*/
#include "CISGlobalType.h"
#include "CISExeType.h"
#include "ChooseOSC.h"

/*--------------------------------------------
	Type Define
*/

typedef struct
{
	QUATERNION_type Quat_Final;
	QUATERNION_type Quat_Step_Post_Mult;
	QUATERNION_type Quat_Step_Pre_Mult;
}SlewOutput_type;

/* --------------------------------------------
	Function Prototye
*/
void GetCISCommitBuff(CISOutput_type *CISResults);
void GetSlewCommitBuff(SlewOutput_type *SlewResults);

void CIS_SunHoldAnnulus();
void CIS_RoughKnowledgeRadius();

void StarsInTrackerFOV(int st_id, DirCosMatrix_type *dcm_eci2st,
                       TrackerFOV_HV_type *star_info);

/***************************************************************************
	Change Log:
	$Log: CISInterface.h,v $
	Revision 1.1  2000/04/26 17:23:54  jzhao
	Initial revision.
	

****************************************************************************/

#endif /* _CISINTERFACE_H */
