/* $Id: StarTracker.c,v 1.3 1999/12/22 01:09:20 jzhao Exp $ */
/* ========================================================================
	FILE NAME:
		StarTracker.c	

	DESCRIPTION:
		This file includes Star Tracker FOV calaculations and transformations
		(bounds, corners in ST frame, corners in BDY, HV to STFrame, STFrame 
		to HV...), Statistics analysis of star postions in HV, Alignment, 
		Available ST List... 

	FUNCTION LIST:

 ==========================================================================

	REVISION HISTORY

 ========================================================================== */

 /* System Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Self Defined Headers */
#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <CISGlobal.h>
#include <StarTracker.h>

/* ======================================================= */

/*---------------------
STFOV_Bounds:	Calulates bounds on Star Tracker Fields Of View(ST Frame) 
				Note: H position mathchs Y, and -V mathchs X.	
Output:
	XMax, XMin, YMax, YMin: Max and Min value of X and Y in ST coordinate.
*/

void STFOV_Bounds(double *XMax, double *XMin, double *YMax, double *YMin) 
{
	*XMax = tan(H_MAX_ANG);
	*XMin = tan(H_MIN_ANG);
	*YMax = tan(V_MIN_ANG); 
	*YMin = tan(V_MAX_ANG);
}

/*---------------------
STFOV_CornerPos_STFrame:	Calculates positions of Star Tracker FOV Corners 
							in the tracker frame. 
Output:	
	CornerPos:	[4x1] Array of VECTOR_type. Corner positions of Star tracker.
				All corner positions are normalized.
*/

void STFOV_CornerPos_STFrame(VECTOR_type CornerPos[])
{
	int i;
	double XMax, XMin, YMax, YMin;

	STFOV_Bounds(&XMax, &XMin, &YMax, &YMin);

	CornerPos[0].Vec[0]=XMin; 	CornerPos[0].Vec[1]=YMin; 
								CornerPos[0].Vec[2]=1; 
	CornerPos[1].Vec[0]=XMin; 	CornerPos[1].Vec[1]=YMax; 
								CornerPos[1].Vec[2]=1; 
	CornerPos[2].Vec[0]=XMax; 	CornerPos[2].Vec[1]=YMin; 
								CornerPos[2].Vec[2]=1; 
	CornerPos[3].Vec[0]=XMax; 	CornerPos[3].Vec[1]=YMax; 
								CornerPos[3].Vec[2]=1; 

	for (i=0; i<4; i++)
		VectorNormalize(CornerPos[i].Vec, CornerPos[i].Vec, DOUBLE_DATA);
}

/*--------------------------------
STFOV_StarPosStatistics:    Calculates Mean, Standard Deviation and Spread 
							of star Postion in ST FOV(H-V position). 

Input:
	TrackedVTs:	TrackerOutputVT_type. Tracked stars in STFOV(H-V position).
Output:
	PosStatistics:	StatisticsVT_type. Mean Std. Deviation and Spread of star
										 postion in ST FOV.
*/

int STFOV_StarPosStatistics(TrackerOutputVT_type *TrackedVTs, 
							StatisticsVT_type	*PosStatistics)
{
	int err_msg=0, Star_indx;
	double mean_H=0, mean_V=0, var_H=0, var_V=0;

	if (TrackedVTs->NumVTs == 0) 
	{
		err_msg = -1;
		return (err_msg);
	}
	for(Star_indx=0; Star_indx<TrackedVTs->NumVTs; Star_indx++)
	{
		mean_H += TrackedVTs->VTs[Star_indx].HPos;
		mean_V += TrackedVTs->VTs[Star_indx].VPos;
		var_H += TrackedVTs->VTs[Star_indx].HPos *
				 TrackedVTs->VTs[Star_indx].HPos;
		var_V += TrackedVTs->VTs[Star_indx].VPos *
				 TrackedVTs->VTs[Star_indx].VPos;
	}
	
	mean_H /= TrackedVTs->NumVTs;
	mean_V /= TrackedVTs->NumVTs;

	var_H /= TrackedVTs->NumVTs;
	var_V /= TrackedVTs->NumVTs;
	var_H -= mean_H*mean_H;
	var_V -= mean_V*mean_V;
	
	PosStatistics->Mean_H = mean_H;
	PosStatistics->Mean_V = mean_V;
	PosStatistics->StdDev_H = sqrt(var_H);
	PosStatistics->StdDev_V = sqrt(var_V);
	PosStatistics->Spread = sqrt(TrackedVTs->NumVTs*(var_H + var_V));

	return(err_msg);
}
/* -----------------------------------
StarTrackerAlignment: Determine the actual alignment of the trackers, 
	Based on the identified stars tracked by each tracker. If a tracker
	is not tracking any ID'd stars, its assumed alignment remain unchanged.

	The alignment obtained is dependent on the reference tracker(s).

	If there is only one reference tracker, i.e., only one tracker is 
	tracking the threshold number of identifiable stars, not enough information
	is available to calculate a new alignment for the tracker. The alignment
	matrices are unchanged.

	If there are two reference trackers, the boresight of Ref1ST is assumed
	to be defined by the existing alignment matrix. Given this condition, 
	the boresight of the Ref2St is assumed to be as close as possible to the
	orientation defined by its existing alignment matrix.   

Input:
	Ref1ST, Ref2ST:		ID number of reference trackers.
	ValidRef:			array[NUM_ST] of int. List of Valid Reference tracks.
						(Valid reference tracker is tracker which tracked 
						threshold number of ID'able stars).
	STBoresight:		Tracker boresight vector in STframe.
	STLosBDY:			array[NUM_ST] of vector. STracker Boresight 
						(LOS) in body frame.
	DCM_ECI2ST:			array[NUM_ST] of DirCosMatrix_type. Direction
						Cosine Matrix from ECI to ST frame.
Output:
	Quat_ST2BDY:
	Quat_BDY2ST:
	DCM_ST2BDY:
	DCM_BDY2ST:
*/

void StarTrackerAlignment(int Ref1ST_ID, int Ref2ST_ID, int ValidRef[], 
						  VECTOR_type *STBoresight,
						  VECTOR_type STLosBDY[],
						  DirCosMatrix_type DCM_ECI2ST[],
						  QUATERNION_type Quat_ST2BDY[],
						  QUATERNION_type Quat_BDY2ST[],
						  DirCosMatrix_type DCM_ST2BDY[],
						  DirCosMatrix_type DCM_BDY2ST[])
{
	int ST_indx;
	double Ref1xRef2BoreBDY[3];
	double DCM_ST2ECI_Ref1[9], DCM_ST2ECI_Ref2[9];
	double BoreECI_Ref1[3], BoreECI_Ref2[3];
	double Ref2BoreInRef1STFrame[3], Ref1BoreInST_indxSTFrame[3]; 
	double Ref1xRef2BoreInRef1STFrame[3];
	double Ref1xRef2BoreInRef1ECIFrame[3];
	double Ref1xRef2BoreInST_indxSTFrame[3];

	/* Do nothing unless at least two trackers may serve as reference trackers
	   (i.e., track a threshold number of identifiable stars) */
	if(Ref2ST_ID > 0)
	{
		/* Calculate the orientations of the reference tracker boresights,
	   	in the body frame. */
		
		CrossProduct(STLosBDY[Ref1ST_ID-1].Vec, 
					 STLosBDY[Ref2ST_ID-1].Vec,
					 Ref1xRef2BoreBDY, DOUBLE_DATA);
		/* Calculate the orientations of the reference tracker boresights,
	   	in the Ref1ST_ID Tracker frame. */

		MatrixTranspose(DCM_ECI2ST[Ref2ST_ID-1].DCM, DCM_ST2ECI_Ref2, 
						3, 3, DOUBLE_DATA);
 		MatrixMult(DCM_ST2ECI_Ref2, STBoresight->Vec, BoreECI_Ref2,
					3, 3, 1, DOUBLE_DATA);
		MatrixMult(DCM_ECI2ST[Ref1ST_ID-1].DCM, BoreECI_Ref2, 
					Ref2BoreInRef1STFrame, 3, 3, 1, DOUBLE_DATA);	
		CrossProduct(STBoresight->Vec, Ref2BoreInRef1STFrame, 
					 Ref1xRef2BoreInRef1STFrame, DOUBLE_DATA);

		MatrixTranspose(DCM_ECI2ST[Ref1ST_ID-1].DCM, DCM_ST2ECI_Ref1, 
													3, 3, DOUBLE_DATA);
		MatrixMult(DCM_ST2ECI_Ref1, STBoresight->Vec, BoreECI_Ref1, 
												 3, 3, 1, DOUBLE_DATA);
		for(ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		{
			/* Don't do anything to the alignment matrix for the tracker unless
			it is tracking a threshold number of ID'able stars. */
			
			if(ValidRef[ST_indx])
			{
				MatrixMult(DCM_ECI2ST[ST_indx].DCM, BoreECI_Ref1, 
							Ref1BoreInST_indxSTFrame, 3, 3, 1, DOUBLE_DATA);
				MatrixMult(DCM_ST2ECI_Ref1, Ref1xRef2BoreInRef1STFrame,
							Ref1xRef2BoreInRef1ECIFrame, 3, 3, 1, DOUBLE_DATA);
				MatrixMult(DCM_ECI2ST[ST_indx].DCM, 
						   Ref1xRef2BoreInRef1ECIFrame, 
						   Ref1xRef2BoreInST_indxSTFrame, 3, 3, 1, DOUBLE_DATA);

				QuatTRIAD(STLosBDY[Ref1ST_ID-1].Vec,
						  Ref1BoreInST_indxSTFrame,
						  Ref1xRef2BoreBDY, 
						  Ref1xRef2BoreInST_indxSTFrame,
						  Quat_BDY2ST[ST_indx].Quat);

				QuaternionInvers(Quat_BDY2ST[ST_indx].Quat, 
								 Quat_ST2BDY[ST_indx].Quat, DOUBLE_DATA);

				Quaternion2DirCosMatrix(Quat_BDY2ST[ST_indx].Quat,
										DCM_BDY2ST[ST_indx].DCM, DOUBLE_DATA);
			
				MatrixTranspose(DCM_BDY2ST[ST_indx].DCM,
								DCM_ST2BDY[ST_indx].DCM, 3, 3, DOUBLE_DATA);

			}/* endof if(ValidRef(ST_indx)) */

		}/* endof ST_indx */		 

	} /* endof if(Ref2ST_ID>0) */
}

/*---------------------
AlignmentEstimateChange:    
                Calculates change in estimate of sensor
                alignment.  The sensor may be a star tracker,
                or a sun sensor
Input:
    original_DCM_Sensor2BDY:    DirCosMatrix_type
                        DCM_Sensor2BDY, from initial
                        estimate (input of CIS)
    refined_DCM_Sensor2BDY:     DirCosMatrix_type 
                        Quat_Sensor2BDY, from "refined"
                        estimate (after CIS run)
    SensorBoresight_Sensor:     VECTOR_type  -
                        Sensor boresight unit vector in
                        Sensor frame.
                        -  For a star
                           tracker, this is (0, 0, 1).
                        -  For a sun
                           sensor, this is (1, 0, 0).
Output:
    SeparationBoresight: pointer to scalar (radians)
                         Angular separation between
                         boresight, given initial
                         estimate, and boresight, given
                         refined estimate.
    RotationAboutBoresight: pointer to scalar (radians)
                        Rotation about sensor boresight
                        required to match (as closely
                        as possible) original frame
                        to refined frame.
*/

void AlignmentEstimateChange (DirCosMatrix_type  Original_DCM_Sensor2BDY,
                              DirCosMatrix_type  Refined_DCM_Sensor2BDY,
                              VECTOR_type SensorBoresight_Sensor,
                              double *SeparationBoresight,
                              double *RotationAboutBoresight)
{
	QUATERNION_type     Original_Quat_Sensor2BDY;
    QUATERNION_type     Refined_Quat_Sensor2BDY;

    VECTOR_type     Original_SensorBoresight_BDY;
    VECTOR_type     Refined_SensorBoresight_BDY;

    double          cos_half_RotationAboutBoresight;
    double          TotalSeparation;

    /* convert direction cosine matrices (DCM's) into quaternion form.  */

    DirCosMatrix2Quaternion (Original_DCM_Sensor2BDY.DCM,
                             Original_Quat_Sensor2BDY.Quat,
                             DOUBLE_DATA);
    DirCosMatrix2Quaternion (Refined_DCM_Sensor2BDY.DCM,
                             Refined_Quat_Sensor2BDY.Quat,
                             DOUBLE_DATA);

    /* Calculate angular separation between the original and refined
       estimates.  I.e., calculate the minimal rotation needed to transform
       one into the other.  */

    TotalSeparation = QuaternionSepAngle(Original_Quat_Sensor2BDY.Quat, 
                                         Refined_Quat_Sensor2BDY.Quat); 

    /* Calculate the angular separation between boresights.  Begin
       by calculating the boresights expressed in the body frame ...  */

    MatrixMult(Original_DCM_Sensor2BDY.DCM, SensorBoresight_Sensor.Vec,
               Original_SensorBoresight_BDY.Vec, 3, 3, 1, DOUBLE_DATA);

    MatrixMult(Refined_DCM_Sensor2BDY.DCM, SensorBoresight_Sensor.Vec,
               Refined_SensorBoresight_BDY.Vec, 3, 3, 1, DOUBLE_DATA);

    /* ... Then calculate the angular separation.  */

    AngleBetVectors(Original_SensorBoresight_BDY.Vec,
                    Refined_SensorBoresight_BDY.Vec,
                    SeparationBoresight, DOUBLE_DATA);

     /* Calculate RotationAboutBoresight from the angles TotalSeparation
        and SeparationBoresight.  Use the fact that the rotation of
        TotalSeparation comes from rotation of SeparationBoresight (about
        an axis perpendicular to the boresight) followed by a rotation of
        SeparationBoresight (about the boresight) -- that is, two rotations
        about axes separated by ninety degrees.

        If SeparationBoresight happens to be 180 degrees, the
        RotationAboutBoresight angle is indeterminant; it is taken to be
        180 degrees here, for simplicity.  (Note: Realistically, such a
        case will not occur; a SeparationBoresight above about two degrees
        will be a sign that something serious is wrong with the alignment
        calculation, or the sensor.)  */

    if (fabs(cos(0.5 * (*SeparationBoresight))) > TINY)  
    {
    	cos_half_RotationAboutBoresight =
            cos(0.5 * TotalSeparation)/cos(0.5 * (*SeparationBoresight));

        *RotationAboutBoresight =
            2 * acos(cos_half_RotationAboutBoresight);
    }
    else
        *RotationAboutBoresight = PI;
}

/*----------------------------------------
ST_HVPos2STFrame: Calculates stars STFrame from given HV Position for the
				  Star Tracker. 
				  STFrame[x] = tan(HPos)
				  STFrame[y] = tan(VPos)
				  STFrame[z] = 1.0
				  Normalization on vector STFrame  

Input:
	StarsHVPos:	TrackerOutputVT_type. Tracked Stars in HV position(radians).
Output:
	StarsSTFrame: TrackerOutputST_type. Tracked Stars in ST Frame(unit vector).
*/

void ST_HVPos2STFrame(TrackerOutputVT_type *StarsHVPos, 
					  TrackerOutputST_type *StarsSTFrame)
{
	int Star_indx;
	double STFrame[3];

	for(Star_indx=0; Star_indx<StarsHVPos->NumVTs; Star_indx++)
	{
		STFrame[0] = tan(StarsHVPos->VTs[Star_indx].HPos);
		STFrame[1] = tan(StarsHVPos->VTs[Star_indx].VPos);
		STFrame[2] = 1.0;
		VectorNormalize(STFrame, StarsSTFrame->Stars[Star_indx].STFrame, 
															DOUBLE_DATA);
		StarsSTFrame->Stars[Star_indx].Magnitude 
									= StarsHVPos->VTs[Star_indx].Magnitude;
		StarsSTFrame->Stars[Star_indx].VT_ID 
									= StarsHVPos->VTs[Star_indx].VT_ID;

	} /* endof Star_indx */
	
	StarsSTFrame->NumStars = StarsHVPos->NumVTs;
	StarsSTFrame->ST_ID = StarsHVPos->ST_ID;
}

/* -----------------------------------------------
ST_STFrame2HVPos: Calculates star HV Position of tracker stars from ST Frame. 

Input:
	StarsSTFrame:	TrackerOutputST_type. Stars in ST Frame(unit vector).
Output:
	StarsHVPos:		TrackerOutputVT_type.	Stars in HV Position(radians).
*/

void ST_STFrame2HVPos(TrackerOutputST_type *StarsSTFrame, 
					  TrackerOutputVT_type *StarsHVPos)
{
	int Star_indx;

	for(Star_indx=0; Star_indx<StarsSTFrame->NumStars; Star_indx++)
	{
		StarsHVPos->VTs[Star_indx].HPos 
					= atan2(StarsSTFrame->Stars[Star_indx].STFrame[0],
						   	StarsSTFrame->Stars[Star_indx].STFrame[2]);
		StarsHVPos->VTs[Star_indx].VPos 
					= atan2(StarsSTFrame->Stars[Star_indx].STFrame[1],
						   	StarsSTFrame->Stars[Star_indx].STFrame[2]);
		StarsHVPos->VTs[Star_indx].Magnitude
					= StarsSTFrame->Stars[Star_indx].Magnitude;
		StarsHVPos->VTs[Star_indx].VT_ID
					= StarsSTFrame->Stars[Star_indx].VT_ID;
							
	} /* endof Star_indx */

	StarsHVPos->ST_ID = StarsSTFrame->ST_ID;
	StarsHVPos->NumVTs = StarsSTFrame->NumStars;
}

/* ----------------
STVector2HVPos:

*/

void STVector2HVPos(double STVector[], double *HPos, double *VPos)
{
	*HPos = atan2(STVector[0], STVector[2]);
	*VPos = atan2(STVector[1], STVector[2]);
}

/*----------------
MagCompareFunVT: Compare Magnitude for qsort.
*/
int MagCompareFunVT(const void *p1, const void *p2)
{
	VT_type *a, *b;

	a = (VT_type *)p1;
	b = (VT_type *)p2;

	if(a->Magnitude < b->Magnitude) return(-1);
	if(a->Magnitude > b->Magnitude) return(1);
	else return(0);
}

/* ---------------------------
GenAvailableSTList: Sort stars in each tracker by magnitude(brighter to
					dimmer). Put ST_ID to AvailableST List if the tracker
					tracked more than one star. Put ST_ID to PossibleRefSTList
					if the tracker meet RefST criterion. Convert tracker data 
					from (H, V) VT coordinate to (X, Y, Z) ST Frame.
					(Assume Nontracked VT already eliminated from RawVTs)
Input:
	RawVTs: Array[NUM_ST] of TrackerOutputVT_type. Tracked Raw data from
			TLM snap buffer.
Output:
	TrackedStarST: Array[NUM_ST} of TrackerOutputST_type. Convertd from
    VT to ST and sorted by magnitude for each tracker.
    AvailableSTList: Array[NUM_ST] of int. List of tracker ST_ID which tracked
	more than one star.
*/

void GenAvailableSTList(TrackerOutputVT_type RawVTs[],
						TrackerOutputST_type TrackedStarST[],
						STList_type *AvailableSTList,
						STList_type *PossibleRefSTList)
{
	int ST_indx;

	AvailableSTList->NumST = 0;
	PossibleRefSTList->NumST = 0;
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		AvailableSTList->ST_ID[ST_indx] = 0;
		PossibleRefSTList->ST_ID[ST_indx] = 0;	
	}

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		/* Sort stars by magnitude */
		qsort(RawVTs[ST_indx].VTs, RawVTs[ST_indx].NumVTs,
			  sizeof(VT_type), MagCompareFunVT);

		/* Convert from HV to ST Frame */
		ST_HVPos2STFrame(&(RawVTs[ST_indx]), &(TrackedStarST[ST_indx]));  

		/* Create AvailableSTList */ 
		if (TrackedStarST[ST_indx].NumStars > 0)
		{
			AvailableSTList->ST_ID[AvailableSTList->NumST] = 
												TrackedStarST[ST_indx].ST_ID; 
			AvailableSTList->NumST++;

			/* Create PossibleRefSTList */
			if (TrackedStarST[ST_indx].NumStars >= PRIMARY_ST_TIGHT_THRESHOLD)
			{
				PossibleRefSTList->ST_ID[PossibleRefSTList->NumST] = 
												TrackedStarST[ST_indx].ST_ID;
				PossibleRefSTList->NumST++; 
			}
		}
	}
}

/* -----------------
int TotalSTStars: Calculates total stars tracked by Star Trackers.

Input: 
	TrackedStarST: TrackerOutputST_type. 
Return: 
	Total Number of Stars Tracked by Trackers.
*/
int TotalSTStars(TrackerOutputST_type TrackedStarST[])
{
	int ST_indx;
	int TotalStars = 0;

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		TotalStars += TrackedStarST[ST_indx].NumStars;		

	return(TotalStars);
}

/**************************************************************************************
	Change Log:
	$Log: StarTracker.c,v $
	Revision 1.3  1999/12/22 01:09:20  jzhao
	Modified on create BestIDStars List.
	
	Revision 1.2  1999/12/15 01:16:12  jzhao
	Modified for Multiple cases Testing.
	
	Revision 1.1  1999/12/13 18:42:53  jzhao
	Initial Release.
	
***************************************************************************************/
