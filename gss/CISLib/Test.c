/* $Id: Test.c,v 1.15 2000/03/28 22:48:26 jzhao Exp $ */

/* ========================================================================
	FILE NAME:
		Test.c	

	DESCRIPTION:
		This is test Driver of CISLib. 

	FUNCTION LIST:
						

 ==========================================================================

	REVISION HISTORY

 ========================================================================== */

 /* System Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/* Self Defined Headers */
#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <MessageIO.h>
#include <CISGlobal.h> 
#include <ephem_intf.h>
#include <StarTracker.h>
#include <StarCatalog.h>
#include <AcqParam.h>
#include <IDentifyStars.h>
#include <ChooseOSC.h>
#include <CIS_Slew.h>
#include <SunTrack.h>
#include <CISExeType.h>
#include <CISExe.h>
#include <TerminateFnc.h>

FILE *LogFp;
FILE *TestOutFp;
int TestCount=0;


/* ====================================================== */
void ReadTestInput(FILE *fp, IniVariable_type *IniVars, 
				   VTBuffer_type VTBuffer[][NUM_VT], 
				   double *SunRA, double *SunDec)
{
	int i, j;
	double tmp;

	fscanf(fp, "%*s %*s %*s\n");
	
	for(i=0; i<4; i++)
		fscanf(fp, "%*s %*s %*s %le;\n", &(IniVars->Quat_ECI2BDY.Quat[i])); 

	for(i=0; i<NUM_ST; i++)
		for(j=0; j<4; j++)
			fscanf(fp, "%*s %*s %*s %le;\n", &(IniVars->Quat_ST2BDY[i].Quat[j]));
		
	fscanf(fp, "%*s %*s %*s %le;\n", SunRA);
	fscanf(fp, "%*s %*s %*s %le;\n", SunDec);
	*SunRA *= DTR;
	*SunDec *= DTR;

	for(i=0; i<NUM_ST; i++)
	{
		for(j=0; j<NUM_VT; j++)
			fscanf(fp, "%*s %*s %*s %le;\n", &(VTBuffer[i][j].HPos)); 
		for(j=0; j<NUM_VT; j++)
			fscanf(fp, "%*s %*s %*s %le;\n", &(VTBuffer[i][j].VPos)); 
		for(j=0; j<NUM_VT; j++)
			fscanf(fp, "%*s %*s %*s %le;\n", &(VTBuffer[i][j].Magnitude)); 
		for(j=0; j<NUM_VT; j++)
		{
			fscanf(fp, "%*s %*s %*s %le;\n", &tmp); 
			VTBuffer[i][j].UserSel = (int)tmp;
			VTBuffer[i][j].Tracked = FALSE;  
		}
	}

/* For Integration Test only */

	for(i=0; i<3; i++)
		fscanf(fp, "%*s %*s %*s %le;\n", 
						&(IniVars->StayOutZones[GSS_EARTH].ECI.Vec[i]));

	for(i=0; i<3; i++)
		fscanf(fp, "%*s %*s %*s %le;\n", 
						&(IniVars->StayOutZones[GSS_MOON].ECI.Vec[i])); 
}

/* ======================================================= */
void main()
{
	int stat=0;
	int i, j, ST_indx;

	char LogFileName[] = {"../LOG/CISLog.txt"};
	AcqSC_type  AcqCatalog;
	
	StarASC_type *ASC;
	int	NumASC;

	IniVariable_type IniVars;
	int CIS_MODE = SUN_HOLD_MODE;
	int STCalibON = TRUE;
	int SSCalibON = TRUE;
	int NumIter = 5000;
	double SunRA, SunDEC;
	CISOutput_type CISOutput;
	
	VTBuffer_type VTBuffer[NUM_ST][NUM_VT];

	char	AscFilename[200];
	char	TestInFilename[200]; 
	char 	TestOutFilename[200];

	time_t t;
	char s[100];
	FILE *TestInFp;

	double Radius;
	double InnerRadius[NUM_ST], OuterRadius[NUM_ST];

	for (i=0; i<MAX_EPHEM_OBJS; i++)
		IniVars.StayOutZones[i].Radius = 0.0;

	/* --------------------------------------------------
		User Selection
	----------------------------------------------------- */
	printf("\n\nInput CIS_MODE(SUN_HOLD_MODE=1, ROUGH_KNOWLEDGE_MODE=0):");
	scanf("%d", &CIS_MODE);
	
	printf("\n");
	printf("Input Iteration Number(Number of Cases) :");
	scanf("%d", &NumIter);

	printf("\n\nFile Name of ASC(with path if not in current Dir)\n");
	scanf("%s", AscFilename);
	printf("AscFilename %s\n", AscFilename);
	
	printf("\n\nFile Name of Input Test Data(with path if not in current Dir)\n");
	scanf("%s", TestInFilename);
	printf("InputFilename %s\n", TestInFilename);

	printf("\n\nFile Name of Output Test Data(with path if not in current Dir)\n");
	scanf("%s", TestOutFilename);
	printf("OutputFilename %s\n", TestOutFilename);

	/* ---------------------------------
		Open Input/Output Test Files
	------------------------------------ */

	if ( (TestOutFp = fopen(TestOutFilename, "w")) == NULL )
	{
		stat = FILE_ERROR;
		DisplayMessage(stat, TestOutFilename);
		LogMessage(stat, TestOutFilename);
		goto Quit;
	}

	if( (TestInFp = fopen(TestInFilename, "r")) == NULL)
	{
		stat = FILE_ERROR;
		DisplayMessage(stat, TestInFilename);
		LogMessage(stat, TestInFilename);
		goto Quit;
	}

	/* -----------------------------------
		Write CIS Mode
	-------------------------------------- */

	time(&t);
	sprintf(s, "#	Start at %s",  ctime(&t) );
	TestOutString(s);

	if (CIS_MODE == SUN_HOLD_MODE)
		TestOutString("%Sun Hold Acq. Mode;");
	else
		TestOutString("%Rough Attitude Knowledge Acq. Mode;");
	
	TestOutString("%");
	TestOutString("%");

	/* -----------------------------------
		Iterate for each Test Case
	-------------------------------------- */

	ASC = (StarASC_type *)calloc(MAXNUM_ASC_STARS, sizeof(StarASC_type) );
	{
		if ( ASC == NULL ) 
		{
			printf("Memory Error");
			goto Quit;
		}
	}

	for (i=0; i<NumIter; i++)
	{
		TestCount++; 
		printf("TestCount %d\n", TestCount);

		/* ----------------------------------------------
			Read in ASC (GUI's ASC type)
		------------------------------------------------- */
		NumASC = Read_StarCatalog(AscFilename, ASC, ASC_TYPE);
		
		/* ---------------------------
			Create CIS ASC pointer list 
		------------------------------ */
		AcqCatalog.NumStars = NumASC;

		for (j=0; j<NumASC; j++)
			AcqCatalog.Stars[j] = &ASC[j];	

		/* ---------------------------------------------
			Read Test Input File
		------------------------------------------------ */
		ReadTestInput(TestInFp, &IniVars, VTBuffer, &SunRA, &SunDEC);
		RAscenDeclina2ECI(SunRA, SunDEC, &(IniVars.StayOutZones[GSS_SUN].ECI),
																DOUBLE_DATA);
		/* ----------------------------------------------
			Set Ini Variables
		------------------------------------------------- */

		Quaternion2DirCosMatrix(IniVars.Quat_ECI2BDY.Quat, 
							IniVars.DCM_ECI2BDY.DCM, DOUBLE_DATA); 

		for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		{
			/* Test input data is BDY2ST need to inverse it to ST2BDY */ 
			QuaternionInvers(IniVars.Quat_ST2BDY[ST_indx].Quat,
							 IniVars.Quat_ST2BDY[ST_indx].Quat, DOUBLE_DATA);

			Quaternion2DirCosMatrix(IniVars.Quat_ST2BDY[ST_indx].Quat, 
									IniVars.DCM_ST2BDY[ST_indx].DCM, DOUBLE_DATA); 
		}
		IniVars.DCM_SS2BDY.DCM[0] = 1/SQRT_2; 
		IniVars.DCM_SS2BDY.DCM[1] = 0; 
		IniVars.DCM_SS2BDY.DCM[2] = 1/SQRT_2; 
		IniVars.DCM_SS2BDY.DCM[3] = 0; 
		IniVars.DCM_SS2BDY.DCM[4] = 1; 
		IniVars.DCM_SS2BDY.DCM[5] = 0; 
		IniVars.DCM_SS2BDY.DCM[6] = -1/SQRT_2; 
		IniVars.DCM_SS2BDY.DCM[7] = 0; 
		IniVars.DCM_SS2BDY.DCM[8] = 1/SQRT_2; 

		DirCosMatrix2Quaternion(IniVars.DCM_SS2BDY.DCM, IniVars.Quat_SS2BDY.Quat,
														DOUBLE_DATA);
		IniVars.MAX_ROUGH_KNOWLEDGE_ERROR = 5.0;		/* Degree */
		IniVars.MAX_TIGHT_POS_TOL = 0.2;           		/* Degree */ 
		IniVars.MAX_ST_MISALIGNMENT_ERROR = 0.15;  		/* Degree */
		IniVars.MAX_SEPARAT_PRIMARY_TOL = 0.02;    		/* Degree */
		IniVars.PARALLEL_ERROR_THRESHOLD = 2.0;			/* Was 0.3, 3000*ATD Degree */ 
		IniVars.MAG_SATURATION = 0.81;
		IniVars.MAGNITUDE_BIAS = 0.0;  
		IniVars.ATTITUDE_UNCERTAINTY = 5.0;			    /* Degree */

		IniVars.StayOutZones[GSS_EARTH].Radius = 30.0;	/* Degree */
		IniVars.StayOutZones[GSS_MOON].Radius = 25.0;	/* Degree */
		IniVars.SunStruc.Velocity.Vec[0] = 0.0;		  /* (m/sec) */
		IniVars.SunStruc.Velocity.Vec[1] = 0.0;
		IniVars.SunStruc.Velocity.Vec[2] = 3.0E4;
		IniVars.SunStruc.DistFromSC =1.5E11;		 /* (m) */ 
/*
		IniVars.StayOutZones.ECI[EARTH].Vec[0] = 0;
		IniVars.StayOutZones.ECI[EARTH].Vec[1] = 0;
		IniVars.StayOutZones.ECI[EARTH].Vec[2] = 1;

		IniVars.StayOutZones.ECI[MOON].Vec[0] = -1;
		IniVars.StayOutZones.ECI[MOON].Vec[1] = 0;
		IniVars.StayOutZones.ECI[MOON].Vec[2] = 0;
*/

		CISEXE(&AcqCatalog, LogFileName, &IniVars, VTBuffer, 
		   		CIS_MODE, STCalibON, SSCalibON, &CISOutput);

		fflush(TestOutFp);
	}

	time(&t);
	sprintf(s, "#	Finished at %s", ctime(&t));
	TestOutString(s);

	GetSunHoldAnnulus(&IniVars, InnerRadius, OuterRadius);
	GetRoughKnowledgeRadius(&IniVars, &Radius);
	printf("Radius %12.7f\n", Radius*RTD);
	for(i=0; i<NUM_ST; i++)
	  printf("InRd = %12.7f  OutRd = %12.7f\n", InnerRadius[i]*RTD, OuterRadius[i]*RTD);

Quit:
	free(ASC);
	fclose(TestInFp);
	fclose(TestOutFp);
}

/************************************************************************
	Change Log:
	$Log: Test.c,v $
	Revision 1.15  2000/03/28 22:48:26  jzhao
	Changed CISOutput format.
	
	Revision 1.14  2000/03/28 22:14:57  jzhao
	Changed priority of recommended tracks.
	Changed output format of recommended tracks.
	
	Revision 1.13  2000/03/21 23:35:46  jzhao
	Added CIS_Slew functions.
	
	Revision 1.12  2000/02/25 21:38:01  jzhao
	Get Log file name from GUI.
	
	Revision 1.11  2000/02/25 01:01:14  jzhao
	Modified for port to NT
	
	Revision 1.10  2000/02/22 18:22:31  jzhao
	Set FILE *LogFp to extern variable(get from GUI).
	
	Revision 1.9  2000/02/19 01:18:39  jzhao
	Undefined CIS_TEST
	
	Revision 1.8  2000/02/14 21:29:05  jzhao
	Add functions of GetRoughKnowledgeRadius and GetSunHoldAnnulus. They can be
	called indipendently of CISEXE.
	Move MAX_ROUGH_KNOWLEDGE_ERROR from macro define to IniVariables.
	
	Revision 1.7  2000/02/12 01:03:51  jzhao
	Added GetSunHoldAnnulus and GetRoughKnowledgeRadius for GUI.
	
	Revision 1.6  2000/02/11 19:26:11  jzhao
	Modified Log Foramt.
	
	Revision 1.5  2000/02/09 23:54:12  jzhao
	Modified for intergrating to GUI.
	
	Revision 1.4  1999/12/23 17:15:00  jzhao
	Changed PARALLEL_ERROR_THRESHOLD to 2.0 Degree.
	
	Revision 1.3  1999/12/22 01:09:21  jzhao
	Modified on create BestIDStars List.
	
	Revision 1.2  1999/12/15 01:16:12  jzhao
	Modified for Multiple cases Testing.
	
	Revision 1.1  1999/12/13 18:42:53  jzhao
	Initial Release.
	
*************************************************************************/
