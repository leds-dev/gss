/* $Id: ChooseOSC.c,v 1.11 2000/03/28 22:14:57 jzhao Exp $ */ 
/* ========================================================================
    FILE NAME:
        ChooseOSC.c

    DESCRIPTION:

    FUNCTION LIST:

 ==========================================================================

    REVISION HISTORY

 ========================================================================== */

 /* System Headers */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/* Self Defined Headers */
#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <CISGlobal.h>
#include <StarTracker.h>
#include <StarCatalog.h>
#include <IDentifyStars.h>
#include <ChooseOSC.h>

/* ======================================================= */
/*----------------
MagCompareFunFOV: Compare Magnitude for qsort.
*/

int MagCompareFunFOV(const void *p1, const void *p2)
{
    Star_HV_type *a, *b;

    a = (Star_HV_type *)p1;
    b = (Star_HV_type *)p2;

    if(a->Magnitude < b->Magnitude) return(-1);
    if(a->Magnitude > b->Magnitude) return(1);
    else return(0);
}

/*----------------
PriCompareFun: Compare Break Track Priority for qsort.
*/

int PriCompareFun(const void *p1, const void *p2)
{
    BT_type *a, *b;

    a = (BT_type *)p1;
    b = (BT_type *)p2;

    if(a->BTrackPriority < b->BTrackPriority) return(-1);
    if(a->BTrackPriority > b->BTrackPriority) return(1);
    else return(0);
}

/* --------------------------------
BinSearchDecASC: Implement Binary search on Declination.
                    Declination sroted in ascending order.

Input:
  Declination: double. The Min index of Star with Decination >= Dec.
  Stars: Array[NumStars] of StarASC_type. Stars need to be searched.
  NumStars: int. Number of Stars is going to search.
Return:
  -1          if NumStars == 0 or Declination < Stars[0].Declina.
   0          if Declination == Stars[0].Declina(first).
  NumStars-1  if Declination ==  Stars[NumStars-1].Declina(last).
  -2          if Declination > Stars[NumStars-1].Declina.
   i          if Stars[i-1].Declina < Declination <= Stars[i].declina.

*/

int BinSearchDecASC(double Declination, StarASC_type *Stars[], int NumStars)
{
    int mid, top, bottom;

    if ( NumStars == 0 || Declination < Stars[0]->Declina ) return(-1);
    if (Declination == Stars[0]->Declina) return(0);
    if (Declination == Stars[NumStars-1]->Declina) return(NumStars-1);
    if (Declination > Stars[NumStars-1]->Declina) return(-2);

    top = 0; bottom = NumStars-1;
    while(bottom-top > 1)
    {
        mid = (top+bottom)/2;
/*
        (Declination <= Stars[mid]->Declina ? bottom : top) = mid;
*/
		if (Declination <= Stars[mid]->Declina) bottom = mid;
		else									top = mid;
    }
    return(bottom);
}

/*-----------------
TrackerFOV: Calculation of cataloged stars in star tracker Field-Of-View(FOV).
            If input parameter FOVStarType is set to ASCInFOV, the
            cataloged stars are ASC Stars, otherwise, they are the OSC Stars.
Input:
    AcqStarCatalog: AcqSC_type.
    STBoresight:    VECTOR_type.
    DCM_ST2ECI:     DirCosMatrix_type.
	FOVSize:		FOVSize_type. (HMax, HMin, VMax, VMin, All in Radians!!)
    TrackerID:      int.
    FOVStarType:    int.
Output:
    CatalogStarsInFOV:  TrackerFOV_HV_type.
*/

void TrackerFOV(AcqSC_type *AcqStarCatalog,
                VECTOR_type *STBoresight,
                DirCosMatrix_type *DCM_ECI2ST,
				FOVSize_type	*FOVSize,
                TrackerFOV_HV_type *CatalogStarsInFOV,
                int TrackerID,
                int FOVStarType)
{
    VECTOR_type STLosECI, StarECI, StarST;
	DirCosMatrix_type DCM_ST2ECI;
    double STLosRA, STLosDE, DeltaRA, MinCosDE;
    double StarRA, StarDE, DiffRA, MinDE, MaxDE;
    double StarH, StarV;
	double MaxFOV_Radius; 
    int MinASCindx, Star_indx, StarsInFOV;

    CatalogStarsInFOV->NumStars = 0;
    CatalogStarsInFOV->ST_ID = TrackerID;
	MaxFOV_Radius = Max(FOVSize->VMax, FOVSize->HMax)*SQRT_2;

	MatrixTranspose(DCM_ECI2ST->DCM, DCM_ST2ECI.DCM, 3, 3, DOUBLE_DATA);

    /* Calculate the Tracker boresight Right Ascension and Declination */
    MatrixMult(DCM_ST2ECI.DCM, STBoresight->Vec, STLosECI.Vec,
                                                3, 3, 1, DOUBLE_DATA);
    ECI2RAscenDeclina(STLosECI.Vec, &STLosRA, &STLosDE, DOUBLE_DATA);

    /* Calculate the star tracker declination limits. These will be
        the boresight declination +/- MaxFOV_Radius. (Max/Min declination
        will be bounded by +/- 90 degrees)  */

    MaxDE = Min( (STLosDE + MaxFOV_Radius), PI/2.0 );
    MinDE = Max( (STLosDE - MaxFOV_Radius), -PI/2.0 );

  /* Do binary search for catalog entry with smallest allowed declination */

    MinASCindx = BinSearchDecASC(MinDE*RTD, AcqStarCatalog->Stars,
                                        AcqStarCatalog->NumStars);

    if (MinASCindx == -1) MinASCindx = 0; /* All Stars in ASC with */
										  /*Declination greater than MinDE */
    if (MinASCindx == -2) MinASCindx = AcqStarCatalog->NumStars-1;
                                         /* no stars in ASC with Declination */
                                         /* greater than MinDE */

    /* Determine Right Ascension residual, DeltaRA. (Note that Right Ascension
       changes with declination. We will assime a worst case residual -- the
       residual calculated at a declination, within the FOV, guaranteed to
       yield the largest possible residual.) For declinations close to the
       poles, DeltaRA is 180 degrees. */

    MinCosDE = Min( cos(MinDE), cos(MaxDE) );
    if ( MinCosDE > MaxFOV_Radius/PI )
        DeltaRA = MaxFOV_Radius/MinCosDE;
    else
        DeltaRA = PI;

    /* Search ASC for ASC/OSC stars within declination window, [MinDE, MaxDE] */

    for (Star_indx=MinASCindx; Star_indx<AcqStarCatalog->NumStars; Star_indx++)
    {
        StarRA = DTR * AcqStarCatalog->Stars[Star_indx]->RAscen;
        StarDE = DTR * AcqStarCatalog->Stars[Star_indx]->Declina;

        /* If the declination of the Star_indx star exceeds the Maximum
           allowable declination, we have exhausted the list of ASC entries
           that may qualify. Exit this loop */

        if (StarDE > MaxDE) break;

        /* Ignore this entry if it is not an OSC Star and FOVStarType is
            set to OSCInFOV. */

        if ( (FOVStarType == OSCInFOV) &&
             (AcqStarCatalog->Stars[Star_indx]->OSC_ID == 0) ) continue;

        /* Calculate the difference between the catalog entry's Right Ascension
           [0, 360] and the tracker boresight Right Ascension [0, 360]. This
           difference must be in the range [-360, 360]; increment or decrement
           by 360, if necessary, to put it in the range [-180, 180]. */ 

        DiffRA = StarRA - STLosRA;

        if ( DiffRA > PI ) DiffRA -= 2*PI;
        else if ( DiffRA <= -PI ) DiffRA += 2*PI;

        /* Perform Right Ascension Check */
        if ( fabs(DiffRA) < DeltaRA )
        {
            /* At this point, it is known that the ASC entry has an RA/DE
               that may allow it to be within the tracker FOV. Determine
               whether it is in the FOV */

            RAscenDeclina2ECI(StarRA, StarDE, StarECI.Vec, DOUBLE_DATA);
       		MatrixMult(DCM_ECI2ST->DCM, StarECI.Vec, StarST.Vec,
                        3, 3, 1, DOUBLE_DATA);
            STVector2HVPos(StarST.Vec, &StarH, &StarV);

            if (StarH <= FOVSize->HMax && StarH >= FOVSize->HMin &&
                StarV <= FOVSize->VMax && StarV >= FOVSize->VMin)
            {
                /* At this point, it has been shown that the entry is in
                   the tracker FOV. Store catalog entry data */

                StarsInFOV = CatalogStarsInFOV->NumStars;
                CatalogStarsInFOV->Stars[StarsInFOV].HPos = StarH;
                CatalogStarsInFOV->Stars[StarsInFOV].VPos = StarV;
                CatalogStarsInFOV->Stars[StarsInFOV].Magnitude =
                    AcqStarCatalog->Stars[Star_indx]->MagnitudeST[TrackerID-1];
                CatalogStarsInFOV->Stars[StarsInFOV].OSC_ID =
                                AcqStarCatalog->Stars[Star_indx]->OSC_ID;
                CatalogStarsInFOV->Stars[StarsInFOV].ASC_ID = Star_indx + 1;
                CatalogStarsInFOV->Stars[StarsInFOV].BSS_ID =
                                AcqStarCatalog->Stars[Star_indx]->BSS_ID;

                CatalogStarsInFOV->NumStars++;

                /* Output a maximum of MAXNUM_STARS_FOV stars in FOV */

                if ( CatalogStarsInFOV->NumStars >= MAXNUM_STARS_FOV ) break;
            }

        } /*endof (fabs(DiffRA < DeltaRA)) */

    } /* for loop */

    /* Sort CatalogStarsInFOV so as to put the brightest stars in the
       first columns. (Of course, ig there are less than 2 elements of
       CatalogStarsInFOV, no sort is needed.) */

    if (CatalogStarsInFOV->NumStars > 1)
        qsort(CatalogStarsInFOV->Stars, CatalogStarsInFOV->NumStars,
                            sizeof(Star_HV_type), MagCompareFunFOV);
}

/* --------------------------------
BreakTrackSet:  This function is used to determine which virtual trackers(vt's)
                should be commanded to break track, and in wich order. The
                procedure here is to break track only for vt's NOT tracking
                a star identifiable as an OSC entry.
                This condition is necessary, but not sufficient, for breaking
                track. Track will only be broken on such a vt if there is an
                OSC star available - i.e., not currently being tracked. As
                there may be fewer OSC stars available than there are vt's
                on which track should be broken, the vt's must be sorted by
                priority preparatory to breaking track.
Input:
Output:
    BreakTrack: BTList_type. record Number of VTs will be break track,
                ID number and break track priority of each VT on the List.
                List sorted by break track priority. (Highest priority first)
    TrackedOSCStars: TrackerFOV_HV_type.  A list of stars in tracking VTs which
    		were ID'ed as OSC stars
*/

void BreakTrackSet(VTBuffer_type (*VTBuff)[NUM_VT],
		   IDStarOutput_type *IDStars,
                   StatisticsVT_type *Centroid,
                   BTList_type *BreakTrack,
		   TrackerFOV_HV_type *TrackedOSCStars)
{
    int CurrVT, IDStar_indx, MatchID;
    double StarH, StarV, Priority ;

    BreakTrack->NumVTs = 0;
    TrackedOSCStars->NumStars = 0;

    /* Cycle through the vt's to determine which one should be commanded to
        break track */

    for (CurrVT=1; CurrVT<=NUM_VT; CurrVT++)
    {
        /* Any Star Identified by current VT? */
        MatchID = 0;
        for (IDStar_indx=0; IDStar_indx<IDStars->NumStars; IDStar_indx++)
        {
            if (IDStars->Stars[IDStar_indx].VT_ID == CurrVT)
                MatchID = IDStar_indx+1;
        }

        if (MatchID == 0)
        {
		/* No ASC was identified as corresponding to the star(if any)
		   tracked by this VT because user did no select the VT. Break track
		   on this VT, with a maximum priority */

		if ( ( (*VTBuff)[CurrVT-1].UserSel == FALSE ) && ( (*VTBuff)[CurrVT-1].Tracked == TRUE ) )
            		BreakTrack->VT[BreakTrack->NumVTs].BTrackPriority = -2;

			/* No ASC was identified as corresponding to the star(if any)
			   tracked by this VT because CIS can not ID. Break track
			   on this VT, with a second priority */

		else if ( (*VTBuff)[CurrVT-1].UserSel == TRUE )
            		BreakTrack->VT[BreakTrack->NumVTs].BTrackPriority = -1;

			/* No ASC was identified as corresponding to the star(if any)
			   tracked by this VT because the VT is not tracking. Break track
			   on this VT, with the lowest priority */

		else if ( (*VTBuff)[CurrVT-1].Tracked == FALSE )
			{
            		BreakTrack->VT[BreakTrack->NumVTs].BTrackPriority = 1000;
			}

           	BreakTrack->VT[BreakTrack->NumVTs].VT_ID = CurrVT;
           	BreakTrack->NumVTs++;
        }
        else
        {
        
            /* Current VT has been identified as tracking a star corresponding
               to an ASC entry */

            STVector2HVPos(IDStars->Stars[MatchID-1].STFrame, &StarH, &StarV);

            if (IDStars->Stars[MatchID-1].OSC_ID == 0)
            {
            /* At this point, CurrVT has been identified as tracking a star
               corresponding to an ASC entry, but not an OSC entry. Break
               track on this star with a priority corresponding to proximity
               to the centroid; the closer the star is to the centroid,
               the more likely a break track will be issued */

                Priority = (StarH - Centroid->Mean_H) *
                           (StarH - Centroid->Mean_H) +
                           (StarV - Centroid->Mean_V) *
                           (StarV - Centroid->Mean_V);

                BreakTrack->VT[BreakTrack->NumVTs].BTrackPriority = Priority;
                BreakTrack->VT[BreakTrack->NumVTs].VT_ID = CurrVT;
                BreakTrack->NumVTs++;
            }
	    else
	    {
 	    /* Current VT has been identified as tracking a star corresponding to an OSC entry */
		TrackedOSCStars->Stars[TrackedOSCStars->NumStars].HPos = StarH;
		TrackedOSCStars->Stars[TrackedOSCStars->NumStars].VPos = StarV;
		TrackedOSCStars->NumStars++;
	    }
	    
        } /* endof else */

    } /* endof for (CurrVT.. ) */

    /* Sort BreakTrack by Break Track Priority, Highest priority first */

    if (BreakTrack->NumVTs > 1)
        qsort(BreakTrack->VT, BreakTrack->NumVTs, sizeof(BT_type), PriCompareFun);
}

/* ---------------------
ChooseOSCStarForVT:    Break track on certain stars,currently being tracked,
                        in favor of stars in the On-Board Star Catalog(OSC).
                        Note: this function does not actually break track,
                        it merely "recommends" which virtual trackers should
                        be commanded to break track, and where they should
                        attempt to establish track on an OSC star.
Input:
	VTBuff:		Array[NUM_ST][NUMVT] of VTBuffer_type.
	STBoresight:	VECTOR_type. 
	IDStars:	Array[NUM_ST] of IDStarOutput_type.
	CentroidIDStar:	Array[NUM_ST] of StatisticsVT_type.
	AcqStarCatalog:	AcqSC_type. 
	DCM_ECI2BDY:	DirCosMatrix_type.
	DCM_BDY2ST:	Array[NUM_ST] of DirCosMatrix_type.
Output:
	RecommendTrack:	Array[NUM_ST] of RecommendVT_type. This array containing
			information on OSC stars NOT currently being tracked,
			but recommended to be tracked. 
*/

void ChooseOSCStarForVT( VTBuffer_type VTBuff[][NUM_VT],
			 VECTOR_type *STBoresight,
			 IDStarOutput_type IDStars[],
			 StatisticsVT_type CentroidIDStar[],
			 AcqSC_type *AcqStarCatalog,
			 DirCosMatrix_type *DCM_ECI2BDY,
			 DirCosMatrix_type DCM_BDY2ST[],
			 RecommendVT_type RecommendTrack[])
{
	int ST_indx, OSC_indx, IDs_indx, Match, i;
	BTList_type BreakTrack;
	TrackerFOV_HV_type TrackedOSCStars;
	TrackerFOV_HV_type OSCStarInFOV; 
	TrackerFOV_HV_type updated_OSCStarInFOV; 
	DirCosMatrix_type DCM_ECI2ST;
	FOVSize_type FOVSize;
	int OSC_indx2, OSC_indx3;
	int sameColFound = FALSE, trackedVTFound = FALSE;
	double column, col_width_deg;
	int updated_indx, tracked_indx;
    
	FOVSize.HMax = SMS_FOV;
	FOVSize.HMin = -1 * SMS_FOV;
	FOVSize.VMax = SMS_FOV;
	FOVSize.VMin = -1 * SMS_FOV;

	col_width_deg = PIXEL_DEG * OVERLAP_PIXELS;

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		RecommendTrack[ST_indx].NumVTs = 0;
		RecommendTrack[ST_indx].ST_ID = IDStars[ST_indx].ST_ID;
		
		updated_OSCStarInFOV.NumStars = 0;

		MatrixMult(DCM_BDY2ST[ST_indx].DCM, DCM_ECI2BDY->DCM, DCM_ECI2ST.DCM, 3, 3, 3, DOUBLE_DATA);
		
		/* Not Only break track on stars tracked by reference trackers,
		   so commanding Direct Field-of-View mode is more likely to
		   result in establishing track	*/

		/* Determine which VT's should be commanded to break track,
		   and in which order. The pocedure here is to break track
		   only for VT's not currently tracking stars identifiable as 
		   OSC entries. */

		BreakTrackSet(&(VTBuff[ST_indx]), &(IDStars[ST_indx]), 
			      &(CentroidIDStar[ST_indx]), &BreakTrack, &TrackedOSCStars);
			        
		if (BreakTrack.NumVTs > 0)
		{
			/* A VT's not tracking a star identifiable as an OSC entry
			   is a necessary, but not sufficient condition for break
			   tracking. Track will only be broken on such a VT if there 
			   is an OSC star available - i.e., an OSC star not currently
			   being tracked. As there may be fewer OSC stars available than 
			   there are VT's on which track should be broken,
			   the VT's must be sorted by priority preparatory to breaking 
			   track.

			   Create a list of OSC stars in the tracker FOV. 	*/

			TrackerFOV(AcqStarCatalog, STBoresight, &DCM_ECI2ST, &FOVSize,
	    				&OSCStarInFOV, IDStars[ST_indx].ST_ID, OSCInFOV);
			updated_indx = 0;
			for (OSC_indx=0; OSC_indx<OSCStarInFOV.NumStars; OSC_indx++)
			{
			
			   /* eliminate from OSCStarInFOV any star in the same column
				  as a brighter star also listed in OSCStarsInFOV */
			   sameColFound = FALSE;
			   for (OSC_indx2=0; OSC_indx2 < OSC_indx; OSC_indx2++)
			   {
				column = RTD * ( OSCStarInFOV.Stars[OSC_indx].HPos - 
					         OSCStarInFOV.Stars[OSC_indx2].HPos );
				if (column < 0)
					column *= -1;
				if (column <= col_width_deg)
					sameColFound = TRUE;			
			   }
			   
			   /* eliminate from OSCStarInFOV any star in the same column
			   	as a tracked OSC star, regardless of magnitudes */
			   trackedVTFound = FALSE;
			   for (tracked_indx = 0; tracked_indx < TrackedOSCStars.NumStars; tracked_indx++)
			   {
				column = RTD * ( OSCStarInFOV.Stars[OSC_indx].HPos - 
						 TrackedOSCStars.Stars[tracked_indx].HPos );
				if (column < 0)
					column *= -1;
				if (column <= col_width_deg)
					trackedVTFound = TRUE;
			   }

			   if ( (sameColFound == FALSE) && (trackedVTFound == FALSE) )
			   {
				updated_OSCStarInFOV.NumStars++;
				memcpy(&(updated_OSCStarInFOV.Stars[updated_indx++]), 
					&(OSCStarInFOV.Stars[OSC_indx]),
					sizeof(Star_HV_type)); 	
				updated_OSCStarInFOV.ST_ID = OSCStarInFOV.ST_ID;
			   }
			}
			
			/* Create a List of RecommendTrack */
			
			for (OSC_indx=0; OSC_indx<updated_OSCStarInFOV.NumStars; OSC_indx++)
			{
				Match = FALSE;
				for (IDs_indx=0; IDs_indx<IDStars[ST_indx].NumStars; 
															IDs_indx++)
				{
					if (IDStars[ST_indx].Stars[IDs_indx].BSS_ID ==
						updated_OSCStarInFOV.Stars[OSC_indx].BSS_ID)	
						Match = TRUE;
				}
					
				if (!Match)
				{
					i = RecommendTrack[ST_indx].NumVTs; 
					memcpy(&(RecommendTrack[ST_indx].Stars[i]), 
						&(updated_OSCStarInFOV.Stars[OSC_indx]),
						sizeof(Star_HV_type)); 	
					RecommendTrack[ST_indx].Stars[i].VT_ID = BreakTrack.VT[i].VT_ID;
					RecommendTrack[ST_indx].NumVTs++;
				}
				
				if (RecommendTrack[ST_indx].NumVTs == BreakTrack.NumVTs)	
					break; /* break OSC_indx loop if no more VTs needing to break*/
					
			} /* endof OSC_indx Loop */
			
	
		} /* endof if (BreakTrack.NumVTs > 0) */

	}/* end of ST_indx loop */	

}

/*********************************************************************************
	Change Log:
	$Log: ChooseOSC.c,v $
	Revision 1.11  2000/03/28 22:14:57  jzhao
	Changed priority of recommended tracks.
	Changed output format of recommended tracks.
	
	Revision 1.10  2000/02/23 22:55:42  jzhao
	Modified fo port to NT
	
	Revision 1.9  2000/02/23 22:45:54  jzhao
	Modified for port to NT
	
	Revision 1.8  2000/02/23 22:11:19  jzhao
	Added #include <string.h>
	
	Revision 1.7  2000/02/14 23:50:24  jzhao
	Changed ASC star's magnitude adjustment to adjusted magnitude.
	
	Revision 1.6  2000/02/09 23:54:11  jzhao
	Modified for intergrating to GUI.
	
	Revision 1.5  2000/01/06 22:44:16  jzhao
	Minor change to prevent Math error.
	
	Revision 1.4  1999/12/23 21:09:32  jzhao
	Modified calling argument list for TrackerFOV and made relevant changes in
	ChooseOSCStarForVT.
	
	Revision 1.3  1999/12/23 17:16:30  jzhao
	Fixed bug in TrackerFOV.
	
	Revision 1.2  1999/12/16 20:05:16  jzhao
	Remove Flag "ValidRef" from ChooseOSCStarForVT.
	
	Revision 1.1  1999/12/13 18:42:51  jzhao
	Initial Release.
	
**********************************************************************************/
