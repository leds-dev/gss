/* $Id: IDentifyStars.c,v 1.9 2000/03/15 21:21:22 jzhao Exp $ */
/* ==================================================================

	FILE NAME:
		IDentifyStars.c

 ===================================================================

	REVISION HISTORY

 =================================================================== */ 
/* System Header */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Self Defined Header */
#include <MathFnc.h>
#include <MessageIO.h>
#include <CISGlobal.h>
#include <StarTracker.h>
#include <StarCatalog.h>
#include <IDentifyStars.h>

/* Macros */

#define ERROR_TEXT "in IDentifyStars"

/* ================================================================= */

/* --------------------------------
BinSearchMagSubSC: Implement Binary search on Magnitude.
			  Magnitude sroted in ascending order.

Input:	
	Mag:	double. The Min index of Star with Magnitude >= Mag.   
	Stars: Array[NumStars] of ECI_SC_type. Stars need to be searched.
	NumStars: int. Number of Stars is going to search.
Return:
	-1 			if NumStars == 0 or Mag < Stars[0].Magnitude.
	0			if Mag == Stars[0].Magnitude(first).
	NumStars-1  if Mag ==  Stars[NumStars-1].Magnitude(last).
	-2			if Mag > Stars[NumStars-1].Magnitude.
	i			if Stars[i-1].Magnitude < Mag <= Stars[i].Magnitude. 

*/

int BinSearchMagSubSC(double Mag, StarSubSC_type *Stars[], int NumStars)
{
	int mid, top, bottom;
	   
	if ( NumStars == 0 || Mag < Stars[0]->Magnitude ) return(-1);
	if (Mag == Stars[0]->Magnitude) return(0);
	if (Mag == Stars[NumStars-1]->Magnitude) return(NumStars-1);
	if (Mag > Stars[NumStars-1]->Magnitude) return(-2);

	top = 0; bottom = NumStars-1;
	while(bottom-top > 1)
	{
		mid = (top+bottom)/2;
/*
		(Mag <= Stars[mid].Magnitude ? bottom : top) = mid;
*/
		if (Mag <= Stars[mid]->Magnitude) bottom = mid;
		else							 top = mid;
	}
	return(bottom);
}

/* ---------------------------------
GetMaxMagnitudeResidue:	Calculates Max. Magnitude residual from Sub-catalog.

Input:
	SubStarCatalog.	SubSC_type.
return:	
	MaxMagRes.	double.
*/

double GetMaxMagnitudeResidue(SubSC_type *SubStarCatalog)
{
	int Star_indx;
	double MaxMagRes;
	
	MaxMagRes = SubStarCatalog->Stars[0]->MagResidual;
	for (Star_indx=1; Star_indx<SubStarCatalog->NumStars; Star_indx++)
	{
		if (SubStarCatalog->Stars[Star_indx]->MagResidual > MaxMagRes)
			MaxMagRes = SubStarCatalog->Stars[Star_indx]->MagResidual;
	}
	return(MaxMagRes);
}
/* ---------------------------------
GetDirCosMatrixPrimaryST2ST: Calculates the Direction Cosine Matrix for each 
							 Star Tracker that translates the Primary Star 
							 tracker frame to each Star tracker frame. 

Input:
	DCM_ST2BDY:	Array[NUM_ST] of DircCosMatrix_type. DCM of ST frame to 
				Body Frame.
	PrimaryST_ID:	int. ST_ID of primary Star Tracker.
Output:
	DCM_PrimaryST2ST:	Array[NUM_ST] of DirCosMatrix_type. DCM of Primary Star
					Tracker ST frame to other Star Trackers ST frame.
*/
void GetDirCosMatrixPrimaryST2ST(DirCosMatrix_type DCM_ST2BDY[], 
							 	 int PrimaryST_ID,
							 	 DirCosMatrix_type DCM_PrimaryST2ST[])
{
	int St_indx;
	double tmp[9];

	for (St_indx=0; St_indx<NUM_ST; St_indx++)
	{
		if (PrimaryST_ID == St_indx+1) 		/* primary tracker self */
			MatrixIdent(DCM_PrimaryST2ST[St_indx].DCM, 3, DOUBLE_DATA);		
		else
		{
			MatrixTranspose(DCM_ST2BDY[St_indx].DCM, tmp, 3, 3, DOUBLE_DATA);
			MatrixMult(tmp, DCM_ST2BDY[PrimaryST_ID-1].DCM, 
						DCM_PrimaryST2ST[St_indx].DCM, 3, 3, 3, DOUBLE_DATA);
		}
	}
}
/* --------------------------------
GetMaxAngBetSTCornerPrimaryStar: Calculates Max. separation angle between each
								corner of ST and Primary Star.
Input:
	PriMaryStarST:	Array[3] of double.  Primary star in it's STFrame.
	DCM_PrimaryST2ST:	Array[NUM_ST] of DirCosMatrix_type. DCM of Primary Star
						Tracker ST frame to other Star Trackers ST frame.
Output:
	MaxAngle:	array[NUM_ST] of double. Max. separation angle(radians) between
				each corner of ST and Primary star. 
*/
void GetMaxAngBetSTCornerPrimaryStar(double  PrimaryStarST[], 
							   		DirCosMatrix_type DCM_PrimaryST2ST[],
							   		double MaxAngle[])
{
	int St_indx, Corner_indx;
	double MaxAng, Angle, tmp[9];
	VECTOR_type Corner[4], CornerInPrimaryST;

	STFOV_CornerPos_STFrame(Corner);
	for (St_indx=0; St_indx<NUM_ST; St_indx++)
	{
		MaxAng = 0;
		for(Corner_indx=0; Corner_indx<4; Corner_indx++)
		{
			MatrixTranspose(DCM_PrimaryST2ST[St_indx].DCM, tmp, 
							3, 3, DOUBLE_DATA);
			MatrixMult(tmp, Corner[Corner_indx].Vec, CornerInPrimaryST.Vec, 
						3, 3, 1, DOUBLE_DATA); 
			AngleBetVectors(CornerInPrimaryST.Vec, PrimaryStarST,
							&Angle, DOUBLE_DATA);
			if(Angle > MaxAng)	MaxAng = Angle;
		}

		MaxAngle[St_indx] = MaxAng;
	}
}

/* --------------------------------
Input:
	PrimaryST_ID:	int. ST_ID of Primary Star Tracker.	
Output:
	SepTolerance[]:	Array[NUM_ST] of double. Star Separation tolerance for 
					direct matching(radians).
*/

void GetSeparationTolerance(int PrimaryST_ID, double SepTolerance[])
{
	int St_indx;
	for (St_indx=0; St_indx<NUM_ST; St_indx++)
	{
		if (PrimaryST_ID == St_indx+1)
			SepTolerance[St_indx] = MAX_SEPARAT_PRIMARY_TOL;
		else
			SepTolerance[St_indx] = MAX_SEPARAT_NONPRIMARY_TOL;
	}
}

/*---------------------------------
GetPositionToleranceLoose:	Calculate position tolerance of loose direct match.
Input:
	MaxAngleSTCornerPrimaryStar:	array[NUM_ST] of double. Max. separation 
									angle(radians) between each corner of ST
									and Primary star. 
	SepTolerance:	Array[NUM_ST] of double. Separation tolerance for 
					direct match(radians).
	MaxRotTolerance:	double. Max Rotation Tolerance(radians).
Output:
	PosTolerance:	Array[NUM_ST] of double. Position Tolerance for 
					direct match(radians).
*/

void GetPositionToleranceLoose(double MaxAngleSTCornerPrimaryStar[], 
								double SepTolerance[],
								double MaxRotTolerance, 
								double PosTolerance[]) 
{
	int St_indx;

	for (St_indx=0; St_indx<NUM_ST; St_indx++)
		PosTolerance[St_indx] = SepTolerance[St_indx] + 
			MaxRotTolerance* sin(MaxAngleSTCornerPrimaryStar[St_indx]);
}

/* ----------------------------------
EliminateDuplicatedIDStars: Eliminate cases where two different stars
							in the tracker FOV have been identified
							as the same star.
Input:
	IDStars:	IDStarOutput_type.
Output:
	IDStars:	IDStarOutput_type.
*/

int EliminateDuplicatedIDStars(IDStarOutput_type *IDStars)
{
	int statu = 0;
	int i, j, count=0;
	int NumIDStars;
	int *eliminate_star;

	NumIDStars = IDStars->NumStars;
	if (NumIDStars < 2) return(statu);

	eliminate_star = calloc(NumIDStars, sizeof(int));
	if( eliminate_star == NULL )
	{
		statu = MEM_ERROR;
		DisplayMessage(statu, ERROR_TEXT);
		LogMessage(statu, ERROR_TEXT);
		goto quit;
	}

	for (i=0; i<NumIDStars; i++) eliminate_star[i] = FALSE;

	/* First, tag all eliminate cases by set eliminate_star[i] = TRUE */
	for (i=0; i<NumIDStars-1; i++)
	for (j=i+1; j<NumIDStars; j++)
	{
		if (IDStars->Stars[i].ASC_ID == IDStars->Stars[j].ASC_ID)
		{
			eliminate_star[i] = TRUE;
			eliminate_star[j] = TRUE;
		}
	}

	/* Eliminate from the IDStars all stars for which eliminate_star=TRUE */
	
	for (i=0; i<NumIDStars; i++)
	{
		if (!eliminate_star[i])
		{
			IDStars->Stars[count].STFrame[0] =  IDStars->Stars[i].STFrame[0];
			IDStars->Stars[count].STFrame[1] =  IDStars->Stars[i].STFrame[1];
			IDStars->Stars[count].STFrame[2] =  IDStars->Stars[i].STFrame[2];
			IDStars->Stars[count].ASC_ID = IDStars->Stars[i].ASC_ID; 
			IDStars->Stars[count].OSC_ID = IDStars->Stars[i].OSC_ID;
			IDStars->Stars[count].BSS_ID = IDStars->Stars[i].BSS_ID;
			IDStars->Stars[count].SubSC_ID = IDStars->Stars[i].SubSC_ID;
			IDStars->Stars[count].VT_ID = IDStars->Stars[i].VT_ID;
			IDStars->Stars[count].MagnitudeCat = IDStars->Stars[i].MagnitudeCat;
			IDStars->Stars[count].MagnitudeTM = IDStars->Stars[i].MagnitudeTM;
			count ++;
		}
	}
	IDStars->NumStars = count;

quit:
	free(eliminate_star);
	return(statu);
}

/* ------------------------------
SearchMinMaxMagIndx: Perform binary search to determine min/max sub-catalog 
					 indices for Canditate determination/Star match. The search
					 object is magnitude.
					 Search for Min_Indx and Max_Indx which make Magnitude 
					 within (CoreMagnitude - MaxMagRes) and 
							(CoreMagnitude + MaxMagRes)
Inputrch:
	SubStarCatalog:	SubSC_type.	
	MaxMagRes:		double. Max. Magnitude residual.
	CoreMagnitude:	double.

Output:	
	*Min_Indx:	int. 
	*Max_Indx:	int.
*/

void SearchMinMaxMagIndx(SubSC_type *SubCatalog, 
						 double MaxMagRes, 
						 double CoreMagnitude,
						 int *Min_Indx, int *Max_Indx)
{
	double Magnitude;

	Magnitude = CoreMagnitude - MaxMagRes;
	*Min_Indx = BinSearchMagSubSC(Magnitude, SubCatalog->Stars, 
								SubCatalog->NumStars);

	Magnitude = CoreMagnitude + MaxMagRes;
	*Max_Indx = BinSearchMagSubSC(Magnitude, SubCatalog->Stars, 
						  		SubCatalog->NumStars); 

	if ( (*Max_Indx > 1) && (*Max_Indx != SubCatalog->NumStars-1) )
		*Max_Indx--;

	if(( *Min_Indx == -2 ) || ( *Max_Indx == -1 )) 
	{	/* There are NO Stars within Magnitude range */

		*Min_Indx = 0; *Max_Indx = 0;	
	}
	else 
	{
		if ( *Min_Indx == -1 ) *Min_Indx = 0;
		if ( *Max_Indx == -2 ) *Max_Indx = SubCatalog->NumStars-1;
	}
}			
/* ------------------------------
MagnitudeCheck: Apply Magnitude check(one of CIS criterion)on Identify 
				candidate star and Identify match star.	
Input:
	SCStarMag:	double. Magnitude of Star Catalog star which is assumed to 
						match the tracked star.
	STStarMag:	double. Magnitude of tracked star. 
	MagRes:		double. Magnitude Residual of Satr Catalog star.
Return:
	PassMagCheck:	int. FALSE/TRUE. 
*/
int MagnitudeCheck(double SCStarMag, double STStarMag, double MagRes)
{
	int PassMagCheck = FALSE;

	if( (fabs(SCStarMag-STStarMag) <= MagRes) || 
		((SCStarMag <= MAG_SATURATION) && (STStarMag <= MAG_SATURATION)) ) 
		PassMagCheck = TRUE; 
	return(PassMagCheck);
}
/* --------------------------------
IDentifyStars: Uses a modification of "direct matching" for star identification.
				An observed star is identified as a catalog entry if the entry 
				uniquely satisfies the following:
				1) The entry has an instrument magnitude within a certain value
					(the magnitude tolerance) of the observed star's instrument
					magnitude.
				
				2) The entry and the position of the observed star in the ECI 
					frame have an angular separation of no more than a certain 
					amont(the position tolerance).

				3) The separation between the observed star and "Primary" star
					must match the separation between the catalog entry and 
					"Primary Candidate", to within a certain value
					(the separation tolerance).

Input:
	SubCatalog:		SubSC_type.	 Sub-Star catalog for current tracker.
	TrackedStars: 	TrackerOutputST_type. Stars tracked by the tracker.
	DCM_ST2ECI:		DirCosMatrix_type.	Estimated Direction Cosine Matrix 
					for STframe to ECI transformation.
	RefCurrentST:	VECTOR_type. Referance in current tracker Frame.	
	RefECI:			array[3] of double. Referance in ECI frame.
	MaxMagRes:		double. Max magnitude residual of SubCatalog Stars.
	PosTolerance:	double. 
	SepTolerance:	double.

Output:
	IDStars:		IDStarOutput_type. Identified Stars.

*/

void IDentifyStars(SubSC_type *SubCatalog,
				   TrackerOutputST_type *TrackedStars,
				   DirCosMatrix_type *DCM_ST2ECI,  
				   VECTOR_type *RefCurrentST,
				   double RefECI[],
				   double MaxMagRes,
				   double PosTolerance,
				   double SepTolerance,
				   IDStarOutput_type *IDStars)
{
	int Star_indx, SC_indx, NumMatchStars, ID_indx;
	int min_SCindx, max_SCindx;
	int PassPosCheck, PassSepCheck, PassMagCheck; 
	int MatchStar_SCindx, PreMatch_SCindx;
	double STStarMag, SCStarMag;
	double PosDiff, delta1, delta2; 
	double MagRes;
	double STStarECI[3], *SCStarECI, *STStarST;

	IDStars->NumStars = 0;	
	IDStars->ST_ID = TrackedStars->ST_ID;

	/* For each tracked star find matching star in Sub-Catalog */

	for(Star_indx=0; Star_indx<TrackedStars->NumStars; Star_indx++)
	{
		/* Calculates the LOS vector of the tracked star in ECI frame */
		
		STStarST = (double *)TrackedStars->Stars[Star_indx].STFrame;
		MatrixMult(DCM_ST2ECI, STStarST, STStarECI, 
					3, 3, 1, DOUBLE_DATA);

		/* Perform binary search to determine min/max sub-catalog indices for 
			a star match. The search object in magnitude. */

		STStarMag = TrackedStars->Stars[Star_indx].Magnitude;
		SearchMinMaxMagIndx(SubCatalog, MaxMagRes, STStarMag,	
							&min_SCindx, &max_SCindx);

		/* Search SubCatalog for stars within the magnitude window, and
			determine if they satisfy magnitude, position and separation 
			criteria. */

		NumMatchStars = 0;
		for(SC_indx=min_SCindx; SC_indx<=max_SCindx; SC_indx++) 
		{
			if ( NumMatchStars < 2 )  
			{
				PassMagCheck = FALSE;
				PassPosCheck = FALSE;
				PassSepCheck = FALSE;

				/* Magnitude check */

				MagRes = SubCatalog->Stars[SC_indx]->MagResidual;
				SCStarMag = SubCatalog->Stars[SC_indx]->Magnitude;
				PassMagCheck = MagnitudeCheck(SCStarMag, STStarMag, MagRes);

				/* Position Check */
				if(PassMagCheck)
				{
					SCStarECI = (double *)SubCatalog->Stars[SC_indx]->ECI; 
					AngleBetVectors(STStarECI, SCStarECI, &PosDiff, DOUBLE_DATA);
					if ( PosDiff <= PosTolerance ) PassPosCheck = TRUE;
				}

				/* Separation check */
				if(PassMagCheck && PassPosCheck)
				{
					AngleBetVectors(RefECI, SCStarECI, &delta1, 
														DOUBLE_DATA);
					AngleBetVectors(RefCurrentST->Vec, STStarST, &delta2, 
														DOUBLE_DATA);
					if ( fabs(delta1 - delta2) <= SepTolerance ) 
						PassSepCheck = TRUE;
				}

				/* Count and Record matched star. 
					Find if there is multiple match. */
				if (PassMagCheck && PassPosCheck && PassSepCheck)
				{
					if (NumMatchStars > 0) PreMatch_SCindx = MatchStar_SCindx;

					MatchStar_SCindx = SC_indx;
					NumMatchStars++;
				}

			} /* end if ( NumMatchStars < 2 ) */

		}/* end for (SC_indx=min_SCindx, SC_indx<max_SCindx; SC_indx++) */

		/* Output Matched star only if unique(NumMatchStars = 1) match */

		if ( NumMatchStars == 1)
		{
			ID_indx = IDStars->NumStars;
			IDStars->Stars[ID_indx].STFrame[0] = STStarST[0];
			IDStars->Stars[ID_indx].STFrame[1] = STStarST[1];
			IDStars->Stars[ID_indx].STFrame[2] = STStarST[2];
			IDStars->Stars[ID_indx].ASC_ID = 
						SubCatalog->Stars[MatchStar_SCindx]->ASC_ID;
			IDStars->Stars[ID_indx].OSC_ID =
						SubCatalog->Stars[MatchStar_SCindx]->OSC_ID;
			IDStars->Stars[ID_indx].BSS_ID =
						SubCatalog->Stars[MatchStar_SCindx]->BSS_ID;
			IDStars->Stars[ID_indx].SubSC_ID = MatchStar_SCindx + 1; 
			IDStars->Stars[ID_indx].VT_ID = 
						TrackedStars->Stars[Star_indx].VT_ID;
			IDStars->Stars[ID_indx].MagnitudeCat = 
						SubCatalog->Stars[MatchStar_SCindx]->Magnitude;
			IDStars->Stars[ID_indx].MagnitudeTM = 
						TrackedStars->Stars[Star_indx].Magnitude;
			IDStars->NumStars ++;

		} /* end if ( NumMatchStars == 1) */

	} /* end for (Star_indx=0; Star_indx<TrackedStars->NumStars; Star_indx++) */

	/* Eliminate cases where two different stars in the tracker FOV have
		been identified as the same star. */

	EliminateDuplicatedIDStars(IDStars);
}
/* -----------------
int TotalIDStars: Calculates total stars IDentified.

Input:
    IDstarsST: Array[NUM_ST} of IDStarOutput_type.
Return:
    Total Number of Stars IDentified.
*/
int TotalIDStars(IDStarOutput_type IDStarsST[])
{
    int ST_indx;
    int TotalStars = 0;

    for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
        TotalStars += IDStarsST[ST_indx].NumStars;

    return(TotalStars);
}

/*--------------------------------
IDStarPosStatistics:    Calculates Mean, Standard Deviation and Spread
                            of a set stars Postion (H-V position).

Input:
    IDStars: IDStarOutput_type. ID'stars in STFrame.
Output:
    PosStatistics:  StatisticsVT_type. Mean Std. Deviation and Spread of star
                                         postion in H-V.
*/

int IDStarPosStatistics(IDStarOutput_type *IDStars,
                        StatisticsVT_type   *PosStatistics)
{
    int err_msg=0, Star_indx;
	double HPos, VPos; 
    double mean_H=0, mean_V=0, var_H=0, var_V=0;

    if (IDStars->NumStars == 0)
    {
        err_msg = MATH_ERROR;
		DisplayMessage(err_msg, ERROR_TEXT);
		LogMessage(err_msg, ERROR_TEXT);
        return (err_msg);
	}
    for(Star_indx=0; Star_indx<IDStars->NumStars; Star_indx++)
    {
	/* Convert Star vector from ST frame to (H, V)position */
		STVector2HVPos(IDStars->Stars[Star_indx].STFrame, &HPos, &VPos); 

        mean_H += HPos;
        mean_V += VPos;
        var_H += HPos * HPos;
        var_V += VPos * VPos;
    }

    mean_H /= IDStars->NumStars;
    mean_V /= IDStars->NumStars;

    var_H /= IDStars->NumStars;
    var_V /= IDStars->NumStars;
    var_H -= mean_H*mean_H;
    var_V -= mean_V*mean_V;

    PosStatistics->Mean_H = mean_H;
    PosStatistics->Mean_V = mean_V;
    PosStatistics->StdDev_H = sqrt(var_H);
    PosStatistics->StdDev_V = sqrt(var_V);
    PosStatistics->Spread = sqrt(IDStars->NumStars*(var_H + var_V));

    return(err_msg);
}

/*--------------
*/

void CalNumIDStarsOSC(IDStarOutput_type IDStars[], int NumIDStarsOSC[],
													int *TotalIDStarsOSC)
{
	int ST_indx, Star_indx;
	int TotalOSC = 0;

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		NumIDStarsOSC[ST_indx] = 0;		
		for (Star_indx=0; Star_indx<IDStars[ST_indx].NumStars; Star_indx++)
		{
			if (IDStars[ST_indx].Stars[Star_indx].OSC_ID > 0)
			{
				NumIDStarsOSC[ST_indx]++;	
				TotalOSC++;
			}
		}
	}
	*TotalIDStarsOSC = TotalOSC;
}

/* --------------
*/

void GetNumIDStars(IDStarOutput_type IDStars[], int NumIDStars[])
{
	int ST_indx;

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		NumIDStars[ST_indx] = IDStars[ST_indx].NumStars;
}

/*********************************************************************************
	Change Log:
	$Log: IDentifyStars.c,v $
	Revision 1.9  2000/03/15 21:21:22  jzhao
	Rearranged SubCatalog memory allocation.
	
	Revision 1.8  2000/02/23 22:56:36  jzhao
	Modified for port to NT
	
	Revision 1.7  2000/02/23 22:45:25  jzhao
	Modified for Port to NT
	
	Revision 1.6  2000/02/11 19:26:11  jzhao
	Modified Log Foramt.
	
	Revision 1.5  2000/02/10 18:25:25  jzhao
	Modiffied CIS Log Format.
	
	Revision 1.4  2000/02/09 23:54:11  jzhao
	Modified for intergrating to GUI.
	
	Revision 1.3  2000/01/06 22:44:17  jzhao
	Minor change to prevent Math error.
	
	Revision 1.2  1999/12/16 20:05:16  jzhao
	Remove Flag "ValidRef" from ChooseOSCStarForVT.
	
	Revision 1.1  1999/12/13 18:42:51  jzhao
	Initial Release.
	
**********************************************************************************/
