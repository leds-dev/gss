/* $Id:$ */
/* ========================================================================
    FILE NAME:
        SunTrack.c

    DESCRIPTION:

    FUNCTION LIST:

    AUTHOR:	David D. Needelman

 ========================================================================== */

 /* System Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/* Self Defined Headers */
#include <MessageIO.h>
#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <CISGlobal.h>
#include <EPHGlobal.h>
#include <StarTracker.h>
#include <StarCatalog.h>
#include <SunTrack.h>
#include <IDentifyStars.h>
#include <ChooseOSC.h>

#define  SUN_POS_TEXT   \
  "Zero, or near-zero, separation between spacecraft and sun, in SunTrack"

/* ======================================================= */

/* --------------------------------
SunTrack: 
		This function is used to calculate the rate at which the
		spacecraft is to be commanded, after a successful attitude
		acquisition.  The rate will be the minimum rate required 
		for the spacecraft to maintain an attitude so that the 
		sun sensor is pointing to the sun.
		(Note: It is assumed that the sun sensor was pointing at the
		sun at the time this function is called.)

Input:
	DCM_SS2BDY:	DirCosMatrix_type. Sun sensor alignment DCM. Sun Sensor
			frame to Body.  (From CIS, or ini file).
	Quat_ECI2BDY:	QUATERNION_type.  Attitude.  (From CIS).
   	Sun_structure:	STAYOUT_type. Information related to position and
			velocity of sun, in ECI frame (with spacecraft at
			origin).  (From ephemeris code).

Output:
	*Spacecraft_Angular_VelocityBDY:	
			pointer to VECTOR_type.  Angular velocity of
				spacecraft, with respect to ECI frame, needed
				to track sun.  Angular velocity is expressed
				in spacecraft frame (degrees/second)
*/

void SunTrack ( VECTOR_type SSBoresightSS, 
				DirCosMatrix_type DCM_SS2BDY,
	      		QUATERNION_type Quat_ECI2BDY,
   	      		SunStruc_type Sun_structure,
	      		VECTOR_type *Spacecraft_Angular_VelocityBDY)
{
    int		i, status;

    double	sun_angular_rate;

    VECTOR_type SSBoresightBDY, SunVelBDY;

    DirCosMatrix_type DCM_ECI2BDY;

    /* Calculate the sun sensor boresight orientation in the spacecraft 
       body frame */

    MatrixMult (DCM_SS2BDY.DCM, SSBoresightSS.Vec, SSBoresightBDY.Vec,
                3, 3, 1, DOUBLE_DATA);

    /* Calculate the sun's velocity in the spacecraft body frame (m/s)
    */

    Quaternion2DirCosMatrix (Quat_ECI2BDY.Quat, DCM_ECI2BDY.DCM, 
			     DOUBLE_DATA);
    MatrixMult (DCM_ECI2BDY.DCM, Sun_structure.Velocity.Vec, 
		SunVelBDY.Vec, 3, 3, 1, DOUBLE_DATA);

    /* Calculate the spacecraft angular velocity.  If the sun 
       is at r, and the sun velocity is v, the angular velocity
       desired, in degrees/s, is 180/pi * (r/|r| x v)/|r|,
       where r/v is in seconds.  In this code:
         1)  SSBoresightBDY.Vec corresponds to r/|r|;
         2)  Sun_structure.DistFromSC corresponds to |r|; and
         3)  SunVelBDY.Vec corresponds to v.
    
       Trap error, if separation between sun and spacecraft is
       too close to 0.
    */

    if (fabs (Sun_structure.DistFromSC) <= TINY)  {
      status = MATH_ERROR;
      DisplayMessage(status, SUN_POS_TEXT);
      LogMessage(status, SUN_POS_TEXT);
      for (i=0; i<3; i++)
        Spacecraft_Angular_VelocityBDY->Vec[i] = 0;
      return;
    }

    CrossProduct (SSBoresightBDY.Vec, SunVelBDY.Vec, 
		  Spacecraft_Angular_VelocityBDY->Vec, DOUBLE_DATA);

    for (i=0; i<3; i++)
      Spacecraft_Angular_VelocityBDY->Vec[i] *= RTD/Sun_structure.DistFromSC;

}


/******************************************************************************
	Change Log:
	$Log:$
	
******************************************************************************/
