/* $Id: IdentifyPrimary.c,v 1.3 2000/02/24 00:13:29 jzhao Exp $ */
/* ==================================================================

	FILE NAME:
			IDentifyPrimary.c

===================================================================

	REVISION HISTORY

=================================================================== */
  /* System Header */

  #include <stdio.h>
  #include <stdlib.h>
  #include <math.h>
  #include <string.h>

  /* Self Defined Header */
  #include <MathFnc.h>
  #include <CISGlobal.h>
  #include <StarTracker.h>
  #include <StarCatalog.h>
  #include <IDentifyStars.h>
  #include <EstimateAttitude.h>

/* ================================================================= */

/* --------------------------------
*/

int IDentifyPrimaryCandidate(int CIS_MODE,
							 ST_type *PrimaryStar,
							 StarSubSC_type *PrimaryCandidate,
							 VECTOR_type *SunECI,
							 DirCosMatrix_type *PrimaryST_DCM_ECI2ST,
							 double SunPrimaryStarAngle,
							 VECTOR_type *FixedPtST,
							 VECTOR_type *FixedPtECI,
							 double *MaxRotationError)
{
	int IDCandidate = FALSE;
	int PassMagnitudeCheck = FALSE;
	int PassTransversErrCheck = FALSE;

	double PrimaryMag;
	double CandidateMag;
	double MagRes;
	double SunPrimaryCandidateAngle;
	double TransversError;
	double LongitudinalError;
	VECTOR_type PrimaryCandidateST;
	VECTOR_type PrimaryCandidateECI;
	VECTOR_type PrimaryStarST;

	/* Magnitude Check */

	PrimaryMag = PrimaryStar->Magnitude;
	CandidateMag = PrimaryCandidate->Magnitude;
	MagRes = PrimaryCandidate->MagResidual;
	PassMagnitudeCheck = MagnitudeCheck(CandidateMag, PrimaryMag, MagRes);
	memcpy(&PrimaryCandidateECI, PrimaryCandidate->ECI, 3*sizeof(double)); 
	memcpy(&PrimaryStarST,PrimaryStar->STFrame, 3*sizeof(double));

	/* Transvers Error Check, Cal. Max. allowable Rotation Error  */	

	if (PassMagnitudeCheck)
	{
		switch ( CIS_MODE )
		{
			case SUN_HOLD_MODE:
				CalTransversErrorSunHold(SunECI, 
										 &PrimaryCandidateECI,
										 SunPrimaryStarAngle,
										 &SunPrimaryCandidateAngle,
										 &TransversError);

				if (TransversError <= MAX_SUN_PRIMARY_ERROR)
				{
					PassTransversErrCheck = TRUE;
					CalMaxRotationErrorSunHold(TransversError,
											   SunPrimaryCandidateAngle,
											   &LongitudinalError,
											   MaxRotationError);
				}
				break;

			case ROUGH_KNOWLEDGE_MODE:
				CalTransversErrorRoughKnowledge(PrimaryST_DCM_ECI2ST,
												&PrimaryCandidateECI,
												&PrimaryStarST,
												&PrimaryCandidateST,
												&TransversError);
				if (TransversError <= MAX_ROUGH_PRIMARY_ERROR)
				{
					PassTransversErrCheck = TRUE;
					CalFixPtMaxRotationErrorRoughKnowledge(
												PrimaryST_DCM_ECI2ST,
												&PrimaryStarST,
												&PrimaryCandidateST,
												FixedPtST,
												FixedPtECI,
											  	MaxRotationError);
				}
				break;
			
			default: break;
		}
	}
	if (PassMagnitudeCheck && PassTransversErrCheck)
		IDCandidate = TRUE;

	return(IDCandidate);
}
							  
/* -----------------------------------
Input:
	PrimarySubCatalog:	SubSC_type. Sub-Star Catalog of primary Tracker.
	PrimaryStar:	ST_type. Primary Star data(from primary tracker).
	DCM_PrimaryST2ST:	Array[NUM_ST] of DirCosMatrix_type. Direction cosine 
						matries of primary ST frame to each ST frame.
	SunSTPrimary:	VECTOR_type. Sun vector ??? in Primary ST frame.
	MaxMagResPrimary:		double. Max. Magnitude Residual in the 
							Primary tracker Sub-Catalog.
Output:
	MaxAngCornerPrimaryStar:	Array[NUM_ST] of double. Max. Angle between
								ST corners and Primary Star.
								(do for each tracker).
	SunPrimaryAngle:	double. Angle between Sun ??? and Primary Star.
						(in Primary ST frame)
	Min_Indx, Max_Indx:	int. Index range in Sub-Catalog for Candidate search.
*/

void IDentifyPrimaryStar(SubSC_type *PrimarySubCatalog,
						 ST_type  *PrimaryStar,  
						 DirCosMatrix_type DCM_PrimaryST2ST[],
						 VECTOR_type *SunSTPrimary, 
						 double MaxMagResPrimary,
						 double MaxAngCornerPrimaryStar[],
						 double *SunPrimaryAngle,
						 int *Min_Indx, int *Max_Indx)
{
	/* Max Angle Bet. Tracker Corners and Primary star(for each ST) */
	GetMaxAngBetSTCornerPrimaryStar(PrimaryStar->STFrame, DCM_PrimaryST2ST,
									MaxAngCornerPrimaryStar);

	/* Angle between Sun vector??? and primary Star(in Primary ST frame) */
	AngleBetNormalVectors(SunSTPrimary->Vec, PrimaryStar->STFrame, 
						  SunPrimaryAngle, DOUBLE_DATA); 

	/* MinMax indx for candidate star search */
	SearchMinMaxMagIndx(PrimarySubCatalog, MaxMagResPrimary, 
						PrimaryStar->Magnitude, Min_Indx, Max_Indx); 
}

/* -------------------------------------
*/

void IDentifyPrimaryStarTracker(int PrimaryST_ID,
								TrackerOutputST_type TrackedStars[],
								DirCosMatrix_type DCM_ST2BDY[],	
								TrackerOutputST_type StarMatchList[], 
								double SepTolerance[],
								DirCosMatrix_type DCM_PrimaryST2ST[])
{
	/* copy TrackedStars to StarMarchList(all trackers) */
	memcpy(StarMatchList, TrackedStars, NUM_ST*sizeof(TrackerOutputST_type));

	/* Separation error tolerance for each ST */
	GetSeparationTolerance(PrimaryST_ID, SepTolerance);

	/* Transformation of PrimaryST to each ST */
	GetDirCosMatrixPrimaryST2ST(DCM_ST2BDY, PrimaryST_ID, DCM_PrimaryST2ST);
}

/* --------------------
EliminateCurPrimaryStar: Eliminate just-looked-at primary star from the 
						 pattern match star tracker output list.
Input/Output:
	StarMatchListPrimaryST:	TrackerOutputST_type.
*/

void ElimnateCurPrimaryStar( TrackerOutputST_type *StarMatchListPrimaryST)
{
	int Star_indx, NumStars;

	StarMatchListPrimaryST->NumStars--;
	NumStars = StarMatchListPrimaryST->NumStars;
	for (Star_indx=0; Star_indx<NumStars; Star_indx++)
	{
		StarMatchListPrimaryST->Stars[Star_indx].VT_ID = 
						StarMatchListPrimaryST->Stars[Star_indx+1].VT_ID; 
		StarMatchListPrimaryST->Stars[Star_indx].Magnitude = 
						StarMatchListPrimaryST->Stars[Star_indx+1].Magnitude; 
		StarMatchListPrimaryST->Stars[Star_indx].STFrame[0] = 
						StarMatchListPrimaryST->Stars[Star_indx+1].STFrame[0]; 
		StarMatchListPrimaryST->Stars[Star_indx].STFrame[1] = 
						StarMatchListPrimaryST->Stars[Star_indx+1].STFrame[1]; 
		StarMatchListPrimaryST->Stars[Star_indx].STFrame[2] = 
						StarMatchListPrimaryST->Stars[Star_indx+1].STFrame[2]; 
	}
}

/***********************************************************************************
	Change Log:
	$Log: IdentifyPrimary.c,v $
	Revision 1.3  2000/02/24 00:13:29  jzhao
	Modified for port to NT
	
	Revision 1.2  2000/02/09 23:54:12  jzhao
	Modified for intergrating to GUI.
	
	Revision 1.1  1999/12/13 18:42:52  jzhao
	Initial Release.
	
************************************************************************************/
