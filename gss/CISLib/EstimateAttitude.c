/* $Id: EstimateAttitude.c,v 1.5 2000/03/15 21:21:21 jzhao Exp $ */
/* ==================================================================

	FILE NAME:
			DirectMatch.c

 ===================================================================

	REVISION HISTORY

 =================================================================== */
	/* System Header */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

	/* Self Defined Header */

#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <MessageIO.h>
#include <CISGlobal.h>
#include <StarTracker.h>
#include <StarCatalog.h>
#include <IDentifyStars.h>

#define ERROR_TEXT "in EstimateAttitude"

/* ================================================================= */

/* -----------------------------------
PrimaryAttitudeEstimate: Calculates estimate of attitude, with the 
						 "primary assumption" - the assumption that 
						 the primary(tracked star) is the primary 
						 candidate(Catalog star).
Input:
	RefPrimaryECI:	VECTOR_type. Sun sensor LOS/FixedPt vector in ECI.
	RefPrimaryST:	VECOTR_type. sun sensor LOS/FixedPt in Primary ST frame.
	PrimaryCandidateECI: array[3] of double. Primary candidate star vector 
						 in ECI.
	PrimaryStarST:	array[3] of double. Primary star vector in Primary ST frame.
Output:
	PrimaryEstimateQuat_ECI2ST:	QUATERNION_type. Estimated attitude from 
								Primary Candidate.
	PrimaryEstimateDCM_ECI2ST: DirCosMatrix_type. Estimated attitude from 
								Primary Candidate. 
*/

void PrimaryAttitudeEstimate(VECTOR_type *RefPrimaryECI,
							 VECTOR_type *RefPrimaryST,
							 double PrimaryCandidateECI[],
							 double PrimaryStarST[],
							 QUATERNION_type *PrimaryEstimateQuat_ECI2ST,	
							 DirCosMatrix_type *PrimaryEstimateDCM_ECI2ST)
{
	VECTOR_type PrimaryCandidate_X_Ref_ECI;
	VECTOR_type PrimaryStar_X_Ref_ST;

	CrossProduct(PrimaryCandidateECI, 
				 RefPrimaryECI->Vec, 
				 PrimaryCandidate_X_Ref_ECI.Vec, DOUBLE_DATA);
	
	CrossProduct(PrimaryStarST, 
				 RefPrimaryST->Vec,
				 PrimaryStar_X_Ref_ST.Vec, DOUBLE_DATA);
				 
	QuatTRIAD(PrimaryCandidateECI, 
			  PrimaryStarST,
			  PrimaryCandidate_X_Ref_ECI.Vec,
			  PrimaryStar_X_Ref_ST.Vec,
			  PrimaryEstimateQuat_ECI2ST->Quat);
			 
	Quaternion2DirCosMatrix(PrimaryEstimateQuat_ECI2ST->Quat,
							PrimaryEstimateDCM_ECI2ST->DCM, DOUBLE_DATA);
}

/* ---------------------------
RefineAttitudeEstimate: Refine Estimated attitude use Identified stars 
						directe Match.  
Input:
	IDStars:	IDStarOutput_type. ID stars through Dir. match.
	SubStarCatalog: SubSC_type. Sub-star catalog used for ID stars.
Output:
	RefineQuat_ECI2ST: QUATERNION_type.
	RefineDCM_ECI2ST: DirCosMatrix_type.	
*/

int RefineAttitudeEstimate(IDStarOutput_type *IDStars, 
							SubSC_type	*SubStarCatalog, 
							QUATERNION_type *RefineQuat_ECI2ST,
							DirCosMatrix_type *RefineDCM_ECI2ST)
{
	int status = 0;
	int Star_indx, SubSC_indx, NumStars;
	double *IDStarST, *IDStarECI;

	NumStars = IDStars->NumStars;
	IDStarST = calloc(3*NumStars, sizeof(double));
	IDStarECI = calloc(3*NumStars, sizeof(double));
	if ( IDStarECI == NULL )
	{
		status = MEM_ERROR;
		DisplayMessage(status, ERROR_TEXT);
		LogMessage(status, ERROR_TEXT);
		goto quit;
	}

	for (Star_indx=0; Star_indx<NumStars; Star_indx++)
	{
		memcpy(IDStarST+Star_indx*3, 
			   IDStars->Stars[Star_indx].STFrame,
			   3*sizeof(double));

		SubSC_indx = IDStars->Stars[Star_indx].SubSC_ID - 1;
		memcpy(IDStarECI+Star_indx*3, 
			   SubStarCatalog->Stars[SubSC_indx]->ECI,
			   3*sizeof(double));
	}

	MatrixTranspose(IDStarST, IDStarST, NumStars, 3, DOUBLE_DATA);
	MatrixTranspose(IDStarECI, IDStarECI, NumStars, 3, DOUBLE_DATA);

	QUEST(NumStars, IDStarST, IDStarECI, RefineQuat_ECI2ST->Quat); 
	Quaternion2DirCosMatrix(RefineQuat_ECI2ST->Quat,
							RefineDCM_ECI2ST->DCM, DOUBLE_DATA);

quit:
	free(IDStarST);
	free(IDStarECI);

	return(status);
}
											
/*----------------------------
DiffBetEstimatedAttitude:	Calculates the change between two estimations. 
							(radians)

*/

void DiffBetEstimatedAttitude(QUATERNION_type *EstimatedQuat_ECI2ST1,  
							  QUATERNION_type *EstimatedQuat_ECI2ST2,
							  QUATERNION_type *DeltaQuat_ECI2ST,
							  VECTOR_type *AcqError, 
							  double *AcqErrorTotal)
{
	int indx;
	double TotalError;
	
	QUATERNION_type EstimatedQuat_ST2ECI1;

	QuaternionInvers(EstimatedQuat_ECI2ST1->Quat, EstimatedQuat_ST2ECI1.Quat,
														DOUBLE_DATA);
	QuaternionMult(EstimatedQuat_ECI2ST2->Quat, EstimatedQuat_ST2ECI1.Quat,
					DeltaQuat_ECI2ST->Quat, DOUBLE_DATA);
	for (indx=0; indx<3; indx++)
		AcqError->Vec[indx] = 2 * DeltaQuat_ECI2ST->Quat[indx];

	TotalError = DotProduct( AcqError->Vec, AcqError->Vec, DOUBLE_DATA);	
	
	*AcqErrorTotal = sqrt(TotalError);
}

/* ---------------------------
CalParamDirectMatch: 
*/

void CalParamDirectMatch( DirCosMatrix_type *PrimaryEstimateDCM_ECI2ST,
						  DirCosMatrix_type *DCM_PrimaryST2CurrST,
						  double RefPrimaryST[],
						  DirCosMatrix_type *CurrDCM_ST2ECI,
						  VECTOR_type *RefCurrST )
{
	DirCosMatrix_type CurrDCM_ECI2ST;

	MatrixMult(DCM_PrimaryST2CurrST->DCM,
               PrimaryEstimateDCM_ECI2ST->DCM,
               CurrDCM_ECI2ST.DCM, 3, 3, 3, DOUBLE_DATA);
    MatrixTranspose(CurrDCM_ECI2ST.DCM, CurrDCM_ST2ECI->DCM, 
					3, 3, DOUBLE_DATA);
	MatrixMult(DCM_PrimaryST2CurrST->DCM, 
			   RefPrimaryST, 
			   RefCurrST->Vec, 3, 3, 1, DOUBLE_DATA);
}
					 
/* ---------------
CalSTBoresightECI:
*/

void CalSTBoresightECI(VECTOR_type *STBoresight,
					   DirCosMatrix_type *DCM_ECI2ST,
					   VECTOR_type *STBoresightECI)
{
	DirCosMatrix_type DCM_ST2ECI;

	MatrixTranspose(DCM_ECI2ST->DCM, DCM_ST2ECI.DCM, 3, 3, DOUBLE_DATA);
	MatrixMult(DCM_ST2ECI.DCM, STBoresight->Vec, STBoresightECI->Vec,
				3, 3, 1, DOUBLE_DATA); 
}

/* --------------------
CalParallelError: (return radians) 
*/

double	CalParallelError(double StarSpread)
{
	double par_error;
	double Spread;
	
	Spread = StarSpread;
	RemoveZero(&Spread);

	par_error = MAX_SEPARAT_PRIMARY_TOL/Spread;
	return(par_error);
}

/* ---------------------
CalPerpendicularError: (return radians) 
*/

double CalPerpendicularError(SubSC_type *SubStarCatalog, 
							 IDStarOutput_type *IDStars,
							 DirCosMatrix_type *DCM_ECI2ST)
{
	int Star_indx, SubSC_indx;
	DirCosMatrix_type st2eci;
	VECTOR_type idstar_eci;
	double perp_error = 0;
	double angle;

	for ( Star_indx=0; Star_indx<IDStars->NumStars; Star_indx++ )
	{
		MatrixTranspose(DCM_ECI2ST->DCM, st2eci.DCM, 3, 3, DOUBLE_DATA); 
		MatrixMult(st2eci.DCM, IDStars->Stars[Star_indx].STFrame, 
					idstar_eci.Vec, 3, 3, 1, DOUBLE_DATA);

		SubSC_indx = IDStars->Stars[Star_indx].SubSC_ID - 1;
		AngleBetVectors(idstar_eci.Vec, 
						SubStarCatalog->Stars[SubSC_indx]->ECI,
						&angle, DOUBLE_DATA);

		perp_error += angle*angle;
	}
	
	if ( IDStars->NumStars != 0)
		perp_error = sqrt(perp_error / IDStars->NumStars);

	return(perp_error);
}

/* ---------------

Input:
	SunECI:	Sun vector in ECI.( Unit vector).
	PrimaryCandidateECI:	Primary Candidate Star in ECI(Unit vector).
							(Search from SubSC)
	SunPrimaryStarAngle:	Angle between Sun sensor LOS and Primry Star in 
							Primary Star Tracker Frame. 
Output:
	SunPrimaryCandidateAngle:	(in ECI)(radians) 
	TransversError:			double. (radians) 

*/
void CalTransversErrorSunHold(VECTOR_type *SunECI,
								VECTOR_type *PrimaryCandidateStarECI,
								double		SunPrimaryStarAngle,
								double	*SunPrimaryCandidateAngle,
								double	*TransversError)
{

	/* Angle between the S/C to sun vector, and the S/C to primary candidate 
	   star vector in ECI */
	AngleBetVectors(SunECI->Vec, PrimaryCandidateStarECI->Vec, 
						SunPrimaryCandidateAngle, DOUBLE_DATA);
		
	*TransversError = fabs(*SunPrimaryCandidateAngle - SunPrimaryStarAngle);	
}

/* ---------------

Input:
	PrimaryST_DCM_ECI2ST:	Direction Cosine Matrix of ECI2ST(Primary ST)
								in rough knowledge Mode.
	PrimaryCandidateECI:	Primary Candidate Star in ECI(Unit vector).
							(Search from SubSC)
	PrimaryStarST:			Primary Star vector in Primary ST frame.

Output:
	PrimaryCandidateST;		Primary Candidate star in primary ST frame.
	TransversError:			double.(radians) 

*/
void CalTransversErrorRoughKnowledge(
							DirCosMatrix_type *PrimaryST_DCM_ECI2ST,
							VECTOR_type *PrimaryCandidateStarECI,
							VECTOR_type *PrimaryStarST,
							VECTOR_type	*PrimaryCandidateST,
							double *TransversError)
{

	/* Roatate Primary Candidate Star form ECI to Primary ST frame */
	MatrixMult(PrimaryST_DCM_ECI2ST->DCM, PrimaryCandidateStarECI->Vec, 
			   PrimaryCandidateST->Vec, 3, 3, 1, DOUBLE_DATA);		
		
	/* Angle(defined as transvers error) between Primary star and primary 
		candidate star */ 
	AngleBetVectors(PrimaryStarST->Vec, PrimaryCandidateST->Vec,
						TransversError, DOUBLE_DATA);
}

/* --------------
*/
void CalMaxRotationErrorSunHold(double TransversError, 
								double SunPrimaryCandidateAngle,
								double *longitudinalError,
								double *MaxRotationError)
{
	double SIN_SunPrimaryCandidateAngle;

	SIN_SunPrimaryCandidateAngle = sin(SunPrimaryCandidateAngle);
	RemoveZero(&SIN_SunPrimaryCandidateAngle);

	*longitudinalError = sqrt( fabs(MAX_SUN_PRIMARY_ERROR *
							  		MAX_SUN_PRIMARY_ERROR - 
							  		TransversError * TransversError) );

	*MaxRotationError = *longitudinalError / SIN_SunPrimaryCandidateAngle;
}

/* ------------------
*/
void CalFixPtMaxRotationErrorRoughKnowledge(
							DirCosMatrix_type *PrimaryST_DCM_ECI2ST,
							VECTOR_type *PrimaryStarST,
						   	VECTOR_type *PrimaryCandidateST,
							VECTOR_type *FixedPtST,
							VECTOR_type *FixedPtECI,
							double *MaxRotationError)
{
	DirCosMatrix_type PrimaryST_DCM_ST2ECI;

	CrossProduct(PrimaryStarST->Vec, PrimaryCandidateST->Vec, FixedPtST->Vec,
				 DOUBLE_DATA);
	MatrixTranspose(PrimaryST_DCM_ECI2ST->DCM, PrimaryST_DCM_ST2ECI.DCM,
					3, 3, DOUBLE_DATA);
	MatrixMult(PrimaryST_DCM_ST2ECI.DCM, FixedPtST->Vec, FixedPtECI->Vec,
			   3, 3, 1, DOUBLE_DATA);
	*MaxRotationError = MAX_ROUGH_KNOWLEDGE_ERROR;		
}

/* --------------------
*/
int IdentifyContradictory( QUATERNION_type *PrevQuat_ECI2ST,
						   QUATERNION_type *CurrQuat_ECI2ST )
{
	int IDContradictory = FALSE;

	QUATERNION_type InvPrevQuat_ECI2ST;
	QUATERNION_type DeltaQuat;
	double DeltaQuatMag;

	QuaternionInvers( PrevQuat_ECI2ST->Quat, InvPrevQuat_ECI2ST.Quat, 
														DOUBLE_DATA);
	QuaternionMult( InvPrevQuat_ECI2ST.Quat, CurrQuat_ECI2ST->Quat,
										DeltaQuat.Quat, DOUBLE_DATA);
	DeltaQuatMag = 2 * sqrt( DeltaQuat.Quat[0] * DeltaQuat.Quat[0] +	
							 DeltaQuat.Quat[1] * DeltaQuat.Quat[1] +
							 DeltaQuat.Quat[2] * DeltaQuat.Quat[2] ); 

	if ( DeltaQuatMag >= PARALLEL_ERROR_THRESHOLD )
		IDContradictory = TRUE;

	return(IDContradictory);
}

/********************************************************************************** 
	Change Log:
	$Log: EstimateAttitude.c,v $
	Revision 1.5  2000/03/15 21:21:21  jzhao
	Rearranged SubCatalog memory allocation.
	
	Revision 1.4  2000/02/24 00:13:28  jzhao
	Modified for port to NT
	
	Revision 1.3  2000/02/09 23:54:11  jzhao
	Modified for intergrating to GUI.
	
	Revision 1.2  2000/01/06 22:30:36  jzhao
	Corrected mistake in longitudinal error Calculation.
	
	Revision 1.1  1999/12/13 18:42:51  jzhao
	Initial Release.
	
***********************************************************************************/
