/* $Id: CIS_Slew.c,v 1.1 2000/03/21 23:35:45 jzhao Exp $ */
/* ========================================================================
    FILE NAME:
        CIS_Slew.c

    DESCRIPTION:
		This file include slew functions used by CIS.(After completion of
		CIS, CIS need provide recommended slew to user).

    FUNCTION LIST:

    AUTHOR:	David D. Needelman

 ========================================================================== */

 /* System Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Self Defined Headers */
#include <MessageIO.h>
#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <CISGlobal.h>
#include <StarTracker.h>
#include <StarCatalog.h>
#include <IDentifyStars.h>
#include <ChooseOSC.h>
#include <ephem_intf.h>
#include <CIS_Slew.h>

/* --------------------------------
Slew_EnoughStars
		This function is used to find which OSC stars will be
		visible to a star tracker, given a slew from an
		input initial orientation.  Value returned is TRUE if
		at least PRIMARY_ST_TIGHT_THRESHOLD OSC stars are visible.

Input:
	DCM_ECI2ST:		DirCosMatrix_type. Conversion from
				Star Tracker frame to ECI.
	axisST:			VECTOR_type. Slew axis, with reference to
				star tracker frame.
	angle:			double.  Slew angle (radians)
	STBoresightST:          Tracker boresight vector in STframe.
	AcqStarCatalog:		AcqSC_type.  Acquisition Star Catalog.
	tracker_index:		integer.  Represents index of tracker 
				[1, NUM_ST]

Output:
	OSCStarsInFOV:		TrackerFOV_HV_type.  List of OSC stars visible
						at end of slew.
	returned value:		integer; TRUE, if at least 
					PRIMARY_ST_TIGHT_THRESHOLD OSC stars 
					are visible.
				  FALSE, otherwise
*/

int Slew_EnoughStars (DirCosMatrix_type DCM_ECI2ST,
		      		  VECTOR_type axisST,
		      		  double angle,
		      		  VECTOR_type STBoresightST,
		      		  AcqSC_type *AcqStarCatalog,
		      		  int tracker_index,
		      		  TrackerFOV_HV_type *OSCStarsInFOV)

{
    QUATERNION_type	Quat_OrigST2NewST;
    DirCosMatrix_type	DCM_OrigST2NewST, DCM_ECI2NewST;
	FOVSize_type FOVSize;

	FOVSize.HMax = H_MAX_ANG;	/*  4 degrees */
	FOVSize.HMin = H_MIN_ANG;	/* -4 degrees */
	FOVSize.VMax = V_MAX_ANG;	/*  4 degrees */
	FOVSize.VMin = V_MIN_ANG;	/* -4 degrees */

    /* Calculate the new ECI-to-ST transformation, using a slew through
       this angle, about this axis.  */

    Quat_AxisAngle2Q(axisST, angle, &Quat_OrigST2NewST);

    Quaternion2DirCosMatrix(&Quat_OrigST2NewST.Quat, 
			    &DCM_OrigST2NewST.DCM, DOUBLE_DATA);

    MatrixMult(DCM_OrigST2NewST.DCM, DCM_ECI2ST.DCM,
	       DCM_ECI2NewST.DCM, 3, 3, 3, DOUBLE_DATA);

    /* Calculate the number of stars in the FOV, given the proposed
       rotation.  Move on to the next angle if we don't meet the
       minimum threshold.  */
 
    TrackerFOV (AcqStarCatalog, &STBoresightST, &DCM_ECI2NewST,
                &FOVSize, OSCStarsInFOV, tracker_index, OSCInFOV);

    if (OSCStarsInFOV->NumStars >= PRIMARY_ST_TIGHT_THRESHOLD)
      return(TRUE);
    else
      return(FALSE);
}

/* --------------------------------
Slew_ToPositionWing: 
		This function is used to design two slews about the sun sensor 
		line-of-sight (which is assumed to be pointing at the sun).  
		These slews will be designated the "upright" slew, and the
		"inverted" slew.  The slews, assumed to be executed when 
		CIS has been run successfully (i.e., with acquisition from 
		at least two trackers), are to satisfy the following
		conditions:

		1)	(Both slews) The slew axis is the sun sensor 
			line-of-sight; and

		2a)	(Upright slew only) At the end of the slew, the 
			wing tip (on the -y-axis) will have the minimum 
			angular separation from the ECI -z-axis possible, 
			given condition 1.

		2b)	(Inverted slew only) At the end of the slew, the 
			wing tip (on the -y-axis) will have the minimum 
			angular separation from the ECI +z-axis possible, 
			given condition 1.

Input:
	SSBoresightSS:	VECTOR_type. Sun Sensor boresight in SS frame.	
	DCM_SS2BDY:		DirCosMatrix_type. Sun sensor alignment DCM.
				Sun Sensor frame to Body.
	Quat_Initial_ECI2BDY:	QUATERNION_type. Attitude. ECI frame to Body
				at start of slew.

Output:
   	upright_slew_angle:	double.  Angle of derived slew, assuming
				goal is to have wing in upright orientation
				(radians, in range (-PI, PI]).
   	inverted_slew_angle:	double.  Angle of derived slew, assuming
				goal is to have wing in inverted orientation
				(radians, in range (-PI, PI]).
*/

void Slew_ToPositionWing (  VECTOR_type SSBoresightSS,
							DirCosMatrix_type DCM_SS2BDY,
		       				QUATERNION_type Quat_Initial_ECI2BDY,
		       				double *upright_slew_angle,
		       				double *inverted_slew_angle)
{
    DirCosMatrix_type DCM_Initial_BDY2ECI;

    QUATERNION_type Quat_Initial_BDY2ECI, Quat_Final_ECI2BDY;
    QUATERNION_type Quat_Initial2Final;

    VECTOR_type SSBoresightBDY, SSBoresightECI;
    VECTOR_type WingBDY = {0, -1, 0};
    VECTOR_type NorthECI = {0, 0, 1};
    VECTOR_type SouthECI = {0, 0, -1};
    VECTOR_type SSxWingBDY, SSxNorthECI, SSxSouthECI;
    VECTOR_type upright_slew_axis, inverted_slew_axis;

    /* Calculate the initial body-to-ECI quaternion, at the start of the
       slew.  */

    QuaternionInvers(Quat_Initial_ECI2BDY.Quat, Quat_Initial_BDY2ECI.Quat, 
		     DOUBLE_DATA);

    /* Calculate the initial body-to-ECI direction cosine matrix.  */
 
    Quaternion2DirCosMatrix(Quat_Initial_BDY2ECI.Quat, DCM_Initial_BDY2ECI.DCM,
                            DOUBLE_DATA);

    /* Calculate the sun sensor boresight orientation in the spacecraft 
       body and ECI frames */

    MatrixMult(DCM_SS2BDY.DCM, SSBoresightSS.Vec, SSBoresightBDY.Vec,
               3, 3, 1, DOUBLE_DATA);
    MatrixMult(DCM_Initial_BDY2ECI.DCM, SSBoresightBDY.Vec, SSBoresightECI.Vec,
               3, 3, 1, DOUBLE_DATA);

    /* Calculate the cross-product of the wing and sun sensor boresight
       vectors.  */

    CrossProduct (SSBoresightBDY.Vec, WingBDY.Vec, SSxWingBDY.Vec, 
		  DOUBLE_DATA);
    VectorNormalize (SSxWingBDY.Vec, SSxWingBDY.Vec, DOUBLE_DATA);

    /* Calculate the delta quaternion for the upright slew  */

    CrossProduct (SSBoresightECI.Vec, SouthECI.Vec, SSxSouthECI.Vec, 
		  DOUBLE_DATA);
    VectorNormalize (SSxSouthECI.Vec, SSxSouthECI.Vec, DOUBLE_DATA);

    QuatTRIAD (SSBoresightECI.Vec, SSBoresightBDY.Vec,
			      SSxSouthECI.Vec, SSxWingBDY.Vec,
			      Quat_Final_ECI2BDY.Quat);

    QuaternionMult(Quat_Initial_BDY2ECI.Quat, Quat_Final_ECI2BDY.Quat,
		   Quat_Initial2Final.Quat, DOUBLE_DATA);

    /* Calculate the axis and angle associated with the upright slew.  
       (Note: Quat_Q2AxisAngle ensures that the output angle is always
       in the range (-PI, PI].)  */
 
    Quat_Q2AxisAngle (Quat_Initial2Final, 
		      &upright_slew_axis, upright_slew_angle);

    /* If the axis is the negative of the sun sensor boresight, take the
       additive inverse of the resultant angle.  */

    if (DotProduct (SSBoresightBDY.Vec, upright_slew_axis.Vec, 
		    DOUBLE_DATA) <= 0)
      *upright_slew_angle *= -1;

    /* Calculate the delta quaternion for the inverted slew  */

    CrossProduct (SSBoresightECI.Vec, NorthECI.Vec, SSxNorthECI.Vec, 
		  DOUBLE_DATA);
    VectorNormalize (SSxNorthECI.Vec, SSxNorthECI.Vec, DOUBLE_DATA);

    QuatTRIAD (SSBoresightECI.Vec, SSBoresightBDY.Vec,
			      SSxNorthECI.Vec, SSxWingBDY.Vec,
			      Quat_Final_ECI2BDY.Quat);

    QuaternionMult(Quat_Initial_BDY2ECI.Quat, Quat_Final_ECI2BDY.Quat,
		   Quat_Initial2Final.Quat, DOUBLE_DATA);

    /* Calculate the axis and angle associated with the inverted slew.  
       (Note: Quat_Q2AxisAngle ensures that the output angle is always
       in the range (-PI, PI].)  */
 
    Quat_Q2AxisAngle (Quat_Initial2Final, 
		      &inverted_slew_axis, inverted_slew_angle);

    /* If the axis is the negative of the sun sensor boresight, take the
       additive inverse of the resultant angle.  */

    if (DotProduct (SSBoresightBDY.Vec, inverted_slew_axis.Vec,
		    DOUBLE_DATA) <= 0)
      *inverted_slew_angle *= -1;
}


/* --------------------------------
Slew_ToStarrierSkies: 
		This function is used to design a slew of a given angle
		about the sun sensor line-of-sight (which is assumed to be
		pointing at the sun).  A positive angle will yield a 
		counter-clockwise slew, from the point of view of an
		observer sitting on the sun; a negative angle will yield
		a clockwise slew.

Input:
	Tracker1_index:	integer.  Represents index of one
					tracker to be used after slew.
	Tracker2_index:	integer.  Represents index of one
					tracker to be used after slew.
	DCM_ST2BDY[]:	DirCosMatrix_type. Star Tracker alignment DCM. 
					Star Tracker frame to Body.
	SSBoresightSS:	Sun Sensor boresight in SS frame.
	STBoresightST:  Tracker boresight vector in STframe.
	DCM_SS2BDY:	DirCosMatrix_type. Sun sensor alignment DCM.
				Sun Sensor frame to Body.
    StayOuts[]:	STAYOUT_type. For each stayout objects.
	Quat_Initial_ECI2BDY:	QUATERNION_type. Attitude. ECI frame to Body
							at start of slew.
	AttitudeUncertainty:	double.  Assumed maximum error in 
							Quat_Initial_ECI2BDY.  (Used to "pad" 
							stayout zones.) (radians)
	AcqStarCatalog:	AcqSC_type.  Acquisition Star Catalog.

Output:
	Intrusion:	integer.  361 x MAX_EPHEM_OBJS.  
				Each element [i][j] is set to TRUE if a 
				slew about the sunline of i degrees will 
				cause either of the two input trackers to 
				wind up in the stayout zone of element j 
				of the StayOuts structure.
   	plus_slew_angle:	double.  Angle of derived slew, assuming
						slew is in positive direction (radians).
   	minus_slew_angle:	double.  Angle of derived slew, assuming
						slew is in negative direction (radians).
*/

void Slew_ToStarrierSkies (int Tracker1_index, int Tracker2_index,
		       			   DirCosMatrix_type DCM_ST2BDY[],
		       			   VECTOR_type SSBoresightSS,
		       			   VECTOR_type STBoresightST,
		       			   DirCosMatrix_type DCM_SS2BDY,
		       			   STAYOUT_type StayOuts[MAX_EPHEM_OBJS],
		       			   QUATERNION_type Quat_Initial_ECI2BDY,
		       			   double AttitudeUncertainty,
		       			   AcqSC_type *AcqStarCatalog,
		       			   int Intrusion[][MAX_EPHEM_OBJS],
		       			   double *plus_slew_angle,
		       			   double *minus_slew_angle)
{
    int i, st, tracker_index, stayout_index;
    int	theta_plus_valid, theta_minus_valid;
    int Intrusion_total [361];
    int StarryEnough;
    double theta_plus, theta_minus;
    double theta_Initial, theta_Final;
    double Effective_StayOut_Radius;
    double holder_angle;

    DirCosMatrix_type DCM_BDY2ST[NUM_ST];
    DirCosMatrix_type DCM_ST2ECI, DCM_ECI2ST[NUM_ST];
    DirCosMatrix_type DCM_BDY2ECI;

    QUATERNION_type Quat_BDY2ECI;

    VECTOR_type SSBoresightST[NUM_ST], SSBoresightBDY, SSBoresightECI;
    VECTOR_type STBoresightBDY;

    TrackerFOV_HV_type	OSCStarsInFOV;
	
    /* Initialize Intrusion array  */

    for (i = 0; i < 361; i++) 
	{
      Intrusion_total [i] = 0;
      for (stayout_index = 0; stayout_index < MAX_EPHEM_OBJS;
	   stayout_index++)
        Intrusion [i][stayout_index] = 0;
    }

    /* Calculate the ECI-referenced position corresponding to position_body
       at the start of the slew.  */

    QuaternionInvers(&Quat_Initial_ECI2BDY.Quat, Quat_BDY2ECI.Quat, 
		     DOUBLE_DATA);
    Quaternion2DirCosMatrix(Quat_BDY2ECI.Quat, DCM_BDY2ECI.DCM, DOUBLE_DATA);

    /* Calculate the sun sensor boresight orientation in the spacecraft 
       body and ECI frames */

    MatrixMult(DCM_SS2BDY.DCM, SSBoresightSS.Vec, SSBoresightBDY.Vec,
               3, 3, 1, DOUBLE_DATA);
    MatrixMult(DCM_BDY2ECI.DCM, SSBoresightBDY.Vec, SSBoresightECI.Vec,
               3, 3, 1, DOUBLE_DATA);

    /* Cycle through the selected trackers to find out which are at risk
       of intrusion.  */

    for (st=1; st<=2; st++)  
	{
      if (st == 1)
        tracker_index = Tracker1_index;
      else
        tracker_index = Tracker2_index;

      /* Calculate the ST-to-body direction cosine matrices.  */

      MatrixTranspose(DCM_ST2BDY[tracker_index-1].DCM, 
		      DCM_BDY2ST[tracker_index-1].DCM, 3, 3, DOUBLE_DATA);

      /* Calculate the tracker boresight position in the body frame.  */

      MatrixMult(DCM_ST2BDY[tracker_index-1].DCM, STBoresightST.Vec, 
		 STBoresightBDY.Vec, 3, 3, 1, DOUBLE_DATA);

      /* Calculate the tracker-to-ECI direction cosine matrix.  */

      MatrixMult(DCM_BDY2ECI.DCM, DCM_ST2BDY[tracker_index-1].DCM,
		 DCM_ST2ECI.DCM, 3, 3, 3, DOUBLE_DATA);
      MatrixTranspose(DCM_ST2ECI.DCM, DCM_ECI2ST[tracker_index-1].DCM, 
		      3, 3, DOUBLE_DATA);

      /* Calculate the sun sensor boresight orientation in the tracker frame.
      */

      MatrixMult(DCM_BDY2ST[tracker_index-1].DCM, SSBoresightBDY.Vec, 
	         SSBoresightST[tracker_index-1].Vec, 3, 3, 1, DOUBLE_DATA);

      /* Cycle through the various celestial objects that have stayout
	 zones.  Ignore the sun (index SUN in StayOuts).  The sun cannot
	 intrude, since we are rotating about the sun-line.  */

      for (stayout_index=0; stayout_index < MAX_EPHEM_OBJS; 
	   													stayout_index++)  
	  {
		if (stayout_index == GSS_SUN) continue; /* skip SUN */

		Effective_StayOut_Radius = 
	  				StayOuts[stayout_index].Radius + AttitudeUncertainty;

		SlewIntersection(Quat_Initial_ECI2BDY,
		         		 STBoresightBDY,
		         		 SSBoresightBDY,
		         		 StayOuts[stayout_index].ECI,
		         		 Effective_StayOut_Radius,
		         		 &theta_plus, &theta_minus,
		         		 &theta_plus_valid, &theta_minus_valid);

	/* The output from SlewIntersection tells us which of three possible
	   cases we currently have:

	   1)	theta_plus_valid and theta_minus_valid are TRUE.
			This means there is a range of angles, between
			theta_plus and theta_minus, in which the current
			tracker, tracker_index, is intruded upon by
			celestial object stayout_index.  We don't know
			which order theta_plus and theta_minus are in; we
			need to find out, and mark the appropriate interval
			in the Intrusion and Intrusion_total arrays.

	  2)	exactly one of theta_plus_valid and theta_minus_valid is TRUE.
			This means there is a single angle at which the 
			current tracker is intruded upon by the current 
			celestial object.  That angle must be marked in the 
			Intrusion and Intrusion_total arrays.

	  3)	theta_plus_valid and theta_minus_valid are FALSE.
			No intrusion occurred here; move on to the next
			celestial object.

	*/

		if (theta_plus_valid && theta_minus_valid)  
		{
	  		if ((theta_plus < theta_minus && theta_minus - theta_plus < PI) ||
	      		(theta_minus < theta_plus && theta_plus - theta_minus > PI))  
			{
	    		theta_Initial = floor (theta_plus * RTD);
	    		theta_Final = ceil (theta_minus * RTD);
	  		}
	  		else  
			{
	    		theta_Initial = floor (theta_minus * RTD);
	    		theta_Final = ceil (theta_plus * RTD);
	  		}
		}
		else if (theta_plus_valid)  
		{
	  		theta_Initial = floor (theta_plus * RTD);
	  		theta_Final = ceil (theta_plus * RTD);
		}
		else if (theta_minus_valid)  
		{
	  		theta_Initial = floor (theta_minus * RTD);
	  		theta_Final = ceil (theta_minus * RTD);
		}
		else
	  		continue;

	/* The intrusion zone is [theta_Initial, theta_Final].  Allow for
	   wrap-around.  This will occur when theta_Initial is greater than
	   theta_Final.  */

		if (theta_Final >= theta_Initial)
	  		for (i = theta_Initial; i <= theta_Final; i++)  
	  		{
	    		Intrusion [i][stayout_index] = TRUE;
	    		Intrusion_total [i] += 1;
	  		}
		else  
		{
	  		for (i = theta_Initial; i < 360; i++)  
	  		{
	    		Intrusion [i][stayout_index] = TRUE;
	    		Intrusion_total [i] += 1;
	  		}
	  		for (i = 0; i <= theta_Final; i++)  
	  		{
	    		Intrusion [i][stayout_index] = TRUE;
	    		Intrusion_total [i] += 1;
	  		}
		}
   	  }		/* end of loop with index stayout_index  */
    }		/* end of loop with index st  */

    /* For convenience, allow Intrusion and Intrusion_total to
       "wrap-around" */

    for (stayout_index=1; stayout_index < MAX_EPHEM_OBJS; 
	 												stayout_index++)  
	{
      Intrusion [360][stayout_index] = Intrusion [0][stayout_index];
      Intrusion_total [360] = Intrusion_total [0];
    }

    /* Recommend slew angle based on smallest required slew for slew
       in positive direction.  */

    for (i = 0; i <= 359; i++)  
	{
      if (Intrusion_total[i] == 0)  
	  {
	/* At this point, it is known that there is no intrusion problem
	   for either tracker.  Check that both trackers "see" a sufficient
	   number of stars.  */

        holder_angle = i * DTR;
        for (st=1; st<=2; st++)  
		{
          if (st == 1)
            tracker_index = Tracker1_index;
          else
            tracker_index = Tracker2_index;

	  	  StarryEnough = Slew_EnoughStars(DCM_ECI2ST[tracker_index-1],
			      						  SSBoresightST[tracker_index-1],
			      						  holder_angle,
			      						  STBoresightST,
			      						  AcqStarCatalog,
			      						  tracker_index,
			      						  &OSCStarsInFOV);
	  	  if (StarryEnough == FALSE)
	    	break;
		}
		if (StarryEnough)  
		{
          *plus_slew_angle = holder_angle;
          break;
		}
      }
    }

    /* Recommend slew angle based on smallest required slew for slew
       in negative direction.  */

    for (i = 360; i >= 1; i--)  
	{
      if (Intrusion_total[i] == 0)  
	  {
        holder_angle = i * DTR - 2 * PI;
        for (st=1; st<=2; st++)  
		{
          if (st == 1)
            tracker_index = Tracker1_index;
          else
            tracker_index = Tracker2_index;

	  	  StarryEnough = Slew_EnoughStars (DCM_ECI2ST[tracker_index-1],
			      						   SSBoresightST[tracker_index-1],
			      						   holder_angle,
			      						   STBoresightST,
			      						   AcqStarCatalog,
			      						   tracker_index,
			      						   &OSCStarsInFOV);
	  	  if (StarryEnough == FALSE)
	    	 break;
		}
		if (StarryEnough)  
		{
          *minus_slew_angle = holder_angle;
          break;
		}
      }
    }
}
 
/* -----------------------------------------------------------------------
SlewIntersection:
		This function is used to determine whether a slew will
		cause a body-fixed point to pass within a fixed distance
		from a celestially-fixed point.  If it does, this function
		returns the slew angles at which this happens.

		It is assumed that the slew is wide enough, and the fixed
		distance is small enough, that there will be 0, 1, or 2
		points of intersection.  Two angles are returned, along
		with a validity flag for each.  (The flag is set if a
		slew through the corresponding angle yields a legitimate 
		intersection.)

Input:
	Quat_Initial_ECI2BDY: QUATERNION_type.  Attitude at time of slew start.
	BodyFixedPtBDY:	VECTOR_type.  Fixed point of interest in body frame.
	SlewAxisBDY:	VECTOR_type.  Slew axis in body frame.
	ECIFixedPtECI:	VECTOR_type.  Fixed point of interest in ECI frame.
	Separation:	double.  Angular separation of interest between
						 BodyFixedPtBDY and ECIFixedPtECI (translated into
						 same frame).  (radians)

Output:
     theta_plus:	double.  Slew angle yielding intersection.
			0 <= theta_plus <= 2 * PI (radians).
     theta_minus:	double.  Slew angle yielding intersection
			0 <= theta_minus <= 2 * PI (radians).
	theta_plus_valid:
			integer. Set to TRUE if theta_plus is an angle of
			intersection; FALSE, otherwise.
	theta_minus_valid:
			integer. Set to TRUE if theta_minus is an angle of
			intersection; FALSE, otherwise.
*/
 
void SlewIntersection(QUATERNION_type Quat_Initial_ECI2BDY,
		      		  VECTOR_type BodyFixedPtBDY, 
		      		  VECTOR_type SlewAxisBDY,
		      		  VECTOR_type ECIFixedPtECI,
		      		  double Separation,
		      		  double *theta_plus, double *theta_minus,
		      		  int *theta_plus_valid, int *theta_minus_valid)
{
    int     i;
    double  cos_sep, holder1, alpha, beta, gamma, discriminant, A, B, C;
    double  sin_theta, cos_theta;
    double  sin_theta_plus, sin_theta_minus;
    double  cos_theta_plus, cos_theta_minus;
    double  cos_sep_plus, cos_sep_minus;
    VECTOR_type BodyFixedPtECI, holder2;
    VECTOR_type	ECIFixedPtBDY, ECIFixedPtBDY_Impinge;
    DirCosMatrix_type DCM_ECI2BDY, DCM_BDY2ECI;
    QUATERNION_type Q_plus_Initial2Impinge, Q_minus_Initial2Impinge;
    DirCosMatrix_type DCM_plus_Initial2Impinge, DCM_minus_Initial2Impinge;
    DirCosMatrix_type DCM_ECI2BDY_Impinge;
 
    /* Initialize various useful variables.  */

    *theta_plus_valid = FALSE;
    *theta_minus_valid = FALSE;

    cos_sep = cos (Separation);

    /* Calculate the Body-referenced position corresponding to ECIFixedPtECI
       at the start of the slew.  */

    Quaternion2DirCosMatrix (&Quat_Initial_ECI2BDY.Quat, DCM_ECI2BDY.DCM, 
			     DOUBLE_DATA);
    MatrixMult(DCM_ECI2BDY.DCM, &ECIFixedPtECI.Vec, ECIFixedPtBDY.Vec,
               3, 3, 1, DOUBLE_DATA);

    /* Calculate the ECI-referenced position corresponding to BodyFixedPtBDY
       at the start of the slew.  */

    MatrixTranspose(DCM_ECI2BDY.DCM, DCM_BDY2ECI.DCM, 3, 3, DOUBLE_DATA);
    MatrixMult(DCM_BDY2ECI.DCM, &BodyFixedPtBDY.Vec, BodyFixedPtECI.Vec,
               3, 3, 1, DOUBLE_DATA);
    holder1 = 
	DotProduct(&SlewAxisBDY.Vec, &BodyFixedPtBDY.Vec, DOUBLE_DATA) *
	DotProduct(&SlewAxisBDY.Vec, &ECIFixedPtBDY.Vec, DOUBLE_DATA);

    alpha = 
      DotProduct(&BodyFixedPtBDY.Vec, ECIFixedPtBDY.Vec, DOUBLE_DATA) - 
      holder1;

    CrossProduct(&BodyFixedPtBDY.Vec, &SlewAxisBDY.Vec,
		 holder2.Vec, DOUBLE_DATA);

    beta = -DotProduct (holder2.Vec, ECIFixedPtBDY.Vec, DOUBLE_DATA);
    gamma = holder1 - cos_sep;

    /* If a valid angle, theta, exists, it will be a solution to:

	0 = alpha * cos(theta) + beta * sin(theta) + gamma	Eq'n(1)
    */

    if (fabs(alpha) < TINY && fabs(beta) < TINY) return;
    else if (fabs(alpha) < TINY)  
	{
      sin_theta = -gamma/beta;
      if (fabs(sin_theta) > 1) return;

      *theta_plus = asin (sin_theta);
      if (*theta_plus < 0) *theta_plus += 2 * PI;

      *theta_minus = PI - *theta_plus;
      if (*theta_minus < 0) *theta_minus += 2 * PI;

      *theta_plus_valid = TRUE;
      *theta_minus_valid = TRUE;
    }
    else if (fabs(beta) < TINY)  
	{
      cos_theta = -gamma/alpha;

      if (fabs(cos_theta) > 1) return;

      *theta_plus = acos (cos_theta);
      if (*theta_plus < 0) *theta_plus += 2 * PI;

      *theta_minus = 2 * PI - *theta_plus;

      *theta_plus_valid = TRUE;
      *theta_minus_valid = TRUE;
    }
    else 	/* There is a solution to E'qn(1) involving finite terms
		   for both sin(theta) and cos(theta).  */
    {   
      A = alpha * alpha + beta * beta;
      B = 2 * beta * gamma;
      C = gamma * gamma - alpha * alpha;

      /* All solutions to Eq'n(1) are also solutions to:

	  0 = A * sin^2(theta) + B * sin(theta) + C		E'qn(2)
      */

      discriminant = B * B - 4 * A * C;

      if (discriminant < 0) return;
      else if (discriminant < TINY) 
	  {
        sin_theta = -B/(2 * A);

        if (fabs (sin_theta) > 1) return;

        *theta_plus = asin(sin_theta);
        if (*theta_plus < 0) *theta_plus += 2 * PI;
  
        *theta_minus = PI - *theta_plus;
        if (*theta_minus < 0) *theta_minus += 2 * PI;

        *theta_plus_valid = TRUE;
        *theta_minus_valid = TRUE;
      }
      else 
	  {
		sin_theta_plus = (-B + sqrt(discriminant))/(2 * A);
		sin_theta_minus = (-B - sqrt(discriminant))/(2 * A);

	/* Note: The following stmts are safe, as the previous if clauses
	   eliminated the cases in which alpha < TINY.  */

		cos_theta_plus = (-beta/alpha) * sin_theta_plus - (gamma/alpha);
		cos_theta_minus = (-beta/alpha) * sin_theta_minus - (gamma/alpha);

		if (fabs(sin_theta_plus) <= 1 && fabs(cos_theta_plus) <= 1) 
		{
	  		*theta_plus = atan2 (sin_theta_plus, cos_theta_plus);
	  		if (*theta_plus < 0)
	    	*theta_plus += 2 * PI;
	  		*theta_plus_valid = TRUE;
		}

		if (fabs(sin_theta_minus) <= 1 && fabs(cos_theta_minus) <= 1)  
		{
	  		*theta_minus = atan2 (sin_theta_minus, cos_theta_minus);
	  		if (*theta_minus < 0) *theta_minus += 2 * PI;
	  		*theta_minus_valid = TRUE;
	 	}

	 	if (~*theta_plus_valid && ~*theta_minus_valid) return;
   	  }
    }

    /* At this point, we have at least one valid theta  */

    if (*theta_plus_valid)  
	{
      Quat_AxisAngle2Q(SlewAxisBDY, *theta_plus, &Q_plus_Initial2Impinge);
      Quaternion2DirCosMatrix(Q_plus_Initial2Impinge.Quat, 
			      DCM_plus_Initial2Impinge.DCM, DOUBLE_DATA);
      MatrixMult(&DCM_plus_Initial2Impinge.DCM, 
		 DCM_ECI2BDY.DCM, DCM_ECI2BDY_Impinge.DCM,
		 3, 3, 3, DOUBLE_DATA);
      MatrixMult(DCM_ECI2BDY_Impinge.DCM, &ECIFixedPtECI.Vec, 
		 ECIFixedPtBDY_Impinge.Vec, 3, 3, 1, DOUBLE_DATA);
      cos_sep_plus = DotProduct (ECIFixedPtBDY_Impinge.Vec,
		                 &BodyFixedPtBDY.Vec, DOUBLE_DATA);

      if (fabs (cos_sep_plus - cos_sep) < TINY)
        *theta_plus_valid = TRUE;
      else
        *theta_plus_valid = FALSE;
    }

    if (*theta_minus_valid)  
	{
      Quat_AxisAngle2Q(SlewAxisBDY, *theta_minus, &Q_minus_Initial2Impinge);
      Quaternion2DirCosMatrix(Q_minus_Initial2Impinge.Quat, 
			      DCM_minus_Initial2Impinge.DCM, DOUBLE_DATA);
      MatrixMult(&DCM_minus_Initial2Impinge.DCM, 
		 DCM_ECI2BDY.DCM, DCM_ECI2BDY_Impinge.DCM,
		 3, 3, 3, DOUBLE_DATA);
      MatrixMult(DCM_ECI2BDY_Impinge.DCM, &ECIFixedPtECI.Vec, 
		 ECIFixedPtBDY_Impinge.Vec, 3, 3, 1, DOUBLE_DATA);
      cos_sep_minus = DotProduct (ECIFixedPtBDY_Impinge.Vec,
		                  BodyFixedPtBDY.Vec, DOUBLE_DATA);

      if (fabs (cos_sep_minus - cos_sep) < TINY)
        *theta_minus_valid = TRUE;
      else
        *theta_minus_valid = FALSE;
    }
}
/* -----------------------------------------------------------------------
Quat_AxisAngle2Q:
        This function is used to calculate the delta quaternion
        representing a slew, given the slew axis and angle.

Input:
    axis:   VECTOR_type.  Axis of derived slew
    angle:  double.  Angle of derived slew (radians).

Output:
    Q:  QUATERNION_type.  Change in attitude from start to
        end of slew.
*/

int Quat_AxisAngle2Q(VECTOR_type axis, double angle, QUATERNION_type *Q)
{
    int status=0;
    int     i;
    double  mag;
    double  half_angle_radians, sin_half_angle, cos_half_angle;

    mag = VectorMagnitude(&axis, DOUBLE_DATA);
   
    if (mag < TINY)
    {
        status = MATH_ERROR;
        return(status);
    }

    half_angle_radians = 0.5 * angle;
    sin_half_angle = sin (half_angle_radians);
    cos_half_angle = cos (half_angle_radians);

    for (i=0; i<3; i++)
      Q->Quat [i] = axis.Vec[i]/mag * sin_half_angle;

    Q->Quat [3] = cos_half_angle;

    return(status);
}


/* -----------------------------------------------------------------------
Quat_Q2AxisAngle:
        This function is used to calculate slew axis and angle
        given the delta quaternion representing the slew.

Input:
    inputQ:
        QUATERNION_type.  Change in attitude from start to
        end of slew (unit length).

Output:
        axis:   3-element double vector.  Axis of derived slew (unit length).
    angle:  double.  Angle of derived slew (radians).

*/

int Quat_Q2AxisAngle(QUATERNION_type inputQ, VECTOR_type *axis,
                     double *angle)
{
    int status = 0;
    int     i;
    double mag;
    QUATERNION_type NormQ;

    /* Verify that the quaternion is normalized.  If not, normalize it,
       and set the error status.  */

    status = QuaternionVerifyNormalize(inputQ.Quat, NormQ.Quat, DOUBLE_DATA);

   /* Calculate the angle for the recommended slew.  This angle will be in
      the range [0 radians, 2 * PI radians); change that range to
      (-PI radians, PI radians].  */

   *angle = (double) 2 * acos (NormQ.Quat[3]);
   if (*angle > PI)
     *angle = *angle - 2 * PI;

   /* Calculate the axis for the recommended slew  */

   for (i=0; i<3; i++)
     axis->Vec[i] = NormQ.Quat[i];
   mag = VectorMagnitude(axis->Vec, DOUBLE_DATA);

   /* A slew of 0 degrees will yield an indeterminant axis.  The x-axis
      is returned in that case, by default.  */

   if (mag < TINY)
   {
     axis->Vec[0] = 1;
     axis->Vec[1] = 0;
     axis->Vec[2] = 0;
   }
   else
     for (i=0; i<3; i++)
       axis->Vec[i] /= mag;

    return(status);
}

/******************************************************************************
	Change Log:
	$Log: CIS_Slew.c,v $
	Revision 1.1  2000/03/21 23:35:45  jzhao
	Added CIS_Slew functions.
	

******************************************************************************/
