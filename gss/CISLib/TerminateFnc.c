/* $Id: TerminateFnc.c,v 1.11 2000/03/28 22:48:26 jzhao Exp $ */
/* ========================================================================
	FILE NAME:
        TerminateFnc.c

    DESCRIPTION:

    FUNCTION LIST:

 ==========================================================================

    REVISION HISTORY

 ========================================================================== */

 /* System Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Self Defined Headers */
#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <MessageIO.h>
#include <CISGlobal.h>
#include <StarTracker.h>
#include <StarCatalog.h>
#include <IDentifyStars.h>
#include <ChooseOSC.h>
#include <CISExeType.h>
#include <CISExe.h>

/* ======================================================================== */

/*----------------
SunSensorAlignment: Determine the actual alignment of the sun sensor, based 
                    on the body frame-to-ECI attitude estimated in CIS, the
                    original estimated sun sensor alignment, and the sun ECI
                    position.

                    Only the sun sensor line-of-sight may be estimated using
                    the attitude and the sun ECI positon. Assume the new 
                    alignment matrix may be ogtained from the old one by 
                    making the minimal rotation such that the line-of-sight
                    is correct.
Input:
    SUN_ECI:    VECTOR_type. Sun vector in ECI.
    SSBoresight:    VECTOR_type. Sun Sensor boresight in sun sensor frame.
    DCM_ECI2BDY:    DirCosMatrix_type. Estimated attitude ECI to Body frame.
Input/Output:
    Quat_SS2BDY:    QUATERNION_type: Sun sensor alignment Quaternion Sun Sensor
                    frame to Body.
    Quat_BDY2SS:    QUATERNION_type: Sun Sensor alignment Quaternion. Body to 
                    Sun sensor frame.
    DCM_SS2BDY:     DirCosMatrix_type. Sun sensor alignment DCM. Sun Sensor
                    frame to Body.
    DCM_BDY2SS:     DirCosMAtrix_type. Sun Sensor alignmenr DCM. Body to Sun
                    Sensor frame.
*/

void SunSensorAlignment(VECTOR_type *SUN_ECI,
                        VECTOR_type *SSBoresight,
                        DirCosMatrix_type *DCM_ECI2BDY,
                        QUATERNION_type *Quat_SS2BDY,
                        QUATERNION_type *Quat_BDY2SS,
                        DirCosMatrix_type *DCM_SS2BDY,
                        DirCosMatrix_type *DCM_BDY2SS)
{
    VECTOR_type NonAlignedSSBoresightBDY;
    VECTOR_type AlignedSSBoresightBDY;
    VECTOR_type AxisBDYNonNorm;
    VECTOR_type AxisBDY, AxisSS;

    /* Calculate the original and modified sun sensor boresight orientation
        in the spacecraft body frame */

    MatrixMult(DCM_SS2BDY->DCM, SSBoresight->Vec, NonAlignedSSBoresightBDY.Vec,
                3, 3, 1, DOUBLE_DATA); 
    MatrixMult(DCM_ECI2BDY->DCM, SUN_ECI->Vec, AlignedSSBoresightBDY.Vec,
                3, 3, 1, DOUBLE_DATA);

    /* Calculate the rotation axis and(minimum) angle necessary to transforme
        the original boresight position estimate to the new boresight position
        estimate. The minimum angle will be realized if the rotation axis is
        chosen to be perpendicular to both the original and new boresight 
        position estimates.    */

    CrossProduct(AlignedSSBoresightBDY.Vec, NonAlignedSSBoresightBDY.Vec,
                 AxisBDYNonNorm.Vec, DOUBLE_DATA);
    VectorNormalize(AxisBDYNonNorm.Vec, AxisBDY.Vec, DOUBLE_DATA);

    MatrixMult(DCM_BDY2SS->DCM, AxisBDY.Vec, AxisSS.Vec, 3, 3, 1,
                DOUBLE_DATA);  

    /* Calculate the quaternion representing the sun sensor alignment with 
        respect to the body frame. This may be done with a rotation, as    
        described above, but a more direct way exists. The sun sensor boresight
        position is known in both the sun sensor frame(SSBoresight) and the 
        body frame after alignment(AlignedSSBoresightBDY). The rotation axis, 
        calculated above, is known in both the sun sensor frame(AxisSS) and
        the body frame prior to alignment(AxisBDY). Since it is an axis of 
        rotation, AxisBDY wikk be the position in the body frame both before
        and after alignment. Therefor, the boresight and rotation axis vector
        are known in both the sun sensor and post-alignment body frames, so
        they can be used to determine the quaternion. */   

    QuatTRIAD(SSBoresight->Vec, AlignedSSBoresightBDY.Vec,
              AxisSS.Vec, AxisBDY.Vec, Quat_SS2BDY->Quat);
   
    /* Derive the sun sensor alignment DCM from Quaternion */
    
	QuaternionInvers(Quat_SS2BDY->Quat, Quat_BDY2SS->Quat, DOUBLE_DATA); 
	Quaternion2DirCosMatrix(Quat_SS2BDY->Quat, DCM_SS2BDY->DCM, DOUBLE_DATA); 
	MatrixTranspose(DCM_SS2BDY->DCM, DCM_BDY2SS->DCM, 3, 3, DOUBLE_DATA);
}

/*---------------------------------
SetReferenceTrackers:	Determine which tracker (or trackers) will be reference
						They will be the trackers tracking the most identifiable
						stars. (in the event of a tie, the reference tracker
						will be the one with the smaller estimated error 
						parallel to the boresight) It is assumed that at
						least one tracker is tracking ID'able stars when this 
						function is called. If only one tracker is doing so,
						the Ref2 tracker index will be set to 0.
Input:
	NumStarID: Array[NUM_ST} of int.
	ValidRefST:	Array[NUM_ST] of int.
	ParallelErr:	Array[NUM_ST} of double.
Output:
	RefST1, RefST2:	int
*/

void SetReferenceTrackers(int NumStarID[], int ValidRefST[], 
						  double ParallelErr[], 
                          int DefaultRefST1_ID, int DefaultRefST2_ID,
                          int *RefST1_ID, int *RefST2_ID)
{
	int ST_indx;
	int Ref1 = 0;
	int Ref2 = 0;
	int DefaultRef1 = FALSE;
	int DefaultRef2 = FALSE;

	/* Check Default Reference Star Tracker ... */
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		if ( (ValidRefST[ST_indx]) && ( (ST_indx+1 == DefaultRefST1_ID) ||
										(ST_indx+1 == DefaultRefST2_ID) ) )
		{
			if (Ref1 == 0)	
			{
				Ref1 = ST_indx+1;			
				DefaultRef1 = TRUE;
			}
			else if ( (NumStarID[ST_indx] > NumStarID[Ref1-1]) || 
					  ( (NumStarID[ST_indx] == NumStarID[Ref1-1]) &&
					    (ParallelErr[ST_indx] < ParallelErr[Ref1-1]) ) )
			{
				Ref2 = Ref1;
				Ref1 = ST_indx+1;
				DefaultRef2 = TRUE;
			}	
			else if (Ref2 == 0)
			{
				Ref2 = ST_indx+1;
				DefaultRef2 = TRUE;
			}
			else if ( (NumStarID[ST_indx] > NumStarID[Ref2-1]) || 
					  ( (NumStarID[ST_indx] == NumStarID[Ref2-1]) &&
					   (ParallelErr[ST_indx] < ParallelErr[Ref2-1]) ) )
				Ref2 = ST_indx+1;	
		}
	}

	/* Check Non_Default Reference Star Tracker, 
							if thera are any openings ... */
	if (Ref2 == 0)
	{
		for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		{
			if ( (ValidRefST[ST_indx]) && 
				 (ST_indx+1 != DefaultRefST1_ID) &&
				 (ST_indx+1 != DefaultRefST2_ID) )
			{
				if (Ref1 == 0)	
					Ref1 = ST_indx+1;			
				else if ( (!DefaultRef1) &&  
						( (NumStarID[ST_indx] > NumStarID[Ref1-1]) || 
					    ( (NumStarID[ST_indx] == NumStarID[Ref1-1]) &&
					    (ParallelErr[ST_indx] < ParallelErr[Ref1-1]) ) ) )
				{
					Ref2 = Ref1;
					Ref1 = ST_indx+1;
				}	
				else if (Ref2 == 0)
					Ref2 = ST_indx+1;
				else if ( (!DefaultRef2) && 
						( (NumStarID[ST_indx] > NumStarID[Ref2-1]) || 
					    ( (NumStarID[ST_indx] == NumStarID[Ref2-1]) &&
					   	(ParallelErr[ST_indx] < ParallelErr[Ref2-1]) ) ) )
					Ref2 = ST_indx+1;	
			}
		}
	}

	*RefST1_ID = Ref1;
	*RefST2_ID = Ref2;
}

/* -------------------------------
CalECI2BDY: Calculate ECI2BDY Quaternion and DCM

Input:
	Quat_ECI2ST: QUATERNION_type.
	Quat_ST2BDY: QUATERNION_type.
Output:
	Quat_ECI2BDY: QUATERNION_type. 
	Quat_BDY2ECI: QUATERNION_type.
	DCM_ECI2BDY: DirCosMatrix_type. 
	DCM_BDY2ECI: DirCosMatrix_type. 
*/

void CalECI2BDY(QUATERNION_type *Quat_ECI2ST, 
				QUATERNION_type *Quat_ST2BDY,
				QUATERNION_type *Quat_ECI2BDY, 
				QUATERNION_type *Quat_BDY2ECI, 
				DirCosMatrix_type *DCM_ECI2BDY,
				DirCosMatrix_type *DCM_BDY2ECI) 
{
	QuaternionMult( Quat_ECI2ST->Quat, Quat_ST2BDY->Quat,
                        			Quat_ECI2BDY->Quat, DOUBLE_DATA);
    Quaternion2DirCosMatrix( Quat_ECI2BDY->Quat, DCM_ECI2BDY->DCM,
                                                        DOUBLE_DATA);
	QuaternionInvers(Quat_ECI2BDY->Quat, Quat_BDY2ECI->Quat, DOUBLE_DATA); 	
	MatrixTranspose(DCM_ECI2BDY->DCM, DCM_BDY2ECI->DCM, 3, 3, DOUBLE_DATA);
}

/* -------------------
*/

int AcqStatus(int IDContradictory[], int IDRef)
{
	int ST_indx;
	int NumContradictoryID;
	int stat;

    NumContradictoryID = 0;
    for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
        if (IDContradictory[ST_indx]) NumContradictoryID++;

    if ( IDRef > 0 )
    {
        if ( NumContradictoryID > 0 )  stat = CONTRADICTORY;
        else                           
		{
			if ( IDRef > 1 ) stat = TOTAL_SUCCESS;
			else			 stat = PARTIAL_SUCCESS;
		}
    }
    else    
        stat = NO_ENOUGH_IDSTARS;

	return(stat);
}

/*--------------------
*/

void WriteString(char *String, char *s)
{
	fprintf(LogFp, "%s %s\n", s, String);
}

void WriteInt(int a, char *s)
{
	fprintf(LogFp, "%s %10d\n", s, a);
}

void WriteDouble(double a, char *s)
{
	fprintf(LogFp, "%s %16.7f\n", s, a);
}

void WriteVector(double Vec[], char *s)
{
	int i;

	fprintf(LogFp, "%s\n", s);
	for (i=0; i<3; i++)
		fprintf(LogFp, "%12.7f ", Vec[i]); 

	fprintf(LogFp, "\n\n");
}

void WriteMatrix(DirCosMatrix_type *Matrix, char *s)
{
	int i, j;

	fprintf(LogFp, "%s\n", s);
	for ( i=0; i<3; i++)
	{
		for ( j=0; j<3; j++)
			fprintf(LogFp, "%12.7f ", Matrix->DCM[i*3+j]); 
		
		fprintf(LogFp, "\n");
	}
	fprintf(LogFp, "\n");
}	

void WriteQuaternion(QUATERNION_type *Quat, char *s)
{
	int i;

	fprintf(LogFp, "%s\n", s);
	for ( i=0; i<4; i++)
		fprintf(LogFp, "%12.7f ", Quat->Quat[i]);
	
	fprintf(LogFp, "\n\n");
}	

void WriteMatrixNUM_ST(DirCosMatrix_type Matrix[], char *s)
{
	int ST_indx;
	char ss[10];
	
	fprintf(LogFp, "%s\n", s); 
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		sprintf(ss, "ST %4d", ST_indx+1); 
		WriteMatrix(&Matrix[ST_indx], ss);
	}
}

void WriteQuaternionNUM_ST(QUATERNION_type Quat[], char *s)
{
	int ST_indx;
	char ss[10];

	fprintf(LogFp, "%s\n", s);
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		sprintf(ss, "ST %4d", ST_indx+1);
		WriteQuaternion(&Quat[ST_indx], ss);
	}
}

void WriteDoubleNUM_ST(double d[], char *s) 
{
	int ST_indx;
	char ss[10];

	fprintf(LogFp, "%s\n", s);
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		sprintf(ss, "ST %4d", ST_indx+1);
		WriteDouble(d[ST_indx], ss);
	}
	fprintf(LogFp, "\n");
}

void WriteIntNUM_ST(int d[], char *s) 
{
	int ST_indx;
	char ss[10];

	fprintf(LogFp, "%s\n", s);
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		sprintf(ss, "ST %4d", ST_indx+1);
		WriteInt(d[ST_indx], ss);
	}
	fprintf(LogFp, "\n");
}

void WriteRawVTs(TrackerOutputVT_type RawVTs[], char *s)
{
	int ST_indx, VT_indx;

	fprintf(LogFp, "\n%s\n", s);
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		fprintf(LogFp, "ST_ID %4d\n", RawVTs[ST_indx].ST_ID);
		fprintf(LogFp, "NumVTs %4d\n", RawVTs[ST_indx].NumVTs);
		for (VT_indx=0; VT_indx<RawVTs[ST_indx].NumVTs; VT_indx++)
		{
			fprintf(LogFp, "VT_ID %10d\n", 
								RawVTs[ST_indx].VTs[VT_indx].VT_ID);
            fprintf(LogFp, "HPos %12.7f\n",
                            	RawVTs[ST_indx].VTs[VT_indx].HPos*RTD);
            fprintf(LogFp, "VPos %12.7f\n",
                            	RawVTs[ST_indx].VTs[VT_indx].VPos*RTD);
            fprintf(LogFp, "Magnitude %12.7f\n\n",
                            	RawVTs[ST_indx].VTs[VT_indx].Magnitude);
		}
	}
}

void WriteIDStars(IDStarOutput_type IDStars[], char *s)
{
	int ST_indx, Star_indx;

	fprintf(LogFp, "%s\n\n", s);
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		fprintf(LogFp, "ST_ID %4d\n", IDStars[ST_indx].ST_ID);
		fprintf(LogFp, "NumIDStars %4d\n", IDStars[ST_indx].NumStars);
		for (Star_indx=0; Star_indx<IDStars[ST_indx].NumStars; Star_indx++)
		{
			fprintf(LogFp, "ASC_ID %10d\n", 
								IDStars[ST_indx].Stars[Star_indx].ASC_ID);
			fprintf(LogFp, "OSC_ID %10d\n", 
								IDStars[ST_indx].Stars[Star_indx].OSC_ID);
			fprintf(LogFp, "BSS_ID %10d\n", 
								IDStars[ST_indx].Stars[Star_indx].BSS_ID);
			fprintf(LogFp, "SubSC_ID %10d\n", 
								IDStars[ST_indx].Stars[Star_indx].SubSC_ID);
			fprintf(LogFp, "VT_ID %10d\n", 
								IDStars[ST_indx].Stars[Star_indx].VT_ID);
			fprintf(LogFp, "MagnitudeCat %12.7f\n", 
								IDStars[ST_indx].Stars[Star_indx].MagnitudeCat);
			fprintf(LogFp, "MagnitudeTM %12.7f\n", 
								IDStars[ST_indx].Stars[Star_indx].MagnitudeTM);
			WriteVector(IDStars[ST_indx].Stars[Star_indx].STFrame, "STFrame");
		}
	}
}

void WritePosStatistics(StatisticsVT_type PosStatistics[], char *s)
{
	int ST_indx;

	fprintf(LogFp, "%s\n", s);
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		fprintf(LogFp, "\nST %4d\n", ST_indx+1);
		fprintf(LogFp, "Centroid_H %12.7f  Centroid_V %12.7f\n", 
						RTD*PosStatistics[ST_indx].Mean_H,
						RTD*PosStatistics[ST_indx].Mean_V); 
		fprintf(LogFp, "StdDev_H   %12.7f  StdDev_V   %12.7f\n",
						RTD*PosStatistics[ST_indx].StdDev_H,
						RTD*PosStatistics[ST_indx].StdDev_V); 
		fprintf(LogFp, "Spread %12.7f\n", RTD*PosStatistics[ST_indx].Spread);	
	}
}

void WriteRecommendTrack(RecommendVT_type RecommendTrack[], char *s)
{
	int ST_indx, VT_indx;

    fprintf(LogFp, "%s\n", s);
    for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
    {
        fprintf(LogFp, "\nST_ID %4d\n", RecommendTrack[ST_indx].ST_ID);
        fprintf(LogFp, "NumVTs %4d\n", RecommendTrack[ST_indx].NumVTs);
        for (VT_indx=0; VT_indx<RecommendTrack[ST_indx].NumVTs; VT_indx++)
        {
            fprintf(LogFp, "\nASC_ID %10d\n",
                            RecommendTrack[ST_indx].Stars[VT_indx].ASC_ID);
            fprintf(LogFp, "OSC_ID %10d\n",
                            RecommendTrack[ST_indx].Stars[VT_indx].OSC_ID);
            fprintf(LogFp, "BSS_ID %10d\n",
                            RecommendTrack[ST_indx].Stars[VT_indx].BSS_ID);
            fprintf(LogFp, "VT_ID %10d\n",
                            RecommendTrack[ST_indx].Stars[VT_indx].VT_ID);
            fprintf(LogFp, "HPos %12.7f\n",
                            RecommendTrack[ST_indx].Stars[VT_indx].HPos*RTD);
            fprintf(LogFp, "VPos %12.7f\n",
                            RecommendTrack[ST_indx].Stars[VT_indx].VPos*RTD);
            fprintf(LogFp, "Magnitude %12.7f\n",
                            RecommendTrack[ST_indx].Stars[VT_indx].Magnitude);
        }
    }
}

void WriteSTList(STList_type *STList, char *s)
{
	int ST_indx;

    fprintf(LogFp, "%s\n", s);
    for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
    {
        fprintf(LogFp, "ST_ID %4d\n", STList->ST_ID[ST_indx]);
	}
	fprintf(LogFp, "\n");
}

void WriteCISOutput(CISOutput_type *CISOutput, char *s)
{
	int ST_indx, VT_indx;

    fprintf(LogFp, "%s\n", s);
	fprintf(LogFp, "\nIDStars\n");
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
        fprintf(LogFp, "\nST_ID %4d\n", ST_indx+1); 
		for (VT_indx=0; VT_indx<NUM_VT; VT_indx++)
		{
			fprintf(LogFp, "VT_%1d BSS_ID %10d\n", VT_indx+1,
				CISOutput->IdentifiedStars[ST_indx].Stars[VT_indx].BSS_ID);
			fprintf(LogFp, "VT_%1d OSC_ID %10d\n", VT_indx+1,
				CISOutput->IdentifiedStars[ST_indx].Stars[VT_indx].OSC_ID);
		}
	}

	fprintf(LogFp, "\nRecommendTrack\n");
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
        fprintf(LogFp, "\nST_ID %4d\n", ST_indx+1); 
		for (VT_indx=0; VT_indx<NUM_VT; VT_indx++)
		{
			fprintf(LogFp, "VT_%1d HPos %12.7f\n", VT_indx+1,
				CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].HPos);
			fprintf(LogFp, "VT_%1d VPos %12.7f\n", VT_indx+1,
				CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].VPos);
			fprintf(LogFp, "VT_%1d Magnitude %12.7f\n", VT_indx+1,
				CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].Magnitude);
			fprintf(LogFp, "VT_%1d TrackAction %1d\n", VT_indx+1,
				CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].TrackAction);
		}
	}
}

#ifdef CIS_TEST
/*------------------------------------------------------

	Write Test Output to Match D. Needelman's output format for resault comparision.

-------------------------------------------------------- */
void TestOutString(char *s)
{
	fprintf(TestOutFp, "%s\n", s);
}

void TestOutCount(char *s)
{
	fprintf(TestOutFp, "%s %d\n", s, TestCount); 
}

void TestOutInt(int a, char *s)
{
	fprintf(TestOutFp, "%s (%1d) = % 10d;\n", s, TestCount, a);   
}

void TestOutDouble(double a, char *s)
{
	fprintf(TestOutFp, "%s (%1d) = % 12.6e;\n", s, TestCount, a);   
}

void TestOutIntNUM_ST(int a[], char *s)
{
	int indx;
	for (indx=0; indx<NUM_ST; indx++)
		fprintf(TestOutFp, "%s (%1d, %1d) = % 10d;\n", s, TestCount, indx+1, a[indx]);
}

void TestOutDoubleNUM_ST(double a[], char *s)
{
	int indx;
	for (indx=0; indx<NUM_ST; indx++)
		fprintf(TestOutFp, "%s (%1d, %1d) = % 12.6e;\n", s, TestCount, indx+1, a[indx]);
}

void TestOutQuaternion(QUATERNION_type *Quat, char *s)
{
	int i;
	for (i=0; i<4; i++)
		fprintf(TestOutFp, "%s (%1d, %1d) = % 12.6e;\n", s, TestCount, i+1, 
																Quat->Quat[i]);

	TestOutString("%");
}

void TestOutQuaternionNUM_ST(QUATERNION_type Quat[], char *s)
{
	int ST_indx;
	char ss[50];

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		sprintf(ss, "%s%1d", s, ST_indx+1);
		TestOutQuaternion(&Quat[ST_indx], ss);
	}
}

void TestOutMatrix(DirCosMatrix_type *Matrix, char *s)
{
	int i, j;
	char ss[50];

	for ( i=0; i<3; i++)
	{
		sprintf(ss, "%s%1d", s, i+1);
		for ( j=0; j<3; j++)
			fprintf(TestOutFp, "%s (%1d, %1d) = % 12.6e;\n", ss, TestCount, j+1, 
															Matrix->DCM[i*3+j]);
		TestOutString("%");
	}
}

void TestOutIDStars(IDStarOutput_type IDStars[], 
					RecommendVT_type RecommendTrack[],
					char *s1, char *s2)
{
	int ST_indx;
	int indx;
	char ss[50];

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		sprintf(ss, "%s_asc%1d", s1, ST_indx+1);
		for (indx=0; indx<IDStars[ST_indx].NumStars; indx++)
			fprintf(TestOutFp, "%s (%1d, %1d) = % 10d;\n", ss, TestCount, indx+1, 
													IDStars[ST_indx].Stars[indx].ASC_ID);
		TestOutString("%");

		sprintf(ss, "%s_osc%1d", s1, ST_indx+1);
		for (indx=0; indx<IDStars[ST_indx].NumStars; indx++)
			fprintf(TestOutFp, "%s (%1d, %1d) = % 10d;\n", ss, TestCount, indx+1, 
													IDStars[ST_indx].Stars[indx].BSS_ID);
		TestOutString("%");

		sprintf(ss, "%s_vt%1d", s1, ST_indx+1);
		for (indx=0; indx<IDStars[ST_indx].NumStars; indx++)
			fprintf(TestOutFp, "%s (%1d, %1d) = % 10d;\n", ss, TestCount, indx+1, 
													IDStars[ST_indx].Stars[indx].VT_ID);
		TestOutString("%");

		sprintf(ss, "%s_osc%1d", s2, ST_indx+1);
		for (indx=0; indx<RecommendTrack[ST_indx].NumVTs; indx++)
			fprintf(TestOutFp, "%s (%1d, %1d) = % 10d;\n", ss, TestCount, indx+1, 
												RecommendTrack[ST_indx].Stars[indx].BSS_ID);
		TestOutString("%");
	}
}

#endif
/*********************************************************************************** 
	Change Log:
	$Log: TerminateFnc.c,v $
	Revision 1.11  2000/03/28 22:48:26  jzhao
	Changed CISOutput format.
	
	Revision 1.10  2000/03/28 22:14:57  jzhao
	Changed priority of recommended tracks.
	Changed output format of recommended tracks.
	
	Revision 1.9  2000/03/21 23:35:45  jzhao
	Added CIS_Slew functions.
	
	Revision 1.8  2000/02/24 00:13:29  jzhao
	Modified for port to NT
	
	Revision 1.7  2000/02/19 01:18:38  jzhao
	Undefined CIS_TEST
	
	Revision 1.6  2000/02/11 19:26:11  jzhao
	Modified Log Foramt.
	
	Revision 1.5  2000/02/10 18:25:25  jzhao
	Modiffied CIS Log Format.
	
	Revision 1.4  2000/02/09 23:54:12  jzhao
	Modified for intergrating to GUI.
	
	Revision 1.3  1999/12/22 01:09:21  jzhao
	Modified on create BestIDStars List.
	
	Revision 1.2  1999/12/15 01:16:12  jzhao
	Modified for Multiple cases Testing.
	
	Revision 1.1  1999/12/13 18:42:53  jzhao
	Initial Release.
	
************************************************************************************/
