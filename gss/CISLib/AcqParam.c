/* $Id: AcqParam.c,v 1.1 1999/12/13 18:42:50 jzhao Exp $ */
/*====================================================================
	
	FILE NAME:
		AcqParam.c

	DESCRIPTION:
		This file include functions to calculate parameters used in 
		Acquisition. 

	FUNCTION LIST:

 =====================================================================

	REVISION HISTORY

 ===================================================================== */

 /* System Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Self defined headers */

#include <CISGlobal.h>
#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <StarTracker.h>
#include <AcqParam.h>

/*-----------------------------------
GetSunSensorLOSInSTFrame:	Calculate orientation of the sun sensor LOS in the 
							various Star Tracker frames.

Input:
	SunSensorLOSnorm:	vector.
	DCM_BDY2ST:	 		Array[NUM_ST] of DirCosMatrix_type. Direction
						Cosine Matrix from BDY(body frame) to ST frame.

Output:
	SunST:				Array[NUM_ST] of VECTOR_type. Sun Sensor LOS
						in ST frame.
*/

void GetSunSensorLOSInSTFrame(VECTOR_type *SunSensorLOSnorm,
							  DirCosMatrix_type DCM_BDY2ST[],
							  VECTOR_type SunST[])
{
	int ST_indx;

	for(ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		MatrixMult(DCM_BDY2ST[ST_indx].DCM, SunSensorLOSnorm->Vec,
				   SunST[ST_indx].Vec, 3, 3, 1, DOUBLE_DATA); 
}

/*-----------------------------------------------
GetTransformECI2ST:	Calculate the ECI to Star Tracker Frame Transformation, 
					using ECI2BDY, and BDY2ST information.

Input:
	Quat_ECI2BDY:	QUATERNION_type. Quaternion of ECI to BDY. 
	Quat_BDY2ST:	Array[NUM_ST] of QUATERNION_type. Quaternions of BDY to
					each Star Tracker Frame.

Output:
	Quat_ST2ECI:	Array[NUM_ST] of QUATERNION_type
	Quat_ECI2St:	Array[NUM_ST] of QUATERNION_type.
	DCM_ST2ECI:		Array[NUM_ST] of QUATERNION_type.
	DCM_ECI2ST:		Array[NUM_ST] of QUATERNION_type.

*/

void GetTransformECI2ST(QUATERNION_type *Quat_ECI2BDY,
						QUATERNION_type Quat_BDY2ST[],
						QUATERNION_type Quat_ST2ECI[],
						QUATERNION_type Quat_ECI2ST[],
						DirCosMatrix_type DCM_ST2ECI[],
						DirCosMatrix_type DCM_ECI2ST[])
{
	int ST_indx;

	for(ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		/* Get Quat_ECI2ST */
		QuaternionMult(Quat_ECI2BDY->Quat, Quat_BDY2ST[ST_indx].Quat,
					   Quat_ECI2ST[ST_indx].Quat, DOUBLE_DATA);

		/* Get Quat_ST2ECI */
		QuaternionInvers(Quat_ECI2ST[ST_indx].Quat, 
						 Quat_ST2ECI[ST_indx].Quat, DOUBLE_DATA);

		/* Get DCM_ST2ECI */
		Quaternion2DirCosMatrix(Quat_ST2ECI[ST_indx].Quat, 
								DCM_ST2ECI[ST_indx].DCM, DOUBLE_DATA); 

		/* Get DCM_ECI2ST */
		MatrixTranspose(DCM_ST2ECI[ST_indx].DCM, 
						DCM_ECI2ST[ST_indx].DCM, 3, 3, DOUBLE_DATA);
	}
}

/* --------------------------------- 
Input:
	RotAngle:	(in radians)
*/

void GetSTOrientation(VECTOR_type RotAngle[], int RotOrder,
	 	 		 	QUATERNION_type Quat_BDY2ST[],
				 	QUATERNION_type Quat_ST2BDY[],
				 	DirCosMatrix_type DCM_BDY2ST[],
				 	DirCosMatrix_type DCM_ST2BDY[])
{
	int ST_indx;
	QUATERNION_type Iden_Quat = {0, 0, 0, 1};

	for(ST_indx = 0; ST_indx < NUM_ST; ST_indx++)
	{
		/* Calculate Quat_BDY2ST */ 
		if(RotOrder == ROTATE123)
			QuaternionRotateEuler123(Iden_Quat.Quat,
									 Quat_BDY2ST[ST_indx].Quat,
									 RotAngle[ST_indx].Vec[0],
									 RotAngle[ST_indx].Vec[1],
									 RotAngle[ST_indx].Vec[2],
									 DOUBLE_DATA);
		else if(RotOrder == ROTATE213)
			QuaternionRotateEuler213(Iden_Quat.Quat,
									 Quat_BDY2ST[ST_indx].Quat,
									 RotAngle[ST_indx].Vec[0],
									 RotAngle[ST_indx].Vec[1],
									 RotAngle[ST_indx].Vec[2],
									 DOUBLE_DATA);
			
		/* Calculate Quat_ST2BDY(inverse of Quat_BDY2ST) */
		QuaternionInvers(Quat_BDY2ST[ST_indx].Quat, 
						 Quat_ST2BDY[ST_indx].Quat, DOUBLE_DATA);

		/* Calculate DCM_BDY2ST */
		Quaternion2DirCosMatrix(Quat_BDY2ST[ST_indx].Quat, 
								DCM_BDY2ST[ST_indx].DCM, DOUBLE_DATA); 

		/* Calcalate DCM_ST2BDY */
		MatrixTranspose(DCM_BDY2ST[ST_indx].DCM, 
						DCM_ST2BDY[ST_indx].DCM, 3, 3, DOUBLE_DATA);
	}
}

/*----------------------- 
GetSTLosBDY:  Calculates the Star Tracker Line of sight vectors in BDY frame.

Input:
	STLos: VECTOR_type. Star Tracker Line of sight in ST Frame(ST Boresight).
	DCM_ST2BDY:	Array[NUM_ST] of DirCosMatrix_type. 
Output:
	STLosBDY:	Array[NUM_ST] of VECTOR_type.

*/
void GetSTLosBDY(VECTOR_type *STLos, 
				 DirCosMatrix_type DCM_ST2BDY[],
				 VECTOR_type STLosBDY[])
{
	int ST_indx;

	for(ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		MatrixMult(DCM_ST2BDY[ST_indx].DCM, STLos->Vec,
				   STLosBDY[ST_indx].Vec, 3, 3, 1, DOUBLE_DATA);
	}
}

/************************************************************************************** 
	Change Log:
	$Log: AcqParam.c,v $
	Revision 1.1  1999/12/13 18:42:50  jzhao
	Initial Release.
	
***************************************************************************************/
