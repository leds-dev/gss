/* $Id: CISExe.c,v 1.27 2000/05/22 23:26:42 jzhao Exp $ */
/* ========================================================================
    FILE NAME:
        CISExe.c

    DESCRIPTION:
		This is file include Main routines to implement Confused In Space(CIS)
		Algorithm.

    FUNCTION LIST:

 ==========================================================================

    REVISION HISTORY

 ========================================================================== */

 /* System Headers */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

/* Self Defined Headers */
#include <MessageIO.h>
#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <CISGlobal.h>
#include <ephem_intf.h>
#include <StarTracker.h>
#include <StarCatalog.h>
#include <AcqParam.h>
#include <IDentifyStars.h>
#include <IdentifyPrimary.h>
#include <EstimateAttitude.h>
#include <ChooseOSC.h>
#include <SunTrack.h>
#include <CIS_Slew.h>
#include <CISExeType.h>
#include <CISExe.h>
#include <TerminateFnc.h>

/* Macros */

#define ERROR_TEXT "in CISExe"

/*-------------------------  
	Global Variables(external) 
------------------------- */

FILE *LogFp;

int PRIMARY_ST_TIGHT_THRESHOLD;		/* direct tight match threshold */
double MAX_ROUGH_KNOWLEDGE_ERROR;	/* radians */
double MAX_SEPARAT_PRIMARY_TOL;		/* radians */
double MAX_SUN_PRIMARY_ERROR;		/* radians */
double MAX_SUN_NONPRIMARY_ERROR;	/* radians */
double MAX_ROUGH_PRIMARY_ERROR;		/* radians */

double MAX_ST_MISALIGNMENT_ERROR;	/* radians */
double MAX_SEPARAT_NONPRIMARY_TOL;	/* radians */
double PARALLEL_ERROR_THRESHOLD;	/* radians */
double MAG_SATURATION;			/* saturation of magnitude */
double MAGNITUDE_BIAS;			/* Magnitude Bias from VT reading */
double SMS_FOV;				/* radians */
double ATTITUDE_UNCERTAINTY;		/* radians */
double OVERLAP_PIXELS;			/* pixels */
double SPREAD_LIMIT;			/* radians */

/*----------------- 
	Local Globals
------------------- */

int DefaultRefST1_ID = 1;
int DefaultRefST2_ID = 3;
int PWR_Config = 0;
int GoodAlign = FALSE;

VECTOR_type STBoresight; 
VECTOR_type SSBoresight;
VECTOR_type SunSensorLOSBdy;
VECTOR_type SunECI; 

QUATERNION_type Quat0_ECI2BDY;
QUATERNION_type Quat0_BDY2ECI;
QUATERNION_type Quat0_SS2BDY;
QUATERNION_type Quat0_BDY2SS;
QUATERNION_type  Quat0_BDY2ST[NUM_ST];
QUATERNION_type  Quat0_ST2BDY[NUM_ST];
DirCosMatrix_type DCM0_BDY2ST[NUM_ST];
DirCosMatrix_type DCM0_ST2BDY[NUM_ST];
DirCosMatrix_type DCM0_ECI2BDY;
DirCosMatrix_type DCM0_BDY2ECI;
DirCosMatrix_type DCM0_SS2BDY;
DirCosMatrix_type DCM0_BDY2SS;

STAYOUT_type StayOutZones[MAX_EPHEM_OBJS];
SunStruc_type SunStruc;
/* -----------------
ReadIniVars(IniVariables); 
*/

void ReadIniVars(IniVariable_type *IniVariables)
{
	int i;

	memcpy(&Quat0_ECI2BDY, &(IniVariables->Quat_ECI2BDY), 
											sizeof(QUATERNION_type));
	memcpy(&Quat0_SS2BDY, &(IniVariables->Quat_SS2BDY), 
											sizeof(QUATERNION_type));
	memcpy(Quat0_ST2BDY, IniVariables->Quat_ST2BDY, 
											NUM_ST*sizeof(QUATERNION_type)); 
	memcpy(&DCM0_ECI2BDY, &(IniVariables->DCM_ECI2BDY), 
											sizeof(DirCosMatrix_type)); 
	memcpy(&DCM0_SS2BDY, &(IniVariables->DCM_SS2BDY), 
											sizeof(DirCosMatrix_type));  
	memcpy(DCM0_ST2BDY, IniVariables->DCM_ST2BDY, 
											NUM_ST*sizeof(DirCosMatrix_type)); 

	memcpy(StayOutZones, IniVariables->StayOutZones, 
											MAX_EPHEM_OBJS*sizeof(STAYOUT_type));
	for (i=0; i<MAX_EPHEM_OBJS; i++) StayOutZones[i].Radius *= DTR;

	memcpy(&SunStruc, &(IniVariables->SunStruc), sizeof(SunStruc_type));
	SunStruc.StayoutRadius *= DTR;	

	MAX_ROUGH_KNOWLEDGE_ERROR = DTR * IniVariables->MAX_ROUGH_KNOWLEDGE_ERROR;
	MAX_ST_MISALIGNMENT_ERROR = DTR * IniVariables->MAX_ST_MISALIGNMENT_ERROR; 
	MAX_SEPARAT_PRIMARY_TOL = DTR * IniVariables->MAX_SEPARAT_PRIMARY_TOL; 
	PARALLEL_ERROR_THRESHOLD = DTR * IniVariables->PARALLEL_ERROR_THRESHOLD; 
	MAG_SATURATION = IniVariables->MAG_SATURATION; 
	MAGNITUDE_BIAS = IniVariables->MAGNITUDE_BIAS; 
	SMS_FOV = DTR * IniVariables->SMS_FOV;
	ATTITUDE_UNCERTAINTY = DTR * IniVariables->ATTITUDE_UNCERTAINTY;
	OVERLAP_PIXELS = IniVariables->OVERLAP_PIXELS;
	SPREAD_LIMIT = DTR * IniVariables->SPREAD_LIMIT;

    PWR_Config = IniVariables->PWR_Config;
    GoodAlign = IniVariables->GoodAlign;
}

/* ------------------------------
Initialization
*/

void Initialization(IniVariable_type *IniVariables) 
{
	int i;

	/* Star Tracker boresight */
	STBoresight.Vec[0] = 0;
	STBoresight.Vec[1] = 0;
	STBoresight.Vec[2] = 1;

	/* Sun Sensor boresight */
	SSBoresight.Vec[0] = 1;
	SSBoresight.Vec[1] = 0;
	SSBoresight.Vec[2] = 0;

	/* Read Initial Variables through GUI */
	ReadIniVars(IniVariables); 

	/* Calculate Initial BDY2ECI, ST2BDY, BDY2SS */
	QuaternionInvers(Quat0_ECI2BDY.Quat, Quat0_BDY2ECI.Quat, DOUBLE_DATA);
	QuaternionInvers(Quat0_SS2BDY.Quat, Quat0_BDY2SS.Quat, DOUBLE_DATA);
	MatrixTranspose(DCM0_ECI2BDY.DCM, DCM0_BDY2ECI.DCM, 3, 3, DOUBLE_DATA);
	MatrixTranspose(DCM0_SS2BDY.DCM, DCM0_BDY2SS.DCM, 3, 3, DOUBLE_DATA);

	for (i=0; i<NUM_ST; i++)
	{
		QuaternionInvers(Quat0_ST2BDY[i].Quat, Quat0_BDY2ST[i].Quat, DOUBLE_DATA);
		MatrixTranspose(DCM0_ST2BDY[i].DCM, DCM0_BDY2ST[i].DCM, 3, 3, DOUBLE_DATA);
	}

    /* Primary ST Tight match tolerance(Global Var) */
	if ( GoodAlign ) 
       PRIMARY_ST_TIGHT_THRESHOLD = PRIMARY_ST_TIGHT_THRESHOLD_GOOD_ALIGN;
    else             
       PRIMARY_ST_TIGHT_THRESHOLD = PRIMARY_ST_TIGHT_THRESHOLD_BAD_ALIGN;

	/* Position and separation tolerance(Global Vars) */
	MAX_SEPARAT_NONPRIMARY_TOL = MAX_SEPARAT_PRIMARY_TOL + 
								 2.0*MAX_ST_MISALIGNMENT_ERROR; /* radians */
	MAX_SUN_PRIMARY_ERROR = MAX_SUN_POINTING_ERROR + 
							MAX_SEPARAT_PRIMARY_TOL +
							MAX_ST_MISALIGNMENT_ERROR; /* radians */	
	MAX_SUN_NONPRIMARY_ERROR = MAX_SUN_PRIMARY_ERROR +
							   MAX_ST_MISALIGNMENT_ERROR; /* radians */
	MAX_ROUGH_PRIMARY_ERROR = MAX_ROUGH_KNOWLEDGE_ERROR + 
							  MAX_SEPARAT_PRIMARY_TOL; /* radians */

    /* Set Default Reference ST for Alignment */
    switch (PWR_Config)
    {
		case PWR12:
				DefaultRefST1_ID = 1;
				DefaultRefST2_ID = 2;	
			 break;			
		case PWR23:
				DefaultRefST1_ID = 2;
				DefaultRefST2_ID = 3;	
			 break;			
		case PWR13:
				DefaultRefST1_ID = 1;
				DefaultRefST2_ID = 3;	
			 break;			
		case PWR123:
				DefaultRefST1_ID = 1;
				DefaultRefST2_ID = 3;	
			 break;			
		default:
			 break;
    }
	/* Sun Vector in ECI */
	memcpy(&SunECI, &(StayOutZones[GSS_SUN].ECI), sizeof(VECTOR_type));

	/* Sun Sensor Line of sight in S/C Body Frame */
	MatrixMult(DCM0_SS2BDY.DCM, SSBoresight.Vec, SunSensorLOSBdy.Vec,
													3, 3, 1, DOUBLE_DATA);
}

/* --------------------------------------
GetRoughKnowledgeRadius:	Get Radius of MaxFOV for Rough knowledge Mode

Input:
	IniVars:		IniVariable_type: Initial Variables from INI file.
Output:
	Radius:	double. (radians).
*/

void GetRoughKnowledgeRadius( IniVariable_type *IniVars, double *Radius)
{
	Initialization(IniVars); 
	CalRadiuSpotSTFOV(MAX_ROUGH_PRIMARY_ERROR, Radius);
}

/* --------------------------------------
GetSunHoldAnnulus:	Get InnerRadius and OuterRadius of Annulus for sun hold Mode.

Input:
	IniVars:		IniVariable_type: Initial Variables from INI file.
Output:	
    InnerRadius:    Array[NUM_ST] of double. (radians)
	OuterRadius:    Array[NUM_ST} of double. (radians).
*/

void GetSunHoldAnnulus( IniVariable_type *IniVars,
						double InnerRadius[], double OuterRadius[])
{
	VECTOR_type FOVCornerST[4];
	VECTOR_type SunST[NUM_ST];

	Initialization(IniVars); 
	STFOV_CornerPos_STFrame(FOVCornerST);
	GetSunSensorLOSInSTFrame(&SunSensorLOSBdy, DCM0_BDY2ST, SunST);
	CalRadiusAnnulusSTFOV(SunST, FOVCornerST, MAX_SUN_NONPRIMARY_ERROR,
						  InnerRadius, OuterRadius);
}

/* ----------------------- 
	LogInitialVars();
*/	
void LogInitialVars(void)
{
	WriteVector(SunECI.Vec, "Sun Vector in ECI");
	WriteVector(SunSensorLOSBdy.Vec, "Sun Sensor LOS in Body Frame");
	WriteVector(SunStruc.Velocity.Vec, "Sun Velocity in ECI(m/sec)");
	WriteDouble(SunStruc.StayoutRadius*RTD,"Sun Stayout Radius(Degree)");
	WriteQuaternion(&Quat0_ECI2BDY, "Quat0_ECI2BDY");
	WriteQuaternion(&Quat0_BDY2ECI, "Quat0_BDY2ECI");
	WriteQuaternion(&Quat0_SS2BDY, "Quat0_SS2BDY");
	WriteQuaternion(&Quat0_BDY2SS, "Quat0_BDY2SS");
	WriteQuaternionNUM_ST(Quat0_BDY2ST, "Quat0_BDY2ST");
	WriteQuaternionNUM_ST(Quat0_ST2BDY, "Quat0_ST2BDY");
	WriteMatrixNUM_ST(DCM0_BDY2ST, "DCM0_BDY2ST");
	WriteMatrixNUM_ST(DCM0_ST2BDY, "DCM0_ST2BDY");
	WriteMatrix(&DCM0_ECI2BDY, "DCM0_ECI2BDY");
	WriteMatrix(&DCM0_BDY2ECI, "DCM0_BDY2ECI");
	WriteMatrix(&DCM0_SS2BDY, "DCM0_SS2BDY");
	WriteMatrix(&DCM0_BDY2SS, "DCM0_BDY2SS");

	WriteDouble(MAX_SUN_POINTING_ERROR*RTD, "MAX_SUN_POINTING_ERROR"); 
	WriteDouble(MAX_ROUGH_KNOWLEDGE_ERROR*RTD, "MAX_ROUGH_KNOWLEDGE_ERROR");  

    WriteInt(PWR_Config, "POWER_ON_CONFIG(PWR12=1,PWR23=2,PWR13=3,PWR123=4)");
    WriteInt(GoodAlign, "GOOD_ALIGN_FLAG(TRUE=1, FALSE=0)");

	WriteInt(PRIMARY_ST_LOOSE_THRESHOLD, "PRIMARY_ST_LOOSE_THRESHOLD");     
	WriteInt(TOTAL_ST_LOOSE_THRESHOLD, "TOTAL_ST_LOOSE_THRESHOLD");   
	WriteInt(PRIMARY_ST_TIGHT_THRESHOLD, "PRIMARY_ST_TIGHT_THRESHOLD");   
	WriteInt(TOTAL_ST_TIGHT_THRESHOLD, "TOTAL_ST_TIGHT_THRESHOLD");   
	WriteInt(PRIMARY_ST_XTIGHT_THRESHOLD, "PRIMARY_ST_XTIGHT_THRESHOLD");    

	WriteDouble(MAX_ST_MISALIGNMENT_ERROR*RTD, "MAX_ST_MISALIGNMENT_ERROR");  
	WriteDouble(MAX_SEPARAT_PRIMARY_TOL*RTD, "MAX_SEPARAT_PRIMARY_TOL");  
	WriteDouble(PARALLEL_ERROR_THRESHOLD*RTD, "PARALLEL_ERROR_THRESHOLD");  
	WriteDouble(MAG_SATURATION, "MAG_SATURATION");  
	WriteDouble(MAGNITUDE_BIAS, "MAGNITUDE_BIAS"); 
	WriteDouble(SMS_FOV*RTD, "SMS_FOV");
	WriteDouble(ATTITUDE_UNCERTAINTY*RTD, "ATTITUDE_UNCERTAINTY");
	WriteDouble(OVERLAP_PIXELS, "OVERLAP_PIXELS");
	WriteDouble(SPREAD_LIMIT*RTD, "SPREAD_LIMIT");
	WriteDouble(MAX_SEPARAT_NONPRIMARY_TOL*RTD, "MAX_SEPARAT_NONPRIMARY_TOL");	
	WriteDouble(MAX_SUN_PRIMARY_ERROR*RTD, "MAX_SUN_PRIMARY_ERROR");	
	WriteDouble(MAX_SUN_NONPRIMARY_ERROR*RTD, "MAX_SUN_NONPRIMARY_ERROR");	
	WriteDouble(MAX_ROUGH_PRIMARY_ERROR*RTD, "MAX_ROUGH_PRIMARY_ERROR");		
}

/*--------------
ReadRawVTs(VTBuffer, RawVTs) 
	Read and
	Convert H, V Position of VTs from Degrees to radians
*/

void ReadRawVTs(VTBuffer_type VTBuffer[][NUM_VT], 
				TrackerOutputVT_type RawVTs[])
{
	int ST_indx, VT_indx, VTs;

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		VTs = 0;
		for (VT_indx=0; VT_indx<NUM_VT; VT_indx++)
		{
			if (VTBuffer[ST_indx][VT_indx].UserSel == 1)
			{
				RawVTs[ST_indx].VTs[VTs].HPos = 
									VTBuffer[ST_indx][VT_indx].HPos*DTR;
				RawVTs[ST_indx].VTs[VTs].VPos = 
									VTBuffer[ST_indx][VT_indx].VPos*DTR;
				RawVTs[ST_indx].VTs[VTs].Magnitude = 
						VTBuffer[ST_indx][VT_indx].Magnitude + MAGNITUDE_BIAS;	
				RawVTs[ST_indx].VTs[VTs].VT_ID = VT_indx+1;
				VTs++;
			}
		}

		RawVTs[ST_indx].ST_ID = ST_indx+1;
		RawVTs[ST_indx].NumVTs = VTs;
	}
}

/* -------------------------------
OutputIDStars:	
*/

void OutputIDStars(IDStarOutput_type IDStars[], CISOutput_type *CISOutput)
{
	int i, j, ST_indx, VT_indx;

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	for (VT_indx=0; VT_indx<NUM_VT; VT_indx++)
	{
		CISOutput->IdentifiedStars[ST_indx].Stars[VT_indx].BSS_ID = 0;
		CISOutput->IdentifiedStars[ST_indx].Stars[VT_indx].OSC_ID = 0;
	}

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	for (i=0; i<NUM_ST; i++)
	{
		if ( ST_indx+1 == IDStars[i].ST_ID )
		for (VT_indx=0; VT_indx<NUM_VT; VT_indx++)
		for (j=0; j<IDStars[i].NumStars; j++)
		{
			if ( VT_indx+1 == IDStars[i].Stars[j].VT_ID )  
			{
				CISOutput->IdentifiedStars[ST_indx].Stars[VT_indx].BSS_ID =
														IDStars[i].Stars[j].BSS_ID;
				CISOutput->IdentifiedStars[ST_indx].Stars[VT_indx].OSC_ID =
														IDStars[i].Stars[j].OSC_ID;
			}
		}
	}
}

/* -------------------------------
OutputRecommendTrack:	
*/

void OutputRecommendTrack(VTBuffer_type VTBuff[][NUM_VT],
						  IDStarOutput_type IDStars[], 
						  RecommendVT_type RecommendTrack[],
						  CISOutput_type *CISOutput)
{
	int i, j, ST_indx, VT_indx;

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	for (VT_indx=0; VT_indx<NUM_VT; VT_indx++)
	{
		CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].HPos = 
										VTBuff[ST_indx][VT_indx].HPos;
		CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].VPos = 
										VTBuff[ST_indx][VT_indx].VPos;
		CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].Magnitude = 
										VTBuff[ST_indx][VT_indx].Magnitude;
		CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].TrackAction = NO_POSN; 
	}

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	for (i=0; i<NUM_ST; i++)
	{
		if ( ST_indx+1 == IDStars[i].ST_ID )
		for (VT_indx=0; VT_indx<NUM_VT; VT_indx++)
		for (j=0; j<IDStars[i].NumStars; j++)
		{
			if ( ( IDStars[i].Stars[j].VT_ID == VT_indx+1) &&
				 ( IDStars[i].Stars[j].OSC_ID > 0 ) )  
				CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].TrackAction = OLD_POSN;
		}
	}

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	for (i=0; i<NUM_ST; i++)
	{
		if ( ST_indx+1 == RecommendTrack[i].ST_ID )
		for (VT_indx=0; VT_indx<NUM_VT; VT_indx++)
		for (j=0; j<RecommendTrack[i].NumVTs; j++)
		{
			if ( RecommendTrack[i].Stars[j].VT_ID == VT_indx+1) 
			{
				CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].HPos = 
											RecommendTrack[i].Stars[j].HPos*RTD;
				CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].VPos = 
											RecommendTrack[i].Stars[j].VPos*RTD;
				CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].Magnitude = 
											RecommendTrack[i].Stars[j].Magnitude;
				CISOutput->RecommendTrack[ST_indx].VTs[VT_indx].TrackAction = NEW_POSN;
			}
		}
	}

}

/* -------------------------------
CISEXE: CIS Algorithm Main Routine. 
Input:
	AcqStarCatalog:	AcqSC_type. Acquisition Star Catalog(ASC).
	IniVariables:	iniVariable_type. Initial Variables from GSS.INI.
	VTBuffer:		Array[NUM_ST][NUM_VT] of VTBuffer_type. 
					Raw VT data Buffer.
	CIS_MODE:		int. User selected CIS MODE.
	STCalibON:		int. Star Tracker Alignment Calibration ON flag. 
					Set by User.
	SSCalibON:		int. Sun Sensor Alignment Calibration ON flag. 
					Set by User.

Output:

	DCM_ECI2BDY:	DirCosMatrix_type. S/C Attitude determined by CIS.
	DCM_ST2BDY:		Array[NUM_ST] of DirCosMatrix_type. Star Tracker 
					Alignment Matrices determined by CIS.
	DCM_SS2BDY:		DirCosMatrix_type. Sun Sensor Alignment matrix determined
					by CIS.
	CISOutput:		CISOutput_type.
*/

int CISEXE(AcqSC_type *AcqStarCatalog,
		   char *LogFileName,
		   IniVariable_type	*IniVariables,
		   VTBuffer_type VTBuffer[][NUM_VT],
		   int CIS_MODE,
		   int STCalibON,
 		   int SSCalibON,
		   CISOutput_type *CISOutput)
{
	int stat = 0;
	int stat1 = 0 ;
	int i;
	int TotalStarsST, TotalIDStarsST, TotalRevIDStarsST;
	int PrimST_indx, PrimaryST_ID, ST_indx;
	int ChangePrimaryST = FALSE;
	int ChangePrimaryStar = FALSE;
	int ChangePrimaryCandidate = FALSE;
    int Min_Indx, Max_Indx, IDCandidate, PrimaryCandidate;
	int IDContradictory[NUM_ST];
	int XTightID[NUM_ST], ValidRef[NUM_ST], IDRef;
	int BestNumIDStarsOSC[NUM_ST];
	int BestNumIDStars[NUM_ST];
	int BestTotalOSC;
	int BestTotalIDStars=0;
	int RefST1_ID, RefST2_ID;

	StarSubSC_type *SubSC;
	SubSC_type SubStarCatalog[NUM_ST];

	TrackerOutputVT_type RawVTs[NUM_ST];
    TrackerOutputST_type TrackedStarST[NUM_ST];
	TrackerOutputST_type TmpStarST[NUM_ST];

	QUATERNION_type  Quat0_ECI2ST[NUM_ST];
	QUATERNION_type  Quat0_ST2ECI[NUM_ST];
    QUATERNION_type  PrimaryEstimateQuat_ECI2ST;
    QUATERNION_type  RefineQuat_ECI2ST;
  	QUATERNION_type  DeltaQuat_ECI2ST;

	QUATERNION_type	 BestQuat_ECI2ST[NUM_ST]; 
    QUATERNION_type  Quat_ST2BDY[NUM_ST];
    QUATERNION_type  Quat_BDY2ST[NUM_ST];
    QUATERNION_type  Quat_ECI2BDY;
    QUATERNION_type  Quat_BDY2ECI;
    QUATERNION_type  Quat_SS2BDY;
    QUATERNION_type  Quat_BDY2SS;

	DirCosMatrix_type DCM0_ECI2ST[NUM_ST];
	DirCosMatrix_type DCM0_ST2ECI[NUM_ST];
    DirCosMatrix_type PrimaryEstimateDCM_ECI2ST;
    DirCosMatrix_type RefineDCM_ECI2ST; 
    DirCosMatrix_type CurrDCM_ST2ECI;

	DirCosMatrix_type BestDCM_ECI2ST[NUM_ST];
    DirCosMatrix_type DCM_ST2BDY[NUM_ST];
    DirCosMatrix_type DCM_BDY2ST[NUM_ST];
    DirCosMatrix_type DCM_ECI2BDY;
    DirCosMatrix_type DCM_BDY2ECI;
    DirCosMatrix_type DCM_SS2BDY;
    DirCosMatrix_type DCM_BDY2SS;

	DirCosMatrix_type DCM_PrimaryST2ST[NUM_ST]; 

    STList_type AvailableSTList;
    STList_type PossibleRefSTList;

	VECTOR_type SunST[NUM_ST];
	VECTOR_type PrimaryStarCurrST;
    VECTOR_type FixedPtST;
    VECTOR_type FixedPtECI;
    VECTOR_type AcqError;
    VECTOR_type STBoresightECI; 
	VECTOR_type STBoresightCurrST;
	VECTOR_type STLosBDY[NUM_ST]; 
	VECTOR_type SC_Angular_VelocityBDY;

	ST_type *PrimaryStar;
    StarSubSC_type *PrimaryCandidateStar;

    IDStarOutput_type IDStars[NUM_ST]; 
    IDStarOutput_type RevIDStars[NUM_ST]; 
    IDStarOutput_type TempBestIDStars[NUM_ST]; 
    IDStarOutput_type BestIDStars[NUM_ST]; 

	StatisticsVT_type  PosStatistics[NUM_ST]; 
	StatisticsVT_type  BestPosStatistics[NUM_ST]; 
	
	RecommendVT_type RecommendTrack[NUM_ST];

    double SepTolerance[NUM_ST];
    double PosToleranceLoose[NUM_ST];
    double MaxAngCornerPrimaryStar[NUM_ST];
	double SunPrimaryStarAngle;
	double MaxMagRes[NUM_ST];
    double MaxRotationError;
    double AcqErrorTotal; 
	double ParallelError, PerpendicularError;
	double BestParallelErr[NUM_ST], BestPerpendicularErr[NUM_ST];
    double dummy;
    double SSBoresight_Correction, STBoresight_Correction[NUM_ST];
    double STRotationAboutBoresight_Correction[NUM_ST];
	double UpRightSlewAngle=0.0; 
	double InvertedSlewAngle=0.0; 
	double PlusSlewAngle=0.0;
	double MinusSlewAngle=0.0;

	int    Intrusion[361][MAX_EPHEM_OBJS];
	time_t t;

/* ==================================================================== */

	if ( (LogFp = fopen(LogFileName, "a")) == NULL )
	{
		stat = FILE_ERROR;
		DisplayMessage(stat, ERROR_TEXT);	
		fclose(LogFp);
		CISOutput->Status = stat;     
		return(stat);
	}

	if ( (CIS_MODE != SUN_HOLD_MODE) && (CIS_MODE != ROUGH_KNOWLEDGE_MODE) )
	{
		stat = MODE_ERROR;
		DisplayMessage(stat, ERROR_TEXT);
		LogMessage(stat, ERROR_TEXT);
		fclose(LogFp);
		CISOutput->Status = stat;     
		return(stat);
	}

	/* ------------------------------
		Initialization 
	--------------------------------- */

	Initialization(IniVariables);

	/* Set CIS Output variables to Initial Input value */
	memcpy(&Quat_ECI2BDY, &Quat0_ECI2BDY, sizeof(QUATERNION_type));
	memcpy(&Quat_BDY2ECI, &Quat0_BDY2ECI, sizeof(QUATERNION_type));
	memcpy(&Quat_SS2BDY, &Quat0_SS2BDY, sizeof(QUATERNION_type)); 
	memcpy(&Quat_BDY2SS, &Quat0_BDY2SS, sizeof(QUATERNION_type)); 
	memcpy(&Quat_BDY2ST, &Quat0_BDY2ST, NUM_ST*sizeof(QUATERNION_type)); 
	memcpy(&Quat_ST2BDY, &Quat0_ST2BDY, NUM_ST*sizeof(QUATERNION_type)); 
	memcpy(&DCM_BDY2ST, &DCM0_BDY2ST, NUM_ST*sizeof(DirCosMatrix_type)); 
	memcpy(&DCM_ST2BDY, &DCM0_ST2BDY, NUM_ST*sizeof(DirCosMatrix_type)); 
	memcpy(&DCM_ECI2BDY, &DCM0_ECI2BDY, sizeof(DirCosMatrix_type)); 
	memcpy(&DCM_BDY2ECI, &DCM0_BDY2ECI, sizeof(DirCosMatrix_type)); 
	memcpy(&DCM_SS2BDY, &DCM0_SS2BDY, sizeof(DirCosMatrix_type));  
	memcpy(&DCM_BDY2SS, &DCM0_BDY2SS, sizeof(DirCosMatrix_type));  

	BestTotalOSC = 0;
	BestTotalIDStars = 0;
	RefST1_ID = 0;
	RefST2_ID = 0;

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		IDStars[ST_indx].NumStars = 0;
		RevIDStars[ST_indx].NumStars = 0;
		RevIDStars[ST_indx].ST_ID = ST_indx+1;
		TempBestIDStars[ST_indx].NumStars = 0;
		TempBestIDStars[ST_indx].ST_ID = ST_indx+1;
		BestIDStars[ST_indx].NumStars = 0;
		BestIDStars[ST_indx].ST_ID = ST_indx+1;
		RecommendTrack[ST_indx].NumVTs = 0;
		BestNumIDStarsOSC[ST_indx] = 0;
	 	BestNumIDStars[ST_indx] = 0;

		BestParallelErr[ST_indx] = -1e-20;
		BestPerpendicularErr[ST_indx] = -1e-20;

		IDContradictory[ST_indx] = FALSE;
		XTightID[ST_indx] = FALSE;
		ValidRef[ST_indx] = FALSE;
	}
	for (i=0; i<3; i++)
		SC_Angular_VelocityBDY.Vec[i] = 0.0;
	
	/* Read rawVTs */

	ReadRawVTs(VTBuffer, RawVTs);

	/*------------------------- 
		Log CIS Input Variables 
	--------------------------- */	
	time(&t);
	fprintf(LogFp, 
	"#####################################################################\n");
	fprintf(LogFp,	"#			CIS Log File\n");
	fprintf(LogFp,	"#			CIS Start at %s", ctime(&t));	
	fprintf(LogFp,
	"#####################################################################\n\n");
	WriteInt(CIS_MODE, "CIS_MODE(SUN_HOLD_MODE=1, ROUGH_KNOWLEDGE_MODE=0)");
	WriteInt(STCalibON, "STCalibON");
	WriteInt(SSCalibON, "SSCalibON");
	WriteRawVTs(RawVTs, "Raw VTs Data From TLM");
	LogInitialVars();

	/* ----------------------------
		Create Sub-Star Catalog 
	------------------------------- */ 
	
	SubSC = (StarSubSC_type *)calloc(NUM_ST*MAXNUM_SUBSC_STARS, 
											sizeof(StarSubSC_type) );
	{
		if ( SubSC == NULL )
		{
			stat = MEM_ERROR;
			DisplayMessage(stat, ERROR_TEXT);	
			LogMessage(stat, ERROR_TEXT);

			free(SubSC);
			fclose(LogFp);
			CISOutput->Status = stat;     
			return(stat);
		}
	}

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		for (i=0; i<MAXNUM_SUBSC_STARS; i++) 
			SubStarCatalog[ST_indx].Stars[i] = 
						&(SubSC[ST_indx*MAXNUM_SUBSC_STARS+i]);

	switch(CIS_MODE)
	{
		case	SUN_HOLD_MODE:

				GetSunSensorLOSInSTFrame(&SunSensorLOSBdy, DCM0_BDY2ST, SunST);
				GenSunPointingSubCatalog(AcqStarCatalog, 
										 &SunECI,
										 SunST,
										 MAX_SUN_NONPRIMARY_ERROR,
                              	    	 SubStarCatalog);
				break;

		case	ROUGH_KNOWLEDGE_MODE:	

				GetTransformECI2ST(&Quat0_ECI2BDY, Quat0_BDY2ST,
                       			   Quat0_ST2ECI, Quat0_ECI2ST,
                       			   DCM0_ST2ECI, DCM0_ECI2ST);

				GenRoughKnowledgeSubCatalog(AcqStarCatalog,
                                 			DCM0_ECI2ST,
                                 			&STBoresight,
										 	MAX_ROUGH_PRIMARY_ERROR,	
                                  			SubStarCatalog);
				break;

		default:
				break;
	}

	/* ------------------------------
		Search Max. Magnitude Residual for each SubStarCatalog 
	--------------------------------- */  

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		MaxMagRes[ST_indx] = GetMaxMagnitudeResidue(&(SubStarCatalog[ST_indx]));

	WriteDoubleNUM_ST(MaxMagRes, "\nMaxMagRes");

/*========================================================== 
	 Start CIS Algorithm   
============================================================ */
	
	GenAvailableSTList(RawVTs, TrackedStarST,
						  &AvailableSTList, &PossibleRefSTList); 
	TotalStarsST = TotalSTStars(TrackedStarST); 

	WriteSTList(&AvailableSTList, "AvailableSTListe");
	WriteSTList(&PossibleRefSTList, "PossibleRefSTList");
	WriteInt(TotalStarsST, "TotalStarsST");

	if ( TotalStarsST < TOTAL_ST_TIGHT_THRESHOLD ) 
	{
		stat = NO_ENOUGH_STARS; 
	/* -----------------------------------------
		DisplayMessage(stat, ERROR_TEXT);
	-------------------------------------------- */
		LogMessage(stat, ERROR_TEXT);
		goto Quit;
	}
	else if ( PossibleRefSTList.NumST == 0 ) 
	{
		stat = NO_REFERENCE_ST;
	/* -----------------------------------------
		DisplayMessage(stat, ERROR_TEXT);
	-------------------------------------------- */
		LogMessage(stat, ERROR_TEXT);
		goto Quit;
	}

	/* -----------------------------------------
		Cycle through PossibleRefSTs as PrimarySTs to make Acquisition
	-------------------------------------------- */

	ChangePrimaryST = TRUE;
	PrimST_indx = 0;

	while ( ChangePrimaryST )
	{
		ChangePrimaryST = FALSE;
		PrimaryST_ID = PossibleRefSTList.ST_ID[PrimST_indx];
		WriteInt(PrimaryST_ID, "\n\nPrimaryST_ID");

		IDentifyPrimaryStarTracker( PrimaryST_ID,
                                 	TrackedStarST,
                                	DCM0_ST2BDY,
                                	TmpStarST,
                                	SepTolerance,
                                	DCM_PrimaryST2ST ); 
		TotalStarsST = TotalSTStars(TmpStarST); 

		for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
			TempBestIDStars[ST_indx].NumStars = 0;

		/* -------------------------------
			Choose Primary Stars
		---------------------------------- */

		/* Attempt to establish a pattern using a "PrimaryStar".
		   If that fails, choose another primary star, and star again.
		   Cycle through possible Primary Stars in the PrimaryST, starting
		   with the brightest stars.    */ 

		ChangePrimaryStar = TRUE;
		while ( ChangePrimaryStar )	
		{
			WriteInt(ChangePrimaryStar, "ChangePrimaryStar");
			ChangePrimaryStar = FALSE;
			PrimaryStar = &(TmpStarST[PrimaryST_ID-1].Stars[0]);
			
			IDentifyPrimaryStar( &(SubStarCatalog[PrimaryST_ID-1]),
                         		 PrimaryStar,
                         		 DCM_PrimaryST2ST,
                         		 &(SunST[PrimaryST_ID-1]),
                         		 MaxMagRes[PrimaryST_ID-1],
                         		 MaxAngCornerPrimaryStar,
                         		 &SunPrimaryStarAngle,
                         		 &Min_Indx, &Max_Indx);
				
			/* --------------------------
				Choose Primary Candidate Stars
			----------------------------- */
			/* Cycle through Primary Candidate stars in the catalog which
				index is between Min_Indx and Max_Indx(whith in allowable 
				Magnitude range) */

			ChangePrimaryCandidate = TRUE;
			PrimaryCandidate = Min_Indx;
			
			while ( ChangePrimaryCandidate )
			{

				ChangePrimaryCandidate = FALSE;
				PrimaryCandidateStar = SubStarCatalog[PrimaryST_ID-1].
											 Stars[PrimaryCandidate];	
				IDCandidate =  IDentifyPrimaryCandidate(
										CIS_MODE,
                             		    PrimaryStar,
                             			PrimaryCandidateStar,
                             			&SunECI,
                             			&(DCM0_ECI2ST[PrimaryST_ID-1]),
                             			SunPrimaryStarAngle,
                             			&FixedPtST,
                             			&FixedPtECI,
                             			&MaxRotationError); 
				/*----------------------------- 
					Make Primary Assumption 
				---------------------------- */
				
				if ( IDCandidate )
				{
					/* Make Primary Assumption. Define (first iteration of)
						estimate of attitude, with the assuption that the
						primary star is the Identified primary canidate star.*/

					switch ( CIS_MODE )
					{
						case	SUN_HOLD_MODE:

								PrimaryAttitudeEstimate( 
										&SunECI,
                             			&(SunST[PrimaryST_ID-1]),
                             			PrimaryCandidateStar->ECI,
                             			PrimaryStar->STFrame,
                             			&PrimaryEstimateQuat_ECI2ST,
                             			&PrimaryEstimateDCM_ECI2ST);
								break;

						case	ROUGH_KNOWLEDGE_MODE:

								PrimaryAttitudeEstimate( 
										&FixedPtECI,
                             			&FixedPtST,
                             			PrimaryCandidateStar->ECI,
                             			PrimaryStar->STFrame,
                             			&PrimaryEstimateQuat_ECI2ST,
                             			&PrimaryEstimateDCM_ECI2ST);
								break;

						default: 
								break;
		
					} /* switch ( CIS_MODE ) */

					/*---------------------- 
						Loose Direct Match 
					----------------------- */

					/* Calculate the position error(tolaerance) for Identify
						stars(loose match) */
 
					GetPositionToleranceLoose( MaxAngCornerPrimaryStar,
                                			   SepTolerance,
                             				   MaxRotationError, 
                                			   PosToleranceLoose); 

					/* Perform "Loose Direct Match" */
					for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
					{
						if (TmpStarST[ST_indx].NumStars > 0)
						{
 							CalParamDirectMatch( 
									&PrimaryEstimateDCM_ECI2ST,
                               		&(DCM_PrimaryST2ST[ST_indx]),
                                    PrimaryStar->STFrame,
                               		&CurrDCM_ST2ECI,
                               		&PrimaryStarCurrST ); 

							IDentifyStars( &(SubStarCatalog[ST_indx]),
                  						   &(TmpStarST[ST_indx]), 
										   &CurrDCM_ST2ECI, 
                   						   &PrimaryStarCurrST,
                   						   PrimaryCandidateStar->ECI,
                    				   	   MaxMagRes[ST_indx],
                   						   PosToleranceLoose[ST_indx],
                   						   SepTolerance[ST_indx],
                   						   &(IDStars[ST_indx]) ); 

						} /*  if (TmpStarST[ST_indx].NumStars > 0) */ 
					} /* for (ST_indx..) */

					/* -------------------------
						Loose Direct Match Check 
					---------------------------- */

					TotalIDStarsST = TotalIDStars(IDStars);
					WriteInt( PrimaryCandidate, "PrimaryCandidate");
					WriteInt(TotalIDStarsST, "TotalIDStarsLoose");
					WriteInt(IDStars[PrimaryST_ID-1].NumStars,
										"NumIDStar_PrimarySTLoose");

					if ( (TotalIDStarsST >= TOTAL_ST_LOOSE_THRESHOLD) &&
						 (IDStars[PrimaryST_ID-1].NumStars 
										>= PRIMARY_ST_LOOSE_THRESHOLD) )
					{
						WriteIDStars(IDStars, "\nLooseMatchIDStars");
						WriteQuaternion(&PrimaryEstimateQuat_ECI2ST, 
										 "PrimaryEstimateQuat_ECI2ST");
					/* ---------------------------
						Refine Estimate Attitude 
					------------------------------ */

					/* Pass Loose Direct Match check and Re-Calculate estimate
						Attitude using the identified stars from only from
						PrimaryST. This will later allow us to estimate 
						misalignment of the other trackers, with respect
						to PrimaryST */ 

						RefineAttitudeEstimate( 
										&(IDStars[PrimaryST_ID-1]),
                            			&(SubStarCatalog[PrimaryST_ID-1]),
                            			&RefineQuat_ECI2ST,
                            			&RefineDCM_ECI2ST);
						WriteQuaternion(&RefineQuat_ECI2ST, "RefineQuat_ECI2ST");

						/* Measurement puropose only */
						DiffBetEstimatedAttitude(&PrimaryEstimateQuat_ECI2ST,
                              					 &RefineQuat_ECI2ST,
                             					 &DeltaQuat_ECI2ST,
                              					 &AcqError,
                             					 &AcqErrorTotal);

						/* ----------------------------
							Tight Direct Match 
						-------------------------------  */

						/* Calculate ST boresight in ECI using Refined Attitude
							for Tight Direct Match */
						CalSTBoresightECI(&STBoresight, &RefineDCM_ECI2ST,
                       					  &STBoresightECI); 

						/* Perform TIGHT Direct Match */
						for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
						{
							if (TrackedStarST[ST_indx].NumStars > 0)
							{
 								CalParamDirectMatch( &RefineDCM_ECI2ST,
                               				&(DCM_PrimaryST2ST[ST_indx]),
                                    		STBoresight.Vec,
                               				&CurrDCM_ST2ECI,
                               				&STBoresightCurrST ); 

								IDentifyStars( &(SubStarCatalog[ST_indx]),
                  						   &(TrackedStarST[ST_indx]), 
										   &CurrDCM_ST2ECI, 
                   						   &STBoresightCurrST,
                   						   STBoresightECI.Vec,
                    				   	   MaxMagRes[ST_indx],
                   						   SepTolerance[ST_indx],
                   						   SepTolerance[ST_indx],
                   						   &(RevIDStars[ST_indx]) ); 

							/*------------------------------  
								Average Star Match error for Primary ST 
							------------------------------- */

								if ( (ST_indx == PrimaryST_ID-1) &&
									 (RevIDStars[ST_indx].NumStars > 0) )
								{
									IDStarPosStatistics(&(RevIDStars[ST_indx]),
											   		&(PosStatistics[ST_indx]));
									ParallelError = CalParallelError(
												PosStatistics[ST_indx].Spread);
									PerpendicularError = CalPerpendicularError(
													&(SubStarCatalog[ST_indx]),
								 				    &(RevIDStars[ST_indx]),
												  	&RefineDCM_ECI2ST ); 
								}/* if ( (ST_indx == PrimaryST_ID-1) && .. */
							} /* if (TrackedStars[ST_indx].NumStars > 0) */
						} /* for (ST_indx=0;..) */

						/* -------------------------------
							Tight Direct March Check
						---------------------------------- */ 

						TotalRevIDStarsST = TotalIDStars(RevIDStars);
						WriteInt(TotalRevIDStarsST, "TotalRevIDStars");
						WriteInt(RevIDStars[0].NumStars, "NumRevIDStar_ST1");
						WriteInt(RevIDStars[1].NumStars, "NumRevIDStar_ST2");
						WriteInt(RevIDStars[2].NumStars, "NumRevIDStar_ST3");

						if ( (TotalRevIDStarsST >= TOTAL_ST_TIGHT_THRESHOLD) &&
							 (RevIDStars[PrimaryST_ID-1].NumStars >=
							  				   PRIMARY_ST_TIGHT_THRESHOLD) &&
							 (ParallelError <= PARALLEL_ERROR_THRESHOLD)   &&
							 (RevIDStars[PrimaryST_ID-1].NumStars >= 
							  		 TempBestIDStars[PrimaryST_ID-1].NumStars) &&
							 ( (PerpendicularError < 
										BestPerpendicularErr[PrimaryST_ID-1]) 
							 	|| (BestPerpendicularErr[PrimaryST_ID-1] < 0) 
								|| (RevIDStars[PrimaryST_ID-1].NumStars >
									TempBestIDStars[PrimaryST_ID-1].NumStars) ) )
						{
							/* Pass Tight Direct Check */

							WriteInt(RevIDStars[PrimaryST_ID-1].NumStars,
											"NumIDStar_PrimarySTTight");
							/* -------------------------
								Identify Cantradictory Estimate
							---------------------------- */

							if ( BestPerpendicularErr[PrimaryST_ID-1] >= 0 )
								IDContradictory[PrimaryST_ID-1] = 
									IdentifyContradictory( 
											&(BestQuat_ECI2ST[PrimaryST_ID-1]),
										   				   &RefineQuat_ECI2ST );
								
							/* ------------------------
								Extra Tight Check 
							--------------------------- */

							/* Move on to the next tracker at the end of
							   the loop, if the extra-tight threshold has
							   been satisfied. Ignore a contradiction in 
							   attitude, if this is the case. */
							
							if (RevIDStars[PrimaryST_ID-1].NumStars >=
												PRIMARY_ST_XTIGHT_THRESHOLD)
							{
								XTightID[PrimaryST_ID-1] = TRUE;
								IDContradictory[PrimaryST_ID-1] = FALSE;
							}

							/* -----------------------------------------
								Create Best Estimate List
							-------------------------------------------- */

							if ( IDContradictory[PrimaryST_ID-1] )
							{
								ValidRef[PrimaryST_ID-1] = FALSE;
								WriteInt(PrimaryST_ID, 
								"*** Contradictory Attitude!! PrimaryST_ID");
							}
							else
							{
								ValidRef[PrimaryST_ID-1] = TRUE;
								BestPerpendicularErr[PrimaryST_ID-1] = 
														PerpendicularError;
								BestParallelErr[PrimaryST_ID-1] = ParallelError;
								memcpy(&(BestPosStatistics[PrimaryST_ID-1]),
									   &(PosStatistics[PrimaryST_ID-1]),
									   sizeof(StatisticsVT_type) );
								memcpy(&(TempBestIDStars), &(RevIDStars),
										NUM_ST*sizeof(IDStarOutput_type));
								memcpy(&(BestQuat_ECI2ST[PrimaryST_ID-1]),
									   &RefineQuat_ECI2ST, 
									   sizeof(QUATERNION_type)); 
								memcpy(&(BestDCM_ECI2ST[PrimaryST_ID-1]),
									   &RefineDCM_ECI2ST,
									   sizeof(DirCosMatrix_type)); 
							} /* else .. */ 

						} /* if (..) Tight Direct Match Check */

					} /* if (..) Loose Direct Match Check */		

				} /* if ( IDCandidate ) */
				
				/* Done with the current Candidate. Move on to the next one
					if one exists, and if have not made an extra-Tight 
					Identification. Otherwise, move on to the next Primary
					Star, if any */

				/* --------------------------------------
					Next Primary Candidate?
				----------------------------------------- */

				if ( (PrimaryCandidate < Max_Indx) &&
					 (!XTightID[PrimaryST_ID-1]) )
				{
					/* More Primary Candidate are available */

					ChangePrimaryCandidate = TRUE;
					PrimaryCandidate++;
				}
				else /* No more Candidate for this Primary Star */ 
				{
				/* All the Primary Candidates for this Primary Star have
					been examined. The search for another Primary Star
					continues. Eliminate the just-examined Primary Star
					from the TmpStarST List. */

					/* ------------------------------
						Next Primary Star?
					--------------------------------- */
					TotalStarsST--;
					
					/* Determine whether there are still enough stars to 
						allow verification of a candidate attitude 
						determination. */ 	
					if ( TotalStarsST >= TOTAL_ST_TIGHT_THRESHOLD )
					{ 
						ElimnateCurPrimaryStar( &(TmpStarST[PrimaryST_ID-1])); 
						
						/* Verify that the Current Primary ST is still
							has enough stars to be a Primary ST, and the
							XTight threshold has not been met yet. If 
							either those is not true, move on to the next
							Possible Primaty Tracker, if any. */	
						
						if ( (TmpStarST[PrimaryST_ID-1].NumStars >=
										PRIMARY_ST_TIGHT_THRESHOLD) &&
							 			(!XTightID[PrimaryST_ID-1]) )
						{
							ChangePrimaryStar = TRUE;	
						}
						else /* no more Primary Stars for this Primary ST */
						{
							/*----------------------------------
								Next Primary ST?
							---------------------------------- */

							/* All the Primary Stars in this ST have been 
							   examined, or we have decided to skip the
							   remaining Primary Stars. Continue the 
							   search for next Primary ST if calibration ON, 
							   or the current ST is NOT a valid Reference ST */

							if ((STCalibON) || (!ValidRef[PrimaryST_ID-1]))
							{
								if ( (PrimST_indx+1) < PossibleRefSTList.NumST)
								{
									PrimST_indx++;
									ChangePrimaryST = TRUE;
								}
							}
						} /* else no more Primary Stars for this Primary ST */
					} /* if ( TotalStarsST >= TOTAL_ST_TIGHT_THRESHOLD ) */	
				} /*else  No more Candidate for this Primary Star */
			} /* while ( ChangePrimaryCandidate ) */
		} /* while ( ChangePrimaryStars ) */  

		/* ----------------------------------
			Store TempBestIDStars to BestIDStars List.

			If tracker ST_indx is a PrimaryST, then store 
			the stars ID'd using it. Furthermore, if stars
			tracked by anothere tracker are ID'd, store 
			that information unless: 

			1) The ST_indx tracker is a valid reference in
				its own right; or

			2) More stars are ID'd for ST_indx using 
				another valid reference tracker.
		----------------------------------------------*/
		for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		{
			if ( ( (ST_indx+1 == PrimaryST_ID) && (ValidRef[ST_indx]) ) ||
				 ( (!ValidRef[ST_indx]) &&
				   (TempBestIDStars[ST_indx].NumStars > BestIDStars[ST_indx].NumStars) ) )

				memcpy(&(BestIDStars[ST_indx]), &(TempBestIDStars[ST_indx]),
		   				sizeof(IDStarOutput_type));
		}
		BestTotalIDStars = TotalIDStars(BestIDStars);	
		CalNumIDStarsOSC(BestIDStars, BestNumIDStarsOSC, &BestTotalOSC); 
		GetNumIDStars(BestIDStars, BestNumIDStars);

	} /* while (ChangePrimaryST) */ 

	/* -------------------------------------------------------
		Acquisition Complete, Prepare for Terminate    	
	---------------------------------------------------------- */
	
	/* Any Valid Reference ST has been Identified? (Default Ref First) */
	
	IDRef = 0;
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		if ( ( (ST_indx+1 == DefaultRefST1_ID) || 
			   (ST_indx+1 == DefaultRefST2_ID) ) &&
			    ValidRef[ST_indx] && (RefST1_ID == 0) )
		{
			IDRef++;
			RefST1_ID = ST_indx+1;
		}

	if ( IDRef == 0 )
	{
		for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
			if ( ( (ST_indx+1 != DefaultRefST1_ID) &&
				   (ST_indx+1 != DefaultRefST2_ID) ) &&
				   ValidRef[ST_indx] && (RefST1_ID == 0) )
			{
				IDRef++ ;
				RefST1_ID = ST_indx+1;
			}
	}

	if ( IDRef > 0)
	{
		/* ----------------------------------------
		 	Calibrate Star Trackers Alignment 
		------------------------------------------- */
		if ( STCalibON )
		{
			SetReferenceTrackers(BestNumIDStars, ValidRef, BestParallelErr, 
                                 DefaultRefST1_ID, DefaultRefST2_ID, &RefST1_ID, &RefST2_ID); 
			
			if (RefST2_ID > 0) 
            {
              IDRef++;

			  GetSTLosBDY( &STBoresight, DCM0_ST2BDY, STLosBDY ); 

			  StarTrackerAlignment(RefST1_ID, RefST2_ID, ValidRef, &STBoresight,
                          		   STLosBDY, BestDCM_ECI2ST,
                          		   Quat_ST2BDY, Quat_BDY2ST,
								   DCM_ST2BDY, DCM_BDY2ST); 

	            /* Set Good Alignment Flag */
				switch (PWR_Config)
				{
					case PWR12:
						if ( (RefST1_ID == 1 && RefST2_ID == 2) ||
							 (RefST1_ID == 2 && RefST2_ID == 1) )
                             GoodAlign = TRUE;
						break;
					case PWR23:
						if ( (RefST1_ID == 2 && RefST2_ID == 3) ||
							 (RefST1_ID == 3 && RefST2_ID == 2) ) 
                             GoodAlign = TRUE;
						break;
					case PWR13:
						if ( (RefST1_ID == 1 && RefST2_ID == 3) ||
							 (RefST1_ID == 3 && RefST2_ID == 1) )
                             GoodAlign = TRUE;
						break;
					case PWR123:
						if ( ValidRef[0] && ValidRef[1] && ValidRef[2] )
                             GoodAlign = TRUE;
						break;
					default:
						break;
	            }
			} /* if (RefST2_ID > 0) */

             /* Determine change in estimated star tracker alignment  */
            for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)  
            {
              AlignmentEstimateChange ( DCM0_ST2BDY[ST_indx],
                                        DCM_ST2BDY[ST_indx],
                                        STBoresight,
                                        &STBoresight_Correction[ST_indx],
                                        &STRotationAboutBoresight_Correction[ST_indx]);

              STBoresight_Correction [ST_indx] *= RTA;
              STRotationAboutBoresight_Correction [ST_indx] *= RTA;
             }

		} /* endof if ( STCalibON ) */

		/* ---------------------------------------
			Calculate ECI2BDY Quaternion and DCM
		------------------------------------------ */	

		CalECI2BDY(&(BestQuat_ECI2ST[RefST1_ID-1]),
				   &(Quat_ST2BDY[RefST1_ID-1]),
				   &Quat_ECI2BDY,
				   &Quat_BDY2ECI,
				   &DCM_ECI2BDY,
				   &DCM_BDY2ECI); 

		/* ----------------------------------------
			Calibrate Sun Sensor Alignment
		------------------------------------------- */

		if ( SSCalibON )
		{
			SunSensorAlignment(&SunECI, &SSBoresight, &DCM_ECI2BDY,
                       		   &Quat_SS2BDY, &Quat_BDY2SS,
                       		   &DCM_SS2BDY, &DCM_BDY2SS); 

             /* Determine change in estimated sun sensor alignment  */

            AlignmentEstimateChange(DCM0_SS2BDY, DCM_SS2BDY,
                                    SSBoresight,
                                    &SSBoresight_Correction,
                                    &dummy);
            SSBoresight_Correction *= RTA;

		} /* endof if ( SSCalibON ) */

		/* -----------------------------------------
			Choose OSC Stars Attempted to Track
		-------------------------------------------- */
	
		ChooseOSCStarForVT(VTBuffer,
						   &STBoresight, 
                      	   BestIDStars,
                      	   BestPosStatistics, 
                      	   AcqStarCatalog,
                      	   &DCM_ECI2BDY,
						   DCM_BDY2ST,
                      	   RecommendTrack); 

	}/*  end of if ( IDRef > 0 ) */ 

	/* -------------------------------
		Acquisition Status Report 
	---------------------------------- */

	stat = AcqStatus(IDContradictory, IDRef);
	/* -------------------------------
	DisplayMessage(stat, ERROR_TEXT);
	---------------------------------- */
	LogMessage(stat, ERROR_TEXT);

	/* -------------------------------
		Calculate Recommend Slew & Sun Tracing Rate
	---------------------------------- */

	if (stat == TOTAL_SUCCESS)
	{
		Slew_ToPositionWing (  SSBoresight,
							   DCM_SS2BDY,
							   Quat_ECI2BDY,
							   &UpRightSlewAngle, 
							   &InvertedSlewAngle );

		SunTrack ( SSBoresight,
				   DCM_SS2BDY,
				   Quat_ECI2BDY,
				   SunStruc,
				   &SC_Angular_VelocityBDY);

		WriteDouble(RTD*UpRightSlewAngle, "\nRECOMMEND_UPRIGHT_SLEW_ANGLE");	
		WriteDouble(RTD*InvertedSlewAngle, "RECOMMEND_INVERTED_SLEW_ANGLE");	   
		WriteVector(SC_Angular_VelocityBDY.Vec, "SC_ANGULAR_VELOCITY_BDY(deg/sec)");
	}
	if (stat == PARTIAL_SUCCESS)
	{
		Slew_ToStarrierSkies (DefaultRefST1_ID,
							  DefaultRefST2_ID,
							  DCM_ST2BDY,
							  SSBoresight,
							  STBoresight,
							  DCM_SS2BDY,
						   	  StayOutZones,
						      Quat_ECI2BDY,
							  ATTITUDE_UNCERTAINTY,
							  AcqStarCatalog,
						  	  Intrusion,
							  &PlusSlewAngle,
							  &MinusSlewAngle);

		SunTrack ( SSBoresight,
				   DCM_SS2BDY,
				   Quat_ECI2BDY,
				   SunStruc,
				   &SC_Angular_VelocityBDY);

		WriteDouble(RTD*PlusSlewAngle, "\nRECOMMEND_PLUS_SLEW_ANGLE");
		WriteDouble(RTD*MinusSlewAngle, "RECOMMEND_MINUS_SLEW_ANGLE");
		WriteVector(SC_Angular_VelocityBDY.Vec, "SC_ANGULAR_VELOCITY_BDY(deg/sec)");
	}

	/* -------------------------------
		CIS Results Output and Log
	---------------------------------- */
	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		BestPerpendicularErr[ST_indx] *= RTA; /* radians to arc second */ ;
		BestParallelErr[ST_indx] *= RTA;
	}

Quit:
	
	/* Set Output Variables */
	memcpy( &(CISOutput->Quat_ECI2BDY), &Quat_ECI2BDY, sizeof(QUATERNION_type) );
	memcpy( CISOutput->DCM_ST2BDY, DCM_ST2BDY, NUM_ST*sizeof(DirCosMatrix_type) );
	memcpy( &(CISOutput->DCM_SS2BDY), &DCM_SS2BDY, sizeof(DirCosMatrix_type) );
	OutputRecommendTrack(VTBuffer, BestIDStars, RecommendTrack, CISOutput);
	memcpy( CISOutput->ValidRefST, ValidRef, NUM_ST*sizeof(int) );
	OutputIDStars(BestIDStars, CISOutput);
	memcpy( CISOutput->AttErrorMetric, BestPerpendicularErr, NUM_ST*sizeof(double) );
	CISOutput->UpRightSlewAngle = RTD * UpRightSlewAngle; 
	CISOutput->InvertedSlewAngle = RTD * InvertedSlewAngle; 
	CISOutput->PlusSlewAngle = RTD * PlusSlewAngle;
	CISOutput->MinusSlewAngle = RTD * MinusSlewAngle;
	memcpy( &(CISOutput->SC_Angular_VelocityBDY), &SC_Angular_VelocityBDY,
													sizeof(VECTOR_type));	
    memcpy( CISOutput->AlignBoresightErrST,  STBoresight_Correction, NUM_ST*sizeof(double) );
    memcpy( CISOutput->AlignRotateErrST,  STRotationAboutBoresight_Correction, NUM_ST*sizeof(double) );
    CISOutput->AlignBoresightErrSS = SSBoresight_Correction;
    CISOutput->GoodAlign = GoodAlign;
	CISOutput->Status = stat;     

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		CISOutput->Spread[ST_indx] = BestPosStatistics[ST_indx].Spread;

	/* Log Results */
	WriteIntNUM_ST(ValidRef, "\nVALID_REFERENCE_TRACKER"); 
    WriteInt(GoodAlign, "GOOD_ALIGN_FLAG(TRUE=1, FALS=0)");
	WriteInt(RefST1_ID, "ALIGNMENT_REF_ST1");
	WriteInt(RefST2_ID, "ALIGNMENT_REF_ST2");
	WriteInt(BestTotalIDStars, "\nBEST_TOTAL_ID_STARS"); 
	WriteIntNUM_ST(BestNumIDStars, "BEST_NUM_ID_STARS"); 
	WriteInt(BestTotalOSC, "BEST_TOTAL_ID_OSC"); 
	WriteIntNUM_ST(BestNumIDStarsOSC, "BEST_NUM_ID_STARS_IN_OSC"); 
	WriteIDStars(BestIDStars, "BEST_IDStars");
	WritePosStatistics(BestPosStatistics, 
							"BEST_STAR_POSITION_STATISTICS(Degree)"); 
	WriteDoubleNUM_ST(BestPerpendicularErr, 
							"\nBEST_PERPENDICULAR_ERROR(Arc Second)");
	WriteDoubleNUM_ST(BestParallelErr, "BEST_PARALLEL_ERROR(Arc Second)");
	WriteMatrixNUM_ST(BestDCM_ECI2ST, "BEST_DCM_ECI2ST");
	WriteQuaternionNUM_ST(BestQuat_ECI2ST, "BEST_QUATERNION_ECI2ST"); 	
	WriteMatrixNUM_ST(DCM_ST2BDY, "ST_ALIGNMENT_DCM_ST2BDY");
	WriteMatrixNUM_ST(DCM_BDY2ST, "ST_ALIGNMENT_DCM_BDY2ST");
	WriteQuaternionNUM_ST(Quat_ST2BDY, "ST_ALIGNMENT_QUAT_ST2BDY"); 
	WriteQuaternionNUM_ST(Quat_BDY2ST, "ST_ALIGNMENT_QUAT_BDY2ST"); 
    WriteDoubleNUM_ST(STBoresight_Correction, 
                      "ST_BORESIGHT_ESTIMATE_CORRECTION(Arc Second)");
    WriteDoubleNUM_ST(STRotationAboutBoresight_Correction,
                      "ST_ROTATION_ABOUT_BORESIGHT_ESTIMATE_CORRECTION(Arc Second)");
	WriteMatrix(&DCM_SS2BDY, "SS_ALIGNMENT_DCM_SS2BDY");
	WriteMatrix(&DCM_BDY2SS, "SS_ALIGNMENT_DCM_BDY2SS");
	WriteQuaternion(&Quat_SS2BDY, "SS_ALIGNMENT_QUAT_SS2BDY");
	WriteQuaternion(&Quat_BDY2SS, "SS_ALIGNMENT_QUAT_BDY2SS");
    WriteDouble(SSBoresight_Correction,
                "SS_BORESIGHT_ESTIMATE_CORRECTION(Arc Second)");
	WriteMatrix(&DCM_ECI2BDY, "ESTIMATED_DCM_ECI2BDY");
	WriteMatrix(&DCM_BDY2ECI, "ESTIMATED_DCM_BDY2ECI");
	WriteQuaternion(&Quat_ECI2BDY, "ESTIMATED_QUAT_ECI2BDY");
	WriteQuaternion(&Quat_BDY2ECI, "ESTIMATED_QUAT_BDY2ECI");
	WriteRecommendTrack(RecommendTrack, "Recommend OSC Stars to be Tracked");
	WriteCISOutput(CISOutput, "\nCIS_OUTPUT");

	stat1 = COMPLETE; 
	/* --------------------------------------------------------------- 
	DisplayMessage(stat1, ERROR_TEXT);
	---------------------------------------------------------------  */
	LogMessage(stat1, ERROR_TEXT);

	time(&t);
	fprintf(LogFp, "#	CIS Finished at %s", ctime(&t));

	free(SubSC);
	fclose(LogFp);

	/* --------------------------------------------------------------- 
		Match D. Needelman's Ouput data format for result comparision 
	---------------------------------------------------------------  */

#ifdef CIS_TEST

	TestOutCount("% Case");
	TestOutString("%");
	TestOutInt(RefST1_ID, "saa_mc_ref1_st");
	TestOutInt(RefST2_ID, "saa_mc_ref2_st");
	TestOutString("%");
	TestOutQuaternion(&Quat_ECI2BDY, "saa_mc_mqecitimu");
	TestOutString("%");
	TestOutQuaternionNUM_ST(Quat_BDY2ST, "saa_mc_mqimutst");
	TestOutString("%");
	TestOutMatrix(&DCM_SS2BDY, "saa_mc_mcsstimu_row");
	TestOutString("%");
	TestOutDoubleNUM_ST(BestPerpendicularErr, "saa_mc_errperp");
	TestOutString("%");
	TestOutDoubleNUM_ST(BestParallelErr, "saa_mc_errpar");
	TestOutString("%");
	TestOutIntNUM_ST(ValidRef, "saa_mc_validref");
	TestOutString("%");
	TestOutIntNUM_ST(IDContradictory, "saa_mc_contid");
	TestOutString("%");
	TestOutIntNUM_ST(XTightID, "saa_mc_xtight_id"); 
	TestOutString("%");
	TestOutIDStars(BestIDStars, RecommendTrack, 
					"saa_mc_idstars", "saa_mc_recommend");
	if (stat == TOTAL_SUCCESS)
	{
		TestOutDouble(RTD*UpRightSlewAngle, "\nRECOMMEND_UPRIGHT_SLEW_ANGLE");	
		TestOutDouble(RTD*InvertedSlewAngle, "RECOMMEND_INVERTED_SLEW_ANGLE");	   
	}
	if (stat == PARTIAL_SUCCESS)
	{
		TestOutDouble(RTD*PlusSlewAngle, "\nRECOMMEND_PLUS_SLEW_ANGLE");
		TestOutDouble(RTD*MinusSlewAngle, "RECOMMEND_MINUS_SLEW_ANGLE");
	}
    TestOutDoubleNUM_ST(STBoresight_Correction, "saa_mc_stboresight_corr");
    TestOutDoubleNUM_ST(STRotationAboutBoresight_Correction,
      "saa_mc_st_rot_boresight_corr");
    TestOutDouble(SSBoresight_Correction, "saa_mc_ssboresight_corr");

	TestOutString("%");

#endif /* CIS_TEST */

	return(stat1);
}	

/************************************************************************************* 
	Change Log:
	$Log: CISExe.c,v $
	Revision 1.27  2000/05/22 23:26:42  jzhao
	Modified for integration.
	
	Revision 1.26  2000/04/03 16:44:40  jzhao
	Modified for integrating to TCL.
	
	Revision 1.25  2000/03/28 22:48:26  jzhao
	Changed CISOutput format.
	
	Revision 1.24  2000/03/28 22:14:56  jzhao
	Changed priority of recommended tracks.
	Changed output format of recommended tracks.
	
	Revision 1.23  2000/03/21 23:35:44  jzhao
	Added CIS_Slew functions.
	
	Revision 1.22  2000/03/15 21:21:21  jzhao
	Rearranged SubCatalog memory allocation.
	
	Revision 1.21  2000/02/25 21:38:00  jzhao
	Get Log file name from GUI.
	
	Revision 1.20  2000/02/24 00:13:28  jzhao
	Modified for port to NT
	
	Revision 1.19  2000/02/22 23:34:24  jzhao
	Move back FILE *LogFp.
	
	Revision 1.18  2000/02/22 22:27:55  jzhao
	Set back LogFp to local.
	
	Revision 1.17  2000/02/22 22:23:42  jzhao
	Move back LogFp and set it in CWD.(GUI is not ready for it)
	
	Revision 1.16  2000/02/22 19:32:16  jzhao
	Remove fopen and fclose for Log file.(GUI take care)
	
	Revision 1.15  2000/02/22 18:22:30  jzhao
	Set FILE *LogFp to extern variable(get from GUI).
	
	Revision 1.14  2000/02/19 01:18:38  jzhao
	Undefined CIS_TEST
	
	Revision 1.13  2000/02/14 21:29:05  jzhao
	Add functions of GetRoughKnowledgeRadius and GetSunHoldAnnulus. They can be
	called indipendently of CISEXE.
	Move MAX_ROUGH_KNOWLEDGE_ERROR from macro define to IniVariables.
	
	Revision 1.12  2000/02/12 01:03:51  jzhao
	Added GetSunHoldAnnulus and GetRoughKnowledgeRadius for GUI.
	
	Revision 1.11  2000/02/11 19:26:10  jzhao
	Modified Log Foramt.
	
	Revision 1.10  2000/02/10 18:25:25  jzhao
	Modiffied CIS Log Format.
	
	Revision 1.9  2000/02/09 23:54:11  jzhao
	Modified for intergrating to GUI.
	
	Revision 1.8  2000/01/06 22:44:16  jzhao
	Minor change to prevent Math error.
	
	Revision 1.7  1999/12/24 00:43:56  jzhao
	Changed Sun Hold Mode error tolerance.
	
	Revision 1.6  1999/12/22 01:09:20  jzhao
	Modified on create BestIDStars List.
	
	Revision 1.5  1999/12/17 18:09:12  jzhao
	Modified for Testing.
	
	Revision 1.4  1999/12/17 01:30:14  jzhao
	Added Conditions for creating BestIDStars List.
	
	Revision 1.3  1999/12/16 20:05:15  jzhao
	Remove Flag "ValidRef" from ChooseOSCStarForVT.
	
	Revision 1.2  1999/12/15 01:16:11  jzhao
	Modified for Multiple cases Testing.
	
	Revision 1.1  1999/12/13 18:42:50  jzhao
	Initial Release.
	
**************************************************************************************/
