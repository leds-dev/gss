/* $Id: StarCatalog.c,v 1.6 2000/03/15 21:21:22 jzhao Exp $ */ 
/*==========================================================================
	
	FILE NAME:
		StarCatalog.c

	DESCRIPTION:
		This file includes Aqcuision Star Catalog, Sub_Star Catalog, .... 
		Creations.

	FUNCTION LIST:
	void GenSunPointingSubCatalog(AcqSC_type	*AcqStarCatalog,
								  VECTOR_type	*SunECI,
								  VECTOR_type	SunST[],
								  VECTOR_type	FOVCornerST[],
								  double		Tolerance,
								  SubSC_type	SubStarCatalog[],
								  MaxMinAng_type MinMaxAngBetSTrackerSun[])

===========================================================================

	REVISION HISTORY

=========================================================================== */

/* System Header */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Self defined Header */

#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <MessageIO.h>
#include <CISGlobal.h>
#include <StarTracker.h>
#include <StarCatalog.h>

#define ERROR_TEXT "in StarCatalog"

/* ================================================================== */

/*----------------
DecCompareFunASC: Compare Declination for qsort. 
*/

int DecCompareFunASC(const void *p1, const void *p2)
{
	StarASC_type *a, *b; 
	a = (StarASC_type *)p1;
	b = (StarASC_type *)p2;

	if(a->Declina < b->Declina) return(-1);
	if(a->Declina > b->Declina) return(1);
	else return(0);
}

/* -------------------------
int Read_StarCatalog: Read StarCatalog from file.

Input:
	FileName: char[]. File name and path.
	CatalogType:	  int. ASC_TYPE(Acq Star Cat) or SUBSC_TYPE(Sub-Star Cat). 
Output:
	StarCatalog:	
return:
	Error status
*/

int Read_StarCatalog(char *FileName, void *StarCatalog, int CatalogType)
{
	int error_st=0;
	int Star_indx=0;
	int	NumStars;
	StarASC_type *ASC;
	SubSC_type *SUBSC;
	FILE *fp;


	if((fp = fopen(FileName, "r")) == NULL)
	{
		error_st = FILE_ERROR; 
		DisplayMessage(error_st, FileName);
		DisplayMessage(error_st, ERROR_TEXT);
	}
	else
	{
		printf("open file %s\n", FileName);

		ASC = StarCatalog;
		SUBSC = StarCatalog;
		
		if(CatalogType == ASC_TYPE)
		{
			fscanf(fp, "%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s\n");

			while(!feof( fp ))
			{
				fscanf(fp, "%ld %ld %ld %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
							&(ASC->Sky2k_ID), 
				 			&(ASC->BSS_ID),
				 			&(ASC->OSC_ID),
				 			&(ASC->RAscen), 
				 			&(ASC->Declina), 
				 			&(ASC->ECI[0]), 
				 			&(ASC->ECI[1]), 
				 			&(ASC->ECI[2]), 
				 			&(ASC->Magnitude), 
				 			&(ASC->MagnitudeST[0]), 
				 			&(ASC->MagnitudeST[1]), 
				 			&(ASC->MagnitudeST[2]), 
				 			&(ASC->MagResidual));
				ASC->ASC_ID = Star_indx; 
				Star_indx++;
				ASC++;

				if ( Star_indx >= MAXNUM_ASC_STARS )  
				{
					error_st = FILE_ERROR;
					DisplayMessage(error_st, "Too Many ASC Stars in file");
					break;
				}
			}	
			NumStars = Star_indx;
		}
		else if(CatalogType == SUBSC_TYPE)
		{
			while(!feof( fp ))
			{
				fscanf(fp, "%ld %ld %lf %lf %lf %lf %lf\n", 
							&(SUBSC->Stars[Star_indx]->ASC_ID), 
							&(SUBSC->Stars[Star_indx]->OSC_ID), 
						 	&(SUBSC->Stars[Star_indx]->ECI[0]),
						 	&(SUBSC->Stars[Star_indx]->ECI[1]),
						 	&(SUBSC->Stars[Star_indx]->ECI[2]),
						 	&(SUBSC->Stars[Star_indx]->Magnitude),
							&(SUBSC->Stars[Star_indx]->MagResidual));  
				Star_indx++;
				SUBSC++;

				if (Star_indx >= MAXNUM_SUBSC_STARS) 
				{
					error_st = FILE_ERROR;
					DisplayMessage(error_st, "Too Many SUBSC Stars");
					break;
				}
			}	
			NumStars = Star_indx;
		}	
	}
	fclose(fp);
	return(NumStars);
}

/* --------------------------

Write_StarCatalog: Write Star Catalog to file.

Input:
	FileName:	Destination File Name and path.
	StarCatalog:	
	CalalogType:		ASC_TYPE or SUBSC_TYPE
Output:
Return:	error status.

*/
int Write_StarCatalog(char *FileName, void *StarCatalog, int CatalogType)
{
	int error_st=0;
	int Star_indx;
	AcqSC_type *ASC;
	SubSC_type *SUBSC;
	FILE *fp;

	if((fp = fopen(FileName, "w")) == NULL)
	{
		printf("Can not open %s\n", FileName);
		error_st = FILE_ERROR; 
		DisplayMessage(error_st, FileName);
		DisplayMessage(error_st, ERROR_TEXT);
	}
	else
	{
		ASC = StarCatalog;
		SUBSC = StarCatalog;

		if(CatalogType == ASC_TYPE)
			for(Star_indx=0; Star_indx < ASC->NumStars; Star_indx++)
			{
				fprintf(fp, "%10d %10d %10.4f %10.4f %10.4f %10.4f %10.4f \
							%10.4f %10.4f %10.4f %10.4f %10.4f %10d %10d\n", 
							ASC->Stars[Star_indx]->Sky2k_ID,
							ASC->Stars[Star_indx]->OSC_ID,
						 	ASC->Stars[Star_indx]->RAscen,
						 	ASC->Stars[Star_indx]->Declina,
							ASC->Stars[Star_indx]->ECI[0],
							ASC->Stars[Star_indx]->ECI[1],
							ASC->Stars[Star_indx]->ECI[2],
						 	ASC->Stars[Star_indx]->Magnitude,
							ASC->Stars[Star_indx]->MagnitudeST[0],
							ASC->Stars[Star_indx]->MagnitudeST[1],
							ASC->Stars[Star_indx]->MagnitudeST[2],
							ASC->Stars[Star_indx]->MagResidual,
							ASC->Stars[Star_indx]->ASC_ID,
							ASC->Stars[Star_indx]->BSS_ID);
			}
		else if(CatalogType == SUBSC_TYPE)
			for(Star_indx=0; Star_indx < SUBSC->NumStars; Star_indx++)
			{
				fprintf(fp, "%10d %10d %10.4f %10.4f %10.4f %10.4f %10.4f\n", 
							SUBSC->Stars[Star_indx]->ASC_ID, 
							SUBSC->Stars[Star_indx]->OSC_ID, 
						 	SUBSC->Stars[Star_indx]->ECI[0],
						 	SUBSC->Stars[Star_indx]->ECI[1],
						 	SUBSC->Stars[Star_indx]->ECI[2],
						 	SUBSC->Stars[Star_indx]->Magnitude,
							SUBSC->Stars[Star_indx]->MagResidual);  
			}
	}
	fclose(fp);
	return(error_st);
}

/* --------------------------

Write_RaDec: Write Star Catalog to file only RA and DEC.

Input:
	FileName:	Destination File Name and path.
	StarCatalog:	
Output:
Return:	error status.

*/
int Write_RaDec(char *FileName, SubSC_type *StarCatalog)
{
	int error_st=0;
	int Star_indx;
	double RA, DEC;
	FILE *fp;

	if((fp = fopen(FileName, "w")) == NULL)
	{
		printf("Can not open %s\n", FileName);
		error_st = FILE_ERROR; 
		DisplayMessage(error_st, FileName);
		DisplayMessage(error_st, ERROR_TEXT);
	}
	else
	{
		for(Star_indx=0; Star_indx < StarCatalog->NumStars; Star_indx++)
		{
			ECI2RAscenDeclina(StarCatalog->Stars[Star_indx]->ECI, 
							  &RA, &DEC, DOUBLE_DATA);
			RA *= RTD; DEC *= RTD;

			fprintf(fp, "%10.4f %10.4f\n", RA, DEC); 
		}
	}

	fclose(fp);
	return(error_st);
}
/*----------------
MagCompareFunSubSC: Compare Magnitude for qsort. 
*/

int MagCompareFunSubSC(const void *p1, const void *p2)
{
	StarSubSC_type *a, *b; 
	a = (StarSubSC_type *)p1;
	b = (StarSubSC_type *)p2;

	if(a->Magnitude < b->Magnitude) return(-1);
	if(a->Magnitude > b->Magnitude) return(1);
	else return(0);
}
/* -----------------------------------
CalMinMaxAngBetSTrackerSun: Calculate the Min nd Max angles between the 
							star tracker corners and the Sun Sensor LOS 
							in ST Frame(in sun-hold mode).
Input:
	SunST:				array[NUM_ST] of VECTOR_type.
						Sun Sensor LOS in each Tracker frame.		
	FOVCornerST:		array[4] of VECTOR_type.
						FOV corner positions of Star Tracker.
Output:
	MaxMinAngBetSTrackerSun:	Array[NUM_ST] of MaxMinAng_type.
								Max and Min angle between the each star tracker 
								corners and the s/c body vector pointing to the 
								sun(in sun-hold mode, radians)
*/

void CalMinMaxAngBetSTrackerSun(VECTOR_type	SunST[],
							  	VECTOR_type	FOVCornerST[],
							  	MaxMinAng_type MinMaxAngBetSTrackerSun[])
{
	int ST_indx, Corner_indx;
	double Angle;

	/* Begin by tentatively assuming that the minimum and maximum angles
	   are at th first corner */
	for(ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		AngleBetVectors(SunST[ST_indx].Vec, 
					    FOVCornerST[0].Vec, &Angle,
						DOUBLE_DATA);
		MinMaxAngBetSTrackerSun[ST_indx].MinAng = Angle;
		MinMaxAngBetSTrackerSun[ST_indx].MaxAng = 
									MinMaxAngBetSTrackerSun[ST_indx].MinAng;
		for(Corner_indx=1; Corner_indx<4; Corner_indx++)
		{
			AngleBetVectors(SunST[ST_indx].Vec, 
							FOVCornerST[Corner_indx].Vec, 
							&Angle, DOUBLE_DATA);
			if(Angle < MinMaxAngBetSTrackerSun[ST_indx].MinAng)
				MinMaxAngBetSTrackerSun[ST_indx].MinAng = Angle;
			else if(Angle > MinMaxAngBetSTrackerSun[ST_indx].MaxAng)
				MinMaxAngBetSTrackerSun[ST_indx].MaxAng = Angle;
		}
	}
}

/*------------------------
CalRadiusAnnulusSTFOV: Calculates the Max possible inner and outer 
						radius of the Annulus for ST FOV at sun hold mode.
Input:
	SunST:				array[NUM_ST] of VECTOR_type.
						Sun Sensor LOS in each Tracker frame.		
	FOVCornerST:		array[4] of VECTOR_type.
						FOV corner positions of Star Tracker.
	Tolerance:	double. (radians).
	
Output:
	InnerRadius[NUM_ST]:	double. (radians).
	OuterRadius[NUM_ST]:	double.	(radians).
*/

void CalRadiusAnnulusSTFOV(VECTOR_type SunST[],
					  		VECTOR_type FOVCornerST[],
					  		double Tolerance,
					  		double InnerRadius[],
					  		double OuterRadius[])
{
	int ST_indx;
	MaxMinAng_type	MinMaxAngBetSTrackerSun[NUM_ST];
	
	CalMinMaxAngBetSTrackerSun(SunST, FOVCornerST, MinMaxAngBetSTrackerSun);

	for (ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		InnerRadius[ST_indx] = MinMaxAngBetSTrackerSun[ST_indx].MinAng 
															- Tolerance;
		OuterRadius[ST_indx] = MinMaxAngBetSTrackerSun[ST_indx].MaxAng 
															+ Tolerance;
	}
}

/*------------------------
GenSunPointingSubCatalog:	Generate Sub-Star Catalog(SSC) of Acquision Star
	Catalog(ASC) for each Star Tracker.  Given sun-position knowledge, SSC 
	will contain all ASC stars within an annulus on the celestial sphere. 
	The "radius" of the annulus will be the maximum possible (given 
	alignment error)angular separation between the sun sensor LOS, and the 
	tracker boresight. The width of the annulus will depend on the tracker FOV 
	geometry, and the location of LOS.

Inupt:
	AcqStarCatalog:		AcqSC_type. contain all Acqusion Stars.
	SunECI:				vector. Position of Sun in ECI frame.
	SunST:
	Tolerance:
Output:
	SubStarCatalog:		array[NUM_ST] of SubSC_type. Contain Sub_catalog stars
						for each Tracker.
*/

void GenSunPointingSubCatalog(AcqSC_type	*AcqStarCatalog,
							  VECTOR_type	*SunECI,
							  VECTOR_type   SunST[], 
							  double 		Tolerance,
							  SubSC_type	SubStarCatalog[])
{
	int ST_indx, ASC_indx, SubSC_indx;
	VECTOR_type FOVCornerST[4];	
	double		InnerRadius[NUM_ST];
    double		OuterRadius[NUM_ST];
	double 		Angle;
	double 		*StarECI;

	STFOV_CornerPos_STFrame(FOVCornerST);
    CalRadiusAnnulusSTFOV(SunST, FOVCornerST, Tolerance,
								 InnerRadius, OuterRadius);


	for(ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		SubStarCatalog[ST_indx].NumStars = 0;
		SubStarCatalog[ST_indx].ST_ID = ST_indx + 1;
	}

/* Creat Sub-Star Catalogs(Sub-Sets of Acq Star Catalog) that contain only the
   stars visible to the relevant tracker. */

	for(ASC_indx=0; ASC_indx<AcqStarCatalog->NumStars; ASC_indx++)
	{
	/* For each star, calculate the ECI vector, and the angular separation 
		from the sun */

		StarECI = (double *)AcqStarCatalog->Stars[ASC_indx]->ECI;
		AngleBetVectors(StarECI, SunECI->Vec, &Angle, DOUBLE_DATA);
		
		/* For each tracker, examine the star is with in the 
			MinMaxAngBetSTrackerSun.
			Assuming that the s/c sun sensor LOS, will be pointing towards
			the sun(to within some acceptable tolerance) at the time we take 
			data from the tracker. The only stars visible to tracker will be
			those such that the LOS angle between star and sun matches the
			angle between tracker boresight and roll axis, to within some 
			given tolerance. this tolerance will depend on the star tracker
			FOV, tracker error(including misalignment), and the allowed error
			in sun pointing. */

		for(ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		{
			if( (Angle >= InnerRadius[ST_indx])&&
				(Angle <= OuterRadius[ST_indx]) )
			{	
				SubSC_indx = SubStarCatalog[ST_indx].NumStars; 

				SubStarCatalog[ST_indx].Stars[SubSC_indx]->ECI[0] = 
																StarECI[0];
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->ECI[1] = 
																StarECI[1];
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->ECI[2] = 
																StarECI[2];
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->Magnitude =
				 		 AcqStarCatalog->Stars[ASC_indx]->MagnitudeST[ST_indx];
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->MagResidual = 
						 AcqStarCatalog->Stars[ASC_indx]->MagResidual;
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->OSC_ID = 
						 AcqStarCatalog->Stars[ASC_indx]->OSC_ID;
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->ASC_ID = ASC_indx+1;
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->BSS_ID = 
						 AcqStarCatalog->Stars[ASC_indx]->BSS_ID;

				SubStarCatalog[ST_indx].NumStars++;
			}	
		} /* end of for(ST_indx..) */
	} /* end of for(ASC_indx..) */

/* Sort Sub-Catalogs by star magnitude(brightest stars first) */
	
	for(ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		qsort(SubStarCatalog[ST_indx].Stars[0], 
			  SubStarCatalog[ST_indx].NumStars,
			  sizeof(StarSubSC_type), MagCompareFunSubSC);
}

/* ----------------------------------------
CalRadiuSpotSTFOV:	Calculates Max possible Radius of 'Spot' for ST FOV at 
					Rough Knowledge mode.

Input:
	Tolerance:	double. (radians) 
Output:
	MaxRadius:	double. (radians).
*/

void CalRadiuSpotSTFOV(double Tolerance, double *MaxRadius)
{
	*MaxRadius = Tolerance + MAX_STFOV_RADIUS;
}

/*------------------------------------------
GenRoughKnowledgeSubCatalog: Generate a Sub-Catalog of AcqCatalog,
							 given rough knowledge of s/c attitude.

Input:
	AcqStarCatalog:	AcqSC_type. contain all SAA Stars.
	DCM_ECI2ST:		array[NUM_ST] of DirCosMatrix_type. Direction 
					Cosine matrix from ECI to ST frame.
	STBoresight:	VECTOR_type:	Boresight of Tracker.	
	MaxRadiuSpot:	double. Max possible radius of ST FOV covered at 
					Rough Knowledge mode(radians). 
	
Output:
	SubStarCatalog:		Array[NUM_ST] of SubSC_type. Contain Sub-Catalog Stars
						for each tracker.
*/

void GenRoughKnowledgeSubCatalog(AcqSC_type *AcqStarCatalog,
								 DirCosMatrix_type  DCM_ECI2ST[],
								 VECTOR_type *STBoresight,
								 double		  Tolerance,
								 SubSC_type	  SubStarCatalog[])
{
	int ST_indx, ASC_indx, SubSC_indx;
	double  *StarECI; 
	double  MaxRadiuSpot;
	VECTOR_type STBoresightECI[NUM_ST];
	double Angle;

	CalRadiuSpotSTFOV(Tolerance, &MaxRadiuSpot); 

	for(ST_indx=0; ST_indx<NUM_ST; ST_indx++)
	{
		SubStarCatalog[ST_indx].NumStars = 0;
		SubStarCatalog[ST_indx].ST_ID = ST_indx + 1;

		MatrixMult(STBoresight->Vec, DCM_ECI2ST[ST_indx].DCM, 
				   STBoresightECI[ST_indx].Vec, 1, 3, 3, DOUBLE_DATA);
	}
/* Create star catalogs(subsets of Acqcatalog) that only contain the stars
	visible to the relevant tracker. */

	for(ASC_indx=0; ASC_indx<AcqStarCatalog->NumStars; ASC_indx++)
	{
		/* For each Star, get the ECI vector */
		StarECI = (double *)AcqStarCatalog->Stars[ASC_indx]->ECI; 

		/* For each Tracker, exmaine the star is qulify for Sub-Catalog */

		/* Assuming that the trackert boresight is pointing towards some
		   known spot (to within some acceptable tolerance). Therefore, 
		   the only stars visible to the tracker will be those within 
		   MAX_STFOV_RADIUS from the boresight, plus the tolerance. */

		for(ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		{
			/* Calculate the angle between STBoresightECI and StarECI */
			AngleBetVectors(STBoresightECI[ST_indx].Vec, StarECI, 
							&Angle, DOUBLE_DATA);

			if(Angle <= MaxRadiuSpot)
			{	
				SubSC_indx = SubStarCatalog[ST_indx].NumStars;

				SubStarCatalog[ST_indx].Stars[SubSC_indx]->ECI[0] = 
																StarECI[0];
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->ECI[1] = 
																StarECI[1];
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->ECI[2] = 
																StarECI[2];
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->Magnitude =
				 		 AcqStarCatalog->Stars[ASC_indx]->MagnitudeST[ST_indx];
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->MagResidual = 
						 AcqStarCatalog->Stars[ASC_indx]->MagResidual;
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->OSC_ID = 
						 AcqStarCatalog->Stars[ASC_indx]->OSC_ID;
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->ASC_ID = ASC_indx+1;
				SubStarCatalog[ST_indx].Stars[SubSC_indx]->BSS_ID = 
						 AcqStarCatalog->Stars[ASC_indx]->BSS_ID;

				SubStarCatalog[ST_indx].NumStars++;
			}	
		} /* end of ST_indx */
	} /* end of Star_indx */

/* Sort Sub-Catalogs by star magnitude(brightest stars first) */
	
	for(ST_indx=0; ST_indx<NUM_ST; ST_indx++)
		qsort(SubStarCatalog[ST_indx].Stars[0], 
			  SubStarCatalog[ST_indx].NumStars,
			  sizeof(StarSubSC_type), MagCompareFunSubSC);
}

/***********************************************************************************
	Change Log:
	$Log: StarCatalog.c,v $
	Revision 1.6  2000/03/15 21:21:22  jzhao
	Rearranged SubCatalog memory allocation.
	
	Revision 1.5  2000/02/23 22:18:47  jzhao
	Modified for port to NT
	
	Revision 1.4  2000/02/11 19:26:11  jzhao
	Modified Log Foramt.
	
	Revision 1.3  2000/02/09 23:54:12  jzhao
	Modified for intergrating to GUI.
	
	Revision 1.2  1999/12/15 01:16:12  jzhao
	Modified for Multiple cases Testing.
	
	Revision 1.1  1999/12/13 18:42:52  jzhao
	Initial Release.
	
************************************************************************************/
