########################################################################
#	$Id: $
########################################################################

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ASC {
	variable num_stars
	namespace export init
	
}

########################################################################

proc ::ASC::init { } {
	variable num_stars
	global asc_len

#	set magnitude variation adjustments before loading catalog.
	mag_var

	set ssc_file $::FILES::ssc

#	Check that ssc file exists
	set err [catch {set inid [open $ssc_file r]}]
	if {$err == 1} {
 	   ::MSG_DLG::attention "Could not Open SSC file \n$ssc_file \nfor read access" \
					red \
					black \
					-INSTRUCTION "Check file name and directory access privileges"
	   return
	} else {
	   close $inid
	}

#	load may be redundant but TCL doesn't care.

	load_ssc 15000 $ssc_file $::CONFIG::data(asc_epoch)

	set num_stars $asc_len

}

########################################################################

proc ::ASC::swig { name tags } {
	global gi

	set caps [string toupper $name]
	set obj [string tolower $name]

	set $obj [$caps]

	foreach i $tags {
		set key $caps
		append key _$i

		set res [$obj configure -$i $::GSSINI::gssini($key)]

	}
}

########################################################################

proc ::ASC::mag_var { } {
	ASC::swig mag_var_adj [list O B A F G K M R N DEFAULT ]
	
}

########################################################################
#	$Log: $
########################################################################
