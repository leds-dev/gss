########################################################################
#	$Id: Exp $
########################################################################
#	output control dialog

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ::OUTPUT:: {
	variable out_mode
	variable control_color
	variable control_text
	variable control_disp
	variable first_active
	variable take_btn
	variable give_btn
	
	namespace export init output_gui

}

########################################################################

proc ::OUTPUT::init { } {
	variable out_mode
	variable control_color
	variable control_text
	variable control_disp
	variable first_active

	set control_color(0) red1
	set control_color(1) green1
	set control_text(0) "InActive"
	set control_text(1) "Active"
	set first_active "True"
	
	set control_disp ""
	
	set out_mode dlrv
	if {$::CONFIG::data(tm_src) != "Realtime"} {
	    set out_mode log
	    
	}
	
}


########################################################################
#	Control the CIS Derived LRVs

proc ::OUTPUT::cis {} {
	global gi
	set logtxt ""
	set err 0

	set sensor_txt(1)   "ST1"
	set sensor_txt(2)   "ST2"
	set sensor_txt(3)   "ST3"
	
	set use_txt(0)      "NOT_USED"
	set use_txt(1)      "USED"

	set star_txt(0)     "BAD_STAR_BREAK"
	set star_txt(1)		"OSC_STAR_STAY"
	set star_txt(2)		"BAD_STAR_STAY"
	
	catch {destroy .output_cis}
	toplevel .output_cis
	wm title .output_cis "CIS DERIVED LRV VALUES"
	wm geometry .output_cis "-0+0"
	
	frame .output_cis.top \
	      -borderwidth 1 \
	      -relief sunken
	                        
	pack .output_cis.top \
	     -side top \
	     -fill both
	     
	frame .output_cis.btn \
	      -borderwidth 1 \
	      -relief sunken
	pack .output_cis.btn \
	     -side top \
	     -fill x
	     
	button .output_cis.btn.ok \
	       -text OK \
	       -font $::CONFIG::fonts(entry) \
	       -command "destroy .output_cis"
	       
	pack .output_cis.btn.ok \
	     -side top \
	     -pady 5
	       
	set outtxt {}

	#	Loop over STs
	foreach st {1 2 3} {

	#	Set Reference Star Trackers
	    set data [GetCommitValidRefST $st]
	    set lrv [$gi \
	             "GSS_ST_ACQ_USE_ST$st\_MNEM" {}]

            if {$::CONFIG::data(tlm_control) == 1} {
	        set err [gss_pseudo_set_send \
                        $lrv \
                        "I" \
                        0.0 \
                        [lindex $data 0] ]
                        
            }
                             
            if {$err != 0} {
                ::MSG_DLG::attention "Epoch error $err \
                                      setting $lrv value" \
                                      red \
                                      black \
                                      -INSTRUCTION "Check GTACS"
	        return
	                
	    }
	            
	    set outtxt "$outtxt $lrv\_VAL\t= \
					$use_txt([lindex $data 0])\n\n"

	#	Loop over VTs
	    foreach vt {1 2 3 4 5} {

	#	Get data
	        set data [GetCommitRecommendTrack $st $vt]

	#	Set Track
	        set lrv [$gi \
	                 "GSS_ST$st\_VT$vt\_DES_OSC_MNEM" {}]	

                if {$::CONFIG::data(tlm_control) == 1} {
	            set err [gss_pseudo_set_send \
                            $lrv \
                            "I" \
                            0.0 \
                            [lindex $data 3] ]
                }
                             
                if {$err != 0} {
                    ::MSG_DLG::attention "Epoch error $err \
                                          setting $lrv value" \
                                          red \
                                          black \
                                          -INSTRUCTION "Check GTACS"
	            return
	                
	        }

	        set outtxt "$outtxt $lrv\_VAL\t= \
						$star_txt([lindex $data 3])\n"

	#	Set H 
	        set lrv [$gi \
	                 "GSS_ST$st\_VT$vt\_DES_H_MNEM" {}]	


                if {$::CONFIG::data(tlm_control) == 1} {
	            set err [gss_pseudo_set_send \
                             $lrv \
                             "F" \
                             [format "%14.7E" [lindex $data 0]] \
                             0]
                             
                }
                             
                if {$err != 0} {
                    ::MSG_DLG::attention "Epoch error $err \
                                          setting $lrv value" \
                                          red \
                                          black \
                                          -INSTRUCTION "Check GTACS"
	            return
	                
	        }
	            
	        set outtxt "$outtxt $lrv\_VAL\t\t= \
	                    [format %+9.7f [lindex $data 0]]\n"

	#	Set V	                    
	        set lrv [$gi \
	                 "GSS_ST$st\_VT$vt\_DES_V_MNEM" {}]	

                if {$::CONFIG::data(tlm_control) == 1} {
	            set err [gss_pseudo_set_send \
                             $lrv \
                             "F" \
                             [format "%14.7E" [lindex $data 1]] \
                             0]
                }
                
                if {$err != 0} {
                    ::MSG_DLG::attention "Epoch error $err \
                                          setting $lrv value" \
                                          red \
                                          black \
                                          -INSTRUCTION "Check GTACS"
	            return
	                
	        }
	            
	        set outtxt "$outtxt $lrv\_VAL\t\t= \
	                    [format %+9.7f [lindex $data 1]]\n"
	                    
	#	Set Magnitude
	        set lrv [$gi \
	                 "GSS_ST$st\_VT$vt\_DES_MAG_MNEM" {}]
	                    
                if {$::CONFIG::data(tlm_control) == 1} {
	            set err [gss_pseudo_set_send \
                             $lrv \
                             "F" \
                             [format "%14.7E" [lindex $data 2]] \
                             0]
                }
                             
                if {$err != 0} {
                    ::MSG_DLG::attention "Epoch error $err \
                                          setting $lrv value" \
                                          red \
                                          black \
                                          -INSTRUCTION "Check GTACS"
	            return
	                
	        }
	            
	        set outtxt "$outtxt $lrv\_VAL\t= \
	                    [format %+6.4f [lindex $data 2]]\n"

	    }

	#	Display these for ST1 only to avoid duplication
		if {$st == 1} {
	        
	    set outtxt "$outtxt \n"

	#	Set CIS Estimated Quaternion
	   		set i 1
	    	foreach quat {0 1 2 3} {
		       	 set lrv [$gi \
	       	          "GSS_QUAT_EST_$i\_MNEM" {}]
		       	 incr i

           	     if {$::CONFIG::data(tlm_control) == 1} {
		       	     set err [gss_pseudo_set_send \
           	                 $lrv \
           	                 "F" \
           	                 [format "%14.7E" $::USER_QUAT::cis_quat($quat)] \
           	                 0 ]
                            
           	     }
                             
           	     if {$err != 0} {
           	         ::MSG_DLG::attention "Epoch error $err \
           	                               setting $lrv value" \
           	                               red \
           	                               black \
           	                               -INSTRUCTION "Check GTACS"
	       	     return
	                
		       	 }
	            
		       	 set outtxt "$outtxt $lrv\_VAL\t\t= \
							 [format %+9.7f $::USER_QUAT::cis_quat($quat)]\n"
	        
	    	}
	    
		    set outtxt "$outtxt \n"

	#	Set Angular velocity
		    set i 1
		    set data [GetCommitSC_AngVelBody]

		    foreach indx {0 1 2} {
		        set lrv [$gi \
		                 "GSS_RATE_CMD_$i\_MNEM" {}]
		        incr i

   	            if {$::CONFIG::data(tlm_control) == 1} {
		            set err [gss_pseudo_set_send \
   	                         $lrv \
   	                         "F" \
   	                         [format "%14.7E" [lindex $data $indx]] \
   	                         0 ]
   	                         
   	             }
   	                          
   	             if {$err != 0} {
   	                 ::MSG_DLG::attention "Epoch error $err \
   	                                       setting $lrv value" \
   	                                       red \
   	                                       black \
   	                                       -INSTRUCTION "Check GTACS"
		            return
		                
			     }
		            
		         set outtxt "$outtxt $lrv\_VAL\t\t= \
							 [format %+14.12f [lindex $data $indx]]\n"
		        
		    }
	
		}
    
	    set outtxt "$outtxt \n"

        text .output_cis.top.lbl$st \
             -setgrid true \
             -wrap word \
             -width 55 \
             -height 31 \
             -borderwidth 1 \
             -relief sunken \
             -font $::CONFIG::fonts(entry) 

	    pack .output_cis.top.lbl$st \
	         -side left \
	         -expand 1\
	         -fill both \
	         -anchor nw \
	         -pady 5 \
	         -padx 5

	    .output_cis.top.lbl$st insert end "$outtxt"

	#	Add to log
            set logtxt "$logtxt$outtxt"
	    set outtxt {}	    

	}


        if {$::CONFIG::data(tlm_control) != 1} {
        
            if {($::CONFIG::data(tm_src) == "Realtime")} {

                ::MSG_DLG::attention "LRVs NOT SET!\n\
                                      This GSS session does not have LRV control Active!" \
                                      red \
                                      black
                                      
            } 
                                                                                              
        } else {

#	Log it

            set msg_txt "OUTPUT CIS LRVs BUTTON PRESSED. Data from CIS\
                         $::CIS_GUI::cis_state_text\n$logtxt"
            ::MSG_DLG::msg_log $msg_txt

	}

}

########################################################################
#	Control the Alignments Derived LRVs

proc ::OUTPUT::alignments {} {
	global gi
	set logtxt ""
	set err 0
	
	set rh0 [$gi NOISE_VAR_HORIZ_RH0 5.875776e-10]
	set rv0 [$gi NOISE_VAR_VERT_RV0  5.875776e-10]
	set z00 [$gi COVAR_THRESH_Z00    0.0]

	set sensor_txt(1)   "ST1"
	set sensor_txt(2)   "ST2"
	set sensor_txt(3)   "ST3"
	set sensor_txt(sun) "SS"
		
	catch {destroy .output_align}
	toplevel .output_align
	wm title .output_align "ALIGNMENTS DERIVED LRV VALUES"
	wm geometry .output_align "-0+0"
	
	frame .output_align.top \
	      -borderwidth 1 \
	      -relief sunken
	                        
	pack .output_align.top \
	     -side top \
	     -fill both
	     
	frame .output_align.btn \
	      -borderwidth 1 \
	      -relief sunken
	pack .output_align.btn \
	     -side top \
	     -fill x
	     
	button .output_align.btn.ok \
	       -text OK \
	       -font $::CONFIG::fonts(entry) \
	       -command "destroy .output_align"
	       
	pack .output_align.btn.ok \
	     -side top \
	     -pady 5
	       
	set outtxt {}
	
	foreach sensor {1 2 3 sun} {
	    foreach i {1 2 3} {
	        foreach j {1 2 3} {
	            set lrv [$gi \
	                     "GSS_$sensor_txt($sensor)\_TO_BDY_$i\_$j\_MNEM" \
	                     {}]
	            
                    if {$::CONFIG::data(tlm_control) == 1} {
	                set err [gss_pseudo_set_send \
                                 $lrv \
                                 "F" \
                                 $::ALIGN::gss_dcm($sensor,$i,$j) \
                                 0]
                                 
                    }
                         
                    if {$err != 0} {
                        ::MSG_DLG::attention "Epoch error $err \
                                              setting $lrv value" \
                                              red \
                                              black \
                                              -INSTRUCTION "Check GTACS"
	                return
	                
                    }
	            
	            set outtxt "$outtxt $lrv\_VAL =\
	                        [format %+9.7f $::ALIGN::gss_dcm($sensor,$i,$j)]\n"
	                        
	        }
	        
	    }
	    
	    if {$sensor == 1} { 

               if {$::CONFIG::data(tlm_control) == 1} {
	               set lrv [$gi "GSS_RH0_MNEM" {}]
               	   set err [gss_pseudo_set_send \
                            $lrv \
                            "F" \
                            $rh0 \
                            0]
                                 
               }
                         
               if {$err != 0} {
                   ::MSG_DLG::attention "Epoch error $err \
                                setting GSS_RH0 value" \
                                red \
                                black \
                                -INSTRUCTION "Check GTACS"
	           return
	                
               }
	            
	       set outtxt "$outtxt GSS_RH0_VAL = [format %+14.7E $rh0]\n"

               if {$::CONFIG::data(tlm_control) == 1} {
	               set lrv [$gi "GSS_RV0_MNEM" {}]
               	   set err [gss_pseudo_set_send \
                            $lrv \
                            "F" \
                            $rv0 \
                            0]
                                 
               }
                         
               if {$err != 0} {
                   ::MSG_DLG::attention "Epoch error $err \
                                setting GSS_RV0 value" \
                                red \
                                black \
                                -INSTRUCTION "Check GTACS"
	           return
	                
               }
	            
	       set outtxt "$outtxt GSS_RV0_VAL = [format %+14.7E $rv0]\n"

               if {$::CONFIG::data(tlm_control) == 1} {
	               set lrv [$gi "GSS_Z00_MNEM" {}]
               	   set err [gss_pseudo_set_send \
                            $lrv \
                            "F" \
                            $z00 \
                            0]
                                 
               }
                         
               if {$err != 0} {
                   ::MSG_DLG::attention "Epoch error $err \
                                setting GSS_Z00 value" \
                                red \
                                black \
                                -INSTRUCTION "Check GTACS"
	           return
	                
               }
	            
	       set outtxt "$outtxt GSS_Z00_VAL = [format %+14.7E $z00]\n"

            }        
	    
	    set outtxt "$outtxt \n"

            text .output_align.top.lbl$sensor \
                 -setgrid true \
                 -wrap word \
                 -width 40 \
                 -height 13 \
                 -borderwidth 1 \
                 -relief sunken \
                 -font $::CONFIG::fonts(entry) 

	    pack .output_align.top.lbl$sensor \
	         -side left \
	         -expand 1\
	         -fill both \
	         -anchor nw \
	         -pady 5 \
	         -padx 5

	   .output_align.top.lbl$sensor insert end "$outtxt"
	   

	#   Add to log
	    set logtxt "$logtxt$outtxt"
	    set outtxt {}
	    
	}

        if {$::CONFIG::data(tlm_control) != 1} {
        
            if {($::CONFIG::data(tm_src) == "Realtime")} {

                ::MSG_DLG::attention "LRVs NOT SET!\n\
                                      This GSS session does not have LRV control Active!" \
                                      red \
                                      black
                                      
            } 
                                                                                              
        } else {
        
#	Log it

        set msg_txt "OUTPUT ALIGNMENT LRVs BUTTON PRESSED - Data from\
					 $::ALIGN::align_log_text \n$logtxt"
            ::MSG_DLG::msg_log $msg_txt
            
        }
	
}

########################################################################
#	Control the Slew Derived LRVs

proc ::OUTPUT::slew {} {
	global gi
	
	set err 0
		
	catch {destroy .output_slew}
	toplevel .output_slew
	wm title .output_slew "SLEW DERIVED LRV VALUES"
	wm geometry .output_slew "-0+0"
	
	frame .output_slew.top \
	      -borderwidth 1 \
	      -relief sunken
	                        
	pack .output_slew.top \
	     -side top \
	     -fill both
	     
	frame .output_slew.btn \
	      -borderwidth 1 \
	      -relief sunken
	pack .output_slew.btn \
	     -side top \
	     -fill x
	     
	button .output_slew.btn.ok \
	       -text OK \
	       -font $::CONFIG::fonts(entry) \
	       -command "destroy .output_slew"
	       
	pack .output_slew.btn.ok \
	     -side top \
	     -pady 5
	       
	set outtxt {}
	set i 1
	foreach quat {0 1 2 3} {
	    set lrv [$gi \
	             "GSS_QUAT_CMD_$i\_MNEM" \
	             {}]
	    incr i
	    	            
            if {$::CONFIG::data(tlm_control) == 1} {
	        set err [gss_pseudo_set_send \
                         $lrv \
                         "F" \
                         [format "%14.7E" $::SLEW::term_quat($quat)] \
                         0]
                         
            }
                             
            if {$err != 0} {
                ::MSG_DLG::attention "Epoch error $err \
                                     setting $lrv value" \
                                     red \
                                     black \
                                     -INSTRUCTION "Check GTACS"
	        return

	    }

	    set outtxt "$outtxt $lrv\_VAL =\
	                [format %+9.7f $::SLEW::term_quat($quat)]\n"

	                        
	}


        text .output_slew.top.lbl \
             -setgrid true \
             -wrap word \
             -width 45 \
             -height 5 \
             -borderwidth 1 \
             -relief sunken \
             -font $::CONFIG::fonts(entry) 

	pack .output_slew.top.lbl \
	     -side left \
	     -expand 1\
	     -fill both \
	     -anchor nw \
	     -pady 5 \
	     -padx 5

	.output_slew.top.lbl insert end "$outtxt"

        if {$::CONFIG::data(tlm_control) != 1} {
        
            if {($::CONFIG::data(tm_src) == "Realtime")} {

                ::MSG_DLG::attention "LRVs NOT SET!\n\
                                      This GSS session does not have LRV control Active!" \
                                      red \
                                      black
                                      
            } 
                                                                                              
        } else {

	#	Log it
	    set msg_txt "OUTPUT SLEW LRVs BUTTON PRESSED - Data from Slew\
                         $::SLEW::slew_state_text\n$outtxt"
	    ::MSG_DLG::msg_log $msg_txt
	    
	}

}


########################################################################
#
proc ::OUTPUT::update_ctrl { } {
    global gi
    
    variable take_btn
    variable give_btn
	        
    # Determine whether this GSS is in control
    if {($::CONFIG::data(tlm_control_ip) == $::CONFIG::data(local_ip)) && \
        ($::CONFIG::data(tlm_control_pid) == $::CONFIG::data(local_pid))} {
	set ::CONFIG::data(tlm_control) 1
    } else { 
   	set ::CONFIG::data(tlm_control) 0
    }
    
    #  Update Output GUI and Main Status Bar display's status
    catch {
    $::OUTPUT::control_disp configure \
          -text       $::OUTPUT::control_text($::CONFIG::data(tlm_control)) \
          -background $::OUTPUT::control_color($::CONFIG::data(tlm_control))
    }

    catch {
    $::CONFIG::control_disp configure \
          -text       $::OUTPUT::control_text($::CONFIG::data(tlm_control)) \
          -background $::OUTPUT::control_color($::CONFIG::data(tlm_control))
    }
    
    # This routine should be called in realtime mode only
    set ini_sc_id [$gi SC_ID 0]
    set tm_sc_id  [::GTACS::int_lrv_value $::GTACS::lrv_idx(SPACECRAFT_ID_LRV)]
    if {($ini_sc_id == $tm_sc_id) } {
    	set status normal
    } else {
	set status disabled
    }

    if {$::CONFIG::data(tlm_control) == 0} {
    	catch {$give_btn configure -state disabled}
    	catch {$take_btn configure -state $status}
    } else {
        catch {$take_btn configure -state disabled}
    	catch {$give_btn configure -state $status}
    }
    
}


########################################################################
#
proc ::OUTPUT::take_ctrl { } {
        global gi
        
        if {$::CONFIG::data(tlm_control_ip) != "000.000.000.000"} {
        
            if {$::CONFIG::data(tlm_control_pid) < 0} {
            
                ::MSG_DLG::attention "The ground has locked out all GSS sessions from Activating LRV control" \
                                      red \
                                      black
                                                      
            } else {
            
                ::MSG_DLG::attention "Another GSS session has LRV control Active \n\
                		      \t IPA = $::CONFIG::data(tlm_control_ip) \n\
                                      \t PID = $::CONFIG::data(tlm_control_pid)" \
                                      red \
                                      black
                                      
            }
            
            return

        } else {

            set ip [split $::CONFIG::data(local_ip) "."]

            set a [format "%3.3d" [string trimleft [lindex $ip 0] "0"]]
            set b [format "%3.3d" [string trimleft [lindex $ip 1] "0"]]
            set c [format "%3.3d" [string trimleft [lindex $ip 2] "0"]]
            set d [format "%3.3d" [string trimleft [lindex $ip 3] "0"]]

            set lrv_ip "$a$b.$c$d"

	    set lrv [$gi \
	             "GSS_OUT_ENABLED_IPA_MNEM" {}]

	    set err [gss_pseudo_set_send \
                    $lrv \
                    "F" \
                    $lrv_ip \
                    0 ]
                             
            if {$err != 0} {
                ::MSG_DLG::attention "Epoch error $err \
                                      setting $lrv value" \
                                      red \
                                      black \
                                      -INSTRUCTION "Check GTACS"
	        return
	                
	    } else {
            
	        set lrv [$gi \
	                 "GSS_OUT_ENABLED_PID_MNEM" {}]

	        set err [gss_pseudo_set_send \
                        $lrv \
                        "F" \
                        $::CONFIG::data(local_pid) \
                        0]
                             
                if {$err != 0} {
                    ::MSG_DLG::attention "Epoch error $err \
                                          setting $lrv value" \
                                          red \
                                          black \
                                          -INSTRUCTION "Check GTACS"
	            return
	        } else {
	        
	            if {$::OUTPUT::first_active == "True"} {
	            	set ::OUTPUT::first_active "False"
	                ::MSG_DLG::attention " Log the Active GSS's IP Address and Process ID for subsequent\n\
                                               use in identifying the logfile associated with this GSS session" \
                                              yellow \
                                              black \
                                              -INSTRUCTION "Record GSS Identifiers"     
                    }
                    
	        }
	        
	    }
	    
	}

}


########################################################################
#
proc ::OUTPUT::release_ctrl { quitting } {
        global gi
        
        if {($::CONFIG::data(tlm_control_ip)  != $::CONFIG::data(local_ip)  ) || \
            ($::CONFIG::data(tlm_control_pid) != $::CONFIG::data(local_pid) ) } {
        
            if { $quitting == "FALSE" } {
                ::MSG_DLG::attention "This GSS session does not have LRV control Active!" \
                                      red \
                                      black
            }
            
            return
          
        } else {
        
	    set lrv [$gi "GSS_OUT_ENABLED_IPA_MNEM" {}]

	    set err [gss_pseudo_set_send \
                        $lrv \
                        "F" \
                        0.0 \
                        0 ]
                             
            if {$err != 0} {
            
                ::MSG_DLG::attention "Epoch error $err setting $lrv value" \
                                          red \
                                          black \
                                          -INSTRUCTION "Check GTACS"
	        return
	                
	    } else {
           
	        set lrv [$gi "GSS_OUT_ENABLED_PID_MNEM" {}]

	        set err [gss_pseudo_set_send \
                            $lrv \
                            "F" \
                            0.0 \
                            0 ]
                             
                if {$err != 0} {
                
                     ::MSG_DLG::attention "Epoch error $err setting $lrv value" \
                                              red \
                                              black \
                                          -INSTRUCTION "Check GTACS"
	            return
	            
	        }
	        
	    }
	    	    
	}

}

########################################################################
#
proc ::OUTPUT::output_gui { } {
	global gi
	variable out_mode
	variable take_btn
	variable give_btn

	set control_color(0) red1
	set control_color(1) green1
	
	catch {destroy .output_ui}
	toplevel .output_ui
	wm title .output_ui "OUTPUT LRV CONTROL"
	wm geometry .output_ui "-0-0"
	set f .output_ui.top
	frame $f
	pack $f


	#	Tell who is in control
	frame $f.ctl -borderwidth 1 \
		     -relief sunken
	pack $f.ctl -side top \
		    -fill both

	frame $f.ctl.title
	pack $f.ctl.title -side top \
			  -fill x \
			  -pady 2
			  	    
	label $f.ctl.title.lbl -text "Output LRV Control Status" \
			       -font $::CONFIG::fonts(label) \
			       -anchor w
	pack $f.ctl.title.lbl -side left \
			      -anchor w \
			      -fill x \
			      -pady 2
			      
	# Display selected whether in control or not
	if {$::CONFIG::data(tlm_control) == 0} {
	    set ctl_color red1
	    
	} else {
	    set ctl_color green1
	    
	}
	
	label $f.ctl.title.stat \
		-text $::OUTPUT::control_text($::CONFIG::data(tlm_control)) \
	        -font $::CONFIG::fonts(entry) \
	        -background \
	          $::OUTPUT::control_color($::CONFIG::data(tlm_control)) \
	        -width 15 \
	        -anchor c

        set ::OUTPUT::control_disp $f.ctl.title.stat
	                     
	pack $f.ctl.title.stat \
	     -side right \
	     -anchor e \
	     -padx 5

	frame $f.ctl.data
	pack $f.ctl.data -side top \
			 -fill x \
			 -anchor w
	
	frame $f.ctl.data.ip
	pack $f.ctl.data.ip -side top \
			    -fill x \
			    -anchor w		 			  
	label $f.ctl.data.ip.lbl1 -text "IP Address of Active GSS: " \
			          -font $::CONFIG::fonts(entry) \
			          -justify left \
			          -width 30 \
			          -anchor w
	pack $f.ctl.data.ip.lbl1 -side left \
			         -anchor w \
			         -padx 10
			
			
	label $f.ctl.data.ip.lbl2 -textvariable ::CONFIG::data(tlm_control_ip) \
			          -font $::CONFIG::fonts(entry) \
			          -relief sunken \
			          -width 15
			 
	pack $f.ctl.data.ip.lbl2 -side right \
			         -anchor e \
			         -padx 5
	
	frame $f.ctl.data.pid
	pack $f.ctl.data.pid -side top \
			     -fill x \
			     -anchor w		 			  
	label $f.ctl.data.pid.lbl1 -text "PID of Active GSS: " \
			           -font $::CONFIG::fonts(entry) \
			           -width 30 \
			           -justify left \
			           -anchor w
	pack $f.ctl.data.pid.lbl1 -side left \
			          -anchor w \
			          -padx 10
			
			
	label $f.ctl.data.pid.lbl2 -textvariable \
	                             ::CONFIG::data(tlm_control_pid) \
			           -font $::CONFIG::fonts(entry) \
			           -relief sunken \
			           -width 15
			 
	pack $f.ctl.data.pid.lbl2 -side right \
			          -anchor e \
			          -padx 5


	frame $f.ctl.btns
	pack $f.ctl.btns -side top \
			 -fill x \
			 -anchor c \
			 -pady 5
	    
	button $f.ctl.btns.take -text "Activate Control" \
			        -font $::CONFIG::fonts(entry) \
			        -state disabled \
			        -command "::OUTPUT::take_ctrl"
			        
	set take_btn $f.ctl.btns.take

	button $f.ctl.btns.give -text "Relinquish Control" \
			        -font $::CONFIG::fonts(entry) \
			        -state disabled \
			        -command "::OUTPUT::release_ctrl FALSE"
		
	set give_btn $f.ctl.btns.give
	        
	pack $f.ctl.btns.take -side left \
			      -fill x \
			      -padx 5 \
			      -anchor c \
			      -pady 5
	pack $f.ctl.btns.give -side right \
			      -fill x \
			      -padx 5 \
			      -anchor c \
			      -pady 5
	
	#	Define "what to do" buttons
	frame $f.out -borderwidth 1 \
		     -relief sunken
	pack $f.out -side top \
		    -fill both
		    
	label $f.out.title -text "Outputs" \
			   -font $::CONFIG::fonts(label) \
			   -anchor w
	pack $f.out.title -side top \
			  -anchor w \
			  -fill x \
			  -pady 2


	frame $f.out.btns
	pack $f.out.btns -side top \
			 -fill x \
			 -pady 5

	button $f.out.btns.cis  -text "CIS LRVs\n$::CIS_GUI::cis_state_text" \
			        -font $::CONFIG::fonts(entry) \
			        -state $::CIS_GUI::cis_committed \
			        -command "::OUTPUT::cis"

	button $f.out.btns.algn -text "Alignment LRVs\n$::ALIGN::align_state_text" \
			        -font $::CONFIG::fonts(entry) \
			        -command "::OUTPUT::alignments"

	button $f.out.btns.slew -text "Slew LRVs\n$::SLEW::slew_state_text" \
			        -font $::CONFIG::fonts(entry) \
			        -state $::SLEW::slew_commited \
			        -command "::OUTPUT::slew"
			        
	pack $f.out.btns.cis $f.out.btns.algn $f.out.btns.slew \
	     -side left \
	     -fill x \
	     -padx 10
	     
	#	OK button to close out the window
	frame $f.btn -borderwidth 1 \
		     -relief sunken
	pack $f.btn -side top \
		    -fill both
		    
	button $f.btn.ok -text OK \
			 -font $::CONFIG::fonts(entry) \
			 -command "destroy .output_ui"
			 
	pack $f.btn.ok -side top \
		       -pady 5
}


########################################################################
#	$Log: $
########################################################################
