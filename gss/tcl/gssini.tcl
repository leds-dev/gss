########################################################################
#	$Id: $
########################################################################
#	read in GSS.INI file

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ::GSSINI:: {
	variable gssini
	variable keys
	variable osc_tag "OSC_INDEX"
	variable osc_index

	namespace export init read_ini

}

########################################################################

proc ::GSSINI::init { } {
	#	global "pointer" to the read function.
	global gi

	set gi ::GSSINI::get_ini

}

########################################################################

proc ::GSSINI::read_ini { filename } {
	variable gssini
	variable keys
	variable osc_tag
	variable osc_index

	set line 0

#	Open INI file and check that it exists
	set err [catch {set file [open $filename r]}]
	if {$err == 1} {
	   ::MSG_DLG::attention "Could not Open INI file \n$filename \nfor read access" \
                             red \
                             black \
                             -INSTRUCTION "Check file name and directory access privileges"
	   return
	}
	
	set strlen [gets $file buf]
	while {$strlen >= 0} {
		incr line

#	        set fields [split $buf]
		set fields $buf
		set len [llength $fields]

		#	ignore empty lines and lines w/only blanks.
                if {([string length $buf] >= 1) || \
                    ([string index [string trim $buf] 0] != "#") || \
                    ($len < 2)                                        } {

		#	get key / command
		    set f [lindex $fields 0]

		#	using field 1 as tag, save remaining fields.
		    if { $len == 2 } {
			    set gssini($f) [lindex $fields 1]

		    } elseif { $f == $osc_tag } {
			    set osc_index([lindex $fields 1])	\
					  [lrange $fields 2 $len]

		    } else {
			    set gssini($f) [lrange $fields 1 $len]

		    }
		    
	        }
	        
	        set strlen [gets $file buf]
	        
	    }
	    
	    close $file

	#	save keys for later
	set keys [array name gssini]

}

########################################################################
#	return requested value or default
#	this allows CALLER to handle his defaults while
#		using gss.ini to initialize them.

proc ::GSSINI::get_ini { key alt } {
	variable gssini
	variable keys

	if {[lsearch -exact $keys $key] > -1} {
		return $gssini($key)

	} else {
		return $alt

	}

}

########################################################################

proc ::GSSINI::dump { } {
	variable gssini

	set names [array names gssini]
	lsort -ascii -increasing names
	foreach tag $names {
		puts "dump(): $tag = $gssini($tag)"

	}

}

########################################################################
#	$Log: $
########################################################################
