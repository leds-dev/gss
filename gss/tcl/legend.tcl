########################################################################
#	$Id: Exp $
########################################################################
#	legend data and screen display

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval LEGEND {
	variable mag_text
	variable mag_color
	variable x_size
	variable y_size
	variable x_val
	variable y_val

	namespace export init draw star_color

}

########################################################################
#	private function to initialize legend data

proc ::LEGEND::init { } {
	variable mag_text
	variable mag_color
	variable x_size
	variable y_size
	variable x_val
	variable y_val
	
	#	Set display size
	set x_size 500
	set y_size 400

	#	STAR MAGNITUDES labels
	set mag_text(0) {           Mag < 4.8}
	set mag_text(1) {4.8 <= Mag < 5.3 }
	set mag_text(2) {5.3 <= Mag < 5.7}
	set mag_text(3) {5.7 <= Mag < 6.0 }
	set mag_text(4) {6.0 <= Mag < 6.2 }
	set mag_text(5) {6.2 <= Mag < 6.3 }
	set mag_text(6) {6.3 <= Mag}

	#	Set ST FOV corners
	set x_val(st1_tlx) 50
	set y_val(st1_tly) 30
	set x_val(st1_brx) 150
	set y_val(st1_bry) 130
	set x_val(st1_title_x) 100
	set y_val(st1_title_y) 15
	set x_val(st1_hlbl_x) 40
	set y_val(st1_hlbl_y) 130
	set x_val(st1_vlbl_x) 150
	set y_val(st1_vlbl_y) 25
	
	set x_val(st2_tlx) 200
	set y_val(st2_tly) 30
	set x_val(st2_brx) 300
	set y_val(st2_bry) 130
	set x_val(st2_title_x) 250
	set y_val(st2_title_y) 15
	set x_val(st2_hlbl_x) 190
	set y_val(st2_hlbl_y) 130
	set x_val(st2_vlbl_x) 300
	set y_val(st2_vlbl_y) 25

	set x_val(st3_tlx) 350
	set y_val(st3_tly) 30
	set x_val(st3_brx) 450
	set y_val(st3_bry) 130
	set x_val(st3_title_x) 400
	set y_val(st3_title_y) 15
	set x_val(st3_hlbl_x) 340
	set y_val(st3_hlbl_y) 130
	set x_val(st3_vlbl_x) 450
	set y_val(st3_vlbl_y) 25
	
	#	STAR POSITIONS
	set x_val(asc_title_x) 50
	set y_val(asc_title_y) 170
	
	set x_val(osc_title_x) 100
	set y_val(osc_title_y) 170
	
	set x_val(am0_tlx) 50
	set y_val(am0_tly) 190
	set x_val(am0_brx) 56
	set y_val(am0_bry) 196
	set x_val(om0_tlx) 100
	set y_val(om0_tly) 190
	set x_val(om0_brx) 106
	set y_val(om0_bry) 196
	set x_val(m0_title_x) 135
	set y_val(m0_title_y) 193
	
	set x_val(am1_tlx) 50
	set y_val(am1_tly) 210
	set x_val(am1_brx) 56
	set y_val(am1_bry) 216
	set x_val(om1_tlx) 100
	set y_val(om1_tly) 210
	set x_val(om1_brx) 106
	set y_val(om1_bry) 216
	set x_val(m1_title_x) 135
	set y_val(m1_title_y) 213
	
	set x_val(am2_tlx) 50
	set y_val(am2_tly) 230
	set x_val(am2_brx) 56
	set y_val(am2_bry) 236
	set x_val(om2_tlx) 100
	set y_val(om2_tly) 230
	set x_val(om2_brx) 106
	set y_val(om2_bry) 236
	set x_val(m2_title_x) 135
	set y_val(m2_title_y) 233
	
	set x_val(am3_tlx) 50
	set y_val(am3_tly) 250
	set x_val(am3_brx) 56
	set y_val(am3_bry) 256
	set x_val(om3_tlx) 100
	set y_val(om3_tly) 250
	set x_val(om3_brx) 106
	set y_val(om3_bry) 256
	set x_val(m3_title_x) 135
	set y_val(m3_title_y) 253
	
	set x_val(am4_tlx) 50
	set y_val(am4_tly) 270
	set x_val(am4_brx) 56
	set y_val(am4_bry) 276
	set x_val(om4_tlx) 100
	set y_val(om4_tly) 270
	set x_val(om4_brx) 106
	set y_val(om4_bry) 276
	set x_val(m4_title_x) 135
	set y_val(m4_title_y) 273
	
	set x_val(am5_tlx) 50
	set y_val(am5_tly) 290
	set x_val(am5_brx) 56
	set y_val(am5_bry) 296
	set x_val(om5_tlx) 100
	set y_val(om5_tly) 290
	set x_val(om5_brx) 106
	set y_val(om5_bry) 296
	set x_val(m5_title_x) 135
	set y_val(m5_title_y) 293
	
	set x_val(am6_tlx) 50
	set y_val(am6_tly) 310
	set x_val(am6_brx) 56
	set y_val(am6_bry) 316
	set x_val(om6_tlx) 100
	set y_val(om6_tly) 310
	set x_val(om6_brx) 106
	set y_val(om6_bry) 316
	set x_val(m6_title_x) 135
	set y_val(m6_title_y) 313


	#	VT
	set x_val(vt_tlx) 300
	set y_val(vt_tly) 165
	set x_val(vt_brx) 310
	set y_val(vt_bry) 175
	set x_val(vt_num_x) 293
	set y_val(vt_num_y) 170
	set x_val(vt_bss_x) 305
	set y_val(vt_bss_y) 155
	set x_val(vt_mag_x) 315
	set y_val(vt_mag_y1) 165
	set y_val(vt_mag_y2) 175
	set x_val(vt_title_x) 355
	set y_val(vt_title_y1) 165
	set y_val(vt_title_y2) 175

	set x_val(invvt_tlx) 300
	set y_val(invvt_tly) 185
	set x_val(invvt_brx) 310
	set y_val(invvt_bry) 195
	set x_val(invvt_title_x) 355
	set y_val(invvt_title_y) 190

	set x_val(ntvt_tlx) 300
	set y_val(ntvt_tly) 205
	set x_val(ntvt_brx) 310
	set y_val(ntvt_bry) 215
	set x_val(ntvt_cntr_x) 305
	set y_val(ntvt_cntr_y) 210
	set x_val(ntvt_title_x) 355
	set y_val(ntvt_title_y) 210
	
	#	Centroid positions
	set x_val(cent_tlx) 300
	set y_val(cent_tly) 225
	set x_val(cent_brx) 310
	set y_val(cent_bry) 235
	set x_val(cent_title_x) 320
	set y_val(cent_title_y) 230
	
	#	AXIS POSITIONS
	set x_val(sky_x_tlx) 299
	set y_val(sky_x_tly) 244
	set x_val(sky_x_brx) 311
	set y_val(sky_x_bry) 256
	set x_val(sky_x_ctrx) 305
	set y_val(sky_x_ctry) 250
	set x_val(sky_x_title_x) 320
	set y_val(sky_x_title_y) 250

	set x_val(sky_y_tlx) 299
	set y_val(sky_y_tly) 264
	set x_val(sky_y_brx) 311
	set y_val(sky_y_bry) 276
	set x_val(sky_y_ctrx) 305
	set y_val(sky_y_ctry) 270
	set x_val(sky_y_title_x) 320
	set y_val(sky_y_title_y) 270

	set x_val(sky_z_tlx) 299
	set y_val(sky_z_tly) 284
	set x_val(sky_z_brx) 311
	set y_val(sky_z_bry) 296
	set x_val(sky_z_ctrx) 305
	set y_val(sky_z_ctry) 290
	set x_val(sky_z_title_x) 320
	set y_val(sky_z_title_y) 290

	set x_val(st_x_tlx) 305
	set y_val(st_x_tly) 310
	set x_val(st_x_brx) 325
	set y_val(st_x_bry) 310
	set x_val(st_x_ctrx) 305
	set y_val(st_x_ctry) 310
	set x_val(st_x_title_x) 330
	set y_val(st_x_title_y) 310

	set x_val(st_y_tlx) 305
	set y_val(st_y_tly) 330
	set x_val(st_y_brx) 325
	set y_val(st_y_bry) 330
	set x_val(st_y_ctrx) 305
	set y_val(st_y_ctry) 330
	set x_val(st_y_title_x) 330
	set y_val(st_y_title_y) 330

	set x_val(st_z_tlx) 305
	set y_val(st_z_tly) 350
	set x_val(st_z_brx) 325
	set y_val(st_z_bry) 350
	set x_val(st_z_ctrx) 305
	set y_val(st_z_ctry) 350
	set x_val(st_z_title_x) 330
	set y_val(st_z_title_y) 350

	#	North symbol
	set x_val(nsym_tlx) 298
	set y_val(nsym_tly) 363
	set x_val(nsym_brx) 311
	set y_val(nsym_bry) 376
	set x_val(nsym_ctrx) 305
	set y_val(nsym_ctry) 370

	set x_val(n_arr_tlx) 325
	set y_val(n_arr_tly) 370
	set x_val(n_arr_brx) 345
	set y_val(n_arr_bry) 370
	set x_val(n_arr_ctrx) 325
	set y_val(n_arr_ctry) 370

	set x_val(s_arr_ctrx) 345
	set y_val(s_arr_ctry) 370

	set x_val(ssym_tlx) 358
	set y_val(ssym_tly) 363
	set x_val(ssym_brx) 371
	set y_val(ssym_bry) 376
	set x_val(ssym_ctrx) 365
	set y_val(ssym_ctry) 370
	
	set x_val(north_title_x) 376
	set y_val(north_title_y) 370
	
}

########################################################################
#	GENERATE LEGEND
#	legend parent defaults to none/top.
#	allows standalone testing as well as notebook setup.

proc ::LEGEND::legend_gui { } {
	variable lc
	variable p
	variable x_size
	variable y_size
	variable x_val
	variable y_val
	variable mag_text


	catch {destroy .legend_ui}
	toplevel .legend_ui
	wm title .legend_ui "SKYMAP / ST LEGEND"
	wm geometry .legend_ui "-0-0"
	set pf .legend_ui.top
	frame $pf
	pack $pf

	set lc [canvas $pf.leg -background black \
			       -width $x_size \
			       -height $y_size]
	pack $lc -side left

	#	STAR TRACKER FOVS

	global gi


	foreach st {1 2 3} {
	    set vcolor [$gi ST_$st\_FOV_COLOR green]
	    set hcolor [$gi ST_H_COLOR_AXIS white]
	    
	    $lc create line \
	               $x_val(st$st\_brx) \
	               $y_val(st$st\_bry) \
	               $x_val(st$st\_tlx) \
	               $y_val(st$st\_bry) \
	               -fill $hcolor \
	               -arrow last

	    $lc create line \
	               $x_val(st$st\_tlx) \
	               $y_val(st$st\_bry) \
	               $x_val(st$st\_tlx) \
	               $y_val(st$st\_tly) \
	               -fill $vcolor 

	    $lc create line \
	               $x_val(st$st\_tlx) \
	               $y_val(st$st\_tly) \
	               $x_val(st$st\_brx) \
	               $y_val(st$st\_tly) \
	               -fill $vcolor 

	    $lc create line \
	               $x_val(st$st\_brx) \
	               $y_val(st$st\_tly) \
	               $x_val(st$st\_brx) \
	               $y_val(st$st\_bry) \
	               -fill $vcolor \
	               -arrow first


	#	title
	    $lc create text \
	               $x_val(st$st\_title_x) \
	               $y_val(st$st\_title_y) \
	               -text "ST $st FOV" \
	               -fill white \
	               -anchor c \
	               -font $::CONFIG::fonts(label)

	#	axis label
	    $lc create text \
	               $x_val(st$st\_hlbl_x) \
	               $y_val(st$st\_hlbl_y) \
	               -text "+H" \
	               -fill white \
	               -anchor c \
	               -font $::CONFIG::fonts(label)
	               
	    $lc create text \
	               $x_val(st$st\_vlbl_x) \
	               $y_val(st$st\_vlbl_y) \
	               -text "+V" \
	               -fill white \
	               -anchor c \
	               -font $::CONFIG::fonts(label)
	              
	}
	
	#	Display the star legend
	#	First do ASC / OSC

	#	titles
	$lc create text \
	               $x_val(asc_title_x) \
	               $y_val(asc_title_y) \
	               -text "ASC" \
	               -fill white \
	               -anchor c \
	               -font $::CONFIG::fonts(label)
	               
	$lc create text \
	               $x_val(osc_title_x) \
	               $y_val(osc_title_y) \
	               -text "OSC" \
	               -fill white \
	               -anchor c \
	               -font $::CONFIG::fonts(label)
	               
	
	#	Stars
	foreach i {0 1 2 3 4 5 6} {
	    $lc create oval \
	               $x_val(am$i\_tlx) \
	               $y_val(am$i\_tly) \
	               $x_val(am$i\_brx) \
	               $y_val(am$i\_bry) \
	               -outline $::DISPLAY::star_colors($i)
	               
	    $lc create oval \
	               $x_val(om$i\_tlx) \
	               $y_val(om$i\_tly) \
	               $x_val(om$i\_brx) \
	               $y_val(om$i\_bry) \
	               -fill $::DISPLAY::star_colors($i) \
	               -outline $::DISPLAY::star_colors($i)


	#	title
	    $lc create text \
	               $x_val(m$i\_title_x) \
	               $y_val(m$i\_title_y) \
	               -text $mag_text($i) \
	               -fill $::DISPLAY::star_colors($i) \
	               -anchor w \
	               -justify right \
	               -font $::CONFIG::fonts(label)
	               
	}

	#	VT definition
	$lc create line \
	               $x_val(vt_brx) \
	               $y_val(vt_bry) \
	               $x_val(vt_tlx) \
	               $y_val(vt_bry) \
	               -fill white 

	$lc create line \
	               $x_val(vt_tlx) \
	               $y_val(vt_bry) \
	               $x_val(vt_tlx) \
	               $y_val(vt_tly) \
	               -fill white 

	$lc create line \
	               $x_val(vt_tlx) \
	               $y_val(vt_tly) \
	               $x_val(vt_brx) \
	               $y_val(vt_tly) \
	               -fill white 

	$lc create line \
	               $x_val(vt_brx) \
	               $y_val(vt_tly) \
	               $x_val(vt_brx) \
	               $y_val(vt_bry) \
	               -fill white

	#	vt number label
	$lc create text \
	               $x_val(vt_num_x) \
	               $y_val(vt_num_y) \
	               -text "VT #" \
	               -fill white \
	               -anchor e \
	               -justify right \
	               -font $::CONFIG::fonts(label)

	#	Star label
	$lc create text \
	               $x_val(vt_bss_x) \
	               $y_val(vt_bss_y) \
	               -text "Star ID" \
	               -fill white \
	               -anchor c \
	               -justify center \
	               -font $::CONFIG::fonts(label)

	#	Magnitude label, mag > saturation
	$lc create text \
	               $x_val(vt_mag_x) \
	               $y_val(vt_mag_y1) \
	               -text "Mag" \
	               -fill white \
	               -anchor w \
	               -justify left \
	               -font $::CONFIG::fonts(label)
	               
	#	Magnitude label, mag < stauration 
	$lc create text \
	               $x_val(vt_mag_x) \
	               $y_val(vt_mag_y2) \
	               -text "Mag" \
	               -fill yellow \
	               -anchor w \
	               -justify left \
	               -font $::CONFIG::fonts(label)

	#	VT title, mag > saturation

    set mag_sat $::GSSINI::gssini(MAG_SATURATION)

	$lc create text \
	               $x_val(vt_title_x) \
	               $y_val(vt_title_y1) \
	               -text "VT with Mag >= [set mag_sat]" \
	               -fill white \
	               -anchor w \
	               -justify left \
	               -font $::CONFIG::fonts(label)

	#	VT title, mag < saturation
	$lc create text \
	               $x_val(vt_title_x) \
	               $y_val(vt_title_y2) \
	               -text "VT with Mag <   [set mag_sat]" \
	               -fill yellow \
	               -anchor w \
	               -justify left \
	               -font $::CONFIG::fonts(label)

	#	INVALID VT
	$lc create line \
	               $x_val(invvt_brx) \
	               $y_val(invvt_bry) \
	               $x_val(invvt_tlx) \
	               $y_val(invvt_bry) \
	               -fill white 

	$lc create line \
	               $x_val(invvt_tlx) \
	               $y_val(invvt_bry) \
	               $x_val(invvt_tlx) \
	               $y_val(invvt_tly) \
	               -fill white 

	$lc create line \
	               $x_val(invvt_tlx) \
	               $y_val(invvt_tly) \
	               $x_val(invvt_brx) \
	               $y_val(invvt_tly) \
	               -fill white 

	$lc create line \
	               $x_val(invvt_brx) \
	               $y_val(invvt_tly) \
	               $x_val(invvt_brx) \
	               $y_val(invvt_bry) \
	               -fill white

	$lc create line \
	               $x_val(invvt_tlx) \
	               $y_val(invvt_tly) \
	               $x_val(invvt_brx) \
	               $y_val(invvt_bry) \
	               -fill white 

	$lc create line \
	               $x_val(invvt_brx) \
	               $y_val(invvt_tly) \
	               $x_val(invvt_tlx) \
	               $y_val(invvt_bry) \
	               -fill white

	#	Invalid VT title
	$lc create text \
	               $x_val(invvt_title_x) \
	               $y_val(invvt_title_y) \
	               -text "Invalid VT" \
	               -fill white \
	               -anchor w \
	               -justify left \
	               -font $::CONFIG::fonts(label)



	#	Non-Tracking VT
	$lc create line \
	               $x_val(ntvt_brx) \
	               $y_val(ntvt_bry) \
	               $x_val(ntvt_tlx) \
	               $y_val(ntvt_bry) \
	               -fill white 

	$lc create line \
	               $x_val(ntvt_tlx) \
	               $y_val(ntvt_bry) \
	               $x_val(ntvt_tlx) \
	               $y_val(ntvt_tly) \
	               -fill white 

	$lc create line \
	               $x_val(ntvt_tlx) \
	               $y_val(ntvt_tly) \
	               $x_val(ntvt_brx) \
	               $y_val(ntvt_tly) \
	               -fill white 

	$lc create line \
	               $x_val(ntvt_brx) \
	               $y_val(ntvt_tly) \
	               $x_val(ntvt_brx) \
	               $y_val(ntvt_bry) \
	               -fill white

	#	Non-tracking VT number
	$lc create text \
	               $x_val(ntvt_cntr_x) \
	               $y_val(ntvt_cntr_y) \
	               -text # \
	               -fill white \
	               -anchor c \
	               -justify center \
	               -font $::CONFIG::fonts(label)

	#	Non-tracking VT title
	$lc create text \
	               $x_val(ntvt_title_x) \
	               $y_val(ntvt_title_y) \
	               -text "Non-Tracking VT" \
	               -fill white \
	               -anchor w \
	               -justify left \
	               -font $::CONFIG::fonts(label)



	#	Centroid
	set iniclr [$gi CENTROID_COLOR white]
	set arc "$lc create arc $x_val(cent_tlx) \
	                        $y_val(cent_tly) \
	                        $x_val(cent_brx) \
	                        $y_val(cent_bry)"
	set opts [list -extent 90.0 \
	               -style pieslice \
	               -outline $iniclr \
	               -tags st_vt]
	set angs { 0.0 90.0 180.0 270.0 }
	set colors " black $iniclr black $iniclr "
	
	foreach {ang} $angs {color} $colors {
		eval "$arc -start $ang -fill $color $opts"
	}

	#	title
	$lc create text \
	               $x_val(cent_title_x) \
	               $y_val(cent_title_y) \
	               -text "Centroid of Tracking VTs" \
	               -fill white \
	               -anchor w \
	               -font $::CONFIG::fonts(label)


	#	S/C body axis in skymap
	foreach axis {x y z} {
	    set uc [string toupper $axis]
	    
	    $lc create oval \
	               $x_val(sky_$axis\_tlx) \
	               $y_val(sky_$axis\_tly) \
	               $x_val(sky_$axis\_brx) \
	               $y_val(sky_$axis\_bry) \
	               -outline white

	#	Axis label
	    $lc create text \
	               $x_val(sky_$axis\_ctrx) \
	               $y_val(sky_$axis\_ctry) \
	               -text $uc \
	               -fill white \
	               -anchor c \
	               -justify center \
	               -font $::CONFIG::fonts(label)

	#	Title
	    $lc create text \
	               $x_val(sky_$axis\_title_x) \
	               $y_val(sky_$axis\_title_y) \
	               -text "S/C $uc Axis" \
	               -fill white \
	               -anchor w \
	               -justify left \
	               -font $::CONFIG::fonts(label)

	}	               

	#	S/C body axis in ST
	foreach axis {x y z} {
	    set uc [string toupper $axis]
	    
	    $lc create line \
	               $x_val(st_$axis\_tlx) \
	               $y_val(st_$axis\_tly) \
	               $x_val(st_$axis\_brx) \
	               $y_val(st_$axis\_bry) \
	               -fill white \
	               -arrow first

	#	Axis label
	    $lc create text \
	               $x_val(st_$axis\_ctrx) \
	               $y_val(st_$axis\_ctry) \
	               -text $uc \
	               -fill white \
	               -anchor c \
	               -justify center \
	               -font $::CONFIG::fonts(wtitle)

	#	Axis Title
	    $lc create text \
	               $x_val(st_$axis\_title_x) \
	               $y_val(st_$axis\_title_y) \
	               -text "S/C $uc Axis Projection" \
	               -fill white \
	               -anchor w \
	               -justify left \
	               -font $::CONFIG::fonts(label)

	}	               


	#	North Pole Symbols
	foreach pole {n s} {
	    set uc [string toupper $pole]
	    
	    $lc create oval \
	               $x_val($pole\sym_tlx) \
	               $y_val($pole\sym_tly) \
	               $x_val($pole\sym_brx) \
	               $y_val($pole\sym_bry) \
	               -outline white 

	#	Pole label
	    $lc create text \
	               $x_val($pole\sym_ctrx) \
	               $y_val($pole\sym_ctry) \
	               -text $uc \
	               -fill white \
	               -anchor c \
	               -justify center \
	               -font $::CONFIG::fonts(wtitle)


	#	 Pole Symbol for arrow
	    $lc create text \
	               $x_val($pole\_arr_ctrx) \
	               $y_val($pole\_arr_ctry) \
	               -text $uc \
	               -fill white \
	               -anchor c \
	               -justify center \
	               -font $::CONFIG::fonts(wtitle)

	}	               

	#	North Pole  arrow
	    $lc create line \
	               $x_val(n_arr_tlx) \
	               $y_val(n_arr_tly) \
	               $x_val(n_arr_brx) \
	               $y_val(n_arr_bry) \
	               -fill white \
	               -arrow first \

	#	North Pole Title
	    $lc create text \
	               $x_val(north_title_x) \
	               $y_val(north_title_y) \
	               -text "Pole, North Arrow, Pole" \
	               -fill white \
	               -anchor w \
	               -justify left \
	               -font $::CONFIG::fonts(label)

		
	#	Add the OK button at the bottom
	set btn [frame .legend_ui.butn]
	pack $btn -side bottom \
		  -anchor c \
		  -ipadx 10 \
		  -fill x

	frame $btn.sep -borderwidth 1 \
		       -height 2 \
		       -relief raised
	pack $btn.sep -side top \
		      -fill x \
		      -pady 2
		      
	button $btn.ok -text OK \
		       -font $::CONFIG::fonts(label) \
		       -relief raised \
		       -command "destroy .legend_ui; update idletasks"

	pack $btn.ok -side top \
		     -anchor c	\
		     -pady 5
	    
}

########################################################################
#	$Log: $
########################################################################
