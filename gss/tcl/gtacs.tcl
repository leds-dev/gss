########################################################################
#	$Id:  $
########################################################################

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ::GTACS:: {
	variable lrv_idx
	variable dlrv_idx
	variable lrvs
	variable dlrvs
	variable first 1

	namespace export init read_tm

}

########################################################################
#	map INI keys to numeric index.
#	lists are over 100 items each.  Sourced in separate files.
#	not invoked unless mode is already good.

proc ::GTACS::init { } {
	variable ace_code

	#	load (D)LRV indices
	::GTACS::set_lrv_idx
	::GTACS::set_dlrv_idx

	#	get ACE prefix for LRVs.  DLRVs don't have any.
	set ace_code "S$::CONFIG::data(ace)"

	::MSG_DLG::progress "stream $::CONFIG::stream"
	::GTACS::setup $::CONFIG::stream

}


########################################################################
#	get the LRV's floating point value returned from TLM

proc ::GTACS::float_lrv_value { id } {
    set err 1
    set i 1
    while { ($err == 1) && ($i <= 3) } {
        set err 0
        set data [gss_get_lrv_value "sim_tm" $id]
    
        set eidx [string first " " $data]
    
        set dummy_f [string range $data 0 $eidx]
    
        set err [catch {expr 0.0 + $dummy_f}]
 
        incr i
 
    }

    if {$err == 1} {
        ::MSG_DLG::attention "float_lrv_value error: LRV $id returned \"$dummy_f\" from \"$data\" " \
                              red \
                              black \
                              -INSTRUCTION \
                              "Hit OK to clear error window"
        return 0.0
    }
            
    return $dummy_f
    
}



########################################################################
#	get the LRV's integer point value returned from TLM

proc ::GTACS::int_lrv_value { id } {

    set err 1
    set i 1
    while { ($err == 1) && ($i <= 3) } {
        set data [gss_get_lrv_value "sim_tm" $id]
   
        set sidx [string first " " $data]
        incr sidx
        set eidx [string first " " $data $sidx]
    
        set dummy_i [string range $data $sidx $eidx]
    
        set err [catch {expr 0 + $dummy_i}]
 
        incr i
 
    }

    if {$err == 1} {
        ::MSG_DLG::attention "int_lrv_value error: LRV $id returned \"$dummy_i\" from \"$data\" " \
                              red \
                              black \
                              -INSTRUCTION \
                              "Hit OK to clear error window"
        return 0
            
    }
 
    return $dummy_i
    
}



########################################################################
#	get the LRV's status (string) value returned from TLM

proc ::GTACS::str_lrv_value { id } {
    set data [gss_get_lrv_value "sim_tm" $id]
    
    set sidx [string last " " $data]
    incr sidx
    set eidx [string length $data]
    
    set dummy_s [string range $data $sidx $eidx]
    
    return $dummy_s
   
}

########################################################################
#	initialize server, and pass all (D)LRV info to it.

proc ::GTACS::setup { server } {
	variable lrv_idx
	variable dlrv_idx
	variable ace_code

	set err [gss_gtacs_connect "$server"]

	if {$err != 0} {
	    ::MSG_DLG::attention "Error from gss_gtacs_connect $err" \
	                         red \
	                         black 
	                         
	}

	set key_list [array names ::GSSINI::gssini *_LRV]
	foreach key $key_list {
		Set_gtacs_lrv_rate	\
		$::GTACS::lrv_idx($key) [lindex $::GSSINI::gssini($key) 1] \
		[lindex $::GSSINI::gssini($key) 2]

	}


	set offset 0.0
	Set_GSS_TimeOffset $offset

	foreach key $key_list {
		if {[string first "TIME" $key] != -1} {
			set err [gss_gtacs_register_lrv "FRAME_TIME" \
				$::GTACS::lrv_idx($key)]
	
			if {$err != 0} {
			    ::MSG_DLG::attention \
			      "Error from gss_gtacs_register_lrv $err" \
			      red \
			      black
			      break
	 		}

		} elseif {[string first "MNF_CNT" $key] != -1} {
			set err [gss_gtacs_register_mfc_lrv		\
					[lindex $::GSSINI::gssini($key) 0] \
						$::GTACS::lrv_idx($key)]

			if {$err != 0} {
			    ::MSG_DLG::attention \
			      "Error from gss_gtacs_register_mfc_lrv $err" \
			      red \
			      black
			      break
			}

		} elseif {[string first "GSS" $key] != -1} {
			set err [gss_gtacs_register_lrv		\
					[lindex $::GSSINI::gssini($key) 0] \
						$::GTACS::lrv_idx($key)]

			if {$err != 0} {
			    ::MSG_DLG::attention \
			      "Error from gss_gtacs_register_lrv $err" \
			      red \
			      black
			      break
			}

		} elseif {[string first "SPACECRAFT" $key] != -1} {
			set err [gss_gtacs_register_lrv		\
					[lindex $::GSSINI::gssini($key) 0] \
						$::GTACS::lrv_idx($key)]

			if {$err != 0} {
			    ::MSG_DLG::attention \
			      "Error from gss_gtacs_register_lrv $err" \
			      red \
			      black
			      break
			}

		} else {
		
			set lrv [lindex $::GSSINI::gssini($key) 0]
			

			if {[lindex $::GSSINI::gssini(MST_LRV_MNEMONICS) 0] != "1"} {
			    set lrv "$ace_code\_$lrv"
			    
			}
	
			set err [gss_gtacs_register_lrv	\
					$lrv $::GTACS::lrv_idx($key)]

			if {$err != 0} {
			    ::MSG_DLG::attention \
			      "Error from gss_gtacs_register_lrv $err" \
			      red \
			      black
			      break
			}

		}
	}


#
###     Call gss_gtacs_read_tm once to acquire all of the LRV pid's at the
###     C level so that further calls to gss_gtacs_read_tm get data.
#
	set err [gss_gtacs_read_tm]

	if {$err != 0} {
	    ::MSG_DLG::attention \
			"Error for stream $::CONFIG::stream from gss_gtacs_read_tm $err" \
			red \
			black \
			-LABEL $err	    
	}

}

########################################################################
#	invoke GTACS TM reader

proc ::GTACS::read_tm { } {
    set zero "0"
    
    set err [gss_gtacs_read_tm]
    if {$err != 0} {
        ::MSG_DLG::attention "Error for stream $::CONFIG::stream from gss_gtacs_read_tm $err" \
                             red \
                             black \
                             -LABEL $err
    }

    #	Update the TCL variables that drive the status screens
    set data [GetTM_ECI2BodyQuat]
    set ::USER_QUAT::tlm_quat(0) [format "%9.7f" [lindex $data 0]]
    set ::USER_QUAT::tlm_quat(1) [format "%9.7f" [lindex $data 1]]
    set ::USER_QUAT::tlm_quat(2) [format "%9.7f" [lindex $data 2]]
    set ::USER_QUAT::tlm_quat(3) [format "%9.7f" [lindex $data 3]]

    set key YAW_FLIP_LRV
    set status [::GTACS::str_lrv_value $::GTACS::lrv_idx($key)]
                            
    if {[string toupper [string index $status 0]] == "I"} {
        set ::CONFIG::data(yaw_flip) "Inv"
                    
    } else {
        set ::CONFIG::data(yaw_flip) "Upr"  
        
    }    

    set key SC_EPHEMERIS_VALIDITY_LRV
    set status [::GTACS::str_lrv_value $::GTACS::lrv_idx($key)]
                            
    if {[string toupper [string index $status 0]] == "I"} {
        set ::CONFIG::data(valid_ephem) "INVALID"
                    
    } else {
        set ::CONFIG::data(valid_ephem) "VALID"  
        
    }    


    set key GSS_OUT_ENABLED_IPA_LRV
    set status [string trim [::GTACS::float_lrv_value $::GTACS::lrv_idx($key)]]

    while {[string index $status 6] != "."} {
        set status "0$status"

    }
    
    while {[string length $status] != 13} {
        set status "$status$zero"
        
    }

    set a [string range $status 0 2]
    set b [string range $status 3 5]
    set c [string range $status 7 9]
    set d [string range $status 10 12]

    set ::CONFIG::data(tlm_control_ip)  "$a.$b.$c.$d"      

    set key GSS_OUT_ENABLED_PID_LRV
    set x [::GTACS::float_lrv_value $::GTACS::lrv_idx($key)]
    set ::CONFIG::data(tlm_control_pid) [expr int($x)]

}

########################################################################
#	define lrv indicies

proc ::GTACS::set_lrv_idx { } {
	variable lrv_idx

	set lrv_idx(ST_1_VT_1_H_LRV) 0
	set lrv_idx(ST_1_VT_1_V_LRV) 1
	set lrv_idx(ST_1_VT_1_MAG_LRV) 2
	set lrv_idx(ST_1_VT_1_OSC_LRV) 3
	set lrv_idx(ST_1_VT_1_VALIDITY_LRV) 4
	set lrv_idx(ST_1_VT_1_TRACK_LRV) 5
	set lrv_idx(ST_1_VT_2_H_LRV) 6
	set lrv_idx(ST_1_VT_2_V_LRV) 7
	set lrv_idx(ST_1_VT_2_MAG_LRV) 8
	set lrv_idx(ST_1_VT_2_OSC_LRV) 9
	set lrv_idx(ST_1_VT_2_VALIDITY_LRV) 10
	set lrv_idx(ST_1_VT_2_TRACK_LRV) 11
	set lrv_idx(ST_1_VT_3_H_LRV) 12
	set lrv_idx(ST_1_VT_3_V_LRV) 13
	set lrv_idx(ST_1_VT_3_MAG_LRV) 14
	set lrv_idx(ST_1_VT_3_OSC_LRV) 15
	set lrv_idx(ST_1_VT_3_VALIDITY_LRV) 16
	set lrv_idx(ST_1_VT_3_TRACK_LRV) 17
	set lrv_idx(ST_1_VT_4_H_LRV) 18
	set lrv_idx(ST_1_VT_4_V_LRV) 19
	set lrv_idx(ST_1_VT_4_MAG_LRV) 20
	set lrv_idx(ST_1_VT_4_OSC_LRV) 21
	set lrv_idx(ST_1_VT_4_VALIDITY_LRV) 22
	set lrv_idx(ST_1_VT_4_TRACK_LRV) 23
	set lrv_idx(ST_1_VT_5_H_LRV) 24
	set lrv_idx(ST_1_VT_5_V_LRV) 25
	set lrv_idx(ST_1_VT_5_MAG_LRV) 26
	set lrv_idx(ST_1_VT_5_OSC_LRV) 27
	set lrv_idx(ST_1_VT_5_VALIDITY_LRV) 28
	set lrv_idx(ST_1_VT_5_TRACK_LRV) 29
	set lrv_idx(ST_2_VT_1_H_LRV) 30
	set lrv_idx(ST_2_VT_1_V_LRV) 31
	set lrv_idx(ST_2_VT_1_MAG_LRV) 32
	set lrv_idx(ST_2_VT_1_OSC_LRV) 33
	set lrv_idx(ST_2_VT_1_VALIDITY_LRV) 34
	set lrv_idx(ST_2_VT_1_TRACK_LRV) 35
	set lrv_idx(ST_2_VT_2_H_LRV) 36
	set lrv_idx(ST_2_VT_2_V_LRV) 37
	set lrv_idx(ST_2_VT_2_MAG_LRV) 38
	set lrv_idx(ST_2_VT_2_OSC_LRV) 39
	set lrv_idx(ST_2_VT_2_VALIDITY_LRV) 40
	set lrv_idx(ST_2_VT_2_TRACK_LRV) 41
	set lrv_idx(ST_2_VT_3_H_LRV) 42
	set lrv_idx(ST_2_VT_3_V_LRV) 43
	set lrv_idx(ST_2_VT_3_MAG_LRV) 44
	set lrv_idx(ST_2_VT_3_OSC_LRV) 45
	set lrv_idx(ST_2_VT_3_VALIDITY_LRV) 46
	set lrv_idx(ST_2_VT_3_TRACK_LRV) 47
	set lrv_idx(ST_2_VT_4_H_LRV) 48
	set lrv_idx(ST_2_VT_4_V_LRV) 49
	set lrv_idx(ST_2_VT_4_MAG_LRV) 50
	set lrv_idx(ST_2_VT_4_OSC_LRV) 51
	set lrv_idx(ST_2_VT_4_VALIDITY_LRV) 52
	set lrv_idx(ST_2_VT_4_TRACK_LRV) 53
	set lrv_idx(ST_2_VT_5_H_LRV) 54
	set lrv_idx(ST_2_VT_5_V_LRV) 55
	set lrv_idx(ST_2_VT_5_MAG_LRV) 56
	set lrv_idx(ST_2_VT_5_OSC_LRV) 57
	set lrv_idx(ST_2_VT_5_VALIDITY_LRV) 58
	set lrv_idx(ST_2_VT_5_TRACK_LRV) 59
	set lrv_idx(ST_3_VT_1_H_LRV) 60
	set lrv_idx(ST_3_VT_1_V_LRV) 61
	set lrv_idx(ST_3_VT_1_MAG_LRV) 62
	set lrv_idx(ST_3_VT_1_OSC_LRV) 63
	set lrv_idx(ST_3_VT_1_VALIDITY_LRV) 64
	set lrv_idx(ST_3_VT_1_TRACK_LRV) 65
	set lrv_idx(ST_3_VT_2_H_LRV) 66
	set lrv_idx(ST_3_VT_2_V_LRV) 67
	set lrv_idx(ST_3_VT_2_MAG_LRV) 68
	set lrv_idx(ST_3_VT_2_OSC_LRV) 69
	set lrv_idx(ST_3_VT_2_VALIDITY_LRV) 70
	set lrv_idx(ST_3_VT_2_TRACK_LRV) 71
	set lrv_idx(ST_3_VT_3_H_LRV) 72
	set lrv_idx(ST_3_VT_3_V_LRV) 73
	set lrv_idx(ST_3_VT_3_MAG_LRV) 74
	set lrv_idx(ST_3_VT_3_OSC_LRV) 75
	set lrv_idx(ST_3_VT_3_VALIDITY_LRV) 76
	set lrv_idx(ST_3_VT_3_TRACK_LRV) 77
	set lrv_idx(ST_3_VT_4_H_LRV) 78
	set lrv_idx(ST_3_VT_4_V_LRV) 79
	set lrv_idx(ST_3_VT_4_MAG_LRV) 80
	set lrv_idx(ST_3_VT_4_OSC_LRV) 81
	set lrv_idx(ST_3_VT_4_VALIDITY_LRV) 82
	set lrv_idx(ST_3_VT_4_TRACK_LRV) 83
	set lrv_idx(ST_3_VT_5_H_LRV) 84
	set lrv_idx(ST_3_VT_5_V_LRV) 85
	set lrv_idx(ST_3_VT_5_MAG_LRV) 86
	set lrv_idx(ST_3_VT_5_OSC_LRV) 87
	set lrv_idx(ST_3_VT_5_VALIDITY_LRV) 88
	set lrv_idx(ST_3_VT_5_TRACK_LRV) 89
	set lrv_idx(ST_1_SMS_ON_OFF_LRV) 90
	set lrv_idx(ST_1_SMS_MODE_LRV) 91
	set lrv_idx(ST_2_SMS_ON_OFF_LRV) 92
	set lrv_idx(ST_2_SMS_MODE_LRV) 93
	set lrv_idx(ST_3_SMS_ON_OFF_LRV) 94
	set lrv_idx(ST_3_SMS_MODE_LRV) 95
	set lrv_idx(SC_ECI_TO_BODY_QUAT_1_LRV) 96
	set lrv_idx(SC_ECI_TO_BODY_QUAT_2_LRV) 97
	set lrv_idx(SC_ECI_TO_BODY_QUAT_3_LRV) 98
	set lrv_idx(SC_ECI_TO_BODY_QUAT_4_LRV) 99
	set lrv_idx(SC_EPHEMERIS_1_LRV) 100
	set lrv_idx(SC_EPHEMERIS_2_LRV) 101
	set lrv_idx(SC_EPHEMERIS_3_LRV) 102
	set lrv_idx(SC_EPHEMERIS_4_LRV) 103
	set lrv_idx(SC_EPHEMERIS_5_LRV) 104
	set lrv_idx(SC_EPHEMERIS_6_LRV) 105
	set lrv_idx(SC_EPHEMERIS_7_LRV) 106
	set lrv_idx(SC_EPHEMERIS_VALIDITY_LRV) 107
	set lrv_idx(FRAME_TIME_LRV) 108
	set lrv_idx(YAW_FLIP_LRV) 109
	set lrv_idx(MNF_CNT_LRV) 110
	set lrv_idx(GSS_OUT_ENABLED_IPA_LRV) 111
	set lrv_idx(GSS_OUT_ENABLED_PID_LRV) 112
	set lrv_idx(ST_1_STP_ON_OFF_LRV) 113 
	set lrv_idx(ST_2_STP_ON_OFF_LRV) 114 
	set lrv_idx(ST_3_STP_ON_OFF_LRV) 115 
	set lrv_idx(ACE_CLOCK_SECS_LRV) 116 
	set lrv_idx(ACE_CLOCK_DAYS_LRV) 117 
	set lrv_idx(SPACECRAFT_ID_LRV) 118
	set lrv_idx(STAR_CAT_STRP_WIDTH_LRV) 119
	set lrv_idx(ST_1_BACKGROUND_LRV) 120
	set lrv_idx(ST_2_BACKGROUND_LRV) 121
	set lrv_idx(ST_3_BACKGROUND_LRV) 122
	}

########################################################################
#	define Derived LRV indicies

proc ::GTACS::set_dlrv_idx { } {
	variable dlrv_idx

	#	GSS_QUAT_CMD_x is a dimensionless real number in range [-1,1]
	#	Indicates the Ref_2_Bdy quaternion at the end of the slew
	#	Ref will be ECI, or some (unknown) offset from ECI
	#	Outputs for Slew Command (scmd 41)
	#	Will be used for sunline slews, and slews from earth to Sun
	#	All other slews (Sun to Earth, Magnetometer Cal, Yaw Flip)
	#	  are pre-canned

	set dlrv_idx(GSS_QUAT_CMD_1_MNEM) 0
	set dlrv_idx(GSS_QUAT_CMD_2_MNEM) 1
	set dlrv_idx(GSS_QUAT_CMD_3_MNEM) 2
	set dlrv_idx(GSS_QUAT_CMD_4_MNEM) 3
 
########################################################################
#	Outputs for Star Tracker Search Command (scmd A5)
#	Will be used to get STs already tracking OSC stars before
#	  switching to SIAD
#	GSS_STx_VTy_DES_H	Real	Indicates desired H for STx/VTy in deg
#	GSS_STx_VTy_DES_V	Real	Indicates desired V for STx/VTy in deg
#	GSS_STx_VTy_DES_MAG	Real	Indicates desired Instr Mag for STx/VTy
#	GSS_STx_VTy_DES_OSC	Bool	0 = STx/VTy currently off desired star
#					1 = STx/VTy currently on desired star
#
	set dlrv_idx(GSS_ST1_VT1_DES_H_MNEM) 4
	set dlrv_idx(GSS_ST1_VT1_DES_V_MNEM) 5
	set dlrv_idx(GSS_ST1_VT1_DES_MAG_MNEM) 6
	set dlrv_idx(GSS_ST1_VT1_DES_OSC_MNEM) 7
	set dlrv_idx(GSS_ST1_VT2_DES_H_MNEM) 8
	set dlrv_idx(GSS_ST1_VT2_DES_V_MNEM) 9
	set dlrv_idx(GSS_ST1_VT2_DES_MAG_MNEM) 10
	set dlrv_idx(GSS_ST1_VT2_DES_OSC_MNEM) 11
	set dlrv_idx(GSS_ST1_VT3_DES_H_MNEM) 12
	set dlrv_idx(GSS_ST1_VT3_DES_V_MNEM) 13
	set dlrv_idx(GSS_ST1_VT3_DES_MAG_MNEM) 14
	set dlrv_idx(GSS_ST1_VT3_DES_OSC_MNEM) 15
	set dlrv_idx(GSS_ST1_VT4_DES_H_MNEM) 16
	set dlrv_idx(GSS_ST1_VT4_DES_V_MNEM) 17
	set dlrv_idx(GSS_ST1_VT4_DES_MAG_MNEM) 18
	set dlrv_idx(GSS_ST1_VT4_DES_OSC_MNEM) 19
	set dlrv_idx(GSS_ST1_VT5_DES_H_MNEM) 20
	set dlrv_idx(GSS_ST1_VT5_DES_V_MNEM) 21
	set dlrv_idx(GSS_ST1_VT5_DES_MAG_MNEM) 22
	set dlrv_idx(GSS_ST1_VT5_DES_OSC_MNEM) 23
#
	set dlrv_idx(GSS_ST2_VT1_DES_H_MNEM) 24
	set dlrv_idx(GSS_ST2_VT1_DES_V_MNEM) 25
	set dlrv_idx(GSS_ST2_VT1_DES_MAG_MNEM) 26
	set dlrv_idx(GSS_ST2_VT1_DES_OSC_MNEM) 27
	set dlrv_idx(GSS_ST2_VT2_DES_H_MNEM) 28
	set dlrv_idx(GSS_ST2_VT2_DES_V_MNEM) 29
	set dlrv_idx(GSS_ST2_VT2_DES_MAG_MNEM) 30
	set dlrv_idx(GSS_ST2_VT2_DES_OSC_MNEM) 31
	set dlrv_idx(GSS_ST2_VT3_DES_H_MNEM) 32
	set dlrv_idx(GSS_ST2_VT3_DES_V_MNEM) 33
	set dlrv_idx(GSS_ST2_VT3_DES_MAG_MNEM) 34
	set dlrv_idx(GSS_ST2_VT3_DES_OSC_MNEM) 35
	set dlrv_idx(GSS_ST2_VT4_DES_H_MNEM) 36
	set dlrv_idx(GSS_ST2_VT4_DES_V_MNEM) 37
	set dlrv_idx(GSS_ST2_VT4_DES_MAG_MNEM) 38
	set dlrv_idx(GSS_ST2_VT4_DES_OSC_MNEM) 39
	set dlrv_idx(GSS_ST2_VT5_DES_H_MNEM) 40
	set dlrv_idx(GSS_ST2_VT5_DES_V_MNEM) 41
	set dlrv_idx(GSS_ST2_VT5_DES_MAG_MNEM) 42
	set dlrv_idx(GSS_ST2_VT5_DES_OSC_MNEM) 43
#
	set dlrv_idx(GSS_ST3_VT1_DES_H_MNEM) 44
	set dlrv_idx(GSS_ST3_VT1_DES_V_MNEM) 45
	set dlrv_idx(GSS_ST3_VT1_DES_MAG_MNEM) 46
	set dlrv_idx(GSS_ST3_VT1_DES_OSC_MNEM) 47
	set dlrv_idx(GSS_ST3_VT2_DES_H_MNEM) 48
	set dlrv_idx(GSS_ST3_VT2_DES_V_MNEM) 49
	set dlrv_idx(GSS_ST3_VT2_DES_MAG_MNEM) 50
	set dlrv_idx(GSS_ST3_VT2_DES_OSC_MNEM) 51
	set dlrv_idx(GSS_ST3_VT3_DES_H_MNEM) 52
	set dlrv_idx(GSS_ST3_VT3_DES_V_MNEM) 53
	set dlrv_idx(GSS_ST3_VT3_DES_MAG_MNEM) 54
	set dlrv_idx(GSS_ST3_VT3_DES_OSC_MNEM) 55
	set dlrv_idx(GSS_ST3_VT4_DES_H_MNEM) 56
	set dlrv_idx(GSS_ST3_VT4_DES_V_MNEM) 57
	set dlrv_idx(GSS_ST3_VT4_DES_MAG_MNEM) 58
	set dlrv_idx(GSS_ST3_VT4_DES_OSC_MNEM) 59
	set dlrv_idx(GSS_ST3_VT5_DES_H_MNEM) 60
	set dlrv_idx(GSS_ST3_VT5_DES_V_MNEM) 61
	set dlrv_idx(GSS_ST3_VT5_DES_MAG_MNEM) 62
	set dlrv_idx(GSS_ST3_VT5_DES_OSC_MNEM) 63

########################################################################
#	Outputs for Memory Upload of Star Tracker DCM and Boresight (scmd AE)
#	DCM Sense:	V_BDY = DCM * V_STx
#	Each element is a dimensionless real number in the range [-1,1]
#	First index = Row; Second index = Col
#	ST Boresight is the 3rd column
#
	set dlrv_idx(GSS_ST1_TO_BDY_1_1_MNEM) 64
	set dlrv_idx(GSS_ST1_TO_BDY_1_2_MNEM) 65
	set dlrv_idx(GSS_ST1_TO_BDY_1_3_MNEM) 66
	set dlrv_idx(GSS_ST1_TO_BDY_2_1_MNEM) 67
	set dlrv_idx(GSS_ST1_TO_BDY_2_2_MNEM) 68
	set dlrv_idx(GSS_ST1_TO_BDY_2_3_MNEM) 69
	set dlrv_idx(GSS_ST1_TO_BDY_3_1_MNEM) 70
	set dlrv_idx(GSS_ST1_TO_BDY_3_2_MNEM) 71
	set dlrv_idx(GSS_ST1_TO_BDY_3_3_MNEM) 72
#
	set dlrv_idx(GSS_ST2_TO_BDY_1_1_MNEM) 73
	set dlrv_idx(GSS_ST2_TO_BDY_1_2_MNEM) 74
	set dlrv_idx(GSS_ST2_TO_BDY_1_3_MNEM) 75
	set dlrv_idx(GSS_ST2_TO_BDY_2_1_MNEM) 76
	set dlrv_idx(GSS_ST2_TO_BDY_2_2_MNEM) 77
	set dlrv_idx(GSS_ST2_TO_BDY_2_3_MNEM) 78
	set dlrv_idx(GSS_ST2_TO_BDY_3_1_MNEM) 79
	set dlrv_idx(GSS_ST2_TO_BDY_3_2_MNEM) 80
	set dlrv_idx(GSS_ST2_TO_BDY_3_3_MNEM) 81
#
	set dlrv_idx(GSS_ST3_TO_BDY_1_1_MNEM) 82
	set dlrv_idx(GSS_ST3_TO_BDY_1_2_MNEM) 83
	set dlrv_idx(GSS_ST3_TO_BDY_1_3_MNEM) 84
	set dlrv_idx(GSS_ST3_TO_BDY_2_1_MNEM) 85
	set dlrv_idx(GSS_ST3_TO_BDY_2_2_MNEM) 86
	set dlrv_idx(GSS_ST3_TO_BDY_2_3_MNEM) 87
	set dlrv_idx(GSS_ST3_TO_BDY_3_1_MNEM) 88
	set dlrv_idx(GSS_ST3_TO_BDY_3_2_MNEM) 89
	set dlrv_idx(GSS_ST3_TO_BDY_3_3_MNEM) 90

########################################################################
#	Outputs for Memory Upload of Sun Sensor DCM and Boresight (scmd AE)
#	DCM Sense:	V_BDY = DCM * V_SS
#	Each element is a dimensionless real number in the range [-1,1]
#	First index = Row; Second index = Col
#	SS Boresight is the 1st column
#
	set dlrv_idx(GSS_SS_TO_BDY_1_1_MNEM) 91
	set dlrv_idx(GSS_SS_TO_BDY_1_2_MNEM) 92
	set dlrv_idx(GSS_SS_TO_BDY_1_3_MNEM) 93
	set dlrv_idx(GSS_SS_TO_BDY_2_1_MNEM) 94
	set dlrv_idx(GSS_SS_TO_BDY_2_2_MNEM) 95
	set dlrv_idx(GSS_SS_TO_BDY_2_3_MNEM) 96
	set dlrv_idx(GSS_SS_TO_BDY_3_1_MNEM) 97
	set dlrv_idx(GSS_SS_TO_BDY_3_2_MNEM) 98
	set dlrv_idx(GSS_SS_TO_BDY_3_3_MNEM) 99
 
########################################################################
#	Outputs for Stellar Acq (scmds 8A and 7F)
#	GSS_ST_ACQ_USE_STx is a bool:
#		0 = Not using STx for stellar acq
#		1 = Using STx for stellar acq
#		Sun Hold CIS applies this through scmd 8A
#	GSS_QUAT_EST_x is a dimensionless real number in the range [-1,1]
#		Indicates the ECI_2_Bdy quaternion estimate determined by CIS
#		Sun Hold CIS applies this through scmd 8A
#		Rough Att CIS applies this through scmd 7F
#	GSS_RATE_CMD_x is a real rate vector in deg/sec to maintain sun hold
#		Referenced to inertial space, expressed in body coordinates
#		Sun Hold CIS applies this through scmd 8A
#
	set dlrv_idx(GSS_ST_ACQ_USE_ST1_MNEM) 100
	set dlrv_idx(GSS_ST_ACQ_USE_ST2_MNEM) 101
	set dlrv_idx(GSS_ST_ACQ_USE_ST3_MNEM) 102
#
	set dlrv_idx(GSS_QUAT_EST_1_MNEM) 103
	set dlrv_idx(GSS_QUAT_EST_2_MNEM) 104
	set dlrv_idx(GSS_QUAT_EST_3_MNEM) 105
	set dlrv_idx(GSS_QUAT_EST_4_MNEM) 106
#
	set dlrv_idx(GSS_RATE_CMD_1_MNEM) 107
	set dlrv_idx(GSS_RATE_CMD_2_MNEM) 108
	set dlrv_idx(GSS_RATE_CMD_3_MNEM) 109
 
########################################################################
#	Control which GSS can write to the output LRVs.
#	When this LRV is 0, it means that no GSS is currently output-enabled.
#	In this case (by operator directive), the GSS can set it to its IP.
#	This is required before any output LRVs are allowed to be changed
#	When done (by operator directive), the GSS can set it back to 0.
#
	set dlrv_idx(GSS_OUT_ENABLED_IPA_MNEM) 110
	set dlrv_idx(GSS_OUT_ENABLED_PID_MNEM) 111

}

########################################################################
#	$Log: $
########################################################################
