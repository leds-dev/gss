########################################################################
#	$Id: $
########################################################################

########################################################################
#	IMPORTS

package provide GSS_TCL_LIB 1.0

########################################################################
#	fake ST setup

set st_list { 1 2 3 }
set vt_list { 1 2 3 4 5 }

proc vtx { st vt } {
	global NUM_VTS
	return [expr ($st - 1) * $NUM_VTS + $vt]
	
}

########################################################################
#	CIS GUI

namespace eval ::CIS_GUI:: {
	variable base_frm
	variable self
	variable ui_sel
	variable cis_committed
	variable commit_btn
	variable cis_commit_cnt
	variable cis_commit_cnt_lbl
	variable cis_state_text
	variable cis_data_time

	variable bsel { deselect select  }

	#	list button text and callback command
	#	TBR - hide if toolbar implemented
	variable buts {
		::CIS_GUI::cancel	Cancel
		::CIS_GUI::snapshot	Snapshot
		::CIS_GUI::rough	Rough_Attitude
		::CIS_GUI::sunhold	Sun_Hold 
		::CIS_GUI::commit	Commit
		}

	variable bstate { "disabled" "normal" }
	variable rough_err
	variable rough_err_min
	variable rough_err_max
	variable temp_rough_err
	
	namespace export init rough sunhold snapshot \
	                 commit cis_button cis_gui

}

########################################################################
#	initialize cis interface

proc ::CIS_GUI::init { } {
	variable base_frm
	variable self
	variable cis_committed
	variable cis_commit_cnt_lbl
	variable cis_commit_cnt
	variable cis_state_text
	variable rough_err
	variable rough_err_min
	variable rough_err_max
	variable temp_rough_err
		
	global gi
	
	set base_frm {}
		
	set cis_committed "disabled"
	set cis_state_text "Not Committed"
	
	set cis_commit_cnt 0
	set cis_commit_cnt_lbl "Commit"

	#	hold gui items for dynamic data-dependent configuration.
	set self(cis) {}

	#	read cis data from ini
	cis_set
	
	#       Init the Rough Error
	set rough_err_min  1.0
	#  Increase rough_err_max from 50 to 180 at engineer request
	set rough_err_max 180.0
	set rough_err [$gi MAX_ROUGH_KNOWLEDGE_ERROR 5.0]
	if { $rough_err < $rough_err_min } {
		set rough_err $rough_err_min
	} elseif { $rough_err > $rough_err_max } {
		set rough_err $rough_err_max
	}
	set temp_rough_err $rough_err
	GetRoughError $rough_err

	#	make sure the stupid filepointer is initialized
	#	TCL won't accept or provide a C FILE ptr.  Now what?
	#	passing filename to CIS API.
	#	CIS must open for append and flush and close when it 
	#	finishes.
	GetLogFileName $::FILES::log

}

########################################################################
#	pump GSS.ini data into CIS. failure exits.

proc ::CIS_GUI::cis_set { } {
	global cis_ini

	variable tags [list \
		MAX_ST_MISALIGNMENT_ERROR \
		MAX_SEPARAT_PRIMARY_TOL \
		PARALLEL_ERROR_THRESHOLD \
		MAG_SATURATION \
		MAGNITUDE_BIAS \
		SMS_FOV \
		ATTITUDE_UNCERTAINTY \
		OVERLAP_PIXELS \
		SPREAD_LIMIT]

	set cis_ini [CIS_INI_VARS -this [cis_ini_get]]
	foreach i $tags {
        $cis_ini configure -$i $::GSSINI::gssini($i)
	}

}

########################################################################
#	BUTTON ACTIONS
########################################################################

########################################################################
#	Rough Attitude

proc ::CIS_GUI::rough { } {

	set err [catch {set logid [open $::FILES::log a]}]
	if {$err == 1} {
		::MSG_DLG::attention "Could not write to log file \n$::FILES::log \nwhich is a prerequisite for executing CIS" \
                             red \
                             black \
                             -INSTRUCTION "Check file name and directory access privileges"
		return
	}
	close $logid

	CIS_RoughKnowledge 
	::CIS_GUI::cis_update

}


########################################################################
#	SunHold

proc ::CIS_GUI::sunhold { } {

    set err [catch {set logid [open $::FILES::log a]}]
    if {$err == 1} {
        ::MSG_DLG::attention "Could not write to log file \n$::FILES::log \nwhich is a prerequisite for executing CIS" \
                             red \
                             black \
                             -INSTRUCTION "Check file name and directory access privileges"
        return
    }
    close $logid

	CIS_SunHold
	::CIS_GUI::cis_update

}

########################################################################
#	get CIS status

proc ::CIS_GUI::cis_status { } {
	variable base_frm
	variable commit_btn
	
	set cisexe_status [cis_result]

	$base_frm.status.txt insert end "$cisexe_status"
	$base_frm.status.txt see end	
	
	if {[string first "Succes" \
	                  $cisexe_status] != -1} {
	                  
	    $commit_btn configure -state normal
	    
	} else {
	    $commit_btn configure -state disabled
	    
	}
	
}

########################################################################
#	commit CIS data

proc ::CIS_GUI::commit { } {
	variable base_frm
	variable cis_committed
	variable cis_commit_cnt_lbl
	variable cis_commit_cnt
	variable cis_state_text
	
	SetCISCommitBuff
	set cis_committed "normal"
	
	#	Update the quaternion displays
	set data [GetCommitECI2BodyQuat]

	set ::USER_QUAT::cis_quat(0) \
	    [format "%9.7f" [lindex $data 0]]
	set ::USER_QUAT::cis_quat(1) \
	    [format "%9.7f" [lindex $data 1]]
	set ::USER_QUAT::cis_quat(2) \
	    [format "%9.7f" [lindex $data 2]]
	set ::USER_QUAT::cis_quat(3) \
	    [format "%9.7f" [lindex $data 3]]
	    
	#	Update the alignments displays
			      
	foreach st {1 2 3} {
	    set data [GetCommitST2BodyDCM $st]
	    set ::ALIGN::st_comit_dcm($st,1,1) \
	        [format "%+9.7f" [lindex $data 0]]
	    set ::ALIGN::st_comit_dcm($st,1,2) \
	        [format "%+9.7f" [lindex $data 1]]
	    set ::ALIGN::st_comit_dcm($st,1,3) \
	        [format "%+9.7f" [lindex $data 2]]
	    set ::ALIGN::st_comit_dcm($st,2,1) \
	        [format "%+9.7f" [lindex $data 3]]
	    set ::ALIGN::st_comit_dcm($st,2,2) \
	        [format "%+9.7f" [lindex $data 4]]
	    set ::ALIGN::st_comit_dcm($st,2,3) \
	        [format "%+9.7f" [lindex $data 5]]
	    set ::ALIGN::st_comit_dcm($st,3,1) \
	        [format "%+9.7f" [lindex $data 6]]
	    set ::ALIGN::st_comit_dcm($st,3,2) \
	        [format "%+9.7f" [lindex $data 7]]
	    set ::ALIGN::st_comit_dcm($st,3,3) \
	        [format "%+9.7f" [lindex $data 8]]
	        
	}

	#	Display the CIS Sun Sensor Alignments
	set data [GetCommitSS2BodyDCM]
	set ::ALIGN::ss_comit_dcm(1,1) \
	    [format "%+9.7f" [lindex $data 0]]
	set ::ALIGN::ss_comit_dcm(1,2) \
	    [format "%+9.7f" [lindex $data 1]]
	set ::ALIGN::ss_comit_dcm(1,3) \
	    [format "%+9.7f" [lindex $data 2]]
	set ::ALIGN::ss_comit_dcm(2,1) \
	    [format "%+9.7f" [lindex $data 3]]
	set ::ALIGN::ss_comit_dcm(2,2) \
	    [format "%+9.7f" [lindex $data 4]]
	set ::ALIGN::ss_comit_dcm(2,3) \
	    [format "%+9.7f" [lindex $data 5]]
	set ::ALIGN::ss_comit_dcm(3,1) \
	    [format "%+9.7f" [lindex $data 6]]
	set ::ALIGN::ss_comit_dcm(3,2) \
	    [format "%+9.7f" [lindex $data 7]]
	set ::ALIGN::ss_comit_dcm(3,3) \
	    [format "%+9.7f" [lindex $data 8]]
	    
	#    Update the Commit count on the button label
	incr cis_commit_cnt
	set cis_commit_cnt_lbl "Commit \#$cis_commit_cnt"
	
	#    Update the text display screen 
	$base_frm.status.txt insert end "\n\n^^^^^^^^^^^^^^^^^^^^ $cis_commit_cnt_lbl\
	                                 ^^^^^^^^^^^^^^^^^^^^\n"
	$base_frm.status.txt see end	

	#    Update the log
	::MSG_DLG::msg_log "\n\nCIS COMMIT BUTTON PRESSED -\
	                    $cis_commit_cnt_lbl\n\n"
	                      
	#    Update the other screens
	set cis_state_text "Commit \#$cis_commit_cnt"
	                      
}

########################################################################

proc ::CIS_GUI::snapshot { } {
	variable cis_data_time

	#	call C layer CIS snapshot
	cis_snapshot
	
	#	timestamp
	set cis_data_time $::CONFIG::data(gss_time)

	#	set button status to vis = track && enabled.
	for {set i 1} {$i <= 15} {incr i} {
		set vis [cis_vt_vis $i]
		cis_vt_sel $i $vis
	}

	#	update gui
	::CIS_GUI::cis_update

}

########################################################################

proc ::CIS_GUI::cis_gui { } {
	global st_list
	global vt_list

	variable base_frm
	variable self
	variable buts
	variable ui_sel
	variable status
	variable temp_rough_err
	variable commit_btn
	variable cis_commit_cnt_lbl
	
	set vthdr(0) "H (deg)"
	set vthdr(1) "V (deg)"
	set vthdr(2) MAG
	set vthdr(3) VALID
	set vthdr(4) TRACKING
	set vthdr(5) "TM ID"
	set vthdr(6) "CIS ID"
	set vthdr(7) "OSC STAR"
	
	#	save parent frame name for exit.
	catch {destroy .cis_ui}
	toplevel .cis_ui
	wm title .cis_ui "CONFUSED-IN-SPACE CONTROL"
	wm geometry .cis_ui "-0-0"
	set f .cis_ui.top
	frame $f
	pack $f
	set base_frm $f

	#-----------------------------------------------
	#	label at top, then vts, then cis status, then buttons.
	#	rough error below label.

	#	rough error entry box
	frame $f.rerr \
	      -borderwidth 1 \
	      -relief sunken
	pack $f.rerr \
	     -side top \
	     -fill x
	     
	label $f.rerr.txt \
	      -text "Rough Attitude Knowledge Error (deg)" \
	      -font $::CONFIG::fonts(label) \
	      -anchor w \
	      -width 40
	entry $f.rerr.val \
	      -textvariable ::CIS_GUI::temp_rough_err \
	      -font $::CONFIG::fonts(entry)
	button $f.rerr.btn \
	       -text "Set Rough Error" \
	       -font $::CONFIG::fonts(label) \
	       -command "::CIS_GUI::update_rough_err"
	      
	pack $f.rerr.txt $f.rerr.val \
	                 -side left \
	                 -pady 5 \
	                 -fill x
	pack $f.rerr.btn \
	                 -side left \
	                 -pady 5 \
	                 -padx 2 \
	                 -fill x


	#	Star Tracker Data
	foreach st {1 2 3} {
	    frame $f.st$st \
	          -borderwidth 1 \
	          -relief sunken
	    pack $f.st$st \
	         -side top \
	         -fill both

	#	Give it a title
	    label $f.st$st.title \
	          -text "Star Tracker $st" \
	          -font $::CONFIG::fonts(label) \
	          -anchor w \
	          -justify left
	    pack $f.st$st.title \
	         -side top \
	         -fill x

	#	Header for VT data
	    frame $f.st$st.vthdr
	    pack $f.st$st.vthdr \
	         -side top \
	         -anchor e
	         
	                     
	    label $f.st$st.vthdr.col7 \
	              -text $vthdr(7) \
	              -font $::CONFIG::fonts(entry) \
	              -width 14 \
	              -borderwidth 2 \
	              -relief sunken \
	              -background gray70 \
	              -anchor c \
	              -justify center
	    pack $f.st$st.vthdr.col7 \
	             -side right
	                     
	    label $f.st$st.vthdr.col6 \
	              -text $vthdr(6) \
	              -font $::CONFIG::fonts(entry) \
	              -width 14 \
	              -borderwidth 2 \
	              -relief sunken \
	              -background gray70 \
	              -anchor c \
	              -justify center
	    pack $f.st$st.vthdr.col6 \
	             -side right

	    foreach col {5 4 3 2 1 0} {
	                     
	        label $f.st$st.vthdr.col$col \
	              -text $vthdr($col) \
	              -font $::CONFIG::fonts(entry) \
	              -width 14 \
	              -borderwidth 1 \
	              -relief sunken \
	              -background gray70 \
	              -anchor c \
	              -justify center
	        pack $f.st$st.vthdr.col$col \
	             -side right

	    }


	#	Put up the VT data
	    foreach vt {1 2 3 4 5} {
	        set vtn [vtx $st $vt]
	        
	        frame $f.st$st.vt$vt
	        pack $f.st$st.vt$vt \
	             -side top \
	             -fill x
	        
	#	Put in selection button
	        checkbutton $f.st$st.vt$vt.cb \
	                    -text "VT $vt" \
	                    -anchor w \
	                    -font $::CONFIG::fonts(entry) \
	                    -borderwidth 1 \
	                    -relief ridge \
 			    -onvalue 1	\
 			    -offvalue 0 \
 			    -variable ::CIS_GUI::ui_sel($vtn,user) \
 			    -width 10 \
	                    -justify right \
	                    -command "::CIS_GUI::user_selection $vtn"
	        
	#	Get VT data
	        set data [cis_vtinfo $vtn]

	        label $f.st$st.vt$vt.h \
	              -text [format "%8.5f" [lindex $data 0]] \
	              -font $::CONFIG::fonts(entry) \
	              -borderwidth 1 \
	              -relief sunken \
	              -width 14

	        label $f.st$st.vt$vt.v \
	              -text [format "%8.5f" [lindex $data 1]] \
	              -font $::CONFIG::fonts(entry) \
	              -borderwidth 1 \
	              -relief sunken \
	              -width 14

	        label $f.st$st.vt$vt.mag \
	              -text [format "%8.5f" [lindex $data 2]] \
	              -font $::CONFIG::fonts(entry) \
	              -borderwidth 1 \
	              -relief sunken \
	              -width 14

	        set valid "Invalid"
	        if {[lindex $data 3] == 1} {
	            set valid "Valid"
	           
	        }

	        label $f.st$st.vt$vt.valid \
	              -text $valid \
	              -font $::CONFIG::fonts(entry) \
	              -justify center \
	              -borderwidth 1 \
	              -relief sunken \
	              -width 14

	        set track "False"
	        if {[lindex $data 4] == 1} {
	            set track "True"
	           
	        }

	        label $f.st$st.vt$vt.track \
	              -text $track \
	              -font $::CONFIG::fonts(entry) \
	              -justify center \
	              -borderwidth 1 \
	              -relief sunken \
	              -width 14

	        label $f.st$st.vt$vt.tmid \
	              -text [lindex $data 5] \
	              -font $::CONFIG::fonts(entry) \
	              -borderwidth 1 \
	              -relief sunken \
	              -width 14

	        label $f.st$st.vt$vt.bssid \
	              -text [lindex $data 6] \
	              -font $::CONFIG::fonts(entry) \
	              -borderwidth 2 \
	              -relief sunken \
	              -width 14

	        set osc "No"
	        if {[lindex $data 7] == 1} {
	            set osc "Yes"
	           
	        }

	        label $f.st$st.vt$vt.osc \
	              -text $osc \
	              -font $::CONFIG::fonts(entry) \
	              -borderwidth 2 \
	              -relief sunken \
	              -width 14

	        pack $f.st$st.vt$vt.osc\
	             $f.st$st.vt$vt.bssid \
	             $f.st$st.vt$vt.tmid \
	             $f.st$st.vt$vt.track \
	             $f.st$st.vt$vt.valid \
	             $f.st$st.vt$vt.mag \
	             $f.st$st.vt$vt.v \
	             $f.st$st.vt$vt.h \
	             $f.st$st.vt$vt.cb \
	             -side right \
	             -fill x 

	    }

	}
	

	#	for CIS execution status
	frame $f.status \
	      -borderwidth 1 \
	      -relief sunken
	pack $f.status \
	     -side top \
	     -fill x
	     
	label $f.status.title \
	      -text "CIS Execution Status" \
	      -font $::CONFIG::fonts(label) \
	      -anchor w \
	      -justify left
	pack $f.status.title \
	     -side top \
	     -fill x \
	     -pady 3

        text $f.status.txt \
	     -yscrollcommand "$f.status.scroll set" \
             -setgrid true \
             -wrap word \
             -width 45 \
             -height 10 \
             -borderwidth 1 \
             -relief sunken \
             -font $::CONFIG::fonts(entry) 

	pack $f.status.txt \
	     -side left \
	     -expand 1\
	     -fill both \
	     -anchor nw \
	     -pady 3 

	scrollbar $f.status.scroll \
	          -command "$f.status.txt yview" \
	          -width 10
	pack $f.status.scroll \
	     -side right \
	     -fill y


	#	Create Control buttons
	frame $f.ctrl \
	      -borderwidth 1 \
	      -relief sunken
	pack $f.ctrl \
	     -side top \
	     -fill x

	#	Snapshot button
	button $f.ctrl.snap \
	       -text "TLM Snapshot" \
	       -font $::CONFIG::fonts(entry) \
	       -command "::CIS_GUI::snapshot"
	     
	#	Rough Attitude button
	button $f.ctrl.rough \
	       -text "Rough Attitude" \
	       -font $::CONFIG::fonts(entry) \
	       -command "::CIS_GUI::rough"
	     
	#	Sun Hold button
	button $f.ctrl.sun \
	       -text "Sun Hold" \
	       -font $::CONFIG::fonts(entry) \
	       -command "::CIS_GUI::sunhold"
	     
	#	Commit button
	button $f.ctrl.commit \
	       -textvariable ::CIS_GUI::cis_commit_cnt_lbl \
	       -state disabled \
	       -font $::CONFIG::fonts(entry) \
	       -command "::CIS_GUI::commit"

	set commit_btn $f.ctrl.commit

	pack $f.ctrl.snap \
	     $f.ctrl.rough \
	     $f.ctrl.sun \
	     $f.ctrl.commit \
	     -side left \
	     -fill x \
	     -anchor c \
	     -padx 45 \
	     -pady 3


	#	for OK button
	frame $f.btn \
	      -borderwidth 1 \
	      -relief sunken 
	pack $f.btn \
	     -side top \
	     -fill x 
	         
	button $f.btn.ok \
	       -text OK \
	       -font $::CONFIG::fonts(entry) \
	       -command "destroy .cis_ui" 
	pack $f.btn.ok \
	     -side top \
	     -pady 3

	#	Take an initial snapshot
	::CIS_GUI::snapshot

	#	Preset the VT selections to those tracking and valid
	foreach st {1 2 3} {
	
	    foreach vt {1 2 3 4 5} {
	    
	        set vtn [vtx $st $vt]

	#	load VT vis = track && valid.
		set vis [cis_vt_vis $vtn]

	#	pre-select based on TM.  vt valid and tracking.
		set ui_sel($vtn,user) $vis

	        ::CIS_GUI::user_selection $vtn
	        
	    }
	    
	}

}

########################################################################
	#	report user selection

proc ::CIS_GUI::user_selection { i } {
	variable ui_sel
	cis_vt_sel $i $ui_sel($i,user)

}


########################################################################
	#	Screen rough knowledge error

proc ::CIS_GUI::update_rough_err { } {
	variable rough_err
	variable rough_err_min
	variable rough_err_max
	variable temp_rough_err
	
	if {[catch {expr $temp_rough_err + 0}] == 1} {
	
	    set temp_rough_err $rough_err
	    ::MSG_DLG::attention "Invalid Entry" \
	                          red \
	                          black \
	                          -INSTRUCTION \
	                          "Re-enter the Rough error value" 

	} elseif { ( $temp_rough_err > $rough_err_max) } {
	
	    set temp_rough_err $rough_err
	    ::MSG_DLG::attention "Rough Attitude Knowledge Error must be <= $rough_err_max degrees." \
	                          red \
	                          black \
	                          -INSTRUCTION \
	                          "Re-enter the Rough error value"   
                
        } elseif { ( $temp_rough_err < $rough_err_min) } {
        
       	    set temp_rough_err $rough_err 
       	    ::MSG_DLG::attention "Rough Attitude Knowledge Error must be >= $rough_err_min degrees." \
	                          red \
	                          black \
	                          -INSTRUCTION \
	                          "Re-enter the Rough error value"   
                 
        } else { 
         
            set rough_err $temp_rough_err 
	    GetRoughError $rough_err
	    ::MSG_DLG::msg_log "\nSET ROUGH ERROR BUTTON PRESSED - Rough Error set to $rough_err deg\n"
	    
	}

}


########################################################################
#	update CIS GUI.

proc ::CIS_GUI::cis_update { } {
	variable base_frm
	variable self
	variable bsel
	variable bstate
	variable ui_sel

#	puts ::CIS_GUI::cis_update

	#	update user selection
	for {set i 1} {$i <= 15} { incr i } { ::CIS_GUI::user_selection $i }

	#	update execution status
	::CIS_GUI::cis_status

	#	Update screen information
	foreach st {1 2 3} {
	
	    foreach vt {1 2 3 4 5} {
	    
	        set vtn [vtx $st $vt]
	    
	#	Get the vt information from CIS
	        set data [cis_vtinfo $vtn]

	        $base_frm.st$st.vt$vt.h configure \
	              -text [format "%8.5f" [lindex $data 0]]

	        $base_frm.st$st.vt$vt.v configure \
	              -text [format "%8.5f" [lindex $data 1]]

	        $base_frm.st$st.vt$vt.mag configure \
	              -text [format "%8.5f" [lindex $data 2]]

	        set valid "Invalid"
	        if {[lindex $data 3] == 1} {
	            set valid "Valid"
	           
	        }

	        $base_frm.st$st.vt$vt.valid configure \
	              -text $valid 

	        set track "False"
	        if {[lindex $data 4] == 1} {
	            set track "True"
	           
	        }

	        $base_frm.st$st.vt$vt.track configure \
	              -text $track 

	        $base_frm.st$st.vt$vt.tmid configure \
	              -text [lindex $data 5] 

	        $base_frm.st$st.vt$vt.bssid configure \
	              -text [lindex $data 6] 

	        set osc "No"
	        if {[lindex $data 7] == 1} {
	            set osc "Yes"
	           
	        }

	        $base_frm.st$st.vt$vt.osc configure \
	              -text $osc

		#	load VT vis = track && valid.
		set vis [cis_vt_vis $vtn]

		#	get button state
		set state [lindex $bstate $vis]

		#	display user select button if VT is okay.
		$base_frm.st$st.vt$vt.cb configure \
		                         -state $state 

	    }
	    
	}
	
}

########################################################################
#	$Log: $
########################################################################
