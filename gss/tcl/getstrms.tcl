package provide RAW_TM_LIB 1.0

proc RAW_TM_getstrms {file} {
#@##################################################################################
#@        COPYRIGHT 2004 THE BOEING COMPANY, ALL RIGHTS RESERVED
#@##################################################################################
#@
#@ MODULE NAME: RAW_TM_GETSTRMS
#@
#@ PURPOSE:
#@         To asertain what streams may be available.
#@
#@ AUTHOR: R. Telkamp
#@
#@    $Id: $
#@
#@ CREATION DATE: 09/18/01
#@
#@ HISTORY: 
#@     MM/DD/YY - Originator, reason for change
#@
#@    $Log: 
#@
#@##################################################################################
#@ OPERATION:
#@
#@ This routine reads the epoch.cfg file to find out what streams are defined.
#@
#@##################################################################################
#@ CALLING SEQUENCE:
#@ 
#@   file - string (passed); full name of the epoch.cfg file
#@
#@##################################################################################

    set strm_list ""

#
###     The file selection menu forces the operator to select a Pulse command file.
###     Therefore, for pulse commands we already have the filename.
#
    set cfg_file $file
    set err [catch {set inid [open $cfg_file r]}]
    
    if {$err != 0} {
        return "rt1a rt1b smint pb1g"
        
    }

#
###     Get 1st data line
#
    set strlen [gets $inid str]

    while {$strlen>=0} {
        if {$strlen > 0} {

#
###             Fixed record format, just pull it apart
#
            set token [lindex $str 0]

#
###             If the token contains STREAM, then the stream name is 2nd param.
#
            if { $token == "STREAM" } {

                lappend strm_list [lindex $str 1]

            }

        }
        set strlen [gets $inid str]

    }
    close $inid
    
    return $strm_list

}

