########################################################################
#	$Id: $
########################################################################

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ::EXT_OBJS:: {

	variable user
	variable sun_radec [RA_DEC]

	variable ext_obj_len 
	variable ext_obj_list
	variable ext_obj

	namespace export init draw update user_gui

}

########################################################################

proc ::EXT_OBJS::init { } {
	global gi
	variable ext_obj_len
	variable ext_obj_list
	variable ext_obj
	variable user
	variable radec
	variable sun_radec 

	set ext_obj_len 17
	set ext_obj_list {mercury venus mars\
	                  jupiter saturn uranus neptune pluto\
	                  asteroid_1 asteroid_2 asteroid_3\
	                  asteroid_4 comet_1 comet_2\
	                  sun moon earth}

	foreach i $ext_obj_list {
		set uc [string toupper $i]
		
		#	set drawing functions
		set ext_obj($i,to_sky)	::EXT_OBJS::to_sky_1
		set ext_obj($i,to_st)	::EXT_OBJS::to_st_1
		set ext_obj($i,update)	::EXT_OBJS::update_1
		set ext_obj($i,draw)	::EXT_OBJS::stayout_1

		# 	set user controls
		set user($i,sky) 0
		set user($i,st) 0

		#	set display attributes
		set ext_obj($i,color)		\
			[$gi [format "%s_COLOR" $uc] red1]
		set ext_obj($i,radius)		\
			[$gi [format "%s_SIZE" $uc] 30.0]
		set ext_obj(stayout_$i,color)	\
			[$gi [format "%s_STAYOUT_COLOR" $uc] red3]
		set ext_obj(stayout_$i,radius)	\
			[$gi [format "%s_STAYOUT_REGION" $uc] 35.0]
			
	}

	#	set numeric ids 
	set ext_obj(earth,id) 0
	set ext_obj(sun,id) 1
	set ext_obj(moon,id) 2
	set ext_obj(mercury,id) 3
	set ext_obj(venus,id) 4
	set ext_obj(mars,id) 5
	set ext_obj(jupiter,id) 6
	set ext_obj(saturn,id) 7
	set ext_obj(uranus,id) 8
	set ext_obj(neptune,id) 9
	set ext_obj(pluto,id) 10
	set ext_obj(asteroid_1,id) 11
	set ext_obj(asteroid_2,id) 12
	set ext_obj(asteroid_3,id) 13
	set ext_obj(asteroid_4,id) 14
	set ext_obj(comet_1,id) 15
	set ext_obj(comet_2,id) 16

	#	set string ids 
	set ext_obj(0,str_id) earth
	set ext_obj(1,str_id) sun
	set ext_obj(2,str_id) moon
	set ext_obj(3,str_id) mercury
	set ext_obj(4,str_id) venus
	set ext_obj(5,str_id) mars
	set ext_obj(6,str_id) jupiter
	set ext_obj(7,str_id) saturn
	set ext_obj(8,str_id) uranus
	set ext_obj(9,str_id) neptune
	set ext_obj(10,str_id) pluto
	set ext_obj(11,str_id) asteroid_1
	set ext_obj(12,str_id) asteroid_2
	set ext_obj(13,str_id) asteroid_3
	set ext_obj(14,str_id) asteroid_4
	set ext_obj(15,str_id) comet_1
	set ext_obj(16,str_id) comet_2


	#	read the sun for CIS
	read_sun

}

########################################################################
#	read sun from ephemeris

proc ::EXT_OBJS::read_sun { } {
	variable sun_radec 

	$sun_radec configure -ra  [ephem_get_ra  sun] \
			     -dec [ephem_get_dec sun]
	}


########################################################################
#	$Log: $
########################################################################
