#!/usr/local/tcl/bin/wish
#########################################################################
#	$Id: $
########################################################################

package provide GSS 1.0

#	DO NOT DO PACKAGE REQUIRE HERE.  SEE BELOW.

namespace eval MAIN {
	variable sim
	variable prog_rev
	variable lib_rev
	variable tcl_rev
	variable c_rev
	variable c_year
	namespace export init top_loop paths

}

########################################################################
#	load config dialog
#	if user continued, load main gui and start update cycle.

proc ::MAIN::init { } {
	set ::MAIN::prog_rev 1.9
	::CONFIG::init                 ;# Initialize program variables
	::FILES::init                  ;# Initialize static DB files path/name
	::MSG_DLG::init                ;# Initialize the message dialogs
	::CONFIG::startup_screen       ;# Initial startup screen
	::FILES::init_log              ;# Initialize SC-specific DB files path/name
	::MSG_DLG::init_log            ;# Initialize the log file
	::GSS::main                    ;# Create the displays
	::MAIN::top_loop               ;# Start the main loop

}

########################################################################
#	once a second, update gmt, read tm, run gui.

proc ::MAIN::top_loop { } {
	::update idletasks
	set x [lindex [time ::TM::update] 0]
	::update idletasks

	UpdateAttitude

#	Update Celestial Local Time
	::CONFIG::calc_local_time [GetSC_Longitude]
	
	set y [lindex [time ::SKYMAP::sky_update] 0]
	::update idletasks

	set z [lindex [time ::ST::st_update] 0]
	::update idletasks
#puts stdout "tm_update time = $x"
#puts stdout "sky_update time = $y"
#puts stdout "st_update time = $z\n"
	set ::CONFIG::data(duty_cycle) [expr ( ($x + $y + $z) / 1024000.0) \
	                                       * 100.0]

	after 1024 ::MAIN::top_loop

}

########################################################################
#	configure app path

proc ::MAIN::paths { } {
	::MAIN::set_auto_path
	::MAIN::loads

}

########################################################################
#	set up application library paths.

proc ::MAIN::set_auto_path { } {
	global auto_path
	global env
	
	set gss ".."
	catch {set gss $env(GOES_GSS_DIR)}
 
	set ap [list $gss/tcl \
	             $gss/swig \
	             $gss/gtacs \
	             $gss/library \
	             $gss/tcl/lib \
	             . \
	             ..]
	set auto_path [concat $ap $auto_path]

}

########################################################################
#	load all shared libraries
#	gtacs isn't loaded if TM is predictive.  tm's responsibility.

proc ::MAIN::loads { } {
	global env
	variable sim

	set gss ".."
	catch {set gss $env(GOES_GSS_DIR)}

	load "$gss\/swig/libgss.so" libgss
	
	set ::MAIN::c_rev [clock format [file mtime "$gss\/swig/libgss.so"] -gmt yes]
	set ::MAIN::c_year [clock format [file mtime "$gss\/swig/libgss.so"] -format "%Y" -gmt yes]
	
	set ::MAIN::lib_rev [package require GSS_TCL_LIB]
	set ::MAIN::tcl_rev [info patchlevel]

	#	set the simulation pointer for EVERYBODY (except CIS).
	global tcl_sim_data
	set tcl_sim_data [SIM_DATA_TYPE -this [sim_data_get]]

	#	load tm simulation
	global argv

	if {[string toupper $argv] == "-MEWIZARD"} {
	    ::MAIN::mr_wizard
	    
	}

	#	sim is also read by tm processing.
	set sim {}
##	if {[llength argv] > 0} { set sim [lindex $argv 0] }

	load_sim $sim

}

proc ::MAIN::exec_mr_wizard_cmd {} {

	global mr_wizard_cmd
	global mr_wizard_cmd_err
	
	set mr_wizard_cmd_err [eval $mr_wizard_cmd]
	
}

proc ::MAIN::mr_wizard {} {
	global mr_wizard_cmd
	global mr_wizard_cmd_err
	catch {destroy .mrwizard}
	toplevel .mrwizard
	wm title .mrwizard "MR WIZARD"
	wm geometry .mrwizard "-0+0"
	set pf .mrwizard
	frame $pf.top -borderwidth 1 \
	              -relief sunken
	pack $pf.top -side top -fill both

	label $pf.top.lbl -text "CMD: " \
	                  -font "Arial 12"

	entry $pf.top.ent -textvariable mr_wizard_cmd \
	                  -font "Arial 12" \
	                  -width 40 

	pack $pf.top.lbl $pf.top.ent -side left \
	                             -anchor w \
	                             -pady 5 \
	                             -fill x

	frame $pf.err -borderwidth 1 \
	              -relief sunken
	pack $pf.err -side top -fill x
	
	label $pf.err.lbl -text "CMD ERROR = " \
	                  -font "Arial 12"
	
	label $pf.err.lbl2 -textvariable mr_wizard_cmd_err \
	                   -font "Arial 12"

	pack $pf.err.lbl $pf.err.lbl2 \
	                 -side left \
	                 -pady 5 \
	                 -fill x

	frame $pf.btn -borderwidth 1 \
	              -relief sunken
	pack $pf.btn -side top -fill x
	
	button $pf.btn.btn -text "Execute CMD" \
	                   -font "Arial 10" \
	                   -command ::MAIN::exec_mr_wizard_cmd

	pack $pf.btn.btn -side top -pady 5
	
}

	#	have to set paths before loading packages
	::MAIN::paths
	::MAIN::init

########################################################################
#	$Log:  $
########################################################################
