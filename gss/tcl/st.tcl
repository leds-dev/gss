########################################################################
#	$Id: $
########################################################################
# Star Trackers

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ST {
	variable st_base
	variable st_canv
	variable st_ui
	variable st_ui_tag
	variable st_gui
	variable st_pxpdeg
	variable st_FOV_corner
	variable st_FOV_zero
	variable st_vt_rest_x
	variable st_vt_rest_y
	variable st_onoff
	variable st_osc_count
	variable st_sms_fov
	variable st_back_color
	variable st_back_dk_lim
	variable st_back_lt_lim
	variable st_back_dk_gry
	variable st_back_lt_gry
	
	variable r0 [HV]
	variable r1 [HV]
	variable pt [GPOINT]
	variable p2 [GPOINT]
	variable ra [RA_DEC]

	variable modes

	namespace export init draw plot user_gui popup test 

}

########################################################################

proc ::ST::init { base_frm } {
	global gi
	variable st_base
	variable st_ui
	variable st_ui_tag
	variable st_canv
	variable modes
	variable st_pxpdeg
	variable st_FOV_corner
	variable st_FOV_zero
	variable st_vt_rest_x
	variable st_vt_rest_y
	variable st_onoff
	variable st_osc_count
	variable st_sms_fov
	variable st_back_color
	variable st_back_dk_lim
	variable st_back_lt_lim
	variable st_back_dk_gry
	variable st_back_lt_gry
	
	set st_osc_count(1) 0
	set st_osc_count(2) 0
	set st_osc_count(3) 0

	#	set ST background counts to black
	set st_back_color(1) black 
	set st_back_color(2) black 
	set st_back_color(3) black 

	#	initialize st background color parameters 
	set st_back_dk_lim [$gi ST_BACK_DARK_LIM 30]
	set st_back_lt_lim [$gi ST_BACK_LIGHT_LIM 300]
	set st_back_dk_clr [$gi ST_BACK_DARK_GRAY gray10]
	set st_back_lt_clr [$gi ST_BACK_LIGHT_GRAY gray50]
	set st_back_dk_gry [string trimleft $st_back_dk_clr gray]
	set st_back_lt_gry [string trimleft $st_back_lt_clr gray]

	#	set ST onoff status to 0 (off) 
	set st_onoff(1) 0 
	set st_onoff(2) 0 
	set st_onoff(3) 0 

	#	set ST mode string values.
	set modes(-1) {OFF}
	set modes(0) {FFOV}
	set modes(1) {DFOV}

	#	Initailize celestial object visibility
	foreach obj {SUN EARTH MOON MERCURY VENUS MARS JUPITER \
	             SATURN URANUS NEPTUNE PLUTO} {
            set st_ui($obj) normal
            set st_ui_tag($obj) $obj

	}


	foreach obj {COMET_1 COMET_2} {
	    set st_ui($obj) hidden
	    set st_ui_tag($obj) $obj
	    if {[lindex [$gi $obj {}] 10] == 1} {
	        set st_ui($obj) normal
	    
	    }
	    
	}
	
	foreach obj {ASTEROID_1 ASTEROID_2 ASTEROID_3 ASTEROID_4} {
	    set st_ui($obj) hidden
	    set st_ui_tag($obj) $obj
	    if {[lindex [$gi $obj {}] 13] == 1} {
	        set st_ui($obj) normal
	    
	    }
	    
	}

	#	grid on
	set st_ui(st_grid) normal
	set st_ui_tag(st_grid) "st_grid"
	
	#	centroid on
	set st_ui(centroid) hidden
	set st_ui_tag(centroid) "centroid"
	
	#	Celestial North off
	set st_ui(north) normal
	set st_ui_tag(north) "north"

	#	VT's on
	set st_ui(vt) normal
	set st_ui_tag(vt) "vt"
	if {$::CONFIG::data(tm_src) == "Planning"} {
	    set st_ui(vt) hidden
	    
	}
	
	#	S/C x axis off
	set st_ui(st_x) hidden
	set st_ui_tag(st_x) "st_x"
	
	#	S/C y axis off
	set st_ui(st_y) hidden
	set st_ui_tag(st_y) "st_y"

	#	S/C z axis off
	set st_ui(st_z) hidden
	set st_ui_tag(st_z) "st_z"
	
	#	All stars on
	set st_ui(stars) normal
	set st_ui(osc) ALL
	set st_ui_tag(osc) "stars"
	
	#	Set magnitude filter level
	set st_ui(prev_mag) 8.0
	set st_ui(mag) 8.0
	set st_ui(temp_mag) 8.0
	set st_ui_tag(mag) "mag"

	#	calculate pixels / degree. Use display x dimension so that
	#	views appear square.
	set st_pxpdeg(x) [expr $::CONFIG::display(disp_x) / 9.0]
	set st_pxpdeg(y) [expr $::CONFIG::display(disp_x) / 9.0]
	
	#	calculate FOV corners
	set st_FOV_corner(tlx) [expr $st_pxpdeg(x) * 0.5]
	set st_FOV_corner(tly) [expr $st_pxpdeg(y) * 0.5]
	
	set st_FOV_corner(trx) [expr $st_pxpdeg(x) * 8.5]
	set st_FOV_corner(try) $st_FOV_corner(tly)
	
	set st_FOV_corner(brx) $st_FOV_corner(trx)
	set st_FOV_corner(bry) [expr $st_pxpdeg(y) * 8.5]
	
	set st_FOV_corner(blx) $st_FOV_corner(tlx)
	set st_FOV_corner(bly) $st_FOV_corner(bry)
	
	#	Calculate zero points
	set st_FOV_zero(x) [expr $st_pxpdeg(x) * 4.5]
	set st_FOV_zero(y) [expr $st_pxpdeg(y) * 4.5]
	
	#	Set SMS FOV
	set st_sms_fov(max) [$gi SMS_FOV 3.89]
	set st_sms_fov(min) [expr -1.0 * $st_sms_fov(max)]

	#	Initialize ST VT rest positions (non-tracking)
	foreach vt {1 2 3 4 5} {
	    set st_vt_rest_x($vt) [expr 0.25 * $st_pxpdeg(x)]
	    set st_vt_rest_y($vt) [expr (1.5 * $st_pxpdeg(y)) + \
	                                ($vt * $st_pxpdeg(y))]
	    
	}    
	    
	#	create frame for each ST
	foreach st {1 2 3} {
	    set st_base($st) [frame $base_frm.st$st -relief raised \
	                                            -borderwidth 1]
	                          
	    pack $base_frm.st$st -side left

	#	create canvas.  Again use display x dimension so views appear
	#	square.
	    set xwidth $::CONFIG::display(disp_x)
	    set yheight $::CONFIG::display(disp_x)

	    set st_canv($st) [canvas $base_frm.st$st.c -bg black \
	                                               -width  $xwidth \
	                                               -height $xwidth ]
	    pack $st_canv($st) -side top \
	                       -anchor nw

	#	Now create a set of reference planes so that we can
	#	manage the stacking order (for visibility)
	#	BORDER / BACKGROUND
	
	#	Fill the canvas with a black rectangle so that
	#	Postscript has a background to print
	    $st_canv($st) create rectangle 0 0 $xwidth $xwidth \
		          -outline black \
		          -fill black \
		          -tags st_mask
	                                      
	    foreach obj $::EXT_OBJS::ext_obj_list {
	        $st_canv($st) create line 0 0 1 0 -fill black \
	                                          -tags "st_$obj\_stayout_plane"

	    }

	    $st_canv($st) create line 0 0 1 0 -fill black \
	                                      -tags "st_star_plane"
	    
	    $st_canv($st) create line 0 0 1 0 -fill black \
	                                      -tags "st_ext_plane"
	    $st_canv($st) create line 0 0 1 0 -fill black \
	                                      -tags "st_body_plane"
	    $st_canv($st) create line 0 0 1 0 -fill black \
	                                      -tags "st_FOV_plane"
	    
	}

}


########################################################################
#	Update ST objects
proc ::ST::st_update { } {
	global gi
	variable st_back_color

	foreach st {1 2 3} {	

	    ::ST::st_back_color_update $st
	    $::ST::st_canv($st) itemconfigure st_mask -fill $st_back_color($st)

	# Update the S/C body axis arrows
	# First, remove the old one S/C axis arrows
	    foreach axis {x y z} {
	        $::ST::st_canv($st) delete "st_$axis"   
	        ::ST::st_axis $st $axis
	    }

	# Get rid of the old VTs
	# Update the Virtual Trackers if not STATIC quat, and Star Tracker is on
	    $::ST::st_canv($st) delete st_vt
	    if { ($::USER_QUAT::gss_qsel != "STATIC") && \
		 ($::ST::st_onoff($st) == 1) } {
	         ::ST::st_vt $st
	    }

	# Update the SMS status 
	    ::ST::st_sms_mode $st
	
	# Update the display RA and DEC
            $::ST::st_canv($st) delete "st_radec"
	    if { $::USER_QUAT::gss_qsel_state == "VALID" } {
	        set data [ST_Boresight $st]
	        set ra  [lindex $data 0]
	        set dec [lindex $data 1]
	        ::DISPLAY::st_radec $st $ra $dec
	    }

	# Update the stars, first delete the old ones
	# Does the user want any of them displayed?
	    $::ST::st_canv($st) delete "star"
	    if { ($::ST::st_ui(stars) == "normal") && \
	         ($::USER_QUAT::gss_qsel_state == "VALID") } {
	         ::ST::st_stars $st
	    }

	# Now get rid of old celestial objects 
	# Now draw in the celestial objects if we have a valid quaternion
	    $::ST::st_canv($st) delete "ephem"
	    if {$::USER_QUAT::gss_qsel_state == "VALID"} {
	        ::ST::st_celestial_obj $st
	        
	    }

	# Remove the N/S arrow
	# Update the N/S arrow if we have a valid quaternion
	    $::ST::st_canv($st) delete st_ns
	    if { ($::ST::st_ui(north) == "normal") && \
	         ($::USER_QUAT::gss_qsel_state == "VALID") } {
	         ::ST::st_north $st
	    }
	        
	# Force update the display	    	        
            ::update idletasks

	}
		
}	    

########################################################################
#	Update ST background color
proc ::ST::st_back_color_update { st } {
	global gi
	variable st_back_color
	variable st_back_dk_lim
	variable st_back_lt_lim
	variable st_back_dk_gry
	variable st_back_lt_gry

	if { ($::CONFIG::data(tm_src) == "Planning") || \
	     ($::USER_QUAT::gss_qsel == "STATIC") } {

		set back_gray 0
		
	} else {
	
        	set key "ST_$st\_BACKGROUND_LRV"
       		set back_cts [expr [::GTACS::int_lrv_value $GTACS::lrv_idx($key)] + 0.0]
       		
		if { $back_cts <= $st_back_dk_lim } {
			set back_gray 0
		} elseif { $back_cts >= $st_back_lt_lim } {
			set back_gray $st_back_lt_gry
		} else {
			# Denom default value 580=700-120, where 120 = patch threshold and 700 = max test data
			set denom [expr ($st_back_lt_lim - $st_back_dk_lim)]
			if { $denom < 1 } {
				set denom 580.0
			}
			set ratio [expr ( ($back_cts - $st_back_dk_lim) / $denom ) ]
			set back_gray [expr $st_back_dk_gry + round( ($st_back_lt_gry - $st_back_dk_gry) * $ratio) ] 
		}
		
	}
	
	set back_color gray$back_gray
	
	set st_back_color($st) $back_color

}	    


########################################################################
#	Display the ST on/off status and SMS mode
#
#   The ST is reporting data when onoff is WARMING_UP or READY
#   The SMS mode is OFF if the state is OFF, otherwise set by mode
#
proc ::ST::st_sms_mode { st } {
	global gi
	
	if {$::CONFIG::data(tm_src) != "Planning"} {

	    set key "ST_$st\_STP_ON_OFF_LRV"
	    set onoff [string range \
	                 [::GTACS::str_lrv_value $GTACS::lrv_idx($key)] \
	                 0 2]

	    set key "ST_$st\_SMS_MODE_LRV"
	    set mode [string range \
	                 [::GTACS::str_lrv_value $GTACS::lrv_idx($key)] \
	                 0 3]

	    set key "ST_$st\_SMS_ON_OFF_LRV"
	    set state [string range \
	                  [::GTACS::str_lrv_value $GTACS::lrv_idx($key)] \
	                  0 2]

        if {$onoff == "REA"} {

            set ::ST::st_onoff($st) 1
	        
        	if {$state == "OFF"} {
            	set status "RDY-OFF"
	    	} else {
	        	set status "RDY-$mode"
    		}

    	} elseif {$onoff == "WAR"} {

            set ::ST::st_onoff($st) 1
	        
        	if {$state == "OFF"} {
            	set status "WRM-OFF"
	    	} else {
	        	set status "WRM-$mode"
    		}

    	} else {

            set ::ST::st_onoff($st) 0
	        
        	if {$state == "OFF"} {
            	set status "OFF-OFF"
	    	} else {
	        	set status "OFF-$mode"
		    }

	    }

	} else {

        if { (($st == 1) && ($::CONFIG::data(sts_active) == 23)) || \
             (($st == 2) && ($::CONFIG::data(sts_active) == 13)) || \
             (($st == 3) && ($::CONFIG::data(sts_active) == 12)) } { 
	   	    set status "OFF"
        } else {
	   	    set status "RDY"
        }

	}
	
	#	Add the number of OSC stars if the quaternion is valid
	if {$::USER_QUAT::gss_qsel_state == "VALID"} {
		set status "$status\-$::ST::st_osc_count($st)"

	#   Set color to white if every VT has an OSC star it can track
	#	Set color to yellow if some excess VTs, but above A14 fault limit
	#   Set color to red if excess VTs, and below the A14 fault limit 
		if {$::ST::st_osc_count($st) >= 5} {
			set color white
		} elseif {$::ST::st_osc_count($st) >= 2} {
			set color yellow 
		} else {
			set color red 
		}

	} else {
		set color white
	}

	$::ST::st_canv($st) delete "st_sms"
	::DISPLAY::st_sms $st "$status" $color

}

########################################################################
#	Display the S/C body X, Y, or Z arrows

proc ::ST::st_axis { st axis } {
	global gi

	set uc [string toupper $axis]
	    
	#	Update the arrow
	if {$::ST::st_ui(st_$axis) == "normal"} {
	    set arow_len   [$gi BODY_$uc\_AXIS_PROJ_LENGTH 2.0]
	    set arow_color [$gi BODY_$uc\_AXIS_PROJ_COLOR white]
	        
	    set coords [ST_$uc $st]
        
	    if {[string first "Error:" $coords] == -1} {
	        ::DISPLAY::st_body_axis $st \
	                                $axis \
	                                [lindex $coords 0] \
	                                [lindex $coords 1] \
	                                $arow_len \
	                                $arow_color
	            
	                 
	    } else {
	        ::MSG_DLG::attention "$coords" \
	                              red \
	                              black \
	                              -INSTRUCTION "Error from \
	                              ST_$uc for axis $axis"
	             
	    }
	            
	}
	
}


########################################################################
#	Draw the ST stars

proc ::ST::st_stars { st } {
	global gi
	
	set ::ST::st_osc_count($st) 0
	
	#	Get the list of visible stars
        set hv_coords [ST_Stars $st]	
            	    
	if {[string first "Error:" $hv_coords] == -1} {

            foreach block $hv_coords {

                foreach star $block {

                    set coords {}

	            set id   [lindex $star 0]
	            set degx [lindex $star 1]
	            set degy [lindex $star 2]

	            set data      [LookupStarInfo $id]
	            set mag       [lindex $data [expr $st + 2]]
	            set sky2000id [lindex $data 6]
	            set osc       [lindex $data 7]
	            set bssid     [lindex $data 8] 

	#	If it is an OSC star and within the real ST FOV count it
                    if { ($osc == 1) && \
                         ( ($degx <= $::ST::st_sms_fov(max)) && \
                           ($degx >= $::ST::st_sms_fov(min)) ) && \
                         ( ($degy <= $::ST::st_sms_fov(max)) && \
                           ($degy >= $::ST::st_sms_fov(min)) )      } {
                        incr ::ST::st_osc_count($st)
                        
                    }

	#	Decide whether the user wants them filtered out
	            if { $mag < $::ST::st_ui(mag) } {
	    
	                if { ( ($osc == 1) && \
	                       ($::ST::st_ui(osc) == "OSC") ) ||\
	                     ( $::ST::st_ui(osc) == "ALL" ) } {

                            ::DISPLAY::st_stars $st \
                                                $id \
                                                $degx \
                                                $degy \
                                                $mag \
                                                $sky2000id \
                                                $osc \
                                                $bssid 
	                }
	       
	            }
	                
	        }
	        
	    }

	} else {
	    ::MSG_DLG::attention "$hv_coords" \
	                          red \
	                          black \
	                          -INSTRUCTION "Error from \
	                          ST_stars"
	               
	}
	        
}


########################################################################
#	Draw Celestial objects

proc ::ST::st_celestial_obj { st } {
	global gi
		    
	#	First get the objects in the FOV
	set hv_coords [ST_Object $st]

	if {[string first "Error:" $hv_coords] == -1} {

            foreach block $hv_coords {

                foreach objs $block {

	            set id   [lindex $objs 0]
	            set obj  $::EXT_OBJS::ext_obj($id,str_id)
	            set degx [lindex $objs 1]
	            set degy [lindex $objs 2]
	            
	#   Check to see that at least some part of the object might be displayed
	#	Using 5.0 instead of 4.5, just to be on the safe side
	            if { (([expr $degx + $::EXT_OBJS::ext_obj(stayout_$obj,radius)] > -5.0)  && \
	                  ([expr $degx - $::EXT_OBJS::ext_obj(stayout_$obj,radius)] <  5.0)) && \
	                 (([expr $degy + $::EXT_OBJS::ext_obj(stayout_$obj,radius)] > -5.0)  && \
	                  ([expr $degy - $::EXT_OBJS::ext_obj(stayout_$obj,radius)] <  5.0)) } {
	                
    	            set uc [string toupper $obj]
	    
	#	Does user want it displayed?  Also, for earth and moon
	#	check for a valid ephemeris.
    	            if { ($obj != "earth") && \
    	                 ($obj != "moon")      } {

    	                if {$::ST::st_ui($uc) == "normal"} {

	#	Does the object have a larger stayout zone?
	                        if {$::EXT_OBJS::ext_obj($obj,radius) != \
	                            $::EXT_OBJS::ext_obj(stayout_$obj,radius)} {
	                        
	                            set radius \
	                            $::EXT_OBJS::ext_obj(stayout_$obj,radius)
	                            set color \
	                            $::EXT_OBJS::ext_obj(stayout_$obj,color)

	                            ::DISPLAY::st_object $st \
	                                                 $obj \
	                                                 "stayout" \
	                                                 $degx \
	                                                 $degy \
	                                                 $radius \
	                                                 $color
	                        
	                            set radius \
	                            $::EXT_OBJS::ext_obj($obj,radius)
	                            set color \
	                            $::EXT_OBJS::ext_obj($obj,color)

	                            ::DISPLAY::st_object $st \
	                                                 $obj \
	                                                 "object" \
	                                                 $degx \
	                                                 $degy \
	                                                 $radius \
	                                                 $color
	                                             
	                        } else {
	                            set radius \
	                            $::EXT_OBJS::ext_obj($obj,radius)
	                            set color \
	                            $::EXT_OBJS::ext_obj($obj,color)
    
    	                        ::DISPLAY::st_object $st \
    	                                             $obj \
    	                                             "object" \
    	                                             $degx \
    	                                             $degy \
    	                                             $radius \
    	                                             $color
	                                             
    	                    }
	                    
    	                }
	                    
    	            } elseif {$::CONFIG::data(valid_ephem) == "VALID"} {
	            
    	                if {$::ST::st_ui($uc) == "normal"} {
	#	Does the object have a larger stayout zone?
    	                    if {$::EXT_OBJS::ext_obj($obj,radius) != \
    	                        $::EXT_OBJS::ext_obj(stayout_$obj,radius)} {
    	                        
    	                        set radius \
    	                        $::EXT_OBJS::ext_obj(stayout_$obj,radius)
    	                        set color \
    	                        $::EXT_OBJS::ext_obj(stayout_$obj,color)

    	                        ::DISPLAY::st_object $st \
    	                                             $obj \
    	                                             "stayout" \
    	                                             $degx \
    	                                             $degy \
    	                                             $radius \
    	                                             $color
	                        
    	                        set radius \
    	                        $::EXT_OBJS::ext_obj($obj,radius)
    	                        set color \
    	                        $::EXT_OBJS::ext_obj($obj,color)

    	                        ::DISPLAY::st_object $st \
    	                                             $obj \
    	                                             "object" \
    	                                             $degx \
    	                                             $degy \
    	                                             $radius \
    	                                             $color
	                                             
    	                    } else {
    	                        set radius \
    	                        $::EXT_OBJS::ext_obj($obj,radius)
    	                        set color \
    	                        $::EXT_OBJS::ext_obj($obj,color)

    	                        ::DISPLAY::st_object $st \
    	                                             $obj \
    	                                             "object" \
    	                                             $degx \
    	                                             $degy \
    	                                             $radius \
    	                                             $color
    	                                             
    	                    }
	                    
    	                }
	                    
    	            }

				}
	                
	        }
	                   
	    }
	    
	} else {
	    ::MSG_DLG::attention "$hv_coords" \
	                          red \
	                          black \
	                          -INSTRUCTION "Error from \
	                          ST_object for object $obj"
	                
	}

}


########################################################################
#	Draw the north / south arrow

proc ::ST::st_north { st } {
	global gi
	
	set arow_len   [$gi CELESTIAL_ARROW_SIZE 2.0]
	set arow_color [$gi CELESTIAL_ARROW_COLOR white]
	        
	set hv_coords [ST_NorthAndSouth $st $arow_len]
	set num_items [llength $hv_coords]
	        
	if {$num_items > 3} {
	    ::DISPLAY::st_ns_arrow $st \
	                           [lindex $hv_coords 0] \
	                           [lindex $hv_coords 1] \
	                           [lindex $hv_coords 2] \
	                           [lindex $hv_coords 3] \
	                           $arow_color
	        
	} else {
	    ::DISPLAY::st_ns_sym $st \
	                         [lindex $hv_coords 0] \
	                         [lindex $hv_coords 1] \
	                         [lindex $hv_coords 2] \
	                         $arow_color
	                                 
	}

}

########################################################################
#	Handle display (or not) of Virtual Trackers (VTs)

proc ::ST::st_vt { st } {

	#	Check for planning mode
	if { $::CONFIG::data(tm_src) == "Planning" } {return}

	#	Check does the user want them displayed
	if { $::ST::st_ui(vt) == "hidden" } {return}

    if { ($::ST::st_ui(centroid) == "normal") } {

        set h_tot 0.0
        set v_tot 0.0
        set num_track 0

    #   Get the current status of the
        foreach vt {1 2 3 4 5} {
            set key "ST_$st\_VT_$vt\_H_LRV"
            set h [::GTACS::float_lrv_value $GTACS::lrv_idx($key)]

            set key "ST_$st\_VT_$vt\_V_LRV"
            set v [::GTACS::float_lrv_value $GTACS::lrv_idx($key)]

            set key "ST_$st\_VT_$vt\_TRACK_LRV"
            set state [::GTACS::str_lrv_value $GTACS::lrv_idx($key)]
            set track 0
            if {[string index $state 0] == "T"} {
                set track 1
            }

    #   Take care of centroid calc
            if {$track == 1} {
                set h_tot [expr $h_tot + $h]
                set v_tot [expr $v_tot + $v]
                incr num_track

            }

        }
   
    #   Complete the Centroid calc and display
   
        if { ($num_track > 0) } {

            set h [expr $h_tot / $num_track]
            set v [expr $v_tot / $num_track]
    
            ::DISPLAY::st_centroid $st $h $v

        }

    }

    #   Get the current status of the
    foreach vt {1 2 3 4 5} {
        set key "ST_$st\_VT_$vt\_H_LRV"
        set h [::GTACS::float_lrv_value $GTACS::lrv_idx($key)]

        set key "ST_$st\_VT_$vt\_V_LRV"
        set v [::GTACS::float_lrv_value $GTACS::lrv_idx($key)]

        set key "ST_$st\_VT_$vt\_MAG_LRV"
        set mag [::GTACS::float_lrv_value $GTACS::lrv_idx($key)]


        set key "ST_$st\_VT_$vt\_OSC_LRV"
        set bss_id [::GTACS::int_lrv_value $GTACS::lrv_idx($key)]
# 2/23/01       set bss_id [expr int($bss_id)]

        set key "ST_$st\_VT_$vt\_VALIDITY_LRV"
        set state [::GTACS::str_lrv_value $GTACS::lrv_idx($key)]
        set valid 0
        if {[string index $state 0] == "V"} {
            set valid 1

        }

        set key "ST_$st\_VT_$vt\_TRACK_LRV"
        set state [::GTACS::str_lrv_value $GTACS::lrv_idx($key)]
        set track 0
        if {[string index $state 0] == "T"} {
            set track 1

        }

        ::DISPLAY::st_vt $st $vt $h $v $mag $bss_id $track $valid

    }
	
}

########################################################################
#	Handle deleting or redrawing the stars

proc ::ST::toggle_all_stars { } {

	set ::ST::st_ui(temp_mag) $::ST::st_ui(mag)

	foreach st {1 2 3} {
	    if {$::ST::st_ui(stars) == "normal"} {
	    
 	        $::ST::st_canv($st) lower "star" "st_star_plane"
 		
 		::ST::toggle_nonosc_stars
 		
 		set ::ST::st_ui(prev_mag) 8.0
	        ::ST::toggle_star_mag
	         		                                 
	    } else {
	    
 	        $::ST::st_canv($st) lower "star" "st_mask"
	
	    }
	    
	}
	
}


########################################################################
#	Handle deleting or redrawing the osc stars

proc ::ST::toggle_nonosc_stars { } {

	set ::ST::st_ui(temp_mag) $::ST::st_ui(mag)

	foreach st {1 2 3} {
	    if {$::ST::st_ui(osc) == "OSC"} {
	    
 	        $::ST::st_canv($st) lower "not_osc" "st_mask"
 		     		                                 
	    } else {
	    
 	        $::ST::st_canv($st) lower "not_osc" "st_star_plane"
	 		
 		set ::ST::st_ui(prev_mag) 8.0
	        ::ST::toggle_star_mag
	        
	    }
	    
	}
	
}


########################################################################
#	Handle displaying / hiding stars by magnitude

proc ::ST::toggle_star_mag { } {

	if {[catch {expr $::ST::st_ui(temp_mag) + 0}] == 1} {
	    ::MSG_DLG::attention "Invalid Entry" \
	                          red \
	                          black \
	                          -INSTRUCTION \
	                          "Re-enter the Star Filter Magnitude" 
	    set ::ST::st_ui(temp_mag) $::ST::st_ui(mag)
	    return
	}
	
	if { $::ST::st_ui(temp_mag) > 8.0 } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                          red \
	                          black \
	                          -INSTRUCTION \
	                          "Star Filter Magnitude must be <= 8.0" 
	    set ::ST::st_ui(temp_mag) $::ST::st_ui(mag)
	    return
	}
	
	if { $::ST::st_ui(temp_mag) < -1.0 } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                          red \
	                          black \
	                          -INSTRUCTION \
	                          "Star Filter Magnitude must be >= -1.0" 
	    set ::ST::st_ui(temp_mag) $::ST::st_ui(mag)
	    return
	}
	
	set ::ST::st_ui(mag) $::ST::st_ui(temp_mag)

	if {$::ST::st_ui(mag) == $::ST::st_ui(prev_mag)} {
	    return
	    
	} elseif {$::ST::st_ui(mag) < $::ST::st_ui(prev_mag)} {
	    set start $::ST::st_ui(mag)
	    set end $::ST::st_ui(prev_mag)
	    set location "st_mask"
	    
	} else {
	    set start $::ST::st_ui(prev_mag)
	    set end $::ST::st_ui(mag)
	    set location "st_star_plane"
	    
	}

	#	Since a "for" loop won't work with floating point increments
	#	build a list of the magnitudes to deal with
	
	set count [expr ($end - $start)	/ 0.1]
	set x $start
	set mags ""
	for {set i 1} {$i <= $count} {incr i} {
	    set mags "$mags mag_[format "%3.1f" $x]"
	    set x [expr $x + 0.1]
	    
	}
	
	foreach st {1 2 3} {
	    foreach mag $mags {
	        $::ST::st_canv($st) lower $mag "$location"

	    }
	
	}

	#	Save the magnitude for next time
	set ::ST::st_ui(prev_mag) $::ST::st_ui(mag)
	
}

########################################################################
#	Handle deleting or redrawing the ST grids

proc ::ST::toggle_st_grid { } {

	if {$::ST::st_ui(st_grid) == "normal"} {
	    foreach i { 1 2 3 } {
 		    ::DISPLAY::st_grid $i
 		     		                                 
	    }
	    
	} else {
	    foreach i { 1 2 3 } {
 		    $::ST::st_canv($i) delete st_grid 
 		     		                                 
	    }
	
	}
	
}


########################################################################
#	print Star Trackers fropm first_ST to last_ST

proc ::ST::ps { first_ST last_ST } {
	global env
	variable st_back_color

	set printer $env(GSS_PRINT_CMD)

	set directory [::FILES::get_env GSS_PS_FILE_DIR ./]/[string tolower $::FILES::pf]/gss
	set dt [clock format [clock seconds] -format "j07/29/20M%S" -gmt yes]
	
	set filelist "\t "

	for {set i $first_ST} {$i <= $last_ST} {incr i} {

	    set filename "GSS_ST$i\_$dt.ps"
            set psfile $directory/$filename
	    
	    set err [catch {set outid [open $psfile w]}]
	    if {$err == 1} {
		::MSG_DLG::attention "Could not Create Star Tracker Postcript File \n$psfile" \
					red \
					black \
					-INSTRUCTION "Check file name and directory access privileges"
		return
	    } else {
		close $outid
		file delete $psfile
	    }

	    $::ST::st_canv($i) itemconfigure st_mask -fill gray80

	    $::ST::st_canv($i) postscript -file "$psfile" \
		                          -rotate no \
		                          -colormode color \
		                          -pagewidth 6.5i

	    $::ST::st_canv($i) itemconfigure st_mask -fill [set st_back_color($i)]
	    
	    set cmd "catch {exec $printer $psfile &}"
	    eval "$cmd"
	
	    set filelist "[set filelist]Filename = $filename \n\t "

	}
	
	set msg_txt "Generated ST postscript file(s): \n\
                              \t Directory = $directory \n\
                              $filelist \n\
                     Printed using command: $printer"
	
	::MSG_DLG::attention $msg_txt
	
	::MSG_DLG::msg_log $msg_txt
	
}


########################################################################

proc ::ST::user_gui { } {
	global gi
	variable orientation_list { y z } 
	variable st_ui
	variable st_ui_tag


	catch {destroy .st_ui}
	toplevel .st_ui
	wm title .st_ui "STAR TRACKER WINDOWS CONTROL"
	wm geometry .st_ui "-0-0"
	set pf .st_ui.top
	frame $pf -bg gray50
	pack $pf

	set f [frame $pf.header -bg gray50]

	set f1 [frame $pf.col1 -bg gray50]
	pack $f1 -side left \
		 -anchor nw 

	set f2 [frame $pf.col2 -bg gray50]
	pack $f2 -side left \
		 -anchor nw 

	set btn [frame .st_ui.butn]
	pack $btn -side bottom \
		  -anchor c \
		  -ipadx 10 \
		  -fill x

	#	Handle the simple cases first
	foreach plnt {Sun Earth Moon Mercury Venus Mars Jupiter Saturn \
	              Uranus Neptune Pluto} {
	    set uc_plnt [string toupper $plnt]
	    set b [checkbutton $f1.cb$plnt -text $plnt \
	                                 -anchor w \
	                                 -font $::CONFIG::fonts(entry) \
	                                 -borderwidth 1 \
	                                 -relief ridge \
	                                 -bg [$gi $uc_plnt\_COLOR grey50] \
	                                 -fg black \
 			                 -onvalue normal	\
 			                 -offvalue hidden \
 			                 -variable ::ST::st_ui($uc_plnt) \
 			                 -width 32 \
	                                 -justify right]
	    pack $b -side top \
	            -fill x 

	}

	#	Handle the comets and asteroids
	foreach obj {COMET_1 COMET_2 ASTEROID_1 ASTEROID_2 \
	              ASTEROID_3 ASTEROID_4} {
	    set b [checkbutton $f1.cb$obj -text [lindex [$gi $obj ?] 0] \
	                                -anchor w \
	                                -font $::CONFIG::fonts(entry) \
	                                -borderwidth 1 \
	                                -relief ridge \
	                                -bg [$gi $obj\_COLOR grey50] \
	                                -fg black \
 			                -onvalue normal	\
 			                -offvalue hidden \
 			                -variable ::ST::st_ui($obj) \
 			                -width 32 \
	                                -justify right]
	    pack $b -side top \
	            -fill x 

	}
	
	#	Handle the individual cases
	set c [checkbutton $f2.st_grid -text Gridlines \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -font $::CONFIG::fonts(entry) \
	                               -bg gray85 \
	                               -fg black \
	                               -borderwidth 1 \
	                               -relief ridge \
 			               -variable ::ST::st_ui(st_grid)	\
 			               -command "::ST::toggle_st_grid"	\
			               -anchor w \
			               -justify right]
			
	pack $c -side top -fill x

	set c [frame $f2.spcr1 -borderwidth 1 \
	                       -relief sunken \
	                       -background gray50]

	pack $c -side top -fill x -ipady 4

	set vt_state normal
	if {$::CONFIG::data(tm_src) == "Planning"} {
	    set vt_state disabled
	    set ::ST::st_ui(vt) hidden
	    
	}
	
	set c [checkbutton $f2.vt      -text "Virtual Trackers" \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -borderwidth 1 \
	                               -state $vt_state \
	                               -relief ridge \
	                               -font $::CONFIG::fonts(entry) \
	                               -bg gray85 \
	                               -fg black \
 			               -variable ::ST::st_ui(vt) \
 			               -anchor w \
 			               -justify right]
			
	pack $c -side top -fill x

	set cent_state normal
	if {$::CONFIG::data(tm_src) == "Planning"} {
	    set cent_state disabled
	    set ::ST::st_ui(centroid) hidden
	    
	}

	set c [checkbutton $f2.centoid -text Centroid \
 			               -onvalue normal	\
 			               -offvalue hidden \
 			               -state $cent_state \
	                               -borderwidth 1 \
	                               -relief ridge \
	                               -font $::CONFIG::fonts(entry) \
	                               -bg gray85 \
	                               -fg black \
 			               -variable ::ST::st_ui(centroid)	\
 			               -anchor w \
 			               -justify right]
			
	pack $c -side top -fill x

	set c [frame $f2.spcr2 -borderwidth 1 \
	                       -relief sunken \
	                       -background gray50]

	pack $c -side top -fill x -ipady 4

	set c [checkbutton $f2.north   -text "Celestial N/S" \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -borderwidth 1 \
	                               -relief ridge \
	                               -font $::CONFIG::fonts(entry) \
	                               -bg gray85 \
	                               -fg black \
 			               -variable ::ST::st_ui(north)	\
 			               -anchor w \
 			               -justify right]
			
	pack $c -side top -fill x

	set c [frame $f2.spcr3 -borderwidth 1 \
	                       -relief sunken \
	                       -background gray50]

	pack $c -side top -fill x -ipady 4

	set c [checkbutton $f2.st_x    -text "S/C X-axis" \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -borderwidth 1 \
	                               -relief ridge \
	                               -font $::CONFIG::fonts(entry) \
	                               -bg gray85 \
	                               -fg black \
 			               -variable ::ST::st_ui(st_x)	\
 			               -anchor w \
 			               -justify right]
			
	pack $c -side top -fill x

	set c [checkbutton $f2.st_y    -text "S/C Y-axis" \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -borderwidth 1 \
	                               -relief ridge \
	                               -font $::CONFIG::fonts(entry) \
	                               -bg gray85 \
	                               -fg black \
 			               -variable ::ST::st_ui(st_y)	\
 			               -anchor w \
 			               -justify right]
			
	pack $c -side top -fill x

	set c [checkbutton $f2.st_z    -text "S/C Z-axis" \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -borderwidth 1 \
	                               -relief ridge \
	                               -font $::CONFIG::fonts(entry) \
	                               -bg gray85 \
	                               -fg black \
 			               -variable ::ST::st_ui(st_z)	\
 			               -anchor w \
 			               -justify right]
			
	pack $c -side top -fill x

	set c [frame $f2.spcr4 -borderwidth 1 \
	                       -relief sunken \
	                       -background gray50]

	pack $c -side top -fill x -ipady 4

	#	Create a frame to pack the star info in for formatting
	frame $f2.mag -borderwidth 1 \
	              -bg gray50 \
		      -relief ridge
	pack $f2.mag -side top -fill x

	set c [checkbutton $f2.mag.stars -text "Stars" \
 			                 -onvalue normal \
 			                 -offvalue disabled \
	                                 -font $::CONFIG::fonts(entry) \
	                                 -borderwidth 1 \
	                                 -relief ridge \
	                                 -bg gray85 \
	                                 -fg black \
 			                 -variable ::ST::st_ui(stars) \
 			                 -command "$f2.mag.osc configure \
 			                           -state \$::ST::st_ui(stars); \
 			                           $f2.mag.mag.ent configure \
 			                           -state \$::ST::st_ui(stars);
 			                           $f2.mag.mag.btn configure \
 			                           -state \$::ST::st_ui(stars);\
 			                           ::ST::toggle_all_stars" \
 			                 -anchor w \
 			                 -justify right]
			    
	pack $f2.mag.stars -side top -fill x
			    	
	frame $f2.mag.spcr1 -width 10 
	pack $f2.mag.spcr1 -side left -anchor nw
	
	set c [checkbutton $f2.mag.osc -text "OSC Stars only" \
 			               -onvalue OSC \
 			               -offvalue ALL \
	                               -font $::CONFIG::fonts(entry) \
	                               -bg gray85 \
	                               -fg black \
	                               -borderwidth 1 \
	                               -relief ridge \
 			               -variable ::ST::st_ui(osc) \
 			               -anchor w \
 			               -state $::ST::st_ui(stars) \
 			               -justify right \
 			               -command "::ST::toggle_nonosc_stars"]
	
	pack $c -side top -fill x
	
	frame $f2.mag.mag -borderwidth 1 \
	                  -relief ridge \
	                  -bg gray85
	pack $f2.mag.mag -side top -fill x
	
	button $f2.mag.mag.btn -text "Filter Stars w/Mag < " \
			       -font $::CONFIG::fonts(entry) \
	                       -bg gray85 \
	                       -fg black \
			       -anchor w \
			       -command "::ST::toggle_star_mag"

        set ::ST::st_ui(temp_mag) $::ST::st_ui(mag)
	entry $f2.mag.mag.ent -width 3 \
			      -font $::CONFIG::fonts(entry) \
 			      -state $::ST::st_ui(stars) \
	                      -fg black \
			      -textvariable ::ST::st_ui(temp_mag) 
			  
	pack $f2.mag.mag.btn $f2.mag.mag.ent -side left -fill x
	
	#	Add the OK button at the bottom
	frame $btn.sep -borderwidth 1 \
		       -height 2 \
		       -relief raised
	pack $btn.sep -side top \
		      -fill x \
		      -pady 2
		      
	button $btn.ok -text OK \
		       -font $::CONFIG::fonts(label) \
		       -relief raised \
		       -command "destroy .st_ui"

	pack $btn.ok -side top \
		     -anchor c	\
		     -pady 5

}

########################################################################
#	$Log: $
########################################################################
