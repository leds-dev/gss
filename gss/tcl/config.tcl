########################################################################
#	$Id: $
########################################################################

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ::CONFIG {
	variable data
	variable display
	variable fonts
	variable keys
	variable ctl
	variable ctl_files
	variable color
	variable not_user
	variable not_bar	{}
	variable status_bar_padx
	variable control_frm_padx
	variable control_disp
	variable live_use_curr_time
	variable liveyear
	variable livemonth
	variable liveday
	variable livehour
	variable liveminute
	variable livestream
	variable hist_use_curr_time
	variable histyear
	variable histmonth
	variable histday
	variable histhour
	variable histminute
	variable histstream
	variable plan_use_curr_time
	variable planyear
	variable planmonth
	variable planday
	variable planhour
	variable planminute
	variable stream

	#	user configuration (GSS-
	#	status bar (GSS-4)
	#	snapshot (GSS-14) *partial*
	namespace export init update status_bar status_page \
			get_planning_time get_sc_init_files \
			load_ephemeris_data_from_ini \
			init_gss_time startup_screen \
			_toggle_time_label _toggle_time_status

}


########################################################################
#

proc ::CONFIG::init { } {
	variable data
	variable display
	variable fonts
	variable ctl
	variable ctl_files
	variable color
	variable keys
	variable not_user
	variable status_bar_padx
	variable control_frm_padx
	variable control_disp
	variable live_use_curr_time
	variable hist_use_curr_time
	variable plan_use_curr_time
	variable livestream	
	variable histstream
	variable stream
	
	set livestream None 
	set histstream None 
	set stream None 

	set ctl_files {gssini ssc_file}
	set data(host) [info hostname]
	
	# Set app window to 80% of mon.
	# Displays are on a 3x2 grid. 
	# All displays except skymap are limited to disp_[x/y].
	# Skymap is 3 "displays" wide.
	
	# Get Monitor display size (in pixels). Y dimension drives everything
	# so views appear square.
	
# changed ratio for widescreen monitors at request of engineers  D.Robinson  2/2019
	set display(monitor_x) [expr [winfo screenwidth .] * 0.30]
	set display(monitor_y) [expr [winfo screenheight .] * 0.60]
	set display(disp_x)    [expr $display(monitor_x) / 3.0]
	set display(disp_y)    [expr $display(monitor_y) / 2.0]
	
	set txt_width [font measure "Arial 10" -displayof . "ABCD"]

        set tk_strictMotif 0
        tk_setPalette LightGray
        option add *Text.foreground             black
        option add *Text.background             white
        option add *Listbox.background          white
        option add *Listbox.selectForeground    white  
        option add *Entry.background            white
        option add *Entry.selectBackground      black
        option add *Entry.selectForeground      white

	# Adjust fonts
	# Use smallest fonts when ABCD pixel length is largest 
	if {$txt_width > 45} {
	    set fonts(wtitle) "Helvetica 10 bold"
	    set fonts(label) "helvetica 8 bold"
	    set fonts(entry) "Helvetica 8"
	    set fonts(axis)  "Helvetica 6"
	    set fonts(st_vt) "Helvetica 6"
	    set status_bar_padx 5
	    set control_frm_padx 7
	    option add *font {Helvetica 8}
	# Use medium fonts when ABCD pixel length is medium
	} elseif {$txt_width > 25} {
	    set fonts(wtitle) "Helvetica 12 bold"
	    set fonts(label) "Helvetica 10 bold"
	    set fonts(entry) "Helvetica 10"
	    set fonts(axis) "Helvetica 8"
	    set fonts(st_vt) "Helvetica 8"
	    set status_bar_padx 4
	    set control_frm_padx 7
	    option add *font {Helvetica 10}
	# Use biggest fonts when ABCD pixel length is smallest 
	} else {
	    set fonts(wtitle) "Helvetica 12 bold"
	    set fonts(label) "Helvetica 10 bold"
	    set fonts(entry) "Helvetica 10"
	    set fonts(axis) "Helvetica 10"
	    set fonts(st_vt) "Helvetica 10"
	    set status_bar_padx 3
	    set control_frm_padx 6
	    option add *font {Helvetica 10}
	}
	
	#	stubs for other csu's
	set data(longitude) -75.0
	set data(logf) {./gss.log}
	set data(program) "GOES"

	set not_user [list host time longitude logf]

	#	user-modifiable values
	set data(yaw_flip) None 
	set data(sc_id) "13"
	set live_use_curr_time -1
	set hist_use_curr_time -1
	set plan_use_curr_time -1
	set data(use_curr_time) -1
	set data(asc_use_curr_time) -1 
	#	define the default TM source
#	set data(tm_src) Realtime
	set data(tm_src) ""
	set data(ace) 0
	set data(sts_active) "None"
	set data(tlm_control) 0
	set data(tlm_control_ip) 0
	set data(tlm_control_pid) 0

        set my_host [info hostname]
        set hid [open /etc/hosts r]
        set strlen [gets $hid str]
        
        while {$strlen != -1} {
        
            if {[string index $str 0] != "#"} {

                if {[string first $my_host $str] != -1} {
                    set x [lindex $str 0]
                    
                }
                
            }
            
            set strlen [gets $hid str]
            
        }

        close $hid
        
##	set x [lindex [exec grep [string tolower [info hostname]] /etc/hosts] 0]
	set x [split $x "."]
    set a [format "%3.3d" [lindex $x 0]]
    set b [format "%3.3d" [lindex $x 1]]
    set c [format "%3.3d" [lindex $x 2]]
    set d [format "%3.3d" [lindex $x 3]]
    set data(local_ip) "$a.$b.$c.$d"

	set data(local_pid) [pid]
	set data(valid_ephem) "VALID"
	set data(lcl_time) "00:00:00"
	set data(duty_cycle) 0.0
	
	#	ctl file colors must use same key as init entry.
	set color(gss_ini) green
	set color(ssc_file) green
	}

########################################################################
#   Clear out the live time structure	

proc ::CONFIG::live_clear_time { } {
	variable liveyear
	variable livemonth
	variable liveday
	variable livehour
	variable liveminute

	set liveyear   ""
	set livemonth  ""
	set liveday    ""
	set livehour   ""
	set liveminute ""
       
}

########################################################################
#   Clear out the hist time structure	

proc ::CONFIG::hist_clear_time { } {
	variable histyear
	variable histmonth
	variable histday
	variable histhour
	variable histminute

	set histyear   ""
	set histmonth  ""
	set histday    ""
	set histhour   ""
	set histminute ""
       
}

########################################################################
#   Clear out the plan time structure	

proc ::CONFIG::plan_clear_time { } {
	variable planyear
	variable planmonth
	variable planday
	variable planhour
	variable planminute

	set planyear   ""
	set planmonth  ""
	set planday    ""
	set planhour   ""
	set planminute ""
       
}

########################################################################
#   Clear out the asc time structure	

proc ::CONFIG::asc_clear_time { } {
	variable data

	set data(asc_year)   ""
	set data(asc_month)  ""
	set data(asc_day)    ""
	set data(asc_hour)   ""
	set data(asc_minute) ""
       
}

########################################################################
#	load asteroid & comet data from ini file into ephemeris structure

proc ::CONFIG::load_ephemeris_data_from_ini { } {

	set parms "asteroid_ini_data asteroid_1 $::GSSINI::gssini(ASTEROID_1)"
	eval $parms
	set parms "asteroid_ini_data asteroid_2 $::GSSINI::gssini(ASTEROID_2)"
	eval $parms
	set parms "asteroid_ini_data asteroid_3 $::GSSINI::gssini(ASTEROID_3)"
	eval $parms
	set parms "asteroid_ini_data asteroid_4 $::GSSINI::gssini(ASTEROID_4)"
	eval $parms
	set parms "comet_ini_data comet_1 $::GSSINI::gssini(COMET_1)"
	eval $parms
	set parms "comet_ini_data comet_2 $::GSSINI::gssini(COMET_2)"
	eval $parms
	set parms {}
       
}

########################################################################
#	load stayout zones from ini file into ephemeris structure

proc ::CONFIG::load_stayout_values { } {
	ephem_set_stayout earth	     $::GSSINI::gssini(EARTH_STAYOUT_REGION)
	ephem_set_stayout sun        $::GSSINI::gssini(SUN_STAYOUT_REGION)
	ephem_set_stayout moon	     $::GSSINI::gssini(MOON_STAYOUT_REGION)
	ephem_set_stayout mercury    $::GSSINI::gssini(MERCURY_STAYOUT_REGION)
	ephem_set_stayout venus	     $::GSSINI::gssini(VENUS_STAYOUT_REGION)
	ephem_set_stayout mars	     $::GSSINI::gssini(MARS_STAYOUT_REGION)
	ephem_set_stayout jupiter    $::GSSINI::gssini(JUPITER_STAYOUT_REGION)
	ephem_set_stayout saturn     $::GSSINI::gssini(SATURN_STAYOUT_REGION)
	ephem_set_stayout uranus     $::GSSINI::gssini(URANUS_STAYOUT_REGION)
	ephem_set_stayout neptune    $::GSSINI::gssini(NEPTUNE_STAYOUT_REGION)
	ephem_set_stayout pluto	     $::GSSINI::gssini(PLUTO_STAYOUT_REGION)
	ephem_set_stayout asteroid_1 $::GSSINI::gssini(ASTEROID_1_STAYOUT_REGION)
	ephem_set_stayout asteroid_2 $::GSSINI::gssini(ASTEROID_2_STAYOUT_REGION)
	ephem_set_stayout asteroid_3 $::GSSINI::gssini(ASTEROID_3_STAYOUT_REGION)
	ephem_set_stayout asteroid_4 $::GSSINI::gssini(ASTEROID_4_STAYOUT_REGION)
	ephem_set_stayout comet_1    $::GSSINI::gssini(COMET_1_STAYOUT_REGION)
	ephem_set_stayout comet_2    $::GSSINI::gssini(COMET_2_STAYOUT_REGION)

}

########################################################################
#	update the file locations on the initialization parameters page

proc ::CONFIG::get_sc_init_files { } {

        ::FILES::init $::CONFIG::data(sc_id)
        set ::CONFIG::data(ssc_file) $::FILES::asc_dump
        set ::CONFIG::data(gss_ini) $::FILES::new_ini

}

########################################################################
#	Return planning mode GSS time (in seconds from 1/1/70)

proc ::CONFIG::get_planning_time { } {
	variable data

	set year 1970
	catch {set year [expr $data(year) + 0]}
	set month 1
	catch {set month [expr $data(month) + 0]}
	set day 1
	catch {set day [expr $data(day) + 0]}
	set hour 0
	catch {set hour [expr $data(hour) + 0]}
	set minute 0
	catch {set minute [expr $data(minute) + 0]}

	set timestring "$month/$day/$year $hour:$minute"

	#	translate into seconds
	set newtime [clock scan $timestring -gmt yes]
	
	return $newtime
	
}

########################################################################
#	Initialize GSS Time

proc ::CONFIG::init_gss_time { } {
	variable data

	#	Calculate the offset to be applied from now on
	#	If not in planning use the value of the FRAME_TIME LRV
	#	otherwise use the system clock.
	if { $::CONFIG::data(tm_src) != "Planning" } {
            set curr_time [::GTACS::int_lrv_value \
	                   $::GTACS::lrv_idx(FRAME_TIME_LRV)]

	} else {
	    set curr_time [clock seconds]
		
	}

	if {$::CONFIG::data(use_curr_time) == 0} {
	#	Set the time offset based on the time the user stated in startup
	    set usr_req_time [::CONFIG::get_planning_time]
	    set ::CONFIG::data(time_offset)	\
	        [expr $usr_req_time - $curr_time]

	} elseif {$::CONFIG::data(use_curr_time) == 1} {
	#	Set the time offset to zero as the user stated in startup
	    set ::CONFIG::data(time_offset) 0
	    
	} else {

	#	Set the time offset based on the ACE clock (time of last major frame sync) 
        set ace_day [::GTACS::int_lrv_value $::GTACS::lrv_idx(ACE_CLOCK_DAYS_LRV)]
        set ace_sec [::GTACS::float_lrv_value $::GTACS::lrv_idx(ACE_CLOCK_SECS_LRV)]
        set mnf_cnt [::GTACS::int_lrv_value $::GTACS::lrv_idx(MNF_CNT_LRV)] 

	# unix counts sec from midnight GMT 1/1/1970; ACE counts sec from noon GMT 1/1/2000
        set unix_to_j2000 946728000

    # ace_time computed assuming a 4kbps tm (~3 sec error if using 1kbps tm)
        set ace_time [expr $unix_to_j2000 + \
                           (86400 * $ace_day) + \
                       int ($ace_sec + (1.024 * $mnf_cnt))]

        set curr_time [::GTACS::int_lrv_value $::GTACS::lrv_idx(FRAME_TIME_LRV)] 

	    set ::CONFIG::data(time_offset) [expr $ace_time - $curr_time]	

	}

	set ::CONFIG::data(gss_time)	\
	    [expr $curr_time + $::CONFIG::data(time_offset)]
	    
	set ::CONFIG::data(time)	\
	    [clock format $::CONFIG::data(gss_time) \
	                  -format "%Y/%m/%d %H:%M:%S" \
	                  -gmt yes]
	                  
	set msg_txt "INITIALIZED GSS TIME TO $::CONFIG::data(time) \n"
	::MSG_DLG::msg_log $msg_txt
				
}

########################################################################
#	set GMT
#	%Y is 4 digits, so it's Y2K-compliant.

proc ::CONFIG::gmt { } {
     variable data

     if { $::CONFIG::data(tm_src) != "Planning" } {
         set curr_time [::GTACS::int_lrv_value \
                        $::GTACS::lrv_idx(FRAME_TIME_LRV)]

     } else {
         set curr_time [clock seconds]

     }

     #    If not using static quaternion, then time advances based on time read from
     #    designated TM source, otherwise fixed at on time of static quat.  
     #    If slew GUI is active, time advances based on the spacecraft slew.

     set nom_gss_time [expr $curr_time + $::CONFIG::data(time_offset)]

     if {$::USER_QUAT::gss_qsel != "STATIC"} {

         set ::CONFIG::data(gss_time) $nom_gss_time
         
         set ::CONFIG::data(gss_time_adjust) 0

     } else {

         if {$::SLEW::slew_active == 0} {
             set ::CONFIG::data(gss_time) $::USER_QUAT::static_q_time

         } else {
             set ::CONFIG::data(gss_time) [expr $::USER_QUAT::static_q_time + \
                                                $::SLEW::slew_time_delta]
         }
         
         set ::CONFIG::data(gss_time_adjust) [expr $::CONFIG::data(gss_time) - $nom_gss_time]
         
     }

     set ::CONFIG::data(time) [clock format $::CONFIG::data(gss_time) \
                           -format "%Y/%m/%d %H:%M:%S" \
                           -gmt yes]


}


########################################################################
#	Initialize ASC Time

proc ::CONFIG::init_asc_time { } {
	variable data

	#	Calculate the offset to be applied

	if {$data(asc_use_curr_time) == 0} {
	
	    set year 1970
	    catch {set year [expr $data(asc_year) + 0]}
	    set month 1
	    catch {set month [expr $data(asc_month) + 0]}
	    set day 1
	    catch {set day [expr $data(asc_day) + 0]}
	    set hour 0
	    catch {set hour [expr $data(asc_hour) + 0]}
	    set minute 0
	    catch {set minute [expr $data(asc_minute) + 0]}

	    set timestring "$month/$day/$year $hour:$minute"

	#	translate into seconds
	    set data(asc_epoch) [clock scan $timestring -gmt yes]

	} elseif {$data(asc_use_curr_time) == 1} {
	    if { $::CONFIG::data(tm_src) != "Planning" } {
	        set data(asc_epoch) [::GTACS::int_lrv_value \
	                             $::GTACS::lrv_idx(FRAME_TIME_LRV)]
	    } else {
		set data(asc_epoch) [clock seconds]
		    
	    }

	} elseif {$data(asc_use_curr_time) == 2} {
	    set data(asc_epoch) $data(gss_time)
	    
	}

        set ::CONFIG::data(asc_time) [clock format $::CONFIG::data(asc_epoch) \
                           -format "%Y/%m/%d %H:%M:%S" \
                           -gmt yes]
                           
	set msg_txt "INITIALIZED ASC TIME TO $::CONFIG::data(asc_time) \n"
	::MSG_DLG::msg_log $msg_txt
				
}


########################################################################
#	Calculate Celestial Local Time

proc ::CONFIG::calc_local_time { longitude } {

	if {$::CONFIG::data(valid_ephem) == "VALID"} {
	    set offset [expr int( ($longitude / 15) * 3600.0)]
	    set slt [expr $::CONFIG::data(gss_time) + $offset]
	    
	    set ::CONFIG::data(lcl_time) \
	        [clock format $slt -format "%H:%M" -gmt yes]
	
	} else {
	    set ::CONFIG::data(lcl_time) "Invalid"
	    
	}

}

########################################################################

proc ::CONFIG::in_ctl { filename } {
	variable ctl

	set d [file dirname $filename]
	set x [expr abs([string compare $d $ctl(app_dir)])]
	return $ctl(color,$x)

}


########################################################################

proc ::CONFIG::status_bar { stat_frm } {
	variable data
	variable color
	variable keys

	#	Set up higher level frames to segregate
	frame $stat_frm.spcr \
	      -backgroun gray40
	pack $stat_frm.spcr \
	     -side left \
	     -anchor c \
	     -padx 2

	frame $stat_frm.a 
	pack $stat_frm.a \
	     -side left \
	     -anchor c \
	     -padx $::CONFIG::status_bar_padx

	frame $stat_frm.b 
	pack $stat_frm.b \
	     -side left \
	     -anchor c \
	     -padx $::CONFIG::status_bar_padx

	frame $stat_frm.c 
	pack $stat_frm.c \
	     -side left \
	     -anchor c \
	     -padx $::CONFIG::status_bar_padx

	frame $stat_frm.d 
	pack $stat_frm.d \
	     -side left \
	     -anchor c \
	     -padx $::CONFIG::status_bar_padx

	#	Determine background color status for INI file location
	set curr_dir [file dirname $::FILES::gssini]
	set sys_dir  [::FILES::get_env GSS_INI_FILE_DIR ""]
	
	if { [string equal $curr_dir $sys_dir] == 1 } {
	    set color(gss_ini) green
	    
	} else {
	    set color(gss_ini) red
	    
	}

	#	Display ini file status
	frame $stat_frm.a.ini_frm -borderwidth 1 \
	                          -relief ridge
	pack $stat_frm.a.ini_frm -side left \
	                         -anchor w \
	                         -padx 2
	                       
	label $stat_frm.a.ini_frm.ini -text "INI FILE" \
	                              -font $::CONFIG::fonts(entry) \
	                              -background $color(gss_ini) \
	                              -foreground black \
	                              -anchor w

	pack $stat_frm.a.ini_frm.ini -side left
	
	#	Determine background color status for SSC file location
	set curr_dir [file dirname $::FILES::ssc]
	set sys_dir  [::FILES::get_env GSS_SSC_FILE_DIR ""]
	
	if { [string equal $curr_dir $sys_dir] == 1 } {
	    set color(ssc_file) green
	    
	} else {
	    set color(ssc_file) red
	    
	}

	#	Display ssc file status
	frame $stat_frm.a.ssc_frm -borderwidth 1 \
	                          -relief ridge
	pack $stat_frm.a.ssc_frm -side left \
	                         -anchor w \
	                         -padx 2
	                       
	label $stat_frm.a.ssc_frm.ssc -text "SSC FILE" \
	                              -font $::CONFIG::fonts(entry) \
	                              -background $color(ssc_file)\
	                              -foreground black \
	                              -anchor w
	                     
	pack $stat_frm.a.ssc_frm.ssc -side left
		   
	# Display Alignment selected
	frame $stat_frm.b.align_frm -borderwidth 1 \
	                            -relief ridge
	pack $stat_frm.b.align_frm -side left \
	                           -anchor w \
	                           -padx 2

	label $stat_frm.b.align_frm.lbl1 -text " Align:" \
	                                 -font $::CONFIG::fonts(entry) \
	                                 -background gray70 \
	                                 -foreground black \
	                                 -anchor w
	                     
	label $stat_frm.b.align_frm.lbl2 -textvariable ::ALIGN::gss_align_sel \
	                                 -font $::CONFIG::fonts(entry) \
	                                 -background gray85 \
	                                 -foreground black \
	                                 -anchor w
	                     
	pack $stat_frm.b.align_frm.lbl1 \
	     $stat_frm.b.align_frm.lbl2 \
	     -side left \
	     -anchor w

	   
	# Display Quaternion selected
	frame $stat_frm.b.quat_frm -borderwidth 1 \
	                           -relief ridge
	pack $stat_frm.b.quat_frm -side left \
	                          -anchor w \
	                          -padx 2

	label $stat_frm.b.quat_frm.lbl1 -text " Quat:" \
	                                -font $::CONFIG::fonts(entry) \
	                                -background gray70 \
	                                -foreground black \
	                                -anchor w
	                     
	label $stat_frm.b.quat_frm.lbl2 -textvariable ::USER_QUAT::gss_qsel \
	                                -font $::CONFIG::fonts(entry) \
	                                -background gray85 \
	                                 -foreground black \
	                                -anchor w
	                     
	pack $stat_frm.b.quat_frm.lbl1 \
	     $stat_frm.b.quat_frm.lbl2 \
	     -side left \
	     -anchor w
	   
	# Display ST status
	frame $stat_frm.b.st_frm -borderwidth 1 \
	                         -relief ridge
	pack $stat_frm.b.st_frm -side left \
	                        -anchor w \
	                        -padx 2

	label $stat_frm.b.st_frm.st1 -text " ST:" \
	                             -font $::CONFIG::fonts(entry) \
	                             -background gray70 \
	                             -foreground black \
	                             -anchor w
	                     
	label $stat_frm.b.st_frm.st2 -textvariable ::CONFIG::data(sts_active) \
	                             -font $::CONFIG::fonts(entry) \
	                             -background gray85 \
	                             -foreground black \
	                             -anchor w
	                     
	pack $stat_frm.b.st_frm.st1 \
	     $stat_frm.b.st_frm.st2 \
	     -side left \
	     -anchor w
	   
	# Display selected ACE
	frame $stat_frm.b.ace_frm -borderwidth 1 \
	                          -relief ridge
	pack $stat_frm.b.ace_frm -side left \
	                         -anchor w \
	                         -padx 2

	label $stat_frm.b.ace_frm.ace1 -text " ACE:" \
	                               -font $::CONFIG::fonts(entry) \
	                               -background gray70 \
	                               -foreground black \
	                               -anchor w
	                     
	label $stat_frm.b.ace_frm.ace2 -textvariable ::CONFIG::data(ace) \
	                               -font $::CONFIG::fonts(entry) \
	                               -background gray85 \
	                               -foreground black \
	                               -anchor w
	                     
	pack $stat_frm.b.ace_frm.ace1 \
	     $stat_frm.b.ace_frm.ace2 \
	     -side left \
	     -anchor w

	# Display selected Yaw/Flip
	frame $stat_frm.b.yf_frm -borderwidth 1 \
	                         -relief ridge
	pack $stat_frm.b.yf_frm -side left \
	                        -anchor w \
	                        -padx 2

	label $stat_frm.b.yf_frm.yf1 -text " Flip:" \
	                             -font $::CONFIG::fonts(entry) \
	                             -background gray70 \
	                             -foreground black \
	                             -anchor w
	                     
	label $stat_frm.b.yf_frm.yf2 -textvariable ::CONFIG::data(yaw_flip) \
	                           -font $::CONFIG::fonts(entry) \
	                           -background gray85 \
	                           -foreground black \
	                           -anchor w
	                     
	pack $stat_frm.b.yf_frm.yf1 \
	     $stat_frm.b.yf_frm.yf2 \
	     -side left \
	     -anchor w

	# Display selected ASC Epoch
	# Turn into date string
	frame $stat_frm.c.asc_frm -borderwidth 1 \
	                          -relief ridge
	pack $stat_frm.c.asc_frm -side left \
	                         -anchor w \
	                         -padx 2

	label $stat_frm.c.asc_frm.asc1 -text " ASC: " \
	                               -font $::CONFIG::fonts(entry) \
	                               -background gray70 \
	                               -foreground black \
	                               -anchor w
	                     
	label $stat_frm.c.asc_frm.asc2 -text $::CONFIG::data(asc_time) \
	                               -font $::CONFIG::fonts(entry) \
	                               -background gray85 \
	                               -foreground black \
	                               -anchor w
	                     
	pack $stat_frm.c.asc_frm.asc1 \
	     $stat_frm.c.asc_frm.asc2 \
	     -side left \
	     -anchor w

	# Display selected GSS Time
	frame $stat_frm.c.time_frm -borderwidth 1 \
	                           -relief ridge
	pack $stat_frm.c.time_frm -side left \
	                          -anchor w \
	                          -padx 2

	label $stat_frm.c.time_frm.t1 -text " GSS:" \
	                              -font $::CONFIG::fonts(entry) \
	                              -background gray70 \
	                              -foreground black \
	                              -anchor w
	                     
	label $stat_frm.c.time_frm.t2 -textvariable ::CONFIG::data(time) \
	                              -font $::CONFIG::fonts(entry) \
	                              -background gray85 \
	                              -foreground black \
	                              -anchor w
	                     
	pack $stat_frm.c.time_frm.t1 \
	     $stat_frm.c.time_frm.t2 \
	     -side left \
	     -anchor w


	# Display Celestial Local Time
	frame $stat_frm.c.local_frm -borderwidth 1 \
	                            -relief ridge
	pack $stat_frm.c.local_frm -side left \
	                           -anchor w \
	                           -padx 2

	label $stat_frm.c.local_frm.t1 -text " SLT:" \
	                               -font $::CONFIG::fonts(entry) \
	                               -background gray70 \
	                               -foreground black \
	                               -anchor w
	                     
	label $stat_frm.c.local_frm.t2 -textvariable ::CONFIG::data(lcl_time) \
	                               -font $::CONFIG::fonts(entry) \
	                               -background gray85 \
	                               -foreground black \
	                               -anchor w
	                     
	pack $stat_frm.c.local_frm.t1 \
	     $stat_frm.c.local_frm.t2 \
	     -side left \
	     -anchor w

	# Display selected whether in control or not
	if {$::CONFIG::data(tlm_control) == 0} {
	    set ctl_color red1
	    set ctl_txt " InActive "
	    
	} else {
	    set ctl_color green1
	    set ctl_txt " Active "
	    
	}
	
	frame $stat_frm.d.ctl_frm -borderwidth 1 \
	                          -relief ridge
	pack $stat_frm.d.ctl_frm -side left \
	                         -anchor w

	label $stat_frm.d.ctl_frm.ctl1 -text $ctl_txt \
	                               -font $::CONFIG::fonts(entry) \
	                               -background $ctl_color \
	                               -foreground black \
	                               -anchor w

	set ::CONFIG::control_disp $stat_frm.d.ctl_frm.ctl1
		                     
	pack $stat_frm.d.ctl_frm.ctl1 \
	     -side left \
	     -anchor w


}

########################################################################

proc ::CONFIG::status_page { } {
	global gi
	variable data
	variable color

	set title(gssini)  "GSS Initialization (INI) File in use"
	set value(gssini)  $::FILES::gssini
	set lblclr(gssini) $color(gss_ini)

	set title(ssc)     "SIAD Star Catalog (SSC) File in use"
	set value(ssc)     $::FILES::ssc
	set lblclr(ssc)    $color(ssc_file)

	set title(osc)     "Latest Executable OSC Upload PROC"
	set value(osc)     $::FILES::osc(PRC)
	set lblclr(osc)    gray70

	set title(osc_txt) "Latest Human-Readable Tabular OSC"
	set value(osc_txt) $::FILES::osc(TXT)
	set lblclr(osc_txt) gray70

	set title(osc_dat) "Latest GENLOAD Input File"
	set value(osc_dat) $::FILES::osc(DAT)
	set lblclr(osc_dat) gray70

	set title(osc_dmp) "Latest Star Data Dump Comparison File"
	set value(osc_dmp) $::FILES::osc(DMP)
	set lblclr(osc_dmp) gray70

	set title(osc_struc) "Latest OSC Structure Dump Comparison File"
	set value(osc_struc) $::FILES::osc(STRUCT)
	set lblclr(osc_struc) gray70

	set title(ini)     "Latest GSS Initialization (INI) File"
	set value(ini)     $::FILES::new_ini
	set lblclr(ini)    gray70

	set title(ascf)     "Latest Human-Readable Tabular ASC"
	set value(ascf)     $::FILES::asc_dump
	set lblclr(ascf)    gray70

	set title(log)     "GSS Log File"
	set value(log)     $::FILES::log
	set lblclr(log)    gray70

	set title(host)    "Host Computer"
	set value(host)    $data(host)
	set lblclr(host)   gray70

	set title(sc)      "Spacecraft"
	set value(sc)      "$data(program) $data(sc_id)"
	
	if {($::CONFIG::data(tm_src) != "Planning")} {
	    set ini_sc_id [$gi SC_ID 0]
	    set tm_sc_id  [::GTACS::int_lrv_value $::GTACS::lrv_idx(SPACECRAFT_ID_LRV)]
	    if {($ini_sc_id == $tm_sc_id) } {
		set lblclr(sc) gray70 
	    } else {
		set lblclr(sc) red 
	    }
	} else {
	    set lblclr(sc) gray70 
	}

	set title(long)    "Spacecraft Longitude (deg East)"
	set value(long) "Invalid"
	if {$data(valid_ephem) == "VALID"} {
	    set value(long) [format "%8.3f" [GetSC_Longitude]]
	}
	set lblclr(long)   gray70

	set title(tmsrc)   "GSS Telemetry Source"
	if {($::CONFIG::data(tm_src) != "Planning")} {
	    set value(tmsrc)   "$data(tm_src): $::CONFIG::stream"
	} else {
	    set value(tmsrc)   "$data(tm_src)"
	}
	set lblclr(tmsrc)  gray70

	set title(time)    "GSS Time (GMT)"
	set value(time)    $data(time)
	set lblclr(time)   gray70

	set title(asc_time)    "ASC Epoch (GMT)"
	set value(asc_time)    $data(asc_time)
	set lblclr(asc_time)   gray70

	set title(ace)     "Selected ACE"
	set value(ace)     "ACE $data(ace)"
	set lblclr(ace)    gray70

	set title(st)      "Star Tracker Power-on Combination"
	set value(st)      $data(sts_active)
	set lblclr(st)     gray70

	set title(tmctrl)  "Output LRV Control Status"
	set value(tmctrl)  "Active"

	if {$data(tlm_control) == 0} {set value(tmctrl) "InActive"}
	set lblclr(tmctrl) green
	if {$data(tlm_control) == 0} {set lblclr(tmctrl) red}

	set title(ctlip)    "IP Address of GSS with LRV control Active"
	set value(ctlip)    $data(tlm_control_ip)
#	set lblclr(ctlip)   $lblclr(tmctrl)
	set lblclr(ctlip)   gray70

	set title(ctlpid)   "Process ID of GSS with LRV control Active"
	set value(ctlpid)   $data(tlm_control_pid)
	set lblclr(ctlpid)  $lblclr(tmctrl)
	set lblclr(ctlpid)  gray70
	
	set title(lclip)    "IP Address of Local GSS"
	set value(lclip)    $data(local_ip)
	set lblclr(lclip)   gray70

	set title(lclpid)   "Process ID of Local GSS"
	set value(lclpid)   $data(local_pid)
	set lblclr(lclpid)  gray70

	set title(quat_src)   "GSS Quaternion Source"
	set value(quat_src)   $::USER_QUAT::gss_qsel
	set lblclr(quat_src)  gray70

	set title(quat_val)   "GSS Quaternion Validity"
	set value(quat_val)   $::USER_QUAT::gss_qsel_state
	set lblclr(quat_val)  gray70

	set title(quat)   "GSS Quaternion (qb.eci)"
	set value(quat)   [format "q(1) %9.7f\tq(2) %9.7f\t\
	                           q(3) %9.7f\t q(4) %9.7f" \
	                           $::USER_QUAT::gss_quat(0) \
	                           $::USER_QUAT::gss_quat(1) \
	                           $::USER_QUAT::gss_quat(2) \
	                           $::USER_QUAT::gss_quat(3)]
	set lblclr(quat)  gray70

	set ephem_data [GetOrbitStateVector]

	set title(ephem_val)   "GSS Ephemeris Validity"
	set value(ephem_val)   $::CONFIG::data(valid_ephem)
	set lblclr(ephem_val)  gray70

	set title(ephem)   "S/C Ephemeris (qo.eci)"
	set value(ephem) [format "q(1) %9.7f\t\
	                          q(2) %9.7f\t\
	                          q(3) %9.7f\t\
	                          q(4) %9.7f\n\
	                          R \t%9.0f\tm\n\
	                          Rdot \t%9.6f\tm/s\n\
	                          w \t%9.7g\trad/s" \
	                          [lindex $ephem_data 0] \
	                          [lindex $ephem_data 1] \
	                          [lindex $ephem_data 2] \
	                          [lindex $ephem_data 3] \
	                          [lindex $ephem_data 4] \
	                          [lindex $ephem_data 5] \
	                          [lindex $ephem_data 6] ]
	set lblclr(ephem)  gray70

	set title(align)   "GSS Alignment Source"
	set value(align)   $::ALIGN::gss_align_sel
	set lblclr(align)  gray70

	#	Get the S/C local time
	set title(slt)   "S/C Local Time"
	set value(slt)   $::CONFIG::data(lcl_time)
	set lblclr(slt)  gray70

	set title(spcr1)   {}
	set value(spcr1)   {}
	set lblclr(spcr1)  gray50

	set title(spcr2)   {}
	set value(spcr2)   {}
	set lblclr(spcr2)  gray50

	set title(spcr3)   {}
	set value(spcr3)   {}
	set lblclr(spcr3)  gray50

	set title(spcr4)   {}
	set value(spcr4)   {}
	set lblclr(spcr4)  gray50

	set title(spcr5)   {}
	set value(spcr5)   {}
	set lblclr(spcr5)  gray50

	set title(spcr6)   {}
	set value(spcr6)   {}
	set lblclr(spcr6)  gray50

	set title(spcr7)   {}
	set value(spcr7)   {}
	set lblclr(spcr7)  gray50

	set title(spcr8)   {}
	set value(spcr8)   {}
	set lblclr(spcr8)  gray50

	set title(spcr9)   {}
	set value(spcr9)   {}
	set lblclr(spcr9)  gray50

	set title(spcr10)   {}
	set value(spcr10)   {}
	set lblclr(spcr10)  gray50

        set keys "gssini ssc log spcr1 ini spcr2 osc osc_dat osc_dmp osc_struc \
                  spcr3 osc_txt ascf spcr4 sc tmsrc asc_time time \
                  spcr5 host lclip lclpid spcr6 tmctrl ctlip ctlpid spcr7 \
                  ace st spcr8 quat_src quat_val quat spcr9 ephem_val \
                  ephem long slt spcr10 align"

	catch {destroy .status_page}
	toplevel .status_page
	wm title .status_page "GSS STATUS PAGE"
	wm geometry .status_page "+0+0"
	set pf .status_page.top
	frame $pf -borderwidth 1 \
	          -relief sunken
	pack $pf -side top
	
    	set fleng 0
	foreach idx $keys {
	    set txt_width [font measure "$::CONFIG::fonts(entry)" -displayof . "$value($idx)"]
	    if {$idx != "ephem"} {
		if {$txt_width > $fleng} {
		    set fleng $txt_width
		}
	    }
	}
	set fleng [expr int(($fleng/6.25) + 5) ]

	foreach idx $keys {
	    if {[string length $title($idx)] != 0} {
	    
		set f [frame $pf.$idx -borderwidth 1 \
		                      -relief ridge]

		pack $f -side top \
		        -anchor nw \
		        -padx 2
		                
		
		label $f.lbl1 -text $title($idx) \
		              -width 45 \
		              -anchor nw \
		              -justify left \
			      -font $::CONFIG::fonts(label)
				  
		label $f.lbl2 -text $value($idx) \
		              -bg $lblclr($idx) \
		              -width $fleng \
		              -anchor nw \
		              -justify left \
			      -font $::CONFIG::fonts(entry)
			      
		pack $f.lbl1 $f.lbl2 -side left \
		                     -anchor nw \
		                     -fill x

	    } else {
		set f [frame $pf.$idx -borderwidth 1 \
		                      -relief ridge \
		                      -background gray50]

		pack $f -side top \
		        -fill x \
		        -ipady 2

	    }	    
	}

	frame $pf.btn -borderwidth 1 \
	              -relief sunken
	pack $pf.btn -side top \
	             -fill x
	             
	button $pf.btn.ok -text OK \
	                  -font $::CONFIG::fonts(entry) \
	                  -command {destroy .status_page}
	pack $pf.btn.ok -side top \
	                -pady 1

}


########################################################################

proc ::CONFIG::_toggle_time_label {state args} {
	set color(ON) black
	set color(OFF) "gray70"

	foreach lbl $args {
		$lbl configure -foreground	\
				$color($state)
	}

}

########################################################################

proc ::CONFIG::_toggle_time_status { state args} {
	set status(ON) normal
	set status(OFF) disabled

	foreach ent $args {
		$ent configure -state $status($state)
	}

}

########################################################################

proc getstrms {file host type} {

    set strm_list ""

#
###     The file selection menu forces the operator to select a Pulse command file.
###     Therefore, for pulse commands we already have the filename.
#
    set cfg_file $file
    set err [catch {set inid [open $cfg_file r]
}]
    
    if {$err != 0} {
        return "rt1a rt1b smint pb1g"
        
    }


#
###     Get 1st data line
#
    set strlen [gets $inid str]

    while {$strlen>=0} {
        if {$strlen > 0} {

#
###             Fixed record format, just pull it apart
#
            set token [lindex $str 0]

#
###             If the token contains STREAM, then the stream name is 2nd param.
#
            if { $token == "STREAM" } {
            
                if {[string first $host $str] != -1} {
                    if {[string first $type $str] != -1} {
                        lappend strm_list [lindex $str 1]
                    }
                }

            }

        }
        set strlen [gets $inid str]

    }
    close $inid
    
    return $strm_list

}

########################################################################
#	Browse for INI file

proc ::CONFIG::browse_ini_file { } {

	set orig_file $::FILES::gssini

	set new_file [tk_getOpenFile \
			-initialdir [::FILES::get_env GSS_INI_FILE_DIR ""] \
			-defaultextension ini \
			-filetypes {{{INI files} {*$::CONFIG::data(sc_id)*.ini}}} ]
		  
	if {[string length $new_file] == 0} {
	    set ::FILES::gssini $orig_file
	} else {
	    set ::FILES::gssini $new_file
	}
		   
}

  
########################################################################
#	Browse for SSC file

proc ::CONFIG::browse_ssc_file { } {

	set orig_file $::FILES::ssc
	
	set new_file [tk_getOpenFile \
			-initialdir [::FILES::get_env GSS_SSC_FILE_DIR ""] \
			-defaultextension txt \
			-filetypes {{{SSC files} {*$::CONFIG::data(sc_id)*.txt} {}}} ] 
		   
	if {[string length $new_file] == 0} {
	    set ::FILES::ssc $orig_file
	} else {
	    set ::FILES::ssc $new_file
	}				   
}

  
########################################################################

proc ::CONFIG::startup_screen {} {
    variable fonts
    variable data
	global _GSS_STARTUP_wait
	global env

	foreach sel {Live Historical Planning} {
	
	    if {$sel == $::CONFIG::data(tm_src)} {
	        set rb_state($sel) normal
	        
	    } else {
	        set rb_state($sel) disabled
	        
	    }
	    
	}

#	# Build the list of streams

#	# Get directory path to epoch config file
	set hostname $data(host)

        set cfgfile_path ""
        catch {set cfgfile_path [lindex [split $env(EPOCH_REPORTS) ":"] 0]}

        if {[string length $cfgfile_path] == 0} {
             set cfgfile_path "/export/home/ssgstest/epoch/database/reports"

        }
	
        # Set main window to be in upper left corner of screen & set name 
	wm geometry . "+0+0"
	wm title . "GSS VERSION $::MAIN::prog_rev INITIALIZATION"

	# create base frame for our display
	set base .startup_screen
	frame $base -borderwidth 8 \
	            -relief ridge

	pack $base -side top
	
	# create frames for S/C, DB files, TM source, ASC time, ACE selection
	# Star Tracker selection, and OK/EXIT buttons.	
	frame $base.sc    -borderwidth 4
	frame $base.files -borderwidth 4
	frame $base.tm    -borderwidth 4
	frame $base.asc   -borderwidth 4
	frame $base.ace   -borderwidth 4
	frame $base.st    -borderwidth 4
	frame $base.but   -borderwidth 6

	pack $base.sc $base.files $base.tm $base.asc $base.ace $base.st $base.but\
		-side top \
		-anchor w \
		-fill x

#
###	### S/C selection 
#
	# Section label
	label $base.sc.lbl1	\
		-text Spacecraft \
		-font $fonts(label) \
	        -anchor w

	pack $base.sc.lbl1	\
		-side left \
		-fill x \
		-pady 3 \
		-anchor w

	# radiobutton row label
	label $base.sc.lbl2	\
		-text GOES \
		-font $fonts(entry) \
		-anchor e

	# Create radio button selections
	radiobutton $base.sc.n \
                -text "13" \
                -value "13" \
                -indicatoron yes \
                -variable ::CONFIG::data(sc_id) \
                -borderwidth 2 \
                -relief ridge \
                -font $fonts(entry) \
                -anchor e \
                -command "::FILES::set_ini; ::FILES::set_ssc"

	radiobutton $base.sc.o \
                -text "14" \
                -value "14" \
                -indicatoron yes \
                -variable ::CONFIG::data(sc_id) \
                -borderwidth 2 \
                -relief ridge \
                -font $fonts(entry) \
                -anchor e \
                -command "::FILES::set_ini; ::FILES::set_ssc"

	radiobutton $base.sc.p \
                -text "15" \
                -value "15" \
                -indicatoron yes \
                -variable ::CONFIG::data(sc_id) \
                -borderwidth 2 \
                -relief ridge \
                -font $fonts(entry) \
                -anchor e \
                -command "::FILES::set_ini; ::FILES::set_ssc"

	radiobutton $base.sc.q \
		-text "16" \
		-value "16" \
		-indicatoron yes \
		-variable ::CONFIG::data(sc_id) \
		-borderwidth 2 \
		-relief ridge \
		-font $fonts(entry) \
                -anchor e \
                -command "::FILES::set_ini; ::FILES::set_ssc"

        radiobutton $base.sc.r \
                -text "17" \
                -value "17" \
                -indicatoron yes \
                -variable ::CONFIG::data(sc_id) \
                -borderwidth 2 \
                -relief ridge \
                -font $fonts(entry) \
                -anchor e \
                -command "::FILES::set_ini; ::FILES::set_ssc"

        radiobutton $base.sc.s\
                -text "18" \
                -value "18" \
                -indicatoron yes \
                -variable ::CONFIG::data(sc_id) \
                -borderwidth 2 \
                -relief ridge \
                -font $fonts(entry) \
                -anchor e \
                -command "::FILES::set_ini; ::FILES::set_ssc"

	# Display data
	pack $base.sc.s $base.sc.r $base.sc.q $base.sc.p $base.sc.o \
		$base.sc.n $base.sc.lbl2 \
		-side right \
		-padx 1 \
		-anchor e

#
###	### DB file selection
#
	# to control display of information, create two frames
	frame $base.files.gss

	frame $base.files.ssc

	pack $base.files.gss $base.files.ssc \
		-side top \
		-pady 3 \
		-anchor w \
		-fill x

	# Create label for GSS.INI file section
	label $base.files.gss.lbl \
		-text {GSS Ini File} \
		-font $fonts(label) \
		-anchor w

	# Create entry field for INI file path and namea
	entry $base.files.gss.ent \
		-cursor {} \
		-justify left \
		-width 64 \
		-textvariable ::FILES::gssini \
		-font $fonts(entry)

        set brse ""
	set err [catch {set brse [string toupper $env(GSS_DB_BROWSE_ENABLE)]}]

	if {$err != 0} {
	    set brse "normal"
	    
	} elseif {$brse == "YES"} {
	    set brse "normal"
	    
	} else {
	    set brse "disabled"
	    
	}
	
	# Create browse request button
	button $base.files.gss.but \
		-text Browse \
		-font $fonts(entry) \
		-relief raised \
		-anchor e \
		-state $brse \
		-command { ::CONFIG::browse_ini_file }
		
	# Display
	pack $base.files.gss.lbl \
		-side left \
		-anchor w \
		-fill x

	pack $base.files.gss.but $base.files.gss.ent \
		-side right \
		-padx 2 \
		-anchor e
	
	# Create label for SSC file selection
	label $base.files.ssc.lbl \
		-text {SIAD Star Catalog File} \
		-font $fonts(label) \
		-anchor w

	# Create entry field for SSC file and path
	entry $base.files.ssc.ent \
		-cursor {} \
		-justify left \
		-width 64 \
		-textvariable ::FILES::ssc \
		-font $fonts(entry)

	set ssc_dir  [::FILES::get_env GSS_SSC_FILE_DIR ""]

	# Create browse button
	button $base.files.ssc.but \
		-text Browse \
		-font $fonts(entry) \
		-relief raised \
		-anchor e \
		-state $brse \
		-command { ::CONFIG::browse_ssc_file }

	# Display
	pack $base.files.ssc.lbl \
		-side left \
		-anchor w \
		-fill x

	pack $base.files.ssc.but $base.files.ssc.ent \
		-side right \
		-padx 2 \
		-anchor e
#
###	### Create frame for TM source selections
#
	# Create section separator
	frame $base.tm.sep1 \
		-borderwidth 1 \
		-height 2 \
		-relief raised

	pack $base.tm.sep1 \
		-side top \
		-fill x \
		-pady 1

	# Create section label at top
	label $base.tm.lbl1 \
		-text {Telemetry Configuration} \
		-font $fonts(label) \
		-anchor w

	pack $base.tm.lbl1 \
		-anchor w \
		-side top

	# We want the separate sections indented so create a frame to force
	# indentation of other frames to follow
	frame $base.tm.spcr1 \
		-width 10

	pack $base.tm.spcr1 \
		-anchor nw \
		-side left \
		-fill y

	# Live TM 
	frame $base.tm.live

	pack $base.tm.live \
		-anchor nw \
		-side top \
		-fill x
		
	# Selection
	frame $base.tm.live.rt
	pack $base.tm.live.rt \
	     -anchor nw \
	     -side top \
	     -fill x

	# Verify epoch.cfg file is there.  If not disable this option
	set strms "EMPTY"
	if {[file exists $cfgfile_path/epoch.cfg]} {
	    set live_state normal

	# Its there, get the realtime streams.  If none disable selection
	    set strms [getstrms $cfgfile_path/epoch.cfg $hostname REALTIME]
	    
	    if {[string length $strms] == 0} {
	        set strms "EMPTY"
	        ::MSG_DLG::attention "No realtime streams \
	                              found in $cfgfile_path/epoch.cfg" \
	                              red \
	                              black \
	                              -INSTRUCTION \
	                              "Live selection disabled" 
	        set live_state disabled
	        
	    }
	    
	} else {
	    ::MSG_DLG::attention "Cannot access $cfgfile_path/epoch.cfg" \
	                          red \
	                          black \
	                          -INSTRUCTION \
	                          "File does not exist or\
	                          insufficient access privileges\n\
	                          Live and Historical selections disabled" 

	    set live_state disabled
	    
	}
	
	# Create radiobutton for selection of Live TM
	radiobutton $base.tm.live.rt.rb \
		-font $fonts(entry) \
		-text Live \
		-width 11 \
		-indicatoron yes \
		-anchor w \
		-value Realtime \
		-state $live_state \
		-variable ::CONFIG::data(tm_src) \
		-command "$base.tm.live.zo.rb configure -state normal; \
		          $base.tm.live.ao.rb configure -state normal; \
		          $base.tm.live.uo.rb configure -state normal; \
		          $base.tm.live.rt.stream configure -state normal; \
		          $base.tm.hist.zo.rb configure -state disabled; \
		          $base.tm.hist.ao.rb configure -state disabled; \
		          $base.tm.hist.uo.rb configure -state disabled; \
		          $base.tm.hist.rt.stream configure -state disabled; \
		          $base.tm.plan.zo.rb configure -state disabled; \
		          $base.tm.plan.uo.rb configure -state disabled; \
		          $base.tm.plan.rb1 configure -state disabled; \
		          $base.tm.plan.rb2 configure -state disabled; \
		          ::CONFIG::hist_clear_time; \
		          ::CONFIG::plan_clear_time; \
		          set ::CONFIG::hist_use_curr_time -1;\
		          set ::CONFIG::plan_use_curr_time -1;\
		          set ::CONFIG::data(yaw_flip) None;\
		          set ::CONFIG::histstream None;\
		          ::CONFIG::_toggle_time_label OFF \
			     	$base.tm.hist.lbl1 \
			     	$base.tm.hist.rt.lbl \
				    $base.tm.hist.uo.lbl1 \
				    $base.tm.hist.uo.lbl2 \
				    $base.tm.hist.uo.lbl3 \
				    $base.tm.hist.uo.lbl4 \
				    $base.tm.hist.uo.lbl5 \
				    $base.tm.plan.uo.lbl1 \
				    $base.tm.plan.uo.lbl2 \
				    $base.tm.plan.uo.lbl3 \
				    $base.tm.plan.uo.lbl4 \
				    $base.tm.plan.uo.lbl5 \
			     	$base.tm.plan.lbl1 \
			     	$base.tm.plan.lbl2; \
		          ::CONFIG::_toggle_time_label ON \
			     	$base.tm.live.rt.lbl \
			     	$base.tm.live.lbl1 "

	label $base.tm.live.rt.lbl \
	      -text "Stream" \
	      -width 10 \
	      -font $fonts(entry) \
		  -foreground gray70 \
	      -justify right \
	      -anchor e
	     
        set cmdstr "tk_optionMenu $base.tm.live.rt.stream \
                    ::CONFIG::livestream \
                    $strms"
        eval $cmdstr
        
        $base.tm.live.rt.stream configure -font $fonts(entry) \
                                          -width 10 \
                                          -state disabled

	pack $base.tm.live.rt.rb \
		-anchor w \
		-side left

	pack $base.tm.live.rt.lbl \
	     -side left \
	     -anchor w

	pack $base.tm.live.rt.stream \
		-anchor w \
		-side left

	# Create another spacer to force indentation
	frame $base.tm.live.spcr1 \
		-width 30

	pack $base.tm.live.spcr1 \
		-anchor nw \
		-side left \
		-fill y
#	pack $base.tm.live.spcr1 \
#		-anchor nw \
#		-side top \
#		-fill y

	# Give the section a title
	label $base.tm.live.lbl1 \
		-font $fonts(entry) \
		-foreground gray70 \
		-anchor w \
		-text "TIME"

	pack $base.tm.live.lbl1 \
		-side top \
		-anchor w \
		-fill x

	# Create a frame for specification of Zero time offset
	frame $base.tm.live.zo

	pack $base.tm.live.zo \
		-anchor w \
		-side top \
		-fill x

	# Create a frame for specification of offset to ACE time
	frame $base.tm.live.ao

	pack $base.tm.live.ao \
		-anchor w \
		-side top \
		-fill x

	# Create a frame for user specified time
	frame $base.tm.live.uo

	pack $base.tm.live.uo \
		-anchor w \
		-side top \
		-fill x
	
	# For user entry, create entry fields for Y, M, D, H, M
	label $base.tm.live.uo.lbl1	\
		-text "YYYY" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.live.uo.ent1	\
		-textvariable ::CONFIG::liveyear \
		-font $fonts(entry) \
		-width 4 \
		-state disabled

	label $base.tm.live.uo.lbl2	\
		-text "/MM" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.live.uo.ent2	\
		-textvariable ::CONFIG::livemonth \
		-font $fonts(entry) \
		-width 2 \
		-state disabled

	label $base.tm.live.uo.lbl3	\
		-text "/DD" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.live.uo.ent3	\
		-textvariable ::CONFIG::liveday \
		-font $fonts(entry) \
		-width 2 \
		-state disabled

	label $base.tm.live.uo.lbl4	\
		-text " HH" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.live.uo.ent4	\
		-textvariable ::CONFIG::livehour \
		-font $fonts(entry) \
		-width 2 \
		-state disabled

	label $base.tm.live.uo.lbl5	\
		-text ":MM" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.live.uo.ent5	\
		-textvariable ::CONFIG::liveminute \
		-font $fonts(entry) \
		-width 2 \
		-state disabled

	# Create a radiobutton to allow for selection of zero offset
	# This button also controls whether the user entry option is 
	# enabled
	radiobutton $base.tm.live.zo.rb \
		-font $fonts(entry) \
		-text "Zero Offset" \
		-indicatoron yes \
		-state $rb_state(Live) \
		-value 1 \
		-variable ::CONFIG::live_use_curr_time \
		-command "::CONFIG::_toggle_time_label OFF \
				$base.tm.live.uo.lbl1 \
				$base.tm.live.uo.lbl2 \
				$base.tm.live.uo.lbl3 \
				$base.tm.live.uo.lbl4 \
				$base.tm.live.uo.lbl5 \
				; \
			    ::CONFIG::_toggle_time_status OFF \
				$base.tm.live.uo.ent1 \
				$base.tm.live.uo.ent2 \
				$base.tm.live.uo.ent3 \
				$base.tm.live.uo.ent4 \
				$base.tm.live.uo.ent5 \
				; \
		        ::CONFIG::live_clear_time"

	# Create a radiobutton to allow for selection of offset to ACE tim
	# This button also controls whether the user entry option is 
	# enabled
	radiobutton $base.tm.live.ao.rb \
		-font $fonts(entry) \
		-text "ACE Clock" \
		-indicatoron yes \
		-state $rb_state(Live) \
		-value 2 \
		-variable ::CONFIG::live_use_curr_time \
		-command "::CONFIG::_toggle_time_label OFF \
				$base.tm.live.uo.lbl1 \
				$base.tm.live.uo.lbl2 \
				$base.tm.live.uo.lbl3 \
				$base.tm.live.uo.lbl4 \
				$base.tm.live.uo.lbl5 \
				; \
			  ::CONFIG::_toggle_time_status OFF \
				$base.tm.live.uo.ent1 \
				$base.tm.live.uo.ent2 \
				$base.tm.live.uo.ent3 \
				$base.tm.live.uo.ent4 \
				$base.tm.live.uo.ent5 \
				; \
		        ::CONFIG::live_clear_time"

	# Create radio button for selection of user input
	radiobutton $base.tm.live.uo.rb \
		-font $fonts(entry) \
		-text "Offset to (GMT):" \
		-value 0 \
		-indicatoron yes \
		-state $rb_state(Live) \
		-variable ::CONFIG::live_use_curr_time \
		-command "::CONFIG::_toggle_time_label ON \
			$base.tm.live.uo.lbl1 \
			$base.tm.live.uo.lbl2 \
			$base.tm.live.uo.lbl3 \
			$base.tm.live.uo.lbl4 \
			$base.tm.live.uo.lbl5 \
			; \
			::CONFIG::_toggle_time_status ON \
			$base.tm.live.uo.ent1 \
			$base.tm.live.uo.ent2 \
			$base.tm.live.uo.ent3 \
			$base.tm.live.uo.ent4 \
			$base.tm.live.uo.ent5"

	# Display
	pack $base.tm.live.zo.rb \
		-anchor w \
		-side left \
		-fill x

	pack $base.tm.live.ao.rb \
		-anchor w \
		-side left \
		-fill x

	pack $base.tm.live.uo.rb \
		-anchor w \
		-side left \
		-fill x

	pack $base.tm.live.uo.ent5 $base.tm.live.uo.lbl5 \
		$base.tm.live.uo.ent4 $base.tm.live.uo.lbl4 \
		$base.tm.live.uo.ent3 $base.tm.live.uo.lbl3 \
		$base.tm.live.uo.ent2 $base.tm.live.uo.lbl2 \
		$base.tm.live.uo.ent1 $base.tm.live.uo.lbl1 \
		-anchor e \
		-side right

	### Historical (playback) TM source
	frame $base.tm.hist

	pack $base.tm.hist \
		-anchor nw \
		-side top \
		-fill x

	frame $base.tm.hist.rt

	pack $base.tm.hist.rt \
		-anchor nw \
		-side top \
		-fill x

	# Verify epoch.cfg file is there.  If not disable this option
	set strms "EMPTY"
	if {[file exists $cfgfile_path/epoch.cfg]} {
	    set hist_state normal

	# Its there, get the playback streams.  If none disable selection
	    set strms [getstrms $cfgfile_path/epoch.cfg $hostname PLAYBACK]
	    
	    if {[string length $strms] == 0} {
	        set strms "EMPTY"
	        ::MSG_DLG::attention "No playback streams found in \
	                              $cfgfile_path/epoch.cfg" \
	                              red \
	                              black \
	                              -INSTRUCTION \
	                              "Historical selection disabled" 
	        set hist_state disabled
	        
	    }
	    
	} else {
	    set hist_state disabled
	    
	}

	# Create radiobutton for selection of Historical TM mode
	radiobutton $base.tm.hist.rt.rb \
		-font $fonts(entry) \
		-text Historical \
		-width 11 \
		-indicatoron yes \
		-anchor w \
		-value Historical \
		-state $hist_state \
		-variable ::CONFIG::data(tm_src) \
		-command "$base.tm.live.zo.rb configure -state disabled; \
		          $base.tm.live.ao.rb configure -state disabled; \
		          $base.tm.live.uo.rb configure -state disabled; \
		          $base.tm.live.rt.stream configure -state disabled; \
		          $base.tm.hist.zo.rb configure -state normal; \
		          $base.tm.hist.ao.rb configure -state normal; \
		          $base.tm.hist.uo.rb configure -state normal; \
		          $base.tm.hist.rt.stream configure -state normal; \
		          $base.tm.plan.zo.rb configure -state disabled; \
		          $base.tm.plan.uo.rb configure -state disabled; \
		          $base.tm.plan.rb1 configure -state disabled; \
		          $base.tm.plan.rb2 configure -state disabled; \
		          ::CONFIG::live_clear_time; \
		          ::CONFIG::plan_clear_time; \
		          set ::CONFIG::live_use_curr_time -1;\
		          set ::CONFIG::plan_use_curr_time -1;\
		          set ::CONFIG::data(yaw_flip) None;\
		          set ::CONFIG::livestream None;\
		          ::CONFIG::_toggle_time_label OFF \
			     	$base.tm.live.lbl1 \
			     	$base.tm.live.rt.lbl \
				    $base.tm.live.uo.lbl1 \
				    $base.tm.live.uo.lbl2 \
				    $base.tm.live.uo.lbl3 \
				    $base.tm.live.uo.lbl4 \
				    $base.tm.live.uo.lbl5 \
				    $base.tm.plan.uo.lbl1 \
				    $base.tm.plan.uo.lbl2 \
				    $base.tm.plan.uo.lbl3 \
				    $base.tm.plan.uo.lbl4 \
				    $base.tm.plan.uo.lbl5 \
			     	$base.tm.plan.lbl1 \
			     	$base.tm.plan.lbl2; \
		          ::CONFIG::_toggle_time_label ON \
			     	$base.tm.hist.rt.lbl \
			     	$base.tm.hist.lbl1 "

	label $base.tm.hist.rt.lbl \
	      -text "Stream" \
	      -width 10 \
	      -font $fonts(entry) \
		  -foreground gray70 \
	      -justify right \
	      -anchor e

        set cmdstr "tk_optionMenu $base.tm.hist.rt.stream \
                    ::CONFIG::histstream \
                    $strms"
        eval $cmdstr
        
        $base.tm.hist.rt.stream configure -font $fonts(entry) \
                                          -width 10 \
                                          -state disabled

	pack $base.tm.hist.rt.rb \
		-anchor w \
		-side left

	pack $base.tm.hist.rt.lbl \
		-anchor e \
		-side left

	pack $base.tm.hist.rt.stream \
		-anchor w \
		-side left

	# Create spacer frame for indentation
	frame $base.tm.hist.spcr1 \
		-width 30

	pack $base.tm.hist.spcr1 \
		-anchor nw \
		-side left \
		-fill y

	# Give the section a label
	label $base.tm.hist.lbl1 \
		-font $fonts(entry) \
		-foreground gray70 \
		-anchor w \
		-text "TIME"

	pack $base.tm.hist.lbl1 \
		-side top \
		-anchor w \
		-fill x

	# Create a frame for specification of Zero time offset
	frame $base.tm.hist.zo

	pack $base.tm.hist.zo \
		-anchor w \
		-side top \
		-fill x

	# Create a frame for specification of offset to ACE time
	frame $base.tm.hist.ao

	pack $base.tm.hist.ao \
		-anchor w \
		-side top \
		-fill x

	# Create a frame for user specified time
	frame $base.tm.hist.uo

	pack $base.tm.hist.uo \
		-anchor w \
		-side top \
		-fill x

	# Create labels and entry fields for Y, M, D, H, M
	label $base.tm.hist.uo.lbl1	\
		-text "YYYY" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.hist.uo.ent1	\
		-textvariable ::CONFIG::histyear \
		-font $fonts(entry) \
		-width 4 \
		-state disabled

	label $base.tm.hist.uo.lbl2	\
		-text "/MM" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.hist.uo.ent2	\
		-textvariable ::CONFIG::histmonth \
		-font $fonts(entry) \
		-width 2 \
		-state disabled

	label $base.tm.hist.uo.lbl3	\
		-text "/DD" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.hist.uo.ent3	\
		-textvariable ::CONFIG::histday \
		-font $fonts(entry) \
		-width 2 \
		-state disabled

	label $base.tm.hist.uo.lbl4	\
		-text " HH" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.hist.uo.ent4	\
		-textvariable ::CONFIG::histhour \
		-font $fonts(entry) \
		-width 2 \
		-state disabled

	label $base.tm.hist.uo.lbl5	\
		-text ":MM" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.hist.uo.ent5	\
		-textvariable ::CONFIG::histminute \
		-font $fonts(entry) \
		-width 2 \
		-state disabled

	# Create radiobutton for controlling whether Zero time offset or
	# user specified time.  This also controls the visibility of the
	# user entry option.
	radiobutton $base.tm.hist.zo.rb \
		-font $fonts(entry) \
		-text "Zero Offset" \
		-indicatoron yes \
		-value 1 \
		-state $rb_state(Historical) \
		-variable ::CONFIG::hist_use_curr_time \
		-command "::CONFIG::_toggle_time_label OFF \
			    $base.tm.hist.uo.lbl1 \
			    $base.tm.hist.uo.lbl2 \
			    $base.tm.hist.uo.lbl3 \
			    $base.tm.hist.uo.lbl4 \
			    $base.tm.hist.uo.lbl5 \
			    ; \
			    ::CONFIG::_toggle_time_status OFF \
			    $base.tm.hist.uo.ent1 \
			    $base.tm.hist.uo.ent2 \
			    $base.tm.hist.uo.ent3 \
			    $base.tm.hist.uo.ent4 \
			    $base.tm.hist.uo.ent5 \
				; \
		        ::CONFIG::hist_clear_time"

	# Create radiobutton for controlling whether Zero time offset or
	# user specified time.  This also controls the visibility of the
	# user entry option.
	radiobutton $base.tm.hist.ao.rb \
		-font $fonts(entry) \
		-text "ACE Clock" \
		-indicatoron yes \
		-value 2 \
		-state $rb_state(Historical) \
		-variable ::CONFIG::hist_use_curr_time \
		-command "::CONFIG::_toggle_time_label OFF \
			    $base.tm.hist.uo.lbl1 \
			    $base.tm.hist.uo.lbl2 \
			    $base.tm.hist.uo.lbl3 \
			    $base.tm.hist.uo.lbl4 \
			    $base.tm.hist.uo.lbl5 \
			    ; \
			    ::CONFIG::_toggle_time_status OFF \
			    $base.tm.hist.uo.ent1 \
			    $base.tm.hist.uo.ent2 \
			    $base.tm.hist.uo.ent3 \
			    $base.tm.hist.uo.ent4 \
			    $base.tm.hist.uo.ent5 \
				; \
		        ::CONFIG::hist_clear_time"

	# Create radiobutton for selecting User entry
	radiobutton $base.tm.hist.uo.rb \
		-font $fonts(entry) \
		-text "Offset to (GMT):" \
		-value 0 \
		-indicatoron yes \
		-state $rb_state(Historical) \
		-variable ::CONFIG::hist_use_curr_time \
		-command "::CONFIG::_toggle_time_label ON \
			    $base.tm.hist.uo.lbl1 \
			    $base.tm.hist.uo.lbl2 \
			    $base.tm.hist.uo.lbl3 \
			    $base.tm.hist.uo.lbl4 \
			    $base.tm.hist.uo.lbl5 \
			    ; \
			    ::CONFIG::_toggle_time_status ON \
			    $base.tm.hist.uo.ent1 \
			    $base.tm.hist.uo.ent2 \
			    $base.tm.hist.uo.ent3 \
			    $base.tm.hist.uo.ent4 \
			    $base.tm.hist.uo.ent5"

	# Display
	pack $base.tm.hist.zo.rb \
		-anchor w \
		-side left \
		-fill x

	pack $base.tm.hist.ao.rb \
		-anchor w \
		-side left \
		-fill x

	pack $base.tm.hist.uo.rb \
		-anchor w \
		-side left \
		-fill x

	pack $base.tm.hist.uo.ent5 $base.tm.hist.uo.lbl5 \
		$base.tm.hist.uo.ent4 $base.tm.hist.uo.lbl4 \
		$base.tm.hist.uo.ent3 $base.tm.hist.uo.lbl3 \
		$base.tm.hist.uo.ent2 $base.tm.hist.uo.lbl2 \
		$base.tm.hist.uo.ent1 $base.tm.hist.uo.lbl1 \
		-anchor e \
		-side right

	### Planning TM source (ephemeris)
	frame $base.tm.plan

	pack $base.tm.plan \
		-anchor nw \
		-side top \
		-fill x

	# Create radiobutton for selecting Planning mode
	radiobutton $base.tm.plan.rt \
		-font $fonts(entry) \
		-text Planning \
		-indicatoron yes \
		-anchor w \
		-value Planning \
		-variable ::CONFIG::data(tm_src) \
		-command "$base.tm.live.zo.rb configure -state disabled; \
		          $base.tm.live.ao.rb configure -state disabled; \
		          $base.tm.live.uo.rb configure -state disabled; \
		          $base.tm.live.rt.stream configure -state disabled; \
		          $base.tm.hist.zo.rb configure -state disabled; \
		          $base.tm.hist.ao.rb configure -state disabled; \
		          $base.tm.hist.uo.rb configure -state disabled; \
		          $base.tm.hist.rt.stream configure -state disabled; \
		          $base.tm.plan.zo.rb configure -state normal; \
		          $base.tm.plan.uo.rb configure -state normal; \
		          $base.tm.plan.rb1 configure -state normal; \
		          $base.tm.plan.rb2 configure -state normal; \
		          ::CONFIG::live_clear_time; \
		          ::CONFIG::hist_clear_time; \
		          set ::CONFIG::live_use_curr_time -1;\
		          set ::CONFIG::hist_use_curr_time -1;\
		          set ::CONFIG::livestream None;\
		          set ::CONFIG::histstream None;\
		          ::CONFIG::_toggle_time_label OFF \
			     	$base.tm.live.lbl1 \
			     	$base.tm.live.rt.lbl \
				    $base.tm.live.uo.lbl1 \
				    $base.tm.live.uo.lbl2 \
				    $base.tm.live.uo.lbl3 \
				    $base.tm.live.uo.lbl4 \
				    $base.tm.live.uo.lbl5 \
				    $base.tm.hist.uo.lbl1 \
				    $base.tm.hist.uo.lbl2 \
				    $base.tm.hist.uo.lbl3 \
				    $base.tm.hist.uo.lbl4 \
				    $base.tm.hist.uo.lbl5 \
			     	$base.tm.hist.lbl1 \
			     	$base.tm.hist.rt.lbl; \
		          ::CONFIG::_toggle_time_label ON \
			     	$base.tm.plan.lbl1 \
			     	$base.tm.plan.lbl2 "


	pack $base.tm.plan.rt \
		-anchor w \
		-side top

	# Create a spacer frame for indentation
	frame $base.tm.plan.spcr1 \
		-width 30

	pack $base.tm.plan.spcr1 \
		-anchor nw \
		-side left \
		-fill y

	# Give it a label
	label $base.tm.plan.lbl1 \
		-font $fonts(entry) \
		-anchor w \
		-foreground gray70 \
		-text "TIME"

	pack $base.tm.plan.lbl1 \
		-side top \
		-anchor w \
		-fill x

	# Create frame for zero offset
	frame $base.tm.plan.zo

	pack $base.tm.plan.zo \
		-anchor w \
		-side top \
		-fill x
		
	# Create frame for user specified
	frame $base.tm.plan.uo

	pack $base.tm.plan.uo \
		-anchor w \
		-side top \
		-fill x

	# Create labels and entry fields for Y, M, D, H, M
	label $base.tm.plan.uo.lbl1	\
		-text "YYYY" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.plan.uo.ent1	\
		-textvariable ::CONFIG::planyear \
		-font $fonts(entry) \
		-width 4 \
		-state disabled

	label $base.tm.plan.uo.lbl2	\
		-text "/MM" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.plan.uo.ent2	\
		-textvariable ::CONFIG::planmonth \
		-font $fonts(entry) \
		-width 2 \
		-state disabled

	label $base.tm.plan.uo.lbl3	\
		-text "/DD" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.plan.uo.ent3	\
		-textvariable ::CONFIG::planday \
		-font $fonts(entry) \
		-width 2 \
		-state disabled

	label $base.tm.plan.uo.lbl4	\
		-text " HH" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.plan.uo.ent4	\
		-textvariable ::CONFIG::planhour \
		-font $fonts(entry) \
		-width 2 \
		-state disabled

	label $base.tm.plan.uo.lbl5	\
		-text ":MM" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.tm.plan.uo.ent5	\
		-textvariable ::CONFIG::planminute \
		-font $fonts(entry) \
		-width 2 \
		-state disabled

	# Create radiobutton for selection of Zero time offset
	radiobutton $base.tm.plan.zo.rb \
		-font $fonts(entry) \
		-text "Zero Offset" \
		-indicatoron yes \
		-value 1 \
		-state $rb_state(Planning) \
		-variable ::CONFIG::plan_use_curr_time \
		-command "::CONFIG::_toggle_time_label OFF \
			    $base.tm.plan.uo.lbl1 \
			    $base.tm.plan.uo.lbl2 \
			    $base.tm.plan.uo.lbl3 \
			    $base.tm.plan.uo.lbl4 \
			    $base.tm.plan.uo.lbl5 \
			    ; \
			    ::CONFIG::_toggle_time_status OFF \
			    $base.tm.plan.uo.ent1 \
			    $base.tm.plan.uo.ent2 \
			    $base.tm.plan.uo.ent3 \
			    $base.tm.plan.uo.ent4 \
			    $base.tm.plan.uo.ent5 \
				; \
		        ::CONFIG::plan_clear_time"

	# Create radiobutton for selection of User specification
	radiobutton $base.tm.plan.uo.rb \
		-font $fonts(entry) \
		-text "Offset to (GMT):" \
		-value 0 \
		-indicatoron yes \
		-state $rb_state(Planning) \
		-variable ::CONFIG::plan_use_curr_time \
		-command "::CONFIG::_toggle_time_label ON \
			    $base.tm.plan.uo.lbl1 \
			    $base.tm.plan.uo.lbl2 \
			    $base.tm.plan.uo.lbl3 \
			    $base.tm.plan.uo.lbl4 \
			    $base.tm.plan.uo.lbl5 \
			    ; \
			    ::CONFIG::_toggle_time_status ON \
			    $base.tm.plan.uo.ent1 \
			    $base.tm.plan.uo.ent2 \
			    $base.tm.plan.uo.ent3 \
			    $base.tm.plan.uo.ent4 \
			    $base.tm.plan.uo.ent5"

	# Display
	pack $base.tm.plan.zo.rb \
		-anchor w \
		-side left \
		-fill x

	pack $base.tm.plan.uo.rb \
		-anchor w \
		-side left \
		-fill x

	pack $base.tm.plan.uo.ent5 $base.tm.plan.uo.lbl5 \
		$base.tm.plan.uo.ent4 $base.tm.plan.uo.lbl4 \
		$base.tm.plan.uo.ent3 $base.tm.plan.uo.lbl3 \
		$base.tm.plan.uo.ent2 $base.tm.plan.uo.lbl2 \
		$base.tm.plan.uo.ent1 $base.tm.plan.uo.lbl1 \
		-anchor e \
		-side right

	# Create label for Yaw/Flip Determination during planning mode
	label $base.tm.plan.lbl2 \
		-font $fonts(entry) \
		-foreground gray70 \
		-text {Yaw Flip Orientation}

	pack $base.tm.plan.lbl2 \
		-anchor w \
		-side top

	# Create radiobutton for controlling yaw/flip orientation
	radiobutton $base.tm.plan.rb1 \
		-font $fonts(entry) \
		-text Upright \
		-value Upr \
		-state disabled \
		-variable ::CONFIG::data(yaw_flip) 

	radiobutton $base.tm.plan.rb2 \
		-font $fonts(entry) \
		-text Inverted \
		-value Inv \
		-state disabled \
		-variable ::CONFIG::data(yaw_flip)

	pack $base.tm.plan.rb1 $base.tm.plan.rb2 \
		-anchor w \
		-side top
		
#
###	### ASC Epoch Entry area
#
	frame $base.asc.sep1 \
		-borderwidth 1 \
		-height 2 \
		-relief raised

	pack $base.asc.sep1 \
		-side top \
		-fill x \
		-pady 1

	# Give it a label
	label $base.asc.lbl \
		-text "ASC Epoch" \
		-font $fonts(label) \
		-anchor nw

	pack $base.asc.lbl \
		-anchor nw \
		-side top \
		-fill x


	# Now give options for the time to use
	frame $base.asc.live

	pack $base.asc.live \
		-anchor nw \
		-side top \
		-fill x
		
	# We want the separate sections indented so create a frame to force
	# indentation of other frames to follow
	frame $base.asc.live.spcr1 \
		-width 40

	pack $base.asc.live.spcr1 \
		-anchor nw \
		-side left \
		-fill y

	# Give the section a title
	label $base.asc.live.lbl1 \
		-font $fonts(entry) \
		-anchor w \
		-text "TIME"

	pack $base.asc.live.lbl1 \
		-side top \
		-anchor w \
		-fill x

	# Create a frame for specification of Zero time offset
	frame $base.asc.live.zo

	pack $base.asc.live.zo \
		-anchor w \
		-side top \
		-fill x

	# Create a frame for specification of time offset from TM section
	frame $base.asc.live.tmo

	pack $base.asc.live.tmo \
		-anchor w \
		-side top \
		-fill x

	# Create a frame for user specified time
	frame $base.asc.live.uo

	pack $base.asc.live.uo \
		-anchor w \
		-side top \
		-fill x

	# For user entry, create entry fields for Y, M, D, H, M
	label $base.asc.live.uo.lbl1	\
		-text "YYYY" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.asc.live.uo.ent1	\
		-textvariable ::CONFIG::data(asc_year) \
		-font $fonts(entry) \
		-width 4 \
		-state normal

	label $base.asc.live.uo.lbl2	\
		-text "/MM" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.asc.live.uo.ent2	\
		-textvariable ::CONFIG::data(asc_month) \
		-font $fonts(entry) \
		-width 2 \
		-state normal

	label $base.asc.live.uo.lbl3	\
		-text "/DD" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.asc.live.uo.ent3	\
		-textvariable ::CONFIG::data(asc_day) \
		-font $fonts(entry) \
		-width 2 \
		-state normal

	label $base.asc.live.uo.lbl4	\
		-text " HH" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.asc.live.uo.ent4	\
		-textvariable ::CONFIG::data(asc_hour) \
		-font $fonts(entry) \
		-width 2 \
		-state normal

	label $base.asc.live.uo.lbl5	\
		-text ":MM" \
		-font $fonts(entry) \
		-foreground "gray70"

	entry $base.asc.live.uo.ent5	\
		-textvariable ::CONFIG::data(asc_minute) \
		-font $fonts(entry) \
		-width 2 \
		-state normal

	# Create a radiobutton to allow for selection of zero offset
	# This button also controls whether the user entry option is 
	# enabled
	radiobutton $base.asc.live.zo.rb \
		-font $fonts(entry) \
		-text "Zero Offset" \
		-indicatoron yes \
		-value 1 \
		-variable ::CONFIG::data(asc_use_curr_time) \
		-command "::CONFIG::_toggle_time_label OFF \
				$base.asc.live.uo.lbl1 \
				$base.asc.live.uo.lbl2 \
				$base.asc.live.uo.lbl3 \
				$base.asc.live.uo.lbl4 \
				$base.asc.live.uo.lbl5 \
				; \
			    ::CONFIG::_toggle_time_status OFF \
				$base.asc.live.uo.ent1 \
				$base.asc.live.uo.ent2 \
				$base.asc.live.uo.ent3 \
				$base.asc.live.uo.ent4 \
				$base.asc.live.uo.ent5 \
				; \
		        ::CONFIG::asc_clear_time"

	# Create a radiobutton to allow for selection of using TLM offset
	# This button also controls whether the user entry option is 
	# enabled
	radiobutton $base.asc.live.tmo.rb \
		-font $fonts(entry) \
		-text "Use Same offset as TLM Selection" \
		-indicatoron yes \
		-value 2 \
		-variable ::CONFIG::data(asc_use_curr_time) \
		-command "::CONFIG::_toggle_time_label OFF \
				$base.asc.live.uo.lbl1 \
				$base.asc.live.uo.lbl2 \
				$base.asc.live.uo.lbl3 \
				$base.asc.live.uo.lbl4 \
				$base.asc.live.uo.lbl5 \
				; \
			    ::CONFIG::_toggle_time_status OFF \
				$base.asc.live.uo.ent1 \
				$base.asc.live.uo.ent2 \
				$base.asc.live.uo.ent3 \
				$base.asc.live.uo.ent4 \
				$base.asc.live.uo.ent5 \
				; \
		        ::CONFIG::asc_clear_time"

	# Create radio button for selection of user input
	radiobutton $base.asc.live.uo.rb \
		-font $fonts(entry) \
		-text "Offset to (GMT):" \
		-value 0 \
		-indicatoron yes \
		-variable ::CONFIG::data(asc_use_curr_time) \
		-command "::CONFIG::_toggle_time_label ON \
			    $base.asc.live.uo.lbl1 \
			    $base.asc.live.uo.lbl2 \
			    $base.asc.live.uo.lbl3 \
			    $base.asc.live.uo.lbl4 \
			    $base.asc.live.uo.lbl5 \
			    ; \
			    ::CONFIG::_toggle_time_status ON \
			    $base.asc.live.uo.ent1 \
			    $base.asc.live.uo.ent2 \
			    $base.asc.live.uo.ent3 \
			    $base.asc.live.uo.ent4 \
			    $base.asc.live.uo.ent5"

	# Display
	pack $base.asc.live.zo.rb \
		-anchor w \
		-side left \
		-fill x

	pack $base.asc.live.tmo.rb \
		-anchor w \
		-side left \
		-fill x

	pack $base.asc.live.uo.rb \
		-anchor w \
		-side left \
		-fill x

	pack $base.asc.live.uo.ent5 $base.asc.live.uo.lbl5 \
		$base.asc.live.uo.ent4 $base.asc.live.uo.lbl4 \
		$base.asc.live.uo.ent3 $base.asc.live.uo.lbl3 \
		$base.asc.live.uo.ent2 $base.asc.live.uo.lbl2 \
		$base.asc.live.uo.ent1 $base.asc.live.uo.lbl1 \
		-anchor e \
		-side right

#
###	### ACE Selection area
#
	frame $base.ace.sep1 \
		-borderwidth 1 \
		-height 2 \
		-relief raised

	pack $base.ace.sep1 \
		-side top \
		-fill x \
		-pady 1

	# Give it a label
	label $base.ace.lbl1 \
		-font $fonts(label) \
		-text {ACE TM Selection} \
		-anchor w

	# Create radiobuttons for selection of ACE 1 or ACE 2
	radiobutton $base.ace.rb1 \
		-indicatoron yes \
		-font $fonts(entry) \
		-text 1 \
		-value 1 \
		-variable ::CONFIG::data(ace)

	radiobutton $base.ace.rb2 \
		-indicatoron yes \
		-font $fonts(entry) \
		-text 2 \
		-value 2 \
		-variable ::CONFIG::data(ace)

	# Display
	pack $base.ace.lbl1 \
		-anchor w \
		-side left

	pack $base.ace.rb2 $base.ace.rb1 \
		-anchor e \
		-side right

#
###	### Star Tracker selection area
#
	frame $base.st.sep1 \
		-borderwidth 1 \
		-height 2 \
		-relief raised

	pack $base.st.sep1 \
		-side top \
		-fill x \
		-pady 1

	# Give it a label
	label $base.st.lbl1 \
		-font $fonts(label) \
		-text {Star Trackers}

	# Create radiobuttons for the various combinations
	radiobutton $base.st.rb1 \
		-indicatoron yes \
		-font $fonts(entry) \
		-text 12 \
		-value 12 \
		-variable ::CONFIG::data(sts_active)

	radiobutton $base.st.rb2 \
		-indicatoron yes \
		-font $fonts(entry) \
		-text 23 \
		-value 23 \
		-variable ::CONFIG::data(sts_active)

	radiobutton $base.st.rb3 \
		-indicatoron yes \
		-font $fonts(entry) \
		-text 13 \
		-value 13 \
		-variable ::CONFIG::data(sts_active)

	radiobutton $base.st.rb4 \
		-indicatoron yes \
		-font $fonts(entry) \
		-text 123 \
		-value 123 \
		-variable ::CONFIG::data(sts_active)

	# Display
	pack $base.st.lbl1 \
		-anchor w \
		-side left

	pack $base.st.rb4 $base.st.rb3 $base.st.rb2 $base.st.rb1 \
		-anchor e \
		-side right


#
###	### OK button area
#
	frame $base.but.sep1 \
		-borderwidth 1 \
		-height 2 \
		-relief raised

	pack $base.but.sep1 \
		-side top \
		-fill x \
		-pady 1
		
	button $base.but.ok \
		 -font $fonts(entry) \
		 -text OK \
		 -command "set _GSS_CONFIG_UI_wait 1"
		
	button $base.but.exit \
		 -font $fonts(entry) \
		 -text Exit \
		 -command "exit"

	pack $base.but.ok -side left \
	                  -fill x \
			  -pady 2 \
	                  -padx 100
	pack $base.but.exit -side right \
	                    -fill x \
			    -pady 2 \
	                    -padx 100

	# Wait here until the operator presses OK button
	set repeat TRUE
	while {$repeat} {
	    vwait _GSS_CONFIG_UI_wait
	    set repeat FALSE
	    
		if { ($::CONFIG::data(tm_src) == "Realtime") } {

	        set ::CONFIG::data(use_curr_time) $::CONFIG::live_use_curr_time 
	        set ::CONFIG::data(year)   $::CONFIG::liveyear 
	        set ::CONFIG::data(month)  $::CONFIG::livemonth 
	        set ::CONFIG::data(day)    $::CONFIG::liveday 
	        set ::CONFIG::data(hour)   $::CONFIG::livehour 
	        set ::CONFIG::data(minute) $::CONFIG::liveminute 

		} elseif { ($::CONFIG::data(tm_src) == "Historical") } {

	        set ::CONFIG::data(use_curr_time) $::CONFIG::hist_use_curr_time 
	        set ::CONFIG::data(year)   $::CONFIG::histyear 
	        set ::CONFIG::data(month)  $::CONFIG::histmonth 
	        set ::CONFIG::data(day)    $::CONFIG::histday 
	        set ::CONFIG::data(hour)   $::CONFIG::histhour 
	        set ::CONFIG::data(minute) $::CONFIG::histminute 

		} else {

	        set ::CONFIG::data(use_curr_time) $::CONFIG::plan_use_curr_time 
	        set ::CONFIG::data(year)   $::CONFIG::planyear 
	        set ::CONFIG::data(month)  $::CONFIG::planmonth 
	        set ::CONFIG::data(day)    $::CONFIG::planday 
	        set ::CONFIG::data(hour)   $::CONFIG::planhour 
	        set ::CONFIG::data(minute) $::CONFIG::planminute 

		}

#** Modified: DNiklewski April 2, 2009  SSGS-1028  GSS Month Input error     ******

		# Prevent '08' and '09' being interpreted as octal integers by scanning the inputs with decimal format
		# (but also check for invalid inputs, so, for example, "0x" doesn't get interpreted as "0")

		proc ::CONFIG::forceInteger { x } {
			set count [scan $x {%d %c} n c]
			if { $count != 1 } {
				return $x
			}
			return $n
		}

		set ::CONFIG::data(month) [forceInteger $::CONFIG::data(month)]
		set ::CONFIG::data(day) [forceInteger $::CONFIG::data(day)]
		set ::CONFIG::data(hour) [forceInteger $::CONFIG::data(hour)]
		set ::CONFIG::data(minute) [forceInteger $::CONFIG::data(minute)]

		set ::CONFIG::data(asc_month) [forceInteger $::CONFIG::data(asc_month)]
		set ::CONFIG::data(asc_day) [forceInteger $::CONFIG::data(asc_day)]
		set ::CONFIG::data(asc_hour) [forceInteger $::CONFIG::data(asc_hour)]
		set ::CONFIG::data(asc_minute) [forceInteger $::CONFIG::data(asc_minute)]

#** End Modification 
       
	 set tsttime "$::CONFIG::data(month)/$::CONFIG::data(day)/$::CONFIG::data(year) \
                     $::CONFIG::data(hour)\:$::CONFIG::data(minute)"

	    if {$::CONFIG::data(use_curr_time) == 0} {
	    
	        if {([catch {expr 0 + $::CONFIG::data(year)}] == 1) || \
	             ($::CONFIG::data(year) < 1970) } {
	            ::MSG_DLG::attention "Invalid Entry" \
	                                 red \
	                                 black \
	                                 -INSTRUCTION \
	                                 "Re-enter the GMT Offset Year" 
	            set repeat TRUE
	            
	        } elseif { ([catch {expr 0 + $::CONFIG::data(month)}] == 1) || \
	                   ($::CONFIG::data(month) <= 0) || \
	                   ($::CONFIG::data(month) > 12)} {
	            ::MSG_DLG::attention "Invalid Entry" \
	                                 red \
	                                 black \
	                                 -INSTRUCTION \
	                                 "Re-enter the GMT Offset Month" 
	            set repeat TRUE
	            
	        } elseif { ([catch {expr 0 + $::CONFIG::data(day)}] == 1) || \
	                   ($::CONFIG::data(day) <= 0) || \
	                   ($::CONFIG::data(day) > 31)} {
	            ::MSG_DLG::attention "Invalid Entry" \
	                                 red \
	                                 black \
	                                 -INSTRUCTION \
	                                 "Re-enter the GMT Offset Day" 
	            set repeat TRUE
	            
	        } elseif { ([catch {expr 0 + $::CONFIG::data(hour)}] == 1) || \
	                   ($::CONFIG::data(hour) < 0) || \
	                   ($::CONFIG::data(hour) > 23)} {
	            ::MSG_DLG::attention "Invalid Entry" \
	                                 red \
	                                 black \
	                                 -INSTRUCTION \
	                                 "Re-enter the GMT Offset Hour" 
	            set repeat TRUE
	            
	        } elseif { ([catch {expr 0 + $::CONFIG::data(minute)}] == 1) || \
	                   ($::CONFIG::data(minute) < 0) || \
	                   ($::CONFIG::data(minute) > 59)} {
	            ::MSG_DLG::attention "Invalid Entry" \
	                                 red \
	                                 black \
	                                 -INSTRUCTION \
	                                 "Re-enter the GMT Offset Minutes" 
	            set repeat TRUE
	            
	        } elseif { [catch {clock scan "$tsttime" -gmt yes}] == 1} {
	            ::MSG_DLG::attention "Invalid Date Entry\n$tsttime" \
	                                 red \
	                                 black \
	                                 -INSTRUCTION \
	                                 "Check for invalid number of days in month" 
	            set repeat TRUE
	            
	        }

	        
	    }


	    if {$::CONFIG::data(asc_use_curr_time) == 0} {
	    
	        if {[catch {expr 0 + $::CONFIG::data(asc_year)}] == 1} {
	            ::MSG_DLG::attention "Invalid Entry" \
	                                 red \
	                                 black \
	                                 -INSTRUCTION \
	                                 "Re-enter the ASC GMT Offset Year" 
	            set repeat TRUE
	            
	        } elseif { ([catch {expr 0 + $::CONFIG::data(asc_month)}] == 1) || \
	                   ($::CONFIG::data(asc_month) <= 0) || \
	                   ($::CONFIG::data(asc_month) > 12)} {
	            ::MSG_DLG::attention "Invalid Entry" \
	                                 red \
	                                 black \
	                                 -INSTRUCTION \
	                                 "Re-enter the ASC GMT Offset Month" 
	            set repeat TRUE
	            
	        } elseif { ([catch {expr 0 + $::CONFIG::data(asc_day)}] == 1) || \
	                   ($::CONFIG::data(asc_day) <= 0) || \
	                   ($::CONFIG::data(asc_day) > 31)} {
	            ::MSG_DLG::attention "Invalid Entry" \
	                                 red \
	                                 black \
	                                 -INSTRUCTION \
	                                 "Re-enter the ASC GMT Offset Day" 
	            set repeat TRUE
	            
	        } elseif { ([catch {expr 0 + $::CONFIG::data(asc_hour)}] == 1) || \
	                   ($::CONFIG::data(asc_hour) < 0) || \
	                   ($::CONFIG::data(asc_hour) > 23)} {
	            ::MSG_DLG::attention "Invalid Entry" \
	                                 red \
	                                 black \
	                                 -INSTRUCTION \
	                                 "Re-enter the ASC GMT Offset Hour" 
	            set repeat TRUE
	            
	        } elseif { ([catch {expr 0 + $::CONFIG::data(asc_minute)}] == 1) || \
	                   ($::CONFIG::data(asc_minute) < 0) || \
	                   ($::CONFIG::data(asc_minute) > 59)} {
	            ::MSG_DLG::attention "Invalid Entry" \
	                                 red \
	                                 black \
	                                 -INSTRUCTION \
	                                 "Re-enter the ASC GMT Offset Minutes" 
	            set repeat TRUE
	            
	        }

	    
	    }	

	    if {[file exists $::FILES::gssini] == 0} {
	        ::MSG_DLG::attention "Cannot Access Specified GSS Ini File" \
	                             red \
	                             black \
	                             -INSTRUCTION \
	                             "File does not exist or\
	                              insufficient access privileges" 
	        set repeat TRUE

	    }

	    if {[file exists $::FILES::ssc] == 0} {
	        ::MSG_DLG::attention "Cannot Access Specified SIAD Star Catalog" \
	                             red \
	                             black \
	                             -INSTRUCTION \
	                             "File does not exist or\
	                              insufficient access privileges" 
	        set repeat TRUE

	    }


		if { ($::CONFIG::data(tm_src) == "Realtime") } {
		    set ::CONFIG::stream $::CONFIG::livestream	
		} elseif { ($::CONFIG::data(tm_src) == "Historical") } {
		    set ::CONFIG::stream $::CONFIG::histstream	
		} else {
			set ::CONFIG::stream "None"
		}

	    if { ($::CONFIG::stream == "None") && \
	         ($::CONFIG::data(tm_src) != "Planning") } {
	        ::MSG_DLG::attention "Invalid Stream Specified" \
	                             red \
	                             black \
	                             -INSTRUCTION \
	                             "Please select a GTACS stream" 
	        set repeat TRUE

	    }

	    if { ($::CONFIG::data(yaw_flip) == "None") && \
	         ($::CONFIG::data(tm_src) == "Planning") } {
	        ::MSG_DLG::attention "Invalid Yaw Flip Specified" \
	                             red \
	                             black \
	                             -INSTRUCTION \
	                             "Please select a Yaw Flip orientation" 
	        set repeat TRUE

	    }

	    if { ($::CONFIG::data(use_curr_time) < 0) } {
	        ::MSG_DLG::attention "Invalid Time Offset Selection Specified" \
	                             red \
	                             black \
	                             -INSTRUCTION \
	                             "Please select a Time Offset Method" 
	        set repeat TRUE

	    }


	    if { ($::CONFIG::data(asc_use_curr_time) < 0) } {
	        ::MSG_DLG::attention "Invalid ASC Time Offset Selection Specified" \
	                             red \
	                             black \
	                             -INSTRUCTION \
	                             "Please select an ASC Time Offset Method" 
	        set repeat TRUE

	    }

	    if { ($::CONFIG::data(ace) == 0) } {
	        ::MSG_DLG::attention "Invalid ACE specified" \
	                             red \
	                             black \
	                             -INSTRUCTION \
	                             "Please select an ACE" 
	        set repeat TRUE

	    }

	    if { ($::CONFIG::data(sts_active) == "None") } {
	        ::MSG_DLG::attention "Invalid Star Tracker Combination Specified" \
	                             red \
	                             black \
	                             -INSTRUCTION \
	                             "Please select a Star Tracker Combination" 
	        set repeat TRUE

	    }

	}

	destroy $base	
}

########################################################################
#	$Log: $
########################################################################
