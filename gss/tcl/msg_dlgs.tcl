########################################################################
#	$Id: $
########################################################################
#	simple message dialogs

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ::MSG_DLG:: {
	variable logf 
	variable progress_txt {}

	namespace export init star_pop
	
}
	
########################################################################
#	Init namespace file

proc ::MSG_DLG::init {} {
    return
    
}

########################################################################
#	Init Log file

proc ::MSG_DLG::init_log { } {

	set bar "###################################"
	set err [catch {set logid [open $::FILES::log w+]}]

	if {$err == 1} {
	    ::MSG_DLG::attention "Could not initialize log file \n$::FILES::log" \
	                         red \
	                         black \
	                         -INSTRUCTION "Check file name and directory access privileges"
	    return
	}

	puts $logid "$bar$bar"
	puts $logid "# Ground SIAD Software (GSS) Log File"
	puts $logid "$bar$bar"
	puts $logid "# LOG FILE CREATED: [clock format [clock seconds] -gmt yes]"
	puts $logid "#\n# PROGRAM: $::CONFIG::data(program)\
	                        $::CONFIG::data(sc_id)"
	puts $logid "#\n# GSS IP ADDRESS: $::CONFIG::data(local_ip)"
	puts $logid "# GSS PROCESS ID: $::CONFIG::data(local_pid)"
	puts $logid "#\n# INITIAL USER SELECTIONS"
	puts $logid "#    Initialization File: $::FILES::gssini"
	puts $logid "#      Star Catalog File: $::FILES::ssc"
	puts $logid "#       Telemetry Source: $::CONFIG::data(tm_src)"

	if {$::CONFIG::data(use_curr_time) == 1} {
	    puts $logid "#        GSS Time Offset: None, Using System Time"
	
	} elseif {$::CONFIG::data(use_curr_time) == 2} {
	    puts $logid "#        GSS Time Offset: To ACE $::CONFIG::data(ace) Clock"
   
    } else {
	    set ts [format "To %d/%d/%d %d:%d:00" $::CONFIG::data(month) \
	                                          $::CONFIG::data(day) \
	                                          $::CONFIG::data(year) \
	                                          $::CONFIG::data(hour) \
	                                          $::CONFIG::data(minute)]
	                                   
	    puts $logid "#        GSS Time Offset: $ts"
	
	}


    if {$::CONFIG::data(tm_src) == "Planning"} {
        puts $logid "#   Yaw Flip Orientation: $::CONFIG::data(yaw_flip)"
    }

	if {$::CONFIG::data(asc_use_curr_time) == 1} {
	    puts $logid "#        ASC Time Offset: None, Using System Time"
	
	} elseif {$::CONFIG::data(asc_use_curr_time) == 2} {

	    if {$::CONFIG::data(use_curr_time) == 1} {
	        puts $logid "#        ASC Time Offset: None, Using System Time"
	
	    } elseif {$::CONFIG::data(use_curr_time) == 2} {
	        puts $logid "#        ASC Time Offset: To ACE $::CONFIG::data(ace) Clock"
   
	    } else {
	        set ts [format "To %d/%d/%d %d:%d:00" $::CONFIG::data(month) \
	                                              $::CONFIG::data(day) \
	                                              $::CONFIG::data(year) \
	                                              $::CONFIG::data(hour) \
	                                              $::CONFIG::data(minute)]
	                                   
	        puts $logid "#        ASC Time Offset: $ts"
	
	    }
	                                   
	
	} else {
	    set ts [format "To %d/%d/%d %d:%d:00" $::CONFIG::data(asc_month) \
	                                          $::CONFIG::data(asc_day) \
	                                          $::CONFIG::data(asc_year) \
	                                          $::CONFIG::data(asc_hour) \
	                                          $::CONFIG::data(asc_minute)]
	                                   
	    puts $logid "#        ASC Time Offset: $ts"
	
	}

	puts $logid "#          ACE Selection: $::CONFIG::data(ace)"
	puts $logid "# Star Tracker Selection: $::CONFIG::data(sts_active)\n#\n"

	close $logid
		
}	


########################################################################
#	master logging function

proc ::MSG_DLG::msg_log { text } {
	set err [catch {set logid [open $::FILES::log a]}]

	if {$err == 1} {
	    ::MSG_DLG::attention "Could not write to log file \n$::FILES::log" \
	                         red \
	                         black \
	                         -INSTRUCTION "Check file name and directory access privileges"
	    return
	}

	set time [clock format \
	                [clock seconds] \
	                -format "%Y/%m/%d %H:%M:%S" \
	                -gmt yes]
	                
	puts $logid "\n$time GMT\tACTION:  [string trim $text]\n"
	
	close $logid

}

########################################################################
#	startup progress function

proc ::MSG_DLG::progress { text } {
	variable progress_txt
	
	set progress_txt "$progress_txt $text"
	
	$::GSS::progress_frm.data.lbl configure -text $progress_txt
	::update idletasks

}


########################################################################
#	Attention window
#     text - character (passed); the text to be displayed
# bg_color - character (passed); the color to be used for the background
#                                any valid color name recognized by TCL, default to grey
# fg_color - character (passed); the color to be used for the foreground
#                                any valid color name recognized by TCL, default to black
#     args - character (passed); optional switches followed by quoted text
#
#          -HEADER               text to display in the blue bar at the top of the
#                                window.
#                                e.g: ... -HEADER "3.3.1.4"
#
#          -INSTRUCTION          Additional text to display as instruction to user
#                                text is displayed in Arial 12 Bold Italic format
#                                e.g. ... -INSTRUCTION "Hit OK to Continue"
#
#          -LABEL          	 Numerical string used in the window frame label
#				 Used to avoid multiple copies of the same window
#				 If not provided, then the clock seconds are used

proc ::MSG_DLG::attention {text {bg_color grey} {fg_color black} args} {

    global ATTENTION_wait
    global at_win
 
#
### Default window header, unless overridden by optional argument
#  
    set header "Attention"
    set wait_option NO

#
### Window label based on clock seconds, unless overridden by optional argument
#
    set sec [clock seconds]
    set label [expr int($sec / 2.0)]

#
### Parse optionl arguments
#
    set i 0
    set option [string toupper [lindex $args $i]]
    while {$option != ""} {
      if {$option == "-HEADER"} {
         incr i
         set option_value [lindex $args $i]
         set header $option_value
      } elseif {$option == "-INSTRUCTION"} {
         incr i
         set option_value [lindex $args $i]
         set instruction $option_value
      } elseif {$option == "-LABEL"} {
         incr i
         set option_value [lindex $args $i]
         set label $option_value
      } elseif {$option == "-WAIT"} {
         set wait_option YES
      }
      incr i
      set option [string toupper [lindex $args $i]]
    } 

#
###     Set up window
#
    set at_win [format ".atten_%d" $label]
    catch {destroy $at_win}
    toplevel $at_win
    wm geometry $at_win -0+0 
    wm resizable $at_win 0 0
    
    set frm [frame $at_win.top \
                   -borderwidth 1 \
                   -relief sunken]
    pack $frm \
         -side top \
         -fill both \
         -expand yes

#
###     Window manager
#
    if {[info exists header] != 1} {
        wm title $at_win "ATTENTION"
        wm iconname $at_win "ATTENTION"
    } else {
        wm title $at_win [string toupper $header]
        wm iconname $at_win [string toupper $header]
    }

   if {[info exists instruction] == 1} {
      label $frm.instruction \
            -text $instruction \
            -font "$::CONFIG::fonts(wtitle) italic" \
            -fg blue
      pack $frm.instruction \
           -side top \
           -anchor nw \
           -fill x \
           -pady 5 \
           -expand yes

   }

#
###     Message
#

    label $frm.msg -text "$text" \
                      -font "$::CONFIG::fonts(label)" \
                      -borderwidth 1 \
                      -relief sunken \
                      -wraplength 10i \
                      -justify left \
                      -anchor nw \
                      -background "$bg_color" \
                      -foreground "$fg_color"

    pack $frm.msg \
         -side top \
         -pady 5 \
         -expand yes

#
###     Buttons
#

    frame $frm.buttons \
          -borderwidth 1 \
          -relief sunken
    pack $frm.buttons \
         -side bottom \
         -fill x \
         -expand yes

    button $frm.buttons.ok \
           -text "  OK  " \
           -font $::CONFIG::fonts(entry) \
           -background "grey" \
           -command "set $frm 1 ;destroy $at_win"

    pack $frm.buttons.ok  \
         -side top \
         -pady 5 \
         -expand yes

    focus -force $frm.buttons.ok
         
#
###
#

if { $wait_option == "YES" } {
    tkwait variable $frm
} else {
    raise $frm
}

catch [::update]

}

########################################################################
#	Attention window
#     text - character (passed); the text to be displayed
# bg_color - character (passed); the color to be used for the background
#                                any valid color name recognized by TCL, default to grey
# fg_color - character (passed); the color to be used for the foreground
#                                any valid color name recognized by TCL, default to black
#     args - character (passed); optional switches followed by quoted text
#
#          -HEADER               text to display in the blue bar at the top of the
#                                window.
#                                e.g: ... -HEADER "3.3.1.4"
#
#          -INSTRUCTION          Additional text to display as instruction to user
#                                text is displayed in Arial 12 Bold Italic format
#                                e.g. ... -INSTRUCTION "Hit OK to Continue"
#
#          -LABEL          	 Numerical string used in the window frame label
#				 Used to avoid multiple copies of the same window
#				 If not provided, then the clock seconds are used

proc ::MSG_DLG::attn_yn {arg1 text {bg_color grey} {fg_color black} args} {

    global ATTENTION_wait
    global at_win
 
    upvar $arg1 answer
    catch {uplevel #0 unset temp}
    
#
### Default window header, unless overridden by optional argument
#  
    set header "Attention"

#
### Window label based on clock seconds, unless overridden by optional argument
#
    set sec [clock seconds]
    set label [expr int($sec / 2.0)]

#
### Parse optionl arguments
#
    set i 0
    set option [string toupper [lindex $args $i]]
    while {$option != ""} {
      if {$option == "-HEADER"} {
         incr i
         set option_value [lindex $args $i]
         set header $option_value
      } elseif {$option == "-INSTRUCTION"} {
         incr i
         set option_value [lindex $args $i]
         set instruction $option_value
      } elseif {$option == "-LABEL"} {
         incr i
         set option_value [lindex $args $i]
         set label $option_value
      }
      incr i
      set option [string toupper [lindex $args $i]]
    } 

#
###     Set up window
#
    set at_win [format ".atten_%d" $label]
    catch {destroy $at_win}
    toplevel $at_win
    wm geometry $at_win -0+0 
    wm resizable $at_win 0 0
    
    set frm [frame $at_win.top \
                   -borderwidth 1 \
                   -relief sunken]
    pack $frm \
         -side top \
         -fill both \
         -expand yes

#
###     Window manager
#
    if {[info exists header] != 1} {
        wm title $at_win "ATTENTION"
        wm iconname $at_win "ATTENTION"
    } else {
        wm title $at_win [string toupper $header]
        wm iconname $at_win [string toupper $header]
    }

   if {[info exists instruction] == 1} {
      label $frm.instruction \
            -text $instruction \
            -font "$::CONFIG::fonts(wtitle) italic" \
            -fg blue
      pack $frm.instruction \
           -side top \
           -anchor nw \
           -fill x \
           -pady 5 \
           -expand yes

   }

#
###     Message
#

    label $frm.msg -text "$text" \
                      -font "$::CONFIG::fonts(label)" \
                      -borderwidth 1 \
                      -relief sunken \
                      -wraplength 10i \
                      -justify left \
                      -anchor nw \
                      -background "$bg_color" \
                      -foreground "$fg_color"

    pack $frm.msg \
         -side top \
         -pady 5 \
         -expand yes

#
###     Buttons
#

    frame $frm.buttons \
          -borderwidth 1 \
          -relief sunken
          
    pack $frm.buttons \
         -side bottom \
         -fill x \
         -expand yes
         
    button $frm.buttons.b1 \
    	-text "  YES  " \
    	-font $::CONFIG::fonts(entry) \
    	-background "grey" \
    	-command "set $frm 1 ;destroy $at_win"
    	
    button $frm.buttons.b2 \
    	-text "  NO  " \
    	-font $::CONFIG::fonts(entry) \
    	-background "grey" \
    	-command "set $frm 0 ;destroy $at_win"

    pack $frm.buttons.b1 $frm.buttons.b2 \
    	-side top \
    	-in $frm.buttons \
    	-padx 4m \
    	-pady 2m \
    	-expand yes
         
    focus -force $frm.buttons.b1

    tkwait variable $frm
    set temp1 [uplevel #0 set $frm]

    if {$temp1} {
         set answer YES
    } else {
         set answer NO
    }

#
###
#

catch [::update]

}


########################################################################
#	$Log:  $
########################################################################
