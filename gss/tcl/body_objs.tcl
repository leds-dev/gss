########################################################################
#	$Id:  $
########################################################################

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ::BODY_OBJS:: {
	variable axes
	variable axis_vect

	variable fwd_aft { cda_tm_at_cad dsn_tm_at_cda dsn_tm_at_dsn cmd_at_cda cmd_at_dsn }
	variable instr { cooler optical }

	variable line
	variable color

	variable user

	variable body_obj_list {}
	variable body_obj

	namespace export init draw update user_gui
	}

########################################################################

proc ::BODY_OBJS::init { } {
	global gi
	variable body_obj_list
	variable body_obj

	variable user

	#	Note that sequence of T&C patterns is smallest to largest each for aft and fwd
	#	Numeric ids correspond to sequence in body_obj_list
	set body_obj_list {cda_tm_at_cda_aft cmd_at_cda_aft cmd_at_dsn_aft dsn_tm_at_cda_aft dsn_tm_at_dsn_aft\
					   dsn_tm_at_cda_fwd dsn_tm_at_dsn_fwd cda_tm_at_cda_fwd cmd_at_cda_fwd cmd_at_dsn_fwd\
					   instrument_cooler sun_sensor}

	set body_obj(cda_tm_at_cda_aft,id) 0
	set body_obj(cmd_at_cda_aft,id) 1
	set body_obj(cmd_at_dsn_aft,id) 2
	set body_obj(dsn_tm_at_cda_aft,id) 3
	set body_obj(dsn_tm_at_dsn_aft,id) 4
	set body_obj(dsn_tm_at_cda_fwd,id) 5
	set body_obj(dsn_tm_at_dsn_fwd,id) 6
	set body_obj(cda_tm_at_cda_fwd,id) 7
	set body_obj(cmd_at_cda_fwd,id) 8 
	set body_obj(cmd_at_dsn_fwd,id) 9 
	set body_obj(instrument_cooler,id) 10
	set body_obj(sun_sensor,id) 11

	foreach i $body_obj_list {
	    set uc [string toupper $i]
	    set id $body_obj($i,id)

	#	set display attributes
	    set body_obj($i,color) \
	        [$gi [format "%s_COLOR" $uc] red1]
	    set body_obj($i,radius) \
	        [$gi [format "%s_COVERAGE" $uc] 30.0]
			
	#	Initialize the coverage radius for the body objects
	    SetCircularCoverage $id $body_obj($i,radius)

	}

			
	#	Initialize the coverage pattern for the Instrument optical FOV
	    set h_w_list [$gi "INSTRUMENT_OPTICAL_COVERAGE" {} ]
	    set cmd "SetInstOptFOV $h_w_list"
	    eval $cmd
		
}

##########################################################################
#	$Log: $
########################################################################
