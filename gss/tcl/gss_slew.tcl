########################################################################
#	$Id: $
########################################################################

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ::SLEW:: {

	variable slew_x
	variable slew_y
	variable slew_z
	variable slew_mag
	variable slew_steps
	variable slew_indx
	variable slew_commited
	variable slew_mode
	variable slew_mode_id
	variable slew_steer
	variable slew_steer_id
	variable slew_active
	variable slew_incr
	variable slew_commit_cnt
	variable slew_commit_cnt_lbl
	variable slew_state_text
	variable slew_time_delta
 	variable slew_time_delta_base
	variable slew_time_step
	variable slew_rate

	variable scrollbar_frm

	variable orig_qsel
	variable orig_quat
	variable base_quat
	variable term_quat
	
	namespace export init execute slew_gui

}

########################################################################

proc ::SLEW::init { } {
	
	variable slew_x
	variable slew_y
	variable slew_z
	variable slew_mag
	variable slew_steps
	variable slew_indx
	variable slew_commited
	variable slew_mode
	variable slew_mode_id
	variable slew_steer
	variable slew_steer_id
	variable slew_incr
	variable slew_active
	variable slew_commit_cnt
	variable slew_commit_cnt_lbl
	variable slew_state_text

	variable orig_qsel
	variable orig_quat
	variable slew_quat
	variable base_quat
	variable term_quat

	set slew_commit_cnt 0
	set slew_commit_cnt_lbl "Commit"
	set slew_state_text "Not Committed"
	
	set slew_x 0.0
	set slew_y 0.0
	set slew_z 1.0
	set slew_mag 0.0
	set slew_steps 1
	set slew_commited disabled
	set slew_mode none
	set slew_steer none 
	set slew_active 0

	set slew_steer_id(inertial) 0
	set slew_steer_id(sunfollow) 1
	set slew_steer_id(earthfollow) 2

	set slew_mode_id(user) 0
	set slew_mode_id(sunhold) 1
	set slew_mode_id(sun) 2
	set slew_mode_id(earthupright) 3
	set slew_mode_id(earthinverted) 4
		
	set orig_quat(0) $::USER_QUAT::gss_quat(0)
	set orig_quat(1) $::USER_QUAT::gss_quat(1)
	set orig_quat(2) $::USER_QUAT::gss_quat(2)
	set orig_quat(3) $::USER_QUAT::gss_quat(3)
	
	set base_quat(0) $::USER_QUAT::gss_quat(0)
	set base_quat(1) $::USER_QUAT::gss_quat(1)
	set base_quat(2) $::USER_QUAT::gss_quat(2)
	set base_quat(3) $::USER_QUAT::gss_quat(3)
	
	set term_quat(0) $::USER_QUAT::gss_quat(0)
	set term_quat(1) $::USER_QUAT::gss_quat(1) 
	set term_quat(2) $::USER_QUAT::gss_quat(2)
	set term_quat(3) $::USER_QUAT::gss_quat(3)

	set slew_indx 0

	catch {pack forget $scrollbar_frm}
	
}


########################################################################
#	Initialize the base and terminal slew quat and clear screen
proc ::SLEW::slew_init { } {
	variable slew_mag
	variable slew_steps
	variable slew_incr
	variable slew_indx
	variable slew_active
	variable slew_time_delta
	variable slew_time_delta_base
 	variable slew_time_step
	variable slew_rate
	variable scrollbar_frm
		
	#   Indicate that slew GUI is active (shuts of VT display, and takes over time control)
	set slew_active 1

	#   Zero the delta and step
	set slew_time_delta 0
	set slew_time_delta_base 0
	set slew_time_step 0
	
	#	Initialize slew rate
	set slew_rate 0.1

	#	Initialize our slew quaternions
	set slew_mag 0.0
	set slew_steps 1
	
	set ::SLEW::orig_quat(0) $::USER_QUAT::static_quat(0)
	set ::SLEW::orig_quat(1) $::USER_QUAT::static_quat(1)
	set ::SLEW::orig_quat(2) $::USER_QUAT::static_quat(2)
	set ::SLEW::orig_quat(3) $::USER_QUAT::static_quat(3)
	
	set ::SLEW::slew_quat(0) $::USER_QUAT::static_quat(0)
	set ::SLEW::slew_quat(1) $::USER_QUAT::static_quat(1)
	set ::SLEW::slew_quat(2) $::USER_QUAT::static_quat(2)
	set ::SLEW::slew_quat(3) $::USER_QUAT::static_quat(3)
	
	set ::SLEW::base_quat(0) $::USER_QUAT::static_quat(0)
	set ::SLEW::base_quat(1) $::USER_QUAT::static_quat(1)
	set ::SLEW::base_quat(2) $::USER_QUAT::static_quat(2)
	set ::SLEW::base_quat(3) $::USER_QUAT::static_quat(3)
	
	#	Initialize the C Layer
	set err [SetStaticQuatValue $::SLEW::slew_quat(0) \
	                            $::SLEW::slew_quat(1) \
	                            $::SLEW::slew_quat(2) \
	                            $::SLEW::slew_quat(3)]
	                            
	if {$err != ""} {
	    ::MSG_DLG::attention $err \
	                         red \
	                         black 

	    set err ""
	    
	}

	set slew_indx 0

#	$scrollbar_frm.lbl2 configure -text "step 0 of 0"

#	catch {pack forget $scrollbar_frm}

	#	Clear the skymap of any slew projections
	$::SKYMAP::sky_canv delete "slew"

}


########################################################################

proc ::SLEW::slew_exec { } {
	global gi
	variable slew_mode
	variable slew_mode_id
	variable slew_steer
	variable slew_steer_id
	variable slew_mag
	variable slew_steps
	variable scrollbar_frm
	variable slew_incr
	variable slew_indx
	variable slew_active
	variable slew_quat
	variable slew_x
	variable slew_y
	variable slew_z
	variable slew_time_step
	variable slew_rate

	if { ($slew_mode == "none") } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                         red \
	                         black \
	                         -INSTRUCTION "Select a Mode for the Slew" 
	    return
	    
	}
	
	if { ($slew_steer == "none") } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                         red \
	                         black \
	                         -INSTRUCTION "Select a Steering Mode for the Slew" 
	    return
	    
	}
	
	if { ($slew_steps <= 0) || \
	     ([catch {expr $slew_steps + 0}] == 1) } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                         red \
	                         black \
	                         -INSTRUCTION "Re-enter number of Slew Steps" 
	    return
	    
	}

	if {[catch {expr $slew_x + 0}] == 1} {
	    ::MSG_DLG::attention "Invalid Entry" \
	                         red \
	                         black \
	                         -INSTRUCTION "Re-enter the Slew X" 
	    return
	    
	}	

	if {[catch {expr $slew_y + 0}] == 1} {
	    ::MSG_DLG::attention "Invalid Entry" \
	                         red \
	                         black \
	                         -INSTRUCTION "Re-enter the Slew Y" 
	    return
	    
	}	

	if {[catch {expr $slew_z + 0}] == 1} {
	    ::MSG_DLG::attention "Invalid Entry" \
	                         red \
	                         black \
	                         -INSTRUCTION "Re-enter the Slew Z" 
	    return
	    
	}	

	if { ($slew_x == 0) && ($slew_y == 0) && ($slew_z == 0) && \
	     ($slew_mode == "user") } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                         red \
	                         black \
	                         -INSTRUCTION "Enter a non-zero slew axis." 
	    return
	    
	}

	if { ($slew_mode == "user" || $slew_mode == "sunhold") && \
	      ($slew_mag == 0) } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                         red \
	                         black \
	                         -INSTRUCTION "Re-enter a non-zero Slew Magnitude" 
	    return
	    
	}	

	if { ($slew_mode == "user" || $slew_mode == "sunhold") && \
	     ( ($slew_mag > 180.0) || \
	       ($slew_mag < -180.0) || \
	       ([catch {expr $slew_mag + 0}] == 1) ) } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                         red \
	                         black \
	                         -INSTRUCTION "Re-enter a Slew Magnitude between -180 and 180 deg" 
	    return
	    
	}	

	if { ($slew_steer == "inertial") && \
	     ( ($slew_rate <= 0.0) || \
	      ([catch {expr $slew_rate + 0}] == 1) ) } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                         red \
	                         black \
	                         -INSTRUCTION "Re-enter a Slew Rate > 0 deg/sec" 
	    return
	    
	}	

	# Sun not allowed to move more than 1 degree during slew, or first order solution breaks down 	
	if { ($slew_steer == "sunfollow") && \
	     ( ($slew_rate < 0.002) || \
	      ([catch {expr $slew_rate + 0}] == 1) ) } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                         red \
	                         black \
	                         -INSTRUCTION "Re-enter a Slew Rate >= 0.002 deg/sec" 
	    return
	    
	}	

	# Earth not allowed to move more than 180 degree during slew, or steering will be in wrong direction 	
	if { ($slew_steer == "earthfollow") && \
	     ( ($slew_rate < 0.0042) || \
	      ([catch {expr $slew_rate + 0}] == 1) ) } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                         red \
	                         black \
	                         -INSTRUCTION "Re-enter a Slew Rate >= 0.0042 deg/sec" 
	    return
	    
	}	

    if { ($slew_rate > 1.0) } {
        ::MSG_DLG::attention "Invalid Entry" \
                             red \
                             black \
                             -INSTRUCTION "Enter a Slew Rate <= 1 deg/sec"
        return
       
    }

    set err [catch {set logid [open $::FILES::log a]}]
    if {$err == 1} {
        ::MSG_DLG::attention "Could not write to log file \n$::FILES::log \nwhich is a prerequisite for executing SLEW" \
                             red \
                             black \
                             -INSTRUCTION "Check file name and directory access privileges"
        return
    }
    close $logid

	set ::SLEW::slew_quat(0) $::SLEW::base_quat(0)
	set ::SLEW::slew_quat(1) $::SLEW::base_quat(1)
	set ::SLEW::slew_quat(2) $::SLEW::base_quat(2)
	set ::SLEW::slew_quat(3) $::SLEW::base_quat(3)
	
	set err [SetStaticQuatValue $::SLEW::slew_quat(0) \
	                            $::SLEW::slew_quat(1) \
	                            $::SLEW::slew_quat(2) \
	                            $::SLEW::slew_quat(3)]
	                            
	if {$err != ""} {
	    ::MSG_DLG::attention $err \
	                         red \
	                         black 

	    set err ""
	    
	}

    #    Set up the rate and nadir axis for SlewPlanning
	set cmd "GetSlewIniVars $slew_rate \
	                        [$gi NADIR_FACING_AXIS "0.0 0.0 1.0"]"

	eval $cmd

	set data [SlewPlanning $slew_mode_id($slew_mode) \
                           $slew_steer_id($slew_steer) \
	                       $slew_steps \
	                       $slew_mag \
	                       $slew_x \
	                       $slew_y \
	                       $slew_z ]
	
	if {[string first "Error" $data] != -1} {
	    ::MSG_DLG::attention "Internal Error within SlewPlanning Call" \
	                         red \
	                         black \
	                         -INSTRUCTION "Call BSS!"

	}
	                       
	set slew_x [format "%7.5f" [lindex $data 0]]
	set slew_y [format "%7.5f" [lindex $data 1]]
	set slew_z [format "%7.5f" [lindex $data 2]]
	set slew_mag [format "%7.5f" [lindex $data 3]]
	          
	set slew_incr      [expr $slew_mag / $slew_steps]
	set slew_time_step [expr int( abs( $slew_incr / $slew_rate ) ) ]
	set slew_indx 0

	$scrollbar_frm.lbl2 configure \
	                    -text [format "step %d of %d" \
	                                  $slew_indx \
	                                  $slew_steps]
	
	pack $scrollbar_frm \
	     -side left \
	     -fill both


	#	Log it
	set msg_txt "SLEW EXECUTE BUTTON PRESSED\n"
	set msg_txt "$msg_txt\Slew Mode       : $slew_mode\n"
	set msg_txt "$msg_txt\Slew Steering   : $slew_steer\n"
	set msg_txt "$msg_txt\Slew X value    : $slew_x\n"
	set msg_txt "$msg_txt\Slew Y value    : $slew_y\n"
	set msg_txt "$msg_txt\Slew Z value    : $slew_z\n"
	set msg_txt "$msg_txt\Slew Mag value  : $slew_mag\n"
	set msg_txt "$msg_txt\Slew Steps value: $slew_steps\n"
	set msg_txt "$msg_txt\Slew Rate value : $slew_rate\n"
	set q_txt [format "q(1) %9.7f\tq(2) %9.7f\t\
	                   q(3) %9.7f\t q(4) %9.7f" \
	                   $::SLEW::orig_quat(0) \
	                   $::SLEW::orig_quat(1) \
	                   $::SLEW::orig_quat(2) \
	                   $::SLEW::orig_quat(3)]
	set msg_txt "$msg_txt\Original Quaternion (qb.eci): $q_txt\n"
	set q_txt [format "q(1) %9.7f\tq(2) %9.7f\t\
	                   q(3) %9.7f\t q(4) %9.7f" \
	                   $::SLEW::base_quat(0) \
	                   $::SLEW::base_quat(1) \
	                   $::SLEW::base_quat(2) \
	                   $::SLEW::base_quat(3)]
	set msg_txt "$msg_txt\Base Quaternion (qb.eci)    : $q_txt\n"

	::MSG_DLG::msg_log $msg_txt
		
	}

########################################################################
#	Accept a slew attempt

proc ::SLEW::slew_accept { } {
	variable scrollbar_frm

    #   Update the base time delta
    set ::SLEW::slew_time_delta_base $::SLEW::slew_time_delta

	#	Set the base quaternion to the current target
	set ::SLEW::base_quat(0) [format "%9.7f" $::SLEW::slew_quat(0)]
	set ::SLEW::base_quat(1) [format "%9.7f" $::SLEW::slew_quat(1)]
	set ::SLEW::base_quat(2) [format "%9.7f" $::SLEW::slew_quat(2)]
	set ::SLEW::base_quat(3) [format "%9.7f" $::SLEW::slew_quat(3)]

	catch {pack forget $scrollbar_frm}

	#	Log it
	set msg_txt "SLEW ACCEPT BUTTON PRESSED\n"
	set q_txt [format "q(1) %9.7f\tq(2) %9.7f\t\
	                   q(3) %9.7f\t q(4) %9.7f" \
	                   $::SLEW::base_quat(0) \
	                   $::SLEW::base_quat(1) \
	                   $::SLEW::base_quat(2) \
	                   $::SLEW::base_quat(3)]
	set msg_txt "$msg_txt\Base Quaternion (qb.eci) set to: $q_txt\n"

	::MSG_DLG::msg_log $msg_txt

}

########################################################################
#	Reject a slew attempt

proc ::SLEW::slew_reject { } {
	variable scrollbar_frm

    #   Reset the time delta
    set ::SLEW::slew_time_delta $::SLEW::slew_time_delta_base

	#	Reset the terminal quaternion back to the base
	set ::SLEW::slew_quat(0) [format "%9.7f" $::SLEW::base_quat(0)]
	set ::SLEW::slew_quat(1) [format "%9.7f" $::SLEW::base_quat(1)]
	set ::SLEW::slew_quat(2) [format "%9.7f" $::SLEW::base_quat(2)]
	set ::SLEW::slew_quat(3) [format "%9.7f" $::SLEW::base_quat(3)]
	
	set err [SetStaticQuatValue $::SLEW::slew_quat(0) \
	                            $::SLEW::slew_quat(1) \
	                            $::SLEW::slew_quat(2) \
	                            $::SLEW::slew_quat(3)]

	if {$err != ""} {
	    ::MSG_DLG::attention $err \
	                         red \
	                         black 

	    set err ""
	    
	}

	catch {pack forget $scrollbar_frm}

	#	Log it
	set msg_txt "SLEW REJECT BUTTON PRESSED\n"
	set q_txt [format "q(1) %9.7f\tq(2) %9.7f\t\
	                   q(3) %9.7f\t q(4) %9.7f" \
	                   $::SLEW::slew_quat(0) \
	                   $::SLEW::slew_quat(1) \
	                   $::SLEW::slew_quat(2) \
	                   $::SLEW::slew_quat(3)]
	set msg_txt "$msg_txt\Quaternion (qb.eci) reset to: $q_txt\n"

	::MSG_DLG::msg_log $msg_txt

}


########################################################################
#	Commit a slew attempt

proc ::SLEW::slew_commit { } {

	#	Set the slew quaternion for the DLRV routine to access
	set ::SLEW::term_quat(0) $::SLEW::slew_quat(0)
	set ::SLEW::term_quat(1) $::SLEW::slew_quat(1)
	set ::SLEW::term_quat(2) $::SLEW::slew_quat(2)
	set ::SLEW::term_quat(3) $::SLEW::slew_quat(3)
	
	#	Store the slew quaternion and time if needed next time
	set ::USER_QUAT::snptlm_quat(0) $::SLEW::slew_quat(0)
	set ::USER_QUAT::snptlm_quat(1) $::SLEW::slew_quat(1)
	set ::USER_QUAT::snptlm_quat(2) $::SLEW::slew_quat(2)
	set ::USER_QUAT::snptlm_quat(3) $::SLEW::slew_quat(3)
	set ::USER_QUAT::snptlm_q_time [expr $::USER_QUAT::static_q_time + $::SLEW::slew_time_delta]

	set ::SLEW::slew_commited normal
	
	#    Update the Commit count on the button label
	incr ::SLEW::slew_commit_cnt
	set ::SLEW::slew_commit_cnt_lbl "Commit \#$::SLEW::slew_commit_cnt"

	#	Log it
	set msg_txt "SLEW COMMIT BUTTON PRESSED -\
	             $::SLEW::slew_commit_cnt_lbl\n"
	             
	set q_txt [format "q(1) %9.7f\tq(2) %9.7f\t\
	                   q(3) %9.7f\t q(4) %9.7f" \
	                   $::SLEW::term_quat(0) \
	                   $::SLEW::term_quat(1) \
	                   $::SLEW::term_quat(2) \
	                   $::SLEW::term_quat(3)]
	set msg_txt "$msg_txt\Terminal Quaternion (qb.eci): $q_txt\n"

	::MSG_DLG::msg_log $msg_txt
	                      
	#    Update the other screens
	set ::SLEW::slew_state_text "Commit \#$::SLEW::slew_commit_cnt"


}

########################################################################
#	Reset everything back to the Original Quaternion

proc ::SLEW::slew_reset { } {
	variable scrollbar_frm

    #   Reset the time deltas back to zero
    set ::SLEW::slew_time_delta 0
    set ::SLEW::slew_time_delta_base 0

	#	Reset the current quaternion back to the initial
	set ::SLEW::slew_quat(0) [format "%9.7f" $::SLEW::orig_quat(0)]
	set ::SLEW::slew_quat(1) [format "%9.7f" $::SLEW::orig_quat(1)]
	set ::SLEW::slew_quat(2) [format "%9.7f" $::SLEW::orig_quat(2)]
	set ::SLEW::slew_quat(3) [format "%9.7f" $::SLEW::orig_quat(3)]

	set err [SetStaticQuatValue $::SLEW::orig_quat(0) \
	                            $::SLEW::orig_quat(1) \
	                            $::SLEW::orig_quat(2) \
	                            $::SLEW::orig_quat(3)]

	if {$err != ""} {
	    ::MSG_DLG::attention $err \
	                         red \
	                         black 

	    set err ""
	    
	}
	
	catch {pack forget $scrollbar_frm}

	#	Log it
	set msg_txt "SLEW RESET BUTTON PRESSED\n"
	set q_txt [format "q(1) %9.7f\tq(2) %9.7f\t\
	                   q(3) %9.7f\t q(4) %9.7f" \
	                   $::SLEW::orig_quat(0) \
	                   $::SLEW::orig_quat(1) \
	                   $::SLEW::orig_quat(2) \
	                   $::SLEW::orig_quat(3)]
	set msg_txt "$msg_txt\Quaternion (qb.eci) reset to: $q_txt\n"

	::MSG_DLG::msg_log $msg_txt

	#	reset the base quaternion back to the original
	set ::SLEW::base_quat(0) [format "%9.7f" $::SLEW::orig_quat(0)]
	set ::SLEW::base_quat(1) [format "%9.7f" $::SLEW::orig_quat(1)]
	set ::SLEW::base_quat(2) [format "%9.7f" $::SLEW::orig_quat(2)]
	set ::SLEW::base_quat(3) [format "%9.7f" $::SLEW::orig_quat(3)]
	
}


########################################################################
#	step the quaternion

proc ::SLEW::step_quat { inc } {
	variable scrollbar_frm
	variable slew_steps
	variable slew_indx
	variable slew_incr
	variable slew_quat
	variable slew_time_delta
	variable slew_time_step

	if {$inc == 0} {return}
	
	incr slew_indx $inc
	
	if {$slew_indx > $slew_steps} {
	    set slew_indx $slew_steps
	    return
	    
	} elseif {$slew_indx < 0} {
	    set slew_indx 0
	    return
	    
	}
	
	$scrollbar_frm.lbl2 configure \
	                    -text [format "step %d of %d" \
	                                  $slew_indx \
	                                  $slew_steps]

    #    Update the time delta based on the sign of the step just taken
    set slew_time_delta [expr $slew_time_delta + $inc*$slew_time_step]

	set data [GetNextStepQuat $inc]
	set slew_quat(0) [format "%9.7f" [lindex $data 0]]
	set slew_quat(1) [format "%9.7f" [lindex $data 1]]
	set slew_quat(2) [format "%9.7f" [lindex $data 2]]
	set slew_quat(3) [format "%9.7f" [lindex $data 3]]
	                 
	set err [SetStaticQuatValue $slew_quat(0) \
	                            $slew_quat(1) \
	                            $slew_quat(2) \
	                            $slew_quat(3)]

	if {$err != ""} {
	    ::MSG_DLG::attention $err \
	                         red \
	                         black 

	    set err ""
	    
	}

}
	
########################################################################
# User interface for controlling Slew Planning

proc ::SLEW::slew_gui { } {
	variable scrollbar_frm

        # Verify we have a static quaternion selected

	if {$::USER_QUAT::gss_qsel != "STATIC"} {
	    ::MSG_DLG::attention "Selected GSS Quaternion must be static" \
	                         red \
	                         black \
	                         -INSTRUCTION "Check the QUATERNION screen"
	    return
	    
	}


	#	Initialize data
	::SLEW::slew_init

	#   Determine whether earth options are allowed, requiring a valid ephemeris
	if { $::CONFIG::data(valid_ephem) == "VALID" } {
    	set earthokst normal 
		set earthokfg black 
	} else {
    	set earthokst disabled
		set earthokfg gray60
	}

	catch {destroy .slew_ui}
	toplevel .slew_ui
	wm title .slew_ui "SLEW PLANNING"
	wm geometry .slew_ui "-0-0"
	set base [frame .slew_ui.top]
	pack $base -side top \
		   -anchor nw \
		   -fill both
	
	#	Set up display		   
	frame $base.mode -borderwidth 1 \
	                 -relief sunken
	pack $base.mode -side top \
			-fill both \
			-anchor w
			
	#	Title
	label $base.mode.lbl -text "Slew Mode" \
			     -font $::CONFIG::fonts(entry) \
			     -width 12 \
			     -justify left \
			     -anchor w

	radiobutton $base.mode.usr -text "User" \
				   -variable ::SLEW::slew_mode \
				   -font $::CONFIG::fonts(entry) \
				   -value user \
				   -indicator on \
				   -command "$base.cntrl.axis.x.ent configure \
				   	     -state normal; \
				   	     $base.cntrl.axis.y.ent configure \
				   	     -state normal; \
				   	     $base.cntrl.axis.z.ent configure \
				   	     -state normal; \
				   	     $base.cntrl.mag.ent configure \
				   	     -state normal; \
				   	     $base.cntrl.steps.ent configure \
				   	     -state normal; \
				   	     $base.cntrl.axis.title.lbl \
				   	      configure \
				   	     -foreground black; \
				   	     $base.cntrl.axis.x.lbl configure \
				   	     -foreground black; \
				   	     $base.cntrl.axis.y.lbl configure \
				   	     -foreground black; \
				   	     $base.cntrl.axis.z.lbl configure \
				   	     -foreground black; \
				   	     $base.cntrl.mag.lbl configure \
				   	     -foreground black"

	radiobutton $base.mode.sunh -text "Sun Line" \
				   -variable ::SLEW::slew_mode \
				   -font $::CONFIG::fonts(entry) \
				   -value sunhold \
				   -indicator on \
				   -command "set ::SLEW::slew_x 0.0; \
						 set ::SLEW::slew_y 0.0; \
						 set ::SLEW::slew_z 0.0; \
						 $base.cntrl.axis.x.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.axis.y.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.axis.z.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.mag.ent configure \
				   	     -state normal; \
				   	     $base.cntrl.steps.ent configure \
				   	     -state normal; \
				   	     $base.cntrl.axis.title.lbl \
				   	      configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.axis.x.lbl configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.axis.y.lbl configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.axis.z.lbl configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.mag.lbl configure \
				   	     -foreground black"

	radiobutton $base.mode.sun -text "To Sun" \
				   -variable ::SLEW::slew_mode \
				   -font $::CONFIG::fonts(entry) \
				   -value sun \
				   -indicator on \
				   -command "set ::SLEW::slew_x 0.0; \
						 set ::SLEW::slew_y 0.0; \
						 set ::SLEW::slew_z 0.0; \
						 set ::SLEW::slew_mag 0.0; \
						 $base.cntrl.axis.x.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.axis.y.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.axis.z.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.mag.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.steps.ent configure \
				   	     -state normal; \
				   	     $base.cntrl.axis.title.lbl \
				   	      configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.axis.x.lbl configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.axis.y.lbl configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.axis.z.lbl configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.mag.lbl configure \
				   	     -foreground gray60"

	radiobutton $base.mode.erthu -text "To Earth (Upr)" \
				   -variable ::SLEW::slew_mode \
				   -font $::CONFIG::fonts(entry) \
				   -value earthupright \
				   -indicator on \
                   -state $earthokst \
                   -foreground $earthokfg \
				   -command "set ::SLEW::slew_x 0.0; \
						 set ::SLEW::slew_y 0.0; \
						 set ::SLEW::slew_z 0.0; \
						 set ::SLEW::slew_mag 0.0; \
						 $base.cntrl.axis.x.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.axis.y.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.axis.z.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.mag.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.steps.ent configure \
				   	     -state normal; \
				   	     $base.cntrl.axis.title.lbl \
				   	      configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.axis.x.lbl configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.axis.y.lbl configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.axis.z.lbl configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.mag.lbl configure \
				   	     -foreground gray60"

	radiobutton $base.mode.erthi -text "To Earth (Inv)" \
				   -variable ::SLEW::slew_mode \
				   -font $::CONFIG::fonts(entry) \
				   -value earthinverted \
				   -indicator on \
                   -state $earthokst \
                   -foreground $earthokfg \
				   -command "set ::SLEW::slew_x 0.0; \
						 set ::SLEW::slew_y 0.0; \
						 set ::SLEW::slew_z 0.0; \
						 set ::SLEW::slew_mag 0.0; \
						 $base.cntrl.axis.x.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.axis.y.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.axis.z.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.mag.ent configure \
				   	     -state disabled; \
				   	     $base.cntrl.steps.ent configure \
				   	     -state normal; \
				   	     $base.cntrl.axis.title.lbl \
				   	      configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.axis.x.lbl configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.axis.y.lbl configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.axis.z.lbl configure \
				   	     -foreground gray60; \
				   	     $base.cntrl.mag.lbl configure \
				   	     -foreground gray60"
				   	     
	pack $base.mode.lbl $base.mode.usr $base.mode.sunh \
	     $base.mode.sun $base.mode.erthu $base.mode.erthi \
	     -side left \
	     -anchor w \
	     -padx 2 \
	     -pady 3 \
	     -fill x


    #   Set up display for steering law
    frame $base.steer -borderwidth 1 \
                      -relief sunken
    pack $base.steer -side top \
            -fill both \
            -anchor w 
   
    #   Title
    label $base.steer.lbl -text "Steering" \
                 -font $::CONFIG::fonts(entry) \
                 -width 12 \
                 -justify left \
                 -anchor w

    radiobutton $base.steer.inert -text "Inertial" \
                   -variable ::SLEW::slew_steer \
                   -font $::CONFIG::fonts(entry) \
                   -value inertial \
                   -indicator on \
                   -command ""

    radiobutton $base.steer.sun -text "Sun Following" \
                   -variable ::SLEW::slew_steer \
                   -font $::CONFIG::fonts(entry) \
                   -value sunfollow \
                   -indicator on \
                   -command ""

    radiobutton $base.steer.earth -text "Earth Following" \
                   -variable ::SLEW::slew_steer \
                   -font $::CONFIG::fonts(entry) \
                   -value earthfollow \
                   -indicator on \
                   -state $earthokst \
                   -foreground $earthokfg \
                   -command ""

    pack $base.steer.lbl $base.steer.inert $base.steer.sun $base.steer.earth \
         -side left \
         -anchor w \
         -padx 2 \
         -pady 3 \
         -fill x


	#	Axis control area
	frame $base.cntrl -borderwidth 1 \
	                  -relief sunken
	pack $base.cntrl -side top \
	                 -fill both
	
	frame $base.cntrl.axis
	pack $base.cntrl.axis \
	     -side top \
	     -fill both \
	     -anchor w \
	     -pady 3
	
	frame $base.cntrl.axis.title
	pack $base.cntrl.axis.title \
	     -side left \
	     -anchor w \
	     -fill both

	label $base.cntrl.axis.title.lbl \
	      -text "Slew Axis Unit Vector (Body Frame)" \
	      -font $::CONFIG::fonts(entry) \
	      -width 34 \
	      -anchor w

	pack $base.cntrl.axis.title.lbl \
	     -side top \
	     -anchor w \
	     -fill both \
         -padx 2
				  
	frame $base.cntrl.axis.x
	pack $base.cntrl.axis.x \
	     -side top \
	     -fill x \
	     -anchor w

	label $base.cntrl.axis.x.lbl \
	      -text "X" \
	      -font $::CONFIG::fonts(entry) \
	      -anchor e

	entry $base.cntrl.axis.x.ent \
	      -textvariable ::SLEW::slew_x \
	      -font $::CONFIG::fonts(entry) \
	      -width 10
			       
	pack $base.cntrl.axis.x.ent $base.cntrl.axis.x.lbl \
	     -side right \
	     -anchor e \
	     -fill x

	frame $base.cntrl.axis.y
	pack $base.cntrl.axis.y \
	     -side top \
	     -fill x \
	     -anchor w

	label $base.cntrl.axis.y.lbl \
	      -text "Y" \
	      -font $::CONFIG::fonts(entry) \
	      -anchor e

	entry $base.cntrl.axis.y.ent \
	      -textvariable ::SLEW::slew_y \
	      -font $::CONFIG::fonts(entry) \
	      -width 10
			       
	pack $base.cntrl.axis.y.ent $base.cntrl.axis.y.lbl \
	     -side right \
	     -anchor e \
	     -fill x

	frame $base.cntrl.axis.z
	pack $base.cntrl.axis.z \
	     -side top \
	     -fill x \
	     -anchor w

	label $base.cntrl.axis.z.lbl \
	     -text "Z" \
	     -font $::CONFIG::fonts(entry) \
	     -anchor e

	entry $base.cntrl.axis.z.ent \
	      -textvariable ::SLEW::slew_z \
	      -font $::CONFIG::fonts(entry) \
	      -width 10
		       
	pack $base.cntrl.axis.z.ent $base.cntrl.axis.z.lbl \
	     -side right \
	     -anchor e \
	     -fill x

	frame $base.cntrl.mag \
	      -relief sunken
	pack $base.cntrl.mag  \
	     -side top \
	     -fill both \
	     -anchor w \
	     -pady 3
	
	label $base.cntrl.mag.lbl \
	      -text "Magnitude (deg)" \
	      -font $::CONFIG::fonts(entry) \
	      -anchor w \
	      -width 34
			    
	pack $base.cntrl.mag.lbl \
	     -side left \
	     -anchor w \
	     -padx 2
			   
	entry $base.cntrl.mag.ent \
	      -textvariable ::SLEW::slew_mag \
	      -font $::CONFIG::fonts(entry) \
	      -width 10

	pack $base.cntrl.mag.ent \
	     -side right \
	     -anchor e \
	     -fill x
			    
	frame $base.cntrl.steps \
	     -relief sunken
	pack $base.cntrl.steps \
	     -side top \
	     -fill both \
	     -anchor w \
	     -pady 3
	
	label $base.cntrl.steps.lbl \
	      -text "Number of Steps" \
	      -font $::CONFIG::fonts(entry) \
	      -anchor w \
	      -width 34
			    
	pack $base.cntrl.steps.lbl \
	     -side left \
	     -anchor w \
	     -padx 2
			   
	entry $base.cntrl.steps.ent \
	      -textvariable ::SLEW::slew_steps \
	      -font $::CONFIG::fonts(entry) \
	      -width 10

	pack $base.cntrl.steps.ent \
	     -side right \
	     -anchor e \
	     -fill x


    frame $base.cntrl.rate \
          -relief sunken
    pack $base.cntrl.rate  \
         -side top \
         -fill both \
         -anchor w \
         -pady 3
   
    label $base.cntrl.rate.lbl \
          -text "Rate (deg/sec)" \
          -font $::CONFIG::fonts(entry) \
          -anchor w \
          -width 34
   
    pack $base.cntrl.rate.lbl \
         -side left \
         -anchor w \
         -padx 2

    entry $base.cntrl.rate.ent \
          -textvariable ::SLEW::slew_rate \
          -font $::CONFIG::fonts(entry) \
          -width 10

    pack $base.cntrl.rate.ent \
         -side right \
         -anchor e \
         -fill x


	#	Quaternion Display area
	frame $base.quat \
	      -borderwidth 1 \
	      -relief sunken
	pack $base.quat \
	     -side top \
	     -fill x

	#	Create an empty frame (for now) to put the control
	#	scrollbar into

	frame $base.quat.control
	pack $base.quat.control -side left

	#	Original Quaternion (before we messed around)	                
	frame $base.quat.init
	pack $base.quat.init -side top \
	                     -anchor e \
	                     -pady 2
	                     
	label $base.quat.init.lbl  \
	      -text "Initial Quaternion" \
	      -font  $::CONFIG::fonts(entry) \
	      -justify left \
	      -anchor w \
	      -width 20
	                     
	label $base.quat.init.lbl1  \
	      -textvariable ::SLEW::orig_quat(0) \
	      -font  $::CONFIG::fonts(entry) \
	      -relief sunken \
	      -justify left \
	      -anchor w \
	      -width 10
	                     
	label $base.quat.init.lbl2  \
	      -textvariable ::SLEW::orig_quat(1) \
	      -font  $::CONFIG::fonts(entry) \
	      -relief sunken \
	      -justify left \
	      -anchor w \
	      -width 10
	                     
	label $base.quat.init.lbl3  \
	      -textvariable ::SLEW::orig_quat(2) \
	      -font  $::CONFIG::fonts(entry) \
	      -relief sunken \
	      -justify left \
	      -anchor w \
	      -width 10
	                     
	label $base.quat.init.lbl4  \
	      -textvariable ::SLEW::orig_quat(3) \
	      -font  $::CONFIG::fonts(entry) \
	      -relief sunken \
	      -justify left \
	      -anchor w \
	      -width 10

	pack $base.quat.init.lbl4 $base.quat.init.lbl3 \
	     $base.quat.init.lbl2 $base.quat.init.lbl1 \
	     $base.quat.init.lbl \
	     -side right \
	     -anchor e \
	     -padx 2

	#	Base Quaternion (After an ACCEPT)	                
	frame $base.quat.base
	pack $base.quat.base -side top \
	                     -anchor e \
	                     -pady 2
	                     
	label $base.quat.base.lbl  \
	      -text "Base Quaternion" \
	      -font  $::CONFIG::fonts(entry) \
	      -justify left \
	      -anchor w \
	      -width 20
	                     
	label $base.quat.base.lbl1  \
	      -textvariable ::SLEW::base_quat(0) \
	      -font  $::CONFIG::fonts(entry) \
	      -relief sunken \
	      -justify left \
	      -anchor w \
	      -width 10
	                     
	label $base.quat.base.lbl2  \
	      -textvariable ::SLEW::base_quat(1) \
	      -font  $::CONFIG::fonts(entry) \
	      -relief sunken \
	      -justify left \
	      -anchor w \
	      -width 10
	                     
	label $base.quat.base.lbl3  \
	      -textvariable ::SLEW::base_quat(2) \
	      -font  $::CONFIG::fonts(entry) \
	      -relief sunken \
	      -justify left \
	      -anchor w \
	      -width 10
	                     
	label $base.quat.base.lbl4  \
	      -textvariable ::SLEW::base_quat(3) \
	      -font  $::CONFIG::fonts(entry) \
	      -relief sunken \
	      -justify left \
	      -anchor w \
	      -width 10

	pack $base.quat.base.lbl4 $base.quat.base.lbl3 \
	     $base.quat.base.lbl2 $base.quat.base.lbl1 \
	     $base.quat.base.lbl \
	     -side right \
	     -anchor e \
	     -padx 2

	#	Current Quaternion (What is running the display)
	frame $base.quat.slew
	pack $base.quat.slew -side top \
	                     -anchor e \
	                     -pady 2
	                     
	label $base.quat.slew.lbl  \
	      -text "Current Quaternion" \
	      -font  $::CONFIG::fonts(entry) \
	      -justify left \
	      -anchor w \
	      -width 20
	                     
	label $base.quat.slew.lbl1  \
	      -textvariable ::SLEW::slew_quat(0) \
	      -font  $::CONFIG::fonts(entry) \
	      -relief sunken \
	      -justify left \
	      -anchor w \
	      -width 10
	                     
	label $base.quat.slew.lbl2  \
	      -textvariable ::SLEW::slew_quat(1) \
	      -font  $::CONFIG::fonts(entry) \
	      -relief sunken \
	      -justify left \
	      -anchor w \
	      -width 10
	                     
	label $base.quat.slew.lbl3  \
	      -textvariable ::SLEW::slew_quat(2) \
	      -font  $::CONFIG::fonts(entry) \
	      -relief sunken \
	      -justify left \
	      -anchor w \
	      -width 10
	                     
	label $base.quat.slew.lbl4  \
	      -textvariable ::SLEW::slew_quat(3) \
	      -font  $::CONFIG::fonts(entry) \
	      -relief sunken \
	      -justify left \
	      -anchor w \
	      -width 10

	pack $base.quat.slew.lbl4 $base.quat.slew.lbl3 \
	     $base.quat.slew.lbl2 $base.quat.slew.lbl1 \
	     $base.quat.slew.lbl \
	     -side right \
	     -anchor e \
	     -padx 2

	
	#	Stepping Control area		
	frame $base.quat.control.scroll
	set scrollbar_frm $base.quat.control.scroll

	label $base.quat.control.scroll.lbl \
	      -text "Step Quat" \
	      -font $::CONFIG::fonts(label) \
	      -width 11 \
	      -anchor c \
	      -justify center
	
	scrollbar $base.quat.control.scroll.sb \
	          -width 15 \
	          -orient horizontal \
	          -command "::SLEW::step_quat"

	label $base.quat.control.scroll.lbl2 \
	      -text "step 0 of 0" \
	      -font $::CONFIG::fonts(label) \
	      -width 15 \
	      -anchor c \
	      -justify center

	pack $base.quat.control.scroll.lbl \
	     $base.quat.control.scroll.sb \
	     $base.quat.control.scroll.lbl2 \
	     -side top\
	     -anchor c 

	frame $base.btns
	pack $base.btns -side top \
			-fill both 

	frame $base.btns.sep -borderwidth 1 \
			     -height 2 \
			     -relief raised

	pack $base.btns.sep -side top \
			    -fill x \
			    -pady 5

	button $base.btns.exec   -text Execute \
				 -font $::CONFIG::fonts(entry) \
				 -command "::SLEW::slew_exec"
				 
	button $base.btns.acept  -text Accept \
				 -font $::CONFIG::fonts(entry) \
				 -command "::SLEW::slew_accept"
				 
	button $base.btns.reject -text Reject \
				 -font $::CONFIG::fonts(entry) \
				 -command "::SLEW::slew_reject"
				 
	button $base.btns.comit  -textvariable ::SLEW::slew_commit_cnt_lbl \
				 -font $::CONFIG::fonts(entry) \
				 -command "::SLEW::slew_commit"
				 
	button $base.btns.reset  -text "Reset to Original" \
				 -font $::CONFIG::fonts(entry) \
				 -command "::SLEW::slew_reset"
				 
	pack $base.btns.exec $base.btns.acept \
	     $base.btns.reject $base.btns.comit \
	     $base.btns.reset \
	     -side left \
	     -padx 10 \
	     -fill both
	     
	     
	#	Add the OK button at the bottom
	set btn [frame .slew_ui.butn]
	pack $btn -side bottom \
		  -anchor c \
		  -ipadx 10 \
		  -fill x
	frame $btn.sep -borderwidth 1 \
		       -height 2 \
		       -relief raised
	pack $btn.sep -side top \
		      -fill x \
		      -pady 5
		      
	button $btn.ok -text OK \
		       -font $::CONFIG::fonts(label) \
		       -relief raised \
		       -command "set ::SLEW::slew_active 0; \
		                 set ::SLEW::slew_mode   none; \
		                 set ::SLEW::slew_steer  none; \
		                 ::SLEW::slew_reset; \
		                 destroy .slew_ui"

	pack $btn.ok -side top \
		     -anchor c	\
		     -pady 5

}

########################################################################
#	$Log: $
########################################################################
