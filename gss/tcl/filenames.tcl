########################################################################
#	$Id: $
########################################################################

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval FILES {
	variable pf

	variable indir
	variable outdir

	variable ssc 
	variable gssini 

	variable log
	variable osc

	variable asc_dump _ASC.TXT
	variable new_ini

	variable splash
	
	namespace export init init_log debug set_ini set_ssc get_env

}

########################################################################

proc ::FILES::init {} {
	variable ssc
	variable gssini
	variable indir
	variable outdir
	variable osc
	variable asc_dump
	variable new_ini

	set osc(BASE) ""
	set osc(ROOT) ""
	set osc(TXT) ""
	set osc(DAT) ""
	set osc(PRC) ""
	set osc(DMP) ""
	set osc(STRUCT) ""

	set root "/export/home/ssgstest/epoch"
	set indir "."

        set server [info hostname]
	set outdir "$root/output/$server"

	# Initialize the GSS.INI path and name
	::FILES::set_ini
	
	# Initialize the SSC.TXT path and name
	::FILES::set_ssc
	
	#	Initialize OSC to none
	set ::FILES::osc(PRC) ""
	set ::FILES::osc(TXT) ""
	set ::FILES::osc(DAT) ""

	#	Initialize splash screen GIF file location
	::FILES::set_splash

	#	use user's home directory (always exists)
	set hd [get_env HOME {}]
	set asc_dump ""
	set new_ini {}
	
}

########################################################################

proc ::FILES::init_log {} {
	variable pf
	variable log

	set pf [string toupper "$::CONFIG::data(program)$::CONFIG::data(sc_id)"]
	
	set log [::FILES::set_log]
	
}

########################################################################

proc ::FILES::get_env { str alt } {
	global env

	set x $alt
	catch {set x $env($str)} {}
	return $x

}

########################################################################

proc ::FILES::set_osc { {decl {}} } {
	global gi
	variable outdir
	variable pf
	variable osc

	set osc(BASE) ""
	set osc(ROOT) ""
	set osc(TXT) ""
	set osc(DAT) ""
	set osc(PRC) ""
	set osc(DMP) ""
	set osc(STRUCT) ""
	set width [$gi OSC_WIDTH 1.0]

	if { $decl == "" } {
	    set ot [format "%s%s" "FUL" \
	            [clock format $::CONFIG::data(asc_epoch) -format %y -gmt yes]]
	    
	} else {
	    set ot [format "%3.3d%s" [expr int(($::OSC_UI::decl_max+90)/$width)]\
	            [clock format $::CONFIG::data(asc_epoch) -format %y -gmt yes]]	    
	}

	set ot1 ZOE$::CONFIG::data(sc_id)SC$ot
	
	set ot2 [get_env GSS_OSC_FILE_DIR $outdir]/[string tolower $pf]/gss/$ot1

	set osc(BASE) $ot1
	set osc(ROOT) $ot2
	set osc(TXT) $ot2.txt
	set osc(DAT) $ot2.dat
	set osc(PRC) $ot2.prc
	set osc(DMP) "$ot2.mem"

        if {$decl == ""} {
	    set osc(STRUCT) [get_env GSS_OSC_FILE_DIR \
	    $outdir]/[string tolower $pf]/gss/ZOE$::CONFIG::data(sc_id)ST$ot\.mem
	    
	}

}

########################################################################

proc ::FILES::set_asc { } {
	variable outdir
	variable pf
	variable asc_dump

    set asc_dt [clock format $::CONFIG::data(asc_epoch) -format "j" -gmt yes]
	set asc_dump [get_env GSS_ASC_FILE_DIR $outdir]/[string tolower $pf]/gss/ASC$::CONFIG::data(sc_id)\_$asc_dt\.txt

}

########################################################################

proc ::FILES::set_ini { } {
	variable indir
	variable gssini

	set path [get_env GSS_INI_FILE_DIR $indir]
	set gssini [format "%s/%s%s_GSS.ini" $path \
                                             $::CONFIG::data(program) \
                                             $::CONFIG::data(sc_id)]
                                       
}


########################################################################

proc ::FILES::set_splash { } {
	variable indir
	variable splash

	set path [get_env GOES_GSS_DIR $indir]
	set splash [format "%s/tcl/splash.gif" $path]
                                       
}

########################################################################

proc ::FILES::set_new_ini { } {
        variable pf
	variable outdir
	variable new_ini

	set path [get_env GSS_NEW_INI_FILE_DIR $outdir]/[string tolower $pf]/gss
	set new_ini [format "%s/%s%s_GSS_ST%s_%s.ini" \
	                   $path \
                           $::CONFIG::data(program) \
                           $::CONFIG::data(sc_id) \
                           $::CONFIG::data(sts_active) \
                           [clock format \
                                  [clock seconds] \
                                  -format "j07/29/20M%S" \
                                  -gmt yes \
                           ]
                   ]
	
	return $new_ini
                                       
}


########################################################################

proc ::FILES::set_ssc { } {
	variable indir
	variable ssc

	set path [get_env GSS_SSC_FILE_DIR $indir]
	set ssc [format "%s/%s%s_SSC.txt" $path \
                                          $::CONFIG::data(program) \
                                          $::CONFIG::data(sc_id)]

}

########################################################################

proc ::FILES::set_log { } {
	variable outdir
	variable pf

	set dt [clock format [clock seconds] -format "j07/29/20M%S" -gmt yes]
	return [get_env GSS_LOG_FILE_DIR $outdir]/[string tolower $pf]/gss/$pf\_$dt.LOG

}

########################################################################
#	$Log: $
########################################################################
