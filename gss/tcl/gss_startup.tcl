########################################################################
#	$Id: $
########################################################################

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ::GSS:: {
	variable _wfont

	variable mainframe
	variable status
	variable progress_frm
	variable prgtext
	variable prgindic -1

	variable font
	variable font_name
	variable toolbar1  1
	variable toolbar2  1

	variable skymap_prnt_cntrl

	variable k_grid

	namespace export main

}

########################################################################
#	Put up User's Guide

proc ::GSS::gss_help_UG {} {
    global env
    set viewer $env(GOES_HTML_VIEWER)
    set hfile "$env(GOES_GSS_DIR)/help/GSS_UG.htm"
    set err [catch {exec -- $viewer $hfile &}]

}

########################################################################
#	Put up Maintenance Manual

proc ::GSS::gss_help_MM {} {
    global env
    set viewer $env(GOES_HTML_VIEWER)
    set hfile "$env(GOES_GSS_DIR)/help/GSS_MM.htm"
    set err [catch {exec -- $viewer $hfile &}]

}

########################################################################
#	Put up About screen

proc ::GSS::gss_about {} {
	global env
	
	#	Put up About box
	catch {destroy .about}
	set about_frm .about
	toplevel $about_frm
	wm title $about_frm "GSS INFO"
	wm geometry $about_frm "+0+0"
	     
	frame $about_frm.data \
	      -borderwidth 1 \
	      -relief sunken
	pack $about_frm.data \
	     -side top \
	     -anchor w \
	     -fill x

	label $about_frm.data.lbl \
	      -text "GOES N-P PROGRAM\n\n GSS Program Rev: $::MAIN::prog_rev\n\
	             TCL Version: $::MAIN::tcl_rev\n\
	             TCL Library Rev: $::MAIN::lib_rev\n\
	             C Library Build Date: $::MAIN::c_rev\n\n\
	             COPYRIGHT $::MAIN::c_year THE BOEING COMPANY,\n\
	             ALL RIGHTS RESERVED" \
	      -font $::CONFIG::fonts(entry) \
	      -justify left \
	      -width 45 \
	      -anchor w
	      
	pack $about_frm.data.lbl \
	     -side left \
	     -anchor w \
	     -fill x
	     
	frame $about_frm.btns \
	      -borderwidth 1 \
	      -relief sunken
	pack $about_frm.btns \
	     -side top \
	     -anchor w \
	     -fill x
	     
	button $about_frm.btns.ok \
	       -text "OK" \
	       -font $::CONFIG::fonts(label) \
	       -command "destroy $about_frm"
	pack $about_frm.btns.ok \
	     -side top 

}

########################################################################

proc ::GSS::create { } {
	global gi
	global tk_patchLevel
	global NUM_STS

	variable _wfont
	variable mainframe
	variable font
	variable prgtext
	variable prgindic

	variable k_grid
	variable frame_header

	variable progress_frm

	set yaw_flip_int(Upr) 0
	set yaw_flip_int(Inv) 1
	
	#	Put up splash screen and progress box
	catch {destroy .progress}
	set progress_frm .progress
	toplevel $progress_frm
	wm title $progress_frm "GSS STARTUP"
	wm geometry $progress_frm "+0+0"
		
	frame $progress_frm.splash -bg black
	pack $progress_frm.splash \
	     -side top \
	     -fill both
	     
	frame $progress_frm.data \
	      -borderwidth 1 \
	      -relief sunken
	pack $progress_frm.data \
	     -side top \
	     -anchor w \
	     -fill x

	label $progress_frm.data.lbl \
	      -text {} \
	      -font $::CONFIG::fonts(entry) \
	      -justify left \
	      -anchor w
	      
	pack $progress_frm.data.lbl \
	     -side left \
	     -anchor w \
	     -fill x
	      
	set im [image create photo -file $::FILES::splash]
	set w [image width $im]
	set h [image height $im]

	label $progress_frm.splash.image -image $im \
	                                 -width $w \
	                                 -height $h \
	                                 -bg black

	pack $progress_frm.splash.image -side left -anchor nw

	::update idletasks

	#	load GSS.INI file
	::GSSINI::init
	::GSSINI::read_ini $::FILES::gssini
	::MSG_DLG::progress "Loaded Initialization File $::FILES::gssini\n"

	#	Init User Quaternion
	::USER_QUAT::init

	#	configure TM
	::TM::init

	#	Initialize GSS Time
	::CONFIG::init_gss_time
	::MSG_DLG::progress "Initialized GSS Time ($::CONFIG::data(time))\n"

	#	Initialize ASC Epoch
	::CONFIG::init_asc_time
	::MSG_DLG::progress "Initialized ASC Time ($::CONFIG::data(asc_time))\n"

	#	initialize ephemeris
	set ::CONFIG::data(longitude) [$gi SC_LONGITUDE -75.0]
	ephem_init $::CONFIG::data(longitude) \
	           $yaw_flip_int($::CONFIG::data(yaw_flip))
	           
	::CONFIG::load_stayout_values
	::CONFIG::load_ephemeris_data_from_ini
	::MSG_DLG::progress "Initialized Ephemeris\n"

	#	load star catalog
	::MSG_DLG::progress "Loaded Star Catalog $::FILES::ssc \n"
	::ASC::init
	
	#	Set GSS Quaternion (could be from TLM)
	::USER_QUAT::update_gss_quat

	#	Init Slew Planning
	::SLEW::init
	
	#	Init output screens
	::OUTPUT::init

	::MSG_DLG::progress "Initialized Displays                                                  I \n"
	
	#	Create base window
	wm title . "GSS for $::CONFIG::data(program)\
	            $::CONFIG::data(sc_id) operating in\
	            $::CONFIG::data(tm_src) mode"
	set mainframe .mainframe
	frame $mainframe -borderwidth 1 \
	                 -relief raised

	#	Create menu bar area
	set menu_frm $mainframe.menu
	frame $menu_frm -borderwidth 1 \
		        -relief raised 

	#	Create button bar area
	set btn_frm $mainframe.buttons
	frame $btn_frm -borderwidth 1 \
		       -relief raised \
		       -background gray40
	
	#	Create skymap area
	set sky_frm $mainframe.sky
	frame $sky_frm -borderwidth 1 \
		       -relief raised
		       
	#	Create star tracker area
	set st_frm $mainframe.st
	frame $st_frm -borderwidth 1 \
	              -relief raised
	              
	#	Create the status bar area
	set stat_frm $mainframe.status
	frame $stat_frm -borderwidth 1 \
	                -relief raised \
	                -background gray40

	#	Display the frames
	pack $mainframe $menu_frm $btn_frm $sky_frm $st_frm $stat_frm \
	     -side top \
	     -anchor w \
	     -fill both \
	     -expand yes


	#	Now start populating the frames
	#	First the menu 
	
	menubutton $menu_frm.file -text "File" \
                                  -underline 0 \
                                  -direction below \
                                  -font $::CONFIG::fonts(label) \
                                  -menu $menu_frm.file.m
                                  
        menu $menu_frm.file.m -tearoff no 

	#	Add the File options.
	$menu_frm.file.m add command -label "Print Skymap" \
                                     -font $::CONFIG::fonts(label) \
                                     -command "::SKYMAP::ps"
	$menu_frm.file.m add command -label "Print All STs" \
                                     -font $::CONFIG::fonts(label) \
                                     -command "::ST::ps 1 3"
	$menu_frm.file.m add command -label "Print ST 1" \
                                     -font $::CONFIG::fonts(label) \
                                     -command "::ST::ps 1 1"
	$menu_frm.file.m add command -label "Print ST 2" \
                                     -font $::CONFIG::fonts(label) \
                                     -command "::ST::ps 2 2"
	$menu_frm.file.m add command -label "Print ST 3" \
                                     -font $::CONFIG::fonts(label) \
                                     -command "::ST::ps 3 3"
        $menu_frm.file.m add command -label "Exit" \
                                     -font $::CONFIG::fonts(label) \
                                     -command "::OUTPUT::release_ctrl TRUE;\
                                               after 1024;\
                                               exit"
                                     
        #	Help button
	menubutton $menu_frm.help -text "Help" \
                                  -underline 0 \
                                  -direction below \
                                  -font $::CONFIG::fonts(label) \
                                  -menu $menu_frm.help.m
                                  
        menu $menu_frm.help.m -tearoff no 

	#	Add Help selections
	$menu_frm.help.m add command -label "User's Guide" \
                                     -font $::CONFIG::fonts(label) \
                                     -command "::GSS::gss_help_UG"
	$menu_frm.help.m add command -label "Maintenance Manual" \
                                     -font $::CONFIG::fonts(label) \
                                     -command "::GSS::gss_help_MM"
        $menu_frm.help.m add command -label "About" \
                                     -font $::CONFIG::fonts(label) \
                                     -command "::GSS::gss_about"

	#	Display the widgets
	pack $menu_frm.file $menu_frm.help \
	     -side left \
		 -fill x

	#   Text displayed to right of menu frame
	label $menu_frm.lbl \
		-text "" \
		-font $::CONFIG::fonts(wtitle) \
		-anchor c \
		-justify center

	#	Display the widgets
	pack $menu_frm.lbl \
		-anchor c \
		-side top \
		-pady 1

	#	add control buttons.
	frame $btn_frm.spcr  \
	      -background gray40
	pack $btn_frm.spcr \
	     -side left \
	     -fill x \
	     -padx 15

	frame $btn_frm.a  \
	      -borderwidth 1 \
	      -relief sunken
	pack $btn_frm.a \
	     -side left \
	     -fill x \
	     -padx $::CONFIG::control_frm_padx

	frame $btn_frm.b  \
	      -borderwidth 1 \
	      -relief sunken
	pack $btn_frm.b \
	     -side left \
	     -fill x \
	     -padx $::CONFIG::control_frm_padx

	frame $btn_frm.c  \
	      -borderwidth 1 \
	      -relief sunken 
	pack $btn_frm.c \
	     -side left \
	     -fill x \
	     -padx $::CONFIG::control_frm_padx

	frame $btn_frm.d  \
	      -borderwidth 1 \
	      -relief sunken 
	pack $btn_frm.d \
	     -side left \
	     -fill x \
	     -padx $::CONFIG::control_frm_padx
	     
	#	CIS Button
	button $btn_frm.c.cis -text CIS \
                              -font $::CONFIG::fonts(label) \
	                      -command "::CIS_GUI::cis_gui"

	#	Slew Planning Button
	button $btn_frm.c.slew -text "SLEW" \
                               -font $::CONFIG::fonts(label) \
	                       -command "::SLEW::slew_gui"
	
	#	add SKYMAP button.
	button $btn_frm.a.skymap -text SKYMAP \
                                 -font $::CONFIG::fonts(label) \
			         -command "::SKYMAP::user_gui"

	#	add STAR TRACKER button.
	button $btn_frm.a.st -text "STAR TRACKER"	\
                             -font $::CONFIG::fonts(label) \
			     -command "::ST::user_gui"

	#	add OSC Upload Script / ASC Dump button.
	button $btn_frm.c.osc -text "CATALOG"	\
                              -font $::CONFIG::fonts(label) \
			      -command "::OSC_UI::osc_gui"

	#	add OUTPUT control button.
	button $btn_frm.c.out -text "OUTPUT"	\
                              -font $::CONFIG::fonts(label) \
			      -command "::OUTPUT::output_gui"

	#	add ALIGNMENTS control button.
	button $btn_frm.b.algn -text "ALIGNMENT"	\
                               -font $::CONFIG::fonts(label) \
			       -command "::ALIGN::align_gui"

	#	add QUATERNION control button.
	button $btn_frm.b.quat -text "QUATERNION"	\
                               -font $::CONFIG::fonts(label) \
			       -command "::USER_QUAT::quat_gui"

	#	add LEGEND button.
	button $btn_frm.d.leg -text "LEGEND"	\
                              -font $::CONFIG::fonts(label) \
			      -command "::LEGEND::legend_gui"

	#	add STATUS page button.
	button $btn_frm.d.stat -text "STATUS"	\
                              -font $::CONFIG::fonts(label) \
			      -command "::CONFIG::status_page"
				
	#	Display the buttons
	pack $btn_frm.a.skymap $btn_frm.a.st \
	     -side left
	pack $btn_frm.b.algn   $btn_frm.b.quat \
	     -side left
	pack $btn_frm.c.cis    $btn_frm.c.slew \
	     $btn_frm.c.osc    $btn_frm.c.out \
	     -side left
	pack $btn_frm.d.leg    $btn_frm.d.stat \
	     -side left


	#	CIS disabled in Planning mode, since there's no VT data.
	if {[string equal $::CONFIG::data(tm_src) Planning] == 1} {
		$btn_frm.c.cis configure -state disabled
		
	}

	::EXT_OBJS::init
	::BODY_OBJS::init
	::ALIGN::init

	#	Populate the status bar
	::CONFIG::status_bar $stat_frm

########################################################################
#	INITIALIZE SUB-PACKAGES
########################################################################

	::DISPLAY::init

	::LEGEND::init

	::SKYMAP::init $sky_frm

	::CIS_GUI::init

	::OSC_UI::init
    if {[string equal $::OSC_UI::osc_size_ok "FALSE"]} {
 	   $btn_frm.c.osc configure -state disabled
    }

	::ST::init $st_frm

	#	draw the ST's FOV's
	for {set st 1} {$st <= $NUM_STS} {incr st} {

		::DISPLAY::st_fov $st
	}


	#	CIS initialization
	CISInitial

	update idletasks

}

########################################################################

proc ::GSS::main {} {
	global gi
	variable prgindic
	variable prgtext
	variable mainframe

	option add *TitleFrame.font "$::CONFIG::fonts(label)"

	wm withdraw .
	wm title . "GSS"

	set prgtext   "Creating Control Window..."
	incr prgindic

	::GSS::create

	::MSG_DLG::progress "Populating Star Field."
	::SKYMAP::sky_stars

	destroy $::GSS::progress_frm
	wm deiconify .
	raise .

	if {($::CONFIG::data(tm_src) != "Planning")} {

		set ini_sc_id [$gi SC_ID 0]
		set tm_sc_id  [::GTACS::int_lrv_value $::GTACS::lrv_idx(SPACECRAFT_ID_LRV)]
	
		if {($ini_sc_id != $tm_sc_id)} {

		    $mainframe.menu configure -background red
			$mainframe.menu.lbl configure -text \
				"SPACECRAFT ID INCONSISTENT BETWEEN GSS AND TM: \
				 GSS = $ini_sc_id decimal; TM = $tm_sc_id decimal"
 
		    ::MSG_DLG::attention "Spacecraft ID contained in INI file \
		                          does not match that in telemetry" \
		                          red \
		                          black \
		                          -INSTRUCTION \
		                          "Check initialization file, re-run GSS \
		                           with the proper spacecraft, \nor re-run GSS \
		                           with proper telemetry stream selected."

		}
	                           
	}
	
}

########################################################################
#	$Log: $
########################################################################
