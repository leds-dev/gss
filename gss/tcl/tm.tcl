########################################################################
#	$Id: $
#	top-level tm interface

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval TM {
	variable read_tm
	variable planning
	variable timeIncr

	namespace export init update

}

########################################################################
#	set up a "function pointer" for TM based on mode.
#	configure for either gtacs or predictive tm.
#	gtacs is from libgtacs library.

proc ::TM::init { } {
	variable read_tm
	variable planning
	variable timeIncr

	set timeIncr 0

	if {[string length $::MAIN::sim] != 0} {
		set planning 0
		
		set read_tm fake_tm

	} elseif {[string equal $::CONFIG::data(tm_src) Planning]} {
		set planning 1

		set read_tm {::TM::propagate_tm}

	} else {
		set planning 0

		::MSG_DLG::progress "Connecting to telemetry"
		::GTACS::init
		::MSG_DLG::progress "\n"

		#	set future actions for $read_tm
		set read_tm {::GTACS::read_tm}

		after 1024 
	        ::update idletasks
		
		set err [gss_gtacs_read_tm]
                if {$err != 0} {
                    ::MSG_DLG::attention "Error for stream $::CONFIG::stream from gss_gtacs_read_tm $err" \
                             red \
                             black \
                             -LABEL $err
                }

#	        ::update idletasks
	        ::update

		set max_try 10
		set count 0
	        set curr_time [::GTACS::int_lrv_value $::GTACS::lrv_idx(FRAME_TIME_LRV)] 

	        while { ($curr_time == 0) } {
	                
		    set curr_time [::GTACS::int_lrv_value $::GTACS::lrv_idx(FRAME_TIME_LRV)] 

                    after 1024 
                    incr count
                    ::update idletasks
                    
                    if { $count > $max_try } {
                
                        ::MSG_DLG::attention "No time received from stream $::CONFIG::stream in $max_try attempts .\
                                              \nTelemetry stream $::CONFIG::stream may be down."\
    	                             red \
	                             black \
	                             -WAIT \
	                             -INSTRUCTION \
	                             "Hit OK to exit"
	                             
	                 exit

                    }
  
                }
                

	# Sync up with minor frame 0 or 16 (containing the ACE clock) if using ACE CLOCK	

            if {$::CONFIG::data(use_curr_time) == 2} {

                ::MSG_DLG::progress "Syncing to major frame."

		set max_try 34
                set count 0
		set prev_mnf_cnt -1
	        set mnf_cnt -1

	        while { (($mnf_cnt !=  1) || ($prev_mnf_cnt !=  0)) } {
	                
	    	    set err [gss_gtacs_read_tm]
                    if {$err != 0} {
                        ::MSG_DLG::attention "Error for stream $::CONFIG::stream from gss_gtacs_read_tm $err" \
                                 red \
                                 black \
                                 -LABEL $err
                    }

		    set prev_mnf_cnt $mnf_cnt
	            set mnf_cnt [::GTACS::int_lrv_value $::GTACS::lrv_idx(MNF_CNT_LRV)] 
                    after 1024 
                    incr count
                    ::update idletasks
                    
                    ::MSG_DLG::progress "."
                    
                    if { $count > $max_try } {
                
                        ::MSG_DLG::attn_yn ans "Could not sync to major frame in $max_try attempts.\
                                                \nTelemetry stream $::CONFIG::stream may be running intermittently.\
                                                \nTelemetry stream $::CONFIG::stream may not be running in real-time." \
	                             red \
	                             black \
	                             -INSTRUCTION \
	                             "Hit YES to try again, or NO to exit"
	                             
	                if { $ans == "YES"} {
                	    set count 0
			    set prev_mnf_cnt -1
	        	    set mnf_cnt -1
	        	} else {
	                    exit
	                }

                    }

                }
                

                ::MSG_DLG::progress "\n"
	    }

	# Set some initial values from TM

	    set status [::GTACS::str_lrv_value $::GTACS::lrv_idx(YAW_FLIP_LRV)]
                            
            set ::CONFIG::data(yaw_flip) "Upr"      
            if {[string toupper [string index $status 0]] == "I"} {
                set ::CONFIG::data(yaw_flip) "Inv"
            }

	    set status [::GTACS::str_lrv_value $::GTACS::lrv_idx(SC_EPHEMERIS_VALIDITY_LRV)]

            set ::CONFIG::data(valid_ephem) "VALID"      
            if {[string toupper [string index $status 0]] == "I"} {
                set ::CONFIG::data(valid_ephem) "INVALID"
                    
            }
                
            if {[string equal $::CONFIG::data(tm_src) Realtime]} {
                
                ::GTACS::read_tm
                
                if { ($::CONFIG::data(tlm_control_ip) == "000.000.000.000") && ($::CONFIG::data(tlm_control_pid) == 0) } {
                    ::MSG_DLG::progress "There is currently no GSS session with LRV control Active.\n"

                } else {
                
                    if { $::CONFIG::data(tlm_control_pid) < 0 } {
                        ::MSG_DLG::progress "The ground has locked out all GSS sessions from Activating LRV control\n"

                    } else {
                        ::MSG_DLG::progress "Another GSS session (IPA = [set ::CONFIG::data(tlm_control_ip)]\
                                             and PID = [set ::CONFIG::data(tlm_control_pid)]) has LRV control Active\n" 

                    }                                       

                }
                
            }                                                                               
                                                             
	}

}

########################################################################
#	read TM from the active stream (GTACS or planning).
#

proc ::TM::update { } {
	variable read_tm
	variable timeIncr 

	if { $::CONFIG::data(tm_src) == "Planning" } {

	#	Get the current GSS time
	    ::CONFIG::gmt
	    ephem_set_time $::CONFIG::data(gss_time)

	#   Construct the ephemeris basd on GSS time
	    ephem_predict_tm

	#	Update the TM array based on the computed ephemeris
	    update_tm_from_ephem

	#	Update the ephemeris
	    ephem_update

	#	Update the TCL variables that drive the status screens
	    set data [GetTM_ECI2BodyQuat]
	    set ::USER_QUAT::tlm_quat(0) [format "%9.7f" [lindex $data 0]]
	    set ::USER_QUAT::tlm_quat(1) [format "%9.7f" [lindex $data 1]]
	    set ::USER_QUAT::tlm_quat(2) [format "%9.7f" [lindex $data 2]]
	    set ::USER_QUAT::tlm_quat(3) [format "%9.7f" [lindex $data 3]]

            ::USER_QUAT::update_gss_quat
            	    
	} else {

	#	Read TM from the selected stream	
	    ::GTACS::read_tm
	    
	#       Update the OUTPUT control status in Realtime mode only
    	    if {($::CONFIG::data(tm_src) == "Realtime")} {
	    	::OUTPUT::update_ctrl
	    }

	#	Get the current GSS time
	    ::CONFIG::gmt
	    ephem_set_time $::CONFIG::data(gss_time)

	#	Copy ephem from TM, adjusting for time offset due to a slew in progress
	    update_ephem_from_tm $::CONFIG::data(gss_time_adjust)

	#	Update the ephemeris
	    ephem_update

	#       Update the gss quaternion (may be from tlm)
            ::USER_QUAT::update_gss_quat
	    
	}

}



########################################################################
#	use TM to update ephemeris
#

proc ::TM::propagate_tm { } {

	ephem_predict_tm

}


########################################################################
#	$Log: $
########################################################################
