########################################################################
#	$Id: $
########################################################################
#	OSC User Interface

########################################################################

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ::OSC_UI:: {
	variable decl_ok 0
	variable decl_min
	variable decl_max
	variable decl_width
	variable onoff
	variable ebox
	variable osc_strip_template
	variable osc_full_template
	variable osc_size_ok
		
	namespace export init osc_gui output

}

########################################################################

proc ::OSC_UI::init { } {

	global gi
	
	variable onoff
	variable decl_min
	variable decl_max
	variable decl_width
	variable osc_strip_template
	variable osc_full_template
	variable osc_size_ok
		
	set onoff(0) disabled
	set onoff(1) normal

	set decl_min 0.0
	set decl_max [expr $decl_min + $::GSSINI::gssini(OSC_WIDTH)]
	
	#	index OSC_INDEX array items based on OSC_WIDTH.
	set decl_width(0.5) 0
	set decl_width(1.0) 1
	set decl_width(2.0) 2

	# 	assume initially that strip allocations are consistent with ACE memory available for OSC
	set osc_size_ok "TRUE"
        
	#	generate complete OSC template filenames
	set osc_strip_template [::FILES::get_env GSS_TPL_FILE_DIR ""]/[$gi STRIP_OSC_TEMPLATE ""] 
	set osc_full_template  [::FILES::get_env GSS_TPL_FILE_DIR ""]/[$gi FULL_OSC_TEMPLATE ""] 
		
	#	funnel OSC data to C layer at startup
	osc_strips
	osc_addr
	osc_keys
	
}

########################################################################
#	funnel osc strip dimensioning for the given strip width into C library
#	decl must be float.  incr decl is int.

proc ::OSC_UI::osc_strips { } {
	variable decl_width
	variable osc_size_ok

	set width $::GSSINI::gssini(OSC_WIDTH)
	set w $decl_width($width)
	
	set max_stars $::GSSINI::gssini(MAX_OSC_STARS)
	set max_stars_per_strip $::GSSINI::gssini(MAX_EDIT_AREA_STARS)

	set idx 0
	set strip_sum 0
	# 	osc_size_test = 0 : strip allocations are consistent with
	#				both the edit area allocation and
	#				ACE memory available for OSC
	#	osc_size_test = 1 : strip allocations are not consistent with
	#				the edit area allocation only
	#	osc_size_test = 2 : strip allocations are not consistent with
	#				ACE memory available for OSC only
	#	osc_size_test = 3 : strip allocations are not consistent with
	#				both the edit area allocation and
	#				ACE memory available for OSC
	set osc_size_test 0
	set vmax 0
	for {set decl -90.0} {$decl < 90.0} {set decl [expr $width + $decl]} {
		set v [lindex $::GSSINI::osc_index($decl) $w]
		set strip_sum [expr $strip_sum + $v]
		GetOSC_Strips $idx $v
		incr idx
		if {$v > $max_stars_per_strip} {
		    	set osc_size_test 1
		}
		if {$v > $vmax} {
			set vmax $v
		}
	}
	
	if {($strip_sum > $max_stars) && ($osc_size_test == "0")} {
		set osc_size_test 2
	} elseif {($strip_sum > $max_stars) && ($osc_size_test != "0")} {
		set osc_size_test 3
        }
        
        switch $osc_size_test {
        	"1" { set osc_size_ok "FALSE"
        	    ::MSG_DLG::attention "More stars per declination strip($vmax) than permitted($max_stars_per_strip)\
		   			    \nCatalog generation GUI disabled" \
			  		    red \
					    black \
					    -INSTRUCTION \
					    "Check max number of stars in a single declination strip in INI file"
		    ::MSG_DLG::msg_log "More stars per declination strip than permitted($max_stars_per_strip). \
		   			Catalog generation GUI disabled" }
        	"2" { set osc_size_ok "FALSE"        	        	        	       	
		    ::MSG_DLG::attention "More OSC Stars($strip_sum) than permitted($max_stars)\
		   			    \nCatalog generation GUI disabled" \
			  		    red \
					    black \
					   -INSTRUCTION \
					   "Check OSC strip allocations in INI file"
		    ::MSG_DLG::msg_log "More OSC Stars($strip_sum) than permitted($max_stars). \
		   			Catalog generation GUI disabled" }
        	"3" { set osc_size_ok "FALSE"        	        	        	       	
		    ::MSG_DLG::attention "More stars per declination strip($vmax) than permitted($max_stars_per_strip)\
		                            \nMore OSC Stars($strip_sum) than permitted($max_stars)\
		   			    \nCatalog generation GUI disabled" \
			  		    red \
					    black \
					   -INSTRUCTION \
					   "Check max number of stars in a single declination strip and \
					    OSC strip allocations in INI file"
		    ::MSG_DLG::msg_log "More stars per declination strip than permitted($max_stars_per_strip). \
		                        More OSC Stars($strip_sum) than permitted($max_stars). \
		   			Catalog generation GUI disabled" }
        	default { set osc_size_ok "TRUE" }		   			
	}

}

########################################################################
#	load osc addresses

proc ::OSC_UI::osc_addr { } {
	global gi
	
	set MaxEditStars	$::GSSINI::gssini(MAX_EDIT_AREA_STARS)
	set MaxOSCStars		$::GSSINI::gssini(MAX_OSC_STARS)
	set addr		$::GSSINI::gssini(OSC_UPLOAD_ADDRESSES)
	set StarAddrState	[lindex $addr 0]
	set StarAddr	 	[lindex $addr 1]
	set CatAddrState	[lindex $addr 2]
	set CatAddr		[lindex $addr 3]
	set EditAddrState	[lindex $addr 4]
	set EditAddr		[lindex $addr 5]
	set DecStepAddrState	[lindex $addr 6]
	set DecStepAddr		[lindex $addr 7]

	GetUploadIni	$MaxEditStars $MaxOSCStars $StarAddrState	\
			$StarAddr $CatAddrState $CatAddr $EditAddrState	\
			$EditAddr $DecStepAddrState $DecStepAddr 


}


########################################################################
#	load osc template keys used for symbol substitution when
#	generating OSC upload procedure.

proc ::OSC_UI::osc_keys { } {
	global gi
		
	#    0   Text at top of output full OSC PROC indicating how file was generated
	GetUploadKeys 0 [lindex [$gi "FULLOSCHEADER_KEY" "@FullOSCHeader"] 0]

	#    1   Text at bottom of output full OSC PROC indicating how file was generated		
	GetUploadKeys 1 [lindex [$gi "FULLOSCFOOTER_KEY" "@FullOSCFooter"] 0]

	#    2   Number of stars in the full OSC		
	GetUploadKeys 2 [lindex [$gi "NUMCATSTARS_KEY" "@NumCatStars"] 0]

	#    3   Number of stars uploaded, including spares		
	GetUploadKeys 3 [lindex [$gi "NUMUPLOADSTARS_KEY" "@NumUploadStars"] 0]		

	#    Declination Strip Width:
	#    4   Address state of ADA variable DEC_STEP
	GetUploadKeys 4 [lindex [$gi "DECSTEPSTARTADDRSTATE_KEY" "@DecStepStartAddrState"] 0]	

	#    5   Address ADA variable  DEC_STEP
	GetUploadKeys 5 [lindex [$gi "DECSTEPSTARTADDR_KEY" "@DecStepStartAddr"] 0]

	#    6   1/2/3 indicating the index of the declination step width
	GetUploadKeys 6 [lindex [$gi "DECSTEPWIDTH_KEY" "@DecStepWidth"] 0]

	#    7   0.5/1.0/2.0 indicating the declination step width in degrees [*]
	GetUploadKeys 7 [lindex [$gi "DECSTEPWIDTHDEG_KEY" "@DecStepWidthDeg"] 0]

	#    Star Data Upload:
	#    8   Address state of ADA variable STAR_AREA
	GetUploadKeys 8 [lindex [$gi "STARAREASTARTADDRSTATE_KEY" "@StarAreaStartAddrState"] 0]	

	#    9   Address of ADA variable STAR_AREA
	GetUploadKeys 9 [lindex [$gi "STARAREASTARTADDR_KEY" "@StarAreaStartAddr"] 0]

	#    10   Number of 32-bit words in the Star dataload	
	GetUploadKeys 10 [lindex [$gi "NUM32BWORDSSTAR_KEY" "@Num32bWordsStar"] 0]

	#    11   Checksum for the Star dataload
	GetUploadKeys 11 [lindex [$gi "CHECKSUMSTAR_KEY" "@CheckSumStar"] 0]

	#    12   	Dataload commands for the Star dataload [big]
	GetUploadKeys 12 [lindex [$gi "FULLOSCSTARDATA_KEY" "@FullOSCStarData"] 0]
	
	#    Catalog Structure Upload:
	#    13   Address state of ADA variable CATALOG
	GetUploadKeys 13 [lindex [$gi "CATAREASTARTADDRSTATE_KEY" "@CatAreaStartAddrState"] 0]

	#    14   Address of ADA variable CATALOG
	GetUploadKeys 14 [lindex [$gi "CATAREASTARTADDR_KEY" "@CatAreaStartAddr"] 0]

	#    15   Number of 32-bit words in the Catalog dataload
	GetUploadKeys 15 [lindex [$gi "NUM32BWORDSCAT_KEY" "@Num32bWordsCat"] 0]

	#    16   Checksum for the Catalog dataload
	GetUploadKeys 16 [lindex [$gi "CHECKSUMCAT_KEY" "@CheckSumCat"] 0]

	#    17   Dataload commands for the Catalog dataload [big]
	GetUploadKeys 17 [lindex [$gi "FULLOSCCATDATA_KEY" "@FullOSCCatData"] 0]

	#    For the Strip OSC Load
	#	    Info Only:
	#    18   Text at top of output strip OSC PROC indicating how file was generated
	GetUploadKeys 18 [lindex [$gi "STRIPOSCHEADER_KEY" "@StripOSCHeader"] 0]

	#    19   Text at bottom of output strip OSC PROC indicating how file was generated
	GetUploadKeys 19 [lindex [$gi "STRIPOSCFOOTER_KEY" "@StripOSCFooter"] 0]

	#    20   Lower bound of declination strip in degrees
	GetUploadKeys 20 [lindex [$gi "LOWDECDEG_KEY" "@LowDecDeg"] 0]

	#    21   Upper bound of declination strip in degrees
	GetUploadKeys 21 [lindex [$gi "HIGHDECDEG_KEY" "@HighDecDeg"] 0]

	#    Check out/in:
	#    22   Index of strip to be loaded
	GetUploadKeys 22 [lindex [$gi "UPLOADDECSTRIPINDX_KEY" "@UpLoadDecStripIndx"] 0]

	#    Edit Area Data Uplaod:
	#    23   Address state of ADA variable EDIT_AREA
	GetUploadKeys 23 [lindex [$gi "EDITAREASTARTADDRSTATE_KEY" "@EditAreaStartAddrState"] 0]

	#    24   Address of ADA variable EDIT_AREA
	GetUploadKeys 24 [lindex [$gi "EDITAREASTARTADDR_KEY" "@EditAreaStartAddr"] 0]

	#    25   Checksum for the Strip dataload
	GetUploadKeys 25 [lindex [$gi "STARLISTCHECKSUM_KEY" "@StarListCheckSum"] 0]

	#    26   Number of OSC stars to load for the strip [formerly NumStarsCat]
	GetUploadKeys 26 [lindex [$gi "NUMSTRIPSTARS_KEY" "@NumStripStars"] 0]

	#    27   Dataload commands for the Strip dataload [big]
	GetUploadKeys 27 [lindex [$gi "STRIPOSCSTARDATA_KEY" "@StripOSCStarData"] 0]

	#    28   Root filename of OSC PROC
	GetUploadKeys 28 [lindex [$gi "OSCLOADPROC_KEY" "@OSCLoadProc"] 0]

}
	
########################################################################
# Make sure declination is rounded down to integer multiple
# of ini file strip width

proc ::OSC_UI::_set_decl_min {} {
	global gi
	set iniwidth [$gi OSC_WIDTH 1.0]

	if {[catch {expr $::OSC_UI::decl_min + 0}] == 1} {
	    ::MSG_DLG::attention "Invalid Entry" \
	                          red \
	                          black \
	                          -INSTRUCTION \
	                          "Re-enter the Minimum declination" 

	    return

	}

	if {$::OSC_UI::decl_min < -90.0} {set ::OSC_UI::decl_min -90.0}

	if {$::OSC_UI::decl_min > [expr 90.0 - $iniwidth]} {
	    set ::OSC_UI::decl_min [expr 90.0 - $iniwidth]
	    
	}

    set ::OSC_UI::decl_min [expr double(int($::OSC_UI::decl_min / \
                                     $iniwidth) * $iniwidth)]
	set ::OSC_UI::decl_max [expr $::OSC_UI::decl_min \
	                             + $iniwidth]
	    
}

########################################################################
# Make sure declination is rounded down to integer multiple
# of ini file strip width

proc ::OSC_UI::_set_decl_max {} {
	global gi
	set iniwidth [$gi OSC_WIDTH 1.0]

	if {[catch {expr $::OSC_UI::decl_max + 0}] == 1} {
	    ::MSG_DLG::attention "Invalid Entry" \
	                          red \
	                          black \
	                          -INSTRUCTION \
	                          "Re-enter the Maximum declination" 

	    return

	}
	
	if {$::OSC_UI::decl_max > 90.0} {set ::OSC_UI::decl_max 90.0}
	
	if {$::OSC_UI::decl_max < [expr -90.0 + $iniwidth]} {
	    set ::OSC_UI::decl_max [expr -90.0 + $iniwidth]
	    
	}
	
    set ::OSC_UI::decl_max [expr double(int($::OSC_UI::decl_max / \
                                     $iniwidth) * $iniwidth)]
	set ::OSC_UI::decl_min [expr $::OSC_UI::decl_max \
	                             - $iniwidth]
}

########################################################################
# OSC Script generation Control window
proc ::OSC_UI::osc_gui { } {
	global gi
	variable decl_ok
	variable decl_min
	variable decl_max
	variable ebox
	variable osc_full_template
	variable osc_strip_template

	catch {destroy .osc_ui}
	toplevel .osc_ui
	wm title .osc_ui "STAR CATALOG CONTROL"
	wm geometry .osc_ui "-0-0"
	set f .osc_ui.top
	frame $f
	pack $f

	#	Give current strip width
	#	Output Files
	
	frame $f.frm -borderwidth 1 \
		     -relief sunken
	pack $f.frm -side top \
		    -fill both
		    
	label $f.frm.title -text "Input Data" \
			   -font $::CONFIG::fonts(label) \
			   -anchor w
	pack $f.frm.title -side top \
			  -anchor w \
			  -fill x \
			  -pady 2
		   
	frame $f.frm.sw
	pack $f.frm.sw -side top \
		       -fill x \
		       -anchor w
		   
	label $f.frm.sw.lbl1 -text "Initialization (INI) file declination\
	                            strip width: " \
			     -font $::CONFIG::fonts(entry) \
			     -width 34 \
			     -anchor w 
			     
	if { $::CONFIG::data(tm_src) == "Planning" } {
	    set clr gray70
	    
	} else {

		set tm_width [string range \
						[::GTACS::str_lrv_value $::GTACS::lrv_idx(STAR_CAT_STRP_WIDTH_LRV)] \
						0 2]
     
        if {[catch {expr $tm_width + 0}] == 1} {
			set tm_width "No Catalog Loaded"  
        }

	    if { [$gi OSC_WIDTH 0.0] != $tm_width } {
	        set clr red
	    } else {
			set clr green    
	    }
	    
	}
	
	label $f.frm.sw.lbl2 -text [$gi OSC_WIDTH 0.0] \
			     -font $::CONFIG::fonts(entry) \
			     -background $clr \
			     -anchor w
	label $f.frm.sw.lbl3 -text " deg" \
			     -font $::CONFIG::fonts(entry) \
			     -anchor w 

	pack $f.frm.sw.lbl1 $f.frm.sw.lbl2 $f.frm.sw.lbl3 \
	     -side left \
	     -fill x \
	     -pady 3 \
	     -anchor w

	if {$::CONFIG::data(tm_src) != "Planning"} {
		  
	    frame $f.frm.swtm
	    pack $f.frm.swtm -side top \
		       -fill x \
		       -anchor w
		   
	    label $f.frm.swtm.lbl1 -text "Telemetry declination\
	                            strip width: " \
			     -font $::CONFIG::fonts(entry) \
			     -width 34 \
			     -anchor w 
			     
	    label $f.frm.swtm.lbl2 -text $tm_width \
			         -font $::CONFIG::fonts(entry) \
			         -background $clr \
			         -anchor w
			         
	    label $f.frm.swtm.lbl3 -text " deg" \
			         -font $::CONFIG::fonts(entry) \
			         -anchor w

	    pack $f.frm.swtm.lbl1 $f.frm.swtm.lbl2 $f.frm.swtm.lbl3 \
	         -side left \
	         -fill x \
	         -pady 3 \
	         -anchor w

	}
	          		                          
	#	Indicate the ASC Epoch	
	frame $f.frm.asc
	pack $f.frm.asc -side top \
	                  -fill x \
	                  -anchor w
	                  
	label $f.frm.asc.lbl7 -text "ASC Epoch: " \
	    		     -font $::CONFIG::fonts(entry) \
			         -width 34 \
			         -anchor w 
			     
	label $f.frm.asc.lbl8 -text $::CONFIG::data(asc_time) \
			         -font $::CONFIG::fonts(entry) \
			         -background gray70 \
			         -anchor w
			         
	label $f.frm.asc.lbl9 -text " GMT" \
			         -font $::CONFIG::fonts(entry) \
			         -anchor w

	pack $f.frm.asc.lbl7 $f.frm.asc.lbl8 $f.frm.asc.lbl9\
	         -side left \
	         -fill x \
	         -pady 3 \
	         -anchor w

	#	Give the SSC
	frame $f.frm.ssc
	pack $f.frm.ssc -side top \
	                -fill x \
	                -anchor w
	                  
	label $f.frm.ssc.lbl -text "SIAD Star Catalog (SSC) file: " \
			     -font $::CONFIG::fonts(entry) \
			     -width 34 \
			     -anchor w 
	                  
	label $f.frm.ssc.file -textvariable ::FILES::ssc \
			      -font $::CONFIG::fonts(entry) \
			      -background $::CONFIG::color(ssc_file) \
			      -anchor w

	pack $f.frm.ssc.lbl $f.frm.ssc.file -side left \
	                                    -fill x \
	                                    -pady 3 \
	                                    -anchor w

	#	Give the INI file
	frame $f.frm.ini
	pack $f.frm.ini -side top \
	                -fill x \
	                -anchor w
	                  
	label $f.frm.ini.lbl -text "GSS Initialization (INI) file: " \
			     -font $::CONFIG::fonts(entry) \
			     -width 34 \
			     -anchor w 
	                  
	label $f.frm.ini.file -textvariable ::FILES::gssini \
			      -font $::CONFIG::fonts(entry) \
			      -background $::CONFIG::color(gss_ini) \
			      -anchor w

	pack $f.frm.ini.lbl $f.frm.ini.file -side left \
	                                    -fill x \
	                                    -pady 3 \
	                                    -anchor w

	#	Give the Templates
	frame $f.frm.tplt1
	pack $f.frm.tplt1 -side top \
	                  -fill x \
	                  -anchor w
	                  
	label $f.frm.tplt1.lbl -text "Full Catalog PROC Template: " \
			       -font $::CONFIG::fonts(entry) \
			       -width 34 \
			       -anchor w 
	                  
	label $f.frm.tplt1.file -text $osc_full_template \
			        -font $::CONFIG::fonts(entry) \
			        -background gray70 \
			        -anchor w

	pack $f.frm.tplt1.lbl $f.frm.tplt1.file -side left \
	                                        -fill x \
	                                        -pady 3 \
	                                        -anchor w

	frame $f.frm.tplt2
	pack $f.frm.tplt2 -side top \
	                  -fill x \
	                  -anchor w
	                  
	label $f.frm.tplt2.lbl -text "Single Strip PROC Template: " \
			       -font $::CONFIG::fonts(entry) \
			       -width 34 \
			       -anchor w 
	                  
	label $f.frm.tplt2.file -text $osc_strip_template \
			        -font $::CONFIG::fonts(entry) \
			        -background gray70 \
			        -anchor w

	pack $f.frm.tplt2.lbl $f.frm.tplt2.file -side left \
	                                        -fill x \
	                                        -pady 3 \
	                                        -anchor w

	#	Output Files
	
	frame $f.out -borderwidth 1 \
		     -relief sunken
	pack $f.out -side top \
		    -fill both
		    
	label $f.out.title -text "Latest Output Files Generated" \
			   -font $::CONFIG::fonts(label) \
			   -anchor w
	pack $f.out.title -side top \
			  -anchor w \
			  -fill x \
			  -pady 2

	#	Give the generated OSC filename (prc)
	frame $f.out.file1
	pack $f.out.file1 -side top \
			  -fill x \
			  -anchor w

	label $f.out.file1.lbl -text "Executable OSC Upload PROC: " \
			      -font $::CONFIG::fonts(entry) \
			      -width 34 \
			      -anchor w

	label $f.out.file1.lbl2 -textvariable ::FILES::osc(PRC) \
			       -font $::CONFIG::fonts(entry) \
			       -anchor w \
			       -width 80 \
			       -borderwidth 1 \
			       -relief ridge
				    
	pack $f.out.file1.lbl $f.out.file1.lbl2 \
	     -side left \
	     -anchor w \
	     -pady 3 

	#	Give the generated OSC GENLOAD data filename (dat)
	frame $f.out.file2
	pack $f.out.file2 -side top \
			  -fill x \
			  -anchor w

	label $f.out.file2.lbl -text "GENLOAD Input file: " \
			      -font $::CONFIG::fonts(entry) \
			      -width 34 \
			      -anchor w

	label $f.out.file2.lbl2 -textvariable ::FILES::osc(DAT) \
			       -font $::CONFIG::fonts(entry) \
			       -anchor w \
			       -width 80 \
			       -borderwidth 1 \
			       -relief ridge
				    
	pack $f.out.file2.lbl $f.out.file2.lbl2 \
	     -side left \
	     -anchor w \
	     -pady 3 


	#	Give the generated OSC memory dump filename
	frame $f.out.file3
	pack $f.out.file3 -side top \
			  -fill x \
			  -anchor w

	label $f.out.file3.lbl -text "Star Data Dump Comparison file: " \
			      -font $::CONFIG::fonts(entry) \
			      -width 34 \
			      -anchor w

	label $f.out.file3.lbl2 -textvariable ::FILES::osc(DMP) \
			       -font $::CONFIG::fonts(entry) \
			       -anchor w \
			       -width 80 \
			       -borderwidth 1 \
			       -relief ridge
				    
	pack $f.out.file3.lbl $f.out.file3.lbl2 \
	     -side left \
	     -anchor w \
	     -pady 3 


	#	Give the generated OSC catalog structure filename
	frame $f.out.file4
	pack $f.out.file4 -side top \
			  -fill x \
			  -anchor w

	label $f.out.file4.lbl -text "OSC Structure Dump Comparison file: " \
			      -font $::CONFIG::fonts(entry) \
			      -width 34 \
			      -anchor w

	label $f.out.file4.lbl2 -textvariable ::FILES::osc(STRUCT) \
			       -font $::CONFIG::fonts(entry) \
			       -anchor w \
			       -width 80 \
			       -borderwidth 1 \
			       -relief ridge
				    
	pack $f.out.file4.lbl $f.out.file4.lbl2 \
	     -side left \
	     -anchor w \
	     -pady 3 

	#	Give the generated OSC filename (txt)
	frame $f.out.file5
	pack $f.out.file5 -side top \
			  -fill x \
			  -anchor w

	label $f.out.file5.lbl -text "Human-Readable Tabular OSC: " \
			      -font $::CONFIG::fonts(entry) \
			      -width 34 \
			      -anchor w

	label $f.out.file5.lbl2 -textvariable ::FILES::osc(TXT) \
			       -font $::CONFIG::fonts(entry) \
			       -anchor w \
			       -width 80 \
			       -borderwidth 1 \
			       -relief ridge
				    
	pack $f.out.file5.lbl $f.out.file5.lbl2 \
	     -side left \
	     -anchor w \
	     -pady 3 

	#	Give the generated ASC filename
	frame $f.out.file6
	pack $f.out.file6 -side top \
			  -fill x \
			  -anchor w

	label $f.out.file6.lbl -text "Human-Readable Tabular ASC: " \
			      -font $::CONFIG::fonts(entry) \
			      -width 34 \
			      -anchor w

	label $f.out.file6.lbl2 -textvariable ::FILES::asc_dump \
			       -font $::CONFIG::fonts(entry) \
			       -anchor w \
			       -width 80 \
			       -borderwidth 1 \
			       -relief ridge
				    
	pack $f.out.file6.lbl $f.out.file6.lbl2 \
	     -side left \
	     -anchor w \
	     -pady 3 
	
	
	
	#	Single Strip
	frame $f.one -borderwidth 1 \
		     -relief sunken
	pack $f.one -side top \
		    -fill both
		    
	label $f.one.title -text "Single Strip Upload" \
			   -font $::CONFIG::fonts(label) \
			   -anchor w
	pack $f.one.title -side top \
			  -anchor w \
			  -fill x \
			  -pady 2

	frame $f.one.data
	pack $f.one.data -side top \
			 -fill x \
			 -anchor w

	frame $f.one.data.min
	pack $f.one.data.min -side top \
			     -fill x \
			     -anchor w
			     
	label $f.one.data.min.lbl -text "Declination strip lower bound (deg):      \n\
			    	         (rounded down to integer multiple of current strip width)"\
				  -font $::CONFIG::fonts(entry) \
				  -justify left \
				  -anchor nw
	pack $f.one.data.min.lbl -side left \
				 -anchor nw
			
			
	entry $f.one.data.min.ent -textvariable ::OSC_UI::decl_min \
				  -font $::CONFIG::fonts(entry) \
				  -width 5
			 
	pack $f.one.data.min.ent -side right \
				 -anchor ne \
				 -padx 5

	bind $f.one.data.min.ent <Return> ::OSC_UI::_set_decl_min

	frame $f.one.data.max
	pack $f.one.data.max -side top \
			     -fill x \
			     -anchor w
			     
	label $f.one.data.max.lbl -text "Declination strip upper bound (deg):      \n\
			    	         (rounded down to integer multiple of current strip width)"\
				  -font $::CONFIG::fonts(entry) \
				  -justify left \
				  -anchor nw
	pack $f.one.data.max.lbl -side left \
				 -anchor nw
			
			
	entry $f.one.data.max.ent -textvariable ::OSC_UI::decl_max \
				  -font $::CONFIG::fonts(entry) \
				  -width 5
			 
	pack $f.one.data.max.ent -side right \
				 -anchor ne \
				 -padx 5

	bind $f.one.data.max.ent <Return> ::OSC_UI::_set_decl_max

	#	Generate a single strip		
	button $f.one.btn -text "Generate a Single Strip Upload PROC" \
			  -font $::CONFIG::fonts(entry) \
			  -command "::OSC_UI::_set_decl_min; \
			  	    set ::OSC_UI::decl_ok 1;\
			  	    ::OSC_UI::output"

	pack $f.one.btn -side top \
			-pady 5


	#	FULL OSC Script
	frame $f.full -borderwidth 1 \
		      -relief sunken
	pack $f.full -side top \
		     -fill both
		    
	label $f.full.title -text "Full Catalog Upload" \
			    -font $::CONFIG::fonts(label) \
			    -anchor w
	pack $f.full.title -side top \
			   -anchor w \
			   -fill x \
			   -pady 2

	button $f.full.btn -text "Generate a Full Catalog Upload PROC" \
			   -font $::CONFIG::fonts(entry) \
			   -command "set ::OSC_UI::decl_ok 0;\
			             set ::OSC_UI::decl_min -90.0;\
			             set ::OSC_UI::decl_max 90.0;\
			  	     ::OSC_UI::output"

	pack $f.full.btn -side top \
			 -pady 5


	#	Dump ASC
	frame $f.asc -borderwidth 1 \
		     -relief sunken
	pack $f.asc -side top \
		    -fill both
		    
	label $f.asc.title -text "Aquisition Star Catalog (ASC)" \
			   -font $::CONFIG::fonts(label) \
			   -anchor w
	pack $f.asc.title -side top \
			  -anchor w \
			  -fill x \
			  -pady 2

	button $f.asc.btn -text "Generate a Human-Readable Tabular ASC" \
			  -font $::CONFIG::fonts(entry) \
			  -command "::OSC_UI::output_asc"

	pack $f.asc.btn -side top \
		        -pady 5


	#	OK button to close out the window
	frame $f.btn -borderwidth 1 \
		     -relief sunken
	pack $f.btn -side top \
		    -fill both
		    
	button $f.btn.ok -text OK \
			 -font $::CONFIG::fonts(entry) \
			 -command "destroy .osc_ui"
			 
	pack $f.btn.ok -side top \
		       -pady 5

	}

########################################################################
#	Output OSC

proc ::OSC_UI::output { } {
	global gi

	variable decl_ok
	variable decl_min
	variable decl_max
	variable osc_strip_template
	variable osc_full_template

	#	if width is no good, don't output anything.
	set width $::GSSINI::gssini(OSC_WIDTH)

	if {$decl_ok} {
	    ::FILES::set_osc $decl_min
            set full NO
            set template_file $osc_strip_template

	} else {
	    ::FILES::set_osc ""
            set full YES        
            set template_file $osc_full_template
	    
	}
       
	#	Tell the proc generator whether the ini file is using 
	#	MST style LRV mnemonics (Lxxxxx) or not
	set mst_lrvs [$gi MST_LRV_MNEMONICS 0]

	#   Check that able to create .txt file 
	set err [catch {set outid [open $::FILES::osc(TXT) w]}]
	if {$err == 1} {
	   ::MSG_DLG::attention "Could not Create OSC Text file \n$::FILES::osc(TXT)"\
					red \
					black \
					-INSTRUCTION "Check file name and directory access privileges"
	   set ::FILES::osc(TXT) ""
	   set ::FILES::osc(DAT) ""
	   set ::FILES::osc(PRC) ""
	   set ::FILES::osc(DMP) ""
	   set ::FILES::osc(STRUCT) ""
	   return
	} else {
	   close $outid
	   file delete $::FILES::osc(TXT)   
	}

	#   Create .txt file	
	dump_osc $::FILES::osc(TXT) \
		   $::FILES::ssc \
		   $::CONFIG::data(asc_time) \
		   $::MAIN::prog_rev \
		   $decl_min \
		   $decl_max \
		   $width 
		   
	#   Check that template file exists
	set err [catch {set inid [open $template_file r]}]
	if {$err == 1} {
	   ::MSG_DLG::attention "Could not Open Template file \n$template_file \nfor read access" \
                             red \
                             black \
                             -INSTRUCTION "Check file name and directory access privileges"
	   set ::FILES::osc(DAT) ""
	   set ::FILES::osc(PRC) ""
	   set ::FILES::osc(DMP) ""
	   set ::FILES::osc(STRUCT) ""
	   return
	} else {
	   close $inid
	}

	#   Check that able to create .dat file
	set err [catch {set outid [open $::FILES::osc(DAT) w]}]
	if {$err == 1} {
	   ::MSG_DLG::attention "Could not Create OSC Data file \n$::FILES::osc(DAT)"\
                            red \
                            black \
                            -INSTRUCTION "Check file name and directory access privileges"
	   set ::FILES::osc(DAT) ""
	   set ::FILES::osc(PRC) ""
	   set ::FILES::osc(DMP) ""
	   set ::FILES::osc(STRUCT) ""
	   return
	} else {
	   close $outid
	   file delete $::FILES::osc(DAT)  
	}

	#   Check that able to create .prc file
	set err [catch {set outid [open $::FILES::osc(PRC) w]}]
	if {$err == 1} {
	   ::MSG_DLG::attention "Could not Create OSC Proc file \n$::FILES::osc(PRC)"\
                            red \
                            black \
                            -INSTRUCTION "Check file name and directory access privileges"
	   set ::FILES::osc(DAT) ""
	   set ::FILES::osc(PRC) ""
	   set ::FILES::osc(DMP) ""
	   set ::FILES::osc(STRUCT) ""
	   return
	} else {
	   close $outid
	   file delete $::FILES::osc(PRC) 
	}

	#   Check that able to create data .mem file
	set err [catch {set outid [open $::FILES::osc(DMP) w]}]
	if {$err == 1} {
	   ::MSG_DLG::attention "Could not Create OSC Data dump comparison file \n$::FILES::osc(DMP)"\
                            red \
                            black \
                            -INSTRUCTION "Check file name and directory access privileges"
	   set ::FILES::osc(DAT) ""
	   set ::FILES::osc(PRC) ""
	   set ::FILES::osc(DMP) ""
	   set ::FILES::osc(STRUCT) ""
	   return
	} else {
	   close $outid
	   file delete $::FILES::osc(DMP) 
	}

	#   Check that able to create structure .mem file, if doing a full upload
	if {$decl_ok == 0} {
	   set err [catch {set outid [open $::FILES::osc(STRUCT) w]}]
	   if {$err == 1} {
		::MSG_DLG::attention "Could not Create OSC Structure dump comparison file \n$::FILES::osc(STRUCT)"\
                               red \
                               black \
                               -INSTRUCTION "Check file name and directory access privileges"
		set ::FILES::osc(DAT) ""
		set ::FILES::osc(PRC) ""
		set ::FILES::osc(DMP) ""
		set ::FILES::osc(STRUCT) ""
		return
	   } else {
		close $outid
		file delete $::FILES::osc(STRUCT) 
	   }
	}

	#	output the giant STOL procedure.
	UpLoadOSC $template_file \
		  $::FILES::osc(BASE) \
		  $::FILES::osc(ROOT) \
		  [file rootname $::FILES::osc(STRUCT)] \
		  $::FILES::ssc \
		  $::FILES::gssini \
		  $::CONFIG::data(asc_time) \
		  $::MAIN::prog_rev \
		  $decl_min \
		  $decl_max \
		  $width \
		  $mst_lrvs

	#	Log it
	if {$full == "NO"} {
	    set msg_txt "GENERATE AN OSC SINGLE STRIP SCRIPT BUTTON PRESSED \n\n"
	    
	} else {
	    set msg_txt "GENERATE A FULL OSC SCRIPT BUTTON PRESSED \n\n"
	    
	}
	
	set msg_txt "$msg_txt\OSC Upload PROC Generated:       $::FILES::osc(PRC)\n"
	set msg_txt "$msg_txt\OSC GENLOAD Data Generated:      $::FILES::osc(DAT)\n"
	set msg_txt "$msg_txt\OSC Data Compare Generated:      $::FILES::osc(DMP)\n"
	if {$full == "YES"} {
	    set msg_txt "$msg_txt\OSC Structure Compare Generated: $::FILES::osc(STRUCT)\n"
	}
	set msg_txt "$msg_txt\OSC Tabular File Generated:      $::FILES::osc(TXT)\n\n"
	
	set msg_txt "$msg_txt\INI File used:     $::FILES::gssini\n"
	set msg_txt "$msg_txt\SSC Catalog used:  $::FILES::ssc\n"
	set msg_txt "$msg_txt\ASC Epoch used:    $::CONFIG::data(asc_time)\n"
	set msg_txt "$msg_txt\OSC Template used: $template_file\n\n"
	
	set msg_txt "$msg_txt\Minimum Declination:     $decl_min Deg\n"
	set msg_txt "$msg_txt\Maximum Declination:     $decl_max Deg\n"
	set msg_txt "$msg_txt\Declination Strip Width: $width Deg\n"
	
	::MSG_DLG::msg_log $msg_txt

}


########################################################################
#	Output ASC

proc ::OSC_UI::output_asc { } {
	global gi

	#       get ASC filename
	::FILES::set_asc

	#	make sure that file can be created
	set err [catch {set outid [open $::FILES::asc_dump w]}]
	if {$err == 1} {
	   ::MSG_DLG::attention "Could not Create ASC Text file \n$::FILES::asc_dump" \
                             red \
                             black \
                             -INSTRUCTION "Check file name and directory access privileges"
	   set ::FILES::asc_dump {}
 	   return
	} else {
	   close $outid
	}
	
	#	simple ASCII output
	dump_asc $::FILES::asc_dump \
	         $::FILES::ssc \
	         $::MAIN::prog_rev \
	         $::CONFIG::data(asc_time)
	
	#	Log it
	set msg_txt "GENERATE A HUMAN-READABLE TABULAR ASC BUTTON PRESSED \n\n"
	    
	set msg_txt "$msg_txt\ASC Tabular File Generated:      $::FILES::asc_dump\n\n"
	set msg_txt "$msg_txt\INI File used:     $::FILES::gssini\n"
	set msg_txt "$msg_txt\SSC Catalog used:  $::FILES::ssc\n"
	set msg_txt "$msg_txt\ASC Epoch used:    $::CONFIG::data(asc_time)\n"

	::MSG_DLG::msg_log $msg_txt

}

########################################################################
#	$Log: $
##########################################################################
