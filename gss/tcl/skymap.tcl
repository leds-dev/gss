########################################################################
#	$Id: $
########################################################################

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval SKYMAP {
	variable frame
	variable display
	variable user
	variable sky_ui
	variable sky_ui_tag
	variable sky_canv
	variable sky_mask
	variable sky_pxpdeg
	variable sky_zero
	variable sky_maxx
	variable sky_maxy
	variable sky_zf
	variable sky_prev_zf
	variable sky_grid_incr
	variable sky_canv_ra
	variable sky_canv_dec

	variable bg black

	variable ann_rad

	variable axis_id
	
	variable bssid_to_star

	namespace export init plot ps user_gui
	}

########################################################################

proc ::SKYMAP::init { {pf {}} } {
	global gi
	
	variable frame
	variable display
	variable user
	variable bg
	variable res
	variable sky_ui
	variable sky_ui_tag
	variable sky_canv
	variable sky_mask
	variable sky_pxpdeg
	variable sky_zero
	variable sky_maxx
	variable sky_maxy
	variable sky_zf
	variable sky_prev_zf
	variable sky_grid_incr

	variable pix_ratio
	variable ann_rad
	variable axis_id

	variable sky_canv_ra
	variable sky_canv_dec
	variable disp_center
	
	set st_list(12) {1 2}
	set st_list(23) {2 3}
	set st_list(13) {1 3}
	set st_list(123) {1 2 3}
	
	#	Set S/C body axis id's
	set axis_id(x) 0
	set axis_id(y) 1
	set axis_id(z) 2

	#	get display limits from GSS
	#	  - this controls the size of the X-window display.
	set sky_maxx [expr $::CONFIG::display(disp_x)*3.0]
	set sky_maxy $::CONFIG::display(disp_y)

        set disp_center(x) [expr ($sky_maxx / 2.0) + 1.29]
        set disp_center(y) [expr ($sky_maxy / 2.0) + 1.29]
        set sky_canv_ra 180.0
        set sky_canv_dec 0.0
	
	#	calculate pixels / degree. Use display x dimension so that
	#	views appear square.
	set sky_pxpdeg(x) [expr $sky_maxx / 360.0]
	set sky_pxpdeg(y) [expr $sky_maxy / 180.0]
	
	#	Calculate zero points
	set sky_zero(x) $sky_maxx
	set sky_zero(y) [expr $sky_pxpdeg(y) * 90.0]
	
	set sky_zf 1
	set sky_prev_zf 1
	
	set pix_ratio [expr $sky_maxy / 180.0]

	#	Initialize the skymap grid min/max/interval for different
	#	zoom factors
	set sky_grid_incr(ra,1) "30 330 30"
	set sky_grid_incr(ra,2) "15 345 15"
	set sky_grid_incr(ra,5) "5 355 5"
	set sky_grid_incr(ra,10) "2 358 2"
	set sky_grid_incr(ra,20) "1 359 1"
	set sky_grid_incr(dec,1) "-60 60 30"
	set sky_grid_incr(dec,2) "-75 75 15"
	set sky_grid_incr(dec,5) "-85 85 5"
	set sky_grid_incr(dec,10) "-88 88 2"
	set sky_grid_incr(dec,20) "-89 89 1"
	
	#	Initailize celestial object hidden visibility
	foreach obj {SUN EARTH MOON MERCURY VENUS MARS JUPITER \
	             SATURN URANUS NEPTUNE PLUTO } {
            set sky_ui($obj) normal
            set sky_ui_tag($obj) $obj

	}

	foreach obj {COMET_1 COMET_2} {
	    set sky_ui($obj) hidden
	    set sky_ui_tag($obj) $obj
	    if {[lindex [$gi $obj {}] 10] == 1} {
	        set sky_ui($obj) normal
	    
	    }
	    
	}
	
	foreach obj {ASTEROID_1 ASTEROID_2 ASTEROID_3 ASTEROID_4} {
	    set sky_ui($obj) hidden
	    set sky_ui_tag($obj) $obj
	    if {[lindex [$gi $obj {}] 13] == 1} {
	        set sky_ui($obj) normal
	    
	    }
	    
	}
	             

	#	Initialize grid visibility        
	set sky_ui(sky_grid) normal
	set sky_ui_tag(sky_grid) "sky_grid"

	foreach tag {bands cda_tm_at_cda dsn_tm_at_cda dsn_tm_at_dsn \
	             cmd_at_cda cmd_at_dsn instrument_cooler \
	             instrument_optical sun_sensor \
	             sky_x sky_y sky_z} {
	             
	    set sky_ui($tag) disabled
	    set sky_ui_tag($tag) $tag
	    
	}

	set sky_ui(sky_x) normal
	set sky_ui(sky_y) normal
	set sky_ui(sky_z) normal
	
	set sky_ui(stars) normal
	set sky_ui_tag(stars) stars
	
	set sky_ui(st) normal
	set sky_ui(osc) ALL
	set sky_ui_tag(osc) osc
	
	#	Set magnitude filter level
	set sky_ui(prev_mag) 8.0
	set sky_ui(mag) 8.0
	set sky_ui(temp_mag) 8.0
	set sky_ui_tag(mag) "mag"
	
	
	#	Enable per power on combination
	set sky_ui(st_1) disabled
	set sky_ui(st_2) disabled
	set sky_ui(st_3) disabled
	
	foreach st $st_list($::CONFIG::data(sts_active)) {
	    set sky_ui(st_$st) normal
	    
	}
	set sky_ui_tag(st) st
	
	set sky_ui(st_mode) square
	set sky_ui_tag(st_mode) st_mode

	#	DISPLAY
	set skymap [frame $pf.skymap]
	pack $skymap -side left -fill both -expand yes

	set display [canvas $skymap.c \
	                    -bg $bg \
		            -width $sky_maxx  \
		            -height $sky_maxy \
                            -scrollregion "0 0 $sky_maxx $sky_maxy" \
                            -xscrollcommand "$skymap.hscroll set" \
                            -yscrollcommand "$skymap.vscroll set"]
                            
	set sky_canv $display
	
	#	SCALING/ZOOM CONTROL
	frame $skymap.pos -width $sky_maxx
	scrollbar $skymap.hscroll \
	          -orient horiz \
                  -width 8 \
                  -command "::SKYMAP::_set_sky_canv_hpos"
	scrollbar $skymap.vscroll -command "::SKYMAP::_set_sky_canv_vpos" \
                                  -width 8

	label $skymap.pos.ra \
	      -textvariable ::SKYMAP::sky_canv_ra \
	      -font $::CONFIG::fonts(entry)
	label $skymap.pos.dec \
	      -textvariable ::SKYMAP::sky_canv_dec \
	      -font $::CONFIG::fonts(entry)

	pack $skymap.pos.ra $skymap.pos.dec \
	                        -side left \
	                        -padx 5

	pack $skymap.hscroll -side bottom \
	                     -fill x
	pack $skymap.vscroll -side right \
	                     -fill y
	
	pack $display -side top -anchor nw

	#	Now create a set of reference planes so that we can
	#	manage the stacking order (for visibility)
	#	BORDER / BACKGROUND
	set ::SKYMAP::sky_mask [$display create rectangle \
	                                        0 \
	                                        0 \
	                                        $sky_maxx \
	                                        $sky_maxy \
		                                -outline white \
		                                -fill $bg \
		                                -tags "sky_mask"]

	$display create line 0 0 1 0 -fill white -tags "sky_stayout_plane"
	$display create line 0 0 1 0 -fill white -tags "sky_star_plane"
	$display create line 0 0 1 0 -fill white -tags "sky_ext_plane"
	$display create line 0 0 1 0 -fill white -tags "sky_body_plane"
	$display create line 0 0 1 0 -fill white -tags "sky_FOV_plane"
	
	#	Display the Skymap FOV
	::DISPLAY::sky_fov


}

########################################################################
#	Update skymap objects
proc ::SKYMAP::sky_update { } {
	global gi

	#	Now draw in the objects
	foreach obj $::EXT_OBJS::ext_obj_list {
	    ::SKYMAP::sky_celestial_object $obj
	    
	}


	#	Draw in the star Trackers FOV's
	foreach st {1 2 3} {
	
	#	Remove the old one
	    $::SKYMAP::sky_canv delete "st_$st"

	#	Does user want any of them?
	    if { $::SKYMAP::sky_ui(st) == "normal" } {
	        ::SKYMAP::sky_st_FOV $st

	    }

	}	    
		
	#	Now draw in the S/C body objects
	foreach obj $::BODY_OBJS::body_obj_list {

	#	Delete the old one
	    $::SKYMAP::sky_canv delete $obj

	#	Check for a valid quaternion
	    if {$::USER_QUAT::gss_qsel_state == "VALID"} {
	        ::SKYMAP::sky_body_obj $obj
	    
	    }

	}
	
	#	Now do the Imager / Sounder optical FOV
	#	First delete the old one
	    $::SKYMAP::sky_canv delete inst_opt

	#	Check for a valid quaternion
	if {$::USER_QUAT::gss_qsel_state == "VALID"} {
	    ::SKYMAP::sky_inst_optical 
	    
	}
	
	#	Now do X, Y, and Z axis
	#	First delete the old one
	foreach axis {x y z} {
	    $::SKYMAP::sky_canv delete sky_$axis
	    
	#	Check for a valid quaternion
	    if {$::USER_QUAT::gss_qsel_state == "VALID"} {
	        ::SKYMAP::sky_axis $axis
	    
	    }
	    
	}
		
	::update idletasks
	
}	    



########################################################################
#	draw celestial objects

proc ::SKYMAP::sky_celestial_object { obj } {
	set uc [string toupper $obj]
	
	# 	first get rid of old object
	$::SKYMAP::sky_canv delete $obj

	#	if earth or moon we need a valid ephemeris
	if { ( ($obj == "earth") || \
	       ($obj == "moon" )   ) && \
	     ($::CONFIG::data(valid_ephem) == "INVALID") } {
	         
	    return
	         
	}
	           
	#	Does user want it displayed?
	if {$::SKYMAP::sky_ui($uc) == "normal"} {
	    
	#	Does the object have a larger stayout zone?
	    if {$::EXT_OBJS::ext_obj($obj,radius) !=\
	        $::EXT_OBJS::ext_obj(stayout_$obj,radius)} {
	            
	#	Get RA/DEC for display object's stayout zone
	        set coords [Sky_Object \
	                        $::EXT_OBJS::ext_obj($obj,id) \
	                        $::EXT_OBJS::ext_obj(stayout_$obj,radius)]

	        if {[string first "Error:" $coords] == -1} {
	            ::DISPLAY::sky_object \
	                      "$obj" \
	                      $coords \
	                      "stayout" \
	                      $::EXT_OBJS::ext_obj(stayout_$obj,color)
	                 
	        } else {
	            ::MSG_DLG::attention "($obj stayout) $coords" \
	                                  red \
	                                  black \
	                                  -INSTRUCTION "Error from Sky_Object"
	                
	        }
	                 
	    }

	            
	#	Get RA/DEC for display object
	    set coords [Sky_Object \
	                    $::EXT_OBJS::ext_obj($obj,id) \
	                    $::EXT_OBJS::ext_obj($obj,radius) ]

	    if {[string first "Error:" $coords] == -1} {
	        ::DISPLAY::sky_object \
	                   "$obj" \
	                   $coords \
	                   "object" \
	                   $::EXT_OBJS::ext_obj($obj,color)
	                 
	    } else {
	        ::MSG_DLG::attention "($obj stayout) $coords" \
	                              red \
	                              black \
	                              -INSTRUCTION "Error from Sky_Object"
	                
	    }
	                   
	}
	    

}


########################################################################
#	draw ST FOVs

proc ::SKYMAP::sky_st_FOV { st } {
	global gi
	
	#	Check each individual one for display
	if {$::SKYMAP::sky_ui(st_$st) == "normal"} {
	    
	    if { ($::SKYMAP::sky_ui(st_mode) == "square" )  && \
	         ($::USER_QUAT::gss_qsel_state == "VALID")     } {
	        set coords [Sky_ST $st]

	        if {[string first "Error:" $coords] == -1} {
	            ::DISPLAY::sky_st_square \
	                       $st \
	                       $coords \
	                       [$gi ST_H_AXIS_COLOR grey60] \
	                       [$gi [format "ST_%d_FOV_COLOR" $st] \
	                             grey60]
	                 
	        } else {
	            ::MSG_DLG::attention "$coords" \
	                                  red \
	                                  black \
	                                  -INSTRUCTION "Error from Sky_ST\
	                                  for Tracker $st"
	                
	        }
	        
	    } elseif { ($::SKYMAP::sky_ui(st_mode) == "circle" )  && \
	               ($::USER_QUAT::gss_qsel_state == "VALID")     } {
	        set radius [expr $::CIS_GUI::rough_err + \
	                         (4.0 * sqrt(2))]
	        set coords [Sky_RoughBoresight $st $radius]

	        if {[string first "Error:" $coords] == -1} {
	            ::DISPLAY::sky_st_circle \
	                       $st \
	                       $coords \
	                       [$gi [format "ST_%d_FOV_COLOR" $st] \
	                             grey60]
	                 
	        } else {
	            ::MSG_DLG::attention "$coords" \
	                                  red \
	                                  black \
	                                  -INSTRUCTION "Error from Sky_ST\
	                                  for Tracker $st"
	                
	        }
	        
	    } elseif {$::SKYMAP::sky_ui(st_mode) == "annulus"} {
	        set coords [Sky_Annulus $st]

	        if {[string first "Error:" $coords] == -1} {
	            ::DISPLAY::sky_st_annulus \
	                       $st \
	                       $coords \
	                       [$gi [format "ST_%d_FOV_COLOR" $st] \
	                            grey60]
	                                          
	        } else {
	            ::MSG_DLG::attention "$coords" \
	                                  red \
	                                  black \
	                                  -INSTRUCTION "Error from Sky_ST\
	                                  for Tracker $st"
	                
	        }
	        
   	    }
   	            
   	}

}	    


########################################################################
#	draw S/C body objects

proc ::SKYMAP::sky_body_obj { obj } {
	    
	#	Except for the Instrument Cooler and sun sensor, 
	#	display controls are at the root level (cmd_fwd -> cmd)
	#	so create a key without the "_fwd" or "_aft"
	
	if {([string first "instrument" $obj] == -1) && \
	    ([string first "sun" $obj] == -1)            } {
	        
	    set idx [expr [string last "_" $obj] - 1]
	    set root [string range $obj 0 $idx]
	        
	} else {
	    set root $obj
	        
	}
	    	    
	#	Does user want any of it displayed?
	if {$::SKYMAP::sky_ui($root) == "normal"} {

	#	    Do they want this particular one displayed?
	    if {$::SKYMAP::sky_ui($obj) == "normal"} {	    
	#	    Get RA/DEC for forward display object
	        set coords [Sky_Circular_Zone \
	                    $::BODY_OBJS::body_obj($obj,id) ]

	        if {[string first "Error:" $coords] == -1} {
	            ::DISPLAY::sky_body \
	                       $obj \
	                       $coords \
	                       $::BODY_OBJS::body_obj($obj,color)
	                       
	        } else {
	            ::MSG_DLG::attention "$coords" \
	                                  red \
	                                  black \
	                                  -INSTRUCTION "Error from \
	                                  Sky_Circular_Zone for object $obj"
	                
	        }
	            
	    }
	                   
	}
	    
}


########################################################################
#	draw Instrument Optical FOV

proc ::SKYMAP::sky_inst_optical { } {
	global gi
	
	#	Does user want it displayed?
	if {$::SKYMAP::sky_ui(instrument_optical) == "normal"} {
	    
	#	Get RA/DEC for forward display object
	        set coords [Sky_InstOptFOV]

	    if {[string first "Error:" $coords] == -1} {
	        ::DISPLAY::sky_instoptFOV $coords \
	                                  [$gi \
	                                   INSTRUMENT_OPTICAL_COLOR \
	                                   PeachPuff1]
	                 
	    } else {
	        ::MSG_DLG::attention "$coords" \
	                              red \
	                              black \
	                              -INSTRUCTION "Error from \
	                              Sky_InstOptFOV for Imager/Sounder \
	                              Optical FOV"
	                
	    }
	                   
	}

}


########################################################################
#	draw S/C X, Y, or Z axis

proc ::SKYMAP::sky_axis { axis } {
	global gi
		
	#	Does user want it displayed?
	if {$::SKYMAP::sky_ui(sky_$axis) == "normal"} {
	    set uc [string toupper $axis]
	        
	#	Get color
	    set color [$gi BODY_$uc\_AXIS_PROJ_COLOR red]
	        	    
	#	Get RA/DEC for axis
	    set coords [Sky_Body_Axis $::SKYMAP::axis_id($axis)]

	    if {[string first "Error:" $coords] == -1} {
	        ::DISPLAY::sky_body_axis $axis \
	                                 [lindex $coords 0] \
	                                 [lindex $coords 1] \
	                                 $color
	            
	                 
	    } else {
	        ::MSG_DLG::attention "$coords" \
	                              red \
	                              black \
	                              -INSTRUCTION "Error from \
	                              Sky_Body_Axis for axis $axis"
	                
	    }
	                   
	}

}


########################################################################
#	draw stars

proc ::SKYMAP::sky_stars { } {
	variable bssid_to_star
	global asc_len

	#	The LookupStarInfo function provides star information 
	#	for the id (ACS index) passed.  The information is 
	#	listed in the order as:
	#
	#	Right Ascention (deg)
	#	Declination (deg)
	#	Magnitude for Skymap
	#	Magnitude for ST1
	#	Magnitude for ST2
	#	Magnitude for ST3
	#	Sky 2000 ID
	#	Boolean Flag specifying whether it is an OSC
	#	BSS ID
 
	set asc_len [GetASC_Size]
	
	for {set star 0} {$star < $asc_len} {incr star} {
	    set data [LookupStarInfo $star]
	    set ra [lindex $data 0]
	    set dec [lindex $data 1]
	    set mag [lindex $data 2]
	    set sky2000id [lindex $data 6]
	    set osc [lindex $data 7]
	    set bssid [lindex $data 8]
	    
	    set bssid_to_star($bssid) $star 

	#	Decide whether the user wants them filtered out
	    set state "hidden"
	    if { $mag < $::SKYMAP::sky_ui(mag) } {
	    
	        if { ( ($osc == 1) && ($::SKYMAP::sky_ui(osc) == "OSC") ) ||\
	             ( $::SKYMAP::sky_ui(osc) == "ALL" ) } {

	            set state "normal"

	       }
	       
	    }
	    
	    ::DISPLAY::sky_star $star \
	                        $ra \
	                        $dec \
	                        $mag \
	                        $sky2000id \
	                        $osc \
	                        $bssid \
	                        $state
	    
	    if {[expr int(($star / 500.0)) * 500] == $star} {
	        ::MSG_DLG::progress "."
	        
	        if {$::CONFIG::data(tm_src) != "Planning"} {
	            set err [gss_gtacs_read_tm]
	            if {$err != 0} {
                        ::MSG_DLG::attention "Error for stream $::CONFIG::stream from gss_gtacs_read_tm $err" \
                                 red \
                                 black \
                                 -LABEL $err
                    }
	            ::update 
	        
	        }
	        
	    }
	    
	}
	
}


########################################################################
#	print skymap

proc ::SKYMAP::ps { } {
	global env
	
	set printer $env(GSS_PRINT_CMD)

        set directory [::FILES::get_env GSS_PS_FILE_DIR ./]/[string tolower $::FILES::pf]/gss
	set dt [clock format [clock seconds] -format "j07/29/20M%S" -gmt yes]
	set filename "GSS_SKY_$dt.ps"
        set psfile $directory/$filename

	set err [catch {set outid [open $psfile w]}]
	if {$err == 1} {
	   ::MSG_DLG::attention "Could not Create SkyMap Postcript File \n$psfile" \
			red \
			black \
			-INSTRUCTION "Check file name and directory access privileges"
	   return
	} else {
	   close $outid
	   file delete $psfile
	}

	$::SKYMAP::sky_canv itemconfigure sky_mask -fill gray80

	$::SKYMAP::sky_canv postscript -file "$psfile" \
		                       -rotate yes \
		                       -colormode color \
		                       -pagewidth 10.0i

	$::SKYMAP::sky_canv itemconfigure sky_mask -fill black
	
	set cmd "catch {exec $printer $psfile &}"
	eval "$cmd"
	
	set msg_txt "Generated SkyMap postscript file: \n\
                              \t Directory = $directory \n\
                              \t Filename = $filename \n\n\
                     Printed using command: $printer"
	
        ::MSG_DLG::attention $msg_txt
                              
        ::MSG_DLG::msg_log $msg_txt
	              
}


########################################################################
#	Handle deleting or redrawing the Skymap grid

proc ::SKYMAP::toggle_sky_grid { } {

	if {$::SKYMAP::sky_ui(sky_grid) == "normal"} {
 	    ::DISPLAY::sky_grid
 		     		                                 
	} else {
 	    $::SKYMAP::sky_canv delete sky_grid 
	
	}
	
}

########################################################################
#	Handle deleting or redrawing the Skymap normal mode bands

proc ::SKYMAP::toggle_bands { } {

	if {$::SKYMAP::sky_ui(bands) == "normal"} {
 	    ::DISPLAY::sky_bands
 		     		                                 
	} else {
 	    $::SKYMAP::sky_canv delete sky_bands 
	
	}
	
}


########################################################################
#	Handle deleting or redrawing the stars

proc ::SKYMAP::toggle_all_stars { } {

	set ::SKYMAP::sky_ui(temp_mag) $::SKYMAP::sky_ui(mag)

	if {$::SKYMAP::sky_ui(stars) == "normal"} {
	
 	    $::SKYMAP::sky_canv lower "star" "sky_star_plane"
 	    
 	    ::SKYMAP::toggle_nonosc_stars
 	    
 	    set ::SKYMAP::sky_ui(prev_mag) 8.0
	    ::SKYMAP::toggle_star_mag
 		     		                                 
	} else {
	
 	    $::SKYMAP::sky_canv lower "star" "sky_mask"
	
	}
	
}


########################################################################
#	Handle deleting or redrawing the osc stars

proc ::SKYMAP::toggle_nonosc_stars { } {

	set ::SKYMAP::sky_ui(temp_mag) $::SKYMAP::sky_ui(mag)

	if {$::SKYMAP::sky_ui(osc) == "OSC"} {
	
 	    $::SKYMAP::sky_canv lower "not_osc" "sky_mask"
 		     		                                 
	} else {
	
 	    $::SKYMAP::sky_canv lower "not_osc" "sky_star_plane"
 	    
 	    set ::SKYMAP::sky_ui(prev_mag) 8.0
	    ::SKYMAP::toggle_star_mag
	
	}
	
}


########################################################################
#	Handle displaying / hiding stars by magnitude

proc ::SKYMAP::toggle_star_mag { } {

	if {[catch {expr $::SKYMAP::sky_ui(temp_mag) + 0}] == 1} {
	    ::MSG_DLG::attention "Invalid Entry" \
	                          red \
	                          black \
	                          -INSTRUCTION \
	                          "Re-enter the Star Filter Magnitude" 
	    set ::SKYMAP::sky_ui(temp_mag) $::SKYMAP::sky_ui(mag)
	    return
	}

	if { $::SKYMAP::sky_ui(temp_mag) > 8.0 } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                          red \
	                          black \
	                          -INSTRUCTION \
	                          "Star Filter Magnitude must be <= 8.0" 
	    set ::SKYMAP::sky_ui(temp_mag) $::SKYMAP::sky_ui(mag)
	    return
	}
	
	if { $::SKYMAP::sky_ui(temp_mag) < -1.0 } {
	    ::MSG_DLG::attention "Invalid Entry" \
	                          red \
	                          black \
	                          -INSTRUCTION \
	                          "Star Filter Magnitude must be >= -1.0" 
	    set ::SKYMAP::sky_ui(temp_mag) $::SKYMAP::sky_ui(mag)
	    return
	}
	
	set ::SKYMAP::sky_ui(mag) $::SKYMAP::sky_ui(temp_mag)

	if {$::SKYMAP::sky_ui(mag) == $::SKYMAP::sky_ui(prev_mag)} {
	    return
	    
	} elseif {$::SKYMAP::sky_ui(mag) < $::SKYMAP::sky_ui(prev_mag)} {
	    set start $::SKYMAP::sky_ui(mag)
	    set end $::SKYMAP::sky_ui(prev_mag)
	    set location "sky_mask"
	    
	} else {
	    set start $::SKYMAP::sky_ui(prev_mag)
	    set end $::SKYMAP::sky_ui(mag)
	    set location "sky_star_plane"
	    
	}

	#	Since a "for" loop won't work with floating point increments
	#	build a list of the magnitudes to deal with
	
	set count [expr ($end - $start)	/ 0.1]
	set x $start
	set mags ""
	for {set i 1} {$i <= $count} {incr i} {
	    set mags "$mags mag_[format "%3.1f" $x]"
	    set x [expr $x + 0.1]
	    
	}
	
	foreach mag $mags {
	    $::SKYMAP::sky_canv lower $mag "$location"

	
	}

	#	Save the magnitude for next time
	set ::SKYMAP::sky_ui(prev_mag) $::SKYMAP::sky_ui(mag)
	
}

########################################################################
#	Handle changed in Skymap scaling (zoom)

proc ::SKYMAP::scale_sky {} {

	if {$::SKYMAP::sky_prev_zf == $::SKYMAP::sky_zf} {return}
	
	#	Delete star/object labels, to be redrawn later.
	for {set i 1} { $i <= $::DISPLAY::number_of_displays } {incr i } {
	    set obj $::DISPLAY::list_display_id($i)
            $::SKYMAP::sky_canv delete sky_label_$obj
	}
	
	set xscale [expr (1.0 / $::SKYMAP::sky_prev_zf) * $::SKYMAP::sky_zf]
	set yscale $xscale
	
	set ::SKYMAP::sky_maxx [expr $::SKYMAP::sky_maxx * $xscale]
	set ::SKYMAP::sky_maxy [expr $::SKYMAP::sky_maxy * $yscale]	
	
	#	Record the relative location of the visible window region
	set xrelpos [$::SKYMAP::sky_canv xview]
	set yrelpos [$::SKYMAP::sky_canv yview]
	set xlpos [lindex $xrelpos 0]
	set xrpos [lindex $xrelpos 1]
	set xcpos [expr 0.5*($xlpos + $xrpos)]
	set ytpos [lindex $yrelpos 0]
	set ybpos [lindex $yrelpos 1]
	set ycpos [expr 0.5*($ytpos + $ybpos)]
	
	#	Scale the canvas to pick up whatever we can
	$::SKYMAP::sky_canv scale all 0 0 $xscale $yscale
	
	#	Change the scroll region to match the new size
	$::SKYMAP::sky_canv configure \
	                    -scrollregion "0\
	                                   0\
	                                   $::SKYMAP::sky_maxx\
	                                   $::SKYMAP::sky_maxy"
	                                  
	#	Locate the visible portion of the rescaled window so center does not move
	set fraction [expr 1.0/$::SKYMAP::sky_zf]
	set newxlpos [expr ($xcpos - (0.5*$fraction)) ]
	set newxrpos [expr ($xcpos + (0.5*$fraction)) ]
	set newytpos [expr ($ycpos - (0.5*$fraction)) ]
	set newybpos [expr ($ycpos + (0.5*$fraction)) ]
	if { $newxlpos < 0.0 } { set newxlpos 0.0 }
	if { $newxrpos > 1.0 } { set newxlpos [expr (1.0 - $fraction)] }
	if { $newytpos < 0.0 } { set newytpos 0.0 }
	if { $newybpos > 1.0 } { set newytpos [expr (1.0 - $fraction)] }
	$::SKYMAP::sky_canv xview moveto $newxlpos
	$::SKYMAP::sky_canv yview moveto $newytpos	
	                                               
	#	Change the pix/deg so new drawings are sized correctly	                                   
	set ::SKYMAP::sky_pxpdeg(x) [expr $::SKYMAP::sky_pxpdeg(x) * $xscale]
	set ::SKYMAP::sky_pxpdeg(y) [expr $::SKYMAP::sky_pxpdeg(y) * $yscale]
	set ::SKYMAP::sky_zero(x)   [expr $::SKYMAP::sky_zero(x)   * $xscale]
	set ::SKYMAP::sky_zero(y)   [expr $::SKYMAP::sky_zero(y)   * $yscale]

	#	Redraw star labels.  Object labels redraw automatically.
	for {set i 1} { $i <= $::DISPLAY::number_of_displays } {incr i } {
	    set bssid $::DISPLAY::list_display_id($i)
	    if {[catch {expr $bssid + 0}] != 1} {
		set star $::SKYMAP::bssid_to_star($bssid)
		set data [LookupStarInfo $star]
		set ra   [lindex $data 0]
		set dec  [lindex $data 1]
		set mag [lindex $data 2]
		set osc [lindex $data 7]
		::DISPLAY::sky_star_label $ra $dec $mag $osc $bssid
	    }
	}

	#	Save the zoom factor for next time
	set ::SKYMAP::sky_prev_zf $::SKYMAP::sky_zf

	#	Display the new ra center position	
        set horz [$::SKYMAP::sky_canv canvasx $::SKYMAP::disp_center(x)]

        set ::SKYMAP::sky_canv_ra \
            [expr 360.0 - ($horz / $::SKYMAP::sky_pxpdeg(x))]

	#	Display the new dec center position
        set vert [$::SKYMAP::sky_canv canvasy $::SKYMAP::disp_center(y)]
        set txt_vert [$::SKYMAP::sky_canv canvasy 8]

        set ::SKYMAP::sky_canv_dec \
            [expr 90.0 - ($vert / $::SKYMAP::sky_pxpdeg(y))]

	#	Because of rounding errors et al, a scale factor of 1
	#	doesn't display 180.0 0.0.  So just force it.
	if {$::SKYMAP::sky_zf == 1} {
	    set ::SKYMAP::sky_canv_ra  180.0
	    set ::SKYMAP::sky_canv_dec   0.0
	    
	}
	
        #	Update the display
        ::DISPLAY::sky_center $horz $txt_vert
		
}

########################################################################
#	Display the skymap ra center coordinates when changed 
#       by the scrollbar
	
proc ::SKYMAP::_set_sky_canv_hpos { args } {
    set cmd "$::SKYMAP::sky_canv xview $args"
    eval $cmd

    set horz [$::SKYMAP::sky_canv canvasx $::SKYMAP::disp_center(x)]
    set txt_vert [$::SKYMAP::sky_canv canvasy 8]

    set ::SKYMAP::sky_canv_ra \
        [expr 360.0 - ($horz / $::SKYMAP::sky_pxpdeg(x))]

    ::DISPLAY::sky_center $horz $txt_vert
    
}

########################################################################
#	Display the skymap dec center coordinates when changed 
#       by the scrollbar
	
proc ::SKYMAP::_set_sky_canv_vpos { args } {
    set cmd "$::SKYMAP::sky_canv yview $args"
    eval $cmd

    set horz [$::SKYMAP::sky_canv canvasx $::SKYMAP::disp_center(x)]
    set vert [$::SKYMAP::sky_canv canvasy $::SKYMAP::disp_center(y)]

    set txt_vert [$::SKYMAP::sky_canv canvasy 8]

    set ::SKYMAP::sky_canv_dec \
        [expr 90.0 - ($vert / $::SKYMAP::sky_pxpdeg(y))]

    ::DISPLAY::sky_center $horz $txt_vert
        
}


########################################################################

proc ::SKYMAP::user_gui { } {
	global gi
	variable orientation_list { y z } 
	variable sky_ui
	variable sky_ui_tag

	set sky_ui(centroid) 1
	set sky_ui_tag(centroid) "Centroid"

	set sky_ui(celestial_ns) 1
	set sky_ui_tag(celestial_ns) "Celestial N/S"

	set sky_ui(vt) 1
	set sky_ui_tag(vt) "VT"

	set rtd 57.295779513

	#-----------------------------------------------
	#	label at top

	catch {destroy .sky_ui}
	toplevel .sky_ui
	wm title .sky_ui "SKYMAP WINDOW CONTROL"
	wm geometry .sky_ui "-0-0"
	set pf .sky_ui.top
	frame $pf -bg gray50
	pack $pf

	set f [frame $pf.header]

	set f1 [frame $pf.col1 -bg gray50]
	pack $f1 -side left \
		 -anchor nw \
		 -fill x 

	set f2 [frame $pf.col2 -bg gray50]
	pack $f2 -side left \
		 -anchor nw \
	         -fill x 

	set f3 [frame $pf.col3 -bg gray50]
	pack $f3 -side left \
		 -anchor nw \
		 -fill x 

	set btn [frame .sky_ui.butn]
	pack $btn -side bottom \
		  -anchor c \
		  -ipadx 10 \
		  -fill x

	#	Handle the simple cases first
	foreach plnt {Sun Earth Moon Mercury Venus Mars Jupiter Saturn \
	              Uranus Neptune Pluto} {

	    frame $f1.cb$plnt -borderwidth 1 \
	                      -relief ridge
	    pack $f1.cb$plnt -side top \
	                     -anchor w \
	                     -fill x 
	            
	    set uc_plnt [string toupper $plnt]
	    set lc_plnt [string tolower $plnt]
	    
	    set ra    [expr $rtd * [ephem_get_ra $lc_plnt]]
	    set dec   [expr $rtd * [ephem_get_dec $lc_plnt]]
	    set radec [format "RA/DEC %5.1f/%+5.1f" $ra $dec]
	    set color [$gi $uc_plnt\_COLOR grey50]
	    
	    set b [checkbutton $f1.cb$plnt.btn \
	                       -text $plnt \
	                       -anchor w \
	                       -font $::CONFIG::fonts(entry) \
	                       -borderwidth 1 \
	                       -relief ridge \
	                       -bg $color \
	                       -fg black \
 			       -onvalue normal	\
 			       -offvalue hidden \
 			       -variable ::SKYMAP::sky_ui($uc_plnt) \
 			       -width 32 \
	                       -justify right]

	                       
	    set lbl [label $f1.cb$plnt.lbl \
	                   -text "$radec" \
	                   -font $::CONFIG::fonts(entry) \
	                   -anchor w \
	                   -justify left \
	                   -width 18 \
	                   -bg $color \
	                   -fg black]

	                       
	    pack $b -side left \
	            -fill x 

	    pack $lbl -side right \
	              -fill x
	              
	}

	#	Handle the comets and asteroids
	foreach obj {COMET_1 COMET_2 ASTEROID_1 ASTEROID_2 \
	              ASTEROID_3 ASTEROID_4} {
	              
	    frame $f1.cb$obj -borderwidth 1 \
	                     -relief ridge
	    pack $f1.cb$obj -side top \
	                    -fill x 
	                    
	    set lc_obj [string tolower $obj]
	    
	    set ra    [expr $rtd * [ephem_get_ra $lc_obj]]
	    set dec   [expr $rtd * [ephem_get_dec $lc_obj]]
	    set radec [format "RA/DEC %5.1f/%+5.1f" $ra $dec]
	    set color [$gi $obj\_COLOR grey50]

	    set b [checkbutton $f1.cb$obj.btn \
	                       -text [lindex [$gi $obj ?] 0] \
	                       -anchor w \
	                       -font $::CONFIG::fonts(entry) \
	                       -borderwidth 1 \
	                       -relief ridge \
	                       -bg $color \
	                       -fg black \
 			       -onvalue normal	\
 			       -offvalue hidden \
 			       -variable ::SKYMAP::sky_ui($obj) \
 			       -width 32 \
	                       -justify right]

	                       
	    set lbl [label $f1.cb$obj.lbl \
	                   -text "$radec" \
	                   -font $::CONFIG::fonts(entry) \
	                   -anchor w \
	                   -justify left \
	                   -width 18 \
	                   -bg $color \
	                   -fg black]

	    pack $b -side left \
	            -fill x 
	            
	    pack $lbl -side right \
	              -fill x
	                     

	}
	
	#	Handle the individual cases
	set c [checkbutton $f2.sky_grid -text Gridlines \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -font $::CONFIG::fonts(entry) \
	                               -borderwidth 1 \
	                               -bg gray85 \
	                               -fg black \
	                               -relief ridge \
 			               -variable ::SKYMAP::sky_ui(sky_grid)	\
 			               -command "::SKYMAP::toggle_sky_grid"	\
			               -anchor w \
			               -justify right]
			
	pack $c -side top -fill x

	set c [checkbutton $f2.bands   -text "Normal Mode Bands" \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -font $::CONFIG::fonts(entry) \
	                               -borderwidth 1 \
	                               -bg gray85 \
	                               -fg black \
	                               -relief ridge \
 			               -variable ::SKYMAP::sky_ui(bands)	\
 			               -command "::SKYMAP::toggle_bands"	\
			               -anchor w \
			               -justify right]
			
	pack $c -side top -fill x

	set c [frame $f2.spcr1 -borderwidth 1 \
	                       -relief sunken \
	                       -background gray50]
			
	pack $c -side top -fill x -ipady 4

	frame $f2.cdacda -borderwidth 1 \
		      -relief ridge
	pack $f2.cdacda -side top \
		     -anchor w \
		     -fill x
	set c [checkbutton $f2.cdacda.cb  -text "CDA at CDA" \
 			               -onvalue normal	\
 			               -offvalue disabled \
	                               -bg gray85 \
	                               -fg black \
	                               -borderwidth 1 \
	                               -relief ridge \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(cda_tm_at_cda)	\
 			               -anchor w \
 			               -justify right \
		                       -command "$f2.cdacda.cb1 configure \
 			                    -state \$::SKYMAP::sky_ui(cda_tm_at_cda); \
 			                    $f2.cdacda.cb2 configure \
 			                    -state \$::SKYMAP::sky_ui(cda_tm_at_cda)" \
 			                ]
 			               
	set l1 [checkbutton $f2.cdacda.cb1 -text "FWD" \
 			               -onvalue normal	\
 			               -offvalue disabled \
 			               -state $::SKYMAP::sky_ui(cda_tm_at_cda) \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(cda_tm_at_cda_fwd) \
 			               -bg $::BODY_OBJS::body_obj(cda_tm_at_cda_fwd,color) \
 			               -fg black \
 			               -anchor w \
 			               -justify right]

	set l2 [checkbutton $f2.cdacda.cb2 -text "AFT" \
 			               -onvalue normal	\
 			               -offvalue disabled \
 			               -state $::SKYMAP::sky_ui(cda_tm_at_cda) \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(cda_tm_at_cda_aft) \
 			               -bg $::BODY_OBJS::body_obj(cda_tm_at_cda_aft,color) \
 			               -fg black \
 			               -anchor w \
 			               -justify right]


	pack $c -side left -fill x
	pack $l2 $l1 -side right -fill x

	frame $f2.dsncda -borderwidth 1 \
		        -relief ridge
	pack $f2.dsncda -side top \
		       -anchor w \
		       -fill x
	set c [checkbutton $f2.dsncda.cb  -text "DSN at CDA" \
 			                 -onvalue normal \
 			                 -offvalue disabled \
	                                 -bg gray85 \
	                                 -fg black \
	                                 -borderwidth 1 \
	                                 -relief ridge \
	                                 -font $::CONFIG::fonts(entry) \
 			                 -variable ::SKYMAP::sky_ui(dsn_tm_at_cda) \
 			                 -anchor w \
 			                 -justify right \
		                         -command "$f2.dsncda.cb1 configure \
 			                    -state \$::SKYMAP::sky_ui(dsn_tm_at_cda); \
 			                    $f2.dsncda.cb2 configure \
 			                    -state \$::SKYMAP::sky_ui(dsn_tm_at_cda)" \
 			                ]
 			               
	set l1 [checkbutton $f2.dsncda.cb1 -text "FWD" \
 			               -onvalue normal	\
 			               -offvalue disabled \
 			               -state $::SKYMAP::sky_ui(dsn_tm_at_cda) \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(dsn_tm_at_cda_fwd) \
 			               -bg $::BODY_OBJS::body_obj(dsn_tm_at_cda_fwd,color) \
 			               -fg black \
 			               -anchor w \
 			               -justify right]

	set l2 [checkbutton $f2.dsncda.cb2 -text "AFT" \
 			               -onvalue normal	\
 			               -offvalue disabled \
 			               -state $::SKYMAP::sky_ui(dsn_tm_at_cda) \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(dsn_tm_at_cda_aft) \
 			               -bg $::BODY_OBJS::body_obj(dsn_tm_at_cda_aft,color) \
 			               -fg black \
 			               -anchor w \
 			               -justify right]


	pack $c  -side left -fill x
	pack $l2 $l1 -side right -fill x
	
	frame $f2.cmdcda -borderwidth 1 \
		        -relief ridge
	pack $f2.cmdcda -side top \
		       -anchor w \
		       -fill x
	set c [checkbutton $f2.cmdcda.cb  -text "CMD at CDA" \
 			                 -onvalue normal \
 			                 -offvalue disabled \
	                                 -bg gray85 \
	                                 -fg black \
	                                 -borderwidth 1 \
	                                 -relief ridge \
	                                 -font $::CONFIG::fonts(entry) \
 			                 -variable ::SKYMAP::sky_ui(cmd_at_cda) \
 			                 -anchor w \
 			                 -justify right \
		                         -command "$f2.cmdcda.cb1 configure \
 			                    -state \$::SKYMAP::sky_ui(cmd_at_cda); \
 			                    $f2.cmdcda.cb2 configure \
 			                    -state \$::SKYMAP::sky_ui(cmd_at_cda)" \
 			                ]
 			               
	set l1 [checkbutton $f2.cmdcda.cb1 -text "FWD" \
 			               -onvalue normal	\
 			               -offvalue disabled \
 			               -state $::SKYMAP::sky_ui(cmd_at_cda) \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(cmd_at_cda_fwd) \
 			               -bg $::BODY_OBJS::body_obj(cmd_at_cda_fwd,color) \
 			               -fg black \
 			               -anchor w \
 			               -justify right]

	set l2 [checkbutton $f2.cmdcda.cb2 -text "AFT" \
 			               -onvalue normal	\
 			               -offvalue disabled \
 			               -state $::SKYMAP::sky_ui(cmd_at_cda) \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(cmd_at_cda_aft) \
 			               -bg $::BODY_OBJS::body_obj(cmd_at_cda_aft,color) \
 			               -fg black \
 			               -anchor w \
 			               -justify right]


	pack $c -side left -fill x
	pack $l2 $l1 -side right -fill x

	frame $f2.dsndsn -borderwidth 1 \
		        -relief ridge
	pack $f2.dsndsn -side top \
		       -anchor w \
		       -fill x
	set c [checkbutton $f2.dsndsn.cb  -text "DSN at DSN" \
 			                 -onvalue normal \
 			                 -offvalue disabled \
	                                 -bg gray85 \
	                                 -fg black \
	                                 -borderwidth 1 \
	                                 -relief ridge \
	                                 -font $::CONFIG::fonts(entry) \
 			                 -variable ::SKYMAP::sky_ui(dsn_tm_at_dsn) \
 			                 -anchor w \
 			                 -justify right \
		                         -command "$f2.dsndsn.cb1 configure \
 			                    -state \$::SKYMAP::sky_ui(dsn_tm_at_dsn); \
 			                    $f2.dsndsn.cb2 configure \
 			                    -state \$::SKYMAP::sky_ui(dsn_tm_at_dsn)" \
 			                ]
 			               
	set l1 [checkbutton $f2.dsndsn.cb1 -text "FWD" \
 			               -onvalue normal	\
 			               -offvalue disabled \
 			               -state $::SKYMAP::sky_ui(dsn_tm_at_dsn) \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(dsn_tm_at_dsn_fwd) \
 			               -bg $::BODY_OBJS::body_obj(dsn_tm_at_dsn_fwd,color) \
 			               -fg black \
 			               -anchor w \
 			               -justify right]

	set l2 [checkbutton $f2.dsndsn.cb2 -text "AFT" \
 			               -onvalue normal	\
 			               -offvalue disabled \
 			               -state $::SKYMAP::sky_ui(dsn_tm_at_dsn) \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(dsn_tm_at_dsn_aft) \
 			               -bg $::BODY_OBJS::body_obj(dsn_tm_at_dsn_aft,color) \
 			               -fg black \
 			               -anchor w \
 			               -justify right]


	pack $c -side left -fill x
	pack $l2 $l1 -side right -fill x

	frame $f2.cmddsn -borderwidth 1 \
		        -relief ridge
	pack $f2.cmddsn -side top \
		       -anchor w \
		       -fill x
	set c [checkbutton $f2.cmddsn.cb  -text "CMD at DSN" \
 			                 -onvalue normal \
 			                 -offvalue disabled \
	                                 -bg gray85 \
	                                 -fg black \
	                                 -borderwidth 1 \
	                                 -relief ridge \
	                                 -font $::CONFIG::fonts(entry) \
 			                 -variable ::SKYMAP::sky_ui(cmd_at_dsn) \
 			                 -anchor w \
 			                 -justify right \
		                         -command "$f2.cmddsn.cb1 configure \
 			                    -state \$::SKYMAP::sky_ui(cmd_at_dsn); \
 			                    $f2.cmddsn.cb2 configure \
 			                    -state \$::SKYMAP::sky_ui(cmd_at_dsn)" \
 			                ]
 			               
	set l1 [checkbutton $f2.cmddsn.cb1 -text "FWD" \
 			               -onvalue normal	\
 			               -offvalue disabled \
 			               -state $::SKYMAP::sky_ui(cmd_at_dsn) \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(cmd_at_dsn_fwd) \
 			               -bg $::BODY_OBJS::body_obj(cmd_at_dsn_fwd,color) \
 			               -fg black \
 			               -anchor w \
 			               -justify right]

	set l2 [checkbutton $f2.cmddsn.cb2 -text "AFT" \
 			               -onvalue normal	\
 			               -offvalue disabled \
 			               -state $::SKYMAP::sky_ui(cmd_at_dsn) \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(cmd_at_dsn_aft) \
 			               -bg $::BODY_OBJS::body_obj(cmd_at_dsn_aft,color) \
 			               -fg black \
 			               -anchor w \
 			               -justify right]


	pack $c -side left -fill x
	pack $l2 $l1 -side right -fill x

	set c [frame $f2.spcr2 -borderwidth 1 \
	                       -relief sunken \
	                       -background gray50]
			
	pack $c -side top -fill x -ipady 4

	set c [checkbutton $f2.inst_c  -text "Instrument Cooler" \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -font $::CONFIG::fonts(entry) \
	                               -borderwidth 1 \
	                               -relief ridge \
 			               -variable ::SKYMAP::sky_ui(instrument_cooler)	\
			               -anchor w \
			               -justify right \
	                               -bg $::BODY_OBJS::body_obj(instrument_cooler,color) \
	                               -fg black]

	pack $c -side top -fill x

	set c [checkbutton $f2.inst_o  -text "Instrument Optical" \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -font $::CONFIG::fonts(entry) \
	                               -borderwidth 1 \
	                               -relief ridge \
 			               -variable ::SKYMAP::sky_ui(instrument_optical)	\
			               -anchor w \
			               -justify right \
	                               -bg [$gi \
	                                    INSTRUMENT_OPTICAL_COLOR \
	                                    PeachPuff1] \
	                               -fg black]

	pack $c -side top -fill x
	
	set c [frame $f2.spcr3 -borderwidth 1 \
	                       -relief sunken \
	                       -background gray50]
			
	pack $c -side top -fill x -ipady 4

	set c [checkbutton $f2.sun     -text "Sun Sensor" \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -font $::CONFIG::fonts(entry) \
	                               -borderwidth 1 \
	                               -relief ridge \
 			               -variable ::SKYMAP::sky_ui(sun_sensor)	\
			               -anchor w \
			               -justify right \
	                               -bg $::BODY_OBJS::body_obj(sun_sensor,color) \
	                               -fg black]

	pack $c -side top -fill x
	
	set c [frame $f2.spcr4 -borderwidth 1 \
	                       -relief sunken \
	                       -background gray50]
			
	pack $c -side top -fill x -ipady 4

	set c [checkbutton $f2.sky_x   -text "Spacecraft X-axis" \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -borderwidth 1 \
	                               -relief ridge \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(sky_x) \
 			               -anchor w \
			               -justify right \
	                               -bg [$gi \
	                                    BODY_X_AXIS_PROJ_COLOR \
	                                    orchid1] \
	                               -fg black]
			
	pack $c -side top -fill x

	set c [checkbutton $f2.sky_y   -text "Spacecraft Y-axis" \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -borderwidth 1 \
	                               -relief ridge \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(sky_y) \
 			               -anchor w \
 			               -justify right \
 			               -bg [$gi \
 			                    BODY_Y_AXIS_PROJ_COLOR \
 			                    MediumOrchid1] \
 			               -fg black]
			
	pack $c -side top -fill x

	set c [checkbutton $f2.sky_z   -text "Spacecraft Z-axis" \
 			               -onvalue normal	\
 			               -offvalue hidden \
	                               -borderwidth 1 \
	                               -relief ridge \
	                               -font $::CONFIG::fonts(entry) \
 			               -variable ::SKYMAP::sky_ui(sky_z) \
 			               -anchor w \
 			               -justify right \
 			               -bg [$gi \
 			                    BODY_Z_AXIS_PROJ_COLOR \
 			                    DarkOrchid1] \
 			               -fg black]
			
	pack $c -side top -fill x

	frame $f3.st_mode -borderwidth 1 \
		          -relief ridge \
		          -bg gray50
	pack $f3.st_mode -side top -fill x 

	frame $f3.st_mode.st
	pack $f3.st_mode.st -side top \
	                    -fill x

	set c [checkbutton $f3.st_mode.st.cb \
	                   -text "Star Trackers" \
 			   -onvalue normal \
 			   -offvalue disabled \
	                   -font $::CONFIG::fonts(entry) \
	                   -bg gray85 \
	                   -fg black \
	                   -borderwidth 1 \
	                   -relief ridge \
 			   -variable ::SKYMAP::sky_ui(st) \
 			   -command "$f3.st_mode.sq configure \
 			             -state \$::SKYMAP::sky_ui(st); \
 			             $f3.st_mode.cr configure \
 			             -state \$::SKYMAP::sky_ui(st); \
 			             $f3.st_mode.an configure \
 			             -state \$::SKYMAP::sky_ui(st); \
 			             $f3.st_mode.st.cb1 configure \
 			             -state \$::SKYMAP::sky_ui(st); \
 			             $f3.st_mode.st.cb2 configure \
 			             -state \$::SKYMAP::sky_ui(st); \
 			             $f3.st_mode.st.cb3 configure \
 			             -state \$::SKYMAP::sky_ui(st)" \
 			             -anchor w \
 			             -justify right]

 			               
	set l1 [checkbutton $f3.st_mode.st.cb1 \
	                    -text "1" \
 			    -onvalue normal	\
 			    -offvalue disabled \
	                    -bg gray85 \
	                    -fg black \
 			    -state $::SKYMAP::sky_ui(st) \
	                    -font $::CONFIG::fonts(entry) \
	                    -borderwidth 1 \
	                    -relief ridge \
 			    -variable ::SKYMAP::sky_ui(st_1) \
 			    -anchor w \
 			    -justify right]

	set l2 [checkbutton $f3.st_mode.st.cb2 \
	                    -text "2" \
 			    -onvalue normal	\
 			    -offvalue disabled \
	                    -bg gray85 \
	                    -fg black \
 			    -state $::SKYMAP::sky_ui(st) \
	                    -font $::CONFIG::fonts(entry) \
	                    -borderwidth 1 \
	                    -relief ridge \
 			    -variable ::SKYMAP::sky_ui(st_2) \
 			    -anchor w \
 			    -justify right]

	set l3 [checkbutton $f3.st_mode.st.cb3 \
	                    -text "3" \
 			    -onvalue normal	\
 			    -offvalue disabled \
	                    -bg gray85 \
	                    -fg black \
 			    -state $::SKYMAP::sky_ui(st) \
	                    -font $::CONFIG::fonts(entry) \
	                    -borderwidth 1 \
	                    -relief ridge \
 			    -variable ::SKYMAP::sky_ui(st_3) \
 			    -anchor w \
 			    -justify right]


	pack $c -side left -fill x
	pack $l3 $l2 $l1 -side right -fill x
	
	frame $f3.st_mode.spcr1 -width 10 -bg gray50
	pack $f3.st_mode.spcr1 -side left -anchor nw
			    	
	set c [radiobutton $f3.st_mode.sq -text "Square" \
 			                  -value square \
 			                  -indicator on \
	                                  -font $::CONFIG::fonts(entry) \
	                                  -bg gray85 \
	                                  -fg black \
	                                  -borderwidth 1 \
	                                  -relief ridge \
 			                  -variable ::SKYMAP::sky_ui(st_mode) \
 			                  -anchor w \
 			                  -state $::SKYMAP::sky_ui(st) \
 			                  -justify right]
	
	pack $c -side top -fill x
	
	set c [radiobutton $f3.st_mode.cr -text "Circle" \
 			                  -value circle \
 			                  -indicator on \
	                                  -font $::CONFIG::fonts(entry) \
	                                  -bg gray85 \
	                                  -fg black \
	                                  -borderwidth 1 \
	                                  -relief ridge \
 			                  -variable ::SKYMAP::sky_ui(st_mode) \
 			                  -anchor w \
 			                  -state $::SKYMAP::sky_ui(st) \
 			                  -justify right]
	
	pack $c -side top -fill x
	
	set c [radiobutton $f3.st_mode.an -text "Annulus" \
 			                  -value annulus \
 			                  -indicator on \
	                                  -font $::CONFIG::fonts(entry) \
	                                  -bg gray85 \
	                                  -fg black \
	                                  -borderwidth 1 \
	                                  -relief ridge \
 			                  -variable ::SKYMAP::sky_ui(st_mode) \
 			                  -anchor w \
 			                  -state $::SKYMAP::sky_ui(st) \
 			                  -justify right]
	
	pack $c -side top -fill x

	#	Star Magnitude control
	frame $f3.mag -bg gray50 \
	              -borderwidth 1 \
	              -relief ridge 

	pack $f3.mag -side top -fill x

	set c [checkbutton $f3.mag.stars -text "Stars" \
 			                 -onvalue normal \
 			                 -offvalue disabled \
	                                 -font $::CONFIG::fonts(entry) \
	                                 -bg gray85 \
	                                 -fg black \
	                                 -borderwidth 1 \
	                                 -relief ridge \
 			                 -variable ::SKYMAP::sky_ui(stars) \
 			                 -command "$f3.mag.osc configure \
 			                    -state \$::SKYMAP::sky_ui(stars); \
 			                    $f3.mag.mag.ent configure \
 			                    -state \$::SKYMAP::sky_ui(stars);\
 			                    $f3.mag.mag.btn configure \
 			                    -state \$::SKYMAP::sky_ui(stars);\
 			                    ::SKYMAP::toggle_all_stars" \
 			                 -anchor w \
 			                 -justify right]
			    
	pack $c -side top -fill x
			    	
	frame $f3.mag.spcr1 -width 10 -bg gray50
	pack $f3.mag.spcr1 -side left -anchor nw
	
	set c [checkbutton $f3.mag.osc -text "OSC Stars only" \
 			               -onvalue OSC \
 			               -offvalue ALL \
	                               -font $::CONFIG::fonts(entry) \
	                               -bg gray85 \
	                               -fg black \
	                               -borderwidth 1 \
	                               -relief ridge \
 			               -variable ::SKYMAP::sky_ui(osc) \
 			               -anchor w \
 			               -state $::SKYMAP::sky_ui(stars) \
 			               -justify right \
 			               -command ::SKYMAP::toggle_nonosc_stars]
	
	pack $c -side top -fill x
	
	frame $f3.mag.mag  -borderwidth 1 \
	                   -relief ridge \
	                   -bg gray85

	pack $f3.mag.mag -side top -fill x
	
	button $f3.mag.mag.btn -text "Filter Stars w/Mag < " \
			       -font $::CONFIG::fonts(entry) \
	                       -bg gray85 \
	                       -fg black \
			       -anchor w \
			       -command "set ::SKYMAP::sky_ui(mag) \
			                 \$::SKYMAP::sky_ui(temp_mag);\
			                 ::SKYMAP::toggle_star_mag"
			       
        set ::SKYMAP::sky_ui(temp_mag) $::SKYMAP::sky_ui(mag)
	entry $f3.mag.mag.ent -width 3 \
			      -font $::CONFIG::fonts(entry) \
	                      -fg black \
			      -state $::SKYMAP::sky_ui(stars) \
			      -textvariable ::SKYMAP::sky_ui(temp_mag) 
			  
	pack $f3.mag.mag.btn $f3.mag.mag.ent -side left -fill x


	#	Skymap Zoom control
	frame $f3.zom -borderwidth 1 \
		      -relief ridge \
		      -bg gray50
	pack $f3.zom -side top -fill x

	set c [label $f3.zom.lbl -text "Zoom" \
	                         -font $::CONFIG::fonts(entry) \
	                         -bg gray85 \
	                         -fg black \
	                         -borderwidth 1 \
	                         -relief ridge \
 			         -anchor w \
 			         -justify right]
			    
	pack $c -side top -fill x
			    	
	frame $f3.zom.spcr1 -width 10 -bg gray50
	pack $f3.zom.spcr1 -side left -anchor nw

	#	When a zoom factor is selected, rescale the dimensions,
	#	then force the grid to be displayed (if they don't 
	#	want it they can turn it back off)
	
	set c [radiobutton $f3.zom.x1 \
	                   -text "1x" \
 			   -value 1 \
	                   -font $::CONFIG::fonts(entry) \
	                   -bg gray85 \
	                   -fg black \
	                   -borderwidth 1 \
	                   -relief ridge \
 			   -variable ::SKYMAP::sky_zf \
 			   -anchor w \
 			   -justify right \
 			   -command "::SKYMAP::scale_sky;\
 			             set ::SKYMAP::sky_ui(sky_grid) normal;\
 			             ::SKYMAP::toggle_sky_grid"]
	
	pack $c -side top -fill x
	
	set c [radiobutton $f3.zom.x2 \
	                   -text "2x" \
 			   -value 2 \
	                   -font $::CONFIG::fonts(entry) \
	                   -bg gray85 \
	                   -fg black \
	                   -borderwidth 1 \
	                   -relief ridge \
 			   -variable ::SKYMAP::sky_zf \
 			   -anchor w \
 			   -justify right \
 			   -command "::SKYMAP::scale_sky;\
 			             set ::SKYMAP::sky_ui(sky_grid) normal;\
 			             ::SKYMAP::toggle_sky_grid"]
	
	pack $c -side top -fill x
	
	set c [radiobutton $f3.zom.x5 \
	                   -text "5x" \
 			   -value 5 \
	                   -font $::CONFIG::fonts(entry) \
	                   -bg gray85 \
	                   -fg black \
	                   -borderwidth 1 \
	                   -relief ridge \
 			   -variable ::SKYMAP::sky_zf \
 			   -anchor w \
 			   -justify right \
 			   -command "::SKYMAP::scale_sky;\
 			             set ::SKYMAP::sky_ui(sky_grid) normal;\
 			             ::SKYMAP::toggle_sky_grid"]
	
	pack $c -side top -fill x
	
	set c [radiobutton $f3.zom.x10 \
	                   -text "10x" \
 			   -value 10 \
	                   -font $::CONFIG::fonts(entry) \
	                   -bg gray85 \
	                   -fg black \
	                   -borderwidth 1 \
	                   -relief ridge \
 			   -variable ::SKYMAP::sky_zf \
 			   -anchor w \
 			   -justify right \
 			   -command "::SKYMAP::scale_sky;\
 			             set ::SKYMAP::sky_ui(sky_grid) normal;\
 			             ::SKYMAP::toggle_sky_grid"]
	
	pack $c -side top -fill x
	
	set c [radiobutton $f3.zom.x20 \
	                   -text "20x" \
 			   -value 20 \
	                   -font $::CONFIG::fonts(entry) \
	                   -bg gray85 \
	                   -fg black \
	                   -borderwidth 1 \
	                   -relief ridge \
 			   -variable ::SKYMAP::sky_zf \
 			   -anchor w \
 			   -justify right \
 			   -command "::SKYMAP::scale_sky;\
 			             set ::SKYMAP::sky_ui(sky_grid) normal;\
 			             ::SKYMAP::toggle_sky_grid"]
	
	pack $c -side top -fill x
	
	
	#	Add the OK button at the bottom
	frame $btn.sep -borderwidth 1 \
		       -height 2 \
		       -relief raised
	pack $btn.sep -side top \
		      -fill x \
		      -pady 2
		      
	button $btn.ok -text OK \
		       -font $::CONFIG::fonts(label) \
		       -relief raised \
		       -command "destroy .sky_ui"

	pack $btn.ok -side top \
		     -anchor c	\
		     -pady 5
	
}

########################################################################
#	$Log: $
########################################################################
