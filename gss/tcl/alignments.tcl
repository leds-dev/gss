########################################################################
#	$Id: $
########################################################################
#	alignments data and screen display

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ALIGN {
	variable user_asel
	variable ini_SX2BodyDCM
	variable gss_dcm
	variable gss_align_sel
	variable gss_align_src
	variable gss_align_src_int
	variable gss_align_src_txt
	variable st_comit_dcm
	variable ss_comit_dcm
	variable sensor_id
	variable align_state_text
	variable align_log_text

	namespace export init 

}

########################################################################
#	private function to initialize align data

proc ::ALIGN::init { } {
	global gi
	variable gss_dcm
	variable gss_align_sel
	variable gss_align_src
	variable gss_align_src_int
	variable gss_align_src_txt
	variable user_asel
	variable st_comit_dcm
	variable ss_comit_dcm
	variable sensor_id
	variable align_state_text
	variable align_log_text
	
	set user_asel ini
	set gss_align_sel ini

	set gss_align_src FACTORY
	
	set gss_align_src_int(FACTORY) 0
	set gss_align_src_int(CALIBRATED) 1
	
	set gss_align_src_txt(0) FACTORY
	set gss_align_src_txt(1) CALIBRATED

	set sensor_id(1) 0	
	set sensor_id(2) 1	
	set sensor_id(3) 2	
	set sensor_id(sun)   3	

	set align_state_text "INI File"
	set align_log_text   "$align_state_text"
	
	#	initialize C layer
	catch {::ALIGN::set_dcms}

	#	Update the GSS variables 
	::ALIGN::set_gss_dcm
	
}

########################################################################
#	GENERATE Display Window
#	allows standalone testing as well as notebook setup.

proc ::ALIGN::align_gui { } {
	global gi
	
	variable lc
	variable p
	variable x_size
	variable y_size
	variable x_val
	variable y_val
	variable mag_text
	variable sensor_id
	
	set stcol_title(1) H
	set stcol_title(2) V
	set stcol_title(3) Boresight
	
	set sscol_title(1) Boresight
	set sscol_title(2) ACSS
	set sscol_title(3) TOSS
	
	set rh0 [$gi NOISE_VAR_HORIZ_RH0 5.875776e-10]
	set rv0 [$gi NOISE_VAR_VERT_RV0  5.875776e-10]
	set z00 [$gi COVAR_THRESH_Z00    0.0]
    
##	set cis_state(0) disabled
##	set cis_state(1) normal
##	set cis_state_text(disabled) "Not Committed"
##	set cis_state_text(normal) "Committed"

	catch {destroy .align_ui}
	toplevel .align_ui
	wm title .align_ui "SENSOR ALIGNMENT SELECTION"
	wm geometry .align_ui "-0-0"

	set tf .align_ui.top
	frame $tf
	pack $tf -side top

	set bf .align_ui.bottom
	frame $bf
	pack $bf -side top \
	         -fill x

	frame $tf.f0 -borderwidth 1 \
	             -relief sunken
	pack $tf.f0 -side top \
	            -fill both
	            
	frame $tf.f1 -borderwidth 1 \
	             -relief sunken
	pack $tf.f1 -side top \
	            -fill both
	            
	frame $tf.f2 -borderwidth 1 \
	             -relief sunken
	pack $tf.f2 -side top \
	            -fill both
	            
	frame $tf.f0.hdr
	pack $tf.f0.hdr -side top \
	                -fill x \
	                -pady 2
	
	frame $tf.f0.rh0d
	pack $tf.f0.rh0d -side top \
	                -fill x \
	                -pady 2
	
	frame $tf.f0.rv0d
	pack $tf.f0.rv0d -side top \
	                -fill x \
	                -pady 2
	
	frame $tf.f0.z00d
	pack $tf.f0.z00d -side top \
	                -fill x \
	                -pady 2
	
	frame $tf.f1.pwr
	pack $tf.f1.pwr -side top \
	                -fill x \
	                -pady 2
	
	frame $tf.f1.ini
	pack $tf.f1.ini -side top \
	                -fill x 
	             
	frame $tf.f1.cis2
	pack $tf.f1.cis2 -side top \
	                 -fill x \
	                 -pady 2
	             
	frame $bf.set -borderwidth 1 \
	              -relief sunken
	pack $bf.set -side top \
	             -fill x
	             
	frame $bf.ini -borderwidth 1 \
	              -relief sunken
	pack $bf.ini -side top \
	             -fill x
	
	frame $bf.btn -borderwidth 1 \
	              -relief sunken
	pack $bf.btn -side top \
	             -fill x

	#	Header for noise parameters
	label $tf.f0.hdr.lbl1 -text "Star Tracker Noise Parameters" \
	                      -font $::CONFIG::fonts(entry) \
	                      -anchor w \
	                      -width 40
	pack $tf.f0.hdr.lbl1 -side left \
	                    -anchor w
	                    
	#	Give the horizontal measurement noise
	label $tf.f0.rh0d.lbl1 -text "rH0" \
	                      -font $::CONFIG::fonts(entry) \
	                      -width 12
	pack $tf.f0.rh0d.lbl1 -side left \
	                    -anchor w
	
	label $tf.f0.rh0d.lbl2 -text "$rh0" \
	                      -font $::CONFIG::fonts(entry) \
	                      -width 12 \
	                      -borderwidth 1 \
	                      -anchor w\
	                      -justify left \
	                      -relief sunken \
	                      -background grey70 \
	                      -foreground black
	pack $tf.f0.rh0d.lbl2 -side left \
	                    -anchor w
	                    
	label $tf.f0.rh0d.lbl3 -text "rad^2" \
	                      -font $::CONFIG::fonts(entry) \
	                      -anchor w\
	                      -width 12
	pack $tf.f0.rh0d.lbl3 -side left \
	                    -anchor w
	
	#	Give the vertical measurement noise
	label $tf.f0.rv0d.lbl1 -text "rV0" \
	                      -font $::CONFIG::fonts(entry) \
	                      -width 12
	pack $tf.f0.rv0d.lbl1 -side left \
	                    -anchor w
	
	label $tf.f0.rv0d.lbl2 -text "$rv0" \
	                      -font $::CONFIG::fonts(entry) \
	                      -width 12 \
	                      -borderwidth 1 \
	                      -anchor w \
	                      -justify left \
	                      -relief sunken \
	                      -background grey70 \
	                      -foreground black
	pack $tf.f0.rv0d.lbl2 -side left \
	                    -anchor w
	
	label $tf.f0.rv0d.lbl3 -text "rad^2" \
	                      -font $::CONFIG::fonts(entry) \
	                      -anchor w\
	                      -width 12
	pack $tf.f0.rv0d.lbl3 -side left \
	                    -anchor w
	
	#	Give the sanity test threshold
	label $tf.f0.z00d.lbl1 -text "Z00" \
	                      -font $::CONFIG::fonts(entry) \
	                      -width 12
	pack $tf.f0.z00d.lbl1 -side left \
	                    -anchor w
	
	label $tf.f0.z00d.lbl2 -text "$z00" \
	                      -font $::CONFIG::fonts(entry) \
	                      -width 12 \
	                      -borderwidth 1 \
	                      -anchor w \
	                      -justify left \
	                      -relief sunken \
	                      -background grey70 \
	                      -foreground black
	pack $tf.f0.z00d.lbl2 -side left \
	                    -anchor w
	                    
	label $tf.f0.z00d.lbl3 -text "rad^2" \
	                      -font $::CONFIG::fonts(entry) \
	                      -anchor w\
	                      -width 12
	pack $tf.f0.z00d.lbl3 -side left \
	                    -anchor w
	
	#	Give the ST power-on combination selected
	label $tf.f1.pwr.lbl1 -text "Star Trackers" \
	                      -font $::CONFIG::fonts(entry) \
	                      -anchor w \
	                      -width 12
	pack $tf.f1.pwr.lbl1 -side left \
	                    -anchor w
	
	label $tf.f1.pwr.lbl2 -text "$::CONFIG::data(sts_active)" \
	                      -font $::CONFIG::fonts(entry) \
	                      -width 3 \
	                      -borderwidth 1 \
	                      -relief sunken \
	                      -background grey70 \
	                      -foreground black
	pack $tf.f1.pwr.lbl2 -side left \
	                    -anchor w
	
	#	Ini file data
	frame $tf.f1.ini.rb
	pack $tf.f1.ini.rb -side left \
	                   -fill y \
	                   -padx 1

	label $tf.f1.ini.rb.spcr \
	      -text {} \
	      -font $::CONFIG::fonts(entry)
	                   
	radiobutton $tf.f1.ini.rb.rb -text "Ini File" \
				     -font $::CONFIG::fonts(entry) \
				     -variable ::ALIGN::user_asel \
				     -value ini \
				     -indicator on \
				     -anchor w \
				     -width 15

	label $tf.f1.ini.rb.src  \
	      -text [$gi PWR_$::CONFIG::data(sts_active)\_DCM_SRC Error] \
	      -font $::CONFIG::fonts(label) \
	      -justify left \
	      -foreground black \
	      -width 15

	pack $tf.f1.ini.rb.spcr $tf.f1.ini.rb.rb $tf.f1.ini.rb.src \
	     -side top \
	     -anchor w \
	     -fill x

	frame $tf.f1.ini.col
	pack $tf.f1.ini.col -side left \
	                    -fill y

	label $tf.f1.ini.col.lbl1 -text "X" \
	                          -font $::CONFIG::fonts(label) \
	                          -justify left \
	                          -anchor w \
	                          -borderwidth 1 \
	                          -relief ridge \
	                          -background gray80 \
	                          -foreground black \
	                          -width 2

	label $tf.f1.ini.col.lbl2 -text "Y" \
	                          -font $::CONFIG::fonts(label) \
	                          -justify left \
	                          -anchor w \
	                          -borderwidth 1 \
	                          -relief ridge \
	                          -background gray80 \
	                          -foreground black \
	                          -width 2
	                          
	label $tf.f1.ini.col.lbl3 -text "Z" \
	                          -font $::CONFIG::fonts(label) \
	                          -justify left \
	                          -anchor w \
	                          -borderwidth 1 \
	                          -relief ridge \
	                          -background gray80 \
	                          -foreground black \
	                          -width 2
	                          
	pack $tf.f1.ini.col.lbl3 $tf.f1.ini.col.lbl2 \
	     $tf.f1.ini.col.lbl1 \
	     -side bottom \
	     -anchor s \
	     -ipadx 2
			      
	foreach st {1 2 3} {
	    frame $tf.f1.ini.st$st
	    pack $tf.f1.ini.st$st -side left \
	                          -anchor w \
	                          -fill x \
	                          -padx 3

	    label $tf.f1.ini.st$st.lbl -text "ST $st" \
	                               -font $::CONFIG::fonts(entry) \
	                               -justify center
	    pack $tf.f1.ini.st$st.lbl -side top \
	                              -anchor c \
	                              -fill x
	    foreach col {1 2 3} {
	        frame $tf.f1.ini.st$st.c$col
	        pack $tf.f1.ini.st$st.c$col -side left \
	                                    -anchor w \
	                                    -fill x
	                                    
	        label $tf.f1.ini.st$st.c$col.hdr \
	              -text $stcol_title($col) \
	              -font $::CONFIG::fonts(entry) \
	              -justify center \
	              -borderwidth 1 \
	              -relief ridge \
	              -background gray80 \
	              -foreground black \
	              -width 11
	        pack $tf.f1.ini.st$st.c$col.hdr \
	             -side top \
	              -anchor w


	        foreach y {1 2 3} {
	            label $tf.f1.ini.st$st.c$col.lbl$y \
	                  -textvariable ::ALIGN::ini_SX2BodyDCM($st,$y,$col) \
	                  -font $::CONFIG::fonts(entry) \
	                  -justify left \
	                  -borderwidth 1 \
	                  -relief sunken \
	                  -background gray70 \
	                  -foreground black \
	                  -width 11
	            pack $tf.f1.ini.st$st.c$col.lbl$y \
	                  -side top \
	                  -anchor w
	            

	        }
	         
	    }
	    
	}

	#	Display Sun Sensor Alignments
	frame $tf.f1.ini.sun
	pack $tf.f1.ini.sun -side left \
	                    -anchor w \
	                    -fill x \
	                    -padx 3

	label $tf.f1.ini.sun.lbl -text "Sun Sensor" \
	                         -font $::CONFIG::fonts(entry) \
	                         -justify center
	pack $tf.f1.ini.sun.lbl -side top \
	                        -anchor c \
	                        -fill x
	                        
	foreach col {1 2 3} {
	    frame $tf.f1.ini.sun.c$col
	    pack $tf.f1.ini.sun.c$col -side left \
	                              -anchor w \
	                              -fill x
	                                    
	    label $tf.f1.ini.sun.c$col.hdr \
	          -text $sscol_title($col) \
	          -font $::CONFIG::fonts(entry) \
	          -justify center \
	          -borderwidth 1 \
	          -relief ridge \
	          -background gray80 \
	          -foreground black \
	          -width 11
	    pack $tf.f1.ini.sun.c$col.hdr \
	         -side top \
	          -anchor w

	    foreach y {1 2 3} {
	        label $tf.f1.ini.sun.c$col.lbl$y \
	              -textvariable ::ALIGN::ini_SX2BodyDCM(sun,$y,$col) \
	              -font $::CONFIG::fonts(entry) \
	              -justify left \
	              -borderwidth 1 \
	              -relief sunken \
	              -background gray70 \
	              -foreground black \
	              -width 11
	        pack $tf.f1.ini.sun.c$col.lbl$y \
	             -side top \
	             -anchor w
	            

	    }
	         
	}

	
	#	Comitted CIS data
	frame $tf.f1.cis2.rb
##	pack $tf.f1.cis2.rb -side left \
##	                   -fill y
	pack $tf.f1.cis2.rb -side left \
	                    -fill y \
	                    -padx 1

	label $tf.f1.cis2.rb.spcr \
	      -text {} \
	      -font $::CONFIG::fonts(entry) 
                   
	radiobutton $tf.f1.cis2.rb.rb \
	           -text "CIS Results" \
		   -font $::CONFIG::fonts(entry) \
		   -variable ::ALIGN::user_asel \
		   -value cis \
		   -state $::CIS_GUI::cis_committed \
		   -indicator on \
		   -anchor w \
		   -width 15

	set qlty [GetAlignSel]

	label $tf.f1.cis2.rb.src  \
	      -text $::ALIGN::gss_align_src_txt($qlty) \
	      -font $::CONFIG::fonts(label) \
	      -justify left \
	      -foreground black \
	      -width 15

##	label $tf.f1.cis2.rb.stat  \
##	      -text "($cis_state_text($::CIS_GUI::cis_committed)" \
##	      -font $::CONFIG::fonts(label) \
##	      -justify left \
##	      -foreground black \
##	      -width 15
	label $tf.f1.cis2.rb.stat  \
	      -textvariable ::CIS_GUI::cis_state_text \
	      -font $::CONFIG::fonts(label) \
	      -justify left \
	      -foreground black \
	      -width 15

	pack $tf.f1.cis2.rb.spcr $tf.f1.cis2.rb.rb $tf.f1.cis2.rb.src \
	     $tf.f1.cis2.rb.stat \
	      -side top \
	      -anchor w \
	      -fill both

	frame $tf.f1.cis2.col
	pack $tf.f1.cis2.col -side left \
	                     -fill y

	label $tf.f1.cis2.col.lbl1 -text "X" \
	                           -font $::CONFIG::fonts(label) \
	                           -justify left \
	                           -anchor w \
	                           -borderwidth 1 \
	                           -relief sunken \
	                           -background gray80 \
	                           -foreground black \
	                           -width 2

	label $tf.f1.cis2.col.lbl2 -text "Y" \
	                           -font $::CONFIG::fonts(label) \
	                           -justify left \
	                           -anchor w \
	                           -borderwidth 1 \
	                           -relief sunken \
	                           -background gray80 \
	                           -foreground black \
	                           -width 2
	                          
	label $tf.f1.cis2.col.lbl3 -text "Z" \
	                           -font $::CONFIG::fonts(label) \
	                           -justify left \
	                           -anchor w \
	                           -borderwidth 1 \
	                           -relief sunken \
	                           -background gray80 \
	                           -foreground black \
	                           -width 2
	                          
	pack $tf.f1.cis2.col.lbl3 $tf.f1.cis2.col.lbl2 \
	     $tf.f1.cis2.col.lbl1 \
	     -side bottom \
	     -anchor s \
	     -ipadx 2
			      
	foreach st {1 2 3} {
##	    set data [GetCommitST2BodyDCM $st]
##	    set dcm(1,1) [lindex $data 0]
##	    set dcm(1,2) [lindex $data 1]
##	    set dcm(1,3) [lindex $data 2]
##	    set dcm(2,1) [lindex $data 3]
##	    set dcm(2,2) [lindex $data 4]
##	    set dcm(2,3) [lindex $data 5]
##	    set dcm(3,1) [lindex $data 6]
##	    set dcm(3,2) [lindex $data 7]
##	    set dcm(3,3) [lindex $data 8]
	    
	    frame $tf.f1.cis2.st$st
	    pack $tf.f1.cis2.st$st -side left \
	                          -anchor w \
	                          -fill x \
	                          -padx 3

	    label $tf.f1.cis2.st$st.lbl -text "ST $st" \
	                               -font $::CONFIG::fonts(entry) \
	                               -justify center
	    pack $tf.f1.cis2.st$st.lbl -side top \
	                              -anchor c \
	                              -fill x
	    foreach col {1 2 3} {
	        frame $tf.f1.cis2.st$st.c$col
	        pack $tf.f1.cis2.st$st.c$col -side left \
	                                    -anchor w \
	                                    -fill x
                                    
	        label $tf.f1.cis2.st$st.c$col.hdr \
	              -text $stcol_title($col) \
	              -font $::CONFIG::fonts(entry) \
	              -justify center \
	              -borderwidth 1 \
	              -relief ridge \
	              -background gray80 \
	              -foreground black \
	              -width 11
	        pack $tf.f1.cis2.st$st.c$col.hdr \
	             -side top \
	              -anchor w

	        foreach y {1 2 3} {
#	            label $tf.f1.cis2.st$st.c$col.lbl$y \
#	                  -text [format "%7.5f" $dcm($y,$col)] \
#	                  -font $::CONFIG::fonts(entry) \
#	                  -justify left \
#	                  -borderwidth 1 \
#	                  -relief sunken \
#	                  -background gray70 \
#	                  -foreground black \
#	                  -width 9
	            label $tf.f1.cis2.st$st.c$col.lbl$y \
	                  -textvariable ::ALIGN::st_comit_dcm($st,$y,$col) \
	                  -font $::CONFIG::fonts(entry) \
	                  -justify left \
	                  -borderwidth 1 \
	                  -relief sunken \
	                  -background gray70 \
	                  -foreground black \
	                  -width 11
	            pack $tf.f1.cis2.st$st.c$col.lbl$y \
	                  -side top \
	                  -anchor w
	            

	        }
	         
	    }
	    
	}

	#	Display the CIS Sun Sensor Alignments
##	set data [GetCommitSS2BodyDCM]
##	set dcm(1,1) [lindex $data 0]
##	set dcm(1,2) [lindex $data 1]
##	set dcm(1,3) [lindex $data 2]
##	set dcm(2,1) [lindex $data 3]
##	set dcm(2,2) [lindex $data 4]
##	set dcm(2,3) [lindex $data 5]
##	set dcm(3,1) [lindex $data 6]
##	set dcm(3,2) [lindex $data 7]
##	set dcm(3,3) [lindex $data 8]
	    
	frame $tf.f1.cis2.sun
	pack $tf.f1.cis2.sun -side left \
	                     -anchor w \
	                     -fill x \
	                     -padx 3

	label $tf.f1.cis2.sun.lbl -text "Sun Sensor" \
	                          -font $::CONFIG::fonts(entry) \
	                          -justify center
	pack $tf.f1.cis2.sun.lbl -side top \
	                         -anchor c \
	                         -fill x

	foreach col {1 2 3} {
	    frame $tf.f1.cis2.sun.c$col
	    pack $tf.f1.cis2.sun.c$col -side left \
	                               -anchor w \
	                               -fill x
                                    
	    label $tf.f1.cis2.sun.c$col.hdr \
	          -text $sscol_title($col) \
	          -font $::CONFIG::fonts(entry) \
	          -justify center \
	          -borderwidth 1 \
	          -relief ridge \
	          -background gray80 \
	          -foreground black \
	          -width 11
	    pack $tf.f1.cis2.sun.c$col.hdr \
	          -side top \
	          -anchor w

	    foreach y {1 2 3} {
#	        label $tf.f1.cis2.sun.c$col.lbl$y \
#	              -text [format "%7.5f" $dcm($y,$col)] \
#	              -font $::CONFIG::fonts(entry) \
#	              -justify left \
#	              -borderwidth 1 \
#	              -relief sunken \
#	              -background gray70 \
#	              -foreground black \
#	              -width 9
	        label $tf.f1.cis2.sun.c$col.lbl$y \
	              -textvariable ::ALIGN::ss_comit_dcm($y,$col) \
	              -font $::CONFIG::fonts(entry) \
	              -justify left \
	              -borderwidth 1 \
	              -relief sunken \
	              -background gray70 \
	              -foreground black \
	              -width 11
	        pack $tf.f1.cis2.sun.c$col.lbl$y \
	              -side top \
	              -anchor w
	           

	    }
	         
	}


	
	#	GSS data
	frame $tf.f2.src
	pack $tf.f2.src -side top \
	                -fill x
	                   
	label $tf.f2.src.lbl -text "GSS DCM SOURCE" \
			     -font $::CONFIG::fonts(entry) \
			     -anchor w \
			     -width 20 
	                   
	label $tf.f2.src.lbl2 -textvariable ::ALIGN::gss_align_sel \
			      -font $::CONFIG::fonts(entry) \
			      -relief sunken \
			      -anchor c \
			      -justify center \
			      -width 5 

	pack $tf.f2.src.lbl $tf.f2.src.lbl2 \
	                    -side left \
	                    -fill x \
	                    -pady 3

	frame $tf.f2.gss
	pack $tf.f2.gss -side left \
	                -fill x \
	                -pady 3

	frame $tf.f2.gss.hdr
	pack $tf.f2.gss.hdr -side left \
	                    -fill y
	                    
	label $tf.f2.gss.hdr.spcr \
	      -text {} \
	      -font $::CONFIG::fonts(entry)
	      
	label $tf.f2.gss.hdr.lbl \
	      -text "GSS DCM" \
	      -font $::CONFIG::fonts(entry) \
	      -anchor w \
	      -relief flat \
	      -width 19

	label $tf.f2.gss.hdr.src  \
	      -textvariable ::ALIGN::gss_align_src \
	      -font $::CONFIG::fonts(label) \
	      -justify left \
	      -foreground black \
	      -width 15

	pack $tf.f2.gss.hdr.spcr $tf.f2.gss.hdr.lbl $tf.f2.gss.hdr.src \
	     -side top \
	     -fill x

	frame $tf.f2.gss.col
	pack $tf.f2.gss.col -side left \
	                    -anchor s \
	                    -fill y

	label $tf.f2.gss.col.lbl1 -text "X" \
	                           -font $::CONFIG::fonts(label) \
	                           -justify left \
	                           -anchor w \
	                           -borderwidth 1 \
	                           -relief sunken \
	                           -background gray80 \
	                           -foreground black \
	                           -width 2

	label $tf.f2.gss.col.lbl2 -text "Y" \
	                           -font $::CONFIG::fonts(label) \
	                           -justify left \
	                           -anchor w \
	                           -borderwidth 1 \
	                           -relief sunken \
	                           -background gray80 \
	                           -foreground black \
	                           -width 2
	                          
	label $tf.f2.gss.col.lbl3 -text "Z" \
	                           -font $::CONFIG::fonts(label) \
	                           -justify left \
	                           -anchor w \
	                           -borderwidth 1 \
	                           -relief sunken \
	                           -background gray80 \
	                           -foreground black \
	                           -width 2
	                          
	pack $tf.f2.gss.col.lbl3 $tf.f2.gss.col.lbl2 \
	     $tf.f2.gss.col.lbl1 \
	     -side bottom \
	     -anchor s \
	     -ipadx 2
			      
	foreach st {1 2 3} {
	    frame $tf.f2.gss.st$st
	    pack $tf.f2.gss.st$st -side left \
	                      -anchor w \
	                      -fill x \
	                      -padx 3

	    label $tf.f2.gss.st$st.lbl -text "ST $st" \
	                           -font $::CONFIG::fonts(entry) \
	                           -justify center
	    pack $tf.f2.gss.st$st.lbl -side top \
	                           -anchor c \
	                           -fill x
	                           
	    foreach col {1 2 3} {
	        frame $tf.f2.gss.st$st.c$col
	        pack $tf.f2.gss.st$st.c$col -side left \
	                                 -anchor w \
	                                 -fill x 
                                    
	        label $tf.f2.gss.st$st.c$col.hdr \
	              -text $stcol_title($col) \
	              -font $::CONFIG::fonts(entry) \
	              -justify center \
	              -borderwidth 1 \
	              -relief ridge \
	              -background gray80 \
	              -foreground black \
	              -width 11
	        pack $tf.f2.gss.st$st.c$col.hdr \
	             -side top \
	              -anchor w

	        foreach y {1 2 3} {
	            label $tf.f2.gss.st$st.c$col.lbl$y \
	                  -textvariable ::ALIGN::gss_dcm($st,$y,$col) \
	                  -font $::CONFIG::fonts(entry) \
	                  -justify left \
	                  -borderwidth 1 \
	                  -relief sunken \
	                  -background gray70 \
	                  -foreground black \
	                  -width 11
	            pack $tf.f2.gss.st$st.c$col.lbl$y \
	                  -side top \
	                  -anchor w
	            

	        }
	         
	    }
	    
	}

#	Put up the Sun Sensor Alignments
	frame $tf.f2.gss.sun
	pack $tf.f2.gss.sun -side left \
	                    -anchor w \
	                    -fill x \
	                    -padx 3

	label $tf.f2.gss.sun.lbl -text "Sun Sensor" \
	                         -font $::CONFIG::fonts(entry) \
	                         -justify center
	pack $tf.f2.gss.sun.lbl -side top \
	                        -anchor c \
	                        -fill x
	                           
	foreach col {1 2 3} {
	    frame $tf.f2.gss.sun.c$col
	    pack $tf.f2.gss.sun.c$col -side left \
	                                 -anchor w \
	                                 -fill x 
                                    
	    label $tf.f2.gss.sun.c$col.hdr \
	          -text $sscol_title($col) \
	          -font $::CONFIG::fonts(entry) \
	          -justify center \
	          -borderwidth 1 \
	          -relief ridge \
	          -background gray80 \
	          -foreground black \
	          -width 11
	    pack $tf.f2.gss.sun.c$col.hdr \
	         -side top \
	          -anchor w

	    foreach y {1 2 3} {
	        label $tf.f2.gss.sun.c$col.lbl$y \
	              -textvariable ::ALIGN::gss_dcm(sun,$y,$col) \
	              -font $::CONFIG::fonts(entry) \
	              -justify left \
	              -borderwidth 1 \
	              -relief sunken \
	              -background gray70 \
	              -foreground black \
	              -width 11
	        pack $tf.f2.gss.sun.c$col.lbl$y \
	              -side top \
	              -anchor w
	            

	    }
	         
	}


	#	Create Update GSS button
	button $bf.set.btn -text "SET GSS DCM" \
	                    -font $::CONFIG::fonts(entry) \
	                    -width 15 \
	                    -command "::ALIGN::set_gss_dcm"	

	pack $bf.set.btn -side top \
	                  -pady 5


	#	Create Update INI file button
	button $bf.ini.btn -text "UPDATE INI FILE ALIGNMENTS" \
	                   -font $::CONFIG::fonts(entry) \
	                   -width 30 \
	                   -command "::ALIGN::update_ini_file"	

	label $bf.ini.lbl -textvariable ::FILES::new_ini \
	                  -font $::CONFIG::fonts(entry) \
	                  -width 80 \
	                  -anchor w \
	                  -justify left \
	                  -borderwidth 1 \
	                  -relief sunken

	pack $bf.ini.btn $bf.ini.lbl \
	                 -side left \
	                 -pady 5 \
	                 -padx 10


	#	OK button
	button $bf.btn.ok -text "OK" \
	                   -font $::CONFIG::fonts(entry) \
	                   -command "destroy .align_ui"

	pack $bf.btn.ok -side top \
	                 -pady 5


}


########################################################################
#	Update the DCMs in the new INI file

proc ::ALIGN::update_ini_file { } {

    global gi
    variable sensor_id
    variable stcombos [list 12 13 23 123]
    
    #	No INI file updates unless using CIS alignments
    if {$::ALIGN::gss_align_sel == "INI"} {
       ::MSG_DLG::attention "Can not update INI file with original alignments" \
                            red \
                            black \
                             -INSTRUCTION "Select CIS Alignments and try again"

	::MSG_DLG::msg_log "ERROR attempting generate a new ini file using original alignments"

        return

    }
    
    set infile $::FILES::gssini
    set outfile [::FILES::set_new_ini]
    
    #   Retrieve which STs were successfully used by CIS (0=NOT_USED, 1=USED)
    set data [GetCommitValidRefST 1]
    set use_st1 [lindex $data 0]
    set data [GetCommitValidRefST 2]
    set use_st2 [lindex $data 0]
    set data [GetCommitValidRefST 3]
    set use_st3 [lindex $data 0]
    
    #   Figure out which ST combos had all included STs successfully calibrated by CIS
    set sts_caled_12  YES
    set sts_caled_13  YES
    set sts_caled_23  YES
    set sts_caled_123 YES
    if {$use_st1 == 0} {
    	set sts_caled_12  NO
    	set sts_caled_13  NO
    	set sts_caled_123 NO
    }
    if {$use_st2 == 0} {
    	set sts_caled_12  NO
    	set sts_caled_23  NO
    	set sts_caled_123 NO
    }
    if {$use_st3 == 0} {
    	set sts_caled_13  NO
    	set sts_caled_23  NO
    	set sts_caled_123 NO
    }
    
    #   Retrieve initial quality flags for all ST combos
    set st_qual_12  [$gi PWR_12_DCM_SRC FACTORY]
    set st_qual_13  [$gi PWR_13_DCM_SRC FACTORY]
    set st_qual_23  [$gi PWR_23_DCM_SRC FACTORY]
    set st_qual_123 [$gi PWR_123_DCM_SRC FACTORY]
    
    #	Figure out which ST power on combos to update
    #   	Previously uncalibrated ST combos
    #   	Previously calibrated ST combos, if all included STs were just cal'ed
    #   Update quality flag only for previoulsy uncalibrated ST combis if all included STs were just cal'ed
    foreach i $stcombos {
    	set st_qual  [set st_qual_$i  ]
    	set st_caled [set sts_caled_$i]
        if { ($st_qual  == "FACTORY") || \
             ($st_caled == "YES"    ) } {
            set update_st[set i] YES
            if { ($st_qual  == "FACTORY") && \
                 ($st_caled == "YES"    ) } {
                 set st_qual_[set i] $::ALIGN::gss_align_src
            }
        } else {
            set update_st[set i] NO
        }
    }

    #	Define INI file tags to target
    set date_target "GENERATION DATE:"

    set align_target12  "UPDATED ToSTA PWR_12:"
    set align_target13  "UPDATED ToSTA PWR_13:"
    set align_target23  "UPDATED ToSTA PWR_23:"
    set align_target123 "UPDATED ToSTA PWR_123:"
    
    set st_target12  "PWR_12_ST_"
    set st_target13  "PWR_13_ST_"
    set st_target23  "PWR_23_ST_"
    set st_target123 "PWR_123_ST_"
    
    set src_target12  "PWR_12_DCM_SRC"
    set src_target13  "PWR_13_DCM_SRC"
    set src_target23  "PWR_23_DCM_SRC"
    set src_target123 "PWR_123_DCM_SRC"
    
    set ss_target12  "PWR_12_SS_"
    set ss_target13  "PWR_13_SS_"
    set ss_target23  "PWR_23_SS_"
    set ss_target123 "PWR_123_SS_"
    
    #	Open the files
    set err [catch {set inid [open $infile r]}]

    if {$err == 1} {
        ::MSG_DLG::attention "Could not open INI file \n$infile \nfor read access" \
                             red \
                             black \
                             -INSTRUCTION "Check file name and directory access privileges"

        set ::FILES::new_ini {}

        return

    }

    set err [catch {set outid [open $outfile w]}]

    if {$err == 1} {
        ::MSG_DLG::attention "Could not Create new INI file \n$outfile" \
                             red \
                             black \
                             -INSTRUCTION "Check file name and directory access privileges"

        set ::FILES::new_ini {}

        return

    }

    #	Get data line
    set strlen [gets $inid str]

    while {$strlen>=0} {
        if { ([string index $str 0] == "#") || \
              ($strlen == 0)                     } {

    #	INI file header time stamp      
            if {[string first $date_target $str] != -1} {
                set date_str [clock format [clock seconds] -gmt yes]
                puts $outid "#\t$date_target\t$date_str (By GSS)"
                
    #	Individual alignment time stamps      
            } elseif {([string first $align_target12 $str] != -1) && ($update_st12 == "YES")} {
                set date_str [clock format [clock seconds] -gmt yes]
                set align_str [clock format $::CIS_GUI::cis_data_time -gmt yes]              
                puts $outid "#\t$align_target12\t$date_str, using Data Collected $align_str (By GSS)"
                
            } elseif {([string first $align_target13 $str] != -1) && ($update_st13 == "YES")} {
                set date_str [clock format [clock seconds] -gmt yes]
                set align_str [clock format $::CIS_GUI::cis_data_time -gmt yes]              
                puts $outid "#\t$align_target13\t$date_str, using Data Collected $align_str (By GSS)"
 
            } elseif {([string first $align_target23 $str] != -1) && ($update_st23 == "YES")} {
                set date_str [clock format [clock seconds] -gmt yes]
                set align_str [clock format $::CIS_GUI::cis_data_time -gmt yes]              
                puts $outid "#\t$align_target23\t$date_str, using Data Collected $align_str (By GSS)"
 
            } elseif {([string first $align_target123 $str] != -1) && ($update_st123 == "YES")} {
                set date_str [clock format [clock seconds] -gmt yes]
                set align_str [clock format $::CIS_GUI::cis_data_time -gmt yes]              
                puts $outid "#\t$align_target123\t$date_str, using Data Collected $align_str (By GSS)"
           
    #	Blank or Comment line       
            } else {
                puts $outid $str
                
            }
            
        } else {

            set key [lindex $str 0]
            
    #	Star tracker alignments
            if {(([string first $st_target12 $key]  != -1) && ($update_st12  == "YES")) || \
                (([string first $st_target13 $key]  != -1) && ($update_st13  == "YES")) || \
                (([string first $st_target23 $key]  != -1) && ($update_st23  == "YES")) || \
                (([string first $st_target123 $key] != -1) && ($update_st123 == "YES")) } {

                set end [string last "_" $key]
                set indx [expr $end - 1]
                   
                set st [string index $key $indx]

                set dlist [GetSX2STA_DCM_Data $sensor_id($st)]
                set data \
                     [format "%s\t%14.7e\t%14.7e\t%14.7e\t%14.7e\t%14.7e\t%14.7e\t%14.7e\t%14.7e\t%14.7e" \
                      $key \
                      [lindex $dlist 0] \
                      [lindex $dlist 1] \
                      [lindex $dlist 2] \
                      [lindex $dlist 3] \
                      [lindex $dlist 4] \
                      [lindex $dlist 5] \
                      [lindex $dlist 6] \
                      [lindex $dlist 7] \
                      [lindex $dlist 8] \
                     ]
                     
                 puts $outid "$data"

    #	Sun Sensor alignments
            } elseif {(([string first $ss_target12 $key]  != -1) && ($update_st12  == "YES")) || \
                      (([string first $ss_target13 $key]  != -1) && ($update_st13  == "YES")) || \
                      (([string first $ss_target23 $key]  != -1) && ($update_st23  == "YES")) || \
                      (([string first $ss_target123 $key] != -1) && ($update_st123 == "YES")) } {

                set dlist [GetSX2STA_DCM_Data $sensor_id(sun)]
                set data \
                     [format "%s\t%14.7e\t%14.7e\t%14.7e\t%14.7e\t%14.7e\t%14.7e\t%14.7e\t%14.7e\t%14.7e" \
                      $key \
                      [lindex $dlist 0] \
                      [lindex $dlist 1] \
                      [lindex $dlist 2] \
                      [lindex $dlist 3] \
                      [lindex $dlist 4] \
                      [lindex $dlist 5] \
                      [lindex $dlist 6] \
                      [lindex $dlist 7] \
                      [lindex $dlist 8] \
                     ]

                 puts $outid "$data"
            
    #	Alignment quality flags
            } elseif { (($key == $src_target12)  && ($update_st12  == "YES")) } {
                puts $outid "$key\t$st_qual_12"
                      
            } elseif { (($key == $src_target13)  && ($update_st13  == "YES")) } {
                puts $outid "$key\t$st_qual_13"
                      
            } elseif { (($key == $src_target23)  && ($update_st23  == "YES")) } {
                puts $outid "$key\t$st_qual_23"
                      
            } elseif { (($key == $src_target123) && ($update_st123 == "YES")) } {   
                puts $outid "$key\t$st_qual_123"
                        
    #	Other INI file item not involved with alignments
            } else {
                puts $outid $str
                
            }

        }
        
        set strlen [gets $inid str]

    }
    
    close $inid
   close $outid

    ::MSG_DLG::msg_log "UPDATE INI FILE ALIGNMENTS BUTTON PRESSED \n\n\
                        New ini file generated for updated alignments: \n\
                        $outfile"
    
}

########################################################################
#	Set the gss DCMs

proc ::ALIGN::set_gss_dcm {} {
	global gi
	variable sensor_id
	variable gss_align_src_int
	variable align_state_text		
	variable align_log_text		
	
# Switching to INI file alignments
	if {$::ALIGN::user_asel == "ini"} {
	
	    set ::ALIGN::gss_align_sel [string toupper $::ALIGN::user_asel]
	    
#	Set the Star Tracker and Sun Sensor alignments using data obtained from INI file
#        and copy alignment information back to the GuiInterface	    	
	    foreach sensor {1 2 3 sun} {
	        set ::ALIGN::gss_dcm($sensor,1,1) \
	            $::ALIGN::ini_SX2BodyDCM($sensor,1,1)
	        set ::ALIGN::gss_dcm($sensor,1,2) \
	            $::ALIGN::ini_SX2BodyDCM($sensor,1,2)
	        set ::ALIGN::gss_dcm($sensor,1,3) \
	            $::ALIGN::ini_SX2BodyDCM($sensor,1,3)
	        set ::ALIGN::gss_dcm($sensor,2,1) \
	            $::ALIGN::ini_SX2BodyDCM($sensor,2,1)
	        set ::ALIGN::gss_dcm($sensor,2,2) \
	            $::ALIGN::ini_SX2BodyDCM($sensor,2,2)
	        set ::ALIGN::gss_dcm($sensor,2,3) \
	            $::ALIGN::ini_SX2BodyDCM($sensor,2,3)
	        set ::ALIGN::gss_dcm($sensor,3,1) \
	            $::ALIGN::ini_SX2BodyDCM($sensor,3,1)
	        set ::ALIGN::gss_dcm($sensor,3,2) \
	            $::ALIGN::ini_SX2BodyDCM($sensor,3,2)
	        set ::ALIGN::gss_dcm($sensor,3,3) \
	            $::ALIGN::ini_SX2BodyDCM($sensor,3,3)
	        set cmd "SetSX2Body $sensor_id($sensor) $::ALIGN::ini_SX2BodyDCM($sensor,1,1)\
	        					$::ALIGN::ini_SX2BodyDCM($sensor,1,2)\
	        					$::ALIGN::ini_SX2BodyDCM($sensor,1,3)\
	        					$::ALIGN::ini_SX2BodyDCM($sensor,2,1)\
	        					$::ALIGN::ini_SX2BodyDCM($sensor,2,2)\
	        					$::ALIGN::ini_SX2BodyDCM($sensor,2,3)\
	        					$::ALIGN::ini_SX2BodyDCM($sensor,3,1)\
	        					$::ALIGN::ini_SX2BodyDCM($sensor,3,2)\
	        					$::ALIGN::ini_SX2BodyDCM($sensor,3,3)"
	        eval $cmd
	    }

#	Set the alignment quality flag to value read from INI file
	    set ::ALIGN::gss_align_src [$gi PWR_$::CONFIG::data(sts_active)\_DCM_SRC Error]
	    
#	Update sun sensor and star tracker alignments, and quality flag for use by CIS
	    SetCIS_SensorAlignment 
	    ChooseAlignment $gss_align_src_int($::ALIGN::gss_align_src)

#	Update the other TCL screens
	    set align_state_text "INI File"
	    set align_log_text   "$align_state_text"
	    
# Switching to CIS alignments
	} else {

	    set ::ALIGN::gss_align_sel [string toupper $::ALIGN::user_asel]
	
#	Set the Star Tracker alignments using data obtained from CIS commit buffer
#        and copy alignment information back to the GuiInterface
	    foreach st {1 2 3} {
	        set data [GetCommitST2BodyDCM $st]
	        set ::ALIGN::gss_dcm($st,1,1) \
	            [format "%+9.7f" [lindex $data 0]]
	        set ::ALIGN::gss_dcm($st,1,2) \
	            [format "%+9.7f" [lindex $data 1]]
	        set ::ALIGN::gss_dcm($st,1,3) \
	            [format "%+9.7f" [lindex $data 2]]
	        set ::ALIGN::gss_dcm($st,2,1) \
	            [format "%+9.7f" [lindex $data 3]]
	        set ::ALIGN::gss_dcm($st,2,2) \
	            [format "%+9.7f" [lindex $data 4]]
	        set ::ALIGN::gss_dcm($st,2,3) \
	            [format "%+9.7f" [lindex $data 5]]
	        set ::ALIGN::gss_dcm($st,3,1) \
	            [format "%+9.7f" [lindex $data 6]]
	        set ::ALIGN::gss_dcm($st,3,2) \
	            [format "%+9.7f" [lindex $data 7]]
	        set ::ALIGN::gss_dcm($st,3,3) \
	            [format "%+9.7f" [lindex $data 8]]
	        set cmd "SetSX2Body $sensor_id($st) $data"
	        eval $cmd
	    }

	    	    
#	Set the Sun Sensor alignments using data obtained from CIS commit buffer
#        and copy alignment information back to the GuiInterface
	    set data [GetCommitSS2BodyDCM]
	    set ::ALIGN::gss_dcm(sun,1,1) \
	        [format "%+9.7f" [lindex $data 0]]
	    set ::ALIGN::gss_dcm(sun,1,2) \
	        [format "%+9.7f" [lindex $data 1]]
	    set ::ALIGN::gss_dcm(sun,1,3) \
	        [format "%+9.7f" [lindex $data 2]]
	    set ::ALIGN::gss_dcm(sun,2,1) \
	        [format "%+9.7f" [lindex $data 3]]
	    set ::ALIGN::gss_dcm(sun,2,2) \
	        [format "%+9.7f" [lindex $data 4]]
	    set ::ALIGN::gss_dcm(sun,2,3) \
	        [format "%+9.7f" [lindex $data 5]]
	    set ::ALIGN::gss_dcm(sun,3,1) \
	        [format "%+9.7f" [lindex $data 6]]
	    set ::ALIGN::gss_dcm(sun,3,2) \
	        [format "%+9.7f" [lindex $data 7]]
	    set ::ALIGN::gss_dcm(sun,3,3) \
	        [format "%+9.7f" [lindex $data 8]]    
	    set cmd "SetSX2Body $sensor_id(sun) $data" 
	    eval $cmd
	    
#	Set the Star Tracker Alignment Quality flag based on CIS results
	    set qlty [GetAlignSel]
	    set ::ALIGN::gss_align_src $::ALIGN::gss_align_src_txt($qlty)

#	Update sun sensor and star tracker alignments, and quality flag for use by CIS
	    SetCIS_SensorAlignment 
	    ChooseAlignment $gss_align_src_int($::ALIGN::gss_align_src)
	           
#	Update the other TCL screens
	    set align_state_text $::CIS_GUI::cis_state_text
	    set align_log_text "CIS $align_state_text"
	    
	}

#	Log the change in alignment selection
	set msg_txt "SET GSS DCM BUTTON PRESSED\n"
	set row_id(1) X
	set row_id(2) Y
	set row_id(3) Z

	foreach sensor {1 2 3 sun} {

	    set label "\nST $sensor"
	    set title "\tH\t\tV\t\tBoresight"
	    if {$sensor == "sun"} {
	        set label "\nSun Sensor"
	        set title "\tBoresight\tACSS\t\tTOSS"

	    }

	    set msg_txt "\n$msg_txt $label\
	                 \nSource: $::ALIGN::gss_align_sel\
	                 \nAlignment Quality: $::ALIGN::gss_align_src\
	                 \n$title\n"

	    foreach row {1 2 3} {
	        set msg_txt "$msg_txt$row_id($row)\t"
	        foreach col {1 2 3} {      
	            set msg_txt "$msg_txt[format %+14.7E%s $::ALIGN::gss_dcm($sensor,$row,$col) \t]"
	        }
	        set msg_txt "$msg_txt\n"
	    }
	    
	}

	::MSG_DLG::msg_log $msg_txt

}
    
########################################################################
#	load the DCMs from INI file.

proc ::ALIGN::set_dcms { } {
	global gi

	set pwr_comb(12) 1
	set pwr_comb(23) 2
	set pwr_comb(13) 3
	set pwr_comb(123) 4
	
	set sxid(ST_1) 0
	set sxid(ST_2) 1
	set sxid(ST_3) 2
	set sxid(SS) 3
	
	variable sensor_id

	#	Set the STA 2 Body DCM for the appropriate power on config
	set data [$gi PWR_$::CONFIG::data(sts_active)\_STA_DCM {} ]

	if { $data == {} } {
	    ::MSG_DLG::attention "No INI file entry found for\
	                          PWR_$::CONFIG::data(sts_active)\_STA_DCM" \
	                          red \
	                          black \
	                          -INSTRUCTION "Check GSS ini file contents"
	                          
	} else {
	    set cmd "SetSTA2Body $data"
	    eval $cmd
	    
	}
	
	#	Set the ST and Sun Sensor 2 STA DCM for the 
	#	appropriate power on config
	foreach sensor {ST_1 ST_2 ST_3 SS} {
	    set data [$gi \
	              PWR_$::CONFIG::data(sts_active)\_$sensor\_DCM {} ]

	    if { $data == {} } {
	        ::MSG_DLG::attention \
	               "No INI file entry found for\
	                PWR_$::CONFIG::data(sts_active)\_$sensor\_DCM" \
	                red \
	                black \
	                -INSTRUCTION "Check GSS ini file contents"
	                          
	    } else {
	        set cmd "SetSX2STA $sxid($sensor) $data"
	        eval $cmd
	    
	    }
	
	}

	#	Keep the SX2Body DCMs calculated from the INI file
	foreach sensor {1 2 3 sun} {
	    set data [GetSX2BodyDCM_Data $sensor_id($sensor)]
#x	    set ::ALIGN::ini_SX2BodyDCM($sensor,1,1) \
#x	        [format "%7.5f" [lindex $data 0]]
	    set ::ALIGN::ini_SX2BodyDCM($sensor,1,1) \
	        [format "%+9.7f" [lindex $data 0]]
	    set ::ALIGN::ini_SX2BodyDCM($sensor,1,2) \
	        [format "%+9.7f" [lindex $data 1]]
	    set ::ALIGN::ini_SX2BodyDCM($sensor,1,3) \
	        [format "%+9.7f" [lindex $data 2]]
	    set ::ALIGN::ini_SX2BodyDCM($sensor,2,1) \
	        [format "%+9.7f" [lindex $data 3]]
	    set ::ALIGN::ini_SX2BodyDCM($sensor,2,2) \
	        [format "%+9.7f" [lindex $data 4]]
	    set ::ALIGN::ini_SX2BodyDCM($sensor,2,3) \
	        [format "%+9.7f" [lindex $data 5]]
	    set ::ALIGN::ini_SX2BodyDCM($sensor,3,1) \
	        [format "%+9.7f" [lindex $data 6]]
	    set ::ALIGN::ini_SX2BodyDCM($sensor,3,2) \
	        [format "%+9.7f" [lindex $data 7]]
	    set ::ALIGN::ini_SX2BodyDCM($sensor,3,3) \
	        [format "%+9.7f" [lindex $data 8]]
	    
	}

	#	Initialize the body object boresight DCMs
	foreach obj $::BODY_OBJS::body_obj_list {
	    set uc [string toupper $obj]
	    set id $::BODY_OBJS::body_obj($obj,id)
	    
	    if {$obj == "sun_sensor"} {
	        set data [$gi \
	                  "PWR_$::CONFIG::data(sts_active)\_SS_DCM" \
	                  {}]

	    } else {
	        set data [$gi "$uc\_DCM" {} ]
	        
	    }
	    
	    set cmd "SetCircularZoneDCM $id $data"
	    eval $cmd

	}
	
			
	#	Initialize the DCM for the Instrument Optical FOV
	    set data [$gi "INSTRUMENT_OPTICAL_DCM" {} ]
	    set cmd "SetInstOptDCM $data"
	    eval $cmd

	#	Inform CIS as to the power on combination
	SetPwrOn $pwr_comb($::CONFIG::data(sts_active))
	
	#	Inform CIS as to the quality of the alignments
	
	ChooseAlignment

}

########################################################################
#	$Log: $
########################################################################
