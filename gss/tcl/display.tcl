########################################################################
#	$Id: $
########################################################################
#	common DISPLAY package

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ::DISPLAY:: {
	#	DISPLAYS (skymap plus three STs)
	variable displays
	variable up_down
	variable objs {}

	#	ST FRAME COLORS
	variable st_colors

	#	STAR COLORS
	variable star_colors

	#	canvas points
	variable x0 [GPOINT]
	variable x1 [GPOINT]

	#	circle points
	variable cstep 10
	variable cpoints
	
	#  	id of star whose info box was displayed	
	variable display_id
	variable list_display_id
	variable idx_display_id
	variable number_of_displays
	variable total_displays

	namespace export init draw update plot_obj pcircle star	\
			grid_line arrow dbl spawn_frame object

}

########################################################################

proc ::DISPLAY::init { } {
	#	ST FRAME COLORS
	variable st_colors
	variable star_colors
	variable display_id
	variable list_display_id
	variable idx_display_id
	variable number_of_displays
	variable total_displays
	global gi

	set st_colors(1) [$gi ST_1_FOV_COLOR red]
	set st_colors(2) [$gi ST_2_FOV_COLOR yellow]
	set st_colors(3) [$gi ST_3_FOV_COLOR blue]

	#	STAR MAGNITUDE COLORS (LEGEND ONLY)
	variable colors
	set star_colors(asc) red
	set star_colors(osc) white

	#	STAR COLORS
	set star_colors(0) white
	set star_colors(1) red
	set star_colors(2) orange
	set star_colors(3) yellow
	set star_colors(4) green
	set star_colors(5) blue
	set star_colors(6) purple

	#	precompute $cstep points around a circle
	variable cpoints
	variable cstep
	global DTR
	set k [expr 360 / $cstep * $DTR]
	for {set i 0} { $i < $cstep } {incr i } {
		set th [expr $i * $k]
		set t {}
		lappend t [expr cos($th)]
		lappend t [expr sin($th)]
		lappend cpoints $t
		}

	#	set hide/display cmd for major groupings
	#	boolean is inverted because most tests are easier to
	#	write that way.

	variable hide_display
	set hide_display(0) raise
	set hide_display(1) lower

	variable up_down
	set up_down(0) raise
	set up_down(1) lower
	
	#	Star Info Box bookkeeping
	set number_of_displays 0
	set total_displays 50
	for {set i 1} { $i < 65536 } {incr i } {	
	    set display_id($i) "FALSE"
	    set idx_display_id($i) 0
	}
	foreach i $::EXT_OBJS::ext_obj_list {
	    set display_id($i) "FALSE"
	    set idx_display_id($i) 0
	}
	for {set i 1} { $i < $total_displays } {incr i } {	
	    set list_display_id($i) ""
	}

}

########################################################################
#	Resolve the color of a star depending on its magnitude

proc ::DISPLAY::resolve_star_color { mag } {
	variable star_colors

	if { $mag < 4.8 } {
	    return $star_colors(0)
	    
	} elseif { ($mag >= 4.8) && ($mag < 5.3) } {
	    return $star_colors(1)
	    
	} elseif { ($mag >= 5.3)  && ($mag < 5.7) } {
	    return $star_colors(2)
	    
	} elseif { ($mag >= 5.7) && ($mag < 6.0) } {
	    return $star_colors(3)
	    
	} elseif { ($mag >= 6.0)  && ($mag < 6.2) } {
	    return $star_colors(4)
	    
	} elseif { ($mag >= 6.2)  && ($mag < 6.3) } {
	    return $star_colors(5)
	    
	} elseif { $mag >= 6.3 } {
	    return $star_colors(6)

	}
	
}

########################################################################
#	draw star tracker fov

proc ::DISPLAY::st_fov { stnum } {
	#	four lines w/white bottom
	#	probably NOT at right angles or equal length, so no rect.
	#	white bottom prevents faster polygon call.
	#	white bottom in first, from left to right in HV.
	#		then up, left, and back to origin.
	#	V axis is drawn backwards, so use -first for its arrow.

	global gi

	set vcolor [$gi ST_$stnum\_FOV_COLOR green]
	set hcolor [$gi ST_H_COLOR_AXIS white]

	$::ST::st_canv($stnum) create line \
	                      $::ST::st_FOV_corner(brx) \
	                      $::ST::st_FOV_corner(bry) \
	                      $::ST::st_FOV_corner(blx) \
	                      $::ST::st_FOV_corner(bly) \
	                      -fill $hcolor \
	                      -arrow last \
	                      -tags "st_fov"

	$::ST::st_canv($stnum) create line \
	                      $::ST::st_FOV_corner(blx) \
	                      $::ST::st_FOV_corner(bly) \
	                      $::ST::st_FOV_corner(tlx) \
	                      $::ST::st_FOV_corner(tly) \
	                      -fill $vcolor \
	                      -tags "st_fov"

	$::ST::st_canv($stnum) create line \
	                      $::ST::st_FOV_corner(tlx) \
	                      $::ST::st_FOV_corner(tly) \
	                      $::ST::st_FOV_corner(trx) \
	                      $::ST::st_FOV_corner(try) \
	                      -fill $vcolor \
	                      -tags "st_fov"

	$::ST::st_canv($stnum) create line \
	                      $::ST::st_FOV_corner(trx) \
	                      $::ST::st_FOV_corner(try) \
	                      $::ST::st_FOV_corner(brx) \
	                      $::ST::st_FOV_corner(bry) \
	                      -fill $vcolor \
	                      -arrow first \
	                      -tags "st_fov"

	#	title
	$::ST::st_canv($stnum) create text \
	                       $::ST::st_FOV_zero(x) \
	                       [expr $::ST::st_pxpdeg(y) * 0.25] \
	                       -text "ST $stnum" \
	                       -fill white \
	                       -anchor c \
	                       -font $::CONFIG::fonts(axis) \
	                       -tags "st_fov"

	#	axis labels
	$::ST::st_canv($stnum) create text \
	                       [expr $::ST::st_pxpdeg(x) * 0.25] \
	                       $::ST::st_FOV_corner(bly) \
	                       -text {+H} \
	                       -fill white \
	                       -anchor c \
	                       -font $::CONFIG::fonts(axis) \
	                       -tags "st_fov"

	$::ST::st_canv($stnum) create text \
	                       $::ST::st_FOV_corner(trx) \
	                       [expr $::ST::st_pxpdeg(y) * 0.25] \
	                       -text {+V} \
	                       -fill white \
	                       -anchor c \
	                       -font $::CONFIG::fonts(axis) \
	                       -tags "st_fov"

	#	grid labels (labels always shown whether grid is or not)
	set textx [expr $::ST::st_pxpdeg(x) * 8.75]
	set texty [expr $::ST::st_pxpdeg(y) * 8.75]
	
	foreach ang {4 3 2 1 0 -1 -2 -3 -4} {
	    set angx [expr $::ST::st_FOV_zero(x) - \
	                   ($ang * $::ST::st_pxpdeg(x))]
	    set angy [expr $::ST::st_FOV_zero(y) - \
	                   ($ang * $::ST::st_pxpdeg(y))]
	                   
	    $::ST::st_canv($stnum) create text \
	                           $angx \
	                           $texty \
	                           -text "$ang" \
	                           -fill white \
	                           -anchor c \
	                           -font $::CONFIG::fonts(axis) \
	                           -tags "st_fov"
	    $::ST::st_canv($stnum) create text \
	                           $textx \
	                           $angy \
	                           -text "$ang" \
	                           -fill white \
	                           -anchor c \
	                           -font $::CONFIG::fonts(axis) \
	                           -tags "st_fov"

	    }

	    $::ST::st_canv($stnum) raise st_fov st_FOV_plane

	#	Display grid
	::DISPLAY::st_grid $stnum	

}
########################################################################
#       Object Info box (remove)

proc ::DISPLAY::objinfo_remove { obj } {

        destroy .objinfo_$obj
        
        $::SKYMAP::sky_canv delete sky_label_$obj
        
        #	Info Box bookkeeping
	if { $::DISPLAY::display_id($obj) == "TRUE"} {
	        set ::DISPLAY::display_id($obj) "FALSE"
	        set start_i $::DISPLAY::idx_display_id($obj)
	        for {set i $start_i} { $i < $::DISPLAY::number_of_displays } {incr i } {
	        	set next_i [expr $i + 1]	
			set ::DISPLAY::list_display_id($i) $::DISPLAY::list_display_id($next_i)
			set o $::DISPLAY::list_display_id($i)
	        	incr ::DISPLAY::idx_display_id($o) -1
		}
	        incr ::DISPLAY::number_of_displays -1
	}
	      
}

########################################################################
#	Star Label

proc ::DISPLAY::st_star_label { st h v mag osc bssid } {
	global gi
	
	set star_rad [$gi STAR_SIZE 0.1]
	set star_off_px 9
	set star_offset [expr ($::ST::st_pxpdeg(x) * $star_rad) + $star_off_px ]

	set h_px [expr $::ST::st_FOV_zero(x) - ($::ST::st_pxpdeg(x) * $h)]
        set v_px [expr $::ST::st_FOV_zero(y) - ($::ST::st_pxpdeg(y) * $v)]
                       
	set taglst  "star not_osc mag_[format "%3.1f" $mag]" 
	if {$osc == 1} {
	        set taglst "star in_osc mag_[format "%3.1f" $mag]"
	}
	    
	set id2 [$::ST::st_canv($st) create text $h_px [expr $v_px + $star_offset] \
            	-text $bssid \
             	-fill white \
             	-anchor c \
             	-justify center \
             	-font $::CONFIG::fonts(st_vt) \
             	-tags "$taglst"]
        $::ST::st_canv($st) raise $id2 st_star_plane
	           
}


########################################################################
#	Object Label

proc ::DISPLAY::st_obj_label { st h v obj label} {
	global gi
	
	set uc [string toupper $obj]
	
	set obj_rad [$gi [set uc]_SIZE 0.1]
	set obj_off_px 9
	set obj_offset [expr ($::ST::st_pxpdeg(x) * $obj_rad) + $obj_off_px ]

	set h_px [expr $::ST::st_FOV_zero(x) - ($::ST::st_pxpdeg(x) * $h)]
        set v_px [expr $::ST::st_FOV_zero(y) - ($::ST::st_pxpdeg(y) * $v)]
	    
	set id2 [$::ST::st_canv($st) create text $h_px [expr $v_px + $obj_offset] \
            	-text $label \
             	-fill white \
             	-anchor c \
             	-justify center \
             	-font $::CONFIG::fonts(st_vt) \
             	-tags "ephem"]
        $::ST::st_canv($st) raise $id2 st_ext_plane
	           
}


########################################################################
#	Star Info box (enter)

proc ::DISPLAY::st_star_pop { st star h v mag sky2000id osc bssid } {

	set data [LookupStarInfo $star]
	set ra   [lindex $data 0]
	set dec  [lindex $data 1]
	
	if { $::DISPLAY::display_id($bssid) == "TRUE"} {
		destroy .objinfo_$bssid
	}
	
	set info_frm .objinfo_$bssid
	toplevel $info_frm
        wm title $info_frm "Star Info"
	
	set f [frame $info_frm.bss \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "BSS ID:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text "$bssid" \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width 10 \
	      -anchor e \
	      -justify right
	      
	 pack $f.lbl $f.lbl2 \
	      -side left \
	      -fill x

	set f [frame $info_frm.sky \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "SKY2000 ID:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format "%8.8d" $sky2000id] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width 10 \
	      -anchor e \
	      -justify right

	 pack $f.lbl $f.lbl2 \
	      -side left \
	      -fill x

	set f [frame $info_frm.ra \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "RA:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%8.3f} $ra] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width 10 \
	      -anchor e \
	      -justify right

	 pack $f.lbl $f.lbl2 \
	      -side left \
	      -fill x

	set f [frame $info_frm.dec \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "DEC:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%7.3f} $dec] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width 10 \
	      -anchor e \
	      -justify right

	pack $f.lbl $f.lbl2 \
	     -side left \
	     -fill x

	set f [frame $info_frm.h \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "ST [set st]  H:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%6.3f} $h] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width 10 \
	      -anchor e \
	      -justify right

	pack $f.lbl $f.lbl2 \
	     -side left \
	     -fill x

	set f [frame $info_frm.v \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x
	      
	label $f.lbl \
	      -text "ST [set st]  V:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%6.3f} $v] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width 10 \
	      -anchor e \
	      -justify right

	pack $f.lbl $f.lbl2 \
	     -side left \
	     -fill x

	set f [frame $info_frm.mag \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x
	     
	if { $mag < $::GSSINI::gssini(MAG_SATURATION) } {
	     set bgcolor yellow
	} else {
	     set bgcolor "gray90"
	}
	      
	label $f.lbl \
	      -text "Mag:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%5.3f} $mag] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background $bgcolor \
	      -width 10 \
	      -anchor e \
	      -justify right

	pack $f.lbl $f.lbl2 \
	     -side left \
	     -fill x

	set f [frame $info_frm.osc \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x
	                
	set osc_lbl "No"
	if {$osc == 1} { set osc_lbl "Yes" }
	
	label $f.lbl \
	      -text "OSC Star:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text "$osc_lbl" \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width 10 \
	      -anchor e \
	      -justify right

	pack $f.lbl $f.lbl2 \
	     -side left \
	     -fill x
	     
	set f [frame $info_frm.btn \
	             -borderwidth 1 \
                     -background gray70 \
	             -relief sunken]
                                       
        pack $f \
             -side top \
             -anchor w \
             -fill x

        button $f.btn -text OK \
                      -font $::CONFIG::fonts(entry) \
                      -foreground black \
                      -background gray90 \
                      -command "::DISPLAY::objinfo_remove $bssid"
        	              
        pack $f.btn -side top \
                    -pady 3    

	if {$bssid != 0} {
		
	#	Star Info Box bookkeeping
	    if { $::DISPLAY::display_id($bssid) == "FALSE"} {
	        set ::DISPLAY::display_id($bssid) "TRUE"
	        incr ::DISPLAY::number_of_displays
	        set ::DISPLAY::idx_display_id($bssid) [set ::DISPLAY::number_of_displays]
	        set ::DISPLAY::list_display_id([set ::DISPLAY::number_of_displays]) $bssid
	    }
	    
	    ::DISPLAY::st_star_label $st $h $v $mag $osc $bssid
	    ::DISPLAY::sky_star_label $ra $dec $mag $osc $bssid

        }
                      
}

########################################################################
#	Star Info box (enter)

proc ::DISPLAY::st_object_pop { st obj name ra dec h v } {

	set name_len [string length $name]
	if {$name_len < 10} {set name_len 10}
	
	set label [lindex $name 0]
		
	if { $::DISPLAY::display_id($obj) == "TRUE"} {
		destroy .objinfo_$obj
	}
	
	set info_frm .objinfo_$obj
	toplevel $info_frm
        wm title $info_frm "Object Info"
	
	set f [frame $info_frm.object \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "Object:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text "$name" \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width $name_len \
	      -anchor e \
	      -justify right
	      
	 pack $f.lbl $f.lbl2 \
	      -side left \
	      -fill x

	set f [frame $info_frm.ra \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "RA:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%8.3f} $ra] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width $name_len \
	      -anchor e \
	      -justify right

	 pack $f.lbl $f.lbl2 \
	      -side left \
	      -fill x

	set f [frame $info_frm.dec \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "DEC:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%7.3f} $dec] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width $name_len \
	      -anchor e \
	      -justify right

	pack $f.lbl $f.lbl2 \
	     -side left \
	     -fill x

	set f [frame $info_frm.h \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "ST [set st]  H:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%6.3f} $h] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width $name_len \
	      -anchor e \
	      -justify right

	pack $f.lbl $f.lbl2 \
	     -side left \
	     -fill x

	set f [frame $info_frm.v \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x
	      
	label $f.lbl \
	      -text "ST [set st]  V:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%6.3f} $v] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width $name_len \
	      -anchor e \
	      -justify right

	pack $f.lbl $f.lbl2 \
	     -side left \
	     -fill x
	     
	set f [frame $info_frm.btn \
	             -borderwidth 1 \
                     -background gray70 \
	             -relief sunken]
                                       
        pack $f \
             -side top \
             -anchor w \
             -fill x

        button $f.btn -text OK \
                      -font $::CONFIG::fonts(entry) \
                      -foreground black \
                      -background gray90 \
                      -command "::DISPLAY::objinfo_remove $obj"
        	              
        pack $f.btn -side top \
                    -pady 3    
		
	#	Info Box bookkeeping
	if { $::DISPLAY::display_id($obj) == "FALSE"} {
	        set ::DISPLAY::display_id($obj) "TRUE"
	        incr ::DISPLAY::number_of_displays
	        set ::DISPLAY::idx_display_id($obj) [set ::DISPLAY::number_of_displays]
	        set ::DISPLAY::list_display_id([set ::DISPLAY::number_of_displays]) $obj
	}
	    
	::DISPLAY::st_obj_label $st $h $v $obj $label
                      
}


########################################################################
#	draw star tracker RA and DEC

proc ::DISPLAY::st_radec { stnum ra dec } {

	set ra_text [format "RA: %5.1f" $ra]
	set dec_text [format "DEC: %5.1f" $dec]
	
	#	RA 
	$::ST::st_canv($stnum) create text \
	                       [expr $::ST::st_pxpdeg(x) * 2.5] \
	                       [expr $::ST::st_pxpdeg(y) * 0.25] \
	                       -text $ra_text \
	                       -fill white \
	                       -anchor w \
	                       -font $::CONFIG::fonts(axis) \
	                       -tags "st_radec"

	#	DEC
	$::ST::st_canv($stnum) create text \
	                       [expr $::ST::st_pxpdeg(x) * 5.5] \
	                       [expr $::ST::st_pxpdeg(y) * 0.25] \
	                       -text $dec_text \
	                       -fill white \
	                       -anchor w \
	                       -font $::CONFIG::fonts(axis) \
	                       -tags "st_radec"

	$::ST::st_canv($stnum) lower st_radec st_FOV_plane

}


########################################################################
#	draw star tracker sms mode

proc ::DISPLAY::st_sms { stnum status color } {

	#	status 
	$::ST::st_canv($stnum) create text \
	                       5 \
	                       [expr $::ST::st_pxpdeg(y) * 0.25] \
	                       -text $status \
	                       -fill $color \
	                       -anchor w \
	                       -font $::CONFIG::fonts(axis) \
	                       -tags "st_sms"

	$::ST::st_canv($stnum) lower st_sms st_FOV_plane

}


########################################################################
#	draw star tracker grid

proc ::DISPLAY::st_grid { stnum } {
	
	foreach ang {3 2 1 0 -1 -2 -3} {
	    set color grey60
	    if {$ang == 0} {set color white}
	    set angx [expr $::ST::st_FOV_zero(x) - \
	                   ($ang * $::ST::st_pxpdeg(x))]
	    set angy [expr $::ST::st_FOV_zero(y) - \
	                   ($ang * $::ST::st_pxpdeg(y))]
	                   
	    $::ST::st_canv($stnum) create line \
	                          $angx \
	                          $::ST::st_FOV_corner(tly) \
	                          $angx \
	                          $::ST::st_FOV_corner(bly) \
	                          -fill $color \
	                          -tags st_grid

	    $::ST::st_canv($stnum) create line \
	                          $::ST::st_FOV_corner(tlx) \
	                          $angy \
	                          $::ST::st_FOV_corner(trx)\
	                          $angy \
	                          -fill $color \
	                          -tags st_grid

	    }

	    $::ST::st_canv($stnum) lower st_grid st_FOV_plane

	}


########################################################################
#	draw projection in Star Trackers

proc ::DISPLAY::st_object { st name type h v radius color } {
	global gi
	set rtd 57.295779513

	set coords {}

  	#	Convert h and v to pixels and calculate corners
	set h_px [expr $::ST::st_FOV_zero(x) - ($::ST::st_pxpdeg(x) * $h)]
 	set v_px [expr $::ST::st_FOV_zero(y) - ($::ST::st_pxpdeg(y) * $v)]
	set radius [expr $radius * $::ST::st_pxpdeg(x)]
	
	set tlx [expr $h_px - $radius]
	set tly [expr $v_px - $radius]
	set brx [expr $h_px + $radius]
	set bry [expr $v_px + $radius]
    
	#	Draw the circle	
	set id [$::ST::st_canv($st) create oval $tlx $tly $brx $bry \
	                        -fill $color \
		                -outline $color \
		                -tags "ephem"]

	set uc [string toupper $name]
	    
	if { ([string first "ASTEROID" $uc] != -1) || \
	     ([string first "COMET" $uc]    != -1) } {
		set object "[lindex [$gi $uc ?] 0]"
		set label $object
	} else {
		set object "$name"
		set label [string totitle $object]
	}
	    
	if {$type == "stayout"} {
       		$::ST::st_canv($st) lower $id "st_$name\_stayout_plane"
	        set long_name "$label Stayout Zone"
	} else {
	        $::ST::st_canv($st) lower $id "st_ext_plane"
	        set long_name "$label"
	}
    
    
	#       Label object if its corresponding Info box was displayed
	if { ($::DISPLAY::display_id($name) == "TRUE") } {
		::DISPLAY::st_obj_label $st $h $v $name $label

	}
    
	set ra  [expr $rtd * [ephem_get_ra  $name]]
	set dec [expr $rtd * [ephem_get_dec $name]]
    
	$::ST::st_canv($st) bind $id <Button-3> "::DISPLAY::st_object_pop \
	                                     $st \
	                                     \"$name\" \
	                                     \"$long_name\" \
	                                     $ra \
	                                     $dec \
	                                     $h \
	                                     $v"

}

########################################################################
#	draw Star tracker star

proc ::DISPLAY::st_stars { st star h v mag sky2000id osc bssid } {
	variable colors
	global gi
	
	set star_rad [$gi STAR_SIZE 0.1]

	#	Convert h and v to pixels and calculate corners
	set h_px [expr $::ST::st_FOV_zero(x) - \
	               ($::ST::st_pxpdeg(x) * $h)]
	set v_px [expr $::ST::st_FOV_zero(y) - \
	               ($::ST::st_pxpdeg(y) * $v)]
	set radius [expr $star_rad * $::ST::st_pxpdeg(x)]
	
	set tlx [expr $h_px - $radius]
	set tly [expr $v_px - $radius]
	set brx [expr $h_px + $radius]
	set bry [expr $v_px + $radius]

	set clr [::DISPLAY::resolve_star_color $mag]	
	set fillclr black
	set taglst  "star not_osc mag_[format "%3.1f" $mag]"
	
	if {$osc == 1} {
	    set fillclr $clr
	    set taglst "star in_osc mag_[format "%3.1f" $mag]"
	    
	}

	#	Draw the circle	
        set id [$::ST::st_canv($st) create oval $tlx $tly $brx $bry \
	                              -fill $fillclr \
		                      -outline $clr \
		                      -tags "$taglst"]
		              
        #       Label star if its corresponding Star Info box was displayed         		                      

        if { ($::DISPLAY::display_id($bssid) == "TRUE") } {
        	::DISPLAY::st_star_label $st $h $v $mag $osc $bssid
        }
       	
	$::ST::st_canv($st) bind $id <Button-3> "::DISPLAY::st_star_pop \
	                                     $st \
	                                     $star \
	                                     $h \
	                                     $v \
	                                     $mag \
	                                     $sky2000id \
	                                     $osc \
	                                     $bssid"

	$::ST::st_canv($st) lower $id st_star_plane
	       

}


########################################################################
#	draw Star tracker VTs

proc ::DISPLAY::st_vt { st vt h v mag bss_id track valid } {
	global gi
	
	set star_rad [$gi STAR_SIZE 0.1]
	set box_margin 0.05

	#	Convert h and v to pixels and calculate corners,
	#	if not tracking then place in rest position
	set radius [expr ($star_rad + $box_margin) * $::ST::st_pxpdeg(x)]
	if {$track == 1} {
	    set h_px [expr $::ST::st_FOV_zero(x) - \
	                   ($::ST::st_pxpdeg(x) * $h)]
	    set v_px [expr $::ST::st_FOV_zero(y) - \
	                   ($::ST::st_pxpdeg(y) * $v)]
	} else {
	    set h_px $::ST::st_vt_rest_x($vt)
	    set v_px $::ST::st_vt_rest_y($vt)
	    
	}
	
	set tlx [expr $h_px - $radius]
	set tly [expr $v_px - $radius]
	set brx [expr $h_px + $radius]
	set bry [expr $v_px + $radius]

	#	Draw the box	
        $::ST::st_canv($st) create line $brx $bry $tlx $bry \
	                           -fill white \
		                   -tags "st_vt"
        $::ST::st_canv($st) create line $tlx $bry $tlx $tly \
	                           -fill white \
		                   -tags "st_vt"
        $::ST::st_canv($st) create line $tlx $tly $brx $tly \
	                           -fill white \
		                   -tags "st_vt"
        $::ST::st_canv($st) create line $brx $tly $brx $bry \
	                           -fill white \
		                   -tags "st_vt"

	#	Mark whether valid or not
	if {$valid == 0 && $track == 1} {
            $::ST::st_canv($st) create line $tlx $tly $brx $bry \
	                               -fill grey60 \
	                               -width 2 \
		                           -tags "st_vt"
            $::ST::st_canv($st) create line $brx $tly $tlx $bry \
	                               -fill grey60 \
	                               -width 2 \
		                           -tags "st_vt"
		                       
	}
	
	#	Annotate
	if {$track == 1} {
	    set txt_x [expr $tlx - (0.10 * $::ST::st_pxpdeg(x))]
	    set txt_y $v_px

	    $::ST::st_canv($st) create text $txt_x $txt_y \
	                        -text $vt \
	                        -fill white \
	                        -anchor c \
	                        -justify center \
	                        -font $::CONFIG::fonts(st_vt) \
	                        -tags "st_vt"


	    set txt_x [expr $brx + (0.30 * $::ST::st_pxpdeg(x))]
	    
	    if { $mag < $::GSSINI::gssini(MAG_SATURATION) } {
	         set textcolor yellow
	    } else {
	         set textcolor white
	    }

	    $::ST::st_canv($st) create text $txt_x $txt_y \
	                        -text [format "%3.1f" $mag] \
	                        -fill $textcolor \
	                        -anchor c \
	                        -justify center \
	                        -font $::CONFIG::fonts(st_vt) \
	                        -tags "st_vt"
	                        
	    set txt_x $h_px
	    set txt_y [expr $tly - (0.20 * $::ST::st_pxpdeg(y))]

	    if {$bss_id != 0} {
	        $::ST::st_canv($st) create text $txt_x $txt_y \
	                            -text $bss_id \
	                            -fill white \
	                            -anchor c \
	                            -justify center \
	                            -font $::CONFIG::fonts(st_vt) \
	                            -tags "st_vt"
	                            
	    }

	} else {
	    $::ST::st_canv($st) create text $h_px $v_px \
	                        -text $vt \
	                        -fill white \
	                        -anchor c \
	                        -justify center \
	                        -font $::CONFIG::fonts(st_vt) \
	                        -tags "st_vt"
	}	  

	$::ST::st_canv($st) lower st_vt st_FOV_plane
        

}


########################################################################
#	draw centroid.

proc ::DISPLAY::st_centroid { st h v } {
	global gi

	set radius [$gi CENTROID_SIZE 5]
	set iniclr [$gi CENTROID_COLOR green]

	set tlx [expr $::ST::st_FOV_zero(x) - \
	              ($h * $::ST::st_pxpdeg(x)) - $radius]
	set tly [expr $::ST::st_FOV_zero(y) - \
	              ($v * $::ST::st_pxpdeg(y)) - $radius]
	set brx [expr $::ST::st_FOV_zero(x) - \
	              ($h * $::ST::st_pxpdeg(x)) + $radius]
	set bry [expr $::ST::st_FOV_zero(y) - \
	              ($v * $::ST::st_pxpdeg(y)) + $radius]

	set arc "$::ST::st_canv($st) create arc $tlx $tly $brx $bry"
	set opts [list -extent 90.0 \
	               -style pieslice \
	               -outline $iniclr \
	               -tags st_vt]
	set angs { 0.0 90.0 180.0 270.0 }
	set colors " black $iniclr black $iniclr "
	
	foreach {ang} $angs {color} $colors {
		eval "$arc -start $ang -fill $color $opts"
	}

        foreach radius {1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0} {

	    set tlx [expr $::ST::st_FOV_zero(x) - \
	                  (($h - $radius) * $::ST::st_pxpdeg(x))]
	    set tly [expr $::ST::st_FOV_zero(y) - \
	                  (($v - $radius) * $::ST::st_pxpdeg(y))]
	    set brx [expr $::ST::st_FOV_zero(x) - \
	                  (($h + $radius) * $::ST::st_pxpdeg(x))]
	    set bry [expr $::ST::st_FOV_zero(y) - \
	                  (($v + $radius) * $::ST::st_pxpdeg(y))]
	                  
	    #	Draw the circle	
            $::ST::st_canv($st) create oval $tlx $tly $brx $bry \
		                            -outline $iniclr \
		                            -tags st_vt
		                            
	}
	    
        
}


########################################################################
#	draw Star Tracker North/South Arrow

proc ::DISPLAY::st_ns_arrow { st nh nv sh sv color } {
	variable colors

	#	Convert h and v to pixels
	set nh_px [expr $::ST::st_FOV_zero(x) - \
	                ($::ST::st_pxpdeg(x) * $nh)]
	set nv_px [expr $::ST::st_FOV_zero(y) - \
	                ($::ST::st_pxpdeg(y) * $nv)]
	set sh_px [expr $::ST::st_FOV_zero(x) - \
	                ($::ST::st_pxpdeg(x) * $sh)]
	set sv_px [expr $::ST::st_FOV_zero(y) - \
	                ($::ST::st_pxpdeg(y) * $sv)]
	                
	set nslabel_offset 6
	
	set ntos_h_dpx [expr $nh_px - $sh_px]
	set ntos_v_dpx [expr $nv_px - $sv_px]
	set ntos_norm [expr sqrt ($ntos_h_dpx*$ntos_h_dpx + $ntos_v_dpx*$ntos_v_dpx)]
	if { $ntos_norm < 1 } {
		set ntos_norm 113.0
	}
	set ntos_h_inc [expr $nslabel_offset* ($ntos_h_dpx / $ntos_norm)]
	set ntos_v_inc [expr $nslabel_offset* ($ntos_v_dpx / $ntos_norm)]
	
	set nlabel_h_px [expr $nh_px + $ntos_h_inc]
	set nlabel_v_px [expr $nv_px + $ntos_v_inc]
	set slabel_h_px [expr $sh_px - $ntos_h_inc]
	set slabel_v_px [expr $sv_px - $ntos_v_inc]

	#	Draw the line
        $::ST::st_canv($st) create line \
                                    $nh_px $nv_px $sh_px $sv_px \
	                            -fill $color \
	                            -arrow first \
		                    -tags "st_ns"

	#	Print the text
	$::ST::st_canv($st) create text $nlabel_h_px $nlabel_v_px \
	                        -text "N" \
	                        -fill white \
	                        -anchor c \
	                        -justify center \
	                        -font $::CONFIG::fonts(label) \
	                        -tags "st_ns"
	$::ST::st_canv($st) create text $slabel_h_px $slabel_v_px \
	                        -text "S" \
	                        -fill white \
	                        -anchor c \
	                        -justify center \
	                        -font $::CONFIG::fonts(label) \
	                        -tags "st_ns"

	$::ST::st_canv($st) lower st_ns st_body_plane

}


########################################################################
#	draw North or South Pole.

proc ::DISPLAY::st_ns_sym { st txt h v color} {

	set h_px [expr $::ST::st_FOV_zero(x) - \
	               ($::ST::st_pxpdeg(x) * $h)]
	set v_px [expr $::ST::st_FOV_zero(y) - \
	               ($::ST::st_pxpdeg(y) * $v)]


	#	Calculate the circle
	set tlx [expr $h_px - 8]
	set tly [expr $v_px - 8]
	set brx [expr $h_px + 8]
	set bry [expr $v_px + 8]
	              
	#	Draw the circle	
        $::ST::st_canv($st) create oval $tlx $tly $brx $bry \
		                      -outline $color \
		                      -tags st_ns

	#	Print the text
	$::ST::st_canv($st) create text $h_px $v_px \
	                        -text $txt \
	                        -fill white \
	                        -anchor c \
	                        -justify center \
	                        -font $::CONFIG::fonts(label) \
	                        -tags "st_ns"

	$::ST::st_canv($st) lower st_ns st_body_plane

}

########################################################################
#	draw S/C body axis arrow

proc ::DISPLAY::st_body_axis {st axis h_frac v_frac length color} {

	set x_px [expr $::ST::st_FOV_zero(x) - \
	               ( $::ST::st_pxpdeg(x) *($h_frac * $length) )]
	set y_px [expr $::ST::st_FOV_zero(y) - \
	               ( $::ST::st_pxpdeg(y) *($v_frac * $length) )]

	$::ST::st_canv($st) create text \
	                           $x_px \
	                           $y_px \
	                           -text [string toupper $axis] \
	                           -font $::CONFIG::fonts(wtitle) \
	                           -fill $color \
	                           -tags "st_$axis"

	$::ST::st_canv($st) create line \
	                           $::ST::st_FOV_zero(x) \
	                           $::ST::st_FOV_zero(y) \
	                           $x_px \
	                           $y_px \
	                           -fill $color \
	                           -arrow last \
	                           -tags "st_$axis"
	                           
}


########################################################################
#	draw Skymap fov

proc ::DISPLAY::sky_fov {  } {

	#	Unlike the Star Tracker FOV routine, the grid axis 
	#	labels must be drawn with the grid.  This is because
	#	the grid spacing is dynamic depending on zoom factor.

	#	Display grid
	::DISPLAY::sky_grid

	#	Display skymap center
	::DISPLAY::sky_center $::SKYMAP::disp_center(x) 8

	
}


########################################################################
#	draw Skymap center position annotation

proc ::DISPLAY::sky_center { horz vert } {

	set txt [format "RA: %5.1f  DEC: %5.1f" \
	                $::SKYMAP::sky_canv_ra \
	                $::SKYMAP::sky_canv_dec]
	                
	$::SKYMAP::sky_canv delete sky_center_pos
	set id [$::SKYMAP::sky_canv create text \
	                            $horz \
	                            $vert \
	                            -text $txt \
	                            -font $::CONFIG::fonts(entry) \
	                            -fill white \
	                            -anchor c \
	                            -justify center \
	                            -tags sky_center_pos]
	                            
	$::SKYMAP::sky_canv raise sky_center_pos sky_FOV_plane

}


########################################################################
#	Star Info box (enter)

proc ::DISPLAY::sky_star_label { ra dec mag osc bssid } {
	global gi
	
	set star_rad [$gi STAR_SIZE 0.1]
	set star_off_px 7
	set star_offset [expr ($::SKYMAP::sky_pxpdeg(x) * $star_rad) + $star_off_px ]

	set ra_px  [expr $::SKYMAP::sky_zero(x) - ($::SKYMAP::sky_pxpdeg(x) * $ra)]
	set dec_px [expr $::SKYMAP::sky_zero(y) - ($::SKYMAP::sky_pxpdeg(y) * $dec)]

	set taglst  "sky_label_$bssid star not_osc mag_[format "%3.1f" $mag]"	
	if {$osc == 1} {
	        set taglst "sky_label_$bssid star in_osc mag_[format "%3.1f" $mag]"    
	}
                                             	
	set id2 [$::SKYMAP::sky_canv create text $ra_px [expr $dec_px + $star_offset] \
            	-text $bssid \
             	-fill white \
            	-anchor c \
             	-justify center \
            	-font $::CONFIG::fonts(st_vt) \
             	-tags "$taglst"]
             	
        if { ($::SKYMAP::sky_ui(stars) == "normal") && \
             (($osc == 1) || ($::SKYMAP::sky_ui(osc) == "ALL") ) && \
             ($mag <=  $::SKYMAP::sky_ui(mag))} {
		$::SKYMAP::sky_canv raise $id2 "sky_star_plane"
	} else {
		$::SKYMAP::sky_canv lower $id2 "sky_mask"
	}
           
}

########################################################################
#	Object Info box (enter)

proc ::DISPLAY::sky_obj_label { ra dec obj label } {
	global gi
	
	set uc [string toupper $obj]

	set obj_rad [$gi [set uc]_SIZE 0.1]
	set obj_off_px 7
	set obj_offset [expr ($::SKYMAP::sky_pxpdeg(x) * $obj_rad) + $obj_off_px ]

	set ra_px  [expr $::SKYMAP::sky_zero(x) - ($::SKYMAP::sky_pxpdeg(x) * $ra)]
	set dec_px [expr $::SKYMAP::sky_zero(y) - ($::SKYMAP::sky_pxpdeg(y) * $dec)]
                                             
        set taglst  "sky_label_$obj $obj"	
                            	                 	
	set id2 [$::SKYMAP::sky_canv create text $ra_px [expr $dec_px + $obj_offset] \
            	-text $label \
             	-fill white \
            	-anchor c \
             	-justify center \
            	-font $::CONFIG::fonts(st_vt) \
             	-tags "$taglst"]
	$::SKYMAP::sky_canv raise $id2 "sky_ext_plane"
           
}

########################################################################
#	Star Info box (enter)

proc ::DISPLAY::sky_star_pop { star ra dec mag sky2000id osc bssid } {

        if { $::DISPLAY::display_id($bssid) == "TRUE"} {
		destroy .objinfo_$bssid
	}
	
	set info_frm .objinfo_$bssid
	toplevel $info_frm
        wm title $info_frm "Star Info"

	set f [frame $info_frm.bss \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "BSS ID:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text "$bssid" \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width 10 \
	      -anchor e \
	      -justify right
	      
	 pack $f.lbl $f.lbl2 \
	      -side left \
	      -fill x

	set f [frame $info_frm.sky \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "SKY2000 ID:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format "%8.8d" $sky2000id] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width 10 \
	      -anchor e \
	      -justify right

	 pack $f.lbl $f.lbl2 \
	      -side left \
	      -fill x

	set f [frame $info_frm.ra \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "RA:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%8.3f} $ra] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width 10 \
	      -anchor e \
	      -justify right

	 pack $f.lbl $f.lbl2 \
	      -side left \
	      -fill x

	set f [frame $info_frm.dec \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "DEC:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%7.3f} $dec] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width 10 \
	      -anchor e \
	      -justify right

	pack $f.lbl $f.lbl2 \
	     -side left \
	     -fill x

	set f [frame $info_frm.mag \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x
	     
	if { $mag < $::GSSINI::gssini(MAG_SATURATION) } {
	     set bgcolor yellow
	} else {
	     set bgcolor "gray90"
	}
	      
	label $f.lbl \
	      -text "Mag:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%5.3f} $mag] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background $bgcolor \
	      -width 10 \
	      -anchor e \
	      -justify right

	pack $f.lbl $f.lbl2 \
	     -side left \
	     -fill x

	set f [frame $info_frm.osc \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x
	                
	set osc_lbl "No"
	if {$osc == 1} { set osc_lbl "Yes" }
	
	label $f.lbl \
	      -text "OSC Star:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text "$osc_lbl" \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width 10 \
	      -anchor e \
	      -justify right

	pack $f.lbl $f.lbl2 \
	     -side left \
	     -fill x
	    
        set f [frame $info_frm.btn \
        		-borderwidth 1 \
			-background gray70 \
			-relief sunken]
                                       
        pack $f \
             -side top \
             -anchor w \
             -fill x

        button $f.btn -text OK \
                      -font $::CONFIG::fonts(entry) \
                      -foreground black \
                      -background gray90 \
                      -command "::DISPLAY::objinfo_remove $bssid"
        	              
        pack $f.btn -side top \
                    -pady 3    
                                                                       	
	if {$bssid != 0} {
	    #	Star Info Box bookkeeping
	    if { $::DISPLAY::display_id($bssid) == "FALSE"} {
	        set ::DISPLAY::display_id($bssid) "TRUE"
	        incr ::DISPLAY::number_of_displays
	        set ::DISPLAY::idx_display_id($bssid) [set ::DISPLAY::number_of_displays]
	        set ::DISPLAY::list_display_id([set ::DISPLAY::number_of_displays]) $bssid
	    }
	    
	    ::DISPLAY::sky_star_label $ra $dec $mag $osc $bssid

        }
           
}

########################################################################
#	Skymap Object Info box 

proc ::DISPLAY::sky_object_pop { obj name ra dec } {

	set name_len [string length $name]
 	if {$name_len < 10} {set name_len 10}
 	
 	set label [lindex $name 0]

        if { $::DISPLAY::display_id($obj) == "TRUE"} {
		destroy .objinfo_$obj
	}
	
	set info_frm .objinfo_$obj
	toplevel $info_frm
        wm title $info_frm "Object Info"

	set f [frame $info_frm.object \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "Object:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text "$name" \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width $name_len \
	      -anchor e \
	      -justify right
	      
	 pack $f.lbl $f.lbl2 \
	      -side left \
	      -fill x

	set f [frame $info_frm.ra \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "RA:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%8.3f} $ra] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width $name_len \
	      -anchor e \
	      -justify right

	 pack $f.lbl $f.lbl2 \
	      -side left \
	      -fill x

	set f [frame $info_frm.dec \
	             -borderwidth 1 \
	             -relief sunken]
	pack $f \
	     -side top \
	     -fill x

	label $f.lbl \
	      -text "DEC:" \
	      -font $::CONFIG::fonts(label) \
	      -foreground black \
	      -background gray70 \
	      -width 11 \
	      -anchor e \
	      -justify right
	label $f.lbl2 \
	      -text [format {%7.3f} $dec] \
	      -font $::CONFIG::fonts(entry) \
	      -foreground black \
	      -background gray90 \
	      -width $name_len \
	      -anchor e \
	      -justify right

	pack $f.lbl $f.lbl2 \
	     -side left \
	     -fill x
	    
        set f [frame $info_frm.btn \
        		-borderwidth 1 \
			-background gray70 \
			-relief sunken]
                                       
        pack $f \
             -side top \
             -anchor w \
             -fill x

        button $f.btn -text OK \
                      -font $::CONFIG::fonts(entry) \
                      -foreground black \
                      -background gray90 \
                      -command "::DISPLAY::objinfo_remove $obj"
        	              
        pack $f.btn -side top \
                    -pady 3    
                                                                       	
	#	Info Box bookkeeping
	if { $::DISPLAY::display_id($obj) == "FALSE"} {
	        set ::DISPLAY::display_id($obj) "TRUE"
	        incr ::DISPLAY::number_of_displays
	        set ::DISPLAY::idx_display_id($obj) [set ::DISPLAY::number_of_displays]
	        set ::DISPLAY::list_display_id([set ::DISPLAY::number_of_displays]) $obj
	}
	    
	::DISPLAY::sky_obj_label $ra $dec $obj $label

           
}

########################################################################
#	draw Skymap grid

proc ::DISPLAY::sky_grid { } {

	#	Set start/end/incr by the size of zoom
	set ra_min [lindex $::SKYMAP::sky_grid_incr(ra,$::SKYMAP::sky_zf) 0]
	set ra_max [lindex $::SKYMAP::sky_grid_incr(ra,$::SKYMAP::sky_zf) 1]
	set ra_inc [lindex $::SKYMAP::sky_grid_incr(ra,$::SKYMAP::sky_zf) 2]
	set dec_min [lindex $::SKYMAP::sky_grid_incr(dec,$::SKYMAP::sky_zf) 0]
	set dec_max [lindex $::SKYMAP::sky_grid_incr(dec,$::SKYMAP::sky_zf) 1]
	set dec_inc [lindex $::SKYMAP::sky_grid_incr(dec,$::SKYMAP::sky_zf) 2]

	#	Set position for grid labels
	set text_x [expr (.15 * $ra_inc) * $::SKYMAP::sky_pxpdeg(x)]
	set text_y [expr $::SKYMAP::sky_maxy - \
	                 ( (.15 * $dec_inc) * $::SKYMAP::sky_pxpdeg(y)) ]

	#	leave space for grid labels 
	set offset_lx [expr (0.3 * $ra_inc) * $::SKYMAP::sky_pxpdeg(x)]
	set offset_rx $::SKYMAP::sky_maxx
	set offset_ty 1.0
	set offset_by [expr $::SKYMAP::sky_maxy - \
	                    ( (0.3 * $dec_inc) * $::SKYMAP::sky_pxpdeg(y)) ]

	#	Delete the old grid markings
	$::SKYMAP::sky_canv delete sky_grid
	
	#	Draw the new one
	for {set ang $ra_min} {$ang <= $ra_max} {incr ang $ra_inc} {
	    set angx [expr $::SKYMAP::sky_zero(x) - \
	                   ($ang * $::SKYMAP::sky_pxpdeg(x))]
	                   
	    $::SKYMAP::sky_canv create line \
	                          $angx \
	                          $offset_ty \
	                          $angx \
	                          $offset_by \
	                          -fill grey60 \
	                          -tags sky_grid

	    $::SKYMAP::sky_canv create text \
	                               $angx \
	                               $text_y \
	                               -text "$ang" \
	                               -fill grey90 \
	                               -anchor c \
	                               -font $::CONFIG::fonts(axis) \
	                               -tags sky_grid

	}
	
	for {set ang $dec_min} {$ang <= $dec_max} {incr ang $dec_inc} {
	    set angy [expr $::SKYMAP::sky_zero(y) - \
	                   ($ang * $::SKYMAP::sky_pxpdeg(y))]
	                   
	    $::SKYMAP::sky_canv create line \
	                          $offset_lx\
	                          $angy\
	                          $offset_rx\
	                          $angy \
	                          -fill grey60 \
	                          -tags sky_grid

	    $::SKYMAP::sky_canv create text \
	                               $text_x \
	                               $angy \
	                               -text "$ang" \
	                               -fill grey90 \
	                               -anchor c \
	                               -font $::CONFIG::fonts(axis) \
	                               -tags sky_grid

	}

	$::SKYMAP::sky_canv lower sky_grid sky_FOV_plane
	
}


########################################################################
#	draw Skymap star

proc ::DISPLAY::sky_star { star ra dec mag sky2000id osc bssid state} {
	variable colors
	global gi
	
	set star_rad [$gi STAR_SIZE 0.1]

	#	Convert ra and dec to pixels and calculate corners
	set ra_px [expr $::SKYMAP::sky_zero(x) - \
	                ($::SKYMAP::sky_pxpdeg(x) * $ra)]
	set dec_px [expr $::SKYMAP::sky_zero(y) - \
	                 ($::SKYMAP::sky_pxpdeg(y) * $dec)]
	set radius [expr $star_rad * $::SKYMAP::sky_pxpdeg(x)]
	
	set tlx [expr $ra_px - $radius]
	set tly [expr $dec_px - $radius]
	set brx [expr $ra_px + $radius]
	set bry [expr $dec_px + $radius]
	
	#	For purposes of the tags, need a magnitude rounded down to the nearest 0.1, and then in_osc vs not_osc	
	#	Non-OSC stars get a black center, and so look hollow; OSC stars get a solid colored center	
	set clr [::DISPLAY::resolve_star_color $mag]
	if { $mag >= 0.0 } {
	    set tag_mag  $mag
	} else {
		set tag_mag [expr $mag - 0.1]
	}
	set taglst  "star not_osc mag_[format "%3.1f" $tag_mag]"
	set fillclr black
	if {$osc == 1} {
	    set fillclr $clr
	    set taglst "star in_osc mag_[format "%3.1f" $tag_mag]"
	}

	#	Draw the circle	
	set id [$::SKYMAP::sky_canv create oval $tlx $tly $brx $bry \
		                    -fill $fillclr \
		                    -outline $clr \
		                    -tags "$taglst"]
		                  
	$::SKYMAP::sky_canv bind $id <Button-3> "::DISPLAY::sky_star_pop \
	                                         $star \
	                                         $ra \
	                                         $dec \
	                                         $mag \
	                                         $sky2000id \
	                                         $osc \
	                                         $bssid"

	#	Decide whether the object is "visible" or not
	if {$state == "hidden"} {
	    $::SKYMAP::sky_canv lower $id "sky_mask"
	} else {
	    $::SKYMAP::sky_canv lower $id "sky_star_plane"
	}
	
}


########################################################################
#	draw projection in skymap
proc ::DISPLAY::sky_object {name radec_coords type color} {
    global gi
    set rtd 57.295779513

    foreach block $radec_coords {

        foreach obj $block {

            set coords {}

            set count [expr [llength $obj] -1]
            set j 1
            for {set i 0} {$i <= $count} {incr i 2} {
                set degx [lindex $obj $i]
                set degy [lindex $obj $j]

                set coordx [expr $::SKYMAP::sky_zero(x) - \
                                 ( $degx * $::SKYMAP::sky_pxpdeg(x) )]
                set coordy [expr $::SKYMAP::sky_zero(y) - \
                                 ( $degy * $::SKYMAP::sky_pxpdeg(y) )]
                                 
                set coords [concat $coords $coordx $coordy]

                incr j 2
                
            }

            set cmd [format "set tcl_id \[%s\]" \
            "$::SKYMAP::sky_canv create polygon \
                                         $coords \
                                         -outline $color \
                                         -fill $color \
                                         -tag $name " ]

            eval $cmd

	    set uc [string toupper $name]
	    
	    if { ([string first "ASTEROID" $uc] != -1) || \
	         ([string first "COMET" $uc]    != -1) } {
	            set object "[lindex [$gi $uc ?] 0]"
	            set label $object
	    } else {
	            set object "$name"
	            set label [string totitle $object]
	    }
	    
	    if {$type == "stayout"} {
	        $::SKYMAP::sky_canv lower $tcl_id "sky_stayout_plane"
	        set long_name "$label Stayout Zone"
	    } else {
	        $::SKYMAP::sky_canv lower $tcl_id "sky_ext_plane"
	        set long_name "$label"
	    }

            set ra  [expr $rtd * [ephem_get_ra  $name]]
            set dec [expr $rtd * [ephem_get_dec $name]]
            
            #       Label object if its corresponding Info box was displayed
            if { ($::DISPLAY::display_id($name) == "TRUE") } {
            	::DISPLAY::sky_obj_label $ra $dec $name $label
            }
    
            $::SKYMAP::sky_canv bind $tcl_id <Button-3> \
                                     "::DISPLAY::sky_object_pop \
	                              \"$name\" \
	                              \"$long_name\" \
	                              $ra \
	                              $dec"

        }
        
    }

}


########################################################################
#	draw Skymap Circular Body Patterns

proc ::DISPLAY::sky_body {name radec_coords color} {

    foreach block $radec_coords {

        foreach obj $block {

            set coords {}

            set count [expr [llength $obj] -1]
            set j 1
            for {set i 0} {$i <= $count} {incr i 2} {
                set degx [lindex $obj $i]
                set degy [lindex $obj $j]

                set coordx [expr $::SKYMAP::sky_zero(x) - \
                                 ( $degx * $::SKYMAP::sky_pxpdeg(x) )]
                set coordy [expr $::SKYMAP::sky_zero(y) - \
                                 ( $degy * $::SKYMAP::sky_pxpdeg(y) )]
                                 
                set coords [concat $coords $coordx $coordy]

                incr j 2
                
            }

	    if {$name == "sun_sensor"} {
                set cmd [format "set obj_id \[%s\]" \
                "$::SKYMAP::sky_canv create line \
                                            $coords \
                                            -fill $color \
                                            -tags $name " ]

	    } else {
                set cmd [format "set obj_id \[%s\]" \
                "$::SKYMAP::sky_canv create polygon \
                                            $coords \
                                            -fill $color \
                                            -stipple gray12 \
                                            -tags $name " ]
                                            
            }

            eval $cmd

	    $::SKYMAP::sky_canv raise $obj_id "sky_stayout_plane"

        }
        
    }

}


########################################################################
#	draw Star Tracker projection in skymap as a square

proc ::DISPLAY::sky_instoptFOV {radec_coords color} {

    foreach block $radec_coords {

        foreach obj $block {

            set lseg 0
            foreach seg $obj {

                set coords {}
                set count [expr [llength $seg] -1]

                set j 1
                for {set i 0} {$i <= $count} {incr i 2} {
                    set degx [lindex $seg $i]

                    set degy [lindex $seg $j]

                    set coordx [expr $::SKYMAP::sky_zero(x) \
                                     - ( $degx * $::SKYMAP::sky_pxpdeg(x) )]
                    set coordy [expr $::SKYMAP::sky_zero(y) \
                                     - ( $degy * $::SKYMAP::sky_pxpdeg(y) )]
                    set coords [concat $coords $coordx $coordy]

                    incr j 2
                }

                set cmd [format "set obj_id \[%s\]" \
                "$::SKYMAP::sky_canv create line $coords \
                                                 -tags inst_opt \
                                                 -fill $color " ]

                eval $cmd

	        $::SKYMAP::sky_canv lower $obj_id "sky_body_plane"

                ::update
                
            }
        }
    }
}



########################################################################
#	draw Star Tracker projection in skymap as a square

proc ::DISPLAY::sky_st_square {st radec_coords h_color v_color} {

    foreach block $radec_coords {

        foreach obj $block {

            set lseg 0
            foreach seg $obj {

                if {$lseg == 0} {
                    set color $h_color
                    set arow first
                    set lseg 1
                } elseif {$lseg == 1} {
                    set color $v_color
                    set arow last
                    set lseg 2
                } else {
                    set arow none
                    
                }
            
                set coords {}
                set count [expr [llength $seg] -1]

                set j 1
                for {set i 0} {$i <= $count} {incr i 2} {
                    set degx [lindex $seg $i]

                    set degy [lindex $seg $j]

                    set coordx [expr $::SKYMAP::sky_zero(x) \
                                     - ( $degx * $::SKYMAP::sky_pxpdeg(x) )]
                    set coordy [expr $::SKYMAP::sky_zero(y) \
                                     - ( $degy * $::SKYMAP::sky_pxpdeg(y) )]
                    set coords [concat $coords $coordx $coordy]

                    incr j 2
                }

                set cmd [format "set obj_id \[%s\]" \
                "$::SKYMAP::sky_canv create line $coords \
                                             -tag st_$st \
                                             -arrow $arow \
                                             -width 2 \
                                             -fill $color " ]

                eval $cmd

	        $::SKYMAP::sky_canv lower $obj_id "sky_body_plane"

                ::update
                
            }
        }
    }
}


########################################################################
#	draw Star Tracker projection in skymap as circle

proc ::DISPLAY::sky_st_circle {st radec_coords color} {

        foreach block $radec_coords {

            foreach obj $block {

                set coords {}

                set count [expr [llength $obj] -1]
                set j 1
                for {set i 0} {$i <= $count} {incr i 2} {
                    set degx [lindex $obj $i]
                    set degy [lindex $obj $j]

                    set coordx [expr $::SKYMAP::sky_zero(x) - \
                                     ( $degx * $::SKYMAP::sky_pxpdeg(x) )]
                    set coordy [expr $::SKYMAP::sky_zero(y) - \
                                     ( $degy * $::SKYMAP::sky_pxpdeg(y) )]
                                 
                    set coords [concat $coords $coordx $coordy]

                    incr j 2
                
                }

                set cmd [format "set obj_id \[%s\]" \
                "$::SKYMAP::sky_canv create line  \
                                             $coords \
                                             -fill $color \
                                             -width 2 \
                                             -tag st_$st " ]

                eval $cmd

	        $::SKYMAP::sky_canv lower $obj_id "sky_body_plane"

            }

        }

}

########################################################################
#	draw Star Tracker projection in skymap as an annulus

proc ::DISPLAY::sky_st_annulus {st radec_coords color} {

    foreach struct $radec_coords {

        foreach block $struct {

            foreach obj $block {

                set coords {}

                set count [expr [llength $obj] -1]
                set j 1
                for {set i 0} {$i <= $count} {incr i 2} {
                    set degx [lindex $obj $i]
                    set degy [lindex $obj $j]

                    set coordx [expr $::SKYMAP::sky_zero(x) - \
                                     ( $degx * $::SKYMAP::sky_pxpdeg(x) )]
                    set coordy [expr $::SKYMAP::sky_zero(y) - \
                                     ( $degy * $::SKYMAP::sky_pxpdeg(y) )]
                                 
                    set coords [concat $coords $coordx $coordy]

                    incr j 2
                
                }

                set cmd [format "set obj_id \[%s\]" \
                "$::SKYMAP::sky_canv create line  \
                                             $coords \
                                             -fill $color \
                                             -width 2 \
                                             -tag st_$st " ]

                eval $cmd

	        $::SKYMAP::sky_canv lower $obj_id "sky_body_plane"

            }

        }
        
    }

}

########################################################################
#	draw S/C body axis marker

proc ::DISPLAY::sky_body_axis {axis ra dec color} {

	set ra_px  [expr $::SKYMAP::sky_zero(x) - \
	                 ($ra * $::SKYMAP::sky_pxpdeg(x))]
	set dec_px [expr $::SKYMAP::sky_zero(y) - \
	                 ($dec * $::SKYMAP::sky_pxpdeg(y))]

	set tlx [expr $ra_px  - 10]
	set tly [expr $dec_px - 10]
	set brx [expr $ra_px  + 10]
	set bry [expr $dec_px + 10]

	$::SKYMAP::sky_canv create text \
	                           $ra_px \
	                           $dec_px \
	                           -text [string toupper $axis] \
	                           -font $::CONFIG::fonts(wtitle) \
	                           -fill $color \
	                           -tags "sky_$axis"

	$::SKYMAP::sky_canv create oval \
	                           $tlx \
	                           $tly \
	                           $brx \
	                           $bry \
	                           -outline $color \
	                           -tags "sky_$axis"
	                           
}


#########################################################################
proc ::DISPLAY::sky_bands { } {
	global gi
	
	set bands [$gi NORMAL_ST_BAND_DECLS 0.0]
	
	foreach band $bands {
	    set angy [expr $::SKYMAP::sky_zero(y) - \
	                   ($band * $::SKYMAP::sky_pxpdeg(y))]
	                   
	    $::SKYMAP::sky_canv create line \
	                          0 \
	                          $angy\
	                          $::SKYMAP::sky_maxx\
	                          $angy \
	                          -fill white \
	                          -tags sky_bands

	    $::SKYMAP::sky_canv lower sky_bands "sky_FOV_plane"

	}

}	
########################################################################
#	$Log: $
########################################################################
