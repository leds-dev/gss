########################################################################
#	$Id: $
########################################################################
#	user quaternion interface

package provide GSS_TCL_LIB 1.0

########################################################################

namespace eval ::USER_QUAT:: {
	variable base_frm
	variable entry_fields
	variable user_qsel
	variable gss_qsel
	variable gss_qsel_state
	variable int_gss_qsel
	variable int_gss_qsel_state
	variable user_quat
	variable tlm_quat
	variable snptlm_quat
	variable cis_quat
	variable null_quat 
	variable static_quat
	variable gss_quat
	variable snptlm_q_time
	variable static_q_time

	namespace export init gui

}

########################################################################
#	user quaternion setup

proc ::USER_QUAT::init { } {
	variable base_frm
	variable entry_fields
	variable user_qsel
	variable gss_qsel
	variable gss_qsel_state
	variable int_gss_qsel
	variable int_gss_qsel_state
	variable user_quat
	variable tlm_quat
	variable snptlm_quat
	variable cis_quat
	variable null_quat 
	variable static_quat
	variable gss_quat
	variable int_state
	variable snptlm_q_time
	variable static_q_time

	set base_frm {}
		
	set entry_fields ""

	set user_qsel tlm
	set gss_qsel "TLM"
	
	set int_gss_qsel(NULL) 0
	set int_gss_qsel(TLM) 1
	set int_gss_qsel(CIS) 2
	set int_gss_qsel(STATIC) 3	
	
	set gss_qsel_state "VALID"
	
	set int_gss_qsel_state(INVALID) 0
	set int_gss_qsel_state(VALID) 1
	
	set null_quat(0) 0.0000000
	set null_quat(1) 0.0000000
	set null_quat(2) 0.0000000
	set null_quat(3) 1.0000000
	
	set tlm_quat(0) 0.0000000
	set tlm_quat(1) 0.0000000
	set tlm_quat(2) 0.0000000
	set tlm_quat(3) 1.0000000
	
	set snptlm_quat(0) 0.0000000
	set snptlm_quat(1) 0.0000000
	set snptlm_quat(2) 0.0000000
	set snptlm_quat(3) 1.0000000
	
	set cis_quat(0) 0.0000000
	set cis_quat(1) 0.0000000
	set cis_quat(2) 0.0000000
	set cis_quat(3) 1.0000000
	
	set user_quat(0) 0.0000000
	set user_quat(1) 0.0000000
	set user_quat(2) 0.0000000
	set user_quat(3) 1.0000000

	set static_quat(0) 0.0000000
	set static_quat(1) 0.0000000	
	set static_quat(2) 0.0000000		
	set static_quat(3) 1.0000000	

	set gss_quat(0) 0.0000000
	set gss_quat(1) 0.0000000
	set gss_quat(2) 0.0000000
	set gss_quat(3) 1.0000000

	set snptlm_q_time 0
	set static_q_time 0

	#	Initialize C layer
	SetStaticQuatValue $gss_quat(0) \
	                   $gss_quat(1) \
	                   $gss_quat(2) \
	                   $gss_quat(3)
	SetQuatSelAndValid \
	   $int_gss_qsel($gss_qsel) \
	   $int_gss_qsel_state($gss_qsel_state) 


}

########################################################################
#	Normalize the user entered quaternion and set the 
#	static quaternion values from the user's

proc ::USER_QUAT::set_static_from_user { } {
	set i 1
	foreach quat {0 1 2 3} {
	    if {[catch {expr $::USER_QUAT::user_quat($quat) + 0}] == 1} {
	        ::MSG_DLG::attention "Invalid Entry" \
	                              red \
	                              black \
	                              -INSTRUCTION \
	                              "Re-enter the User Quaternion\
	                              q($i) value" 

	        return

	    }
	    incr i
	    
	}

	set rms [expr sqrt( pow($::USER_QUAT::user_quat(0),2) + \
	                    pow($::USER_QUAT::user_quat(1),2) + \
	                    pow($::USER_QUAT::user_quat(2),2) + \
	                    pow($::USER_QUAT::user_quat(3),2)     )]
	                    
	if {$rms == 0.0} {
	        ::MSG_DLG::attention "Null Quaternion" \
	                              red \
	                              black \
	                              -INSTRUCTION \
	                              "Re-enter the User Quaternion" 
	        return
	}                          
	                    
	set ::USER_QUAT::static_quat(0) \
	    [format "%9.7f" [expr $::USER_QUAT::user_quat(0) / $rms]]
	set ::USER_QUAT::static_quat(1) \
	    [format "%9.7f" [expr $::USER_QUAT::user_quat(1) / $rms]]
	set ::USER_QUAT::static_quat(2) \
	    [format "%9.7f" [expr $::USER_QUAT::user_quat(2) / $rms]]
	set ::USER_QUAT::static_quat(3) \
	    [format "%9.7f" [expr $::USER_QUAT::user_quat(3) / $rms]]

	set ::USER_QUAT::static_q_time $::CONFIG::data(gss_time)

	if {$::USER_QUAT::gss_qsel == "STATIC"} {
		::USER_QUAT::set_gss_quat
	}
	
}

########################################################################
#	Set the static quaternion values from the sample

proc ::USER_QUAT::set_static_from_sample { } {

	set ::USER_QUAT::static_quat(0) [format "%9.7f" $::USER_QUAT::snptlm_quat(0) ]
	set ::USER_QUAT::static_quat(1) [format "%9.7f" $::USER_QUAT::snptlm_quat(1) ]
	set ::USER_QUAT::static_quat(2) [format "%9.7f" $::USER_QUAT::snptlm_quat(2) ]
	set ::USER_QUAT::static_quat(3) [format "%9.7f" $::USER_QUAT::snptlm_quat(3) ]

	set ::USER_QUAT::static_q_time $::USER_QUAT::snptlm_q_time
	
	if {$::USER_QUAT::gss_qsel == "STATIC"} {
		::USER_QUAT::set_gss_quat
	}
	  
}

########################################################################
#	Get the current Telemetered quaternion

proc ::USER_QUAT::get_tlm_quat { } {

	    set data [GetTM_ECI2BodyQuat]
	    set ::USER_QUAT::tlm_quat(0) [format "%9.7f" [lindex $data 0]]
	    set ::USER_QUAT::tlm_quat(1) [format "%9.7f" [lindex $data 1]]
	    set ::USER_QUAT::tlm_quat(2) [format "%9.7f" [lindex $data 2]]
	    set ::USER_QUAT::tlm_quat(3) [format "%9.7f" [lindex $data 3]]

}


########################################################################
#	Depending on mode (null,tlm,cis,or static) enable or disable
#	the assorted buttons and entry fields
proc ::USER_QUAT::endis_fields {state} {

	foreach wdgt $::USER_QUAT::entry_fields {
	    $wdgt configure -state $state
	    
	}

}


########################################################################
#	Set the GSS quaternion to what the user specified
proc ::USER_QUAT::set_gss_quat { } {
	variable base_frm
	
	set ::USER_QUAT::gss_qsel [string toupper $::USER_QUAT::user_qsel]

	if {$::USER_QUAT::gss_qsel == "NULL"} {
	    set ::USER_QUAT::gss_qsel_state "INVALID"
	    
	} else {
	    set ::USER_QUAT::gss_qsel_state "VALID"

	}
		
	foreach quat {0 1 2 3} {
	    set a "set ::USER_QUAT::gss_quat($quat)"
	    set b $::USER_QUAT::user_qsel
	    set c "::USER_QUAT::$b\_quat($quat)"
	                   
	    set cmd "$a \$$c"
	    eval "$cmd"

	#	Revector display labels
	    set indx [expr $quat + 1]
	    set cmd "$base_frm.out.fr2.lbl$indx configure \
	                                        -textvariable $c"
	    eval "$cmd"

	}

	#	Set C Layer
	set err [SetStaticQuatValue $::USER_QUAT::gss_quat(0) \
	                            $::USER_QUAT::gss_quat(1) \
	                            $::USER_QUAT::gss_quat(2) \
	                            $::USER_QUAT::gss_quat(3)]
	                            
	if {$err != ""} {
	    ::MSG_DLG::attention $err \
	                         red \
	                         black \
	                         -INSTRUCTION "Verify inputs"
	                         
	    set err ""
	    
	}
	                   
	SetQuatSelAndValid \
	   $::USER_QUAT::int_gss_qsel($::USER_QUAT::gss_qsel) \
	   $::USER_QUAT::int_gss_qsel_state($::USER_QUAT::gss_qsel_state) 

	#	Log the change
	set msg_txt "SET GSS QUATERNION BUTTON PRESSED\n"
	set msg_txt "$msg_txt\GSS Quaternion Source: $::USER_QUAT::gss_qsel\n"
	set msg_txt "$msg_txt\GSS Quaternion Validity:\
	             $::USER_QUAT::gss_qsel_state\n"
	set q_txt [format "q(1) %9.7f\tq(2) %9.7f\t\
	                   q(3) %9.7f\t q(4) %9.7f" \
	                   $::USER_QUAT::gss_quat(0) \
	                   $::USER_QUAT::gss_quat(1) \
	                   $::USER_QUAT::gss_quat(2) \
	                   $::USER_QUAT::gss_quat(3)]
	set msg_txt "$msg_txt\GSS Quaternion (qb.eci): \n$q_txt\n"

	::MSG_DLG::msg_log $msg_txt
	
}

########################################################################
#	Set the GSS quaternion to what the user specified
proc ::USER_QUAT::update_gss_quat { } {
		
	foreach quat {0 1 2 3} {
	    set a "set ::USER_QUAT::gss_quat($quat)"
	    set b $::USER_QUAT::user_qsel
	    set c "::USER_QUAT::$b\_quat($quat)"
	                   
	    set cmd "$a \$$c"
	    eval "$cmd"

	}

}

########################################################################
#	user quaternion gui

proc ::USER_QUAT::quat_gui { } {
	variable base_frm
	variable entry_fields
	variable user_qsel
	variable user_quat
	variable tlm_quat
	variable snptlm_quat
	variable cis_quat
	variable null_quat 

	set ent_state(null) disabled
	set ent_state(tlm) disabled
	set ent_state(cis) disabled
	set ent_state(static) normal
	
	catch {destroy .quat_ui}
	toplevel .quat_ui
	wm title .quat_ui "QUATERNION SELECTION"
	wm geometry .quat_ui "-0-0"
	set pf .quat_ui.top
	frame $pf
	pack $pf

	set base_frm $pf
	frame $pf.in
	pack $pf.in -side top \
		    -fill both

	frame $pf.out -borderwidth 1 \
	              -relief sunken
	pack $pf.out -side top \
		     -fill both \
		     -ipady 5

	#	NULL Entry
	frame $pf.in.null -borderwidth 1 \
			  -relief sunken 

	pack $pf.in.null -side top \
			 -anchor w \
			 -fill x \
			 -ipady 5
			 
	radiobutton $pf.in.null.rb -text "Null" \
				   -font $::CONFIG::fonts(entry) \
				   -variable ::USER_QUAT::user_qsel \
				   -value null \
				   -indicator on \
				   -anchor w \
				   -width 20 \
				   -command "::USER_QUAT::endis_fields \
				             disabled"


	pack $pf.in.null.rb -side left \
			    -anchor w \
			    -fill x

	label $pf.in.null.lbl1 -text "INVALID - No valid quaternion in use" \
			       -font  $::CONFIG::fonts(entry) \
			       -relief sunken \
			       -width 48

	pack $pf.in.null.lbl1 \
	     -side right \
	     -anchor e \
	     -padx 2

	#	Telemetry Entry
	frame $pf.in.tlm  -borderwidth 1 \
			  -relief sunken 

	pack $pf.in.tlm  -side top \
			 -anchor w \
			 -fill x \
			 -ipady 5
			 
	radiobutton $pf.in.tlm.rb  -text "Telemetry" \
				   -font $::CONFIG::fonts(entry) \
				   -variable ::USER_QUAT::user_qsel \
				   -value tlm \
				   -indicator on \
				   -anchor w \
				   -width 20 \
				   -command "::USER_QUAT::endis_fields \
				             disabled"


	pack $pf.in.tlm.rb  -side left \
			    -anchor w \
			    -fill x
			    
	label $pf.in.tlm.lbl1  -textvariable ::USER_QUAT::tlm_quat(0) \
			       -font  $::CONFIG::fonts(entry) \
			       -justify right \
			       -anchor e \
			       -relief sunken \
			       -width 10

	label $pf.in.tlm.lbl2  -textvariable ::USER_QUAT::tlm_quat(1) \
			       -font  $::CONFIG::fonts(entry) \
			       -justify right \
			       -anchor e \
			       -relief sunken \
			       -width 10

	label $pf.in.tlm.lbl3  -textvariable ::USER_QUAT::tlm_quat(2) \
			       -font  $::CONFIG::fonts(entry) \
			       -justify right \
			       -anchor e \
			       -relief sunken \
			       -width 10

	label $pf.in.tlm.lbl4  -textvariable ::USER_QUAT::tlm_quat(3) \
			       -font  $::CONFIG::fonts(entry) \
			       -justify right \
			       -anchor e \
			       -relief sunken \
			       -width 10

	pack $pf.in.tlm.lbl4 $pf.in.tlm.lbl3 \
	     $pf.in.tlm.lbl2 $pf.in.tlm.lbl1 \
	     -side right \
	     -anchor e \
	     -padx 2


	#	CIS Entry
	frame $pf.in.cis  -borderwidth 1 \
			  -relief sunken 

	pack $pf.in.cis  -side top \
			 -anchor w \
			 -fill x \
			 -ipady 5
			 
	radiobutton $pf.in.cis.rb  -text "CIS" \
				   -font $::CONFIG::fonts(entry) \
				   -variable ::USER_QUAT::user_qsel \
				   -value cis \
				   -state $::CIS_GUI::cis_committed \
				   -indicator on \
				   -anchor w \
				   -width 20 \
				   -command "::USER_QUAT::endis_fields \
				             disabled "

	pack $pf.in.cis.rb  -side left \
			    -anchor w \
			    -fill x

	label $pf.in.cis.lbl0 -textvariable ::CIS_GUI::cis_state_text \
			      -font $::CONFIG::fonts(entry) \
			      -anchor w
			      
	pack $pf.in.cis.lbl0 -side left \
			     -anchor w

	label $pf.in.cis.lbl1  -textvariable ::USER_QUAT::cis_quat(0) \
			       -font  $::CONFIG::fonts(entry) \
			       -relief sunken \
			       -justify right \
			       -anchor e \
			       -width 10

	label $pf.in.cis.lbl2  -textvariable ::USER_QUAT::cis_quat(1) \
			       -font  $::CONFIG::fonts(entry) \
			       -relief sunken \
			       -justify right \
			       -anchor e \
			       -width 10

	label $pf.in.cis.lbl3  -textvariable ::USER_QUAT::cis_quat(2) \
			       -font  $::CONFIG::fonts(entry) \
			       -relief sunken \
			       -justify right \
			       -anchor e \
			       -width 10

	label $pf.in.cis.lbl4  -textvariable ::USER_QUAT::cis_quat(3) \
			       -font  $::CONFIG::fonts(entry) \
			       -relief sunken \
			       -justify right \
			       -anchor e \
			       -width 10

	pack $pf.in.cis.lbl4 $pf.in.cis.lbl3 \
	     $pf.in.cis.lbl2 $pf.in.cis.lbl1 \
	     -side right \
	     -anchor e \
	     -padx 2


	#	Static Entry
	frame $pf.in.stat -borderwidth 1 \
			  -relief sunken 

	pack $pf.in.stat -side top \
			 -anchor w \
			 -fill x \
			 -ipady 5
			 
	frame $pf.in.stat.fr1
	pack $pf.in.stat.fr1 -side top \
			     -anchor w \
			     -fill x \
			     -pady 5
			     
	radiobutton $pf.in.stat.fr1.rb  -text "Static (normalized)" \
				        -font $::CONFIG::fonts(entry) \
				        -variable ::USER_QUAT::user_qsel \
				        -value static \
				        -indicator on \
				        -anchor w \
				        -width 20 \
				        -command "::USER_QUAT::endis_fields \
				                  normal "

	pack $pf.in.stat.fr1.rb  -side left \
			         -anchor w \
			         -fill x

	label $pf.in.stat.fr1.lbl1  -textvariable ::USER_QUAT::static_quat(0) \
			            -font  $::CONFIG::fonts(entry) \
			            -relief sunken \
			            -justify right \
			            -anchor e \
			            -width 10

	label $pf.in.stat.fr1.lbl2  -textvariable ::USER_QUAT::static_quat(1) \
			            -font  $::CONFIG::fonts(entry) \
			            -relief sunken \
			            -justify right \
			            -anchor e \
			            -width 10

	label $pf.in.stat.fr1.lbl3  -textvariable ::USER_QUAT::static_quat(2) \
			            -font  $::CONFIG::fonts(entry) \
			            -relief sunken \
			            -justify right \
			            -anchor e \
			            -width 10

	label $pf.in.stat.fr1.lbl4  -textvariable ::USER_QUAT::static_quat(3) \
			            -font  $::CONFIG::fonts(entry) \
			            -relief sunken \
			            -justify right \
			            -anchor e \
			            -width 10

	pack $pf.in.stat.fr1.lbl4 $pf.in.stat.fr1.lbl3 \
	     $pf.in.stat.fr1.lbl2 $pf.in.stat.fr1.lbl1 \
	     -side right \
	     -anchor e \
	     -padx 2

	#	User entry area for filling the static quaternions			 
	frame $pf.in.stat.fr2
	pack $pf.in.stat.fr2 -side top \
			     -anchor w \
			     -fill x

	frame $pf.in.stat.fr2.spcr -width 10
	pack $pf.in.stat.fr2.spcr -side left \
				  -anchor w
				  
	frame $pf.in.stat.fr2.usr
	pack $pf.in.stat.fr2.usr -side top \
				 -anchor w \
				 -fill x
	
	button $pf.in.stat.fr2.usr.btn -text "Update from User"	\
		       -font $::CONFIG::fonts(entry) \
		       -width 18 \
		       -state $ent_state($::USER_QUAT::user_qsel) \
		       -command "::USER_QUAT::set_static_from_user"	
		       		 
	pack $pf.in.stat.fr2.usr.btn -side left \
				     -anchor w

	lappend ::USER_QUAT::entry_fields $pf.in.stat.fr2.usr.btn
	
	button $pf.in.stat.fr2.usr.btn2 -text "Copy CIS" \
                       -font $::CONFIG::fonts(entry) \
                       -width 8 \
                       -state $ent_state($::USER_QUAT::user_qsel) \
		       -command "set ::USER_QUAT::user_quat(0) \$::USER_QUAT::cis_quat(0); \
		       		 set ::USER_QUAT::user_quat(1) \$::USER_QUAT::cis_quat(1); \
		       		 set ::USER_QUAT::user_quat(2) \$::USER_QUAT::cis_quat(2); \
		       		 set ::USER_QUAT::user_quat(3) \$::USER_QUAT::cis_quat(3)"	
			      
	pack $pf.in.stat.fr2.usr.btn2 -side left \
			              -anchor w \
			              -padx 4 \
			              -fill x
			              
	lappend ::USER_QUAT::entry_fields $pf.in.stat.fr2.usr.btn2

	entry $pf.in.stat.fr2.usr.ent1  \
		-textvariable ::USER_QUAT::user_quat(0) \
		-font  $::CONFIG::fonts(entry) \
		-state $ent_state($::USER_QUAT::user_qsel) \
		-justify right \
		-relief sunken \
		-width 10

	entry $pf.in.stat.fr2.usr.ent2  \
		-textvariable ::USER_QUAT::user_quat(1) \
		-font  $::CONFIG::fonts(entry) \
		-state $ent_state($::USER_QUAT::user_qsel) \
		-justify right \
		-relief sunken \
		-width 10

	entry $pf.in.stat.fr2.usr.ent3  \
		-textvariable ::USER_QUAT::user_quat(2) \
		-font  $::CONFIG::fonts(entry) \
		-state $ent_state($::USER_QUAT::user_qsel) \
		-justify right \
		-relief sunken \
		-width 10

	entry $pf.in.stat.fr2.usr.ent4  \
		-textvariable ::USER_QUAT::user_quat(3) \
		-font  $::CONFIG::fonts(entry) \
		-state $ent_state($::USER_QUAT::user_qsel) \
		-justify right \
		-relief sunken \
		-width 10


	pack $pf.in.stat.fr2.usr.ent4 $pf.in.stat.fr2.usr.ent3 \
	     $pf.in.stat.fr2.usr.ent2 $pf.in.stat.fr2.usr.ent1 \
	     -side right \
	     -anchor e \
	     -padx 1

	lappend ::USER_QUAT::entry_fields $pf.in.stat.fr2.usr.ent4 \
	                                  $pf.in.stat.fr2.usr.ent3 \
	                                  $pf.in.stat.fr2.usr.ent2 \
	                                  $pf.in.stat.fr2.usr.ent1
	
	#	Telemetry sample area for filling the static quaternions
	frame $pf.in.stat.fr2.tlm
	pack $pf.in.stat.fr2.tlm -side top \
				 -anchor w \
				 -fill x
	
	button $pf.in.stat.fr2.tlm.btn -text "Update from Sample"	\
		       -font $::CONFIG::fonts(entry) \
		       -state $ent_state($::USER_QUAT::user_qsel) \
		       -width 18 \
		       -command "::USER_QUAT::set_static_from_sample"	
		       		 
	pack $pf.in.stat.fr2.tlm.btn -side left \
				     -anchor w

	lappend ::USER_QUAT::entry_fields $pf.in.stat.fr2.tlm.btn
			    
	button $pf.in.stat.fr2.tlm.btn2 -text "Sample TLM" \
                       -font $::CONFIG::fonts(entry) \
                       -width 8 \
                       -state $ent_state($::USER_QUAT::user_qsel) \
		       -command "set ::USER_QUAT::snptlm_quat(0) \$::USER_QUAT::tlm_quat(0); \
		       		 set ::USER_QUAT::snptlm_quat(1) \$::USER_QUAT::tlm_quat(1); \
		       		 set ::USER_QUAT::snptlm_quat(2) \$::USER_QUAT::tlm_quat(2); \
		       		 set ::USER_QUAT::snptlm_quat(3) \$::USER_QUAT::tlm_quat(3); \
		       		 set ::USER_QUAT::snptlm_q_time \$::CONFIG::data(gss_time)"	
			      
	pack $pf.in.stat.fr2.tlm.btn2 -side left \
			              -anchor w \
			              -padx 4 \
			              -fill x
			              
	lappend ::USER_QUAT::entry_fields $pf.in.stat.fr2.tlm.btn2

	label $pf.in.stat.fr2.tlm.lbl1  \
			       -textvariable ::USER_QUAT::snptlm_quat(0) \
			       -font  $::CONFIG::fonts(entry) \
			       -relief sunken \
			       -justify right \
			       -anchor e \
			       -width 10

	label $pf.in.stat.fr2.tlm.lbl2  \
			       -textvariable ::USER_QUAT::snptlm_quat(1) \
			       -font  $::CONFIG::fonts(entry) \
			       -relief sunken \
			       -justify right \
			       -anchor e \
			       -width 10

	label $pf.in.stat.fr2.tlm.lbl3  \
	    		       -textvariable ::USER_QUAT::snptlm_quat(2) \
			       -font  $::CONFIG::fonts(entry) \
			       -relief sunken \
			       -justify right \
			       -anchor e \
			       -width 10

	label $pf.in.stat.fr2.tlm.lbl4  \
			       -textvariable ::USER_QUAT::snptlm_quat(3) \
			       -font  $::CONFIG::fonts(entry) \
			       -relief sunken \
			       -justify right \
			       -anchor e \
			       -width 10

	pack $pf.in.stat.fr2.tlm.lbl4 $pf.in.stat.fr2.tlm.lbl3 \
	     $pf.in.stat.fr2.tlm.lbl2 $pf.in.stat.fr2.tlm.lbl1 \
	     -side right \
	     -anchor e \
	     -padx 2

	#	Display Final GSS quaternion source and data
	frame $pf.out.fr1
	pack $pf.out.fr1 -side top \
			 -anchor w \
			 -fill both 
			     
	label $pf.out.fr1.lbl1  -text "GSS Quaternion Source:" \
			        -font  $::CONFIG::fonts(entry) \
			        -anchor w \
			        -width 22

	pack $pf.out.fr1.lbl1 -side left \
	                      -anchor w 

	                      			     
	label $pf.out.fr1.lbl2  -textvariable ::USER_QUAT::gss_qsel \
			        -font  $::CONFIG::fonts(entry) \
			        -relief sunken

	pack $pf.out.fr1.lbl2 -side left \
	                      -anchor w \
	                      -ipadx 3 \
	                      -padx 5 \
	                      -pady 5
	                      			     
	label $pf.out.fr1.lbl3  -textvariable ::USER_QUAT::gss_qsel_state \
			        -font  $::CONFIG::fonts(entry) 

	pack $pf.out.fr1.lbl3 -side left \
	                      -anchor w \
	                      -ipadx 3 \
	                      -padx 5 \
	                      -pady 5

	frame $pf.out.fr2
	pack $pf.out.fr2 -side top \
	                 -anchor w \
	                 -fill both
	                 
	label $pf.out.fr2.lbl0  -text "GSS Quaternion" \
			        -font  $::CONFIG::fonts(entry) \
			        -width 23 \
			        -anchor w

	pack $pf.out.fr2.lbl0 -side left \
	                      -anchor w 


	set b $::USER_QUAT::user_qsel
	set cmd "label $pf.out.fr2.lbl1  \
	               -textvariable ::USER_QUAT::$b\_quat(0) \
		       -font \"$::CONFIG::fonts(entry)\" \
		       -relief sunken \
		       -anchor e \
		       -justify right \
		       -width 10"
	eval "$cmd"

	set cmd "label $pf.out.fr2.lbl2  \
	               -textvariable ::USER_QUAT::$b\_quat(1) \
		       -font  \"$::CONFIG::fonts(entry)\" \
		       -relief sunken \
		       -justify right \
		       -anchor e \
		       -width 10"

	eval "$cmd"
	
	set cmd "label $pf.out.fr2.lbl3  \
	               -textvariable ::USER_QUAT::$b\_quat(2) \
		       -font  \"$::CONFIG::fonts(entry)\" \
		       -relief sunken \
		       -justify right \
		       -anchor e \
		       -width 10"

	eval "$cmd"

	set cmd "label $pf.out.fr2.lbl4  \
	               -textvariable ::USER_QUAT::$b\_quat(3) \
		       -font  \"$::CONFIG::fonts(entry)\" \
		       -relief sunken \
		       -justify right \
		       -anchor e \
		       -width 10"

	eval "$cmd"

	pack $pf.out.fr2.lbl4 $pf.out.fr2.lbl3 \
	     $pf.out.fr2.lbl2 $pf.out.fr2.lbl1 \
	     -side right \
	     -anchor e \
	     -padx 2
	


	#	Update GSS quaternion button
	frame .quat_ui.update -borderwidth 1 \
	                      -relief sunken
	pack .quat_ui.update -side top \
	                     -fill both

	button .quat_ui.update.btn -text "SET GSS QUATERNION" \
	                           -font $::CONFIG::fonts(entry) \
	                           -width 40 \
	                           -command "::USER_QUAT::set_gss_quat"	

	pack .quat_ui.update.btn -side top \
	                         -pady 5


	#	OK button
	frame .quat_ui.btn -borderwidth 1 \
	                   -relief sunken

	pack .quat_ui.btn -side top \
	                  -fill both

	button .quat_ui.btn.ok -text "OK" \
	                       -font $::CONFIG::fonts(entry) \
	                       -command "destroy .quat_ui"

	pack .quat_ui.btn.ok -side top \
	                     -pady 5

	#	Initialize the telemetered quaternion
	::USER_QUAT::get_tlm_quat
	
}


########################################################################
#	$Log: $
########################################################################
