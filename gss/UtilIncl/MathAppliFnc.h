/*===================================================================

	FILE NAME:
		MathAppliFnc.h

	DESCRIPTION:
		This is the haeder file of MathAppliFnc.c


 ====================================================================

	REVISION HISTORY:


 ==================================================================== */

#ifndef _MATHAPPLIFNC_H
#define _MATHAPPLIFNC_H

/* $Id: MathAppliFnc.h,v 1.1 1999/12/13 22:15:39 jzhao Exp $ */

/* ------------------
 Include headers 
*/
#include <MathFnc.h>

/* ------------------
 Macros 
*/

/*-------------------
 Function prototypes
*/
int RAscenDeclina2ECI(double RAscen, double Declina, void *ECI, int type);
int ECI2RAscenDeclina(void *ECI, double *RAscen, double *Declina, int type);
double QuaternionSepAngle(double Quat1[], double Quat2[]);
int QuatTRIAD(double U_A[], double U_B[], double V_A[], double V_B[],
														double Quat[]);
int QUEST(int N, double UM[], double UR[], double Quat[]);

/*******************************************************************************
	Change Log:
	$Log: MathAppliFnc.h,v $
	Revision 1.1  1999/12/13 22:15:39  jzhao
	Initial Release.
	
********************************************************************************/

#endif /* _MATHAPPLIFNC_H */

