/*===================================================================

	FILE NAME:
		MathDef.h

	DESCRIPTION:
		This provides definitions for math functions


 ====================================================================

	REVISION HISTORY:


 ==================================================================== */


/* $Id: MathDef.h,v 1.2 2000/05/16 22:30:52 jzhao Exp $ */

/* --------------------
   Macros 
*/
#define PI      3.14159265358979323846
#define DTR     (PI/180.0)      /* Degree To Radians */
#define RTD     (180.0/PI)      /* Radians To Degrees */
#define RTA     (RTD*3600.0)    /* Radian To Arc seconds */
#define DTA     3600.0          /* Degree To Arc seconds */
#define ATR		(1.0/RTA)		/* Arc seconds To Radians */
#define ATD		(1.0/DTA)		/* Arc seconds To Degree */
#define SQRT_2  1.4142135623731 /* Square Root 2 */

#define Max(A, B)   ((A) > (B) ? (A) : (B))
#define Min(A, B)	((A) < (B) ? (A) : (B))

/******************************************************************************
	Change Log:
	$Log: MathDef.h,v $
	Revision 1.2  2000/05/16 22:30:52  jzhao
	Add more digits to PI.
	
	Revision 1.1  2000/05/16 21:26:40  klafter
	Split out pure definitions into separate header
	
	
	
*******************************************************************************/
