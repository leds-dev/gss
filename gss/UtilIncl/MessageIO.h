/*===================================================================

    FILE NAME:
        MessageIO.h

    DESCRIPTION:
        This is the haeder file of MessageIO.c


 ====================================================================

    REVISION HISTORY:


 ==================================================================== */

#ifndef _MESSAGEIO_H
#define _MESSAGEIO_H

/* $Id: MessageIO.h,v 1.4 2000/03/30 17:51:40 jzhao Exp $ */

	extern FILE *LogFp;

	enum error_type
	{
		NO_ERROR			= 0,		
		MEM_ERROR			= -1,
		FILE_ERROR			= -2,
		MATH_ERROR			= -3,
		NON_NORMALIZE		= -4,
		MODE_ERROR			= -5,
		LAST_ERROR			= -6	
	};	

	enum status_type
	{
		NO_ENOUGH_STARS		= 1,	
		NO_REFERENCE_ST		= 2, 
		CONTRADICTORY		= 3,
		NO_ENOUGH_IDSTARS	= 4,
		PARTIAL_SUCCESS		= 5,
		TOTAL_SUCCESS		= 6,
		COMPLETE			= 7,
		LAST_STATUS			= 8	
	};

	char *GetMessage(int stat, char *s);
	void DisplayMessage(int stat, char *s);
	void LogMessage(int stat, char *s);	

/*******************************************************************************
	Change Log:
	$Log: MessageIO.h,v $
	Revision 1.4  2000/03/30 17:51:40  jzhao
	Modified for integration.
	
	Revision 1.3  2000/02/24 19:51:57  jzhao
	Added GetMessage to extern function list.
	
	Revision 1.2  2000/02/09 23:52:18  jzhao
	Added DirCosMatrix2Quaternion function.
	Modified CIS Status message.
	
	Revision 1.1  1999/12/13 22:15:40  jzhao
	Initial Release.
	
********************************************************************************/

#endif /* _MESSAGEIO_H */
