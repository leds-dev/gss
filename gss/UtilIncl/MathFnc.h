/*===================================================================

	FILE NAME:
		MathFnc.h

	DESCRIPTION:
		This is the header file of MathFnc.c


 ====================================================================

	REVISION HISTORY:


 ==================================================================== */

#ifndef _MATHFNC_H
#define _MATHFNC_H

/* $Id: MathFnc.h,v 1.6 2000/05/16 21:26:41 klafter Exp $ */

#include "MathDef.h"

/* --------------------
   Macros 
*/
#define FLOAT_DATA  1
#define DOUBLE_DATA 2

#define TINY	1.0e-10		/* Define a tiny number */
#define NORM_TOL 1.0e-5		/* Quat/Vect Normalizition tolerance */

/*-------------------
 Function prototypes
*/
int RemoveZero(double *pt_mag);
double VectorMagnitude(void *vect, int type);
int VectorNormalize(void *vect1, void *vect2, int type);
int VectorVerifyNormalize(void *vect1, void *vect2, int type);
double DotProduct(void *vect1, void *vect2, int type);
void CrossProduct(void *vect1, void *vect2, void *vect3, int type);
int AngleBetVectors(void *vect1, void *vect2, double *angle, int type);
int AngleBetNormalVectors(void *vect1, void *vect2, double *angle, int type);
int VectorDiff(void *vect1, void *vect2, void *vect3, int type);
int VectorDivScalar(void *vect1, void *vect2, double scalar, int type);

int MatrixZero(void *mat1, int m, int n, int type);
int MatrixIdent(void *mat1, int m, int type);
int MatrixAdd(void *mat1, void *mat2, void *mat3, int m, int n, int type);
int MatrixMultScalar(void *mat1, void *mat2, double scalar,
			 		int m, int n, int type);
int MatrixMult(void *mat1, void *mat2, void *mat3,
			 int m, int n, int p, int type);
int MatrixTranspose(void *mat1, void *mat2, int m, int n, int type);
double MatrixDeterminant_3x3(double *mat);

void QuaternionIdent(void *Quat, int type);
int QuaternionMult(void *Quat1, void *Quat2, void *Quat3, int type);
int QuaternionInvers(void *Quat1, void *Quat2, int type);
int Quaternion2DirCosMatrix(void *Quat, void *DirCosMat, int type);
int QuaternionNormalize(void *Quat1, void *Quat2, int type);
int QuaternionVerifyNormalize(void *Quat1, void *Quat2, int type);
int DirCosMatrix2Quaternion(void *DirCosMat, void *Quat, int type);
int QuaternionRotateEuler123(void *Quat1, void *Quat2, double Angle1,
							 double Angle2, double Angle3, int type);
int QuaternionRotateEuler213(void *Quat1, void *Quat2, double Angle1,
							 double Angle2, double Angle3, int type);

/******************************************************************************
	Change Log:
	$Log: MathFnc.h,v $
	Revision 1.6  2000/05/16 21:26:41  klafter
	Split out pure definitions into separate header
	
	Revision 1.5  2000/03/30 17:51:39  jzhao
	Modified for integration.
	
	Revision 1.4  2000/02/24 00:11:14  jzhao
	Modified for port to NT
	
	Revision 1.3  2000/02/23 17:34:19  jzhao
	Added QuaternionNormalize function.
	
	Revision 1.2  2000/02/09 23:52:17  jzhao
	Added DirCosMatrix2Quaternion function.
	Modified CIS Status message.
	
	Revision 1.1  1999/12/13 22:15:40  jzhao
	Initial Release.
	
*******************************************************************************/

#endif /* _MATHFNC_H  */
