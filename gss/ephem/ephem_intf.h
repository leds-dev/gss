#ifndef EPHEM_INTF
#define EPHEM_INTF

/* $Id: ephem_intf.h,v 1.5 2000/05/17 08:07:12 klafter Exp $ */

/****************************************************************/
/* INCLUDES */

/* for Jane's ECI vector type */
#include "CISGlobal.h"

/* for RA_DEC */
#include "mathlib.h"

/* for EPHEM_OBJ_type */
#include "EPHGlobal.h"

/****************************************************************/
/* read object's position in RA-DEC and ECI */
/* also read velocity */

RA_DEC *ephem_get_ra_dec( EPHEM_OBJ_type id );
VECTOR_type *ephem_get_eci_pos( EPHEM_OBJ_type id );
VECTOR_type *ephem_get_eci_vel( EPHEM_OBJ_type id );

/****************************************************************/
/* get and set ephemeris object stayout radius (in degrees) */

void   ephem_set_stayout( char *id, double stayout );
double ephem_get_stayout( EPHEM_OBJ_type id );

/****************************************************************/
/* get object's distance from SC */

double ephem_get_distSC( EPHEM_OBJ_type id );

/****************************************************************/
/* read ini data */

void asteroid_ini_data(	char *id,
			char *name,
			int month,
			int day,
			int year,
			int hour,
			int min,
			double sec,
			double per_dist,
			double e,
			double incl,
			double arg_per,
			double asc_node,
			double mean_anom,
			int display );

void comet_ini_data(	char *id,
			char *name,
			int month,
			int day,
			int year,
			double fraction_of_day,
			double per_dist,
			double e,
			double incl,
			double arg_per,
			double asc_node,
			int display );

/****************************************************************
$Log: ephem_intf.h,v $
Revision 1.5  2000/05/17 08:07:12  klafter
Standardized functional naming convention

Revision 1.4  2000/05/16 22:33:41  jzhao
Added function ephem_get_distSC.

Revision 1.3  2000/05/12 21:36:11  sue
Cleaning up more collisions between the .h files.

Revision 1.2  2000/05/12 21:17:40  sue
Added ephem_intf.h from libastro.
EPHTest.c partitioned into functions matching ephem_intf.h.
Deleting duplicate definitions from EPHGlobal.h.

Revision 1.1  2000/05/12 19:35:35  sue
Added libastro's ephem_intf.h.

****************************************************************/

#endif
