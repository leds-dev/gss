/* ========================================================================
    FILE NAME:
        Eph.c

    DESCRIPTION:

    FUNCTION LIST:

    AUTHOR:	David D. Needelman

 ==========================================================================

    REVISION HISTORY

 ========================================================================== */

 /* System Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/* Self Defined Headers */
#include <MessageIO.h>
#include <CISGlobal.h>
#include <MathFnc.h>
#include <MathAppliFnc.h>
#include <EPHGlobal.h>
#include <Eph.h>
#include <slalib.h>

#define  Q2AXISANGLE_WARNING_TEXT   "Non-normalized quaternion in Q2AxisAngle"

/* ======================================================= */

/* --------------------------------
Eph_Body:
		This function is used to calculate the various parameters
		pertaining to an input celestial body.

Input:
	body:		integer, or EPHEM_OBJ type. Specifies celestial body.
   	time_structure:	TIME_type. Specifies time-of-interest (typically
			current time, when in live mode).

	reference_body_structure:	
			ECI_POSITION_type.  Includes reference body position
			and velocity, expressed in ECI.  The reference body
			is needed because SLALIB calls define desired body
			position and velocity with respect to the reference;
			we need them with respect to the spacecraft.

			The reference body is not used, so the reference
			body is "don't care", when body is Earth.
			
			The reference body is Earth, when body is
			sun, or moon.
			
			The reference body is the sun, otherwise.

	orbital_state_vector:	
			double, with 7 elements.  This vector is only used
			when body is Earth.
			
			The elements represent:
				0-3 - 	Elements of ECI-to-orbit frame
					quaternion (q_o_eci).  Fourth
					element is scalar.
				4 - 	Spacecraft distance from center of
					Earth (meters).  (Taken to be
					GEOSYNC_ORBIT_RADIUS_M for planning
					mode.)
				5 -	Radial component of spacecraft
					velocity (meters/second).  (Taken to
					be 0 for planning mode.)
				6 -	Magnitude of spacecraft angular
					velocity (orbital rate) 
					(radians/second).  (Taken to be
					2 * PI/SECONDS_IN_SIDEREAL_DAY for 
					planning mode.)

	asteroid:	ASTEROID_DATA_type array, with MAX_NUMBER_ASTEROIDS
			entries.  Data only used when body is an asteroid.

	comet:		COMET_DATA_type array, with MAX_NUMBER_COMETS
			entries.  Data only used when body is an comet.

Output:
	body_structure:	Pointer to ECI_POSITION_type.  Position of celestial 
			body in ECI frame, with origin corresponding to 
			spacecraft location.  Data structure includes 
			information in RA/DEC (degrees).
*/

void Eph_Body  (int			body,
		TIME_type		time_structure,
		ECI_POSITION_type	reference_body_structure,
		double			orbital_state_vector [7],
		ASTEROID_DATA_type	asteroid[MAX_NUMBER_ASTEROIDS],
		COMET_DATA_type		comet[MAX_NUMBER_COMETS],
		ECI_POSITION_type	*body_structure)
{
    int		i, status, planet_index, asteroid_index, comet_index; 
    double	pv[6], dummy_double_1, dummy_double_2;
    double	JEpoch = 2000.0;
    double	dvb[3], dpb[3], dvh[3], dph[3];

    VECTOR_type		Z = {0, 0, 1};
    VECTOR_type    	velocity_Earth_o, velocity_sc_o;

    QUATERNION_type	q_eci_o;

    DirCosMatrix_type	c_eci_o;

    memcpy (&body_structure->time_tag, &time_structure, sizeof(TIME_type));

    /* Determine pv[0:5] - the position, and velocity, of the body.  */

    if (body == GSS_SUN)  {

      /* slaEvp calculates Earth's position/velocity in a heliocentric 
	 frame, aligned with ECI.  The additive inverse of those values
	 will be the position/rate of the Sun, in the ECI frame, with 
	 Earth as the origin.  */
	 
	slaEvp (time_structure.modified_julian_date, JEpoch,
	               dvb, dpb, dvh, dph);
	               
	pv[0] = -dph[0];
	pv[1] = -dph[1];
	pv[2] = -dph[2];
	pv[3] = -dvh[0];
	pv[4] = -dvh[1];
	pv[5] = -dvh[2];
	
    }

    else if (body == GSS_MOON)
      slaDmoon (time_structure.modified_julian_date, pv);

    else if (body == GSS_EARTH)  {

      /* Calculate q_eci_o, the orbit frame-to-ECI quaternion.  This
	 will be the inverse of q_o_eci, the ECI-to-orbit frame
	 quaternion, the first four elements of the orbital state 
	 vector.  */

      for (i=0; i<4; i++)
	q_eci_o.Quat[i] = orbital_state_vector[i];
      q_eci_o.Quat[3] *= -1;

      /* Use this quaternion to calculate the orbit frame-to-ECI
	 direction cosine matrix.  */

      QuaternionNormalize(q_eci_o.Quat, q_eci_o.Quat, DOUBLE_DATA);
      Quaternion2DirCosMatrix(q_eci_o.Quat, c_eci_o.DCM, DOUBLE_DATA);

      /* Calculate the ECI position of the Earth.  */

      MatrixMult (c_eci_o.DCM, Z.Vec, body_structure->pos_unit.Vec,
                  3, 3, 1, DOUBLE_DATA);
 
      body_structure->dist_from_sc = orbital_state_vector[4];

      /* Extract the linear velocity of the spacecraft (with respect
	 to the Earth, expressed in the orbit frame) from the orbital
	 state vector.  */

      velocity_sc_o.Vec[0] = 
	orbital_state_vector[6] * orbital_state_vector[4];
      velocity_sc_o.Vec[1] = 0;
      velocity_sc_o.Vec[2] = -orbital_state_vector[5];

      /* Multiply the linear velocity of the spacecraft by -1 to
	 obtain the velocity of the Earth with respect to the s/c.  */

      for (i=0; i<3; i++)
        velocity_Earth_o.Vec[i] = -velocity_sc_o.Vec[i];

      MatrixMult (c_eci_o.DCM, velocity_Earth_o.Vec, 
		  body_structure->velocity.Vec,
                  3, 3, 1, DOUBLE_DATA);
    }

    else if (body <= GSS_PLUTO)  {

      /* Calculate "planet_index" - the SLALIB index for the relevant planet.
      */

      if (body < GSS_MARS)
        planet_index = body - GSS_MERCURY + 1;
      else
        planet_index = body - GSS_MARS + 4;
 
      slaPlanet (time_structure.modified_julian_date, planet_index, 
		 pv, &status);
    }

    else if (body <= GSS_ASTEROID_4)  {

      asteroid_index = body - GSS_ASTEROID_1;

      slaPlanel (time_structure.modified_julian_date, SLA_PLANEL_ASTEROID,
		 asteroid[asteroid_index].epoch,
		 asteroid[asteroid_index].orbinc,
		 asteroid[asteroid_index].anode,
		 asteroid[asteroid_index].perih,
		 asteroid[asteroid_index].aorq,
		 asteroid[asteroid_index].e,
		 asteroid[asteroid_index].aorl,
		 dummy_double_1,
		 pv, &status);
    }

    else if (body <= GSS_COMET_2)  {

      comet_index = body - GSS_COMET_1;

      slaPlanel (time_structure.modified_julian_date, SLA_PLANEL_COMET,
		 comet[comet_index].epoch,
		 comet[comet_index].orbinc,
		 comet[comet_index].anode,
		 comet[comet_index].perih,
		 comet[comet_index].aorq,
		 comet[comet_index].e,
		 dummy_double_1, dummy_double_2,
		 pv, &status);
    }

    /* If "body" is not Earth, then, at this point, we have pv, the 
       body's position-velocity in the ECI frame, using the reference body 
       as the origin.  Convert that to position with s/c origin, and 
       extract the distance between the s/c and the body.
       
       If "body" is Earth, we have the position/velocity and distance we need, 
       so this step is un-necessary.  */

    if (body != GSS_EARTH)  {
      for (i=0; i<3; i++)  {
        body_structure->pos_unit.Vec[i] = 
          reference_body_structure.dist_from_sc *
            reference_body_structure.pos_unit.Vec[i] + 
	  pv[i] * AU_TO_METERS;
        body_structure->velocity.Vec[i] = 
          reference_body_structure.velocity.Vec[i] + pv[i+3] * AU_TO_METERS;
      }
   
      body_structure->dist_from_sc =
        VectorMagnitude (body_structure->pos_unit.Vec, DOUBLE_DATA);

      VectorNormalize (body_structure->pos_unit.Vec, 
		       body_structure->pos_unit.Vec, 
		       DOUBLE_DATA);
    }

    /* Calculate right ascension/declination corresponding to ECI position.  */
 
    ECI2RAscenDeclina (body_structure->pos_unit.Vec, 
		       &body_structure->ra_dec.ra, 
		       &body_structure->ra_dec.dec,
		       DOUBLE_DATA);
}


/* --------------------------------
Eph_InitializeOrbitalStateVector:
		This function is used to initialize the orbit state (ephemeris)
		vector, when in planning mode.

Input:
   	time_structure:		TIME_type.  Specifies data for time-of-
					    interest.
   	j2000:			TIME_type.  Specifies data for J2000 
					    (01 Jan. 2000, 12:00:00 U.T.)
	spacecraft_longitude: 	double.     Specifies longitude, in radians.

Output:
	orbital_state_vector:	
			double, with 7 elements.  The elements represent:
				0-3 - 	Elements of ECI-to-orbit frame
					quaternion (q_o_eci).  Fourth
					element is scalar.
				4 - 	Spacecraft distance from center of
					Earth (meters).  (Taken to be
					GEOSYNC_ORBIT_RADIUS_M for planning
					mode.)
				5 -	Radial component of spacecraft
					velocity (meters/second).  (Taken to
					be 0 for planning mode.)
				6 -	Magnitude of spacecraft angular
					velocity (orbital rate) 
					(radians/second).  (Taken to be
					2 * PI/SECONDS_IN_SIDEREAL_DAY for 
					planning mode.)

      	North_ECI:	VECTOR_type.  ECI-position (with respect to J200) of
				      "north".  The ideal GEO orbit will have
				      the center of the s/c rotating about this
				      axis once per day.
*/

void Eph_InitializeOrbitalStateVector
		(TIME_type		time_structure,
		 TIME_type		j2000,
		 double			spacecraft_longitude,
		 double			orbital_state_vector[7],
		 VECTOR_type		*North_ECI)
{
    int		i, status, planet_index; 

    double	spacecraft_right_ascension;

    VECTOR_type     Y = {0, 1, 0}, Z = {0, 0, 1};
    VECTOR_type     North_TrueOfDate_ECI = {0, 0, 1}, South_ECI;
    VECTOR_type	    Earth_to_Spacecraft_ECI, Spacecraft_to_Earth_ECI;

    QUATERNION_type	    Quat_ECI2ORB;

    DirCosMatrix_type       DCM_Current_to_J2000;

    /* Calculate precession matrix - transformation between True-of-Date
       north ([0, 0, 1], in ECI), and J2000 north (North_ECI).  */
 
    slaPrec (time_structure.julian_epoch, j2000.julian_epoch,
             &DCM_Current_to_J2000.DCM);
 
    MatrixMult (DCM_Current_to_J2000.DCM, North_TrueOfDate_ECI.Vec,
                North_ECI->Vec, 3, 3, 1, DOUBLE_DATA);
 
#ifdef DEBUG
    North_ECI->Vec[0] = 0;
    North_ECI->Vec[1] = 0;
    North_ECI->Vec[2] = 1;
#endif

    for (i=0; i<3; i++)
      South_ECI.Vec[i] = -North_ECI->Vec[i];
 
    /* Calculate spacecraft right ascension in ECI, using longitude
       and Greenwich Mean Sidereal Time.  */
 
    spacecraft_right_ascension =
      spacecraft_longitude + time_structure.gmst;
 
    /* Calculate Earth-to-Spacecraft, in ECI.  Use the calculated
       spacecraft right ascension, and the fact that Earth-to-Spacecraft
       is perpendicular to Earth's axis.  */
 
    Earth_to_Spacecraft_ECI.Vec[0] = cos (spacecraft_right_ascension);
    Earth_to_Spacecraft_ECI.Vec[1] = sin (spacecraft_right_ascension);
    Earth_to_Spacecraft_ECI.Vec[2] =
      -(Earth_to_Spacecraft_ECI.Vec[0] * North_ECI->Vec[0] +
        Earth_to_Spacecraft_ECI.Vec[1] * North_ECI->Vec[1])/
        North_ECI->Vec[2];
 
    VectorNormalize (Earth_to_Spacecraft_ECI.Vec,
                     Earth_to_Spacecraft_ECI.Vec, DOUBLE_DATA);
 
    for (i=0; i<3; i++)
      Spacecraft_to_Earth_ECI.Vec[i] = -Earth_to_Spacecraft_ECI.Vec[i];
 
    /* Calculate orbit frame to ECI transformation */
 
    status = QuatTRIAD (Spacecraft_to_Earth_ECI.Vec, Z.Vec,
                 	South_ECI.Vec, Y.Vec,
                 	Quat_ECI2ORB.Quat);
 
    /* Fill in elements for orbital_state_vector.  */

    for (i=0; i<4; i++)
      orbital_state_vector[i] = Quat_ECI2ORB.Quat[i];
 
    orbital_state_vector[4] = GEOSYNC_ORBIT_RADIUS_M;
    orbital_state_vector[5] = 0;
    orbital_state_vector[6] = GEOSYNC_ORBIT_RATE_RPS;
}


/* --------------------------------
Att_QuatRef2Body:
		This function is used to calculate the quaternion representing
		the transformation from the reference frame (taken to be ECI)
		to the body frame, when in planning mode.

Input:
   	yaw_flip:		int.  	    Represents desired orientation of
					    the body - UPRIGHT or INVERTED.
	orbital_state_vector:	
			double, with 7 elements.  The elements represent:
				0-3 - 	Elements of ECI-to-orbit frame
					quaternion (q_o_eci).  Fourth
					element is scalar.
				4 - 	Spacecraft distance from center of
					Earth (meters).  (Taken to be
					GEOSYNC_ORBIT_RADIUS_M for planning
					mode.)
				5 -	Radial component of spacecraft
					velocity (meters/second).  (Taken to
					be 0 for planning mode.)
				6 -	Magnitude of spacecraft angular
					velocity (orbital rate) 
					(radians/second).  (Taken to be
					2 * PI/SECONDS_IN_SIDEREAL_DAY for 
					planning mode.)

Output:
      	*Quat_Ref2Body:	QUATERNION_type.  
				Represents transformation from Reference-to-
				body frame (q_b_ref).
*/

void Att_QuatRef2Body
		(int			yaw_flip,
		 double			orbital_state_vector[7],
		 QUATERNION_type	*Quat_Ref2BDY)
{
    int		i;

    QUATERNION_type	    Quat_ORB2BDY, Quat_Ref2ORB;

    /* Calculate body-to-orbit frame transformation quaternion.  */

    for (i=0; i<4; i++)
      Quat_ORB2BDY.Quat[i] = 0;
 
    if (yaw_flip == UPRIGHT)
      Quat_ORB2BDY.Quat[2] = 1;		/* UPRIGHT Orbit frame-to-body =
					   [0 0 1 0].  */
    else
      Quat_ORB2BDY.Quat[3] = 1;		/* INVERTED Orbit frame-to-body =
					   [0 0 0 1].  */
      
    /* Fill in elements for reference frame (ECI) to orbit frame 
       transformation quaternion.  */

    for (i=0; i<4; i++)
      Quat_Ref2ORB.Quat[i] = orbital_state_vector[i];
 
    /* Calculate reference frame-to-body transformation quaternion.  */

    QuaternionMult(Quat_Ref2ORB.Quat, Quat_ORB2BDY.Quat,
                   Quat_Ref2BDY->Quat, DOUBLE_DATA);
}

/* -----------------------------------------------------------------------

Angle_Bound:
		This function is used to convert an angle (radians)
		to an equivalent value (differing from the original by a
		multiple of 2 * PI) in the range [0, 2 * PI).

Input/Output:
   	*angle:		double.  Angle (radians).
 
*/
 
void Angle_Bound (double *angle)
{
    double  intermediate_angle;

    intermediate_angle = fmod (*angle, 2 * PI);

    /* At this point, intermediate_angle is in the range (-2 * PI, 2 * PI).
       We need to place it in the range [0, 2 * PI).  */

    if (intermediate_angle < 0)
      *angle = intermediate_angle + 2 * PI;
    else
      *angle = intermediate_angle;
}


/* --------------------------------
Eph_InitializeOrbitalStateVector:
		This function is used to initialize the orbit state (ephemeris)
		vector, when in planning mode.

Input:
   	time_structure:		TIME_type.  Specifies data for time-of-
					    interest.
   	j2000:			TIME_type.  Specifies data for J2000 
					    (01 Jan. 2000, 12:00:00 U.T.)
	spacecraft_longitude: 	double.     Specifies longitude, in radians.

Output:
	orbital_state_vector:	
			double, with 7 elements.  The elements represent:
				0-3 - 	Elements of ECI-to-orbit frame
					quaternion (q_o_eci).  Fourth
					element is scalar.
				4 - 	Spacecraft distance from center of
					Earth (meters).  (Taken to be
					GEOSYNC_ORBIT_RADIUS_M for planning
					mode.)
				5 -	Radial component of spacecraft
					velocity (meters/second).  (Taken to
					be 0 for planning mode.)
				6 -	Magnitude of spacecraft angular
					velocity (orbital rate) 
					(radians/second).  (Taken to be
					2 * PI/SECONDS_IN_SIDEREAL_DAY for 
					planning mode.)

      	North_ECI:	VECTOR_type.  ECI-position (with respect to J200) of
				      "north".  The ideal GEO orbit will have
				      the center of the s/c rotating about this
				      axis once per day.
*/

void Eph_CalculateLongitude
		(TIME_type		time_structure,
		 double			orbital_state_vector[7],
		 double			*spacecraft_longitude)
{
    int		i; 

    double	spacecraft_right_ascension;

    VECTOR_type     Z = {0, 0, 1};
    VECTOR_type	    Earth_to_Spacecraft_ECI, Spacecraft_to_Earth_ECI;

    QUATERNION_type	    Quat_ORB2ECI;

    DirCosMatrix_type	    DCM_ORB2ECI;

    /* Use the elements from the orbital_state_vector to calculate
       the quaternion mapping the orbital to ECI frame.  Note: The
       orbital stat vector gives the ECI-to-orbital frame translation;
       therefore, we need to invert the resulting quaternion.  */

    for (i=0; i<4; i++)
      Quat_ORB2ECI.Quat[i] = orbital_state_vector[i];
 
    Quat_ORB2ECI.Quat[3] *= -1;

    Quaternion2DirCosMatrix(Quat_ORB2ECI.Quat, DCM_ORB2ECI.DCM, DOUBLE_DATA);

    /* Calculate the spacecraft-to-Earth vector, in ECI.  This corresponds
       to the z-axis in the orbital frame.  */

    MatrixMult (DCM_ORB2ECI.DCM, Z.Vec, Spacecraft_to_Earth_ECI.Vec,
                  3, 3, 1, DOUBLE_DATA);
 
    for (i=0; i<3; i++)
      Earth_to_Spacecraft_ECI.Vec[i] = -Spacecraft_to_Earth_ECI.Vec[i];
 
    /* Calculate spacecraft right ascension in ECI, using longitude
       and Greenwich Mean Sidereal Time.  */
 
    spacecraft_right_ascension = 
      atan2 (Earth_to_Spacecraft_ECI.Vec[1], Earth_to_Spacecraft_ECI.Vec[0]);

    *spacecraft_longitude =
      spacecraft_right_ascension - time_structure.gmst;
}


/* -----------------------------------------------------------------------

Time_FillInStructure:
		This function is used to fill in (complete) a TIME structure.
		using GSS time

Input:
	gss_time			GSS_TIME_type structure (defined in
					  EPHGlobal.h)
					GSS time is in seconds, since midnight,
					01 January 1970
Output:
   	*time_structure:		TIME_type structure.

Note: For reference, the TIME_type structure is defined in EPHGlobal.h
      as follows:

    typedef struct  {
        YMDHMS_type     ymdhms;  
        GSS_TIME_type   gss;
        float           fraction_of_day;
        double          modified_julian_date;
        int             day_of_year;
        double          julian_epoch;
        double          gmst;
    } TIME_type;

*/
 
void Time_FillInStructure (GSS_TIME_type gss_time, TIME_type *time_structure) 
{
 
  int		status, realigned_year;
  int		delta_time_days, delta_time_seconds;

  struct tm	tm_structure;

  /* Fill gss time node of time structure.  */
  time_structure->gss = gss_time;

  /* Calculate GMT, in the tm_structure format (defined in time.h).  */
  tm_structure = *gmtime (&(time_structure->gss));

  /* Convert to the YMDHMS_type format.  */
  time_structure->ymdhms.year = 1900 + tm_structure.tm_year;
  time_structure->ymdhms.month = 1 + tm_structure.tm_mon;
  time_structure->ymdhms.day = tm_structure.tm_mday;
  time_structure->ymdhms.hour = tm_structure.tm_hour;
  time_structure->ymdhms.minute = tm_structure.tm_min;
  time_structure->ymdhms.second = tm_structure.tm_sec;

  /* Calculate the fraction-of-day.  */
  time_structure->fraction_of_day = 
    (time_structure->ymdhms.hour * 3600 +
     time_structure->ymdhms.minute * 60 +
     time_structure->ymdhms.second)/SECONDS_IN_SOLAR_DAY;

  /* Calculate the Modified Julian Date.  (Note: the routine slaCaldj assumes
     the time is midnight.  After this call, the returned value has to be
     calculated for the correct time.)  */
  slaCaldj (time_structure->ymdhms.year, 
	    time_structure->ymdhms.month, 
	    time_structure->ymdhms.day,
      	    &time_structure->modified_julian_date, &status);

  time_structure->modified_julian_date += time_structure->fraction_of_day;

  /* Calculate the "realigned year", and day-of-year.  (Note: the "realigned
     year" will equal time_structure->year, for all dates between 1900 March 01
     and 2100 February 28, and a good many dates outside that range; 
     therefore, realigned_year is thrown away here.  day-of-year will not 
     be the expected value outside that range.  Test cases should be within
     the above range.)  */
  slaCalyd (time_structure->ymdhms.year, 
	    time_structure->ymdhms.month, 
	    time_structure->ymdhms.day,
      	    &realigned_year, &time_structure->day_of_year, &status);

  /* Calculate the Julian epoch.  */
  time_structure->julian_epoch = slaEpj (time_structure->modified_julian_date);

  /* Calculate the Greenwich Mean Sidereal Time (GMST).  */
  time_structure->gmst = slaGmst (time_structure->modified_julian_date);
}


/* -----------------------------------------------------------------------

Time_YMDHMS_to_GSS:
		This function is used to convert a time, in YMDHMS_type format
		to GSS time

Input:
   	YMDHMS_structure:		YMDHMS_type structure.

Output:
   	GSS_TIME_structure:		*GSS_TIME_type structure.

*/
 
void Time_YMDHMS_to_GSS (YMDHMS_type	YMDHMS_structure,
			 GSS_TIME_type  *GSS_TIME_structure)
{
 
  int		status;

  double	YMDHMS_modified_julian_date;

  /* Convert the YMDHMS year/month/day to modified julian date.  */
  slaCaldj (YMDHMS_structure.year,
            YMDHMS_structure.month,
            YMDHMS_structure.day,
            &YMDHMS_modified_julian_date, &status);

  /* Adjust the modified julian date to allow for hour/minute/second.  */
  YMDHMS_modified_julian_date +=
    (YMDHMS_structure.hour/24.0 +
     YMDHMS_structure.minute/(24 * 60.0) +
     YMDHMS_structure.second/(24 * 60 * 60.0));

  /* Convert to GSS time, by subtracting off the GSS epoch, then converting
     from days to seconds.  */

  *GSS_TIME_structure = 
    (YMDHMS_modified_julian_date - GSS_EPOCH_IN_MJD) * SECONDS_IN_SOLAR_DAY;
}
 

/******************************************************************************
	Change Log:
	$Log: Eph.c,v $
	Revision 1.1 2003/12/12 14:12:30  tcnguyen
	Normalized q_eci_o.Quat before calculating the orbit frame-to-ECI 
	direction cosine matrix.
	
******************************************************************************/
