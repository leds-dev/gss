/* =======================================================================

	FILE NAME:
			EPHGlobal.h

	DESCRIPTION:
			This is a header file of EPH global definitions. 

==========================================================================

	REVISION HISTORY

========================================================================== */

#ifndef _EPHGLOBAL_H
#define _EPHGLOBAL_H

/* $Id: EPHGlobal.h,v 1.2 1999/12/23 21:12:21 jzhao Exp $ */

#include <time.h>
#include "mathlib.h"
#include "CISGlobalType.h"

/* The value DEBUG is defined to force "North" to always point in the
direction of Earth's axis at the current time, as opposed to J2000.
*/

#define	DEBUG

/* constants */

#define	AU_TO_METERS		1.495979e+11
#define	SECONDS_IN_SOLAR_DAY	86400.0
#define	SECONDS_IN_SIDEREAL_DAY	(SECONDS_IN_SOLAR_DAY * (1 - 1.0/365.25))

#define SECONDS_IN_LEAP_YEAR		(SECONDS_IN_SOLAR_DAY * 366)
#define SECONDS_IN_NON_LEAP_YEAR	(SECONDS_IN_SOLAR_DAY * 365)

#define G_MU			3.986005e14	/* m^3-s^2 */

#define GEOSYNC_ORBIT_RADIUS_M	\
	(pow(SECONDS_IN_SIDEREAL_DAY * sqrt(G_MU)/(2 * PI), 2.0/3.0))

#define	GEOSYNC_ORBIT_RATE_RPS	(2 * PI/SECONDS_IN_SIDEREAL_DAY)

#define GSS_EPOCH_IN_MJD	40587.0

#define MAX_NUMBER_ASTEROIDS	4
#define MAX_NUMBER_COMETS	2

#define EPH_TEST

/* Global variables */

    typedef enum {
	GSS_EARTH,
	GSS_SUN,
	GSS_MOON,

	GSS_MERCURY,
	GSS_VENUS,
	GSS_MARS,
	GSS_JUPITER,
	GSS_SATURN,
	GSS_URANUS,
	GSS_NEPTUNE,
	GSS_PLUTO,

	GSS_ASTEROID_1,
	GSS_ASTEROID_2,
	GSS_ASTEROID_3,
	GSS_ASTEROID_4,

	GSS_COMET_1,
	GSS_COMET_2,

	MAX_EPHEM_OBJS
    } EPHEM_OBJ_type;

/* Structures and enumerated types used for celestial body-related 
   definitions  */

    enum SlaPlanel_Arg_types  {
	SLA_PLANEL_PLANET=1,
	SLA_PLANEL_ASTEROID,
	SLA_PLANEL_COMET
    };


    typedef struct  {
	double		epoch;		/* epoch of perihelion T (TT MDJ) */
	double		orbinc;		/* inclination i T (radians) */
	double		anode;		/* longitude of asc. node T (radians)*/
	double		perih;		/* argument of perihelion (radians) */
	double		aorq;		/* perihelion distance q (AU) */
	double		e;		/* eccentricity e (0 <= e <= 1) */
	double		aorl;		/* mean anomaly M (radians) */
    } ASTEROID_DATA_type;

    typedef struct  {
	double		epoch;		/* epoch of perihelion T (TT MDJ) */
	double		orbinc;		/* inclination i T (radians) */
	double		anode;		/* longitude of asc. node T (radians)*/
	double		perih;		/* argument of perihelion (radians) */
	double		aorq;		/* perihelion distance q (AU) */
	double		e;		/* eccentricity e (0 <= e <= 10) */
    } COMET_DATA_type;

/* Structures and enumerated types used for spacecraft-related definitions  */

    enum Yaw_Flip_Types
		{ UPRIGHT,
		  INVERTED };

/* Structures and enumerated types used for time definitions */

    typedef struct  {
	int	year;
	int	month;
	int	day;
	int	hour;
	int	minute;
	double	second;
    } YMDHMS_type;

    typedef time_t GSS_TIME_type; /* Time in seconds, since 01 January 1970
									 midnight */

    typedef struct  {
	YMDHMS_type  	ymdhms;		/* Structure, holding Greenwich Mean Time */
	GSS_TIME_type	gss;		/* Structure holding GSS time */
	float		fraction_of_day;	/* 0 = midnight; 0.5 = noon */
	double		modified_julian_date;	/* Julian Date - 2400000.5 */
	int		day_of_year;
	double		julian_epoch;
	double		gmst;		/* Greenwich Mean Sidereal Time (radians) */
    } TIME_type;

    typedef struct  {
	char		name[20];	/* name of body */
	TIME_type	time_tag;	/* time structure */
	RA_DEC		ra_dec;		/* right ascension/dec (radians) */
	VECTOR_type	pos_unit;	/* ECI position (unit vector) */
	VECTOR_type	velocity;	/* velocity in ECI (magnitude =
					   speed) (m/s) */
	double		dist_from_sc;	/* distance from spacecraft (m) */
	double		stayout_radius;	/* radius of stayout zone (degrees) */
	double		body_radius;	/* radius of body, used for plotting
					   purposes only (degrees) */
	short int	display_st_fov;	/* flag indicating whether displayed */
	short int	display_skymap;	/* flag indicating whether displayed */
    } ECI_POSITION_type;


extern FILE *LogFp;

#ifdef EPH_TEST
extern FILE *TestOutFp;
extern int TestCount;
#endif

/******************************************************************************
	Change Log:
	$Log: EPHGlobal.h,v $
	Revision 1.2  1999/12/23 21:12:21  jzhao
	Added FOVSize_type and Modified TrackerFOV calling argument list.
	
	Revision 1.1  1999/12/13 17:12:39  jzhao
	Initial Release.
	
******************************************************************************/

#endif /* _EPHGLOBAL_H */
