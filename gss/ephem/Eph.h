/*============================================================
	
	FILE NAME:
		Eph.h

	DESCRIPTION:
		This is header file of Eph.c

 ============================================================= 

	REVISION HISTORY

 ============================================================= */

#ifndef _EPH_H
#define _EPH_H

/* Function Prototypes */

void Angle_Bound
		(double 		*angle);

void Att_QuatRef2Body
                (int                    yaw_flip,
                 double                 orbital_state_vector[7],
                 QUATERNION_type        *Quat_Ref2BDY);

void Eph_Body  (int                     body,
                TIME_type          	time_structure,
                ECI_POSITION_type       reference_body_structure,
		double			orbital_state_vector [7],
                ASTEROID_DATA_type      asteroid[MAX_NUMBER_ASTEROIDS],
                COMET_DATA_type         comet[MAX_NUMBER_COMETS],
                ECI_POSITION_type       *body_structure);

void Eph_InitializeOrbitalStateVector
                (TIME_type              time_structure,
                 TIME_type              j2000,
                 double                 spacecraft_longitude,
                 double                 orbital_state_vector [7],
                 VECTOR_type            *North_ECI);

void Eph_CalculateLongitude
                (TIME_type              time_structure,
                 double                 orbital_state_vector [7],
                 double                 *spacecraft_longitude);

void Time_FillInStructure 	
		(GSS_TIME_type	gss_time,
		TIME_type 	*time_structure);

void Time_YMDHMS_to_GSS
		(YMDHMS_type 	YMDHMS_structure,
		 GSS_TIME_type	*GSS_TIME_structure);

/******************************************************************************
	Change Log:
	$Log: Eph.h,v $
	Revision 1.1  2000/02/17 17:12:38  dneedelman
	Initial Release.
	
******************************************************************************/

#endif /* _EPH_H */
