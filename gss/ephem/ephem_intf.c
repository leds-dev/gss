/* $Id: ephem_intf.c,v 1.2 2000/05/17 08:02:44 klafter Exp $ */
/* ========================================================================
	FILE NAME:
		ephem_intf.c	

	DESCRIPTION:
		This provides an interface layer between the TCL code and 
		the C-based ephemeris code.  It is based on the test driver
		file (EphTest) of the ephemeris code.

	FUNCTION LIST:
						

 ==========================================================================

	REVISION HISTORY

 ========================================================================== */

 /* System Headers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

/* Self Defined Headers */
#include "ephem_wrap.h"
#include "ephem_intf.h"

#include "MathFnc.h"
#include "MathAppliFnc.h"
#include "CISGlobal.h"
#include "MessageIO.h"
#include "EPHGlobal.h"
#include "Eph.h"
#include "slalib.h"
#include "stars.h"

/* #define STAND_ALONE */
/* #define TESTING */

/* ======================================================= */
/* ephemeris data */

static double	spacecraft_longitude, spacecraft_right_ascension;
static double	orbital_state_vector[7];

static char	sign_ra, sign_dec, dummy_character;
static int	sun_ra_hmsf[4], sun_dec_hmsf[4];
static int	moon_ra_hmsf[4], moon_dec_hmsf[4];
static int	planet_ra_hmsf[4], planet_dec_hmsf[4];
static int	asteroid_ra_hmsf[4], asteroid_dec_hmsf[4];
static int	comet_ra_hmsf[4], comet_dec_hmsf[4];


static VECTOR_type	North_ECI;

static QUATERNION_type	Quat_Ref2Body;

static TIME_type	time_structure, j2000;
static TIME_type	asteroid_time[MAX_NUMBER_ASTEROIDS],
			comet_time[MAX_NUMBER_COMETS];

static GSS_TIME_type	tmp_gss_time;

static ECI_POSITION_type	ECI_pos[MAX_EPHEM_OBJS], dummy_ECI_pos;

static ASTEROID_DATA_type	Asteroid[MAX_NUMBER_ASTEROIDS];

static COMET_DATA_type		Comet[MAX_NUMBER_COMETS];

static int yaw_flip = 0;

/*************************************************************/
/* FORWARD DECLARATIONS */

static void init_objs();

/*************************************************************/
/* provide data from Ephemeris code */

/* ======================================================= */

EPHEM_OBJ_type resolve_object (char *id)
{
	EPHEM_OBJ_type id_out;

	if (strcmp(id,"earth") == 0) return GSS_EARTH;
	if (strcmp(id,"sun") == 0) return GSS_SUN;
	if (strcmp(id,"moon") == 0) return GSS_MOON;
	if (strcmp(id,"mercury") == 0) return GSS_MERCURY;
	if (strcmp(id,"venus") == 0) return GSS_VENUS;
	if (strcmp(id,"mars") == 0) return GSS_MARS;
	if (strcmp(id,"jupiter") == 0) return GSS_JUPITER;
	if (strcmp(id,"saturn") == 0) return GSS_SATURN;
	if (strcmp(id,"neptune") == 0) return GSS_NEPTUNE;
	if (strcmp(id,"uranus") == 0) return GSS_URANUS;
	if (strcmp(id,"pluto") == 0) return GSS_PLUTO;
	if (strcmp(id,"asteroid_1") == 0) return GSS_ASTEROID_1;
	if (strcmp(id,"asteroid_2") == 0) return GSS_ASTEROID_2;
	if (strcmp(id,"asteroid_3") == 0) return GSS_ASTEROID_3;
	if (strcmp(id,"asteroid_4") == 0) return GSS_ASTEROID_4;
	if (strcmp(id,"comet_1") == 0) return GSS_COMET_1;
	if (strcmp(id,"comet_2") == 0) return GSS_COMET_2;
}

RA_DEC *ephem_get_ra_dec( EPHEM_OBJ_type id )
{ return( &ECI_pos[id].ra_dec); }

double ephem_get_ra( char *id )
{ return( ECI_pos[resolve_object(id)].ra_dec.ra); }

double ephem_get_dec( char *id )
{ return( ECI_pos[resolve_object(id)].ra_dec.dec); }

VECTOR_type *ephem_get_eci_pos( EPHEM_OBJ_type id )
{ return( &ECI_pos[id].pos_unit); }

VECTOR_type *ephem_get_eci_vel( EPHEM_OBJ_type id )
{ return( &ECI_pos[id].velocity); }

void ephem_set_stayout( char *id, double stayout )
{ ECI_pos[resolve_object(id)].stayout_radius = stayout; }

double ephem_get_stayout( EPHEM_OBJ_type id )
{ return( ECI_pos[id].stayout_radius ); }

double ephem_get_distSC( EPHEM_OBJ_type id )
{ return( ECI_pos[id].dist_from_sc); }

double GetSC_Longitude ()
{
	double sc_long;
	Eph_CalculateLongitude (time_structure, orbital_state_vector, &sc_long);

	if (sc_long > PI)
		sc_long -= 2.0*PI;
	else if (sc_long < -PI)
		sc_long += 2.0*PI;

	return (sc_long*RTD);
}

/* ======================================================= */
/* set longitude */

static void set_long( double longitude )
{
	spacecraft_longitude = longitude * DTR;
	Angle_Bound (&spacecraft_longitude);
}

/* ======================================================= */
/* longitude in degrees */
/* yaw-flip orientation (0=UPRIGHT; 1=INVERTED) */

void ephem_init( double longitude, int cfg_yaw_flip )
{
	int	i, j, status;
	int	GSS_i;
	int	stat = 0;

	init_objs();		/* initialize planet names */

	yaw_flip = cfg_yaw_flip;		/* save yaw flip */

	set_long( longitude );			/* set longitude */

	/* Perform date conversions for J2000 */

	j2000.ymdhms.year = 2000;
	j2000.ymdhms.month = 1;
	j2000.ymdhms.day = 1;

	j2000.ymdhms.hour = 12;
	j2000.ymdhms.minute = 0;
	j2000.ymdhms.second = 0;

	Time_YMDHMS_to_GSS (j2000.ymdhms, &tmp_gss_time);

	Time_FillInStructure (tmp_gss_time, &j2000);

}

/**********************************************************************/
/* Perform date conversions for current date */
/* gss_time is seconds since J2000 */

void ephem_set_time( long int gss_time )
{
	Time_FillInStructure ((GSS_TIME_type) gss_time, &time_structure);
}

/**********************************************************************/
/* Initialize body data.  */

static void init_objs()
{
	/* Sun/Moon/Planets */

	*strcpy (ECI_pos[GSS_SUN].name, "Sun");
	*strcpy (ECI_pos[GSS_MOON].name, "Moon");
	*strcpy (ECI_pos[GSS_EARTH].name, "Earth");
        *strcpy (ECI_pos[GSS_MERCURY].name, "Mercury");
        *strcpy (ECI_pos[GSS_VENUS].name, "Venus");
        *strcpy (ECI_pos[GSS_MARS].name, "Mars");
        *strcpy (ECI_pos[GSS_JUPITER].name, "Jupiter");
        *strcpy (ECI_pos[GSS_SATURN].name, "Saturn");
        *strcpy (ECI_pos[GSS_URANUS].name, "Uranus");
        *strcpy (ECI_pos[GSS_NEPTUNE].name, "Neptune");
        *strcpy (ECI_pos[GSS_PLUTO].name, "Pluto");
}

/**********************************************************************/
/*  get asteroid data from ini file */
void asteroid_ini_data(
			char *id,
			char *name,
			int month,
			int day,
			int year,
			int hour,
			int min,
			double sec,
			double per_dist,
			double e,
			double incl,
			double arg_per,
			double asc_node,
			double mean_anom,
			int display )
{
	int		asteroid_ndx;


	asteroid_ndx = (int)resolve_object(id) - GSS_ASTEROID_1;

	asteroid_time[asteroid_ndx].ymdhms.year = year;
	asteroid_time[asteroid_ndx].ymdhms.month = month;
	asteroid_time[asteroid_ndx].ymdhms.day = day;
	asteroid_time[asteroid_ndx].ymdhms.hour = hour;
	asteroid_time[asteroid_ndx].ymdhms.minute = min;
	asteroid_time[asteroid_ndx].ymdhms.second = sec;

	/* Convert epoch date to GSS_TIME_type  */
	Time_YMDHMS_to_GSS (asteroid_time[asteroid_ndx].ymdhms, 
			      &tmp_gss_time);

	/* Calculate modified julian date from supplied 
	   information  */
	Time_FillInStructure (tmp_gss_time, 
	        &asteroid_time[asteroid_ndx]);

	/* Use ini file values to set body-specific variables.  */
	*strcpy (ECI_pos[resolve_object(id)].name, name);
	ECI_pos[resolve_object(id)].display_st_fov = display;
	ECI_pos[resolve_object(id)].display_skymap = display;

	/* Use ini file values to set asteroid-specific variables.  */
	Asteroid[asteroid_ndx].epoch = 
	  asteroid_time[asteroid_ndx].modified_julian_date;
	Asteroid[asteroid_ndx].orbinc = incl * PI/180;
	Asteroid[asteroid_ndx].anode = asc_node * PI/180;
	Asteroid[asteroid_ndx].perih = arg_per * PI/180;
	Asteroid[asteroid_ndx].aorq = per_dist;
	Asteroid[asteroid_ndx].e = e;
	Asteroid[asteroid_ndx].aorl = mean_anom * PI/180;
}

/**********************************************************************/
/*  get comet data from ini file */
void comet_ini_data(
			char *id,
			char *name,
			int month,
			int day,
			int year,
			double fraction_of_day,
			double per_dist,
			double e,
			double incl,
			double arg_per,
			double asc_node,
			int display )
{
	int		comet_ndx;


	comet_ndx = (int)resolve_object(id) - GSS_COMET_1;

	/* Fill in epoch time - hr/minute/second */


	comet_time[comet_ndx].ymdhms.year = year;
	comet_time[comet_ndx].ymdhms.month = month;
	comet_time[comet_ndx].ymdhms.day = day;
	comet_time[comet_ndx].ymdhms.hour = floor(fraction_of_day * 24.0);
	comet_time[comet_ndx].ymdhms.minute =
		floor ((fraction_of_day - 
			comet_time[comet_ndx].ymdhms.hour/24.0) * 
			24.0 * 60.0);
	comet_time[comet_ndx].ymdhms.second =
		(fraction_of_day - 
			comet_time[comet_ndx].ymdhms.hour/24.0 -
			comet_time[comet_ndx].ymdhms.minute/(24.0 * 60.0)) * 
			24.0 * 60.0 * 60.0;

	/* Convert epoch date to GSS_TIME_type  */
	Time_YMDHMS_to_GSS (comet_time[comet_ndx].ymdhms, &tmp_gss_time);

	/* Calculate modified julian date from supplied information  */
	Time_FillInStructure (tmp_gss_time, &comet_time[comet_ndx]);

	/* Use ini file values to set body-specific variables.  */
	*strcpy (ECI_pos[resolve_object(id)].name, name);
	ECI_pos[resolve_object(id)].display_st_fov = display;
	ECI_pos[resolve_object(id)].display_skymap = display;

	/* Use ini file values to set comet-specific variables.  */
	Comet[comet_ndx].epoch = comet_time[comet_ndx].modified_julian_date;
	Comet[comet_ndx].orbinc = incl * PI/180;
	Comet[comet_ndx].anode = asc_node * PI/180;
	Comet[comet_ndx].perih = arg_per * PI/180;
	Comet[comet_ndx].aorq = per_dist;
	Comet[comet_ndx].e = e;
}

/**********************************************************************/
void ephem_predict_tm()
{

	/* Calculate orbital state vector */
	Eph_InitializeOrbitalStateVector (
					/* inputs */
					time_structure,
					j2000, 
					spacecraft_longitude,
					/* outputs */
					orbital_state_vector,
					&North_ECI
					);

	/* Calculate reference-to-body quaternion */
	Att_QuatRef2Body (yaw_flip, orbital_state_vector, &Quat_Ref2Body);

}

/**********************************************************************/
/* Propagate sim_data to orbital_state_vector & Quat_Ref2Body

Input: adjust_time: time in seconds to propagate ephemeris forward or backward

*/

void update_ephem_from_tm ( int adjust_time )
{
	double half_adjust_angle;
	QUATERNION_type adjust_quat, input_quat, output_quat;

	half_adjust_angle = 0.5 * adjust_time * sim_data.tm.ephem[6];

/*	Adjustment angle is about the negative orbit-2 axis */
	adjust_quat.Quat[0] =  0.0;
	adjust_quat.Quat[1] = -sin(half_adjust_angle);
	adjust_quat.Quat[2] =  0.0;
	adjust_quat.Quat[3] =  cos(half_adjust_angle);

	memcpy(&input_quat, &sim_data.tm.ephem, sizeof(QUATERNION_type));

    QuaternionMult(input_quat.Quat, adjust_quat.Quat, output_quat.Quat, DOUBLE_DATA);

	memcpy(&sim_data.tm.ephem, &output_quat, sizeof(QUATERNION_type));

	memcpy(&orbital_state_vector, &sim_data.tm.ephem, 7*sizeof(double));
	memcpy(&Quat_Ref2Body, &sim_data.tm.eci2body.quat,
						sizeof(QUATERNION_type));
}

/**********************************************************************/
/* Propagate orbital_state_vector & Quat_Ref2Body to tm_data */

void update_tm_from_ephem ()
{
	/* Update sim_data */
	memcpy(&sim_data.tm.eci2body.quat, &Quat_Ref2Body,
						sizeof(QUATERNION_type)); 
	/* Synchronize the direction cosine matrix and the inverse */
	rot_set_q (&sim_data.tm.eci2body, &sim_data.tm.eci2body.quat);

	memcpy(&sim_data.tm.ephem, &orbital_state_vector, 7*sizeof(double));
}

/**********************************************************************/

void ephem_update()
{
	int GSS_i;
	int i;

	/* update ephemeris info for each body. */
	/*Always update earth first. Gimme the Earth */

	Eph_Body (GSS_EARTH, time_structure, dummy_ECI_pos,
		  orbital_state_vector, Asteroid, Comet, &ECI_pos[GSS_EARTH]);

#ifdef STAND_ALONE
	printf ("\n\n	System time is %ld seconds since 1970.",
		ECI_pos[GSS_EARTH].time_tag.gss);

	printf ("\nEarth position is [%f %f %f] A.U.", 
  		ECI_pos[GSS_EARTH].dist_from_sc * 
		  ECI_pos[GSS_EARTH].pos_unit.Vec[0]/AU_TO_METERS, 
  		ECI_pos[GSS_EARTH].dist_from_sc * 
		  ECI_pos[GSS_EARTH].pos_unit.Vec[1]/AU_TO_METERS, 
  		ECI_pos[GSS_EARTH].dist_from_sc * 
		  ECI_pos[GSS_EARTH].pos_unit.Vec[2]/AU_TO_METERS);

	printf ("\nEarth velocity is [%f %f %f] m/s", 
  		ECI_pos[GSS_EARTH].velocity.Vec[0],
  		ECI_pos[GSS_EARTH].velocity.Vec[1],
  		ECI_pos[GSS_EARTH].velocity.Vec[2]);

	printf ("\nEarth stayout region is [%f] degrees", 
  		ECI_pos[GSS_EARTH].stayout_radius);
#endif

	/* Gimme the Sun */

	Eph_Body (GSS_SUN, time_structure, ECI_pos[GSS_EARTH],
		  orbital_state_vector, Asteroid, Comet, &ECI_pos[GSS_SUN]);

#ifdef STAND_ALONE
	printf ("\n\n	System time is %ld seconds since 1970.",
		ECI_pos[GSS_SUN].time_tag.gss);

	printf ("\nSun position is [%f %f %f] A.U.", 
  		ECI_pos[GSS_SUN].dist_from_sc * 
		  ECI_pos[GSS_SUN].pos_unit.Vec[0]/AU_TO_METERS, 
  		ECI_pos[GSS_SUN].dist_from_sc * 
		  ECI_pos[GSS_SUN].pos_unit.Vec[1]/AU_TO_METERS, 
  		ECI_pos[GSS_SUN].dist_from_sc * 
		  ECI_pos[GSS_SUN].pos_unit.Vec[2]/AU_TO_METERS);

	printf ("\nSun stayout region is [%f] degrees", 
  		ECI_pos[GSS_SUN].stayout_radius);

	printf ("\nSun is %f A.U. from spacecraft", 
		ECI_pos[GSS_SUN].dist_from_sc/AU_TO_METERS);

	printf ( "\nSun RA/Dec is [%f %f]", 
		ECI_pos[GSS_SUN].ra_dec.ra * 180/PI, 
		ECI_pos[GSS_SUN].ra_dec.dec * 180/PI);

	slaDr2tf (2, ECI_pos[GSS_SUN].ra_dec.ra, &sign_ra, sun_ra_hmsf); 

	printf ( "\nSun RA is %c%d hrs; %f minutes",
		sign_ra, sun_ra_hmsf[0], 
		(double) sun_ra_hmsf[1] + 
			((double) sun_ra_hmsf[2] +
				((double) sun_ra_hmsf[3])/100.0)/60.0);

	slaDr2af (2, ECI_pos[GSS_SUN].ra_dec.dec, &sign_dec, sun_dec_hmsf); 

	printf ("\nSun Dec is %c%d degrees; %d minutes; %f seconds",
		sign_dec, sun_dec_hmsf[0], sun_dec_hmsf[1], 
		(double) sun_dec_hmsf[2] + ((double) sun_dec_hmsf[3])/100.0);

	printf ("\nSun velocity is [%f %f %f] m/s", 
  		ECI_pos[GSS_SUN].velocity.Vec[0],
  		ECI_pos[GSS_SUN].velocity.Vec[1],
  		ECI_pos[GSS_SUN].velocity.Vec[2]);
#endif

	/* Gimme the moon */

	Eph_Body (GSS_MOON, time_structure, ECI_pos[GSS_EARTH],
		  orbital_state_vector, Asteroid, Comet, &ECI_pos[GSS_MOON]);

#ifdef STAND_ALONE
	printf ("\n\n	System time is %ld seconds since 1970.",
		ECI_pos[GSS_MOON].time_tag.gss);

	printf ( "\nMoon position is [%f %f %f] kilometers", 
		ECI_pos[GSS_MOON].dist_from_sc *
			ECI_pos[GSS_MOON].pos_unit.Vec[0]/1000, 
		ECI_pos[GSS_MOON].dist_from_sc *
			ECI_pos[GSS_MOON].pos_unit.Vec[1]/1000, 
		ECI_pos[GSS_MOON].dist_from_sc *
			ECI_pos[GSS_MOON].pos_unit.Vec[2]/1000);

	printf ("\nMoon stayout region is [%f] degrees", 
  		ECI_pos[GSS_MOON].stayout_radius);

	printf ("\nMoon is %f kilometers from spacecraft", 
		ECI_pos[GSS_MOON].dist_from_sc/1000);

	printf ( "\nMoon RA/Dec is [%f %f]", 
		ECI_pos[GSS_MOON].ra_dec.ra * 180/PI, 
		ECI_pos[GSS_MOON].ra_dec.dec * 180/PI);

	slaDr2tf (2, ECI_pos[GSS_MOON].ra_dec.ra, &sign_ra, moon_ra_hmsf); 

	printf ( "\nMoon RA is %c%d hrs; %f minutes",
		sign_ra, moon_ra_hmsf[0], 
		(double) moon_ra_hmsf[1] + 
			((double) moon_ra_hmsf[2] +
				((double) moon_ra_hmsf[3])/100.0)/60.0);

	slaDr2af (2, ECI_pos[GSS_MOON].ra_dec.dec, &sign_dec, moon_dec_hmsf); 

	printf ( "\nMoon Dec is %c%d degrees; %d minutes; %f seconds",
		sign_dec, moon_dec_hmsf[0], moon_dec_hmsf[1], 
		(double) moon_dec_hmsf[2] + ((double) moon_dec_hmsf[3])/100.0);

	printf ("\nMoon velocity is [%f %f %f] m/s", 
  		ECI_pos[GSS_MOON].velocity.Vec[0],
  		ECI_pos[GSS_MOON].velocity.Vec[1],
  		ECI_pos[GSS_MOON].velocity.Vec[2]);
#endif

	/* Gimme the planets */

	for (GSS_i = GSS_MERCURY; GSS_i <= GSS_PLUTO; GSS_i++)  {

	  Eph_Body (GSS_i, time_structure, ECI_pos[GSS_SUN],
		orbital_state_vector, Asteroid, Comet, &ECI_pos[GSS_i]);

#ifdef STAND_ALONE
	  printf ("\n\n	System time is %ld seconds since 1970.",
		ECI_pos[GSS_i].time_tag.gss);
#endif

	  if (GSS_i < GSS_MARS)
	    i = GSS_i - GSS_MERCURY + 1;
	  else
	    i = GSS_i - GSS_MARS + 4;

#ifdef STAND_ALONE
	  printf ( "\nPlanet %s position is [%f %f %f] A.U.",
		ECI_pos[GSS_i].name,
	 	ECI_pos[GSS_i].dist_from_sc *
			ECI_pos[GSS_i].pos_unit.Vec[0]/AU_TO_METERS,
		ECI_pos[GSS_i].dist_from_sc *
			ECI_pos[GSS_i].pos_unit.Vec[1]/AU_TO_METERS,
		ECI_pos[GSS_i].dist_from_sc *
			ECI_pos[GSS_i].pos_unit.Vec[2]/AU_TO_METERS);

	  printf ("\nPlanet %s stayout region is [%f] degrees", 
  		ECI_pos[GSS_i].name,
  		ECI_pos[GSS_i].stayout_radius);

	  printf ("\nPlanet %s is %f A.U. from spacecraft", 
		ECI_pos[GSS_i].name,
		ECI_pos[GSS_i].dist_from_sc/AU_TO_METERS);

	  printf ("\nPlanet %s RA/Dec is [%f %f]",
		ECI_pos[GSS_i].name,
		ECI_pos[GSS_i].ra_dec.ra * 180/PI, 
		ECI_pos[GSS_i].ra_dec.dec * 180/PI);

	  slaDr2tf (2, ECI_pos[GSS_i].ra_dec.ra, &sign_ra, planet_ra_hmsf); 

	  printf ("\nPlanet %s RA is %c%d hrs; %f minutes", 
		ECI_pos[GSS_i].name,
	  	sign_ra, planet_ra_hmsf[0], 
	  	(double) planet_ra_hmsf[1] + 
	  		((double) planet_ra_hmsf[2] + 
				((double) planet_ra_hmsf[3])/100.0)/60.0);

	  slaDr2af (2, ECI_pos[GSS_i].ra_dec.dec, &sign_dec, planet_dec_hmsf); 

	  printf ("\nPlanet %s Dec is %c%d degrees; %d minutes; %f seconds", 
		ECI_pos[GSS_i].name,
		sign_dec, planet_dec_hmsf[0], planet_dec_hmsf[1], 
		(double) planet_dec_hmsf[2] +
			((double) planet_dec_hmsf[3])/100.0);

	  printf ("\nPlanet %s velocity is [%f %f %f] m/s", 
		ECI_pos[GSS_i].name,
  		ECI_pos[GSS_i].velocity.Vec[0],
  		ECI_pos[GSS_i].velocity.Vec[1],
  		ECI_pos[GSS_i].velocity.Vec[2]);
#endif
	}

	/* Gimme the asteroids  */

	for (GSS_i = GSS_ASTEROID_1; 
         GSS_i < GSS_ASTEROID_1 + MAX_NUMBER_ASTEROIDS; GSS_i++)  {
	  Eph_Body (GSS_i, time_structure, ECI_pos[GSS_SUN],
		    orbital_state_vector, Asteroid, Comet, &ECI_pos[GSS_i]);

#ifdef STAND_ALONE
	  printf ("\n\n	System time is %ld seconds since 1970.",
		  ECI_pos[GSS_i].time_tag.gss);
  
	   printf ("\nAsteroid %s position is [%f %f %f] A.U.",
		  ECI_pos[GSS_i].name,
		  ECI_pos[GSS_i].dist_from_sc *
			  ECI_pos[GSS_i].pos_unit.Vec[0]/AU_TO_METERS,
		  ECI_pos[GSS_i].dist_from_sc *
			  ECI_pos[GSS_i].pos_unit.Vec[1]/AU_TO_METERS,
		  ECI_pos[GSS_i].dist_from_sc *
			  ECI_pos[GSS_i].pos_unit.Vec[2]/AU_TO_METERS);
  
	  printf ("\nAsteroid %s stayout region is [%f] degrees", 
  		  ECI_pos[GSS_i].name,
  		  ECI_pos[GSS_i].stayout_radius);
  
	  printf ("\nAsteroid %s is %f A.U. from spacecraft", 
		  ECI_pos[GSS_i].name,
		  ECI_pos[GSS_i].dist_from_sc/AU_TO_METERS);
  
	  printf ("\nAsteroid %s RA/Dec is [%f %f]",
		  ECI_pos[GSS_i].name,
		  ECI_pos[GSS_i].ra_dec.ra * 180/PI, 
		  ECI_pos[GSS_i].ra_dec.dec * 180/PI);
  
	  slaDr2tf (2, ECI_pos[GSS_i].ra_dec.ra, &sign_ra, asteroid_ra_hmsf); 
  
	  printf ("\nAsteroid %s RA is %c%d hrs; %f minutes", 
		  ECI_pos[GSS_i].name,
		  sign_ra, asteroid_ra_hmsf[0], 
		  (double) asteroid_ra_hmsf[1] + 
	 		  ((double) asteroid_ra_hmsf[2] + 
				  ((double) asteroid_ra_hmsf[3])/100.0)/60.0);
  
	  slaDr2af (2, ECI_pos[GSS_i].ra_dec.dec, &sign_dec, asteroid_dec_hmsf); 
  
	  printf ("\nAsteroid %s Dec is %c%d degrees; %d minutes; %f seconds", 
		  ECI_pos[GSS_i].name,
		  sign_dec, asteroid_dec_hmsf[0], asteroid_dec_hmsf[1], 
		  (double) asteroid_dec_hmsf[2] + 
			  ((double) asteroid_dec_hmsf[3])/100.0);
  
	  printf ("\nAsteroid %s velocity is [%f %f %f] m/s", 
	          ECI_pos[GSS_i].name,
  		  ECI_pos[GSS_i].velocity.Vec[0],
  		  ECI_pos[GSS_i].velocity.Vec[1],
  		  ECI_pos[GSS_i].velocity.Vec[2]);
#endif
    }

	/* Gimme the comets  */

	for (GSS_i = GSS_COMET_1; 
         GSS_i < GSS_COMET_1 + MAX_NUMBER_COMETS; GSS_i++)  {
	  Eph_Body (GSS_i, time_structure, ECI_pos[GSS_SUN],
		    orbital_state_vector, Asteroid, Comet, &ECI_pos[GSS_i]);

#ifdef STAND_ALONE
	  printf ("\n\n	System time is %ld seconds since 1970.",
		  ECI_pos[GSS_i].time_tag.gss);
  
	  printf ("\nComet %s position is [%f %f %f] A.U.",
		  ECI_pos[GSS_i].name,
		  ECI_pos[GSS_i].dist_from_sc *
			  ECI_pos[GSS_i].pos_unit.Vec[0]/AU_TO_METERS,
		  ECI_pos[GSS_i].dist_from_sc *
			  ECI_pos[GSS_i].pos_unit.Vec[1]/AU_TO_METERS,
		  ECI_pos[GSS_i].dist_from_sc *
			  ECI_pos[GSS_i].pos_unit.Vec[2]/AU_TO_METERS);
  
	  printf ("\nComet %s stayout region is [%f] degrees", 
  		  ECI_pos[GSS_i].name,
  		  ECI_pos[GSS_i].stayout_radius);

	  printf ("\nComet %s is %f A.U. from spacecraft", 
		  ECI_pos[GSS_i].name,
		  ECI_pos[GSS_i].dist_from_sc/AU_TO_METERS);
  
	  printf ("\nComet %s RA/Dec is [%f %f]",
		  ECI_pos[GSS_i].name,
		  ECI_pos[GSS_i].ra_dec.ra * 180/PI, 
		  ECI_pos[GSS_i].ra_dec.dec * 180/PI);
  
	  slaDr2tf (2, ECI_pos[GSS_i].ra_dec.ra, &sign_ra, comet_ra_hmsf); 
  
	  printf ("\nComet %s RA is %c%d hrs; %f minutes", 
		  ECI_pos[GSS_i].name,
	  	  sign_ra, comet_ra_hmsf[0], 
	  	  (double) comet_ra_hmsf[1] + 
	  		  ((double) comet_ra_hmsf[2] + 
	    			  ((double) comet_ra_hmsf[3])/100.0)/60.0);
  
	  slaDr2af (2, ECI_pos[GSS_i].ra_dec.dec, &sign_dec, comet_dec_hmsf); 
  
	  printf ("\nComet %s Dec is %c%d degrees; %d minutes; %f seconds", 
		  ECI_pos[GSS_i].name,
		  sign_dec, comet_dec_hmsf[0], comet_dec_hmsf[1], 
		  (double) comet_dec_hmsf[2] + 
			  ((double) comet_dec_hmsf[3])/100.0);
  
	  printf ("\nComet %s velocity is [%f %f %f] m/s", 
	          ECI_pos[GSS_i].name,
  		  ECI_pos[GSS_i].velocity.Vec[0],
  		  ECI_pos[GSS_i].velocity.Vec[1],
  		  ECI_pos[GSS_i].velocity.Vec[2]);
#endif
    }

 
}


/************************************************************************
Change Log:
$Log: ephem_intf.c,v $
Revision 1.2  2000/05/17 08:02:44  klafter
Standardized functional naming convention
Added functions to provide data from Ephemeris

Revision 1.1  2000/05/15 23:30:33  klafter
Renamed EPHTest.c to ephem_intf.c

Revision 1.4  2000/05/12 21:17:40  sue
Added ephem_intf.h from libastro.
EPHTest.c partitioned into functions matching ephem_intf.h.
Deleting duplicate definitions from EPHGlobal.h.

Revision 1.3  2000/05/12 19:13:52  sue
Fixed .h file references.
All data is file static.

Revision 1.2  2000/05/12 19:05:07  sue
Changed MathGlobal.h references to CISGlobal.h.

Revision 1.1  2000/05/12 18:58:08  sue
Initial release from Dave Needelman.
Didn't get the expected results.

*************************************************************************/
