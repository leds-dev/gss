#ifndef EPHEM_WRAP
#define EPHEM_WRAP

/* $Id:$ */

#include "EPHGlobal.h"

/****************************************************************/
/* initialize ephemeris */
/* s/c longitude from ini file */
/* yaw flip from config dialog (planning mode only) */

void ephem_init( double longitude, int cfg_yaw_flip );

/****************************************************************/
/* generate predictive ephemeris pseudo-TM data */
/* routine will fill in SC_EPHEM structure */

void ephem_predict_tm();

/****************************************************************/
/* update position of i-th star */
/* fancy star position calculation. */
/* should NOT be buried inside the ASC catalog. */
/* do not expose ASC, STAR or SLALIB. */
/* fn does not exist in LIBASTRO, at least as a separate routine. */

/*
void ephem_star_pos( int ssc_star );
*/

/****************************************************************/
/* set ephemeris to given time */
/* time is seconds since J2000. */

void ephem_set_time( long int gss_time );

/****************************************************************/
/* update all the ephemeris objects */

void ephem_update();

/****************************************************************/
/* get and set ephemeris object stayout radius (in degrees) */

void ephem_set_stayout( char *id, double stayout );

/****************************************************************/
/* get ra and dec values  */

double ephem_get_ra( char *id );
double ephem_get_dec( char *id );

/****************************************************************/
/* This function returns the spacecraft longitude in deg.       */
double GetSC_Longitude ();

/****************************************************************/
/* propagation of tm */ 

/* planning mode */ 
void update_tm_from_ephem();

/* real-time or historical mode */ 
void update_ephem_from_tm( int adjust_time );

/****************************************************************/
/* read asteroid and comet ini data */ 

void asteroid_ini_data(	char *id,
			char *name,
			int month,
			int day,
			int year,
			int hour,
			int min,
			double sec,
			double per_dist,
			double e,
			double incl,
			double arg_per,
			double asc_node,
			double mean_anom,
			int display );

void comet_ini_data(	char *id,
			char *name,
			int month,
			int day,
			int year,
			double fraction_of_day,
			double per_dist,
			double e,
			double incl,
			double arg_per,
			double asc_node,
			int display );

/****************************************************************/
/* close log file */
/*
void close_logfile ( );
*/
/****************************************************************
$Log:$

****************************************************************/

#endif
