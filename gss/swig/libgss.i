/* $Id: libgss.i,v 1.15 2000/03/01 23:33:46 sue Exp $ */

%module libgss
%readwrite
%include "array.i"

%{
#include "../gui/common.h"
#include "../gui/types.h"
#include "../UtilIncl/MathDef.h"
#include "../gui/mathlib.h"
#include "../gui/pixels.h"
#include "../gui/catalog.h"
#include "../gui/st.h"
#include "../gui/cis_intf.h"
#include "../gui/fov_stars.h"
#include "../gui/annulus.h"
#include "../gui/stars.h"
#include "../gui/user_quat.h"
#include "../gui/draw_objs.h"
#include "../gui/fake_tm.h"
#include "../gtacs/gss_gtacs.h"
#include "../ephem/ephem_wrap.h"
#include "../GuiInterface/TclInterface.h"
#include "../GuiInterface/TclDisplayInter.h"
%}

%readwrite

%include "../gui/common.h"
%include "../gui/types.h"
%include "../UtilIncl/MathDef.h"
%include "../gui/mathlib.h"
%include "../gui/pixels.h"
%include "../gui/annulus.h"
%include "../gui/catalog.h"
%include "../gui/st.h"
%include "../gui/cis_intf.h"
%include "../gtacs/gss_gtacs.h"
%include "../ephem/ephem_wrap.h"
%include "../GuiInterface/TclInterface.h"
%include "../GuiInterface/TclDisplayInter.h"

/* from SWIG example */
%typemap(tcl, argout) POINT * {
	puts( "SWIG POINT argout" );
	char temp[2][ TCL_INTEGER_SPACE ];
	Tcl_PrintInt( interp, *($source->x), &temp[0] );
	Tcl_SetVar(interp, $arg, &temp[0], 0);
	Tcl_PrintInt( interp, *($source->y), &temp[1] );
	Tcl_SetVar(interp, $arg, &temp[1], 0);
	}
%typemap(tcl,in) POINT * {
	puts( "SWIG POINT in" );
	static POINT p;
	$target = &p;
	}

%typemap(tcl, out) PT_LIST * {
	char temp[20];
	int i;
	puts( "SWIG PT_LIST argout" );

	for (i = 0; i < ($source)->len; ++i )
		{
		p = ($source)->points[i];
		Tcl_PrintInt( interp, *($source->x), &temp );
		Tcl_AppendElement(interp, &temp );
		Tcl_PrintInt( interp, *($source->y), &temp );
		Tcl_AppendElement(interp, &temp);
		}
	}

%typemap(tcl,ignore) PT_LIST * {
	puts( "SWIG PT_LIST in" );
	static PT_LIST p;
	$target = &p;
	}

%readonly
%include "../gui/fov_stars.h"
%include "../gui/stars.h"
%include "../gui/user_quat.h"
%include "../gui/draw_objs.h"
%include "../gui/fake_tm.h"

/*
$Log: libgss.i,v $
Revision 1.15  2000/03/01 23:33:46  sue
Forgot to include st.h in C layer.

Revision 1.14  2000/02/22 19:58:07  sue
Wrapped CIS LogFp pointer.

Revision 1.13  2000/02/15 23:40:55  sue
Deleted VEC3 and DIM33 entries; can't get it to work.

Revision 1.12  2000/02/14 23:15:33  sue
Trying to get array typemaps working the way I want.

Revision 1.11  2000/02/09 22:23:20  sue
Added draw_objs to library.
Trying to get TCL array access working so I don't have to write
a ream of interface code.

Revision 1.10  2000/02/01 23:49:43  sue
Added common.h includes.

Revision 1.9  2000/01/19 16:43:43  sue
Forgot to change second annulus include reference from pound to percent.

Revision 1.8  2000/01/19 16:28:36  sue
Added annulus.h reference.

Revision 1.7  1999/12/21 20:07:39  sue
Added fake_tm.h reference.

Revision 1.6  1999/12/09 17:54:20  sue
Moved PT_LIST string into main library.

Revision 1.5  1999/12/07 21:39:47  sue
More work on data types.

Revision 1.4  1999/12/02 20:07:24  sue
Added typemap for POINT * output in argument list.

Revision 1.3  1999/12/02 15:23:42  sue
Added mathlib.h for RA_DEC and HV.

Revision 1.2  1999/12/01 23:49:24  sue
clean target doesn't delete SWIG .i files.
SWIG complains mightily about mathlib.h; need to ifdef out components.

Revision 1.1  1999/11/29 21:12:12  sue
Initial cut at one big gss library.  Done to avoid [fov_]stars.c
interdependency.

*/
